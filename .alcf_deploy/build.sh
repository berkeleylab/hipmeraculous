#!/bin/bash  -l
set -x
set -e


getcores()
{
  if lscpu 
  then
     :
  fi 2>/dev/null | awk '/^CPU\(s\):/ {print $2}'
  if sysctl -a
  then
     :
  fi 2>/dev/null | awk '/^machdep.cpu.core_count/ {print $2}'
}

set -x
[ -n "${HIPMER_BUILD_ENV}" ] || . $(dirname $0)/env.sh

[ -n "${BUILD}" ]
[ -n "${PREFIX}" ]

SRC=$(pwd)
if [ -e "${BUILD}" ]
then 
    cmakecache=${BUILD}/CMakeCache.txt
    if [ -f ${cmakecache} ]
    then
      testsrc=$(grep HipMer_SOURCE_DIR ${cmakecache} | sed 's/.*=//;')
      if [ "${testsrc}" != "${SRC}" ]
      then
        echo "Source dirs do not match ${testsrc} vs ${SRC}.  performing a DIST_CLEAN build to ${SRC}"
        DIST_CLEAN=1
      fi
    fi

    if [ -n "${DIST_CLEAN}" ]
    then
      chmod -R u+w ${BUILD}
      rm -r ${BUILD}
      mkdir ${BUILD}
      rm -rf ${PREFIX}
    elif [ -n "${CLEAN}" ]
    then
      (cd ${BUILD} ; make clean )
    fi

else
    mkdir ${BUILD}
fi

BUILD_TYPE=${BUILD_TYPE:=Release}
cd ${BUILD}
if [ -x /usr/bin/lfs ]
then
  /usr/bin/lfs setstripe -c 1 . 2>/dev/null || /bin/true
fi
export HDF5_ROOT=${HDF5_DIR}
cmakelog=${BUILD}/cmake.log
export TMPDIR=/tmp
if [ ! -f ${cmakelog} ]
then
  time cmake -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ${HIPMER_BUILD_OPTS} ${SRC} 2>&1 | tee -a ${cmakelog}.tmp \
     && mv ${cmakelog}.tmp ${cmakelog}
fi

make_threads=${BUILD_THREADS:=${CORES_PER_NODE:=$(getcores)}}
echo Using $make_threads threads for the build
time make REPLACE_VERSION_H
time make -j ${make_threads} 2>&1 | tee make.err | grep -v "jobserver unavailable" || TMPDIR=/tmp make VERBOSE=1 2>&1 | tee make.err 
# on generic, something in the cmake process causes parallel makes to fail



