#!/bin/bash
#COBALT -q debug-cache-quad
#COBALT --attrs mcdram=cache:numa=quad 
#COBALT -t 60
#COBALT -n 8
#COBALT -A CSC250STPM17
#COBALT --run_project

env

set -e

nodes=${COBALT_JOBSIZE}
cores=${COBALT_PARTCORES}
tasks=$(($nodes * $cores))

echo "Starting on $nodes nodes"
$SCRATCH/hipmer-install-alcf-theta-gnu/bin/test_hipmer.sh human-benchmark 


