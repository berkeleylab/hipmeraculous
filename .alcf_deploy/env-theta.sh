module use /projects/CSC250STPM17/modulefiles
module load upcxx-bupc-narrow

module load cmake/3.14.5

module list

which upcrun
which upcc
which upcxx


#export UPCXX_VERBOSE=1
#export UPC_VERBOSE=1


export HIPMER_ENV=alcf-theta
export CC=$(which cc)
export CXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
export HIPMER_NO_UNIT_TESTS=1

export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=1500}

# memory paramter for at-scale ~2500 knl nodes
export GASNET_COLL_SCRATCH_SIZE=4M
