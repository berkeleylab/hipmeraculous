#!/bin/bash
#COBALT -q debug-cache-quad
#COBALT --attrs mcdram=cache:numa=quad 
#COBALT -t 30
#COBALT -n 2
#COBALT -A CSC250STPM17

env

set -e

$SCRATCH/hipmer-install-alcf-theta/bin/test_hipmer.sh validation validation-mg

$SCRATCH/hipmer-install-alcf-theta/bin/test_hipmer.sh ecoli

$SCRATCH/hipmer-install-alcf-theta/bin/test_hipmer.sh chr14-benchmark


