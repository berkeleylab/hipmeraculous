#!/bin/bash

#export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
set -e

export CORES_PER_NODE=16

N=${N:=2048}

RUNSCRIPT=${RUNSCRIPT:=run_hipmer_unified.sh}

export SCRATCH=/tmp
HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-${USER}-alcf}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/${RUNSCRIPT} ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  #module list || /bin/true

  export THREADS=${THREADS:=${N}}
  export RUNDIR=${RUNDIR:=$SCRATCH/${USER}-human-${THREADS}-${COBALT_JOBID}-$(date '+%Y%m%d_%H%M%S')}
  echo "Preparing ${RUNDIR}"
  mkdir -p $RUNDIR
  # to improve IO performance on edison set stripe to max on scratch
  # lfs setstripe -c -1 ${RUNDIR}
  #echo "Setting stripe to maximum for $RUNDIR"

  $INST/bin/hipmer_setup_human_data.sh ${INST} ${RUNDIR}

#  export ILLUMINA_VERSION=18

  if [ "$KMER_LEN" ]; then
      # if kmer len is set, modify the config file appropriately
      echo "Running kmer length $KMER_LEN"
      grep -v "mer_size" ${RUNDIR}/meraculous-human.config > ${RUNDIR}/tmp.config
      echo "mer_size $KMER_LEN" >> ${RUNDIR}/tmp.config
      mv ${RUNDIR}/tmp.config ${RUNDIR}/meraculous-human.config 
  fi

  export PATH+=":${INST}/bin"
  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=200}
  UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_MB}
  MPIRUN="${MPIRUN:=srun -n}" UPCRUN="${UPCRUN:=upcrun -q -n}" UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_MB} ${INST}/bin/${RUNSCRIPT} ${RUNDIR}/meraculous-human.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi

