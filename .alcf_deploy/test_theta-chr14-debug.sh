#!/bin/bash
#COBALT -q debug-cache-quad
#COBALT --attrs mcdram=cache:numa=quad 
#COBALT -t 60
#COBALT -n 8
#COBALT -A CSC250STPM17
#COBALT --run_project

env

set -e

nodes=${COBALT_JOBSIZE}
cores=${COBALT_PARTCORES}
tasks=$(($nodes * $cores))

for n in 8 2 4 1
do
  echo "Starting intel on $n nodes"
  RUN_OPTS="--threads $(($cores * $n))" \
  $SCRATCH/hipmer-install-alcf-theta/bin/test_hipmer.sh chr14-benchmark >>$COBALT_JOBID.intel.$n-nodes 2>&1 & 
  sleep 15

  #echo "Starting gnu on $n nodes"
  #RUN_OPTS="--threads $(($cores * $n))" \
  #$SCRATCH/hipmer-install-alcf-theta-gnu/bin/test_hipmer.sh chr14-benchmark >>$COBALT_JOBID.gnu.$n-nodes 2>&1 & 
  #sleep 15
done

wait


