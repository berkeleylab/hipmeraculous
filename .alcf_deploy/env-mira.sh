soft add +bgqtoolchain-gcc484  
soft add +mpiwrapper-gcc   
soft add +cmake-3.3.0 
soft add +git-2.3.0 

#export PATH=$HOME/install/bin:$PATH


export BUPC_INST=/home/projects/pgas/berkeley_upc-2.26.0/V1R2M2/gcc-narrow/
export PATH=$BUPC_INST/bin:$PATH
export UPCRUN="$BUPC_INST/bin/upcrun -n"

export TMPDIR=${TMPDIR:=/tmp}
export HIPMER_ENV=alcf
export CC=$(which mpicc)
export CXX=$(which mpicxx)
export HIPMER_BUILD_OPTS="-DHIPMER_NO_LINK_MPI_LIBS=1 -DZLIB_ROOT=/soft/libraries/alcf/20151124/gcc/ZLIB -DDISCOVER_LIBC=1 -DCMAKE_UPC_COMPILER=$BUPC_INST/bin/upcc"
export HIPMER_NO_UNIT_TESTS=1
export HIPMER_FULL_BUILD=0

#export HIPMER_BUILD_OPTS="-DZLIB_ROOT=/soft/libraries/alcf/20151124/gcc/ZLIB -DDISCOVER_LIBC=1"
export HIPMER_BUILD_TYPE="Release"
export HIPMER_ALTIVEC=1

