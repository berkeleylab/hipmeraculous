#!/bin/bash

#export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
set -x

export CORES_PER_NODE=16

N=${N:=512}

RUNSCRIPT=${RUNSCRIPT:=run_hipmer_unified.sh}

export PROJECT_DIR=/projects/PEACEndStation_2
export SCRATCH=${HOME}/projects/hipmer
HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-${USER}-alcf}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/${RUNSCRIPT} ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  #module list || /bin/true

  export THREADS=${THREADS:=${N}}
  export RUNDIR=${RUNDIR:=$SCRATCH/${USER}-chr14-${THREADS}-${COBALT_JOBID}-$(date '+%Y%m%d_%H%M%S')}
  echo "Preparing ${RUNDIR}"
  mkdir -p $RUNDIR

  $INST/bin/hipmer_setup_chr14_data.sh ${INST} ${RUNDIR}

  export ILLUMINA_VERSION=18

  export PATH+=":${INST}/bin"
  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=100}
  cd ${RUNDIR}
  MPIRUN="${MPIRUN:=srun -n}" UPCRUN="${UPCRUN:=upcrun -q -n}" UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_MB} ${INST}/bin/${RUNSCRIPT} ${RUNDIR}/meraculous-chr14.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi

