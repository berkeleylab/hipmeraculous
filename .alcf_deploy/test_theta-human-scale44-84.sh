#!/bin/bash
#COBALT -q default
#COBALT --attrs mcdram=cache:numa=quad 
#COBALT -t 60
#COBALT -n 128
#COBALT -A CSC250STPM17
#COBALT --run_project

env

set -e

nodes=${COBALT_JOBSIZE}
cores=${COBALT_PARTCORES}
tasks=$(($nodes * $cores))

for n in 44 84 
do
  echo "Starting on $n nodes"
  RUN_OPTS="--threads $(($cores * $n))" \
  $SCRATCH/hipmer-install-alcf-theta-gnu/bin/test_hipmer.sh human-benchmark >>$COBALT_JOBID.gnu.$n-nodes 2>&1 & 
  sleep 15
done

wait


