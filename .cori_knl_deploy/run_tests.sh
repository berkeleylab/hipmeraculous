#!/bin/bash

set -e

myjobs=/tmp/${USER}.$$
if .misc_deploy/build_envs.sh install .cori_knl_deploy/env-debug.sh .cori_knl_deploy/env.sh
then
  if /bin/true
  then
    inst=$SCRATCH/hipmer-install-cori-knl-dbg/bin
#
# currently debug execution on KNL fails!!
#
#    AUTORESTART=0 CACHED_IO=0 sbatch --parsable --job-name="Hipmer-knl-debug-validation-nocached_io" --nodes=2 --qos=debug --account=m2865 --time=30:00 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh validation-par_hmm validation
#    AUTORESTART=0 CACHED_IO=0 sbatch --parsable --job-name="Hipmer-knl-debug-validation-nocached_io" --nodes=2 --qos=debug --account=m2865 --time=10:00 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh validation
#    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-knl-validation"                   --nodes=4 --qos=debug --account=m2865 --time=10:00 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh validation-mg

    inst=$SCRATCH/hipmer-install-cori-knl/bin
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-knl-ecoli"            --time=30:00 --nodes=16 --qos=debug --account=m2865 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh validation validation-mg_cgraph ecoli chr14-benchmark
#    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-knl-chr14-benchmark"               --nodes=16 --qos=debug --account=m2865 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh chr14-benchmark
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-knl-mg250"                         --nodes=64 --qos=regular --account=m2865 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh mg250-short
#    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-knl-human-benchmark"               --nodes=256 --qos=regular --account=m2865 ${inst}/sbatch_cori-knl.sh ${inst}/test_hipmer.sh human-benchmark

  fi | tee $myjobs
else
  echo "FAILED TO BUILD!!" 1>&2
  exit 1
fi

echo $(cat ${myjobs})
echo less $(
for j in $(cat ${myjobs})
do
  echo slurm-${j}.out
done)
rm $myjobs
