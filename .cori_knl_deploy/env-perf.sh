#!/bin/bash

export MPICH_GNI_FMA_SHARING=ENABLED
module remove darshan
module rm craype-haswell
module load craype-mic-knl
module load PrgEnv-intel
module load git
module load cmake
module load python
module load perftools-base
module load perftools

#module swap intel/16.0.3.210
#module rm bupc-narrow
#module rm bupc-narrow-4Kpages
#module load bupc-narrow-4Kpages
#export PATH=/global/cscratch1/sd/hargrove/bld-cori-knl-noexit/install/bin:$PATH
# Use special build which replaces _exit() with exit() so perftools can cleanup their files
#export PATH=/global/cscratch1/sd/hargrove/bld-cori-knl-noexit/install2/bin:$PATH
module swap intel/17.0.1.132
module load bupc-narrow/2.24.2

module list

# until pthreads build is available
export GASNET_NETWORKDEPTH_SPACE=1K
export PMI_MMAP_SYNC_WAIT_TIME=75

export HIPMER_ENV=cori-knl-perf
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BUILD_OPTS=""
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=700}
export HIPMER_FULL_BUILD=0
export USE_SBCAST=${USE_SBCAST:=0} # cray pat has a bug that requires the binary NOT be broadcast 
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export MPICH_GNI_MALLOC_FALLBACK=enabled
export HUGETLB_MORECORE=no

# files to copy to HIPMER_INSTALL
export HIPMER_BIN_SCRIPTS="sbatch_cori-knl.sh"
# copy to HIPMER_INSTALL and use as upcrun -conf=
export HIPMER_UPCRUN_CONF=upcrun-knl.conf

export HIPMER_POST_INSTALL=test/hipmer/CrayPat.sh # build the pat binary
export HIPMER_NO_UNIT_TESTS=1
export HIPMER_ENABLE_TESTING=OFF
export MPICH_GNI_NDREG_ENTRIES=1024


