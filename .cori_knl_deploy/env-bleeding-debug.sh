#!/bin/bash

export MPICH_GNI_FMA_SHARING=ENABLED
module swap PrgEnv-intel
module rm craype-haswell
module load craype-mic-knl
module load git
module load darshan
module load cmake
module load python
module load gcc
module swap gcc/10.3.0

# use the GASNet-EX builds
module load upcxx-bupc-narrow/bleeding-edge

module list
which upcrun

# until pthreads build is available
export GASNET_NETWORKDEPTH_SPACE=1K
export PMI_MMAP_SYNC_WAIT_TIME=75
export GASNET_GNI_FMA_SHARING=1

export HIPMER_ENV=cori-knl-bleeding-dbg
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Debug"
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=700}
export HIPMER_FULL_BUILD=0
export USE_SBCAST=${USE_SBCAST:=1}
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export HIPMER_HMMER_CONFIGURE_OPTS=--host=x86_64
export MPICH_GNI_MALLOC_FALLBACK=enabled
export HUGETLB_MORECORE=no
export AUTORESTART=${AUTORESTART:=0}

# files to copy to HIPMER_INSTALL (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS="sbatch_cori-knl.sh"
# copy to HIPMER_INSTALL and use as upcrun -conf=  (located in .misc_deploy)
export HIPMER_UPCRUN_CONF=upcrun-knl.conf

export HIPMER_NO_UNIT_TESTS=1
export HIPMER_ENABLE_TESTING=OFF
export GASNET_BACKTRACE=${GASNET_BACKTRACE:=1}
export MPICH_GNI_NDREG_ENTRIES=1024

