#!/bin/bash

export MPICH_GNI_FMA_SHARING=ENABLED
module rm PrgEnv-intel
module load PrgEnv-cray
module load craype-haswell
module load craype-mic-knl
module load git
module load darshan
module load cmake


module list

# until pthreads build is available

export HIPMER_ENV=cori-knl-crayupc
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BUILD_OPTS=""
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=700}
export HIPMER_NO_UNIT_TESTS=1
export HIPMER_ENABLE_TESTING=OFF
export HIPMER_FULL_BUILD=0
export UPCRUN="srun"
export USE_SBCAST=${USE_SBCAST:=1}
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export HIPMER_HMMER_CONFIGURE_OPTS=--host=x86_64
export HIPMER_NO_CGRAPH=1

# files to copy to HIPMER_INSTALL (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS="sbatch_cori-knl.sh"

