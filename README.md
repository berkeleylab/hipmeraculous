______________________________________________________________________________

    HipMer v 1.0, Copyright (c) 2019, The Regents of the University of California,
    through Lawrence Berkeley National Laboratory (subject to receipt of any
    required approvals from the U.S. Dept. of Energy).  All rights reserved.
 
    If you have questions about your rights to use or distribute this software,
    please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.
 
    NOTICE.  This Software was developed under funding from the U.S. Department
    of Energy and the U.S. Government consequently retains certain rights. As such,
    the U.S. Government has been granted for itself and others acting on its behalf
    a paid-up, nonexclusive, irrevocable, worldwide license in the Software to
    reproduce, distribute copies to the public, prepare derivative works, and
    perform publicly and display publicly, and to permit other to do so. 

______________________________________________________________________________

# *HipMer* -- High Performance Meraculous


>
> HipMer is a high performance, distributed memory and scalable version of [Meraculous](http://jgi.doe.gov/data-and-tools/meraculous/), a de novo genome assembler. HipMer and MetaHipMer share the same source code but have different workflows for single genome and metagenome assemblies respectively.
>
> HipMer is a PGAS application, and the main software dependencies are the [UPC language](https://upc.lbl.gov) and [UPC++ library](https://upcxx.lbl.gov), both of which use [GASNet-EX](https://gasnet.lbl.gov) for communication.
> It can run on most any system from laptops to supercomputers, but requires a high speed and low latency networking interface for any cluster such as infiniband to scale efficiently.
>
> This project is a joint collaboration between [JGI](http://jgi.doe.gov), 
> [NERSC](http://www.nersc.gov/) and [CRD](http://crd.lbl.gov/) and is primarily funded by the [ExaBiome](https://sites.google.com/lbl.gov/exabiome/) project, one of the [US Department of Energy (DOE)](https://www.energy.gov)'s [Exascale Computing Projects (ECP)](https://www.exascaleproject.org)
>
> Primary authors are:
> Evangelos Georganas, Aydın Buluç, Steven Hofmeyr, Rob Egan and Eugene Goltsman, 
> with leadership, direction and advice from Kathy Yelick and Leonid Oliker.
>
> The original Meraculous was developed by Jarrod Chapman, Isaac Ho, Eugene Goltsman,
> and Daniel Rokhsar.

### Related Publications

* Evangelos Georganas, Rob Egan, Steven Hofmeyr, Eugene Goltsman, Bill Arndt, Andrew Tritt, Aydın Buluç, Leonid Oliker and Katherine Yelick, ["Extreme Scale De Novo Metagenome Assembly"](https://arxiv.org/abs/1809.07014). International Conference for High Performance Computing, Networking, Storage and Analysis (“Supercomputing”, SC 2018), Dallas, Texas, November 2018.  Best Paper Finalist. 

* Evangelos Georganas, Steven Hofmeyr, Leonid Oliker, Rob Egan, Daniel Rokhsar, Aydin Buluc, Katherine Yelick.  ["Extreme-Scale De Novo Genome Assembly"](http://books.google.com/books?hl=en&lr=&id=smlQDwAAQBAJ&oi=fnd&pg=PA409&dq=info:LKO0MYbJhCcJ:scholar.google.com&ots=Erhvo7a74R&sig=lowj2GFSLb-rtVijICflDHTRiq4), Exascale Scientific Applications: Scalability and Performance Portability, CRC Press, November 13, 2017.

* Marquita Ellis, Evangelos Georganas, Rob Egan, Steven Hofmeyr, Aydin Buluc, Brandon Cook, Leonid Oliker, and Katherine Yelick. ["Performance characterization of de novo genome assembly on leading parallel systems"](https://people.eecs.berkeley.edu/~aydin/HipMer-EuroPar17.pdf). In EuroPar - International European Conference on Parallel and Distributed Computing, 2017.

* Evangelos Georganas. ["Scalable Parallel Algorithms for Genome Analysis"](https://people.eecs.berkeley.edu/~egeor/thesis.pdf). PhD thesis, EECS Department, University of California, Berkeley, August 2016. 

* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Steven Hofmeyr, Chaitanya Aluru, Rob Egan, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["HipMer: An Extreme-Scale De Novo Genome Assembler"](http://www.eecs.berkeley.edu/~egeor/sc15_genome.pdf). 27th ACM/IEEE International Conference on High Performance Computing, Networking, Storage and Analysis (SC 2015), Austin, TX, USA, November 2015.

* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["merAligner: A Fully Parallel Sequence Aligner"](http://www.eecs.berkeley.edu/~egeor/ipdps_genome.pdf). 29th IEEE International Parallel & Distributed Processing Symposium (IPDPS 2015), Hyderabad, INDIA, May 2015.

* Jarrod A Chapman, Martin Mascher, Aydın Buluç, Kerrie Barry, Evangelos Georganas, Adam Session, Veronika Strnadova, Jerry Jenkins, Sunish Sehgal, Leonid Oliker, Jeremy Schmutz, Katherine A Yelick, Uwe Scholz, Robbie Waugh, Jesse A Poland, Gary J Muehlbauer, Nils Stein and Daniel S Rokhsar ["A whole-genome shotgun approach for assembling and anchoring the hexaploid bread wheat genome"](http://genomebiology.biomedcentral.com/articles/10.1186/s13059-015-0582-8) . Genome Biology 2015, 16:26 .

* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["Parallel De Bruijn Graph Construction and Traversal for De Novo Genome Assembly"](http://www.eecs.berkeley.edu/~egeor/sc14_genome.pdf). 26th ACM/IEEE International Conference on High Performance Computing, Networking, Storage and Analysis (SC 2014), New Orleans, LA, USA, November 2014.

-----------------------
## Building and installing

HipMer can run on compute platforms of any size and scale from the largest Cray supercomputers like
those hosted at [NERSC](https://www.nersc.gov) to smaller linux clusters (with low latency networks)
and also on *any* single Linux.  The only requirement is a properly configured set of compilers for your platform.

### Requirements

1. Working [Unified Parallel C - UPC](https://en.wikipedia.org/wiki/Unified_Parallel_C) Environment
    1. [Berkeley UPC >= 2021.4](http://upc.lbl.gov)
        a. Must include support for Clang's UPC2C translator (It is known that version 3.9.1-1 has issues compiling with gcc > 7.x, so recommend using [>= 9.0.1-2 from](https://github.com/clangupc/upc2c) )
    2. see contrib/install_upc.sh for a simplified install
2. Working UPC++
    1. [Berkeley UPC++ >= 2021.3](https://upcxx.lbl.gov) Environment
    2. see contrib/install_upc.sh for a simplified install
3. The GASnet communications Layer
    1. The same version as both Berkeley UPC++ and Berkeley UPC was built against
    2. see contrib/install_upc.sh for a simplified install
4. Working C/C++ compiler with C++17 and C11 standards
    1. Intel >= 17.0.2
    2. GCC >= 7.5
    3. CLang >= 4.0.0
5. CMake >= 3.8 (>= 3.13 recommended)
6. (optional) [upcxx-utils >= 0.2.0](https://bitbucket.org/berkeleylab/upcxx-utils/) (provided as a git submodule when building if necessary)
7. Some clusters require 'mpirun' for spawning job, and if so UPC and UPC++ should be compiled with MPI support
8. (optional) python psutil module - for extra support in profiling and monitoring the execution

To assist in installing UPC++ and UPC we have the following script that downloads and installs this software.  Be sure to supply the correct Conduit for your hardwar platform.  Performance over the mpi and udp "compatiblity" conduits will not be good.  The smp conduit will only work for single-node shared memory platforms.

```
contrib/install_upc.sh Conduit(smp|ibv|ofi|aries|mpi|udp) [ SHARED_MEMORY(posix|xpmem|sysv|file|none) [ INSTALL_DIR(~/install) [ BUILD_DIR(/dev/shm) [ CODE_DIR(~/) ] ] ] ]
You must choose a default networking conduit: smp, mpi, udp, ibv or ofi
```

(experimental) See README-MacOSX.md for instructions on how to prepare the compilers for 
a Mac running OS X 10.14

### Maintained Installations

We have built, configured, deployed and tested HipMer on several platforms:

1. [NERSC](https://www.nersc.gov)'s Cori supercomputer
   * module load HipMer/latest or HipMer/latest-knl
2. LBL's IT super cluster [Lawrencium](https://scs.lbl.gov/Systems)
   * ~rsegan/install/hipmer-install-lawrencium
3. [Amazon Web Services](https://aws.amazon.com) 
   * AMI (US N. Virginia zone, search AMIs for "HipMer SMP")
4. [KBase](https://kbase.us)


### Download HipMer

Anyone can download releases of the source from sourceforge: https://sourceforge.net/projects/hipmer/

Or, if you have access, download or clone the source from bitbucket.org: https://bitbucket.org/berkeleylab/hipmeraculous 
Note this repository requires git [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to be cloned as well


```
git clone https://bitbucket.org/berkeleylab/hipmeraculous.git
cd hipmeraculous
git submodule init
git submodule update
```


### Building

To build, install and run test cases, simple configurations (standard Linux distros) no configuration
should be necessary. (see README-Linux.md for details for a complete install of prerequisites on Ubuntu)

HipMer builds with cmake, so if all the requirements for building are in the PATH environment you
can simply build with these commands

```
mkdir build
cd build
# (optional or have upcxx in PATH) UPCXX_DIR=/path/to/upcxx \
# (optional (must be same as what upcxx uses)) CC=/path/to/C_compiler CXX=/path/to/C++_compiler
cmake .. -DCMAKE_INSTALL_PREFIX=/path/to/install \
# (optional or have upcc in PATH) -DCMAKE_UPCC_COMPILER=/path/to/upcc
make install
```

For more complex hardware environments (like HPC clusters), check the env scripts from the appropriate 
.platform_deploy, where 'platform' is one of several different platforms, e.g.:
* .cori_deploy for NERSC's cori hawell system
* .cori_knl_deploy for NERSC's cori Knights Landing system
* .lawrencium_deploy for LBL IT inifiband super cluster
* .generic_deploy for a generic Linux system

Then copy, modify and/or link the appropriate env.sh script to the top-level source directory (this dir)
as hipmer_env.sh.  See hipmer_env.sh-EXAMPLE for a comprehensive list of variables that can (optionally)
be specified

This will be your machine-specific environment script and it will be installed and then used whenever
HipMer executes.

Alternatively to making a hipmer_env.sh script in the top level source dirctory, you can export the path
to the HIPMER_ENV_SCRIPT variable or put it into your environment or as an ENV=val command line
argument to several of the scripts (i.e. hipmer, hipmer.py, test_hipmer.sh, and bootstrap_hipmer_env.sh):

   export HIPMER_ENV_SCRIPT=path/to/env.sh

Then execute the bootstrap build/install script:

   ./bootstrap_hipmer_env.sh [HIPMER_ENV_SCRIPT=/path/to/hipmer_env.sh]  [build|install]

Within the HIPMER_ENV_SCRIPT you should specify the environmental variable SCRATCH for default placement 
of the build and install paths. You can also change the default build and install 
paths by overriding the environmental variables HIPMER_BUILD and HIPMER_INSTALL variables


To build and install:

    ./bootstrap_hipmer_env.sh install

To only perform the build:

    ./bootstrap_hipmer_env.sh build


By default, the build will be in $HIPMER_BUILD or $SCRATCH/${USER}-hipmer-build-${HIPMER_ENV} and the install will 
be in $HIPMER_INSTALL $SCRATCH/hipmer-install-${HIPMER_ENV}

You should then be able to executue the hipmer pipeline by specifying the fully-qualified path to 
hipmer  ($HIPMER_INSTALL/bin/hipmer <options>)

There are environmental variables that are automatically set for a release 
(non-debug) build (.platform_deploy/env.sh). To build a debug version, set 
HIPMER_BUILD_TYPE=Debug in the HIPMER_ENV_SCRIPT used to build and install.

Examples can be found at .*_deploy/*-debug.sh

Then install with the bootstrap_hipmer_env.sh script

To force a complete rebuild:

    CLEAN=1 ./bootstrap_hipmer_env.sh install

To force a rebuild with all the environment checks:

    DIST_CLEAN=1 ./bootstrap_hipmer_env.sh install

Note that running ./bootstrap_hipmer_env.sh install should do partial rebuilds for 
changed files.

WARNING: the build process does not detect header file dependencies for UPC 
automatically, so changes to header files will not necessarily trigger 
rebuilds. The dependencies need to be manually added. This has been done for 
some, but not all, stages. So if any code gets changed in a header file, it is recommended to
build with CLEAN=1.

Some features of the cmake build process:

  * Builds multiple binaries based on the build parameters:
  export HIPMER_BUILD_OPTS="-DHIPMER_KMER_LENGTHS='32; 64'" 
  (HIPMER_KMER_LENGTHS need to be in multiples of 32)
  * Properly builds UPC source (if you name the source .upc or set the LANGUAGE and LINKER_LANGUAGE
  property to UPC)  
  * Sets the -D definition flags consistently 
  * Supports -DCMAKE_BUILD_TYPE=Release or Debug

Some special environment variables (advanced):

  * HIPMER_FULL_BUILD=0 (Default is 1. When set to 1, builds a separate executable for each stage,
    and runs them separately. When set to 0, builds a single executable incorporating all the
    stages.)

-------
## Troubleshooting

Some people had seen the following error during build: 

    make: *** No rule to make target REPLACE_VERSION_H.  Stop.

That's a cmake target that gets executed before the rest of the pipeline and we think that if the
build directory was only partially configured, then cmake forgets to properly prepare that target
the next time around. Try executing:

    DIST_CLEAN=1 ./bootstrap_hipmer_env.sh install


## Preprocessing

see the PREPROCESSING.md for instructions on how to prepare your fastq files

-------
## Running

The new way to execute is to use the hipmer script (which wraps hipmer.py).  Both need to be in your PATH (i.e. $HIPMER_INSTALL/bin)

```
$ hipmer -h

Options:

  -h                                  Show options
  -o STRING, --outdir STRING          Output directory (hipmer-run-2019-06-06-134838.479141)
  -k STRING                           Comma-separated list of kmer lengths (21,33,55)
  -s STRING, --single STRING          Single read library, with modifiers - see below
  -p STRING, --paired STRING          Paired read library, with modifiers - see below
  -i STRING, --interleaved STRING     Interleaved read library, with modifiers - see below
  --meta                              Assemble metagenome
  --merge-reads INT                   Merge overlapping paired reads: 0 - never, 1 - always, 2 - in metagenome scaffolding only (1)
  --quality STRING                    Quality tuning for metagenomes: minerror, normal or maxctgy (normal)
  --cached-io                         Use cached IO, on by default when multi-node
  --checkpoint                        Checkpoint after successful stages, on by default with cached-io
  --auto-restart                      Attempt to restart if a stage fails (True)
  --resume                            Attempt to resume an aborted or interrupted run. Default is to autodetect.
                                      Set to False to avoid an attempted resume of an existing run directory.
  -v--verbose                         Verbose output: a *lot* of detailed run information
  --advanced-help                     Show more advanced options

Multiple libraries with modifiers can be specified for each of -s, -p and -i.
Libraries with ".fofn" extensions are files of file names, with one file per line; otherwise fastq is expected.
Libraries (and fofn) can be specified with the following modifiers:
   :i<insertSize> with :s<standardDeviation>  (for paired or interleaved libraries, of course)
   :rc (library is reverse forward <--  -->, and has a small % of "innie" reads with a very short insert size: --> <--)
   :nocontig (do not use this library in the contig stages - i.e. high indel rate)
The following modifiers apply only to single genome assembly:
   :nosplint (do not use this library for splinting - i.e. high chimeric rate)
   :nogapclosing (do not use this library in the gapclosing stage)
Additionally, libraries can be specfied with glob expansion wildcards within the fofn
   (and on the command line with appropriate escaping to prevent shell expansion)

```
    
There are also many advanced options that will be listed when run with '--advanced-help' which are available mainly for developers.

hipmer attempts to parse the job environment (SLURM and Grid Engine supported) to auto-detect the job shape (i.e. the nodes, cores per node and total threads) to spawn the executable.
You can override what was detected with the --threads and --cores-per-node options.  If this is not working, one may need to play with the upcrun configuration to properly spawn on your cluster.  See the Berkeley UPC documentation for upcrun.

Additionally, for convenience, there is a script test_hipmer.sh that sets up and executes a run on a few
standard datasets. It creates an unique output directory ($RUNDIR) in $SCRATCH, copies the config file
into that directory, links all data files into the directory, and executes the run script.  This script
will properly bootstrap if called with the fully qualified path.

   ${HIPMER_INSTALL}/bin/test_hipmer.sh [validation|validation-mg|validation2D-diploid|ecoli|chr14|human|mg250]

To see examples of how the test_hipmer.sh script is used, use the test_hipmer.sh script which is installed.
This script recognizes a few standard tests and will download the appropriate data sets.  It is best to execute
first outside the job to prepare the data sets (hipmer_setup_*_data.sh), then run again within a job with the
proper environment (i.e. within one of the sbatch scripts) which translates the job environmental variables into
something that the generic hipmer can use to initiate upcrun with the proper options.

The recognized tests are:

test_hipmer.sh validation
  A small validation run. It runs in 10s of seconds on a single note, and includes checking of the
  assembly at the end to determine if it is correct.

test_hipmer.sh ecoli
  The ecoli dataset. Small enough to run on a single node and should complete in a few
  minutes. Checking of the results is also performed, although the assembly result could differ
  slightly from the default due to the non-deterministic nature of the algorithms.

test_hipmer.sh chr14
  The human chromosome 14. This may be too large to run on a single node, unless it has at least 128G of 
  memory. It will take tens of minutes.

test_hipmer.sh human
  The full human dataset. This is likely too large to run on a single node, unless it has at least 1TB of 
  memory. It will take several hours on a large-memory single node.

test_hipmer.sh mg250
  A simple test for assembling metagenomes using the metagenome pipeline. This will run on a single
  node and should take under 10 minutes. This script can also be used for running other, larger
  metagenome data sets within the same data directory by setting the environment variable
  HIPMER_TEST to the chosen config file.

The data for these can be setup using the scripts test/hipmer/hipmer_setup_*_data.sh.

Often the easiest way to set up a new run will be to simply use a modified version of one of the
test_*.sh scripts.

--------
## Rerunning stages

By default, the hipmer script will store checkpoints into the $RUNDIR/intermediates folder and a run can be
resumed from any stage listed by the --list-stages option.  If a run failed to complete because of
time or memory issues, it can be restarted and by default it will resume from the most recent checkpoint. 
This can be altered by the --resume=False option (to start from the beginning again) or the --restart-stage option
to restart from a specific stage (a developer option)

Once a run has successfully completed, it is safe to purge the intermediates folder.

--------
## Saving memory

Memory should be scalable in almost all stages, so if one runs out of memory in the job, executing with more nodes is likely to solve the problem.

At the cost of a LOT of extra I/O one can execute with --cached-io=False.  This will cause the RAM disk for intermediate files (/dev/shm) to not be used and force  a tremendous amount of extra files in the per_thread directory tree and, additionally, the fastq files will be read many times from the disk throughout the job.  This option will likely degrade performance at scale.

At the cost of less I/O but no ability to restore from a previous stage upon failure, the --benchmark option will disable intermediate checkpointing, and improve performance modestly.

If you get a SIGBUS error, this is almost certainly caused by insufficient memory. This is most likely to happen if you are using upc compiled with posix shared memory (the default). On Linux, posix shared memory is backed by /run/shm, which by default is limited to a maximum of 50% of the total memory. If this is a problem, you can increase the memory available to /run/shm by adding the following line to /etc/fstab

```
  none  /run/shm  tmpfs  nosuid,nodev,size=80%  0 0
```

and then remounting the virtual file system:

```
mount -o remount /run/shm
```

--------
## Diagnostics and comparing outputs

The pipeline produces a file called diags.log that records a number of results from each stage as
key-value pairs. Multiple runs can be compared with the script compare_diags.py, included in the bin
install directory. The script compares each key-value pair for name mismatches and checks to see if
the value lies within a specified threshold, and reports keys that don't. For checking, diagnostics
outputs are provided for the standard tests with chr14 and the small metagenome. Those files are in
test/pipeline and are called chr14.diags and metagenome.diags, respectively.

--------
## Workflow

The HipMer workflow is controlled within the configuration file, when the 
libraries are specified. For each library, you can specify what round of oNo to 
use it in, and you can specify whether or not to use it for splinting. The 
workflow is as follows (see hipmer.py for details):

1. (prepare input fastq files)
  1. They must be uncompressed
  2. They ought to be striped for efficient parallel access
  3. It is best if they are trimmed of contaminates and adaptors (trim_fastq.sh).
  4. Paired reads must either be interleaved into 1 file, or separated into two files but FIXED
     length. Use interleave_fastq if you have variable length paired files.
2. prepare command line options
3. submit your job and execute hipmer <options>

The stages of HipMer are roughly:
1. kcount
2. contigs
3. contigMerDepth
4. if diploid:
    1. contigEndAnalyzer
    2. bubbleFinder
5. (optional) canonical_assembly (of contigs): canonical_contigs.fa
6. merAligner (mapping)
7. scaffolding (cgraph or ono + gapclosing)
8. canonical_assembly (of scaffolds): final_assembly.fa

Cgraph is several stages of processing mapped reads and traversing the graph

Ono is generally:
1. for each library:
    1. merAligner
    2. splinter (if specified in config file)
2. for each oNoSetID:
    1. for each library in that oNoSetId:
        1. merAlignerAnalyzer (histogrammer)
        2. spanner
    2. bmaToLinks
    3. parCC
    4. oNo


This means that the first round of bmaToLinks could end up processing the 
outputs from multiple iterations of splinter plus multiple ones of spanner. The 
subsequent calls to bmaToLinks will only process outputs from spanner.
