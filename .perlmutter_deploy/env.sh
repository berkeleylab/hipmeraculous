#!/bin/bash

module rm PrgEnv-cray
module rm PrgEnv-gnu
module rm PrgEnv-nvhpc
module rm PrgEnv-nvidia
module load PrgEnv-gnu
module swap craype/2.7.19
module rm darshan
module load cmake
module load python

# use the new GASNet-EX bupc and upc++ -- use specific versions so the build does not fail when the default updates
module use /global/common/software/m2878/perlmutter/modulefiles
module rm upcxx
module rm bupc-narrow
module rm upcxx-bupc-narrow
#module load upcxx-bupc-narrow
module load bupc-narrow/2022.10.0
module load upcxx/2022.9.0 

module list

export HIPMER_ENV=perlmutter
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
#export HIPMER_BUILD_OPTS=""
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=2000}
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}
export USE_SBCAST=1
export HIPMER_NO_AVX512F=0
export HIPMER_BUILD_TEST=1
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export MIN_NODES_FOR_HUGEPAGE_CHECK=${MIN_NODES_FOR_HUGEPAGE_CHECK:=2}
export MPICH_GNI_MALLOC_FALLBACK=enabled
export HUGETLB_MORECORE=no

# files to copy to HIPMER_INSTALL (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS=""
# copy to HIPMER_INSTALL and use as upcrun -conf=
#export HIPMER_UPCRUN_CONF=upcrun.conf
export MPICH_GNI_NDREG_ENTRIES=1024

