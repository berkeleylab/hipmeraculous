#!/bin/bash

set -e
set -x 

module rm PrgEnv-intel
module load PrgEnv-gnu
module swap gcc/8.2.0
module load esslurm
module load jgi/exvivo-openmpi/3.1.4

inst=/usr/common/jgi/exvivo-upc/gnu-openmpi-3.1.4

mkdir -p $inst
export PATH=$inst/bin:$PATH

cd ~/workspace/hipmeraculous
CC=gcc CXX=g++ contrib/install_upc.sh mpi posix $inst


#SCRATCH=$BSCRATCH CC=icc CXX=icpc HIPMER_ENV_SCRIPT=.generic_deploy/env.sh ./bootstrap_hipmer_env.sh install
#SCRATCH=$BSCRATCH CC=icc CXX=icpc HIPMER_ENV_SCRIPT=.generic_deploy/env-debug.sh ./bootstrap_hipmer_env.sh install

#SCRATCH=$BSCRATCH PATH=$BSCRATCH/hipmer-install-Linux-debug/bin:$PATH test_hipmer.sh validation ecoi mg250
#SCRATCH=$BSCRATCH PATH=$BSCRATCH/hipmer-install-Linux/bin:$PATH       test_hipmer.sh validation ecoi mg250

