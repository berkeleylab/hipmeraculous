#!/bin/bash

module load PrgEnv-intel
module load esslurm
module load gcc

# build bupc and upcxx
# module load gcc
# CC=icc CXX=icpc ./contrib/install_upc.sh smp posix /global/common/software/m2865/exvivo-bupc-upcxx-2019.4-intel /dev/shm
export PATH=/global/common/software/m2865/exvivo-bupc-upcxx-2019.4-intel/bin:$PATH

export CC=$(which icc)
export CXX=$(which icpc)

export HIPMER_ENV=exvivo-intel
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=32G}
