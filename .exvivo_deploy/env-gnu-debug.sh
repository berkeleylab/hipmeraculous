#!/bin/bash

module rm PrgEnv-intel
module load PrgEnv-gnu
module swap gcc/8.2.0
module load esslurm

export PATH=/global/common/software/m2865/exvivo-bupc-upcxx-2019.4/bin:$PATH

export CC=$(which gcc)
export CXX=$(which g++)

export HIPMER_ENV=exvivo-gnu-debug
export HIPMER_BUILD_TYPE="Debug"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=1}

export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=32G}

export HIPMER_MAX_KMER_SIZE=128
export AUTORESTART=${AUTORESTART:=0}
export GASNET_BACKTRACE=1

