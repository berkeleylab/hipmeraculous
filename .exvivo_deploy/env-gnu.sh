#!/bin/bash

module rm PrgEnv-intel
module load PrgEnv-gnu
module swap gcc/10.1.0
module load esslurm

# install bupc & upcxx
# module load gcc
# CC=gcc CXX=g++ ./contrib/install_upc.sh smp posix /global/common/software/m2865/exvivo-bupc-upcxx-2019.4 /dev/shm

# NOTE to build, use gcc 7 to build clang first!!
# CC=$(which gcc) CXX=$(which g++) ./contrib/install_upc.sh ibv posix /global/common/software/m2865/exvivo-bupc-upcxx-2020.03-ibv
# CC=$(which gcc) CXX=$(which g++) ./contrib/install_upc.sh ibv posix /global/common/software/m2865/exvivo-bupc-upcxx-2021.03-ibv
export PATH=/global/common/software/m2865/exvivo-bupc-upcxx-2021.03-ibv/bin:$PATH
export UPCXX_NETWORK=ibv

export CC=$(which gcc)
export CXX=$(which g++)

export HIPMER_ENV=exvivo-gnu
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=32000M}
export GASNET_PHYSMEM_PROBE=NO
