#!/bin/bash 

module load esslurm
module load gcc

# build bupc and upcxx
# module load gcc
# CC=icc CXX=icpc ./contrib/install_upc.sh smp posix /global/common/software/m2865/exvivo-bupc-upcxx-2019.4-intel /dev/shm
export PATH=/global/common/software/m2865/exvivo-bupc-upcxx-2019.4-intel/bin:$PATH

export CC=$(which icc)
export CXX=$(which icpc)

export HIPMER_ENV=exvivo-intel-debug
export HIPMER_BUILD_TYPE="Debug"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}
export HIPMER_NO_UNIT_TESTS=${HIPMER_NO_UNIT_TESTS:=0}

export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=32G}

export AUTORESTART=0
export GASNET_BACKTRACE=1

