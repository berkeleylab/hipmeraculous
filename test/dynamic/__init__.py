"""
PyCTest driver functions
"""

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import os
import sys
import pyctest.pyctest as pyct

__all__ = ["example_tests", "benchmark_tests"]


def example_tests(*args, **kwargs):
    """
    Create the example tests
    """
    pyct.test("example-test", [sys.executable, "--version"],
              {"WORKING_DIRECTORY": pyct.BINARY_DIRECTORY})


def benchmark_tests(*args, **kwargs):
    """
    Benchmark tests
    """
    pyct.test("benchmark-hostname", ["hostname"])
    pyct.test("benchmark-sleep", ["sleep", "5"])
