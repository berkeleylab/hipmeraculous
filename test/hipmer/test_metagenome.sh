#!/bin/bash


if [ -z "$HIPMER_INSTALL" ]; then
    export HIPMER_INSTALL=$(dirname `dirname $0`)
fi

if [ -z "$HIPMER_TEST" ]; then
    # for checking output with standard 2.5% test case
    export HIPMER_POSTRUN="compare_diags.py ${HIPMER_INSTALL}/etc/meraculous/pipeline/metagenome.diags diags.log"
fi

CANONICALIZE=1 \
HIPMER_DATA_DIR=$SCRATCH/hipmer_metagenome_data \
HIPMER_TEST=${HIPMER_TEST:=metagenome} ${HIPMER_INSTALL}/bin/test_hipmer.sh

