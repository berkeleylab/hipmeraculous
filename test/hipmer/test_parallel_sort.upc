#include <stdio.h>
#include <stdlib.h>

#include <upc.h>
#include "Buffer.h"

#define UPC_PARALLEL_SORT_TYPE int32_t
#include "upc_parallel_sort.upc"

//#undef UPC_PARALLEL_SORT_TYPE
//#define UPC_PARALLEL_SORT_TYPE int64_t
//#include "upc_parallel_sort.upc"



int cmp_int32_t(const void *_a, const void *_b){
    int32_t *a = (int32_t*)_a, *b = (int32_t*)_b;
    return ((*a < *b) ? -1 : (*a==*b) ? 0 : 1);
}
int revcmp_int64_t(const void *_a, const void *_b){
    int64_t *a = (int64_t*)_a, *b = (int64_t*)_b;
    return ((*a > *b) ? -1 : (*a==*b) ? 0 : 1);
}

#define NUM 10
int main(int argc, char **argv)
{
INIT_STATIC_VARS;
SET_HIPMER_VERBOSITY(LL_DBG2);


    Buffer buf = initBuffer(1024);
    for(int i = 0; i < NUM; i++) {
        int32_t *ptr = (int32_t*) allocFromBuffer(buf, sizeof(int32_t));
        *ptr = (THREADS-MYTHREAD) * (NUM) - i; // worst case initial ordering
    }

    int nentries;
    int32_t *ptr;

    nentries = getLengthBuffer(buf)/sizeof(int32_t);
    printf("Th%d: Have %d entries\n", MYTHREAD, nentries); 
    fflush(stdout);
    upc_barrier;

    ptr = (int32_t*) getStartBuffer(buf);
    for(int i = 0; i < nentries; i++) {
        printf("Th%d: before %d: %d\n", MYTHREAD, i, ptr[i]);
    }
    fflush(stdout);
    upc_barrier;

    int32_t_parallel_sort(buf, cmp_int32_t);

    fflush(stdout);
    upc_barrier;

    nentries = getLengthBuffer(buf)/sizeof(int32_t);
    printf("Th%d: Got %d entries\n", MYTHREAD, nentries); 
    fflush(stdout);
    upc_barrier;

    upc_barrier;
    ptr = (int32_t*) getStartBuffer(buf);
    for(int i = 0; i < nentries; i++) {
        printf("Th%d: after %d: %d\n", MYTHREAD, i, ptr[i]);
    }
    fflush(stdout);
    upc_barrier;

    SLOG("Success\n");

    return 0;
}

