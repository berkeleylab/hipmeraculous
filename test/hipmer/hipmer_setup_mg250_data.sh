#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_MG250_DATA=${HIPMER_MG250_DATA:=${SCRATCH}/hipmer_metagenome-250_data}
HIPMER_MG250_DATA_URL=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/hipmer_metagenome-250.tar.gz

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_MG250_DATA=$HIPMER_MG250_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with mg250 data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi


if [ ! -d ${HIPMER_MG250_DATA} ] || rmdir ${HIPMER_MG250_DATA} 2>/dev/null # i.e. it does not exist or is empty
then
    echo "Downloading mg250 fastq from the NERSC HPSS system... this could take a while"
    if [  -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download the data within a batch job. run 'mkdir -p ${HIPMER_MG250_DATA}; lfs setstripe -c 72 ${HIPMER_MG250_DATA} 2>/dev/null || true ; cd ${HIPMER_MG250_DATA} ; curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_MG250_DATA_URL | tar -xzf' offline"
      exit 1
    fi
    mkdir -p ${HIPMER_MG250_DATA}.tmp
    lfs setstripe -c 72 ${HIPMER_MG250_DATA}.tmp 2>/dev/null || true
    cd ${HIPMER_MG250_DATA}.tmp
    for i in $(seq 0 16)
    do
      curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_MG250_DATA_URL | tar -xzf - \
        && [ "${PIPESTATUS[*]}" == "0 0" ] && break || rm -rf hipmer_metagenome-250_data
      echo "Download failed, trying again in 30 min"; sleep 1800
    done
    cd -
    [ -f ${HIPMER_MG250_DATA}.tmp/hipmer_metagenome-250_data/mock2pct.250.bbqc.sampled.fastq.merged.fastq ] && [ -f ${HIPMER_MG250_DATA}.tmp/hipmer_metagenome-250_data/mock2pct.250.bbqc.sampled.fastq.unmerged.fastq ] && \
    mv ${HIPMER_MG250_DATA}.tmp/hipmer_metagenome-250_data ${HIPMER_MG250_DATA} && \
    rmdir ${HIPMER_MG250_DATA}.tmp
    echo "Done downloading mg250 dataset"
fi

echo "Linking metagenome-250 set to ${RUNDIR}"
mkdir -p ${RUNDIR}
for i in ${HIPMER_MG250_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR/ 2>/dev/null || ln -fs $i $RUNDIR/ ; done

cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/mg250* ${RUNDIR}/
cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*.hmm ${RUNDIR}/


echo "Done setting up mg250 datasets"

