#include <stdint.h>
#include <upc.h>

typedef struct _MyData {
    int32_t val;
} MyData;

#define UPC_LIST_NAME mydata
#define UPC_LIST_TYPE MyData

#include "upc_list_generic.upc"

int main(int argc, char **argv)
{
    INIT_STATIC_VARS;

    LOG("Starting\n");
    LIST_T_HEAD_ALL_SHPTR heads = NULL;

    UPC_ALL_ALLOC_CHK(heads, THREADS, sizeof(LIST_T_HEAD));
    heads[MYTHREAD] = NULL;

    int long varsPerThread = 5000;
    LIST_T_ALL_SHPTR heaps = NULL;
    UPC_ALL_ALLOC_CHK(heaps, THREADS * varsPerThread, sizeof(LIST_T));
    for (long i = 0; i < varsPerThread; i++) {
        heaps[MYTHREAD + i * THREADS].data.val = MYTHREAD + i * THREADS;
        heaps[MYTHREAD + i * THREADS].next = NULL;
    }
    upc_barrier;

    // create a list per thread over the heap entries
    // the order indetermined, but set is fixed as a chain that is diagonally across threads in the heaps matrix

    if (heads[MYTHREAD] != NULL) {
        DIE("error initializing\n");
    }

    upc_barrier;

    for (long i = 0; i < varsPerThread; i++) {
        long idx = MYTHREAD + i * THREADS;
        LIST_T_SHPTR next = (LIST_T_SHPTR)&heaps[idx];
        if (upc_threadof(next) != MYTHREAD) {
            DIE("invalid assumption\n");
        }
        int headIdx = (MYTHREAD + i) % THREADS; // rotate heads forward so lists will be forwards diagonal: ////
        LIST_T_HEAD_SHPTR targetHead = (LIST_T_HEAD_SHPTR)&heads[headIdx];
        PUSH_NODE_FRONT(targetHead, next);
    }

    upc_barrier;
    // now verify the correct list_t entries got into the correct list
    LIST_T_HEAD_SHPTR myHead = (LIST_T_HEAD_SHPTR)&heads[MYTHREAD];
    LIST_T_SHPTR node = NULL;
    LIST_T_SHPTR last = *myHead;
    int count = 0;
    do {
        UPC_ATOMIC_GET_SHPTR(&node, myHead);
        if (last != node) {
            DIE("last != node!\n");
        }
        if (node == NULL) {
            break;
        }
        count++;
        LIST_T copyNode = *node;
        // val should be a number from this formula: ((MYTHREAD-i)%THREADS+THREADS)%THREADS + i*THREADS
        long i = copyNode.data.val / THREADS;
        long exp = ((MYTHREAD - i) % THREADS + THREADS) % THREADS + i * THREADS;
        if (copyNode.data.val != exp) {
            DIE("Incorrect memember of my list (%d).  val=%lld i=%lld exp=%lld\n", MYTHREAD, (lld)copyNode.data.val, (lld)i, (lld)exp);
        }
        last = copyNode.next;
        myHead = &(node->next);
    } while (1);
    if (count != varsPerThread) {
        DIE("Got %d in my list not %d!\n", count, varsPerThread);
    }

    upc_barrier;

    // now pop each list, rotating the heads across threads

    for (long i = 0; i < varsPerThread; i++) {
        long idx = MYTHREAD + i * THREADS;
        LIST_T_SHPTR next = (LIST_T_SHPTR)&heaps[idx];
        if (upc_threadof(next) != MYTHREAD) {
            DIE("invalid assumption\n");
        }
        int headIdx = (MYTHREAD + i) % THREADS; // rotate heads forward so lists will be forwards diagonal: ////
        LIST_T_HEAD_SHPTR targetHead = (LIST_T_HEAD_SHPTR)&heads[headIdx];
        node = POP_NODE_FRONT(targetHead);
        if (node == NULL) {
            DIE("Pops should be evenly distributed so none should return NULL");
        }
        LIST_T copyNode = *node;
        // val should be a number from this formula: ((headIdx-i)%THREADS+THREADS)%THREADS + i*THREADS
        long i = copyNode.data.val / THREADS;
        long exp = ((headIdx - i) % THREADS + THREADS) % THREADS + i * THREADS;
        if (copyNode.data.val != exp) {
            DIE("Incorrect memember of my list (%d).  val=%lld i=%lld exp=%lld\n", MYTHREAD, (lld)copyNode.data.val, (lld)i, (lld)exp);
        }
    }

    upc_barrier;

    if (POP_NODE_FRONT( (LIST_T_HEAD_SHPTR) &heads[MYTHREAD] ) != NULL) {
        DIE("There should be no more nodes in my list after barrier\n");
    }
    if (heads[MYTHREAD] != NULL) {
        DIE("Myhead should be NULL now!\n");
    }

    upc_barrier;


    // now reset and try again changing the list member not the head
    heads[MYTHREAD] = NULL;
    for (long i = 0; i < varsPerThread; i++) {
        heaps[MYTHREAD + i * THREADS].data.val = MYTHREAD + i * THREADS;
        heaps[MYTHREAD + i * THREADS].next = NULL;
    }
    upc_barrier;

    if (heads[MYTHREAD] != NULL) {
        DIE("error initializing\n");
    }
    for (long i = 0; i < varsPerThread; i++) {
        long idx = (MYTHREAD + i) % THREADS + i * THREADS; // list will be backwards diagonal
        LIST_T_SHPTR next = (LIST_T_SHPTR)&heaps[idx];
        int headIdx = MYTHREAD; // always my on thread
        LIST_T_HEAD_SHPTR targetHead = (LIST_T_HEAD_SHPTR)&heads[headIdx];
        PUSH_NODE_FRONT(targetHead, next);
    }

    upc_barrier;
    // now verify the correct list_t entries got into the correct list
    count = 0;
    myHead = (LIST_T_HEAD_SHPTR)&heads[MYTHREAD];
    last = *myHead;
    node = NULL;
    do {
        UPC_ATOMIC_GET_SHPTR(&node, myHead);
        if (last != node) {
            DIE("last != node!\n");
        }
        if (node == NULL) {
            break;
        }
        count++;
        LIST_T tmp = *node;
        // val should be a number from this formula: (MYTHREAD+i)%THREADS + i*THREADS
        long i = tmp.data.val / THREADS;
        long exp = (MYTHREAD + i) % THREADS + i * THREADS;
        if (tmp.data.val != exp) {
            DIE("Incorrect memember of my list (%d).  data.val=%lld i=%lld exp=%lld\n", MYTHREAD, (lld)tmp.data.val, (lld)i, (lld)exp);
        }
        last = tmp.next;
        myHead = &(node->next);
    } while (1);
    if (count != varsPerThread) {
        DIE("Got %d in my list not %d!\n", count, varsPerThread);
    }

    upc_barrier;

    // now pop each list, from my head

    for (long i = 0; i < varsPerThread; i++) {
        long idx = MYTHREAD + i * THREADS;
        LIST_T_SHPTR next = (LIST_T_SHPTR)&heaps[idx];
        if (upc_threadof(next) != MYTHREAD) {
            DIE("invalid assumption\n");
        }
        int headIdx = MYTHREAD;
        LIST_T_HEAD_SHPTR targetHead = (LIST_T_HEAD_SHPTR)&heads[headIdx];
        node = POP_NODE_FRONT(targetHead);
        if (node == NULL) {
            DIE("Pops should be evenly distributed so none should return NULL");
        }
        LIST_T copyNode = *node;
        // val should be a number from this formula: (MYTHREAD+i)%THREADS + i*THREADS
        long i = copyNode.data.val / THREADS;
        long exp = (MYTHREAD + i) % THREADS + i * THREADS;
        if (copyNode.data.val != exp) {
            DIE("Incorrect memember of my list (%d).  val=%lld i=%lld exp=%lld\n", MYTHREAD, (lld)copyNode.data.val, (lld)i, (lld)exp);
        }
    }

    if (POP_NODE_FRONT( (LIST_T_HEAD_SHPTR) &heads[MYTHREAD] ) != NULL) {
        DIE("There should be no more nodes in my list after barrier\n");
    }
    if ( heads[MYTHREAD] != NULL ) {
        DIE("my head should be NULL now!\n");
    }

    upc_barrier;

    // now lets leave the memory unclean but only have a single head handle all the contention
    LIST_T_HEAD_SHPTR singleHead = (LIST_T_HEAD_SHPTR)&heads[THREADS-1];
    for(long i = 0; i < varsPerThread; i++) {
        long idx = MYTHREAD + i * THREADS;
        LIST_T_SHPTR next = (LIST_T_SHPTR)&heaps[idx];
        if (upc_threadof(next) != MYTHREAD) {
            DIE("invalid assumption\n");
        }
        PUSH_NODE_FRONT(singleHead, next);
    }

    upc_barrier;

    for(long i = 0; i < varsPerThread; i++) {
        long idx = MYTHREAD + i * THREADS;
        LIST_T_SHPTR node = POP_NODE_FRONT(singleHead);
        if (node == NULL) {
            DIE("should not return NULL\n");
        }
    }

    upc_barrier;

    if (POP_NODE_FRONT(singleHead) != NULL) {
        DIE("No remaining should exist!\n");
    }
    if ( *singleHead != NULL ) {
        DIE("Head should be null now!\n");
    }
    upc_barrier;

    // now lets free the node array and use individually allocated nodes
    UPC_ALL_FREE_CHK(heaps);

    for(long i = 0; i < varsPerThread; i++) {
        int val = MYTHREAD + i*THREADS;
        MyData mydata;
        mydata.val = val;
        node = ALLOC_NODE(mydata);
        int headIdx = (MYTHREAD + i) % THREADS; // rotate heads forward so lists will be forwards diagonal: ////
        LIST_T_HEAD_SHPTR targetHead = (LIST_T_HEAD_SHPTR)&heads[headIdx];
        PUSH_NODE_FRONT(targetHead, node);
    }
    upc_barrier;

    // now verify the correct list_t entries got into the correct list
    myHead = (LIST_T_HEAD_SHPTR)&heads[MYTHREAD];
    node = NULL;
    count = 0;
    do {
        UPC_ATOMIC_GET_SHPTR(&node, myHead);
        if (node == NULL) {
            break;
        }
        count++;
        LIST_T copyNode = *node;
        // val should be a number from this formula: ((MYTHREAD-i)%THREADS+THREADS)%THREADS + i*THREADS
        long i = copyNode.data.val / THREADS;
        long exp = ((MYTHREAD - i) % THREADS + THREADS) % THREADS + i * THREADS;
        if (copyNode.data.val != exp) {
            DIE("Incorrect memember of my list (%d).  val=%lld i=%lld exp=%lld\n", MYTHREAD, (lld)copyNode.data.val, (lld)i, (lld)exp);
        }
        myHead = &(node->next);
    } while (1);
    if (count != varsPerThread) {
        DIE("Got %d in my list not %d!\n", count, varsPerThread);
    }

    upc_barrier;
    myHead = (LIST_T_HEAD_SHPTR)&heads[MYTHREAD];
    if ( (count = FREE_LIST(myHead)) != varsPerThread) {
        DIE("The list should have %d entries, got %d!\n", varsPerThread, count);
    }

    MyData x;
    x.val=1;
    PUSH_NODE_FRONT(myHead, ALLOC_NODE(x));
    PUSH_NODE_FRONT(myHead, ALLOC_NODE(x));
    if (FREE_LIST(myHead) != 2) {
        DIE("The list should have 2 entries!\n");
    }
    if (FREE_LIST(myHead) != 0) {
        DIE("The list should now have 0 entries\n");
    }

    upc_barrier;
    UPC_ALL_FREE_CHK(heads);

    LOG_STATS;
    SLOG("Success\n");

    return 0;
}
