#!/bin/bash

set -e
set -x

for i in $HIPMER_INSTALL/bin/main-*[0-9]
do
  pat_build $i
  mv -f $i $i-orig
  mv ${i##*/}+pat $i
  ln -f $i $i+pat
done
