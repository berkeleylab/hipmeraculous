#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_HUMAN_DATA=${HIPMER_HUMAN_DATA:=${SCRATCH}/hipmer_human_data}
HIPMER_HUMAN_DATA_URL_BASE=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_HUMAN_DATA=$HIPMER_HUMAN_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with human benchmark data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi

if [ ! -d ${HIPMER_HUMAN_DATA} ] || rmdir ${HIPMER_HUMAN_DATA} 2>/dev/null # i.e. it does not exist or is empty
then
    echo "Downloading human fastq from the NERSC HPSS system... this could take a while"
    if [ -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download this data in a batch job.  Do it offline: "
      echo "   $(which $0) $@"
      exit 1
    fi
    mkdir -p ${HIPMER_HUMAN_DATA}.tmp
elif [ ! -f ${HIPMER_HUMAN_DATA}/s_1_1_sequence.fastq ] || [ ! -f ${HIPMER_HUMAN_DATA}/s_1_2_sequence.fastq ]
then
    if [ -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download this data in a batch job.  Do it offline: "
      echo "   $(which $0) $@"
      exit 1
    fi
    echo "Downloading incomplete human data"
    mv ${HIPMER_HUMAN_DATA} ${HIPMER_HUMAN_DATA}.tmp 
fi

if [ -d ${HIPMER_HUMAN_DATA}.tmp ]
then
    lfs setstripe -c 72 ${HIPMER_HUMAN_DATA}.tmp 2>/dev/null || true
    cd ${HIPMER_HUMAN_DATA}.tmp
    ( set -e;
      set -x;
      set -o pipefail;
      for f in s_1_1_sequence.fastq.gz s_1_2_sequence.fastq.gz fos1.fastq.gz fos2.fastq.gz jmp1.fastq.gz jmp2.fastq.gz
      do
          u=$HIPMER_HUMAN_DATA_URL_BASE/$f
          if [ ! -f "${f%.gz}" ]
          then
            echo "Downloading $u at $(date)"
            for i in $(seq 0 16)
            do
              curl --keepalive-time 30 --silent --show-error --max-time 36000 --retry 3 --retry-delay 120 -o - $HIPMER_HUMAN_DATA_URL_BASE/$f | gunzip -c > ${f}.tmp \
                && [ "${PIPESTATUS[*]}" == "0 0" ] && break || rm ${f}.tmp
              echo "Download failed. retrying in 30 minutes." ; sleep 1800
              echo "Trying again at $(date)" 
            done
            mv ${f}.tmp ${f%.gz}
          fi
      done && \
      cd .. && \
      mv ${HIPMER_HUMAN_DATA}.tmp ${HIPMER_HUMAN_DATA}
    ) || exit 1
    [ -f ${HIPMER_HUMAN_DATA}/s_1_1_sequence.fastq ] && [ -f ${HIPMER_HUMAN_DATA}/s_1_2_sequence.fastq ]
fi

mkdir -p $RUNDIR
echo "Linking human data set into ${RUNDIR}"
for i in ${HIPMER_HUMAN_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR/ 2>/dev/null || ln -fs $i $RUNDIR/ ; done
cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*human*.config $RUNDIR 

echo "Done setting up human dataset"




