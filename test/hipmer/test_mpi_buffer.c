#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>

#include <mpi.h>

#define CHECK( cmd ) do { int status = cmd; if (status != MPI_SUCCESS) { char errstr[255]; int errlen; MPI_Error_string(status, errstr, &errlen);  fprintf(stderr, "Failed to perform %s. Error code %d %.*s\n", #cmd, status, errlen, errstr);  MPI_Abort(MPI_COMM_WORLD, 1); } } while (0)

int main(int argc, char **argv) {
        MPI_Init(&argc, &argv);

        int rank,size;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm comms[40];
        int comm_sizes[40];
	int comm_groups[40];
        int num_comms = 0;
        int tmp = size;
	if (!rank) printf("Allocating 384 MB of ram\n");
	char *dummy = calloc(384*1024*1024,1);
	CHECK(MPI_Barrier(MPI_COMM_WORLD));

        while (num_comms < 40 && tmp >= 1) {
		int comm_size = size / tmp;
		int comm_group = rank % tmp;
		comm_groups[num_comms] = comm_group;
                comm_sizes[num_comms] = comm_size;
		if (!rank) printf("Creating comm %d with %d size\n", num_comms, comm_size);
                CHECK( MPI_Comm_split(MPI_COMM_WORLD, (comm_group == 0 ? 0 : MPI_UNDEFINED), 0, &comms[num_comms]) );
		int checkSize;
		if (comms[num_comms] != MPI_COMM_NULL) { 
			assert(comm_group == 0);
			int checkSize;
			CHECK( MPI_Comm_size(comms[num_comms], &checkSize) );
			if (checkSize != comm_size) { fprintf(stderr, "wrong size logic %d vs %d!\n", checkSize, comm_size); }
		}
                tmp /= 2;
		num_comms++;
        }
	if (!rank) printf("Split COMM into %d sub communicators\n", num_comms);
        uint16_t *buf = NULL;
        int64_t bufsize = 64*1024;
	int64_t i, commi;
	int32_t *sendcnt, *sdispls, *recvcnt, *rdispls;
	sendcnt = calloc(size, sizeof(int32_t));
	sdispls = calloc(size, sizeof(int32_t));
	recvcnt = calloc(size, sizeof(int32_t));
	rdispls = calloc(size, sizeof(int32_t));
	
	CHECK(MPI_Barrier(MPI_COMM_WORLD));
        while (bufsize/sizeof(int16_t) <= (1LL<<32)) {
                buf = realloc(buf, bufsize + 2);
		int64_t bufcount = bufsize / sizeof(int16_t) - 1;
		//int64_t bufcount = bufsize / 2 / sizeof(int16_t) - 1;
                if (buf == NULL) break;
                for(i = 0; i < bufcount; i++) buf[i] = (i+rank)%(1<<16);
                for(commi = 0; commi < num_comms; commi++) {
			if (comm_groups[commi] == 0) {
       				if (!rank) { printf("Starting bufsize %ld, comm: %d comm_size: %d, count: %ld", bufsize, commi, comm_sizes[commi], bufcount); fflush(stdout); }
				int thissize = comm_sizes[commi];
				int block = bufcount / thissize;
				for(i = 0; i < thissize; i++) {
					sendcnt[i] = block;
					sdispls[i] = i * block;
					recvcnt[i] = block;
					rdispls[i] = i * block;
				}
				assert(sendcnt[thissize-1] + sdispls[thissize-1] <= bufcount);
				double t = MPI_Wtime();
				// int status = MPI_Reduce(rank==0 ? MPI_IN_PLACE : buf, rank == 0 ? buf : NULL, bufcount, MPI_SHORT, MPI_MAX, 0, comms[commi]);
				int status = MPI_Allreduce(MPI_IN_PLACE, buf, bufcount, MPI_SHORT, MPI_MAX, comms[commi]);
				//int status = MPI_Alltoallv(buf, sendcnt, sdispls, MPI_SHORT, buf + bufcount+1, recvcnt, rdispls, MPI_SHORT, MPI_COMM_WORLD);
				if (status != MPI_SUCCESS) { fprintf(stderr, "Failed on commi %d: size %ld, comm_size %d, count: %ld\n", commi, bufsize, comm_sizes[commi], bufcount); }
				if (!rank) {
					t = MPI_Wtime() - t;
					printf(" %0.6f s %0.3f MB/s (total %0.3f MB/s)\n", t, ((double) bufcount) / t / 1048576.0, ((double) bufcount) * comm_sizes[commi] / t / 1048576.0);
					fflush(stdout);
				}
			}
			CHECK(MPI_Barrier(MPI_COMM_WORLD));
                }
		bufsize *= 2;
        }


        MPI_Finalize();
        return 0;
}

