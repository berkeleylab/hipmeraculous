#include <stdio.h>
#include <stdlib.h>

#include "upc_atomic_data.h"

typedef struct {
    long dummy1, dummy2, dummy3;
} ExamplePayload;

INIT_ATOMIC_DATA(examplePayload, ExamplePayload)
ADD_ATOMIC_TYPED_DEFS(examplePayload, ExamplePayload)


static void testIfItCompileAndWorks() {
    ExamplePayload ep = { 1, 2, 3};
    examplePayload_AtomicData ad, ad2, emptyAD;
    examplePayload_SharedAtomicDataPtr sh = NULL;

    UPC_ALLOC_CHK(sh, sizeof(examplePayload_AtomicData));

    examplePayload_initAtomicData(&emptyAD);
    examplePayload_setAtomicData(&ad, &ep, 1, __FILENAME__, __LINE__);

    SLOG("sizeof(CheckSizeVersion)=%lld sizeof(AtomicDataChecksum)=%lld sizeof(examplePayload_AtomicData)=%lld\n", (lld) sizeof(CheckSizeVersion), sizeof(AtomicDataChecksum), (lld) sizeof(examplePayload_AtomicData));
    AtomicDataChecksum zero;
    zero.val = 0;
    if (zero.val == emptyAD.checksum.val) {
        DIE("An initialized AD should not have a checksum of 0: payloadSize=%lld sizeof(CheckSizeVersion)=%lld sizeof(AtomicDataChecksum) %d:%d:1:%d\n", (lld) emptyAD.checksum.csv.payloadSize, (lld) sizeof(CheckSizeVersion), sizeof(AtomicDataChecksum), UPCAD_HASH_BITS, UPCAD_VERSION_BITS, UPCAD_PAYLOAD_BITS);
    }

    if (!examplePayload_tryPutAtomicData(sh, &ad, zero, __FILENAME__, __LINE__)) {
        DIE("tryPut to zero should have worked\n");
    }

    // initialize the data instead
    *sh = emptyAD;
    upc_fence;

    if (!examplePayload_tryPutAtomicData_Near(sh, &ad, emptyAD.checksum, __FILENAME__, __LINE__)) {
        DIE("tryPut to empty should have worked\n");
    }

    if (examplePayload_tryGetAtomicData(&ad2, sh, __FILENAME__, __LINE__) != &ad2) {
        DIE("tryGet should have worked\n");
    }

    if (ad2.checksum.val != ad.checksum.val) {
        DIE("checksums should be different\n");
    }
    if (memcmp(&(ad.payload), &(ad2.payload), sizeof(ExamplePayload)) != 0) {
        DIE("payloads should be different\n");
    }
    ep.dummy1 = 5;
    examplePayload_setAtomicData(&ad2, &ep, 1, __FILENAME__, __LINE__);
    if (ad.checksum.csv.hash == ad2.checksum.csv.hash) {
        DIE("payload changed so hash should be different %lld vs %lld (%lld vs %lld)\n", (lld) ad.checksum.csv.hash, (lld) ad2.checksum.csv.hash, (lld) ad.checksum.val, (lld) ad2.checksum.val);
    }
    if (examplePayload_tryPutAtomicData(sh, &ad2, emptyAD.checksum, __FILENAME__, __LINE__)) {
        DIE("tryPut should fail to non-empty\n");
    }
    if (!examplePayload_tryPutAtomicData(sh, &ad2, ad.checksum, __FILENAME__, __LINE__)) {
        DIE("tryPut should work to known oldval\n");
    }
    if (examplePayload_tryGetAtomicData(&ad, sh, __FILENAME__, __LINE__) != &ad) {
        DIE("tryGet should work and return the same source ptr\n");
    }
    if (ad2.checksum.val != ad.checksum.val) {
        DIE("checksums should be different\n");
    }
    // corrupt the payload
    sh->payload.dummy3 = -1;
    upc_fence;
    if (examplePayload_tryGetAtomicData(&ad, sh, __FILENAME__, __LINE__) == &ad) {
        DIE("It should not be possible to get atomic data if the checksum does not match the payload\n");
    }
    UPC_FREE_CHK(sh);
    upc_barrier;
    SLOG("Success\n");
}

int main(int argc, char **argv) {
    testIfItCompileAndWorks();
    return 0;
}
