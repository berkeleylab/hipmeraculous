#include <upc.h>

#include "utils.h"

int main(int argc, char **argv)
{
INIT_STATIC_VARS;
SET_HIPMER_VERBOSITY(LL_DBG);

    const int times = 100;
    for (int i = 0 ; i < times; i++) {
        long myval = MYTHREAD*i, tmp, expect;
        expect = (i%THREADS) * i;
        if ( (tmp = broadcast_long(myval, i%THREADS)) != expect) DIE("Error i=%d got %lld, expected %lld\n", i, (lld) tmp, (lld) expect);
    }

    for (int i = 0; i < times; i++) {
        long myval = MYTHREAD * i, tmp, expect;
        expect = ( (long) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_long(myval, UPC_ADD, ALL_DEST)) != expect) DIE ("Error i=%d got %lld, expected %lld", i, (lld) tmp, (lld) expect);
    }
    for (int i = 0; i < times; i++) {
        long myval = MYTHREAD * i, tmp, expect;
        expect = ( (long) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_long(myval, UPC_ADD, SINGLE_DEST)) != (MYTHREAD == 0 ? expect : 0)) DIE ("Error i=%d got %lld, expected %lld", i, (lld) tmp, (lld) expect);
    }
    for (int i = 0; i < times; i++) {
        long myval = MYTHREAD * i, tmp, expect;
        expect = ( (long) (MYTHREAD+1) * (MYTHREAD) / 2 ) * i;
        if ( (tmp = reduce_prefix_long(myval, UPC_ADD, NULL)) != expect) DIE ("Error i=%d got %lld, expected %lld", i, (lld) tmp, (lld) expect);
    }

    for (int i = 0 ; i < times; i++) {
        int32_t myval = MYTHREAD*i, tmp, expect;
        expect = (i%THREADS) * i;
        if ( (tmp = broadcast_int(myval, i%THREADS)) != expect) DIE("Error i=%d got %0.3f, expected %0.3f\n", i,  tmp,  expect);
    }
    for (int i = 0; i < times; i++) {
        int32_t myval = MYTHREAD * i, tmp, expect;
        expect = ( (int32_t) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_int(myval, UPC_ADD, ALL_DEST)) != expect) DIE ("Error i=%d got %0.3f, expected %0.3f", i,  tmp,  expect);
    }
    for (int i = 0; i < times; i++) {
        int32_t myval = MYTHREAD * i, tmp, expect;
        expect = ( (int32_t) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_int(myval, UPC_ADD, SINGLE_DEST)) != (MYTHREAD == 0 ? expect : 0)) DIE ("Error i=%d got %0.3f, expected %0.3f", i,  tmp,  expect);
    }

    for (int i = 0 ; i < times; i++) {
        double myval = MYTHREAD*i, tmp, expect;
        expect = (i%THREADS) * i;
        if ( (tmp = broadcast_double(myval, i%THREADS)) != expect) DIE("Error i=%d got %0.3f, expected %0.3f\n", i,  tmp,  expect);
    }

    for (int i = 0; i < times; i++) {
        double myval = MYTHREAD * i, tmp, expect;
        expect = ( (long) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_double(myval, UPC_ADD, ALL_DEST)) != expect) DIE ("Error i=%d got %0.3f, expected %0.3f", i,  tmp,  expect);
    }
    for (int i = 0; i < times; i++) {
        double myval = MYTHREAD * i, tmp, expect;
        expect = ( (long) THREADS * (THREADS-1) / 2 ) * i;
        if ( (tmp = reduce_double(myval, UPC_ADD, SINGLE_DEST)) != (MYTHREAD == 0 ? expect : 0)) DIE ("Error i=%d got %0.3f, expected %0.3f", i,  tmp,  expect);
    }

    // check gather_long for various block sizes
    for(int i = 0; i < THREADS; i++) {
        int64_t l = i + 100;
        int64_t *gathered = gather_long(&l, 1, i);
        if (i == MYTHREAD) {
            for(int j = 0 ; j < THREADS; j++) {
               if (gathered[j] != l) DIE("Error i=%d j=%d got %lld expected %lld", i, j, (lld) gathered[j], (lld) l);
            }
            free_chk(gathered);
        } else {
            if (gathered != NULL) DIE("Got result but it was not my turn: i=%d", i);
        }

        int64_t *a = calloc_chk(5, sizeof(int64_t));
        for(int k = 0; k < 5; k++) {
            a[k] = i*100 + k;
        }
        int64_t *gathered2 = gather_long(a, 5, i);
        if (i == MYTHREAD) {
            for(int j = 0 ; j < THREADS; j++) {
                for(int k = 0; k < 5; k++) {
                    if (gathered2[j*5+k] != a[k]) DIE("Error i=%d j=%d k=%d got %lld expected %lld", i, j, k, (lld) gathered[j*5+k], (lld) a[k]);
                }
            }
            free_chk(gathered2);
        } else {
            if (gathered2 != NULL) DIE("Got result but it was not my turn: i=%d", i);
        }
        free_chk(a);
    }

    // check scatter_long for various block sizes
    for(int i = 0; i < THREADS; i++) {
        void *local = NULL, *result = NULL;
        if (i == MYTHREAD) {
            local = calloc_chk(THREADS, sizeof(int64_t)*5);
            for(int j = 0; j < THREADS; j++) {
                *(((int64_t*) local) + j) = i*100;
            }
        }
        result = scatter_long(local, 1, i);
        if (result == NULL) DIE("Did not get a result");
        if ( *((int64_t*) result) != i*100 ) DIE("Error i=%d got %lld expeced %lld", i, (lld) *((int64_t*) result), (lld) i*100);
        free_chk(result);

        if (i == MYTHREAD) {
            for(int j = 0; j < THREADS; j++) {
                for(int k = 0; k < 5; k++) {
                    *(((int64_t*) local) + j*5+k) = i*100+k;
                }
            }
        }
        result = scatter_long(local, 5, i);
        if (result == NULL) DIE("Did not get a result");
        for(int k = 0; k < 5; k++) {
            if ( *((int64_t*) result+k) != i*100+k ) DIE("Error i=%d k=%d got %lld expeced %lld", i, k, (lld) *((int64_t*) result+k), (lld) i*100+k);
        }
        free_chk(result);

        if (i == MYTHREAD) {
            free_chk(local);
        }
    }

    SLOG("Success\n");

    return 0;
}
