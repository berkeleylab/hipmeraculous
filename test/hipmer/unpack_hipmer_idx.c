#include <stdio.h>

const char *USAGE = "unpack_hipmer_idx file.idx";

int main(int argc, char **argv) {
  if (argc <= 1) { fprintf(stderr, "Proper usage:\n%s\n\n", USAGE); return 1; }
  FILE *f = fopen(argv[1], "r");
  size_t three[3]; // offset, size, uncompressedSize
  size_t thread = 0;
  while (!feof(f) && 3 == fread(three, sizeof(size_t), 3, f)) {
    printf("%ld\t%ld\t%ld\t%ld\n", thread++, three[0], three[1], three[2]);
  }
  return 0;
}

