#!/bin/bash

# set RUN_MQ=1 to run metaquast. 
# set the refs with REFS envvar
# The parameters are for different fasta files
# So running mg64 could look like:
#
# RUN_MQ=1 REFS=mg64-refs mq.sh metaspades-mg64.fa megahit-mg64.fa hipmer-mg64.fa

RUN_MQ=${RUN_MQ:=0}
QUAST_DIR=${QUAST_DIR:=quast_results/latest}

if [ "${RUN_MQ}" == "1" ]; then
    if [ ! $REFS ]; then
        echo "Must define REFS="
        exit 0
    fi
  
    echo -e "\n\e[32mRunning metaquast...\e[39m"
    metaquast.py --fast -t 80 --no-icarus --no-snps --scaffolds -R ${REFS}/ $1 $2 $3 $4 $5
fi

echo -e "\n\e[32mMisassemblies\e[39m"
cat ${QUAST_DIR}/combined_reference/contigs_reports/misassemblies_report.txt

echo -e "\n\e[32mGenome fraction\e[39m"
tail -n +4 ${QUAST_DIR}/combined_reference/transposed_report.txt| awk '{print $1, $33}'
echo -e "\n\e[32mDuplication ratio\e[39m"
tail -n +4 ${QUAST_DIR}/combined_reference/transposed_report.txt| awk '{print $1, $34}'
echo -e "\n\e[32mNs per 100kbp\e[39m"
tail -n +4 ${QUAST_DIR}/combined_reference/transposed_report.txt| awk '{print $1, $35}'
echo -e "\n\e[32mNA50\e[39m"
tail -n +4 ${QUAST_DIR}/combined_reference/transposed_report.txt| awk '{print $1, $38}'
echo -e "\n\e[32mNA75\e[39m"
tail -n +4 ${QUAST_DIR}/combined_reference/transposed_report.txt| awk '{print $1, $39}'

echo -e "\n\e[32mComputing NGA50...\e[39m"
i=1
 for x in `head -n1 ${QUAST_DIR}/summary/TXT/NGA50.txt| cut -d' ' -f 2-`; do 
    echo $x
    echo -n "NGA50: "
    egrep -v "Assemblies|-" ${QUAST_DIR}/summary/TXT/NGA50.txt |avg.py $i|grep median
    i=$((i+1))
done

run_barrnap()
{
    # check for broken assembly too
    bname=$(basename $1)
    echo $1
    barrnap --threads 24 $1 &> barrnap.$bname
    echo -n "16S: "
    grep "product=16S" barrnap.$bname |grep -v partial|wc -l
    echo -n "23S: "
    grep "product=23S" barrnap.$bname |grep -v partial|wc -l
}

run_barrnap_broken()
{
    run_barrnap $1
    bname=$(basename $1)
    broken_assm=$(echo ${QUAST_DIR}/quast_corrected_input/${bname}|cut -d'.' -f1)_broken.fa
    if [ -e $broken_assm ]; then
        run_barrnap $broken_assm
    fi
}

echo -e "\n\e[32mRunning barrnap...\e[39m"
if [ -n "$1" ]; then run_barrnap_broken $1; fi
if [ -n "$2" ]; then run_barrnap_broken $2; fi
if [ -n "$3" ]; then run_barrnap_broken $3; fi
if [ -n "$4" ]; then run_barrnap_broken $4; fi
if [ -n "$5" ]; then run_barrnap_broken $5; fi



