#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_CHR14_DATA=${HIPMER_CHR14_DATA:=${SCRATCH}/hipmer_chr14_data}
HIPMER_CHR14_DATA_URL=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/hipmer_chr14_data.tar.gz
FILES="frag_1.fastq frag_2.fastq longjump.fastq shortjump.fastq"

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_CHR14_DATA=$HIPMER_CHR14_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with human chr14 data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi


TEST_AWS=$(curl --max-time 2 -s http://169.254.169.254/latest/dynamic/instance-identity/document| grep instanceId || /bin/true)
AWS_URL=
if [ -n "${TEST_AWS}" ]
then
    AWS_URL=https://s3.amazonaws.com/hipmer-benchmarks/hipmer_chr14_data
fi

if [ ! -d ${HIPMER_CHR14_DATA} ] || [ ! -f ${HIPMER_CHR14_DATA}/frag_1.fastq ] || [ ! -f ${HIPMER_CHR14_DATA}/frag_2.fastq ] 
then
  
  mkdir -p ${HIPMER_CHR14_DATA}; lfs setstripe -c 72 ${HIPMER_CHR14_DATA} 2>/dev/null || true
  if [ -n "${AWS_URL}" ]
  then
    echo "Downloading chr14 fastq from AWS"
    (cd ${HIPMER_CHR14_DATA} 
     for f in ${FILES}
     do
         curl -o - $AWS_URL/$f.gz | gunzip -c > $f &
     done
     wait
    )
  else
    echo "Downloading human chr14 fastq... this could take a while"
    if [ -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ] 
    then
      echo "Do not download this data in a batch job.  Do it offline:  mkdir -p ${HIPMER_CHR14_DATA}; lfs setstripe -c 72 ${HIPMER_CHR14_DATA} ; cd ${HIPMER_CHR14_DATA}/.. ; curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_CHR14_DATA_URL | tar -xzf -"
      exit 1
    fi
    mkdir -p ${HIPMER_CHR14_DATA}/tmp; lfs setstripe -c 72 ${HIPMER_CHR14_DATA}/tmp || true ;
    cd ${HIPMER_CHR14_DATA}/tmp
    for i in $(seq 0 16)
    do
      echo "Starting download of $HIPMER_CHR14_DATA_URL"
      curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_CHR14_DATA_URL | tar -xzf - \
        && [ "${PIPESTATUS[*]}" == "0 0" ] \
        && echo "Success at $(date)" && break || rm -rf ${HIPMER_CHR14_DATA}/tmp/*
      echo "Download failed trying again in 30 min"
      sleep 1800
      echo "Trying again at $(date)"
    done
    cd -
    mv ${HIPMER_CHR14_DATA}/tmp/hipmer_chr14_data/* ${HIPMER_CHR14_DATA}
    rm -rf ${HIPMER_CHR14_DATA}/tmp
    [ -f ${HIPMER_CHR14_DATA}/frag_1.fastq ]
    [ -f ${HIPMER_CHR14_DATA}/frag_2.fastq ] 
    [ -f ${HIPMER_CHR14_DATA}/shortjump.fastq ] || echo "Warning shortjump.fastq is missing"
    [ -f ${HIPMER_CHR14_DATA}/longjump.fastq ] || echo "Warning longjump.fastq is missing"
  fi
  echo "Done downloading chr14 fastq from AWS"
fi

echo "Linking chr14 data set to $RUNDIR"
mkdir -p ${RUNDIR}
for i in ${HIPMER_CHR14_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR/ 2>/dev/null || ln -fs $i $RUNDIR/ ; done

cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*chr14*.config $RUNDIR 

echo "Done setting up chr14 data files"


