#include <stdio.h>
#include <stdlib.h>
#include <upc.h>
#include <upc_tick.h>

#include "../../src/hipmer/common/StaticVars.h"
#include "../../src/hipmer/common/upc_common.h"
#include "../../src/hipmer/common/common.h"

typedef struct list_t list_t;

struct list_t {
  shared[] list_t *myself, *next;
  int64_t val1;
#ifdef PAD
  char _pad[PAD];
#endif
};

typedef shared[] list_t *list_t_ptr;
typedef shared[1] list_t_ptr *shared_list_t_ptr;

#ifndef MAX_ENTRIES
#define MAX_ENTRIES 10
#endif

#define PRINT_INVALID 0

#define BTWNPHASE( ptr ) ( upc_phaseof(ptr) >= phaseMin[upc_threadof(ptr)] && upc_phaseof(ptr) <= phaseMax[upc_threadof(ptr)] )
#define BTWNADDR( ptr ) ( upc_addrfield(ptr) >= addrMin[upc_threadof(ptr)] && upc_addrfield(ptr) <= addrMax[upc_threadof(ptr)] )

int main(int argc, char **argv) {
   printf("Hello, I am %d of %d with pointers sized %lld and structs sized %lld\n", MYTHREAD, THREADS, (long long int) sizeof(list_t_ptr), (long long int) sizeof(list_t));

   shared_list_t_ptr heaps = upc_all_alloc(THREADS, sizeof(list_t_ptr));
   heaps[MYTHREAD] = (list_t_ptr) upc_alloc(MAX_ENTRIES * sizeof(list_t));
   int64_t *_tmp = (int64_t*) calloc(THREADS * 4, sizeof(int64_t));
   int64_t *phaseMin = _tmp;
   int64_t *phaseMax = phaseMin + THREADS;
   int64_t *addrMin = phaseMax + THREADS;
   int64_t *addrMax = addrMin + THREADS;
   for (int thr = MYTHREAD; thr < MYTHREAD + THREADS; thr++) {
       int destThr = thr%THREADS;
       phaseMin[destThr] = upc_phaseof(heaps[destThr]);
       addrMin[destThr] = upc_addrfield(heaps[destThr]);
       phaseMax[destThr] = upc_phaseof(heaps[destThr] + MAX_ENTRIES - 1);
       addrMax[destThr] = upc_addrfield(heaps[destThr] + MAX_ENTRIES - 1);
   }
   shared[1] int64_t *mismatches = upc_all_alloc(THREADS, sizeof(int64_t) * 6);
   shared[1] int64_t *mismatches2 = mismatches + THREADS;
   shared[1] int64_t *invalid1 = mismatches2 + THREADS;
   shared[1] int64_t *invalid2 = invalid1 + THREADS;
   shared[1] int64_t *invalid3 = invalid2 + THREADS;
   shared[1] int64_t *invalid4 = invalid3 + THREADS;

   list_t_ptr ptr, lastPtr;

   upc_barrier;
   if (!MYTHREAD) printf("allocated, now initializing...\n");
   fflush(stdout);
   
   lastPtr = ptr = heaps[MYTHREAD];
   for(int i = 0; i < MAX_ENTRIES; i++) {
       ptr = heaps[MYTHREAD] + i;
       ptr->val1 = upc_addrfield(ptr);
       ptr->myself = ptr;
       ptr->next = ptr;
   }
       
   upc_barrier;
   if (!MYTHREAD) printf("now testing...\n");
   fflush(stdout);
    
   unsigned int seed = MYTHREAD + 8675309 * upc_ticks_to_ns(upc_ticks_now());
   int attempts = 1000;
   int globalEntries = MAX_ENTRIES * THREADS;
   for(int round = 0; round < 20; round++) {
      if (!MYTHREAD) printf("Round %d\n", round);
      fflush(stdout);
      upc_barrier;
      mismatches[MYTHREAD] = mismatches2[MYTHREAD] = invalid1[MYTHREAD] = invalid2[MYTHREAD] = invalid3[MYTHREAD] = invalid4[MYTHREAD] = 0;
      for(int i = 0; i < globalEntries * attempts; i++) {
          int pos = rand_r(&seed) % globalEntries;
          int destThread = pos / MAX_ENTRIES;
          int destOffset = pos % MAX_ENTRIES;
          ptr = heaps[destThread];
          ptr += destOffset;
          ptr->next = lastPtr;
          list_t copy = *ptr;
          upc_fence;
          if (copy.next != lastPtr) mismatches[MYTHREAD]++;
          if (copy.myself != ptr) printf("Thread %d: ACK %d %d!\n", MYTHREAD, destThread, destOffset);
          if (!IS_VALID_UPC_PTR(copy.next)) { invalid1[MYTHREAD]++; if (PRINT_INVALID) printf("Thread %d: Copy next is bad %d %d!\n", MYTHREAD, destThread, destOffset); }
          if (!IS_VALID_UPC_PTR(ptr->next)) { invalid2[MYTHREAD]++; if (PRINT_INVALID) printf("Thread %d: next is bad %d %d!\n", MYTHREAD, destThread, destOffset); }
          if (!IS_VALID_UPC_PTR(ptr->next->myself)) { invalid3[MYTHREAD]++; if (PRINT_INVALID) printf("Thread %d: next self is bad %d %d!\n", MYTHREAD, destThread, destOffset); }
          if (!IS_VALID_UPC_PTR(ptr->next->next)) { invalid4[MYTHREAD]++; if (PRINT_INVALID) printf("Thread %d: next next is bad %d %d!\n", MYTHREAD, destThread, destOffset); }
          if (ptr->next != lastPtr) mismatches2[MYTHREAD]++;
          lastPtr = ptr;
      }
      upc_barrier;
      if (!MYTHREAD) {
          for(int thr = 1; thr < THREADS; thr++) {
            mismatches[MYTHREAD] += mismatches[thr];
            mismatches2[MYTHREAD] += mismatches2[thr];
            invalid1[MYTHREAD] += invalid1[thr];
            invalid2[MYTHREAD] += invalid2[thr];
            invalid3[MYTHREAD] += invalid3[thr];
            invalid4[MYTHREAD] += invalid4[thr];
          }
          double totalAttempts = globalEntries * attempts * THREADS;
          printf("Round %d: There were %lld mismatches and %lld active mismatches (%f total) %0.3f %% and %0.3f %% respectively\n",
                  round, (lld) mismatches[MYTHREAD], (lld) mismatches2[MYTHREAD], totalAttempts, 100.0* mismatches[MYTHREAD]/totalAttempts, 100.0*mismatches2[MYTHREAD]/totalAttempts);
          printf("Round %d: invalid1: %lld, invalid2: %lld, invalid3: %lld, invalid4: %lld\n", round, (lld) invalid1[MYTHREAD], (lld) invalid2[MYTHREAD], (lld) invalid3[MYTHREAD], (lld) invalid4[MYTHREAD]);
      }
      fflush(stdout);
   }

   free(_tmp);

   return 0;
}

