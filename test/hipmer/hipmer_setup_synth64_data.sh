#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_SYNTH64_DATA=${HIPMER_SYNTH64_DATA:=${SCRATCH}/hipmer_synth64_data}
HIPMER_SYNTH64_DATA_URL=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/SRR606249.fastq.gz

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_SYNTH64_DATA=$HIPMER_SYNTH64_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with sync64 data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi


echo "Linking SYNTH64 set to ${RUNDIR}"


if [ ! -d ${HIPMER_SYNTH64_DATA} ]
then
    echo "Downloading synth64 fastq from the NERSC HPSS system... this could take a while"
    if [  -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download the data within a batch job. run 'mkdir -p ${HIPMER_SYNTH64_DATA}; lfs setstripe -c 72 ${HIPMER_SYNTH64_DATA} 2>/dev/null || true ; cd ${HIPMER_SYNTH64_DATA} ; curl --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_SYNTH64_DATA_URL | gunzip -c > SRR606249.fastq' offline"
      exit 1
    fi
    mkdir -p ${HIPMER_SYNTH64_DATA} 
    lfs setstripe -c 72 ${HIPMER_SYNTH64_DATA} 2>/dev/null || true
    cd ${HIPMER_SYNTH64_DATA} 
    for i in $(seq 0 16)
    do
      curl --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_SYNTH64_DATA_URL | gunzip -c > SRR606249.fastq.tmp \
        && [ "${PIPESTATUS[*]}" == "0 0" ] && mv SRR606249.fastq.tmp SRR606249.fastq && break
      echo "Download failed, trying again in 30 min"; sleep 1800
    done
    [ -f ${HIPMER_SYNTH64_DATA}/SRR606249.fastq ]
    echo "Done downloading synth64 dataset"
fi

echo "Linking SYNTH64 set to ${RUNDIR}"
mkdir -p $RUNDIR
for i in ${HIPMER_SYNTH64_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR 2>/dev/null || ln -fs $i $RUNDIR ; done

cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/synth64*.config $RUNDIR 
cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*.hmm $RUNDIR 


echo "Done setting up synth64 dataset"

