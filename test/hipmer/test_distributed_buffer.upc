#include <stdint.h>
#include <math.h>
#include <upc.h>

#include "../../src/hipmer/common/upc_common.h"
#include "../../src/hipmer/common/common.h"
#include "../../src/hipmer/common/StaticVars.h"
#include "../../src/hipmer/common/utils.h"
#include "../../src/hipmer/common/Buffer.h"
#include "../../src/hipmer/common/upc_distributed_buffer.h"

#define SMALL_CAPACITY 11
#define LARGE_CAPACITY 1009 /* 11 or 109 or 1009 or 10009 large prime number */

void checkFixed(DistributedBuffer db, int64_t block);
void checkVariable(DistributedBuffer db, int64_t block);
void checkValues(DistributedBuffer db, int64_t block);
void testWithBlockSize(int CAPACITY);

int elementSize = sizeof(int32_t);
int main(int argc, char **argv) {
	
	int64_t block = 1024;
	SLOG("Starting\n");

	// fill lower triangle of THREADS x THREADS matrix with chars:
	// with every row set to (int32_t) thread
	// idxrange(thread): block * [ (thread * thread / 2) to ((thread+1) * (thread+1) / 2 - 1) ]
	// thread = sqrt( (idx / block) * 2 )

        int64_t globalSize;
	int64_t mySize = (MYTHREAD+1) * block, totalCapacity = 0;
	int64_t myOffset = reduce_prefix_long(mySize, UPC_ADD, &globalSize) - mySize;
	upc_barrier;
	totalCapacity = broadcast_long(globalSize, 0);

	Buffer buf = initBuffer(mySize);
	int32_t val = MYTHREAD;
	for(int64_t i = 0; i < mySize / elementSize; i++) {
		writeBuffer(buf, &val, elementSize);
        }
	LOG("mySize=%lld myOffset=%lld bufLen=%lld\n", (lld) mySize, (lld) myOffset, (lld) getLengthBuffer(buf));
	
	DistributedBuffer db = DistributedBuffer_initFixed(mySize, 1);
	DistributedBuffer_writeBufferAt(db, buf, myOffset);

	upc_barrier;

	checkFixed(db, block);
	checkValues(db, block);
	DistributedBuffer_free(&db);

	db = DistributedBuffer_initVariable(mySize, 1);
	rewindBuffer(buf);
	DistributedBuffer_writeBufferAt(db, buf, myOffset);
	upc_barrier;

	checkVariable(db, block);
	checkValues(db, block);
	DistributedBuffer_free(&db);
	freeBuffer(buf);
		
	testWithBlockSize(SMALL_CAPACITY);
	testWithBlockSize(LARGE_CAPACITY);

	return 0;
}

void testWithBlockSize(int CAPACITY) {
	int *tmpBuf = (int*) calloc(CAPACITY+1, sizeof(int));
	// Now an irregular lower triangle of CAPACITY x CAPACITY, blockSize=sizeof(int)
	int64_t rowsPerThread = (CAPACITY + THREADS - 1) / THREADS;
	int64_t myStart = MYTHREAD * rowsPerThread, myEnd = (MYTHREAD+1) * rowsPerThread;
	if (myStart >= CAPACITY) myStart = CAPACITY;
	if (myEnd >= CAPACITY) myEnd = CAPACITY;
	int64_t myCount = 0;
	for(int64_t row = myStart; row < myEnd; row++) {
		myCount += row + 1;
	}
	DistributedBuffer db = DistributedBuffer_initFixed(myCount, sizeof(int));
	LOG("DB: localCount=%lld threadCount=%lld, globalOffset=%lld threadOffset=%lld\n", db->elements[MYTHREAD].localCount, db->elements[MYTHREAD].threadCount, db->elements[MYTHREAD].globalOffset, db->elements[MYTHREAD].threadOffset);
	upc_barrier;
	int64_t myStartOffset = DistributedBuffer_getThreadGlobalOffset(db, MYTHREAD);
	for(int64_t row = myStart; row < myEnd; row++) {
		int *x = tmpBuf;
		int64_t entries = row+1;
		for(int64_t i = 0 ; i < entries; i++) {
			*(x++) = row;
		}
		//DBG("row %lld: writing %lld entries starting at %lld\n", row, entries, myStartOffset);
		myStartOffset += DistributedBuffer_writeAt(db, myStartOffset, tmpBuf, entries);
	}
	upc_barrier;
	free(tmpBuf);

	myStartOffset = DistributedBuffer_getLocalGlobalOffset(db, MYTHREAD);
	int64_t myEndOffset = myStartOffset + DistributedBuffer_getLocalCount(db, MYTHREAD);
	int64_t numNonLocal = 0, failedVal = 0;;
	LOG("myStartOffset=%lld myEndOffset=%lld\n", (lld) myStartOffset, (lld) myEndOffset);
	for(int64_t thisOffset = myStartOffset; thisOffset < myEndOffset; thisOffset++) {
		SharedCharPtr ptr = DistributedBuffer_getSharedPtr(db, thisOffset);
		if (upc_threadof(ptr) != MYTHREAD || upc_threadof( ptr + sizeof(int)-1) != MYTHREAD) numNonLocal++;
		int test = *((shared [] int*) ptr);
		int expVal1 = thisOffset * 2; // expVal1 == row * (row+1) == row*row + row.   0 = 1 *row*row + 1*row - expVal1
		int expVal = (-1 + sqrt(1 + 4*expVal1)) / 2; // (-b + sqrt(b*b - 4ac)) / 2a
                if (test != expVal) failedVal++;
	}
	if (numNonLocal > 0) WARN("Nonlocal: %lld\n", (lld) numNonLocal);
	if (failedVal > 0) DIE("Failed: %lld\n", (lld) failedVal);
	DistributedBuffer_free(&db);
}
		
void checkFixed(DistributedBuffer db, int64_t block) {
	int64_t expTotal = block * (THREADS * (THREADS+1) / 2); // lower triangle
	if (DistributedBuffer_getTotalCount(db) != expTotal) {
		DIE("Error totalCount = %lld not %lld\n", (lld) db->totalCount, (lld) expTotal);
	}
	int64_t expGlobalOffset = ((expTotal+THREADS-1) / THREADS) * MYTHREAD;
	if (db->elements[MYTHREAD].globalOffset != expGlobalOffset) {
		DIE("Error my globalOffset = %lld not %lld\n", (lld) db->elements[MYTHREAD].globalOffset, (lld) expGlobalOffset);
	}
	int64_t localCount = expTotal / THREADS;
	if (db->elements[MYTHREAD].localCount != localCount) {
		DIE("Error my localCount = %lld not %lld\n", (lld) db->elements[MYTHREAD].localCount,(lld) localCount);
	}

}
void checkVariable(DistributedBuffer db, int64_t block) {
	int64_t expTotal = block * (THREADS * (THREADS+1) / 2); // lower triangle
	if (db->totalCount != expTotal) {
		DIE("Error totalCount = %lld not %lld\n", (lld) db->totalCount, (lld) expTotal);
	}
}

void checkValues(DistributedBuffer db, int64_t block) {

	int64_t localCount = (db->totalCount+THREADS-1) / THREADS;
	int64_t globalOffset = localCount * MYTHREAD;
	upc_barrier;
	Buffer buf = initBuffer(block * localCount);
	DistributedBuffer_readBufferAt(db, buf, globalOffset, localCount);

	// Check values
	for (int64_t i = 0; i < localCount / elementSize; i++) {
		int64_t thisOffset = (globalOffset + i*elementSize);
		int32_t expVal1 = thisOffset / block * 2; // expVal1 == thr * (thr+1) == thr*thr + thr.   0 = 1 *thr*thr + 1 *thr - expVal1
		int32_t expVal = (-1 + sqrt(1 + 4*expVal1)) / 2; // (-b + sqrt(b*b - 4ac)) / 2a
		int32_t obsVal = *((int32_t*) (getStartBuffer(buf) + i*elementSize));
		if (obsVal != expVal) {
			DIE("thisOffset %lld: obs=%d exp=%d\n", (lld) thisOffset, obsVal, expVal);
		}
	}
	freeBuffer(buf);
}

