
#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with validation data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi

mkdir -p $RUNDIR

echo "Copying validate set to ${RUNDIR}"
for f in frags.25K.fastq  jumps.25K.fastq
do
    ln ${HIPMER_INSTALL}/etc/meraculous/pipeline/$f ${RUNDIR}/ 2>/dev/null || cp -p  ${HIPMER_INSTALL}/etc/meraculous/pipeline/$f ${RUNDIR}/
done
cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/validation* ${RUNDIR}/
cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*.hmm ${RUNDIR}/


echo "Done setting up validation data set"
