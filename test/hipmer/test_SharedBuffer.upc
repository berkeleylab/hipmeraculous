#include <stdio.h>
#include <stdlib.h>

#include <upc.h>

#include "SharedBuffer.h"

int main(int argc, char **argv)
{

    Buffer b = initBuffer(100);
    printfBuffer(b, "Testing 1 2 3 from thread %d", MYTHREAD);

    AllSharedBufferPtr sb = NULL;
    UPC_ALL_ALLOC_CHK(sb, THREADS, sizeof(SharedBuffer));

    sb[MYTHREAD] = newSharedBuffer(b);
    Buffer test = initBuffer(1);
    getAllSharedBuffer(test, &sb[MYTHREAD]);
    if (strcmp(getStartBuffer(test), getStartBuffer(b)) != 0) {
        DIE("Got incorrect data: '%s' vs '%s'\n", getStartBuffer(b), getStartBuffer(test));
    }
    LOG("Got: %s\n", getStartBuffer(test));

    upc_barrier;

    resetBuffer(b);
    printfBuffer(b, "%d", MYTHREAD);
    putSharedBuffer(&sb[MYTHREAD], b);

    upc_barrier;

    getSharedBuffer(test, (const SharedBufferPtr) &sb[ (MYTHREAD+1) % THREADS ] );

    char tmp[16];
    sprintf(tmp, "%d", (MYTHREAD+1)%THREADS);

    if (strcmp(tmp, getStartBuffer(test)) != 0) {
        DIE("Got incorrect data: '%s' vs '%s'\n", tmp, getStartBuffer(test));
    }

    upc_barrier;

    resetBuffer(b);
    printfBuffer(b, "%d", MYTHREAD*2);
    putSharedBuffer( (SharedBufferPtr) &sb[ (MYTHREAD+2)%THREADS ], b);

    upc_barrier;

    getAllSharedBuffer(test, &sb[ (MYTHREAD+1)%THREADS ]);

    sprintf(tmp, "%d", ((MYTHREAD + THREADS - 1)%THREADS) * 2);

    if (strcmp(tmp, getStartBuffer(test)) != 0) {
        DIE("Got incorrect data: '%s' vs '%s'\n", tmp, getStartBuffer(test));
    }

    upc_barrier;

    freeSharedBuffer(sb[MYTHREAD]);
    UPC_ALL_FREE_CHK(sb);

    return 0;
}
