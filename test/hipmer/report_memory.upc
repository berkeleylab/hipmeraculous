#include <stdio.h>

#ifdef DEBUG
#undef DEBUG
#endif

#include <upc.h>

#include "../../src/hipmer/common/upc_compatibility.h"
#include "../../src/hipmer/common/upc_common.h"
#include "../../src/hipmer/common/common.h"

#define CHUNK_SIZE 100

void report() {
   if (!MYTHREAD) printf("FREE %6.1f MB\tUSED %6.1f MB\tMAX_USED %6.1f MB\n", get_free_mem_gb()*1000.0, get_used_mem_gb()*1000.0, (double) get_max_mem_usage_mb());
   upc_barrier;
}

typedef char * ptr;
typedef shared[1] ptr * global_ptr;
typedef shared[] ptr *local_ptr;

ptr MALLOC(int mb) {
   ptr p = (ptr) malloc(ONE_MB * mb);
   if (!MYTHREAD) printf("Malloced (no touch) %3d MB of private: %10p\t", mb, p);
   report();
   return p;
}

ptr CALLOC(int mb) {
   ptr p = (ptr) calloc(ONE_MB * mb/ sizeof(int64_t), sizeof(int64_t));
   if (!MYTHREAD) printf("Calloced (touch) %3d MB of private: %10p   \t", mb, p);
   report();
   return p;
}

void FREE(ptr p) {
   free(p);
   if (!MYTHREAD) printf("Freed private %10p                         \t", p);
   report();
}

global_ptr GLOBAL_ALLOC(int mb) {
    global_ptr gp = NULL;
    UPC_ALL_ALLOC_CHK(gp, THREADS*mb*ONE_MB / sizeof(int64_t), sizeof(int64_t));
    if (!MYTHREAD) printf("global alloced %3d MB: %10lld             \t\t", mb, (lld) upc_addrfield(gp)); 
    report();
    return gp;
}

void GLOBAL_FREE(global_ptr gp) {
    if (!MYTHREAD) printf("Freed global: %10lld                      \t\t", (lld) upc_addrfield(gp));
    UPC_ALL_FREE_CHK(gp);
    report();
}

local_ptr LOCAL_ALLOC(int mb) {
    local_ptr lp = NULL;
    UPC_ALLOC_CHK(lp, mb*ONE_MB);
    if (!MYTHREAD) printf("local alloc %3d MB: %10lld                \t\t", mb, (lld) upc_addrfield(lp)); 
    report();
    return lp;
}

void LOCAL_FREE(local_ptr lp) {
    if (!MYTHREAD) printf("Freed local: %10lld                       \t\t", (lld) upc_addrfield(lp));
    UPC_FREE_CHK(lp);
    report();
}

#define ITERATIONS 5 
int main(int argc, char **argv) {

   INIT_STATIC_VARS;
   report();

   ptr m[ITERATIONS], c[ITERATIONS];
   global_ptr g[ITERATIONS];
   local_ptr l[ITERATIONS];


   if (!MYTHREAD) printf("Starting catch and release                           \t");
   report();

   for(int i = 0; i < ITERATIONS; i++) {
      m[i] = MALLOC(CHUNK_SIZE);
      
      c[i] = CALLOC(CHUNK_SIZE/2);

      g[i] = GLOBAL_ALLOC(CHUNK_SIZE*2);

      l[i] = LOCAL_ALLOC(CHUNK_SIZE/4);

      memset(m[i], 255, CHUNK_SIZE * ONE_MB);
      if (!MYTHREAD) printf("Touched malloc                                   \t");
      report();
   
      LOCAL_FREE(l[i]);
      GLOBAL_FREE(g[i]);
      FREE(c[i]);
      FREE(m[i]);
   }

   if (!MYTHREAD) printf("Starting accumulate                                 \t");
   report();
   for(int i = 0; i < ITERATIONS; i++) {
      m[i] = MALLOC(CHUNK_SIZE);
      
      c[i] = CALLOC(CHUNK_SIZE/2);

      g[i] = GLOBAL_ALLOC(CHUNK_SIZE*2);

      l[i] = LOCAL_ALLOC(CHUNK_SIZE/4);

      memset(m[i], 255, CHUNK_SIZE * ONE_MB);
      if (!MYTHREAD) printf("Touched malloc                                   \t");
      report();
   }
   if (!MYTHREAD) printf("Starting Free                                       \t");
   report();
   for(int i = ITERATIONS-1; i >=0; i--) {
      LOCAL_FREE(l[i]);
      GLOBAL_FREE(g[i]);
      FREE(c[i]);
      FREE(m[i]);
   }

   if (!MYTHREAD) printf("Starting catch and release again                   \t");
   report();
   for(int i = 0; i < ITERATIONS; i++) {
      m[i] = MALLOC(CHUNK_SIZE);
      
      c[i] = CALLOC(CHUNK_SIZE/2);

      g[i] = GLOBAL_ALLOC(CHUNK_SIZE*2);

      l[i] = LOCAL_ALLOC(CHUNK_SIZE/4);

      memset(m[i], 255, CHUNK_SIZE * ONE_MB);
      if (!MYTHREAD) printf("Touched malloc                                  \t");
      report();
   
      LOCAL_FREE(l[i]);
      GLOBAL_FREE(g[i]);
      FREE(c[i]);
      FREE(m[i]);
   }

   if (!MYTHREAD) printf("End\n");
   report();
   return 0;
}


