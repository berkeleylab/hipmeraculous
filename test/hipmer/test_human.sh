#!/bin/bash

for m in $1 ${NERSC_HOST} generic $(uname -s)
do
  if [ -d "${HIPMER_INSTALL}" ]
  then
    break
  fi

  for d in ${SCRATCH}/hipmer-install-$m ${HOME}/hipmer-install-$m
  do
    if [ -d $d ]
    then
      echo "+ Using $d"
      export HIPMER_INSTALL=$d
      break
    fi
  done
done


UPC_SHARED_HEAP_SIZE=1G \
KMER_CACHE_MB=4096 SW_CACHE_MB=1024 \
HIPMER_DATADIR=$SCRATCH/hipmer_human_data \
HIPMER_TEST=human \
    ${HIPMER_INSTALL}/bin/test_hipmer.sh
