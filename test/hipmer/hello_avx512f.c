#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <immintrin.h>
#include <xmmintrin.h>
#include <emmintrin.h>

#define CHUNK_SEQ 16

int main(int argc, char *argv[]) {

    const __m128i reverse_mask = _mm_setr_epi8(15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0);
    const __m512i trans_mask = _mm512_set_epi32(7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7);
    const __m512i trans_table = _mm512_set_epi32(0, 0, 0, 0, 0, 0, 0, 0, (int32_t)'C', 0, 0, (int32_t)'A', (int32_t)'G', 0, (int32_t)'T', 0);

    int i, seqLen=8192;
    char *seq = (char*) calloc(seqLen+1, 1);
    char *rc_seq = (char*) calloc(seqLen+1, 1);
    const int n_blocks = seqLen/(2*CHUNK_SEQ);
    const int remaining = seqLen - (n_blocks*2*CHUNK_SEQ);
    int start_offset = 0;
    int end_offset = seqLen - CHUNK_SEQ;
    __m128i front_block, back_block;
    __m512i extended_front_block, extended_back_block, translated_front_block, translated_back_block;

    for (i = 0; i < n_blocks; i++) {
      front_block = _mm_lddqu_si128((const __m128i*)&seq[start_offset]);
      front_block = _mm_shuffle_epi8(front_block, reverse_mask);
      extended_front_block = _mm512_cvtepi8_epi32(front_block);

      back_block = _mm_lddqu_si128((const __m128i*)&seq[end_offset]);
      back_block = _mm_shuffle_epi8(back_block, reverse_mask);
      extended_back_block = _mm512_cvtepi8_epi32(back_block);

      extended_front_block = _mm512_and_epi32(extended_front_block, trans_mask);
      translated_front_block = _mm512_permutexvar_epi32(extended_front_block, trans_table);
      front_block = _mm512_cvtepi32_epi8(translated_front_block);

      extended_back_block = _mm512_and_epi32(extended_back_block, trans_mask);
      translated_back_block = _mm512_permutexvar_epi32(extended_back_block, trans_table);
      back_block = _mm512_cvtepi32_epi8(translated_back_block);

      _mm_storeu_si128( (__m128i*)&rc_seq[end_offset], front_block);
      _mm_storeu_si128( (__m128i*)&rc_seq[start_offset], back_block);

      start_offset += CHUNK_SEQ;
       end_offset -= CHUNK_SEQ;
    }

    return EXIT_SUCCESS;
}
