#!/bin/bash

# too much variation for checking the results using checksums

exec_dir=`dirname $0`
HIPMER_POSTRUN="compare_diags.py ${exec_dir}/../etc/meraculous/pipeline/chr14.diags diags.log" \
CANONICALIZE=1 \
HIPMER_TEST=chr14 ${exec_dir}/test_hipmer.sh
