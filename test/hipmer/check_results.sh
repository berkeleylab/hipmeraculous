#!/bin/bash

echo "Starting check_results.sh"
#env
set -e
set -o pipefail

if [ "$RUNDIR" != "" ]; then
    pushd $RUNDIR > /dev/null
fi
if [ -z "$THREADS" ]; then
	export THREADS=`grep processor /proc/cpuinfo|wc -l`
fi

PASS="\x1B[92mPASS\x1B[0m"
FAIL="\x1B[91mFAIL\x1B[0m"
check_md5()
{
    vals=$1
    vals=${vals//,/ }
    file=$2
    filename=$3
    [ -n "$filename" ] || filename=$file
    if [ -f "${file}" ] || [ "${file}" == "-" ]
    then
      if [ "$(uname -s)" == "Darwin" ]
      then
          if [ "${file}" == "-" ]
          then
            check_val="$(md5) -"
          else
            check_val="$(md5 -r ${file})"
          fi
      else
          check_val="$(md5sum ${file}|cut -d' ' -f1) ${file}"
      fi
      for val in $vals; do 
          if [ "${check_val}" == "${val} ${file}" ]
          then
              echo -e "    ${PASS}  ${filename}"
              return 0
          fi
      done
    else
       check_val="No file: '$file'"
    fi
    echo -e "    ${FAIL}  ${filename}"
    echo "      ${check_val}"
    return 1
}

invalid=0
if [ -f results/canonical_contigs.fa ] || [ -f results/final_assembly.fa ]; then
  echo "Evaluating checksums:"
  if [ 1 ]
  then
    if [ -n "$ufx_md5sum" ]
    then
        for ufx in intermediates/ALL_INPUTS.fofn-*.ufx.bin.gz
        do
           if [ -f "$ufx" ]
           then
            echo -n "UFX:                "
            gunzip -c $ufx | sort | check_md5 $ufx_md5sum - $ufx || invalid=1
            break # only check the first UFX as the subsequent rounds may have races in correcting by contigs and slightly different results
           fi
        done
    fi
    if [ -f results/canonical_contigs.fa ] && [ -n "$contigs_md5sum" ]; then
        echo -n "contigs:            "
        check_md5 $contigs_md5sum  results/canonical_contigs.fa || invalid=1
    fi
    if [ -f results/final_assembly.fa ] && [ -n "${assembly_md5sum}" ] ; then
        echo -n "assembly:           "
        if ! check_md5 $assembly_md5sum  results/final_assembly.fa
        then
            if [ -n "$uc_assem_md5sum" ]
            then
                tr '[:lower:]' '[:upper:]' < results/final_assembly.fa > results/final_assembly.uppercase.fa
                echo -n "uppercase assembly: "
                check_md5 $uc_assem_md5sum  results/final_assembly.uppercase.fa || invalid=1
            else
                invalid=1
            fi
        fi
    fi
  fi > .checksums
  cat .checksums
fi

if [ "$RUNDIR" != "" ]; then
    popd > /dev/null
fi

if [ $invalid -ne 0 ]
then
  echo -e "${FAIL} Some of the results were invalid!"
else
  echo -e "${PASS} all results are valid!"
fi

exit $invalid

