#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sched.h>
#include <upc.h>

/* Borrowed from util-linux-2.13-pre7/schedutils/taskset.c */
static char *cpuset_to_cstr(cpu_set_t *mask, char *str)
{
  char *ptr = str;
  int i, j, entry_made = 0;
  for (i = 0; i < CPU_SETSIZE; i++) {
    if (CPU_ISSET(i, mask)) {
      int run = 0;
      entry_made = 1;
      for (j = i + 1; j < CPU_SETSIZE; j++) {
        if (CPU_ISSET(j, mask)) run++;
        else break;
      }
      if (!run)
        sprintf(ptr, "%d,", i);
      else if (run == 1) {
        sprintf(ptr, "%d,%d,", i, i + 1);
        i++;
      } else {
        sprintf(ptr, "%d-%d,", i, i + run);
        i += run;
      }
      while (*ptr != 0) ptr++;
    }
  }
  ptr -= entry_made;
  *ptr = 0;
  return(str);
}


static void log_cpu_affinity() {
  cpu_set_t coremask;
  char clbuf[7 * CPU_SETSIZE], hnbuf[128];

  memset(clbuf, 0, sizeof(clbuf));
  memset(hnbuf, 0, sizeof(hnbuf));
  (void)gethostname(hnbuf, sizeof(hnbuf));
  int maxThreads = THREADS;
  {
    (void)sched_getaffinity(0, sizeof(coremask), &coremask);
    cpuset_to_cstr(&coremask, clbuf);
    printf("thread %d of %d, on %s (%s)\n",
            MYTHREAD, THREADS, hnbuf, clbuf);
  }
}

int main(void)
{
  if (sched_getcpu() < 0)
  {
    exit(1);
  }
  cpu_set_t coremask;
  if (sched_getaffinity(0, sizeof(coremask), &coremask) != 0) {
    exit(1);
  }
  log_cpu_affinity();
  exit(0);
}
