#!/usr/bin/env perl

use strict;
use warnings;

# [1001] 0: [0x6cf232] _gasneti_print_backtrace_ifenabled gasnet_tools.c:?
# [1001] 1: [0x760e40] gasneti_defaultSignalHandler ??:?
# [1001] 2: [0x7e5ea0] __restore_rt sigaction.c:?
# [1001] 3: [0xa05c42] strlen /usr/src/packages/BUILD/glibc-2.11.3/string/../sysdeps/x86_64/multiarch/../strlen.S:31
# [1001] 4: [0x9e8940] _IO_vfprintf_internal /usr/src/packages/BUILD/glibc-2.11.3/stdio-common/vfprintf.c:1570
# [1001] 5: [0xa2b8d6] ___vsnprintf_chk /usr/src/packages/BUILD/glibc-2.11.3/debug/vsnprintf_chk.c:65
# [1001] 6: [0x624552] gzprintf /usr/src/packages/BUILD/zlib-1.2.7/gzwrite.c:366
# [1001] 7: [0x514967] processAlignment splinter.trans.c:?
# [1001] 8: [0x5120fa] splinter_main ??:?
# [1001] 9: [0x41baf2] exec_stage ??:?
# [1001] 10: [0x409ff9] user_main ??:?
# [1001] 11: [0x69c360] upcri_perthread_spawn upcr_globfiles.c:?
# [1001] 12: [0x69df48] bupc_init_reentrant ??:?
# [1001] 13: [0x62dbf8] main ??:?
# [1001] 14: [0x9d0731] __libc_start_main /usr/src/packages/BUILD/glibc-2.11.3/csu/libc-start.c:242
# [1001] 15: [0x400875] _start /usr/src/packages/BUILD/glibc-2.11.3/csu/../sysdeps/x86_64/elf/start.S:116

my @ll_pos;
my %h_desc;

while (<>) {
    chomp;
    if (/\[(\d+)\] (\d+): \[(0x\w+)\] (.+)/) {
        my($pid, $level, $pos, $desc) = ($1, $2, $3, $4);
        $ll_pos[$pid][$level] = $pos;
	$h_desc{$level . ":" . $pos} = [$desc, $_];
    }
}

my %h_count;

foreach my $rl (@ll_pos) {
    next unless defined $rl;
    my @stack;
    for (my $i = 0; $i < scalar(@$rl) ; $i++) {
      if (defined $rl->[$i]) {
        push @stack, "$i:" . $rl->[$i];
      }
    }
    my $st = join("\t", @stack);
    $h_count{$st}++;
}

while (my($stack,$count) = each %h_count) {
  print "$count: $stack\n";
  my @level_pos = split(/\t/, $stack);
  my $n = 0;
  foreach my $p (@level_pos) {
    print "\t$n\t$p\t$h_desc{$p}[1]\n" if (defined $p && defined $h_desc{$p});
  }
  print "\n\n";
}

