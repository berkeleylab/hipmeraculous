#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_ECOLI_DATA=${HIPMER_ECOLI_DATA:=${SCRATCH}/hipmer_ecoli_data}
HIPMER_ECOLI_DATA_URL=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/hipmer_ecoli.tar.gz
FILES="ECO.fastq.0.fq ECO.fastq.1.fq"

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_ECOLI_DATA=$HIPMER_ECOLI_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with ecoli data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi


TEST_AWS=$(curl --max-time 2 -s http://169.254.169.254/latest/dynamic/instance-identity/document| grep instanceId || /bin/true)
AWS_URL=
if [ -n "${TEST_AWS}" ]
then
    AWS_URL=https://s3.amazonaws.com/hipmer-benchmarks/hipmer_ecoli_data
fi

if [ ! -d ${HIPMER_ECOLI_DATA} ] || rmdir ${HIPMER_ECOLI_DATA} 2>/dev/null # i.e. it does not exist or is empty
then
  
  mkdir -p ${HIPMER_ECOLI_DATA}; lfs setstripe -c 72 ${HIPMER_ECOLI_DATA} 2>/dev/null || true
  if [ -n "${AWS_URL}" ]
  then
    echo "Downloading ecoli fastq from AWS"
    (cd ${HIPMER_ECOLI_DATA} 
     for f in ${FILES}
     do
         curl -o - $AWS_URL/$f.gz | gunzip -c > $f &
     done
     wait
    )
  else
    echo "Downloading ecoli fastq from the NERSC HPSS system... this could take a while"
    if [ -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download the data within a batch job. run 'mkdir -p ${HIPMER_ECOLI_DATA}; lfs setstripe -c 72 ${HIPMER_ECOLI_DATA} 2>/dev/null || true ; cd ${HIPMER_ECOLI_DATA} ; curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_ECOLI_DATA_URL | tar -xzf' offline"
      exit 1
    fi
    mkdir -p ${HIPMER_ECOLI_DATA}/tmp
    lfs setstripe -c 8 ${HIPMER_ECOLI_DATA}/tmp 2>/dev/null || true
    cd ${HIPMER_ECOLI_DATA}/tmp
    for i in $(seq 0 16)
    do
      curl --keepalive-time 30 --max-time 7200 --retry 3 --retry-delay 120 -o - $HIPMER_ECOLI_DATA_URL | tar -xzf - \
        && [ "${PIPESTATUS[*]}" == "0 0" ] && echo "Success" && break || rm -rf tmp/*
      echo "Download failed, trying again in 30 min"; sleep 1800
    done 
    cd -
    mv ${HIPMER_ECOLI_DATA}/tmp/* ${HIPMER_ECOLI_DATA}
    rmdir ${HIPMER_ECOLI_DATA}/tmp
    [ -f ${HIPMER_ECOLI_DATA}/ECO.fastq.0.fq ] && [ -f ${HIPMER_ECOLI_DATA}/ECO.fastq.1.fq ]
  fi

  echo "Done downloading ecoli data set"
  
fi

mkdir -p ${RUNDIR}
echo "Linking E. coli set to ${RUNDIR}"
for i in ${HIPMER_ECOLI_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR/ 2>/dev/null || ln -fs $i $RUNDIR/ ; done

cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*ecoli*.config ${HIPMER_INSTALL}/etc/meraculous/pipeline/*hmm $RUNDIR 

echo "Done setting up ecoli data set"

