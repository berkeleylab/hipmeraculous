#!/bin/bash

if [ -z "$HIPMER_INSTALL" ]; then
    export HIPMER_INSTALL=$(dirname `dirname $0`)
fi

echo $HIPMER_INSTALL

contigs_md5sum="18fa925de263d6576ae713d33dd45ead" \
assembly_md5sum="4da1ff76b3b94eb45b507b74a013bd68,b0e4991824d0861d7109f9576ce08a18,730053f3aa996f0e112385db535c3cbf,bb4ad7834d7f6c0fbfe27f35676e61dc" \
uc_assem_md5sum="120059e0a3607697921f93dffdd2789a,63708fc9d2142b6354e42aa431369fdd" \
KMER_LENGTH=21 \
HIPMER_POSTRUN=${HIPMER_INSTALL}/bin/check_results.sh \
CANONICALIZE=${CANONICALIZE:=1} \
HIPMER_TEST=validation ${HIPMER_INSTALL}/bin/test_hipmer.sh


