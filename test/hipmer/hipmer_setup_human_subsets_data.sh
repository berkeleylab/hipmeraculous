#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"


SCRATCH=${SCRATCH:=/tmp}
HIPMER_HUMAN_DATA=${HIPMER_HUMAN_DATA:=${SCRATCH}/hipmer_human_subsets_data}
HIPMER_HUMAN_DATA_URL=https://portal.nersc.gov/archive/home/r/regan/www/HipMer/hipmer_human_subsets.tar.gz

USAGE="$0 [ RUNDIR ]
 (or set these as environmental variables)
 RUNDIR=/path/to/desired/working_directory (if desired)
 SCRATCH=$SCRATCH (if RUNDIR is not specified)
 HIPMER_HUMAN_DATA=$HIPMER_HUMAN_DATA

"
set -e
set -o pipefail

HIPMER_INSTALL=${DIR%/*}

SCRATCH=${SCRATCH:=/tmp}
if [ -n "$1" ]
then
  RUNDIR=${RUNDIR:=$1}
fi

if [ -n "${RUNDIR}" ]
then
  echo "Setting up ${RUNDIR} with human subsets data set at $(date)"
else
  echo "$USAGE"
  exit 1
fi

if [ ! -d ${HIPMER_HUMAN_DATA} ] || rmdir ${HIPMER_HUMAN_DATA} 2>/dev/null # i.e. it does not exist or is empty
then
    echo "Downloading human fastq from the NERSC HPSS system... this could take a while"
    if [ -n "${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}" ]
    then
      echo "Do not download this data in a batch job.  Do it offline:  mkdir ${HIPMER_HUMAN_DATA}; lfs setstripe -c 72 ${HIPMER_HUMAN_DATA} ; cd ${HIPMER_HUMAN_DATA} ; curl --keepalive-time 30 --max-time 36000 --retry 3 --retry-delay 120 -o - $HIPMER_HUMAN_DATA_URL | tar -xzf -"
      exit 1
    fi
    mkdir -p ${HIPMER_HUMAN_DATA}.tmp
    lfs setstripe -c 72 ${HIPMER_HUMAN_DATA}.tmp || true
    cd ${HIPMER_HUMAN_DATA}.tmp 
    for i in $(seq 0 16)
    do
      curl --keepalive-time 30 --max-time 36000 --retry 3 --retry-delay 120 -o - $HIPMER_HUMAN_DATA_URL | tar -xzf - \
        && [ "${PIPESTATUS[*]}" == "0 0" ] && break || rm -rf *.fastq
      echo "Download failed, trying again in 30 min"; sleep 1800
    done && cd .. && mv ${HIPMER_HUMAN_DATA}.tmp ${HIPMER_HUMAN_DATA})
fi

echo "Linking human subsets data into $RUNDIR"

mkdir -p ${RUNDIR}
for i in ${HIPMER_HUMAN_DATA}/* ; do [ ! -f "${i}" ] || ln -f $i $RUNDIR/ 2>/dev/null || ln -fs $i $RUNDIR/ ; done

cp -p ${HIPMER_INSTALL}/etc/meraculous/pipeline/*human*.config $RUNDIR 


echo "Done setting up human subsets dataset"


