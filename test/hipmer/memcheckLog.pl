#!/usr/bin/env perl

use threads;
use threads::shared;
use strict;
use warnings;

# 1s [UU_traversal_final.h:876] 524248 upc_alloc_chk(72) 0,11856 realPtr=0,11792/0x7fc59b32be10 realSize=200 PAD=64
# 1s [UU_traversal_final.h:76] 524917 upc_free_chk(0,536656) realPtr=0,536592/0x7fc59b3ac010 PAD=64 size=320

my @ll_perThreadResults :shared = ();
my $multithreadlog :shared = scalar(@ARGV) > 1;

my %hGlobalFree;
my %hGlobalAlloc;

my $global_last_upc_free = 0;
my $global_last_upc_free_untracked = 0;

my @lInspectedLogs :shared;
my $largestInspected :shared = -1;
my $numInspected = scalar(@ARGV);
foreach my $log (@ARGV) {
  my ($thr) = $log =~ /-(\d+).log$/;
  $lInspectedLogs[$thr] = 1;
  if ($thr > $largestInspected) { $largestInspected = $thr; }
}
my $largestObserved :shared = $largestInspected;

sub processLog {
  my $log :shared;
  my $r_msg :shared;
  my $rh_MyGlobalFree :shared;
  my $rh_MyGlobalAlloc :shared;
  ($log, $r_msg, $rh_MyGlobalFree, $rh_MyGlobalAlloc) = @_;
  my $fh;
  my $msg = "";
  my %hMyGlobalFree;
  my %hMyGlobalAlloc;

  if ($log eq '-') {
    $fh = \*STDIN;
  } else {
    $msg .= "Scanning $log\n";
    open($fh, "<", $log) || die("Could not open $log for reading! $!\n");
  }

  my %hPtrs;
  my $allocs = 0;
  my $frees = 0;
  my $upc_allocs = 0;
  my $upc_frees = 0;
  my $upc_all_allocs = 0;
  my $upc_all_frees = 0;
  my $last_upc_free = 0;
  my $last_upc_free_untracked = 0;
  my $last_free = 0;
  my $last_free_untracked = 0;
  my $lines = 0;
  while (<$fh>) {
    $lines++;
    chomp;
    if (/Starting HipMer version/) {
       # reset counters for this file as it is a re-run
       %hMyGlobalFree = ();
       %hMyGlobalAlloc = ();

       %hPtrs = ();
       $allocs = 0;
       $frees = 0;
       $upc_allocs = 0;
       $upc_frees = 0;
       $upc_all_allocs = 0;
       $upc_all_frees = 0;
       $last_upc_free = 0;
       $last_upc_free_untracked = 0;
       $last_free = 0;
       $last_free_untracked = 0;
    }
    my $free  = undef;
    my $alloc  = undef;
    my $upc_all_alloc  = undef;
    my $upc_all_free  = undef;
    my $upc_alloc  = undef;
    my $upc_free  = undef;
    my $size = 0;
    if (/malloc_chk\(\d+\) (\S+) /) {
      $alloc = $1;
    } elsif (/calloc_chk\(\d+,\s?\d+\) (\S+) /) {
      $alloc = $1;
    } elsif (/realloc_chk\(\d+\) (\S+) -> (\S+) /) {
      $free = $1;
      $alloc = $2;
      if ($free =~ /nil/) { $free = undef; }
    } elsif (/upc_all_alloc_chk\(\d+,\s*\d+\) (\S+)/) {
      $upc_all_alloc = $1;
    } elsif (/upc_all_free_chk\((\S+)\)/) {
      $upc_all_free = $1;
    } elsif (/upc_alloc_chk\(\d+\) .*realPtr=(\d+,\S+)\/\S+/) {
      $upc_alloc = $1;
      #print "Found upc_alloc_chk: $upc_alloc - $_\n";
    } elsif (/ (-?\d+) (-?\d+) upc_free_chk\(\d+,\S+\) realPtr=((\d+),\S+)\/\S+/) {
      my $threadMem = $4;
      if ($threadMem > $largestObserved) { $largestObserved = $threadMem; }
      if ($lInspectedLogs[$threadMem]) { # only track upc_free of memory from threads that are inspected
        $last_upc_free = $1;
        $last_upc_free_untracked = $2;
        $upc_free = $3;
      }
      #print "Found upc_free_chk: $threadMem $upc_free - $_\n";
    } elsif (/ (\d+) ([-]?\d+) free_chk\((\w+)\)/) {
      $last_free = $1;
      $last_free_untracked = $2;
      $free = $3;
    } 
    if (defined $free) {
      $frees++;
      if (not defined $hPtrs{$free}) {
        $msg .= "WARNING: Invalid free: $_ -- $free\n";
      } else {
        delete $hPtrs{$free};
      }
    }
    if (defined $alloc) {
      $allocs++;
      if (exists $hPtrs{$alloc}) {
        $msg .= "WARNING: alloc NotFreed: $hPtrs{$alloc}\nalloc Clobber: $_\n";
      }
      $hPtrs{$alloc} = $_;
    }
    if (defined $upc_all_alloc) {
      $upc_all_allocs++;
      if (exists $hPtrs{$upc_all_alloc}) { 
          $msg .= "WARNING: upc_all_alloc NotFreed: $hPtrs{$upc_all_alloc}\nupc_all_alloc Clobber: $_\n";
      }
      $hPtrs{$upc_all_alloc} = $_;
    }
    if (defined $upc_all_free) {
      $upc_all_frees++;
      if (not defined $hPtrs{$upc_all_free}) {
        $msg .= "WARNING: Invalid upc_all_free: $_ -- $upc_all_free\n";
      } else {
        delete $hPtrs{$upc_all_free};
      }
    }
    if (defined $upc_alloc) {
      $upc_allocs++;
      if (exists $hPtrs{$upc_alloc}) {
        if (not $multithreadlog) { $msg .= "WARNING: upc_alloc NotFreed: $hPtrs{$upc_alloc}\nupc_alloc Clobber:$_\n"; }
        push(@{$hMyGlobalAlloc{$upc_alloc}}, $hPtrs{$upc_alloc});
      }
      $hPtrs{$upc_alloc} = $_;
      #print "upc_alloc $upc_alloc => $_\n";
    }
    if (defined $upc_free) {
      $upc_frees++;
      #print "upc_free  $upc_free\n";
      if ((not defined $hPtrs{$upc_free}) || $_ =~ /NOT MY THREAD/) {
         if (not $multithreadlog) { $msg .= "WARNING: Invalid upc_free: $_ -- $upc_free\n"; }
         #print "upc_free not found: $_\n";
         push(@{$hMyGlobalFree{$upc_free}}, $_);
      } else {
         #print "upc_free  deleted $upc_free: $hPtrs{$upc_free}\n";
         delete $hPtrs{$upc_free};
      }
    }
  }

  $msg .= "Processessed $lines of $log\n";
  while( my($k,$v) = each %hPtrs) {
    if (not $multithreadlog) { $msg .= "WARNING: Not freed: $k $v\n"; }
    #print "outstanding alloc $k => $v\n";
    push(@{$hMyGlobalAlloc{$k}}, $v);
  }
  $msg .= "Allocs=$allocs Frees=$frees Outstanding=$last_free UntrackedOutstanding=$last_free_untracked\n";
  $msg .= "upcAllocs=$upc_allocs upcFrees=$upc_frees Outstanding=$last_upc_free UntrackedOutstanding=$last_upc_free_untracked\n";
  $msg .= "upcAllAllocs=$upc_all_allocs upcAllFrees=$upc_all_frees\n";
  if ($last_upc_free) { $global_last_upc_free += $last_upc_free; }
  if ($last_upc_free_untracked) { $global_last_upc_free_untracked += $last_upc_free_untracked; }
  if ($last_free) { $msg .= "WARNING: The last free_chk states $last_free outstanding\n"; }

  # add myGlobals to the multi-thread global

  # set return values
  $$r_msg = $msg;
  my $sh_k :shared;
  my $sh_v :shared;
  while ( my($k,$v) = each %hMyGlobalFree) {
    #print "myglobalfree: $k \n\t" . join("\n\t", @$v) . "\n";
    $sh_k = $k;
    my @shv :shared = ();
    $sh_v = \@shv;
    @{$sh_v} = @$v;
    ${$rh_MyGlobalFree}{$sh_k} = $sh_v;
  }
  while ( my($k,$v) = each %hMyGlobalAlloc) {
    #print "myglobalalloc: $k \n\t" . join("\n\t", @$v) . "\n";
    $sh_k = $k;
    my @shv :shared = ();
    $sh_v = \@shv;
    @{$sh_v} = @$v;
    ${$rh_MyGlobalAlloc}{$sh_k} = $sh_v;
  }

}

my $thr = 0;
my @l_procs;
foreach my $log (@ARGV) {
  my $msg :shared = "";
  my %MyGlobalFree  :shared;
  my %MyGlobalAlloc :shared;
  my @l :shared = ( \$msg, \%MyGlobalFree, \%MyGlobalAlloc );
  $ll_perThreadResults[$thr++] = \@l;
  
  push(@l_procs, threads->create('processLog', $log, @l));
  #print "Started thread $thr on $log\n";
}

$thr = 0;
foreach my $proc (@l_procs) {
  $proc->join();
  my($r_msg, $rh_MyGlobalFree, $rh_MyGlobalAlloc) = @{$ll_perThreadResults[$thr++]}; 
  #print "Thread $thr\n";
  #print "msg: " . $$r_msg;
  while ( my($k,$v) = each %{$rh_MyGlobalFree}) {
    #print "global free $k " . join ("\t", @$v) . "\n";
    push(@{$hGlobalFree{$k}}, @$v);
  }
  while ( my($k,$v) = each %{$rh_MyGlobalAlloc}) {
    #print "global alloc $k " . join("\t", @$v) . "\n";
    push(@{$hGlobalAlloc{$k}}, @$v);
  }

}

if ($largestInspected == $largestObserved && $numInspected == $largestObserved - 1) {
  warn("Not all threads had logs that were inspected, so global allocations are not going to be output: numLogs=$numInspected largest=$largestObserved\n");
  exit();
}

while ( my($k,$v) = each %hGlobalFree) {
  my $count = 0;
  foreach my $vv (@$v) {
    #print "testing free of $k - $vv\n";
    if (exists $hGlobalAlloc{$k}) {
        my $a = shift @{$hGlobalAlloc{$k}};
        #print "global free of $k -  $a\n";
        if (scalar(@{$hGlobalAlloc{$k}}) == 0) {
            delete $hGlobalAlloc{$k};
            #print "global free of all at $k\n";
        }
    } else {
        $count++;
    }
  }
  if ($count > 0) {
      print "Global Free $k\n\t" . join("\n\t", @$v) . "\n";
  }
}

while ( my($k,$v) = each %hGlobalAlloc) {
    print "Global Alloc $k\n\t" . join("\n\t", @$v) . "\n";
}
if ($global_last_upc_free != 0 || $global_last_upc_free_untracked != 0) {
  print "UPC globally: upc_last_free=$global_last_upc_free upc_last_free_untracked=$global_last_upc_free_untracked\n";
}

exit(0);

