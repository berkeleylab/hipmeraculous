#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <upc.h>

#include "../../src/hipmer/common/upc_common.h"
#include "../../src/hipmer/common/common.h"

#define setup(test1, range, size, value) do { \
   LOG("setup(%s, %s, %s, %s)\n", #test1, #range, #size, #value); \
   int64_t bytes = range*size; \
   if (bytes % 8 != 0) bytes += 8 - bytes % 8; \
   UPC_ALLOC_CHK(test1, bytes); \
   for(int i = 0; i < range; i++) { \
       memcpy( (char*) &test1[i], &value, size); \
   } \
   UPC_VALIDATE_PTR(test1); \
   for(int bufPos = 0; bufPos < UPC_BUFFERING_ALLOC; bufPos++) { \
      SharedLongPtr bufAlloc = (SharedLongPtr) test1; \
      if (bufAlloc[bufPos - UPC_BUFFERING_ALLOC] != bytes) WARN("Yikes! %d %d (expect %lld got %lld) setup(%s,%s,%d,%s)\n", -bufPos, range, (lld) bytes, (lld) bufAlloc[bufPos - UPC_BUFFERING_ALLOC],  #test1, #range, (int) size, #value );  \
      if (bufAlloc[(bytes / 8) + UPC_BUFFERING_ALLOC - bufPos - 1] != bytes) WARN("Yikes2! %d %d (expect %lld got %lld) setup(%s,%s,%d,%s)\n", bufPos, range, (lld) bytes, (lld) bufAlloc[(bytes / 8) + UPC_BUFFERING_ALLOC - bufPos - 1],  #test1, #range, (int) size, #value );  \
   } \
} while(0)

#define setupall(test1, range, bytes, value) do { \
   LOG("setupall(%s, %s, %s, %s)\n", #test1, #range, #bytes, #value); \
   UPC_ALL_ALLOC_CHK(test1, range, bytes); \
   for(int i = MYTHREAD; i < range; i+=THREADS) { \
       memcpy( (char*) &test1[i], &value, size); \
   } \
   upc_barrier; \
   /*UPC_VALIDATE_ALL_PTR(test1);*/ \
} while(0)

#define testA(TYPE, range, value) do { \
   LOG("Performing testA(%s, %s, %s)\n", #TYPE, #range, #value ); \
   shared [] TYPE *test1, *test2, *test3; \
   int64_t size = sizeof(TYPE); \
   int64_t bytes = range * size; \
   setup(test1, range, size, value); \
   setup(test2, range, size, value); \
   setup(test3, range, size, value); \
   UPC_FREE_CHK(test2); \
   if (test2 != NULL) WARN("Yikes4! test2!=NULL\n"); \
   upc_barrier; \
   setup(test2, range, size, value); \
   UPC_FREE_CHK(test1); \
   UPC_FREE_CHK(test2); \
   UPC_FREE_CHK(test3); \
   if (test1 != NULL) WARN("Yikes5! test1!=NULL\n"); \
   if (test2 != NULL) WARN("Yikes6 test2!=NULL (2)\n"); \
   if (test3 != NULL) WARN("Yikes7! test3!=NULL\n"); \
   shared[1] TYPE *atest1, *atest2, *atest3; \
   setupall(atest1, range, size, value); \
   setupall(atest2, range, size, value); \
   setupall(atest3, range, size, value); \
   UPC_ALL_FREE_CHK(atest2); \
   if (atest2 != NULL) WARN("Yikes8! atest2 != NULL\n"); \
   setupall(atest2, range, size, value); \
   UPC_ALL_FREE_CHK(atest1); \
   UPC_ALL_FREE_CHK(atest2); \
   UPC_ALL_FREE_CHK(atest3); \
} while (0)

typedef struct _x {
     int64_t a, b, c;
   } X;

FILE *myLog = NULL;
int main(int argc, char **argv) {
   
   char fpath[256];
   sprintf(fpath, "test_upc_mem.log");
   myLog = fopen_rank_path(fpath, "w", MYTHREAD);
   LOG("Hello from %dof%d\n", MYTHREAD, THREADS);

   int64_t v = 666;
   testA(int64_t, 3342, v);
   int32_t v1 = 663;
   testA(int32_t, 3343, v1);
   char v2 = 83;
   testA(char, 3347, v2);

   LOG("testing struct with size: %d\n", sizeof(X));
   X x;
   x.a=666;
   x.b=-666;
   x.c=6666;

   testA(X, 3234, x);

   upc_barrier;
   LOG("Done\n");
  
   return 0;
}
