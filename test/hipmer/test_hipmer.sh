#!/bin/bash

echo "+ Executing $0 on $(uname -n) at $(date)"
env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\|COBALT\|LSB\)'

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "+ Discovered base path of $DIR"

if [[ "${PATH}" != "${PATH/${DIR}/}" ]]
then
    export PATH=${DIR}:${PATH}
fi

hipmer_install=${DIR%/*}
if [ -z "${HIPMER_ENV}" ] && [ -f "${hipmer_install}/env.sh" ]
then
    echo "+ Sourcing HipMer Environment: ${hipmer_install}/env.sh"
    source ${hipmer_install}/env.sh
fi

if [ -z "$HIPMER_INSTALL" ]
then
    HIPMER_INSTALL=${DIR%/*}
fi

# first parse any ENV=val command line arguments
job_env_args=()
for arg in "$@"
do
  if [ "${arg/=}" != "${arg}" ]
  then
    job_env_args+=("export ${arg};")
    setenvlog="$setenvlog
Setenv $arg"
    eval "export ${arg%=*}='${arg##*=}'"
    shift
  else
    break
  fi
done

if [ -z "${HIPMER_BOOTSTRAPPED}" ]
then
    source ${DIR}/bootstrap_hipmer_env.sh
fi

# call self recursively if multiple tests are requested
if [ $# -gt 1 ]
then
  export HIPMER_NO_RESIZE_FOR_STAGE=1
  set -e
  for arg in $@
  do
    $0 $arg
  done
  exit
fi

if [ -n "${1}" ]
then
    export HIPMER_TEST=${1}
fi
if [ -z "${HIPMER_TEST}" ]
then
    export HIPMER_TEST=validation
fi
export HIPMER_BOOTSTRAP_FUNC=test_${HIPMER_TEST}

export SKIP_LOG=1

HIPMER_CONFIG=${HIPMER_CONFIG:=${HIPMER_INSTALL}/etc/meraculous/pipeline/${HIPMER_TEST}.config}
HIPMER_DATA_DIR=${HIPMER_DATA_DIR:=${SCRATCH}/hipmer_${HIPMER_TEST}_data/}

USAGE="USAGE: $0 [HIPMER_TEST(=${HIPMER_TEST})]
Where the following environmental variables are set:

HIPMER_INSTALL=${HIPMER_INSTALL} or HIPMER_ENV_SCRIPT=${HIPMER_ENV_SCRIPT}

and valid tests packaged with HipMer are:
  validation (200kb of a difficult region of e-coli)
  ecoli
  chr14 (human chr14 diploid)
  human (the full diploid)
  mg250 (a subsample of a mock metagenome)
  mock150 (a larger mock metagenome)
  chr14-benchmark (1 library chr14)
  human-benchmark (1 library human)

The following environmental variables are set automatically based on the HIPMER_TEST specified

HIPMER_CONFIG=${HIPMER_CONFIG}
HIPMER_DATA_DIR=${HIPMER_DATA_DIR}

"


JID=
if [ "$SLURM_JOB_ID" ]; then 
    JID=j${SLURM_JOB_ID}-
elif [ "$PBS_JOBID" ]; then
    JID=j${PBS_JOBID}-
elif [ "$COBALT_JOBID" ]; then
    JID=j${COBALT_JOBID}-
elif [ "${LSB_JOBID}" ]; then
    JID=j${LSB_JOBID}-
fi

echo "+ Started at $(date) in $SECONDS s: $0 $@ (${JID} pid:$$)"

if ( [ -n "${CHECK_FREE_HUGEPAGES_MB}" ] &&  [ "${CHECK_FREE_HUGEPAGES_MB}" != "0" ] ) || [ -n "$MIN_NODES" ] || [ -n "$MAX_NODES" ]
then
    # test the nodes for enough hugepage memory and kick out a few if necessary
    if [ -z "$MIN_NODES" ]
    then
        if [ $SLURM_JOB_NUM_NODES -gt ${MIN_NODES_FOR_HUGEPAGE_CHECK:=4} ]
        then
            MIN_NODES=$((3*SLURM_JOB_NUM_NODES/4))
        else
            MIN_NODES=${SLURM_JOB_NUM_NODES}
        fi
    fi
    if [ -n "${SLURM_JOB_ID}" ] && [ -f /proc/buddyinfo ]
    then
        if [ ! -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            check_hugepage_and_reduce_slurm_job.sh MIN_NODES=${MIN_NODES} || echo "Failed to check hugepages.  Continuing"
        fi
        if [ -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            echo "+ Adjusting job environment: source $TMPDIR/new_slurm_env.SLURM_JOB_ID"
            set -x
            source $TMPDIR/new_slurm_env.$SLURM_JOB_ID
            set +x
        else
            echo "+ There is no file to adjust the environment: $TMPDIR/new_slurm_env.$SLURM_JOB_ID"
        fi
    fi
    export CHECK_FREE_HUGEPAGES_MB=0
fi

RUN_OPTS="${RUN_OPTS:=}"

# autodetect threads in job in slurm
if [ "$SLURM_NTASKS" ]; then
  JOB_THREADS=$SLURM_NTASKS
else
  nodes=1
  if [ -n "$SLURM_JOB_NUM_NODES" ]
  then
      nodes=$SLURM_JOB_NUM_NODES
  fi
  JOB_THREADS=$(( $(getcores) * $nodes ))
fi

export RUNDIR=${RUNDIR:=$SCRATCH/${HIPMER_TEST}-${JOB_THREADS}-${JID}-$$_$(date '+%Y%m%d_%H%M%S')}
mkdir -p $RUNDIR

echo "+ Linking latest_run to output in $RUNDIR"
rm -f latest_run
ln -s $RUNDIR latest_run

set -e

rmdir ${HIPMER_DATA_DIR} 2>/dev/null || true # if it is empty remove it
if [ ! -d ${HIPMER_DATA_DIR} ] || [ ${HIPMER_TEST} == "validation" ] # validation and configs are with the install
then
    echo "+ Attempting to copy/download the data for ${HIPMER_TEST}"
    $HIPMER_INSTALL/bin/hipmer_setup_${HIPMER_TEST%-*}_data.sh ${RUNDIR}
fi

STARTDIR=$(pwd)

OLD_KEEP_TEST_DIR=${KEEP_TEST_DIR}
if [ "${HIPMER_TEST##validation}" != "${HIPMER_TEST}" ] || [ "${HIPMER_TEST##ecoli}" != "${HIPMER_TEST}" ]
then
  export CANONICALIZE=${CANONICALIZE:=1}
  echo "+ Defaulting to checkpoint and not cleanup the test directory in order to validate checksums"
  export KEEP_TEST_DIR=${KEEP_TEST_DIR:=1}
  export CHECKPOINT=${CHECKPOINT:=1}
fi

if [ "${HIPMER_BUILD_TYPE}" == "Debug" ] && [ "${KEEP_TEST_DIR}" == "1" ] && [ "${HIPMER_TEST##validation}" != "${HIPMER_TEST}" ]
then
  CHECK_LOGS=${CHECK_LOGS:=1}
fi

#checksums=${HIPMER_INSTALL}/bin/${HIPMER_TEST%-*}_checksums.sh
#[ ! -f $checksums ] || source $checksums

cd $RUNDIR
if [ -d "${HIPMER_DATA_DIR}" ] && [  "${HIPMER_TEST}" != "validation" ] # validation data is stored with the install
then
  for i in `ls ${HIPMER_DATA_DIR}/*.{fq,fastq,hmm}`; do 
    [ -f "${i##*/}" ] || ln -s $i . 
  done
fi

# set some hipmer.py options from the environment
[ "$CHECKPOINT" != "1" ] || RUN_OPTS="--checkpoint ${RUN_OPTS}"
[ "$TEST_CHECKPOINTING" != "1" ] || RUN_OPTS="--test-checkpointing --checkpoint ${RUN_OPTS}"
[ "$TEST_CHECKPOINTING" != "1" ] || export SAVE_INTERMEDIATES=1
[ "$KEEP_TEST_DIR" != "1" ] || RUN_OPTS="--cleanup=False ${RUN_OPTS}"
[ "$VERBOSE" != "1" ] || RUN_OPTS="-v ${RUN_OPTS}"
[ "$UPC_VERBOSE" != "1" ] || RUN_OPTS="--upc-verbose ${RUN_OPTS}"
[ "${CACHED_IO}" == "" ] || RUN_OPTS="--cached-io=${CACHED_IO} ${RUN_OPTS}"
[ "${AUTORESTART}" == "1" ] || RUN_OPTS="--auto-restart=False ${RUN_OPTS}"
[ "${SAVE_INTERMEDIATES}" != "1" ] || RUN_OPTS="--cleanup=False ${RUN_OPTS}"

echo "+ applying RUN_OPTS=${RUN_OPTS}"

hipmer_opts=
shortconfig=${HIPMER_CONFIG##*/}
if [ "${shortconfig}" == "validation.config" ] || [ "${shortconfig}" == "validation-multiround.config" ] 
then

   export ufx_md5sum="c3e84bfdeb7ba8500e968cb51faec665,ba0dbbb76bf07a6311e9c42b3d9b357f" # merged-reads==0/2, merge-reads==1
   export ufx_md5sum_old="64de2b1ecb843c4244fc555e00ccd68a,72e2d67620a17962f1f45f1d6b776cb0" # old UFX
   export contigs_md5sum="18fa925de263d6576ae713d33dd45ead,430b59ac902ddf22906ebf58aed3c66a,7930e88b09bc3c644e3d3d15c15c8613,e012c5c9e7de0d5fce2da6ecd23e6bab,681e63d7951e7dc300a649af9020fea2"  # for unmerged and merged reads
   export assembly_md5sum="120059e0a3607697921f93dffdd2789a,63708fc9d2142b6354e42aa431369fdd" 
   export uc_assem_md5sum="120059e0a3607697921f93dffdd2789a,63708fc9d2142b6354e42aa431369fdd" 
   # RSE - these are old and may not be valid. The above are from on my machine 10/29/2018
   #export assembly_md5sum="4da1ff76b3b94eb45b507b74a013bd68,b0e4991824d0861d7109f9576ce08a18,730053f3aa996f0e112385db535c3cbf,bb4ad7834d7f6c0fbfe27f35676e61dc" 
   #export uc_assem_md5sum="120059e0a3607697921f93dffdd2789a,63708fc9d2142b6354e42aa431369fdd" 
   RUN_ARGS="-k 21 --interleaved frags.25K.fastq:i180:s10 --interleaved jumps.25K.fastq:rc:i3000:s500 --min-depth 7 --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "validation-mg.config" ]
then

  export CANONICALIZE=1
  export ufx_md5sum="879634a7c0d8906a5911f7101310b357,a426064f18fc2d8a37953afae42b47ec" # merge-reads==0/2 merge-reads==1
  export ufx_md5sum_old="c3e84bfdeb7ba8500e968cb51faec665,ba0dbbb76bf07a6311e9c42b3d9b357f" # merged-reads==0/2 merge-reads==1
  export contigs_md5sum="b5c3d4c87ceb6d485f01eb4bd715ac61,e55fb015a57ad8d95120980b25195e0e,68a3f982e89075a1624bf8b8a9c96c79,9e5666d60896aa7230a31fb835f32b2c,01659bbc042dec2f1b04ab0fd082db86,aa4fef6aff31f076a6e1145445b07356,7fddf7c3fe4a2311a4eba4f825a1465d,b026d05f10a563b697b746d36b901a49,4394e24f954cd8609ecba541b3b9d415,441ebac60f57c852fb9a2fa6d491597d,4654e04c9975dd633df5764b26698487,1b389f922bda223979eb4c885bfe9ec5,b8c11b929bd15ac4e9a054fa13691264,aa6a3cc04677af3a0b47501a089305d2"
#  export contigs_md5sum="f142b7052793afcf32d67588bffcfda9"
  RUN_ARGS="-k 21,27 --meta --interleaved frags.25K.fastq --interleaved jumps.25K.fastq:rc:i3000:s500 --min-depth 7 --localize-reads --canonicalize_output ${hipmer_opts}"


elif [ "${shortconfig}" == "validation-mg27.config" ]
then
  RUN_ARGS="-k 27 --meta --interleaved frags.25K.fastq --interleaved jumps.25K.fastq:rc:i3000:s500 --min-depth 7 --localize-reads --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "validation-mg3.config" ]
then

  export ufx_md5sum="879634a7c0d8906a5911f7101310b357,a426064f18fc2d8a37953afae42b47ec" # merge-reads==0/2 merge-reads==1
  RUN_ARGS="-k 21,27,31 --meta --interleaved frags.25K.fastq --interleaved jumps.25K.fastq:rc:i3000:s500 --min-depth 7 --localize-reads --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "validation2D-diploid.config" ]
then

   export contigs_md5sum="c83a6e324883688db75e002045472ada,b01af9da289c1539fc0f897fc74d46c1,c20021810470983a47af53bd4caa10f2,24a04003b84e5f60365fbdf1416790e6,7c9c3967116408a71f87def1b8d4abe7,24859460ade220879afa251384e656ac,f97b17bedcdc114230cc51b0d854b88b,79f7ceeaa9e240d6fa88029d273e4d00"
# FIXME!!!! validatino2D-diploid does not always work now
#   export assembly_md5sum="ccbf3e7a93f587021e200cff8074394b,b778c42647c2ae35f4551cc74a61a196,2df452ad299cd3ad5fcb12f720f3d33d,d1e1bf3052bfe34bded5e70277308a04,da6ea6b7d764234e18439b7c11ee5b1d"
# FRAGFOO and FRAGBAR are used for splints and spans in the first round
   RUN_ARGS="-k 51 --diploidy high --bubble-depth-cutoff 187 --ono-pair-thresholds 5,10 --min-depth 0 --dynamic-min-depth 0.9 \
             --canonicalize_output ${hipmer_opts} \
             --interleaved frags_haplotype0.fastq:i300:s50 \
             --interleaved frags_haplotype0.02.fastq:i300:s50 \
             --interleaved jumps_haplotype0.fastq:nosplint:nogapclosing:i3250:s250 \
             --interleaved jumps_haplotype0.02.fastq:nosplint:nogapclosing:i3250:s250 \
             --merge-reads=0
           " 

elif [ "${shortconfig}" == "ecoli.config" ] || [ "${shortconfig}" == "ecoli-multiround.config" ]
then

   export CANONICALIZE=1
   export ufx_md5sum="5697ee5e3e590409529572258f8e86cf,91ad0782051504bc12546a2ad1badb09" # --merge-reads==0/2, --merge-reads==1
   export ufx_md5sum_old="795d08b689994f2002e0bee857cb5cab"
   export contigs_md5sum="92fd9c6f5258fd9b14e21e6430ef1946,feb2b57ce042abb0a8c94134bbb066c1,e403dc999c393e1147dfa416abc52916,7d9f55c9e9d702bf18db74ed4afbb0a3,68264751365bdad87c19ac7edd1008a0" # merge-reads==0/2 and merge-reads==1
   RUN_ARGS="-k 21 --paired ECO.fastq.0.fq,ECO.fastq.1.fq --min-depth 7 --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "ecoli-mg.config" ] 
then
   
   RUN_ARGS="-k 21,25,27 --meta --paired ECO.fastq.0.fq,ECO.fastq.1.fq:i210:s21 --min-depth 7 --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "ecoli-mg_single.config" ] 
then
   
   RUN_ARGS="-k 21,25 --meta --single ECO.fastq.0.fq --min-depth 7 ${hipmer_opts}"

elif [ "${shortconfig}" == "ecoli-single.config" ] 
then
   
   RUN_ARGS="-k 21 --single ECO.fastq.0.fq --min-depth 7 ${hipmer_opts}"

elif [ "${shortconfig}" == "chr14-benchmark.config" ]
then

  RUN_ARGS="-k 51 --diploidy low --min-depth 4 --bubble-depth-cutoff 1 --benchmark \
            -p frag_1.fastq,frag_2.fastq \
            ${hipmer_opts}"

elif [ "${shortconfig}" == "chr14.config" ]
then

  RUN_ARGS="-k 51 --diploidy low --min-depth 4 --bubble-depth-cutoff 1 \
            -p frag_1.fastq,frag_2.fastq ${hipmer_opts} \
            -i shortjump.fastq:i2543:s254:rc:nocontig:fivewiggle0:threewiggle25 \
            -i longjump.fastq:i35307:s3531:nocontig:fivewiggle45:threewiggle15:nosplint \
            ${hipmer_opts}"

elif [ "${shortconfig}" == "human-benchmark.config" ]
then

  RUN_ARGS="-k 51 --diploidy low --min-depth 4 --bubble-depth-cutoff 1 --benchmark \
            -p s_1_1_sequence.fastq,s_1_2_sequence.fastq \
            ${hipmer_opts}"

elif [ "${shortconfig}" == "human.config" ]
then

  RUN_ARGS="-k 51 --diploidy low --min-depth 4 --bubble-depth-cutoff 1 \
            --merge-reads=1 \
            -p s_1_1_sequence.fastq,s_1_2_sequence.fastq \
            -i jmp1.fastq:i2283:s221:rc:nocontig:nogapclosing:threewiggle25 \
            -i jmp2.fastq:i2803:s271:rc:nocontig:nogapclosing:threewiggle25 \
            -i fos1.fastq:i35295:s2703:nocontig:nogapclosing:fivewiggle45:threewiggle15:nosplint \
            -i fos2.fastq:i35318:s2759:nocontig:nogapclosing:fivewiggle45:threewiggle15:nosplint \
            ${hipmer_opts}"

elif [ "${shortconfig}" == "mock150.config" ]
then

  RUN_ARGS="-k 21,33,55,77 --meta --merge-reads=1 --localize-reads \
            --interleaved mock.150.bbqc.fastq:i275:s30 \
            --canonicalize_output ${hipmer_opts}"

elif [ "${shortconfig}" == "mg250.config" ]
then

   RUN_ARGS="-k 21,41,63,95,127 --meta \
             --interleaved mock2pct.250.bbqc.sampled.fastq:i400:s20 \
             ${hipmer_opts}"

elif [ "${shortconfig}" == "mg250-short.config" ]
then

   RUN_ARGS="-k 21,45,63 --meta \
             --interleaved mock2pct.250.bbqc.sampled.fastq:i400:s20 \
             ${hipmer_opts}"

else
   unset contigs_md5sum
   unset assembly_md5sum
   unset uc_assem_md5sum

   if ( [ "$USE_DEPRECATED" != "1" ] && [ -z "$RUN_ARGS" ] ) || ( [ "$USE_DEPRECATED" == "1" ] && [ ! -f "$HIPMER_CONFIG" ] )
   then
       echo "$USAGE"
       echo
       echo "Do not know how to test '${HIPMER_TEST}"
       echo
       exit 1
    fi
fi

if [ -n "${ufx_md5sum}${contigs_md5sum}${assembly_md5sum}${uc_assem_md5sum}" ]
then
  export HIPMER_POSTRUN=${HIPMER_INSTALL}/bin/check_results.sh
  export CANONICALIZE=1

  echo "+ Will check results for proper checksums: 

   ufx_md5sum=$ufx_md5sum
   contigs_md5sum=$contigs_md5sum
   assembly_md5sum=$assembly_md5sum
   uc_assem_md5sum=$uc_assem_md5sum

"
fi

[ ! -f "$HIPMER_CONFIG" ] || cp -p $HIPMER_CONFIG .
if [ -n "${HIPMER_CONFIG_SED}" ]
then
  HIPMER_ORIG_CONFIG=${HIPMER_CONFIG##*/}
  HIPMER_CONFIG=${HIPMER_ORIG_CONFIG}.$(echo "${HIPMER_CONFIG_SED}" | cksum - | awk '{print $1}')
  sed "${HIPMER_CONFIG_SED}" ${HIPMER_ORIG_CONFIG} > ${HIPMER_CONFIG}
  echo "+ Generated new config by applying sed '${HIPMER_CONFIG_SED}' to ${HIPMER_ORIG_CONFIG}"
fi
echo "+ Using HIPMER_CONFIG=${HIPMER_CONFIG}"

export PATH=${HIPMER_INSTALL}/bin:${PATH}

# by default use the deprecated run_hipmer.sh script until all tests have been migrated
ret=0
USE_DEPRECATED=${USE_DEPRECATED:=0}
if [ -z "${RUN_ARGS}" ] || [ "$USE_DEPRECATED" == "1" ]
then
    # THREADS is necessary for legacy run_hipmer.sh
    RUNSCRIPT=${RUNSCRIPT:=run_hipmer.sh}
    THREADS=$JOB_THREADS ${HIPMER_INSTALL}/bin/${RUNSCRIPT} $(basename $HIPMER_CONFIG)
    ret=$?
else
    RUNSCRIPT=${RUNSCRIPT:=hipmer}
    ${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} ${RUN_ARGS} ${RUN_OPTS}
    ret=$?
fi

echo "+ runscript returned $ret"

EXIT_VAL=$ret

RUN_NAME=$(basename $RUNDIR)
# keep the old run.out and final_assembly.fa, but also keep a renamed hard link
if [ -f run.out ]
then
    ln -f run.out run-${RUN_NAME}.out
else
    ln -f run.log run-${RUN_NAME}.log
fi
ln -f results/final_assembly.fa results/final_assembly-${RUN_NAME}.fa

if [ "$TEST_CHECKPOINTING_EXTENDED" == "1" ] && [ "$USE_DEPRECATED" != "1" ] && [ -n "${RUN_ARGS}" ]
then

    [ -z "$HIPMER_POSTRUN" ] || $HIPMER_POSTRUN
    echo "=================================================="
    echo "Test checkpointing extended from all stages forward and then reverse"
    echo "=================================================="

    # over-ride any other options that may conflict with an extended test
    EXTENDED_OPTS="--checkpoint --cached-io --auto-restart=False" 
    # test rerun from the beginning with checkpoint on , cached_io on and auto-restart off
    ${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} ${RUN_ARGS} ${RUN_OPTS} ${EXTENDED_OPTS} --resume=False

    num_stages=$( (${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} --list-stages || true) | grep '^[0-9].*: ' | wc -l )
    stages="$(seq 0 $((num_stages - 1)))"

    echo "=================================================="
    echo "Test checkpointing extended -- full resume=False run succeeded with $num_stages stages: " $stages
    echo "=================================================="

    ${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} --list-stages  || true
    [ -z "$HIPMER_POSTRUN" ] || $HIPMER_POSTRUN

    for stage in $stages
    do
        echo "=================================================="
        echo "Restarting start to end stage $stage at $(date)"
        echo "=================================================="
        rm -rf /dev/shm/per_rank
        ${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} ${RUN_ARGS} ${RUN_OPTS} ${EXTENDED_OPTS} --restart-stage=$stage
        [ -z "$HIPMER_POSTRUN" ] || $HIPMER_POSTRUN
    done
    stages="$(seq $((num_stages - 1)) -1 0)"
    for stage in $stages
    do
        echo "=================================================="
        echo "Restarting end to start stage $stage at $(date)"
        echo "=================================================="
        rm -rf /dev/shm/per_rank
        ${HIPMER_INSTALL}/bin/${RUNSCRIPT} -o ${RUNDIR} ${RUN_ARGS} ${RUN_OPTS} ${EXTENDED_OPTS} --restart-stage=$stage
        [ -z "$HIPMER_POSTRUN" ] || $HIPMER_POSTRUN
    done

fi

postrunpid=
if [ "$HIPMER_POSTRUN" ] && [ ${ret} -eq 0 ]
then
  echo "+ Executing PostRun Script $(date): ${HIPMER_POSTRUN}"
  $HIPMER_POSTRUN &
  postrunpid=$!
  echo "+ postrunpid=$postrunpid"
fi

checklogspid=
if [ "${CHECK_LOGS}" == "1" ]
then
    echo "+ Checking logs for memory leaks"
  (
    memchecklog=$(pwd)/memcheck.log
    summary=${memchecklog%.log}-summary.log
    $(which memcheckLog.pl) $(pwd)/per_rank/*/*/HipMer-*.log | tee $memchecklog | sed 's/.*\[/\[/;' | awk '/\[/ {print $1} ' |sort | uniq -c > $summary
    if [ -s "${summary}" ]
    then
       echo -e "\n\033[1;31m>>>>>  WARNING there may be $(wc -l ${summary}) different memory leaks! $(date) <<<<<<\n\t\tsee: ${summary}\033[0;0m\n"
    else
       echo -e "\x1B[92m No memory leaks found \x1B[0m"
    fi
  ) &
  checklogpid=$!
  echo "+ checklogpid=$checklogpid"
fi

if [ -n "$postrunpid" ]
then
  echo "+ Waiting for HIPMER_POSTRUN to complete $(date)"
  if ! wait $postrunpid
  then
       EXIT_VAL=1
       echo -e "\n\033[1;31m>>>>>  WARNING postrun failed $(date): $HIPMER_POSTRUN <<<<<<\033[0;0m\\n"
  fi
fi

if [ -n "$checklogpid" ]
then
  echo "+ Waiting for checklogs to complete $(date)"
  if ! wait $checklogpid
  then
       EXIT_VAL=1
       echo -e "\n\033[1;31m>>>>>  WARNING checklogs failed $(date) <<<<<<\033[0;0m\\n"
  fi
fi

if [ "$EXIT_VAL" == "0" ] && [ "$OLD_KEEP_TEST_DIR" != "1" ]
then
  cleanuppids=
  echo "+ Success! Removing intermediates, and per_rank at $(date) in the background"
  ( [ ! -f run.log ] || gzip -1 -f run.log ; [ ! -f run.out ] || gzip -1 -f run.out ) &
  cleanuppids="$cleanuppids $!"
  rm -rf intermediates &
  cleanuppids="$cleanuppids $!"
  rm -rf per_rank/*/*[1-9] &
  cleanuppids="$cleanuppids $!"
  l=per_rank/00000000/00000000/HipMer-0.log
  if [ -f "$l" ] 
  then
     ( gzip -1 -f -c $l > HipMer-0.log.gz && rm -rf per_rank/*/*0 ) &
     cleanuppids="$cleanuppids $!"
  fi
  if [ "${HIPMER_TEST##validation}" != "${HIPMER_TEST}" ]
  then
     echo "+ Removing validation fastq files"
     rm -f jumps.25K.fastq frags.25K.fastq frags_haplotype0.02.fastq  frags_haplotype0.fastq  jumps_haplotype0.02.fastq  jumps_haplotype0.fastq &
     cleanuppids="$cleanuppids $!"
  fi
  echo "+ Waiting for cleanup pids: $cleanuppids to complete $(date)"
  wait $cleanuppids || EXIT_VAL=$?
fi

echo "+ Finished at $(date) in $SECONDS s: $0 $@ (${JID}pid:$$)"

cd $STARTDIR

exit $EXIT_VAL
