#include <stdio.h>
#include <unistd.h>
#include "mpi.h"

int rank() {
   int rank;
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   return rank;
}

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);
  char name[255];
  gethostname(name, 255);
  printf("Rank %d on %s\n", rank(), name);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
