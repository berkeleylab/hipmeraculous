#include <stddef.h>
#include <stdio.h>

#include <bupc_extern.h>

int main_main(int argc, char **argv)
{
    int rank;
    if (rank % 1000 == 0) {
        fprintf(stdout, "Th%d started\n", rank);
        fflush(stdout);
    }
    return 0;
}

int main(int argc, char **argv)
{
    int rank;
    if (rank == 0) {
        fprintf(stdout, "Called MPI_Init()\n"); 
        fprintf(stdout, "Calling bupc_init_reentrant() for main_main\n"); 
        fflush(stdout);
    }
    bupc_init_reentrant(&argc, &argv, &main_main);
    if (rank == 0) {
        fprintf(stdout, "Finished bupc_init_reentrant\n"); 
        fflush(stdout);
    }
    bupc_exit(0);
    // Probably not reached.  MPI_Finalize() should be called by bupc_exit....
    if (rank == 0) {
      fprintf(stdout, "main_main returned\n"); 
      fflush(stdout);
    }

    if (rank == 0) {
        fprintf(stdout, "Calling MPI_Finalize()\n"); 
        fflush(stdout);
    }
    return 0;
}
