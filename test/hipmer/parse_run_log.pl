#!/usr/bin/env perl

use strict;
use warnings;

use File::stat;

our %stats;
our @modules = qw {LoadFQ Merge_Reads UFX Meraculous merAligner LocalAssm oNo ParCC GapClosing Localize FindNewMers PrefixMerDepth ProgressiveRelativeDepth ContigMerDepth ContigEndAnalyzer CGraph};

# allow many names to map to a standard module
our %module_map;
foreach my $m (@modules) {
    $module_map{$m} = $m;
}
$module_map{'kcount'} = 'UFX';

our @metrics = qw {Date Operator DataSetName GBofFASTQ NumReads MinDepth ContigKmers DistinctKmersWithFP MinDepthKmers CachedIO Version HipmerWorkflow Nodes Threads CoresPerNode NumRestarts ManualRestarts TotalTime NodeHours CalculatedTotalTime NumStages StageTime };
our @fields = (@metrics, @modules, "RunDir", "RunOptions");
foreach my $module (@modules) {
    $stats{$module} = 0;
}


sub printStats {

   print join(";", @fields) . "\n";
   print join(";", @stats{@fields}) . "\n";
}

$stats{"Operator"} = $ENV{"USER"};
my ($diploid, $zygo, $cgraph, $meta);
$stats{"NumRestarts"} = 0;
$stats{"ManualRestarts"} = 0;
our @libs = ();

my $knowGB = 0;
my $in_options = 1;
my $firstUFX = 1;
while (<>) {
    s/[\000-\037]\[(\d|;)+m//g; # remove any control characters from the log
    if (/^Total physical/) { $in_options = 0; }
    if ($in_options && / (\d+\.\d\d) GB /) {
      $knowGB += $1;
    }
    if (/^#  Libs:\s+/) {
        s/#  Libs:\s+//;
        chomp;
        @libs = split(/\s+/);
    }
    if ((not defined $stats{"RunOptions"}) && /Starting hipmer, executed as: (.+)/) {
        $stats{"RunOptions"} = $1;
    }
    if ((not defined $stats{"RunOptions"}) && /Executed as: (.+)/) {
        $stats{"RunOptions"} = $1;
    }
    if (/# Starting HipMer version (\S+) .* on (\d+) threads/) {
        $stats{"Version"} = $1;
        $stats{"Threads"} = $2;
        $stats{"Version"} =~ s/-\d+_\d+//;
        $stats{"Version"} =~ s/-dirty//;
    }
    if (/RESTARTING at stage/ || /\*\*\* RESTARTING \*\*\*/ || /Attempting to restart from stage/) {
        $stats{"NumRestarts"}++;
    }
    if (/Cannot restart: stage/ || /FAILED run at/) {
        $stats{"ManualRestarts"}++;
    }
    if (/#  config_file:\s+(\S+)/) {
        $stats{"Config"} = $1;
    }
    if (/#  mer_sizes:\s+(\d+.*)/) {
        $stats{"ContigKmers"} = $1;
        $stats{"ContigKmers"} =~ s/ *$//;
        $stats{"ContigKmers"} =~ s/ /-/g;
    }
    if (/# Current directory is (\S++)/) {
        $stats{"RunDir"} = $1;
        if ( -d $stats{"RunDir"} ) { # get the user too
           my $uid = stat($stats{"RunDir"})->uid;
           $stats{"Operator"} = getpwuid($uid);
        }
    }
    if (/ use cgraph\s+(\S+)/) {
        $cgraph = ($1 eq "True") ? 1 : 0;
    }
    if (/# cgraph will now continue to perform the scaffolding/) {
        $cgraph = 1;
    }
    if (/#  cores_per_node:\s+(\d+)/) {
        $stats{"CoresPerNode"} = $1;
    }
    if (/#  nodes:\s+(\d+)/) {
        $stats{"Nodes"} = $1;
    }
    if (/#  min_depth_cutoff:\s+(\d+)/) {
        $stats{"MinDepth"} = $1;
    }
    if (/#  use cached IO:\s+(\S+)/) {
        $stats{"CachedIO"} = $1;
    }
    if (/#  high_heterozygosity:\+(\S+)/) {
        $zygo = ($1 =~ /true/i) ? 1 : 0;
    }
    if (/#  is_diploid:\s+(\S+)/) {
        $diploid = ($1 =~ /true/i) ? 1 : 0;
    }
    if (/#  is_metagenome:\s+(\S+)/) {
        $meta = ($1 =~ /true/i) ? 1 : 0;
    }
    if (/# Finished (\S+) in ([\d\.]+) s/) {
        my $stage = $1;
        my $time = $2;
        $stats{"NumStages"}++;
        $stats{"StageTime"} += $time;
        while (my ($module, $module_name) = each %module_map) {
           if ($stage =~ /$module/i) {
              $stats{$module_name} += $time;
           }
        }
    }
    
    if (/# Overall time for HipMer, version .* is ([\d\.]+) s/) {
        $stats{"CalculatedTotalTime"} += $1;
    }
    if (/Completed run at (\d+-\d+-\d+) .* in ([\d\.]+) s/) {
        $stats{"Date"} = $1;
        $stats{"TotalTime"} = $2;
    }
    if ($firstUFX) {
        if (/Total reads processed over all processors is (\d+)/) {
            $stats{"NumReads"} = $1;
        }
        if (/Processed a total of \d+ lines \((\d+) reads\), max read length/) {
            $stats{"NumReads"} = $1;
        }
#        if (/^\S+ : (\d+) MB$/) {
#            $stats{"GBofFASTQ"} += $1/1024;
#        }
        if (/Purged (\d+) .* kmers below frequency threshold of/) {
            $stats{"DistinctKmersWithFP"} = $1;
        }
        if (/Kmerscount hash includes (\d+) distinct elements/ || /Found (\d+) \(.*\) unique kmers/) {
            if (defined $stats{"MinDepthKmers"}) {
                $stats{"DistinctKmersWithFP"} = $stats{"MinDepthKmers"}; # the previous one
            }
            $stats{"MinDepthKmers"} = $1; # the last one
        }
        if (/After purge of kmers <\d+ there are (\d+) unique kmers/) {
            $stats{"MinDepthKmers"} = $1; # the last one
            if (defined $stats{"DistinctKmersWithFP"}) {
               $stats{"DistinctKmersWithFP"} += $1;
            }
        }
        if (/# Finished ufx/ || /# Finished kcount/) {
            $firstUFX = 0;
        }
    }
    if (/at (\d\d\/\d\d\/\d\d \d\d:\d\d:\d\d)/) {
        $stats{"LastStageDate"} = $1;
    }   

}
if (not defined $stats{"RunOptions"}) {
    $stats{"RunOptions"} = $stats{"Config"};
}
$stats{"CalculatedTotalTime"} =~ s/\..*//;
if (not defined $stats{"Date"}) {
    $stats{"Date"} = $stats{"LastStageDate"};
}
if (not defined $stats{"TotalTime"}) {
    $stats{"TotalTime"} = $stats{"CalculatedTotalTime"};
}
if ($stats{"TotalTime"} < $stats{"StageTime"}) {
    $stats{"TotalTime"} = $stats{"StageTime"};
}
$stats{"TotalTime"} =~ s/\..*//;
$stats{"NodeHours"} = $stats{"TotalTime"} * $stats{"Nodes"} / 3600;
$stats{"NodeHours"} =~ s/\..*//;

$stats{"HipmerWorkflow"} = ( $meta ? "Metagenome " : "Single Genome " ) . ( $diploid ? ($zygo ? "High-" : "Low-") . "Zygosity Diploid " : "") . ($cgraph ? "with cgraph " : "");

$stats{"DataSetName"} = $stats{"RunDir"};
$stats{"DataSetName"} =~ s/.*\///;
foreach my $module (@modules) {
    $stats{$module} =~ s/\..*//;
}

if (! -d $stats{"RunDir"}) {
    print STDERR "Could not find rundir: " . $stats{"RunDir"} . "\n";
}

if ($knowGB) {
  $stats{"GBofFASTQ"} = $knowGB;
} else {
 foreach my $lib (@libs) {
   $_ = $lib;
   if ($_ !~ /^\//) {
       $_ = $stats{"RunDir"} . "/" . $_;
   }
   if ( -f $_ ) {
       my $size = stat($_)->size;
       print "Could not find size of $_\n" unless defined $size;
       #print "Found $_ with $size bytes\n";
       $stats{"GBofFASTQ"} += $size / 1024/1024/1024;
   } else {
       $knowGB = 0;
   }
 }
}
if ($knowGB) {
     $stats{"GBofFASTQ"} =~ s/\..*//;
}

printStats();

print "HipMer, version " . $stats{"Version"} . ", was executed on " . $stats{"NumReads"} . " reads" . ($knowGB?" and " . $stats{"GBofFASTQ"} . " GB of fastq " : " ") . "for " . $stats{"TotalTime"} . " seconds in a job over " . $stats{"Nodes"} . " nodes (" . $stats{"Threads"} . " threads) using the " . $stats{"HipmerWorkflow"} . "workflow.\n";

