#include <stdio.h>
#include "upc.h"

int rank();

int main(int argc, char **argv) {
   printf("Hello, I am %d of %d (%d)\n", MYTHREAD, THREADS, rank());
   return 0;
}

