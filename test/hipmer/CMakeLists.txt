MESSAGE("Building helper test code ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

set(CMAKE_CXX_STANDARD 14)

# for c++ ufx reading code

find_package(ZLIB 1.2.3 REQUIRED)
find_package(RT QUIET)
# fix linking when objects are from multiple languages
set(CMAKE_C_IMPLICIT_LINK_LIBRARIES "")
set(CMAKE_C_IMPLICIT_LINK_DIRECTORIES "")
set(CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++")
set(CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "")

SET(CMAKE_EXE_LINKER_FLAGS)

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    set(CMAKE_UPC_FLAGS "${CMAKE_UPC_FLAGS} -g")
endif()

SET_UPC_PROPS(*.upc)

ADD_LIBRARY(dummy OBJECT dummy.upc)

ADD_EXECUTABLE(hello_world-upc hello_world.upc)
INSTALL(TARGETS hello_world-upc DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

if (CMAKE_UPC_COMPILER_ID STREQUAL "BUPC")
  separate_arguments(args UNIX_COMMAND "${CMAKE_UPC_FLAGS}")
  execute_process(COMMAND upcc ${args} -print-include-dir
                  OUTPUT_VARIABLE UPCC_INCLUDE_DIR2
                  )
  message("upcc ${CMAKE_UPC_FLAGS} -print-include-dir returned ${UPCC_INCLUDE_DIR2}")
  ADD_LIBRARY(hello_world-hipmer_obj OBJECT hello_world-hipmer.c)
  SET_TARGET_PROPERTIES(hello_world-hipmer_obj PROPERTIES
                        INCLUDE_DIRECTORIES "${UPCC_INCLUDE_DIR2}"
                       )
  ADD_EXECUTABLE(hello_world-hipmer $<TARGET_OBJECTS:hello_world-hipmer_obj> $<TARGET_OBJECTS:dummy>)
  SET_TARGET_PROPERTIES(hello_world-hipmer PROPERTIES
                        INCLUDE_DIRECTORIES "${UPCC_INCLUDE_DIR2}"
                        LINKER_LANGUAGE UPC
                        LINK_FLAGS "--extern-main --link-with=${CMAKE_CXX_COMPILER}"
                       )
  INSTALL(TARGETS hello_world-hipmer DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
endif()

FOREACH(target test_atomic_pointers report_memory test_distributed_buffer test_atomic_data test_SharedBuffer test_utils test_list test_parallel_sort)

    ADD_EXECUTABLE(${target} ${target}.upc
                   $<TARGET_OBJECTS:upc_common>
                   $<TARGET_OBJECTS:COMMON>
                   $<TARGET_OBJECTS:HIPMER_VERSION>
                   $<TARGET_OBJECTS:HASH_FUNCS>
                   $<TARGET_OBJECTS:Buffer>
                   $<TARGET_OBJECTS:MemoryChk>
                   $<TARGET_OBJECTS:DistributedBuffer>
                   $<TARGET_OBJECTS:UPCAtomicData>
                   $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
    )
    SET_TARGET_PROPERTIES(${target} PROPERTIES LINKER_LANGUAGE "UPC")
    TARGET_LINK_LIBRARIES(${target} ${ZLIB_LIBRARIES} ${RT_LIBRARIES} -lm)
    INSTALL(TARGETS ${target} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

ENDFOREACH()

ADD_EXECUTABLE(unpack_hipmer_idx unpack_hipmer_idx.c)
INSTALL(TARGETS unpack_hipmer_idx DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )


