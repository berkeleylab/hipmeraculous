#include <stdio.h>
#include <stdlib.h>
#include <upcxx/upcxx.hpp>

#include <fstream>

int main() {
	upcxx::init();
	printf("Hello %d of %d\n", upcxx::rank_me(), upcxx::rank_n());
        upcxx::finalize();
	return 0;
}
