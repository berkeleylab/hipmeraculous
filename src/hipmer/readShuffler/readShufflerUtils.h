#include "upc_compatibility.h"
#include "Buffer.h"
#include "upc_distributed_buffer.h"
#include "timers.h"

#ifndef MAX_READ_LEN
#define MAX_READ_LEN DEFAULT_READ_LEN
#endif

#define READ_CHUNK_SIZE 10
#define MAX_LINE_SIZE 1000
#define MAX_READ_FILES 20

#define USE_DISTRIBUTED_BUFFER

typedef struct read_t read_t;
struct read_t {
    char    readName[MAX_READ_NAME_LEN];
    char    readSeq[MAX_READ_LEN];
    char    readQual[MAX_READ_LEN];
    int64_t mapped_contig;
};

typedef shared[] read_t *shared_read_ptr;

/* Read heap data structure */
typedef struct read_heap_t read_heap_t;
struct read_heap_t {
    shared[1] int64_t * heap_indices;                   // Indices of remote heaps
#ifdef USE_DISTRIBUTED_BUFFER
    DistributedBuffer db;
#else
    shared_read_ptr * read_ptr;              // Pointers to shared memory heaps
    shared[1] shared_read_ptr * tmp_heap_ptr;
#endif
};

/* Creates shared heaps that required for read heaps - IMPORTANT: This is a collective function */
int create_read_heaps(int64_t size, read_heap_t *read_heap)
{
    int i;

    UPC_ALL_ALLOC_CHK(read_heap->heap_indices, THREADS, sizeof(int64_t));
    read_heap->heap_indices[MYTHREAD] = 0;
#ifdef USE_DISTRIBUTED_BUFFER
    LOGF("Allocating read heaps for %lld incoming reads: %0.3f MB (local DB)\n", (lld)size, 1.0 * size * sizeof(read_t) / ONE_MB);
    read_heap->db = DistributedBuffer_initFixed(size, sizeof(read_t));
    read_heap->heap_indices[MYTHREAD] = DistributedBuffer_getThreadGlobalOffset(read_heap->db, MYTHREAD);
#else
    LOGF("Allocating read heaps for %lld incoming reads: %0.3f MB + read_ptr: %0.3f MB\n", (lld)size, 1.0 * size * sizeof(read_t) / ONE_MB, 1.0 * THREADS * sizeof(shared_read_ptr) / ONE_MB);
    UPC_ALL_ALLOC_CHK(read_heap->tmp_heap_ptr, THREADS, sizeof(shared_read_ptr));
    UPC_ALLOC_CHK(read_heap->tmp_heap_ptr[MYTHREAD], size * sizeof(read_t));
    upc_barrier;
    read_heap->read_ptr = (shared_read_ptr *)malloc_chk(THREADS * sizeof(shared_read_ptr));
    upc_barrier;
    for (i = 0; i < THREADS; i++) {
        read_heap->read_ptr[i] = read_heap->tmp_heap_ptr[i];
    }
#endif
    UPC_LOGGED_BARRIER;
    return 0;
}

void destroy_read_heaps(read_heap_t *read_heap)
{
    UPC_ALL_FREE_CHK(read_heap->heap_indices);
#ifdef USE_DISTRIBUTED_BUFFER
    DistributedBuffer_free(&(read_heap->db));
#else
    free_chk(read_heap->read_ptr);
    UPC_FREE_CHK(read_heap->tmp_heap_ptr[MYTHREAD]);
    UPC_ALL_FREE_CHK(read_heap->tmp_heap_ptr);
#endif
}

/* Allocate local arrays used for book-keeping when using aggregated upc_memputs */
int allocate_read_local_buffs(read_t **local_buffs, int64_t **local_index)
{
    LOGF("Allocating read_local_buffs: %0.3f MB + local_index: %0.3f MB\n", 1.0 * THREADS * READ_CHUNK_SIZE * sizeof(read_t) / ONE_MB, 1.0 * THREADS * sizeof(int64_t) / ONE_MB);
    (*local_buffs) = (read_t *)malloc_chk(THREADS * READ_CHUNK_SIZE * sizeof(read_t));
    (*local_index) = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    memset((*local_index), 0, THREADS * sizeof(int64_t));
    return 0;
}

/* Adds read to the appropriate shared  heap */
static int64_t *writeTo = NULL;
int add_read_to_shared_heaps(read_t *new_entry, int remote_thread, int64_t *local_index, read_t *local_buffs, read_heap_t *read_heap)
{
    int64_t store_pos;

    /* Store read first to local buffer designated for remote thread */
    if (local_index[remote_thread] <= READ_CHUNK_SIZE - 1) {
        memcpy(&(local_buffs[local_index[remote_thread] + remote_thread * READ_CHUNK_SIZE]), new_entry, sizeof(read_t));
        local_index[remote_thread]++;
    }
    /* If buffer for that thread is full, do a remote upc_memput() */
    if (local_index[remote_thread] == READ_CHUNK_SIZE) {
        if (writeTo == NULL) {
            writeTo = calloc_chk(THREADS, sizeof(int64_t));
        }
        writeTo[remote_thread]++;
        UPC_ATOMIC_FADD_I64(&store_pos, &(read_heap->heap_indices[remote_thread]), READ_CHUNK_SIZE);
#ifdef USE_DISTRIBUTED_BUFFER
        DBG2("Wrote %lld to thread %ld globalOffset=%lld\n", (lld)local_index[remote_thread], remote_thread, store_pos);
        DistributedBuffer_writeAt(read_heap->db, store_pos, &(local_buffs[remote_thread * READ_CHUNK_SIZE]), (READ_CHUNK_SIZE));
#else
        upc_memput((shared[] read_t *)(read_heap->read_ptr[remote_thread] + store_pos), &(local_buffs[remote_thread * READ_CHUNK_SIZE]), (READ_CHUNK_SIZE)*sizeof(read_t));
#endif
        local_index[remote_thread] = 0;
    }
    return 0;
}

/* Adds remaining reads to the shared heaps. */
int add_rest_reads_to_shared_heaps(int64_t *local_index, read_t *local_buffs, read_heap_t *read_heap)
{
    int i;
    int64_t store_pos;

    for (i = 0; i < THREADS; i++) {
        if (local_index[i] != 0) {
            UPC_ATOMIC_FADD_I64(&store_pos, &(read_heap->heap_indices[i]), local_index[i]);
#ifdef USE_DISTRIBUTED_BUFFER
            DBG("Wrote final %lld (+ %lld = %lld) to thread %ld globalOffset=%lld\n", (lld)local_index[i], (lld)READ_CHUNK_SIZE * writeTo[i], (lld)local_index[i] + READ_CHUNK_SIZE * writeTo[i], i, store_pos);
            DistributedBuffer_writeAt(read_heap->db, store_pos, &(local_buffs[i * READ_CHUNK_SIZE]), (local_index[i]));
#else
            upc_memput((shared[] read_t *)((read_heap->read_ptr[i]) + store_pos), &(local_buffs[i * READ_CHUNK_SIZE]), (local_index[i]) * sizeof(read_t));
#endif
        }
    }
    if (writeTo) {
        free_chk(writeTo);
    }
    return 0;
}


int cmpFunc(const void *a, const void *b)
{
    const read_t *p1 = (read_t *)a;
    const read_t *p2 = (read_t *)b;

    if (p1->mapped_contig > p2->mapped_contig) {
        return 1;
    } else if (p1->mapped_contig < p2->mapped_contig) {
        return -1;
    } else if (p1->mapped_contig == p2->mapped_contig) {
        if (strcmp(p1->readName, p2->readName) > 0) {
            return 1;
        } else {
            return -1;
        }
    }
    return 0;
}
