#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "readShufflerUtils.h"
#include "../scaffolding/histogrammer/analyzerUtils.h"
#include "../fqreader/fq_reader.h"
#include "Buffer.h"
#include "../meraligner/fasta.h"
#include "upc_distributed_buffer.h"
#include "utils.h"
#include "upc_output.h"
#include "timers.h"

#define SHUFFLE_TEST 1

int read_shuffler_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();
    option_t *optList, *thisOpt;

    optList = NULL;
    optList = GetOptList(argc, argv, "B:n:L:f:A:l:X");
    print_args(optList, __func__);

    char *alignmentPattern = NULL, *base_dir = NULL;
    int64_t totalContigs = 0;
    int64_t files_per_pair = 1;
    int cached_io_reads = 0;
    int libnum = 0;
    char * libName = NULL;

    /* Process the input arguments */
    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'A':
            alignmentPattern = thisOpt->argument;
            break;
        case 'L':
            libName = thisOpt->argument;
            break;
        case 'n':
            totalContigs = atol(thisOpt->argument);
            break;
        case 'l':
            libnum = atoi(thisOpt->argument);
            break;
        case 'f':
            files_per_pair = atoi(thisOpt->argument);
            break;
        case 'X':
            cached_io_reads = 1;
            break;
        default:
            SWARN("Invalid option: '%c'\n", thisOpt->option);
            break;
        }
    }
    FreeOptList(optList);

    /* Find number of reads by visiting the alignment files */
    char *fgets_result = NULL;
    char cur_read[MAX_READ_NAME_LEN];
    char cur_read_registered[MAX_READ_NAME_LEN];
    char align[MAX_LINE_SIZE];
    int64_t subject;
    int splitRes, qStart, qStop, qLength, sStart, sStop, sLength, strand;
    int64_t my_n_reads = 0;
    char outputMerAligner[MAX_LINE_SIZE];

    if (libName == NULL || strlen(libName) == 0 || alignmentPattern == NULL || strlen(alignmentPattern) == 0)  {
        DIE("Invalid call -L libname and -A alignmentPattern must be specified in order to pick up the fofn and aligments\n");
    }
    upc_tick_t startAlignRead = upc_ticks_now();
    sprintf(outputMerAligner, "%s-%s_Read1" GZIP_EXT, libName, alignmentPattern);
    GZIP_FILE alignmentFD = openCheckpoint(outputMerAligner, "r");

    cur_read[0] = cur_read_registered[0] = '\0';
    fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
    if (fgets_result != NULL) {
        splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
    }
    while (fgets_result != NULL) {
        /* Consume the primary (MERALIGNER-F, MERALIGNER-0, MERALIGNER-1) alignment */
        if (splitRes == 0 || splitRes == 1 || splitRes == 2) {
            my_n_reads++;
            strcpy(cur_read_registered, cur_read);
        }
        /* Consume potential secondary alignments */
        while (strcmp(cur_read_registered, cur_read) == 0 && (fgets_result != NULL)) {
            fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
            if (fgets_result != NULL) {
                splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
            }
        }
    }
    GZIP_REWIND(alignmentFD);
    cur_read[0] = cur_read_registered[0] = '\0';
    upc_tick_t startAlignRead2 = upc_ticks_now();
    LOGF("Read alignments for read count: %s in %0.3fs\n", outputMerAligner, TICKS_TO_S(startAlignRead2 - startAlignRead));

    /* Revisit alignment file and find destination "contig processor" AND memory requirements per thread */
    int64_t i;
    int64_t *dst = (int64_t *)malloc_chk(my_n_reads * sizeof(int64_t));
    int64_t *entriesPerThread = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    shared[1] int64_t * total_entries = NULL;
    UPC_ALL_ALLOC_CHK(total_entries, THREADS, sizeof(int64_t));
    total_entries[MYTHREAD] = 0;
    for (i = 0; i < THREADS; i++) {
        entriesPerThread[i] = 0;
    }
    fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
    if (fgets_result != NULL) {
        splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
    }
    i = 0;
    while (fgets_result != NULL) {
        /* Consume the primary (MERALIGNER-F, MERALIGNER-0, MERALIGNER-1) alignment */
        if (splitRes == 0 || splitRes == 1 || splitRes == 2) {
            if (splitRes == 0) {
                dst[i] = rand() % totalContigs;
            } else {
                dst[i] = subject;
            }
            entriesPerThread[dst[i] % THREADS]++;
            i++;
            strcpy(cur_read_registered, cur_read);
        }
        /* Consume potential secondary alignments */
        while (strcmp(cur_read_registered, cur_read) == 0 && (fgets_result != NULL)) {
            fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
            if (fgets_result != NULL) {
                splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
            }
        }
    }
    closeCheckpoint(alignmentFD);
    upc_tick_t endAlignRead2 = upc_ticks_now();
    LOGF("Read alignments for thread assignment in %0.3fs\n", TICKS_TO_S(endAlignRead2 - startAlignRead2));
    UPC_LOGGED_BARRIER;

    for (i = 0; i < THREADS; i++) {
        if (files_per_pair > 0) {
            entriesPerThread[i] *= 2;
        }
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(total_entries[i]), entriesPerThread[i]);
    }
    UPC_LOGGED_BARRIER;

    DBG("totalEntries[%d] = %lld\t", MYTHREAD, (lld)total_entries[MYTHREAD]);
    for (i = 0; i < THREADS; i++) {
        DBGN("entriesPerThread[%d] = %lld\t", i, (lld)entriesPerThread[i]);
    }
    DBGN("... about to allocate and exchange\n");

    /* Allocate data structures for aggregating stores optimization */
    read_t *local_buffs = NULL;
    int64_t *local_index = NULL;
    read_heap_t read_heap;
    memset(&read_heap, 0, sizeof(read_heap));
    allocate_read_local_buffs(&local_buffs, &local_index);
    create_read_heaps(total_entries[MYTHREAD], &read_heap);
    upc_tick_t allocationTime = upc_ticks_now();
    LOGF("Allocated in %0.3fs\n", TICKS_TO_S(allocationTime - endAlignRead2));

    /* Now visit the first fastq file and distribute appropriately  */
    int found_next, found_next2;
    uint64_t read1ID = MYTHREAD, read2ID = MYTHREAD;
    Buffer idBuf = initBuffer(MAX_READ_NAME_LEN + 1);
    Buffer seqBuf = initBuffer(DEFAULT_READ_LEN + 1);
    Buffer qualsBuf = initBuffer(DEFAULT_READ_LEN + 1);
    char *id = getStartBuffer(idBuf);
    char *seq = getStartBuffer(seqBuf);
    char *quals = getStartBuffer(qualsBuf);
    upc_tick_t pairedLoopTime = allocationTime;
    char fofn[MAX_FILE_PATH];
    sprintf(fofn, "%s.fofn", libName);
    Buffer newFofnBuffer = initBuffer(MAX_FILE_PATH);
    Buffer fofnBuffer = broadcast_file(fofn);
    char read_filename_1[MAX_FILE_PATH], read_filename_2[MAX_FILE_PATH];
    while ( getsBuffer(fofnBuffer, read_filename_1, MAX_FILE_PATH) ) {
        read_filename_1[strlen(read_filename_1)-1] = '\0';
        fq_reader_t fqr = create_fq_reader(), fqr2 = NULL;
        if (files_per_pair == 2) {
            if ( !getsBuffer(fofnBuffer, read_filename_2, MAX_FILE_PATH) ) { DIE("Could not find the second file paired with %s\n", read_filename_1); }
            read_filename_2[strlen(read_filename_2)-1] = '\0';
            fqr2 = create_fq_reader();
        }
        upc_tick_t startReadFile = upc_ticks_now();
        LOGF("Starting read exchange in %0.3fs\n", TICKS_TO_S(startReadFile - pairedLoopTime));
        int64_t fileSize = -1;
        open_fq(fqr, read_filename_1, cached_io_reads, base_dir, cached_io_reads ? -1 : broadcast_file_size(read_filename_1));
        if (files_per_pair == 2) {
            open_fq(fqr2, read_filename_2, cached_io_reads, base_dir, cached_io_reads ? -1 : broadcast_file_size(read_filename_2));
        }
        found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
        read_t cur_read_entry;
        i = 0;
        /* Pocess the reads of the current read File (consider the case of interleaved reads file) */
        while (found_next) {
            int destThread = dst[i] % THREADS;
            id = getStartBuffer(idBuf);
            if (libnum >= 0) {
                hexifyId(id, libnum, &read1ID, &read2ID, THREADS);
            }
            seq = getStartBuffer(seqBuf);
            seq[getLengthBuffer(seqBuf)] = '\0';
            quals = getStartBuffer(qualsBuf);
            quals[getLengthBuffer(qualsBuf)] = '\0';
            assert(*id == '@');
            assert(strlen(id) < MAX_READ_NAME_LEN);
            strcpy(cur_read_entry.readName, id);
            strcpy(cur_read_entry.readSeq, seq);
            strcpy(cur_read_entry.readQual, quals);
            cur_read_entry.mapped_contig = dst[i];
            add_read_to_shared_heaps(&cur_read_entry, (int)(destThread), local_index, local_buffs, &read_heap);
            entriesPerThread[destThread]--;
            /* Consume mate read if we have interleaved  / interleaved reads in the fastq file... */
            if (files_per_pair == 1) { // interleaved reads
                found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
                if (!found_next) {
                    DIE("Invalid interleaved file %s is missing entry for last pair: %s\n", read_filename_1, getStartBuffer(idBuf));
                }
                id = getStartBuffer(idBuf);
                if (libnum >= 0) {
                    hexifyId(id, libnum, &read1ID, &read2ID, THREADS);
                }
                seq = getStartBuffer(seqBuf);
                seq[getLengthBuffer(seqBuf)] = '\0';
                quals = getStartBuffer(qualsBuf);
                quals[getLengthBuffer(qualsBuf)] = '\0';
                assert(*id == '@');
                assert(strlen(id) < MAX_READ_NAME_LEN);
                strcpy(cur_read_entry.readName, id);
                strcpy(cur_read_entry.readSeq, seq);
                strcpy(cur_read_entry.readQual, quals);
                cur_read_entry.mapped_contig = dst[i];
                add_read_to_shared_heaps(&cur_read_entry, (int)(destThread), local_index, local_buffs, &read_heap);
                entriesPerThread[destThread]--;
            }
            if (files_per_pair == 2) { // two read files
                found_next2 = get_next_fq_record(fqr2, idBuf, seqBuf, qualsBuf);
                if (!found_next2) {
                    DIE("Invalid pair file %s is missing entry for %s\n", read_filename_2, getStartBuffer(idBuf));
                }

                id = getStartBuffer(idBuf);
                if (libnum >= 0) {
                    hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
                }
                seq = getStartBuffer(seqBuf);
                seq[getLengthBuffer(seqBuf)] = '\0';
                quals = getStartBuffer(qualsBuf);
                quals[getLengthBuffer(qualsBuf)] = '\0';
                assert(*id == '@');
                assert(strlen(id) < MAX_READ_NAME_LEN);
                strcpy(cur_read_entry.readName, id);
                strcpy(cur_read_entry.readSeq, seq);
                strcpy(cur_read_entry.readQual, quals);
                cur_read_entry.mapped_contig = dst[i];
                add_read_to_shared_heaps(&cur_read_entry, (int)(destThread), local_index, local_buffs, &read_heap);
                entriesPerThread[destThread]--;
            }
            i++;
            /* Read next read  */
            found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
        }

        if (SHUFFLE_TEST) {
            for (int i = 0; i < THREADS; i++) {
                if (entriesPerThread[i] != 0) {
                    WARN("%lld for thread %d. entriesPerThread=%lld (should be 0)\n", (lld)local_index[i], i, (lld)entriesPerThread[i]);
                }
            }
        }
        /* Flush outgoing dedicated buffers */
        add_rest_reads_to_shared_heaps(local_index, local_buffs, &read_heap);
        UPC_LOGGED_BARRIER;
        LOGF("All reads exchanged: heap_indicies[MYTHREAD]=%lld threadOffset=%lld threadCount=%lld localGlobalOffset=%lld localCount=%lld\n", (lld)read_heap.heap_indices[MYTHREAD], DistributedBuffer_getThreadGlobalOffset(read_heap.db, MYTHREAD), DistributedBuffer_getThreadCount(read_heap.db, MYTHREAD), DistributedBuffer_getLocalGlobalOffset(read_heap.db, MYTHREAD), DistributedBuffer_getLocalCount(read_heap.db, MYTHREAD));
        if (SHUFFLE_TEST) {
            int64_t localGlobalOffset = DistributedBuffer_getLocalGlobalOffset(read_heap.db, MYTHREAD);
            int64_t outputStartOffset = localGlobalOffset;
            int64_t nEntries = DistributedBuffer_getLocalCount(read_heap.db, MYTHREAD);
            //verify
            for (int64_t x = outputStartOffset; x < outputStartOffset + nEntries; x++) {
                read_t read_entry = *((shared [] read_t *)DistributedBuffer_getSharedPtr(read_heap.db, x));
                if (strlen(read_entry.readName) == 0) {
                    WARN("NOT okay - %lld heap_indices=%lld\n", (lld)x, read_heap.heap_indices[MYTHREAD]);
                }
            }
        }

        close_fq(fqr);
        destroy_fq_reader(fqr);
        if (files_per_pair == 2) {
            close_fq(fqr2);
            destroy_fq_reader(fqr2);
        }
        upc_tick_t endReadFile = upc_ticks_now();
        LOGF("Read file(s) in %0.3fs\n", TICKS_TO_S(endReadFile - pairedLoopTime));

        read_t *my_read_entries = NULL;
#ifdef USE_DISTRIBUTED_BUFFER
        // sort just the read entries that are wholly contained in local (boundary crossing is not possible)
        // account for interleaved reads, if present which may be crossing thread-local boundaries
        /* Each processor sorts the received reads */
        int64_t localGlobalOffset = DistributedBuffer_getLocalGlobalOffset(read_heap.db, MYTHREAD);
        int64_t outputStartOffset = localGlobalOffset;
        int64_t nEntries = DistributedBuffer_getLocalCount(read_heap.db, MYTHREAD);

        if (files_per_pair > 0) {
            if ((outputStartOffset % 2) != 0) {
                // first read should be a read2, ignore it for sorting
                if (nEntries > 0) {
                    outputStartOffset++;
                    nEntries--;
                }
            }
            if ((nEntries % 2) != 0) {
                // last read should be a read1, ignore it for sorting
                nEntries--;
            }
        }
        if (files_per_pair == 2 && outputStartOffset % 2 != 0) {
            DIE("Invalid calculation for outputStartOffset=%lld it should be even when files_per_pair == 2\n", (lld)outputStartOffset);
        }

        if (nEntries > 0) {
            LOGF("Sorting local reads from globalOffsets %lld through %lld\n", (lld)outputStartOffset, (lld)outputStartOffset + nEntries);
            //shared [] read_t *sharedReadPtr = (shared [] read_t *) DistributedBuffer_getSharedPtr(read_heap.db, outputStartOffset);
            SharedCharPtr _sharedReadPtr = DistributedBuffer_getSharedPtr(read_heap.db, outputStartOffset);
            shared [] read_t * sharedReadPtr = (shared [] read_t *)_sharedReadPtr;
            assert(upc_threadof(sharedReadPtr) == MYTHREAD);
            assert(upc_threadof(sharedReadPtr + nEntries - 1) == MYTHREAD);
            my_read_entries = (read_t *)sharedReadPtr;
        }
#else
        int64_t nEntries = read_heap.heap_indices[MYTHREAD];
        my_read_entries = (read_t *)read_heap.read_ptr[MYTHREAD];
#endif

        if (nEntries > 0) {
            // This is a stable sort so read pairs in files_per_pair > 0 should both be ordered identically
            qsort(my_read_entries, nEntries, sizeof(read_t), cmpFunc);
        }
        upc_tick_t sortedTime = upc_ticks_now();
        LOGF("Sorted in %0.3fs\n", TICKS_TO_S(sortedTime - endReadFile));

        char bname[MAX_LINE_SIZE];
        char maxReadLenFileName[MAX_LINE_SIZE];
        sprintf(maxReadLenFileName, "%s.maxReadLen.txt", get_basename(bname, read_filename_1));
        get_rank_path(maxReadLenFileName, -1);
        char shuffledReadFileName[MAX_LINE_SIZE];
        char shuffledReadFileName2[MAX_LINE_SIZE];
        char * isgz = strstr(read_filename_1, ".gz");
        if (isgz) isgz[0] = '\0';
        sprintf(shuffledReadFileName, "%s_shuffled", get_basename(bname, read_filename_1));
        if (files_per_pair == 2) {
            char * isgz = strstr(read_filename_2, ".gz");
            if (isgz) isgz[0] = '\0';
            sprintf(shuffledReadFileName2, "%s_shuffled", get_basename(bname, read_filename_2));
        }
        if (!MYTHREAD && does_file_exist(maxReadLenFileName)) {
            // link the old maxReadLen to the new file name
            char newMaxReadLenFileName[MAX_LINE_SIZE+40];
            sprintf(newMaxReadLenFileName, "%s.fastq" GZIP_EXT ".maxReadLen.txt", shuffledReadFileName);
            get_rank_path(newMaxReadLenFileName, -1);
            link_chk(maxReadLenFileName, newMaxReadLenFileName);
        }

#ifdef USE_DISTRIBUTED_BUFFER
        // reads are already almost 100% load balanced
        // skip file size calc - write file directly, start at local memory, ensure it is even read_t globalOffset(if files_per_pair > 0)
        int64_t uncompressedFileSize = 0;
        int64_t uncompressedFileSize2 = 0;

        // Calculate the end based on next thread
        shared [1] int64_t * outputOffsets = NULL;
        UPC_ALL_ALLOC_CHK(outputOffsets, THREADS, sizeof(int64_t));
        outputOffsets[MYTHREAD] = outputStartOffset;
        UPC_LOGGED_BARRIER;
        int64_t outputEndOffset = (MYTHREAD == THREADS - 1 ? DistributedBuffer_getTotalCount(read_heap.db) : outputOffsets[MYTHREAD + 1]);

        if (SHUFFLE_TEST) {
            // verify
            for (int64_t x = outputStartOffset; x < outputEndOffset; x++) {
                read_t read_entry = *((shared [] read_t *)DistributedBuffer_getSharedPtr(read_heap.db, x));
                if (strlen(read_entry.readName) == 0) {
                    WARN("%lld\n", (lld)x);
                }
            }
        }
        LOGF("Generating %lld reads starting at global read %lld\n", (lld)outputEndOffset - outputStartOffset, outputStartOffset);

        // Now output to a (potentially compressed) file (potentially in /dev/shm)
        char outputfile_name[MAX_FILE_PATH+50];
        char outputfile_name2[MAX_FILE_PATH+50];

        snprintf(outputfile_name, MAX_FILE_PATH+50, "%.*s.fastq" GZIP_EXT, MAX_FILE_PATH, shuffledReadFileName);
        printfBuffer(newFofnBuffer, "%s\n", outputfile_name);
        
        serial_printf("Creating localized reads file: %s\n", outputfile_name);
        GZIP_FILE my_out_file = openCheckpoint(outputfile_name, "w"), my_out_file2 = NULL;
        if (files_per_pair == 2) {
            snprintf(outputfile_name2, MAX_FILE_PATH+50, "%.*s.fastq" GZIP_EXT, MAX_FILE_PATH, shuffledReadFileName2);
            printfBuffer(newFofnBuffer, "%s\n", outputfile_name2);
            serial_printf("Creating localized reads file: %s\n", outputfile_name2);
            my_out_file2 = openCheckpoint(outputfile_name2, "w");
        }

        Buffer myBuffer = initBuffer(8192);
        for (int64_t readIdx = outputStartOffset; readIdx < outputEndOffset; readIdx++) {
            read_t read_entry = *((shared [] read_t *)DistributedBuffer_getSharedPtr(read_heap.db, readIdx));

            if (strlen(read_entry.readName) == 0 || strlen(read_entry.readSeq) == 0) {
                DIE("Empty read at readIdx=%lld\n", (lld)readIdx);
            }

            resetBuffer(myBuffer);
            if (SHUFFLE_TEST) {
                if (strlen(read_entry.readName) == 0 || strlen(read_entry.readSeq) == 0 || strlen(read_entry.readQual) != strlen(read_entry.readSeq)) {
                    DIE("Invalid read %lld: '%s' '%s' '%s' mapped_contig=%lld mapped_thread=%lld\n", (lld)readIdx, read_entry.readName, read_entry.readSeq, read_entry.readQual, (lld)read_entry.mapped_contig, (lld)read_entry.mapped_contig % THREADS);
                }
            }
            printfBuffer(myBuffer, "%s\n%s\n+\n%s\n", read_entry.readName, read_entry.readSeq, read_entry.readQual);
            if (files_per_pair < 2 || readIdx % 2 == 0) {
                uncompressedFileSize += getLengthBuffer(myBuffer);
                GZIP_FWRITE(getStartBuffer(myBuffer), 1, getLengthBuffer(myBuffer), my_out_file);
            } else {
                uncompressedFileSize2 += getLengthBuffer(myBuffer);
                GZIP_FWRITE(getStartBuffer(myBuffer), 1, getLengthBuffer(myBuffer), my_out_file2);
            }
        }
        closeCheckpoint(my_out_file);
        if (files_per_pair == 2) {
            closeCheckpoint(my_out_file2);
        }

        // write a file with the uncompressedFileSize for downstream use
        
        char ucs_fname[MAX_FILE_PATH+70];
        LOGF("%s has uncompressedFileSize=%lld\n", outputfile_name, (lld)uncompressedFileSize);
        sprintf(ucs_fname, "%s.uncompressedSize", outputfile_name);
        FILE *fsizefd = openCheckpoint0(ucs_fname, "w");
        fwrite_chk(&uncompressedFileSize, sizeof(int64_t), 1, fsizefd);
        closeCheckpoint0(fsizefd);

        if (files_per_pair == 2) {
            // write a file with the uncompressedFileSize for downstream use
            LOGF("%s has uncompressedFileSize=%lld\n", outputfile_name2, (lld)uncompressedFileSize2);
            sprintf(ucs_fname, "%s.uncompressedSize", outputfile_name2);
            fsizefd = openCheckpoint0(ucs_fname, "w");
            fwrite_chk(&uncompressedFileSize2, sizeof(int64_t), 1, fsizefd);
            closeCheckpoint0(fsizefd);
        }

        UPC_LOGGED_BARRIER;
        freeBuffer(myBuffer);
        UPC_ALL_FREE_CHK(outputOffsets);

#else
        int64_t file_size = 0;
        /* Iterate over my reads and measure the required filesize  */
        for (i = 0; i < nEntries; i++) {
            file_size += strlen(my_read_entries[i].readName) + 2 * strlen(my_read_entries[i].readSeq) + 4 + 1; // "@readname\nseq\n+\nqual\n"
        }

        /* Propagate the sizes of the partial read files */
        shared[1] int64_t * ourSizes = NULL;
        UPC_ALL_ALLOC_CHK(ourSizes, THREADS, sizeof(int64_t));
        ourSizes[MYTHREAD] = file_size;
        upc_barrier;
        int64_t *local_copy_ourSizes = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
        for (i = 0; i < MYTHREAD; i++) {
            local_copy_ourSizes[i] = ourSizes[i];
        }
        /* Calculate my file offsets */
        int64_t myOffset = 0;
        for (i = 0; i < MYTHREAD; i++) {
            myOffset += local_copy_ourSizes[i];
        }
        UPC_ALL_FREE_CHK(ourSizes);
        free_chk(local_copy_ourSizes);

        upc_barrier;
        if (MYTHREAD == THREADS - 1) {
            int res = ftruncate(fileno(outFD), myOffset + file_size);
        }
        upc_barrier;
        char *myMap, *myStart;
        size_t myPageOffset, myPos;
        myPageOffset = myOffset % sysconf(_SC_PAGESIZE);
        //myMap = mmap(NULL, file_size + myPageOffset, PROT_WRITE, MAP_SHARED|MAP_FILE, fileno(outFD), myOffset - myPageOffset);
        myStart = myMap + myPageOffset;
        myPos = 0;
        int id_length, read_length, quality_length;

        /* Write the shuffled read file  */
        for (i = 0; i < nEntries; i++) {
            id_length = strlen(my_read_entries[i].readName);
            memcpy(myStart + myPos, my_read_entries[i].readName, id_length * sizeof(char));
            myPos += id_length;
            myStart[myPos] = '\n';
            myPos++;
            read_length = strlen(my_read_entries[i].readSeq);
            memcpy(myStart + myPos, my_read_entries[i].readSeq, read_length * sizeof(char));
            myPos += read_length;
            myStart[myPos] = '\n';
            myPos++;
            my_read_entries[i].readName[0] = '+';
            memcpy(myStart + myPos, my_read_entries[i].readName, id_length * sizeof(char));
            myPos += id_length;
            myStart[myPos] = '\n';
            myPos++;
            quality_length = strlen(my_read_entries[i].readQual);
            memcpy(myStart + myPos, my_read_entries[i].readQual, quality_length * sizeof(char));
            myPos += quality_length;
            myStart[myPos] = '\n';
            myPos++;
        }

        munmap(myMap, file_size + myPageOffset);
        fclose_track(outFD);
#endif

        if (cached_io_reads) {
            char buf[MAX_FILE_PATH];
            char *tmp = strdup(read_filename_1);
            sprintf(buf, "%s", basename(tmp));
            free(tmp);
            get_rank_path(buf, MYTHREAD);
            serial_printf("Unlinking old, cached input file %s\n", buf);
            unlink(buf);
            if (files_per_pair == 2) {
                tmp = strdup(read_filename_2);
                sprintf(buf, "%s", basename(tmp));
                free(tmp);
                get_rank_path(buf, MYTHREAD);
                serial_printf("Unlinking old, cached input file %s\n", buf);
                unlink(buf);
            }
        }
        upc_tick_t writeTime = upc_ticks_now();
        LOGF("Wrote in %0.3fs\n", TICKS_TO_S(writeTime - sortedTime));
        pairedLoopTime = writeTime;
    }

    freeBuffer(fofnBuffer);
    if (!MYTHREAD) {
        serial_printf("Replacing fofn: %s\n", fofn);
        // overwrite the fofn
        char new[MAX_FILE_PATH+20];
        sprintf(new, "%s-PRELOCALIZE", fofn);
        rename(fofn, new);
        FILE * f = fopen_chk(fofn, "w");
        fwrite_chk(getStartBuffer(newFofnBuffer), 1, getLengthBuffer(newFofnBuffer), f);
        fclose_track(f);
    }
    freeBuffer(newFofnBuffer);

    serial_printf("Cleaning up memory\n");
    UPC_LOGGED_BARRIER;
    free_chk(dst);
    free_chk(entriesPerThread);
    free_chk(local_buffs);
    free_chk(local_index);
    UPC_ALL_FREE_CHK(total_entries);
    destroy_read_heaps(&read_heap);
    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]),
                  ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));

    return 0;
}
