#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"

int read_shuffler_main(int argc, char **argv);

int main(int argc, char **argv)
{
    OPEN_MY_LOG("read_shuffler");
    int ret = read_shuffler_main(argc, argv);
    CLOSE_MY_LOG;
    return ret;
}
