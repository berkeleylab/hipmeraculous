MESSAGE("Building read shuffler ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

# fix linking when objects are from multiple languages
set(CMAKE_C_IMPLICIT_LINK_LIBRARIES "")
set(CMAKE_C_IMPLICIT_LINK_DIRECTORIES "")
set(CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++")
set(CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "")

SET(CMAKE_EXE_LINKER_FLAGS)

SET_UPC_PROPS(readShuffler.c)

add_library(readShuffler_objs OBJECT readShuffler.c)
set_target_properties(readShuffler_objs PROPERTIES LINKER_LANGUAGE "UPC")

if (HIPMER_FULL_BUILD)
	ADD_EXECUTABLE(readShuffler readShuffler_exec.upc
                    $<TARGET_OBJECTS:readShuffler_objs>
					$<TARGET_OBJECTS:upc_common>
					$<TARGET_OBJECTS:COMMON>
                                        $<TARGET_OBJECTS:HIPMER_VERSION>
					$<TARGET_OBJECTS:HASH_FUNCS>
					$<TARGET_OBJECTS:UPC_FQ_OBJS>
					$<TARGET_OBJECTS:FASTA>
                    $<TARGET_OBJECTS:AnalyzerUtils>
					$<TARGET_OBJECTS:Buffer>
					$<TARGET_OBJECTS:MemoryChk>
					$<TARGET_OBJECTS:OptList>
					$<TARGET_OBJECTS:DistributedBuffer>
                    $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
	)

	SET_TARGET_PROPERTIES(readShuffler PROPERTIES LINKER_LANGUAGE "UPC")
	TARGET_LINK_LIBRARIES(readShuffler ${ZLIB_LIBRARIES} ${RT_LIBRARIES}) 
       	INSTALL(TARGETS readShuffler DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/)
endif()

