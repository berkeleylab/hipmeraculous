#ifndef FIND_NEW_MERS_MAIN_H_
#define FIND_NEW_MERS_MAIN_H_

#ifdef USE_CRAY_UPC
#include <intrinsics.h>
#endif

#include <upc.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <upc.h>
#include <upc_tick.h>
#include <libgen.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include "findNewMers_dds.h"

#include "optlist.h"

#include "StaticVars.h"
#include "upc_common.h"
#include "Buffer.h"
#include "timers.h"
#include "meraculous.h"
#include "utils.h"
#include "upc_output.h"

#include "../contigs/kmer_handling.h"
#include "../kcount/readufx.h"

#include "kmer_hash_findNewMers.h"
#include "buildUFXhashBinaryNew.h"

#define MAX_CONTIG_SIZE_2 9000000

int findNewMers_main(int argc, char **argv);

#endif // FIND_NEW_MERS_MAIN_H_
