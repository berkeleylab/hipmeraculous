#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include "optlist.h"
#include "upc_common.h"

int main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int threads, i;
    char *input;
    int nLines = 0;

    option_t *optList, *thisOpt;

    optList = NULL;
    optList = GetOptList(argc, argv, "t:i:");
    print_args(optList, __func__);

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 't':
            threads = atoi(thisOpt->argument);
            break;
        case 'i':
            input = thisOpt->argument;
            break;
        default:
            break;
        }
    }

    char filename[MAX_FILE_PATH];
    FILE *curFile;

    sprintf(filename, "%s_0.dat", input);
    curFile = fopen_chk(filename, "r");
    char ch;

    while (!feof(curFile)) {
        ch = fgetc(curFile);
        if (ch == '\n') {
            nLines++;
        }
    }

    fclose_track(curFile);

    double entries[nLines][threads];
    int sizes[nLines];
    char buf[256];
    int curLine, j;

    for (i = 0; i < threads; i++) {
        sprintf(filename, "%s_%d.dat", input, i);
        curFile = fopen_chk(filename, "r");
        curLine = 0;
        while (fgets(buf, sizeof(buf), curFile)) {
            sscanf(buf, "%d\t%lf\n", &sizes[curLine], &(entries[curLine][i]));
            curLine++;
        }
        fclose_track(curFile);
    }

    double min, max, sum;

    printf("==============================================================================\n");
    printf("Size\t|\tAvg\t\t|\tMin\t\t|\tMax\t\n");
    printf("==============================================================================\n");



    for (i = 0; i < nLines; i++) {
        min = entries[i][0];
        max = entries[i][0];
        sum = entries[i][0];

        for (j = 1; j < threads; j++) {
            if (entries[i][j] < min) {
                min = entries[i][j];
            }
            if (entries[i][j] > max) {
                max = entries[i][j];
            }
            sum += entries[i][j];
        }

        printf("%d\t|\t%lf\t|\t%lf\t|\t%lf\n", sizes[i], sum / threads, min, max);
    }

    upc_barrier;

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
