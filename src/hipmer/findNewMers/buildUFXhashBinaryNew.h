#ifndef BUILD_UFX_BINARY_NEW_H_
#define BUILD_UFX_BINARY_NEW_H_

#define MAX_CONTIG_SIZE 500000
#include <stdio.h>
#include <sys/stat.h>
#include <assert.h>

#include "findNewMers_dds.h"

#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "log.h"
#include "StaticVars.h"
#include "Buffer.h"
#include "meraculous.h"

#include "../contigs/packingDNAseq.h"
#include "../contigs/kmer_handling.h"
#include "kmer_hash_findNewMers.h"
#include "UU_traversal_findNewMers.h"
#include "../kcount/readufx.h"



//#define FAIR_COUNTING
#define STRICT_MERS

void simpleSetPosInCounts(char base, int *array, int count);

void printDesignation(LIST_T *resultFromList, double given_error, int64_t dmin);

void adjustDesignation(LIST_T *resultFromList, double given_error, int64_t dmin);

#define BUILD_UFX_HASH_NEW JOIN(CONTIGS_DDS_TYPE, build_UFX_hash_new)
HASH_TABLE_T *BUILD_UFX_HASH_NEW(int64_t size, GZIP_FILE contigFd, MEMORY_HEAP_T *memory_heap_res, int64_t myShare, int64_t nMersFromContigs, int64_t dsize, int64_t dmin, double errorRate, int CHUNK_SIZE, int load_factor, LIST_T **resultList, shared[] LIST_T **freeList, int64_t *nNewMers, GZIP_FILE prefixMerdepthsFile, int64_t nPrevMersFromContigs, int prev_k, int kmer_len, struct ufx_file_t *UFX_f);

#endif // BUILD_UFX_BINARY_NEW_H_
