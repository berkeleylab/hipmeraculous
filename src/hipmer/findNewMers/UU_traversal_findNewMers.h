#ifndef UU_TRAVERSAL_CONTIG_H
#define UU_TRAVERSAL_CONTIG_H

#include "findNewMers_dds.h"

#include "../contigs/contigs_dds.h"
#include "kmer_hash_findNewMers.h"

#include "UU_traversal_generic.h"

#endif // UU_TRAVERSAL_CONTIG_H
