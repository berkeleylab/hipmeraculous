#ifndef FIND_NEW_MERS_DDS_H_
#define FIND_NEW_MERS_DDS_H_

#define FIND_NEW_KMERS 1
#define PRINT_FASTA 1
#define DETAILED_BUILD_PROFILE 1
#define MERACULOUS 1
#define COUNT_INFO 1

#define CONTIGS_DDS_TYPE _find_new_mers_

#endif // FIND_NEW_MERS_DDS_H_
