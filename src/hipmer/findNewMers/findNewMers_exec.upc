#include "findNewMers.h"

shared int64_t contig_id;
int64_t myContigs;
int64_t bases_added;
int main(int argc, char **argv)
{
    OPEN_MY_LOG("findNewMers");
    if (!MYTHREAD) {
        contig_id = 0;
    }
    myContigs = 0;
    bases_added = 0;
    return findNewMers_main(argc, argv);
}
