#include "buildUFXhashBinaryNew.h"

void simpleSetPosInCounts(char base, int *array, int count)
{
    switch (base) {
    case 'A':
        array[0] += count;
        break;
    case 'C':
        array[1] += count;
        break;
    case 'G':
        array[2] += count;
        break;
    case 'T':
        array[3] += count;
        break;
    }
}

void printDesignation(LIST_T *resultFromList, double given_error, int64_t dmin)
{
    int i, how_many_above = 0;

    double dmin_dynamic = given_error * resultFromList->used_flag;

    if (dmin_dynamic < dmin) {
        dmin_dynamic = dmin;
    }
    char rightDesignation;
    char leftDesignation;

    /* Count how many rights are above the threshold */
    for (i = 0; i < 4; i++) {
        if (resultFromList->rights[i] >= dmin_dynamic) {
            how_many_above++;
        }
    }

    if (how_many_above == 1) {
        rightDesignation = 'U';
    } else if (how_many_above >= 2) {
        rightDesignation = 'F';
    } else {
        rightDesignation = 'X';
    }

    how_many_above = 0;

    /* Count how many lefts are above the threshold */
    for (i = 0; i < 4; i++) {
        if (resultFromList->lefts[i] >= dmin_dynamic) {
            how_many_above++;
        }
    }

    if (how_many_above == 1) {
        leftDesignation = 'U';
    } else if (how_many_above >= 2) {
        leftDesignation = 'F';
    } else {
        leftDesignation = 'X';
    }

    DBG("Designation with threshold %lf is: %c %c\n", given_error, rightDesignation, leftDesignation);
}

void adjustDesignation(LIST_T *resultFromList, double given_error, int64_t dmin)
{
    int i, max_index, how_many_above = 0;

    double dmin_dynamic = given_error * resultFromList->used_flag;

    if (dmin_dynamic < dmin) {
        dmin_dynamic = dmin;
    }

    /* Count how many rights are above the threshold */
    for (i = 0; i < 4; i++) {
        if (resultFromList->rights[i] >= dmin_dynamic) {
            how_many_above++;
            max_index = i;
        }
    }

    /* Adjust designation... */
    if (how_many_above == 1) {
        for (i = 0; i < 4; i++) {
            if (i == max_index) {
                resultFromList->rights[i] = resultFromList->used_flag;
            } else {
                resultFromList->rights[i] = 0;
            }
        }
    }

    how_many_above = 0;
    /* Count how many lefts are above the threshold */
    for (i = 0; i < 4; i++) {
        if (resultFromList->lefts[i] >= dmin_dynamic) {
            how_many_above++;
            max_index = i;
        }
    }

    /* Adjust designation... */
    if (how_many_above == 1) {
        for (i = 0; i < 4; i++) {
            if (i == max_index) {
                resultFromList->lefts[i] = resultFromList->used_flag;
            } else {
                resultFromList->lefts[i] = 0;
            }
        }
    }
}



HASH_TABLE_T *BUILD_UFX_HASH_NEW(int64_t size, GZIP_FILE contigFd, MEMORY_HEAP_T *memory_heap_res, int64_t myShare, int64_t nMersFromContigs, int64_t dsize, int64_t dmin, double errorRate, int CHUNK_SIZE, int load_factor, LIST_T **resultList, shared[] LIST_T **freeList, int64_t *nNewMers, GZIP_FILE prefixMerdepthsFile, int64_t nPrevMersFromContigs, int prev_k, int kmer_len, struct ufx_file_t *UFX_f)
{
    HASH_TABLE_T *dist_hashtable;
    MEMORY_HEAP_T memory_heap;
    shared LIST_T *lookup_res;

    int64_t chars_read, cur_chars_read, ptr, retval, i;
    int64_t buffer_size, offset;
    int64_t num_chars, chars_to_be_read;

    shared[1] int64_t * heap_sizes = NULL;
    int64_t *my_heap_sizes, my_heap_size, my_ufx_lines, idx;
    int *ufx_remote_thread, remote_thread;
    int64_t hashval;

    UPC_TICK_T start_read, end_read, start_storing, end_storing, start_setup, end_setup, start_calculation, end_calculation, start_timer, end_timer;

    UPC_ALL_ALLOC_CHK(heap_sizes, THREADS, sizeof(int64_t));
    heap_sizes[MYTHREAD] = 0;

    my_ufx_lines = myShare + nMersFromContigs;

    ufx_remote_thread = (int *)malloc_chk(my_ufx_lines * sizeof(int));

    DBG("Thread %d: Preparing to read %lld UFX lines\n", MYTHREAD, (lld)myShare);

#ifdef DEBUG
    int64_t kmer_count = 0;
#endif

    /* Initialize lookup-table --> necessary for packing routines */
    init_LookupTable();
    char **kmersarr;
    int *counts;
    char *lefts;
    char *rights;

    start_read = UPC_TICKS_NOW();

    int64_t kmers_read = UFXRead(&kmersarr, &counts, &lefts, &rights, myShare, dmin, errorRate, 0, MYTHREAD, kmer_len, UFX_f);
    if (kmers_read < 0) {
        DIE("There was a problem reading the UFX: UFXRead returned %lld", (lld) kmers_read);
    }
    LOGF("Read %lld kmers from UFX file\n", (lld)kmers_read);

    assert(kmers_read == myShare);

    UPC_LOGGED_BARRIER;
    serial_printf("Threads done with I/O\n");

    end_read = UPC_TICKS_NOW();
    if (_sv) {
        MYSV.fileIOTime = UPC_TICKS_TO_SECS(end_read - start_read);
    }
    LOGF("Reading the ufx took: %f seconds\n", UPC_TICKS_TO_SECS(end_read - start_read));

    my_heap_sizes = (int64_t *)calloc_chk(THREADS, sizeof(int64_t));

    start_calculation = UPC_TICKS_NOW();
    /* calculate the exact number of UFX entries for each thread */
    /* previously estimated as EXPANSION_FACTOR * (size + size % THREADS) / THREADS */
    idx = 0;
    assert(kmer_len < MAX_KMER_SIZE);
    char rc_kmer[MAX_KMER_SIZE];
    memset(rc_kmer, 0, MAX_KMER_SIZE); // initialize all bytes
    rc_kmer[kmer_len] = '\0';
    int is_least;

    for (ptr = 0; ptr < kmers_read; ptr++) {
        reverseComplementKmer(kmersarr[ptr], rc_kmer, kmer_len);
        is_least = (strcmp(kmersarr[ptr], rc_kmer) > 0) ? 0  : 1;

        if (is_least && (lefts[ptr] != 'F') && (lefts[ptr] != 'X') && (rights[ptr] != 'F') && (rights[ptr] != 'X')) {
            hashval = hashkmer(load_factor * size, (char *)kmersarr[ptr], kmer_len);
            remote_thread = hashval % (THREADS * BS);
            my_heap_sizes[remote_thread]++;
            ufx_remote_thread[idx] = remote_thread;
        } else {
            ufx_remote_thread[idx] = -1;
        }
        idx++;
    }
    LOGF("Passed through all my kmers\n");

    /* Handle the kmers from the contigs */
    Buffer _contigBuffer = initBuffer(MAX_CONTIG_SIZE);
    int contigLen;
    int64_t nMersProcessed = 0;

    while (gzgetsBuffer(_contigBuffer, MAX_CONTIG_SIZE, contigFd) != NULL) {
        /* Read a contig and its length */
        char *contigBuffer = getStartBuffer(_contigBuffer);
        DBG2("Read %s\n", contigBuffer);
        if (contigBuffer[0] != '>') {
            DIE("Improper fasta file (expected header), reading %.60s\n", contigBuffer);
        }
        resetBuffer(_contigBuffer);
        if (!gzgetsBuffer(_contigBuffer, MAX_CONTIG_SIZE, contigFd)) {
            break;
        }
        chompBuffer(_contigBuffer);
        contigLen = getLengthBuffer(_contigBuffer);
        contigBuffer = getStartBuffer(_contigBuffer);

        int64_t nMers = 0;
        for (i = 1; i < contigLen - kmer_len; i++) {
            reverseComplementKmer(contigBuffer + i, rc_kmer, kmer_len);
            is_least = 0;
            is_least = (strcmp(contigBuffer + i, rc_kmer) > 0) ? 0  : 1;

            if (is_least) {
                hashval = hashkmer(load_factor * size, (char *)contigBuffer + i, kmer_len);
            } else {
                hashval = hashkmer(load_factor * size, rc_kmer, kmer_len);
            }
            remote_thread = hashval % (THREADS * BS);
            my_heap_sizes[remote_thread]++;
            ufx_remote_thread[idx] = remote_thread;
            idx++;
            nMersProcessed++;
            nMers++;
        }
        resetBuffer(_contigBuffer);
    }
    LOGF("Processed kmers from contig.  nMersProcessed=%lld\n", (lld)nMersProcessed);

    GZIP_REWIND(contigFd);

    assert(idx == my_ufx_lines);

    /* now atomically add all my_sizes to global size heap */
    /* TODO replace with all reduce? */
    for (i = MYTHREAD; i < THREADS + MYTHREAD; i++) {
        DBG("Thread %d: sending %ld to thread %d\n", MYTHREAD, my_heap_sizes[i % THREADS], (int)(i % THREADS));
        assert(upc_threadof(heap_sizes + (i % THREADS)) == (i % THREADS));
        if (my_heap_sizes[i % THREADS] > 0) {
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, heap_sizes + (i % THREADS), my_heap_sizes[i % THREADS]);
        }
    }
    free_chk(my_heap_sizes);
    UPC_LOGGED_BARRIER;
    assert(upc_threadof(heap_sizes + MYTHREAD) == MYTHREAD);
    my_heap_size = heap_sizes[MYTHREAD];
    UPC_ALL_FREE_CHK(heap_sizes); // should clean up after last thread gets here
    DBG("Thread %d: allocating memory for %lld kmers\n", MYTHREAD, (lld)my_heap_size);

    end_calculation = UPC_TICKS_NOW();
    if (_sv) {
        MYSV.cardCalcTime = UPC_TICKS_TO_SECS(end_calculation - start_calculation);
    }

    start_setup = UPC_TICKS_NOW();

    /* Create and initialize hashtable */
    SLOG("Creating hashtable with room for %lld entries (my_heap_size=%lld)\n", (lld)size, (lld)my_heap_size);
    dist_hashtable = CREATE_HASH_TABLE(size * load_factor, &memory_heap, my_heap_size);
    UPC_LOGGED_BARRIER;

    end_setup = UPC_TICKS_NOW();
    if (_sv) {
        MYSV.setupTime = UPC_TICKS_TO_SECS(end_setup - start_setup);
    }

    serial_printf("Threads done with setting-up\n");

    start_storing = UPC_TICKS_NOW();
    start_timer = start_storing;

    ptr = 0;
    idx = 0;
    LIST_T new_entry;

    /* Initialize counts to zero */
    new_entry.lefts[0] = 0;
    new_entry.lefts[1] = 0;
    new_entry.lefts[2] = 0;
    new_entry.lefts[3] = 0;
    new_entry.rights[0] = 0;
    new_entry.rights[1] = 0;
    new_entry.rights[2] = 0;
    new_entry.rights[3] = 0;
    new_entry.hasContigConflicts = 0;

    UPC_INT64_T store_pos;
    char exts[2];

    while (ptr < kmers_read) {
        /* Set end of string chars to the appropriate positions:       */
        /* Pointer: working_buffer+ptr                ---> current kmer        */
        /* Pointer: working_buffer+ptr+kmer_len+1  ---> current extensions  */
        if (ufx_remote_thread[idx] >= 0) {
            // get cached remote thread
            remote_thread = ufx_remote_thread[idx];

#ifdef DEBUG
            /* if DEBUG, verify the calculated remote thread is actually the correct one */
            hashval = hashkmer(dist_hashtable->size, (char *)kmersarr[ptr], kmer_len);
            assert(remote_thread == upc_threadof(&(dist_hashtable->table[hashval])));
#endif
            packSequence((unsigned char *)(kmersarr[ptr]), new_entry.packed_key, kmer_len);

            exts[0] = lefts[ptr];
            exts[1] = rights[ptr];
            new_entry.packed_extensions = convertExtensionsToPackedCode((unsigned char *)exts);
            new_entry.used_flag = -1;
            new_entry.my_contig = NULL;
            new_entry.myCount = counts[ptr];
            new_entry.next = NULL;

            ADD_TO_HEAP(&memory_heap, new_entry, remote_thread);
#ifdef DEBUG
            kmer_count++;
#endif
        } else {
            DBG("Thread %d: Skipping %.*s\n", MYTHREAD, kmer_len + 3, kmersarr[ptr]);
        }

        ptr++;
        idx++;
    }

    FINAL_FLUSH_HEAP(&memory_heap);

    UPC_LOGGED_BARRIER;
    end_timer = UPC_TICKS_NOW();
    LOGF("Time for storing the original UFX entries in the hash table is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    upc_fence;

    /* Entries in local shared heap "after" watermark are due to k-mers from contigs */
    int64_t watermark = memory_heap.heap_struct[MYTHREAD].len;

    UPC_LOGGED_BARRIER;
    start_timer = UPC_TICKS_NOW();
    /* Read prefix merDepth file */
    int64_t prefixBufSize = nPrevMersFromContigs * sizeof(int64_t);
    int64_t *prefixBuf = (int64_t *)malloc_chk(nPrevMersFromContigs * sizeof(int64_t));
    int res = 0;
    if (prefixBufSize) {
        GZIP_FREAD(prefixBuf, prefixBufSize, 1, prefixMerdepthsFile);
    }
    int64_t *curPrefixBuf;
    int64_t offsetInBuf = 0;
    int64_t depthSum;
    resetBuffer(_contigBuffer);

    serial_printf("Reading contigs\n");

    while (gzgetsBuffer(_contigBuffer, MAX_CONTIG_SIZE, contigFd) != NULL) {
        char *contigBuffer = getStartBuffer(_contigBuffer);
        DBG2("Read %s\n", contigBuffer);
        if (contigBuffer[0] != '>') {
            DIE("Improper fasta file (expected header), reading %.60s\n", contigBuffer);
        }
        resetBuffer(_contigBuffer);
        /* Read a contig and its length */
        if (!gzgetsBuffer(_contigBuffer, MAX_CONTIG_SIZE, contigFd)) {
            break;
        }
        chompBuffer(_contigBuffer);
        contigLen = getLengthBuffer(_contigBuffer);
        contigBuffer = getStartBuffer(_contigBuffer);

        curPrefixBuf = (int64_t *)&prefixBuf[offsetInBuf];
        offsetInBuf += contigLen - prev_k + 1;

        for (i = 1; i < contigLen - kmer_len; i++) {
            depthSum = curPrefixBuf[i + kmer_len - prev_k] - curPrefixBuf[i - 1];
            remote_thread = ufx_remote_thread[idx];
            reverseComplementKmer(contigBuffer + i, rc_kmer, kmer_len);
            is_least = (strcmp(contigBuffer + i, rc_kmer) > 0) ? 0  : 1;
            if (is_least) {
                packSequence((unsigned char *)contigBuffer + i, new_entry.packed_key, kmer_len);
            } else {
                packSequence((unsigned char *)rc_kmer, new_entry.packed_key, kmer_len);
            }

            if (is_least) {
                exts[0] = contigBuffer[i - 1];
                exts[1] = contigBuffer[i + kmer_len];
            } else {
                exts[1] = reverseComplementBaseStrict(contigBuffer[i - 1]);
                exts[0] = reverseComplementBaseStrict(contigBuffer[i + kmer_len]);
            }
            new_entry.packed_extensions = convertExtensionsToPackedCode((unsigned char *)exts);
            /* WARNING: In the used flag field now we store the kmer depth (count) !!! */
            new_entry.used_flag = (int)((depthSum + kmer_len - prev_k) / (kmer_len - prev_k + 1));
            new_entry.my_contig = NULL;
            new_entry.next = NULL;

            ADD_TO_HEAP(&memory_heap, new_entry, remote_thread);
            idx++;
        }
        resetBuffer(_contigBuffer);
    }
    freeBuffer(_contigBuffer);

    GZIP_REWIND(contigFd);
    assert(idx == my_ufx_lines);

    free_chk(ufx_remote_thread);

    FINAL_FLUSH_HEAP(&memory_heap);
    FREE_LOCAL_BUFFERS(&memory_heap);

    UPC_LOGGED_BARRIER;
    end_timer = UPC_TICKS_NOW();
    LOGF("Time for storing the newKmer entries in the hash table is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));

    start_timer = UPC_TICKS_NOW();

    serial_printf("Adding to local part of hashtable\n");

    // assert( memory_heap.heap_struct[MYTHREAD].len == my_heap_size );
    /* Now each thread will iterate over its local heap and will add nodes to the local part of the hashtable - NO NEED FOR LOCKS!!! */
    int64_t heap_entry;

    LIST_T *local_filled_heap = (LIST_T *)memory_heap.cached_heap_ptrs[MYTHREAD];
    int64_t hashTableSize = dist_hashtable->size;
    shared[] LIST_T * shared_local_filled_heap = (memory_heap.heap_struct[MYTHREAD].ptr);
    unsigned char unpacked_kmer[MAX_KMER_SIZE];
    unpacked_kmer[kmer_len] = '\0';

    LIST_T *result, *resultFromList, *prevPtr;
    int found, foundInList;
    shared[] LIST_T * newKmersList = NULL;
    int64_t nNewFound = 0;
    int64_t mersInMyHeap = memory_heap.heap_struct[MYTHREAD].len;

    shared[] LIST_T * newMersPool = NULL;
    LOGF("allocting newMersPool %0.3f MB (%lld LIST_T)\n", (mersInMyHeap - watermark + 1) * sizeof(LIST_T) * 1.0 / ONE_MB, (lld)(mersInMyHeap - watermark + 1));
    UPC_ALLOC_CHK(newMersPool, (mersInMyHeap - watermark + 1) * sizeof(LIST_T));
    (*freeList) = newMersPool;
    int64_t posInPool = 0;
    int64_t conflictsWithUFX = 0;
    int64_t conflictsWithContigMers = 0;
    kmer_and_ext_t cur_kmer;
    int64_t coherent_kmers = 0;
    int64_t incoherent_kmers = 0;

    LOGF("Iterating over %lld mersInMyHeap\n", (lld)mersInMyHeap);
    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
    for (heap_entry = 0; heap_entry < mersInMyHeap; heap_entry++) {
        unpackSequence((unsigned char *)&(local_filled_heap[heap_entry].packed_key), (unsigned char *)unpacked_kmer, kmer_len);
        hashval = hashkmer(hashTableSize, (char *)(unpacked_kmer), kmer_len);

        /* First make sure that the entry is not there (an entry might be there due to either (1) ufx file or (2) kmers generated by contigs */
        found = 0;
        result = (LIST_T *)dist_hashtable->table[hashval].head;

        for (; result != NULL; ) {
            if (comparePackedSeq((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)result->packed_key, kmer_packed_len) == 0) {
                found = 1;
                break;
            }
            result = (LIST_T *)result->next;
        }

        if (found == 0) {
            if (heap_entry < watermark) {
                /* Entry is from UFX, just add it in the hash table */
                local_filled_heap[heap_entry].next = dist_hashtable->table[hashval].head;
                dist_hashtable->table[hashval].head = (shared[] LIST_T *) & (shared_local_filled_heap[heap_entry]);
            } else {
                /* Make a copy of the mer for the new mers list */
                newMersPool[posInPool] = shared_local_filled_heap[heap_entry];

                /* Add counts to the appropriate base */
                UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY((LIST_T *)&newMersPool[posInPool], &cur_kmer, kmer_len);
                simpleSetPosInCounts(*getLeftExt(&cur_kmer), (int *)newMersPool[posInPool].lefts, newMersPool[posInPool].used_flag);
                simpleSetPosInCounts(*getRightExt(&cur_kmer, kmer_len), (int *)newMersPool[posInPool].rights, newMersPool[posInPool].used_flag);

                /* Add it in the hash table first */
                local_filled_heap[heap_entry].next = dist_hashtable->table[hashval].head;
                dist_hashtable->table[hashval].head = (shared[] LIST_T *) & (shared_local_filled_heap[heap_entry]);

                /* Append the extracted k-mer in the list of new kmers */
                newMersPool[posInPool].next = newKmersList;
                newKmersList = (shared[] LIST_T *) & (newMersPool[posInPool]);
                //printf("Kmer depth is %d\n", (int) local_filled_heap[heap_entry].used_flag );
                nNewFound++;
                posInPool++;
            }
        } else {
            if (local_filled_heap[heap_entry].packed_extensions != result->packed_extensions) {
                //printf("WARNING: Thread %d found inconsistent occurencies of the same kmer!!!\n", MYTHREAD);
                if (result->used_flag == -1) {
                    //DBG2("Found inconsistent extensions, using result from UFX (kmer %s) UFX depth is %d and superRead depth is %d ...\n",unpacked_kmer, result->myCount, local_filled_heap[heap_entry].used_flag);
                    /* Essentially we prioritize the entry coming from the UFX */
                    conflictsWithUFX++;
                }

#ifdef FAIR_COUNTING
                else {
                    /* Add counts appropriately... */

                    /* First, find entry in new kmers list */
                    resultFromList = (LIST_T *)newKmersList;
                    foundInList = 0;

                    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
                    for (; resultFromList != NULL; ) {
                        if (comparePackedSeq((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)resultFromList->packed_key, kmer_packed_len) == 0) {
                            foundInList = 1;
                            break;
                        }
                        resultFromList = (LIST_T *)resultFromList->next;
                    }

                    if (foundInList == 1) {
                        /* Adjust depth first */
                        resultFromList->used_flag += local_filled_heap[heap_entry].used_flag;
                        resultFromList->hasContigConflicts = 1;
                        incoherent_kmers++;
                        /* Add counts to the appropriate base */
                        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY((LIST_T *)&local_filled_heap[heap_entry], &cur_kmer, kmer_len);
                        simpleSetPosInCounts(*getLeftExt(&cur_kmer), resultFromList->lefts, local_filled_heap[heap_entry].used_flag);
                        simpleSetPosInCounts(*getRightExt(&cur_kmer, kmer_len), resultFromList->rights, local_filled_heap[heap_entry].used_flag);
                    }
                }
#endif

#ifdef PRIORITIZE
                else {
                    /* Prioritize the k-mer with LOWER depth  */
                    if (local_filled_heap[heap_entry].used_flag > result->used_flag) {
                        /* Adjust appropriately the hash table */
                        DBG("Found inconsistent extensions, using result with depth %d instead of depth %d ...\n", local_filled_heap[heap_entry].used_flag, result->used_flag);
                        result->used_flag = local_filled_heap[heap_entry].used_flag;
                        result->packed_extensions = local_filled_heap[heap_entry].packed_extensions;

                        /* Adjust appropriately the entry in the newKmers list */
                        resultFromList = (LIST_T *)newKmersList;
                        foundInList = 0;

                        int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
                        for (; resultFromList != NULL; ) {
                            if (comparePackedSeq((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)resultFromList->packed_key, kmer_packed_len) == 0) {
                                foundInList = 1;
                                break;
                            }
                            resultFromList = (LIST_T *)resultFromList->next;
                        }

                        if (foundInList) {
                            resultFromList->used_flag = local_filled_heap[heap_entry].used_flag;
                            resultFromList->packed_extensions = local_filled_heap[heap_entry].packed_extensions;
                        }
                    }
                }
#endif

#ifdef STRICT_MERS
                else if (result->used_flag == -2) {
                    //DBG2("Found inconsistent extensions for MULTIPLE times for same kmer %s...\n", unpacked_kmer);
                } else {
                    /* The entry is also extracted from contigs. Therefore we should remove it from the newKmers list */
                    //DBG2("Found inconsistent extensions, BOTH are from previous contigs (kmer %s)...\n", unpacked_kmer);
                    conflictsWithContigMers++;

                    foundInList = 0;
                    prevPtr = NULL;
                    resultFromList = (LIST_T *)newKmersList;

                    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
                    for (; resultFromList != NULL; ) {
                        if (comparePackedSeq((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)resultFromList->packed_key, kmer_packed_len) == 0) {
                            foundInList = 1;
                            break;
                        }
                        prevPtr = resultFromList;
                        resultFromList = (LIST_T *)resultFromList->next;
                    }

                    if (foundInList == 1) {
                        if (prevPtr == NULL) {
                            /* Case where the entry to be removed is in the head of new Kmers list */
                            newKmersList = newKmersList->next;
                        } else {
                            /* Just skip the entry to be removed */
                            prevPtr->next = resultFromList->next;
                        }
                        nNewFound--;
                        result->used_flag = -2;
                    } else {
                        printf("Fatal ERROR (%d) !!!!\n", MYTHREAD);
                    }
                }
#endif
            }

            if (local_filled_heap[heap_entry].packed_extensions == result->packed_extensions) {
                if ((local_filled_heap[heap_entry].used_flag != -1) && (result->used_flag != -1)) {
                    //DBG2("COHERENT Kmer that arises from multiple contigs has been detected !!!\n");
                    coherent_kmers++;
                }
            }
        }
    }

    UPC_LOGGED_BARRIER;
    end_timer = UPC_TICKS_NOW();
    LOGF("Time for doing the filtering is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));


#ifdef FAIR_COUNTING

    resultFromList = (LIST_T *)newKmersList;
    double given_error = 1.0 - errorRate;

    /* Check how the results would look like if we applied error_rate 0.2, 0.3, 0.4 and apply as if the results were 0.4 */
    for (; resultFromList != NULL; ) {
        if (resultFromList->hasContigConflicts == 1) {
            DBG("Kmer with conflicts that arise from multiple contigs has been detected:\n");
            printDesignation(resultFromList, given_error, dmin);
            printDesignation(resultFromList, 2 * given_error, dmin);
            printDesignation(resultFromList, 3 * given_error, dmin);
            printDesignation(resultFromList, 4 * given_error, dmin);
            adjustDesignation(resultFromList, given_error, dmin);
        }

        resultFromList = (LIST_T *)resultFromList->next;
    }

    UPC_LOGGED_BARRIER;
#endif

    (*resultList) = (LIST_T *)newKmersList;
    (*nNewMers) = nNewFound;
    //DBG2("Found %lld conflicts with UFX\n", (lld) conflictsWithUFX);
    //DBG2("Found %lld conflicts with contig-Mers\n", (lld) conflictsWithContigMers);
    //DBG2("Found %lld new Mers\n", (lld) nNewFound);
    //DBG2("Found %lld COHERENT Mers from contigs\n", (lld) coherent_kmers);
    //DBG2("Found %lld IN-COHERENT Mers from contigs\n", (lld) incoherent_kmers);


    end_storing = UPC_TICKS_NOW();
    if (_sv) {
        MYSV.storeTime = UPC_TICKS_TO_SECS(end_storing - start_storing);
    }

#ifdef PROFILE
    UPC_LOGGED_BARRIER;

    if (MYTHREAD == 0) {
        printf("\n************* SET - UP TIME *****************");
        printf("\nTime spent on setting up the distributed hash table is %f seconds\n", UPC_TICKS_TO_SECS(end_setup - start_setup));

#ifdef DETAILED_IO_PROFILING
        printf("\n\n************* DETAILED TIMINGS FOR I/O AND STORING KMERS *****************\n");
#endif
    }

    UPC_LOGGED_BARRIER;

#ifdef DETAILED_IO_PROFILING

    printf("Thread %d spent %f seconds on read I/O and %f seconds on storing %lld kmers\n", MYTHREAD, UPC_TICKS_TO_SECS(end_read - start_read), UPC_TICKS_TO_SECS(end_storing - start_storing));

#endif

    UPC_LOGGED_BARRIER;

    DeAllocateAll(&kmersarr, &counts, &lefts, &rights, kmers_read);

#ifdef BUCKET_BALANCE_PROFILING
    if (MYTHREAD == 0) {
        printf("\n--------------------------------\n");
        printf("--------- Bucket balance -------\n");
        printf("--------------------------------\n");
    }
    if (MYTHREAD == 0) {
        for (i = 0; i < THREADS; i++) {
            printf("Thread %lld has %lld elements in its buckets\n", i, (lld) memory_heap.heap_struct[i].len);
        }
    }
#endif

#endif

    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        printf("Threads have created distributed hash table\n");
    }

    if (prefixBuf) {
        free_chk(prefixBuf);
    }
    (*memory_heap_res) = memory_heap;
    return dist_hashtable;
}
