FIRST LOG IN AT AN INTERACTIVE NODE ON EDISON with:   
qsub -I -V -q debug -l mppwidth=24

0) (On Edison) module load git

1) git clone https://github.com/egeor/par_meraculous_v0.1.git

================
Compilation process
================	
1)	(For Edison) source loadModules

2)	make all [ KMER_LENGTH=21 ]

you may choose to build several different kmer sizes

=================
Execution process
=================
For par_meraculous_v0.1 the input file should be a single UFX file with N k-mers (each line in the UFX file should be in the format “k-mer    two_letter_code”).
The executable has name meraculous.

First for Edison execute:

1) source loadModules

Then execute:

2) (optional)
  upcrun -n 1 ./mem_reporter-$KMER_LENGTH P the_UFX_text_file 

where:

P: the available number of processors

The mem_reporter will suggest a value H.

Then execute:

3) upcrun -n P -shared-heap=H ./meraculous-$KMER_LENGTH -m minimum_contig_length -i absolute_path_of_input_UFX_file -o name_of_output_files 

where:

P: the available number of processors

H: the value H suggested by mem_reporter

minimum_contig_length: the minimum required length for a contig to be stored 

absolute_path_of_input_UFX_file: the absolute path of the single input UFX file

name_of_output_files: the suffix of the output file names

The output are P files containing the desired UU-contigs.
