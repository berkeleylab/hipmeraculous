#include "findNewMers.h"

int findNewMers_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

#ifdef LHS_PERF
    lookups = 0;
    success = 0;
#endif

    int kmer_len = MAX_KMER_SIZE - 1;
    myContigs = 0; contig_id = 0; // required to link properly
    if (_sv != NULL) {
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    int i;
    int64_t size, original_size;
    HASH_TABLE_T *dist_hashtable;
    MEMORY_HEAP_T memory_heap;
    FILE *fd;
    const char *base_dir = ".";

    shared[1] int64_t * myNmersFromContigs = NULL;
    UPC_ALL_ALLOC_CHK(myNmersFromContigs, THREADS, sizeof(int64_t));

    int c;
    char *input_UFX_name;
    int dmin = 10;
    int chunk_size = 1;
    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:d:C:l:B:N:p:D:k:X");
    print_args(optList, __func__);
    int load_factor = 1;
    int cores_per_node = 0;
    char *contig_file_name;
    int prev_k;
    double dynamicMinDepth = 1.0;
    int is_per_thread = 1;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'k':
            kmer_len = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("This executable only supports kmer_len (%d) up to (%d).  Please compile with a larger -DHIPMER_MAX_KMER_SIZE\n", kmer_len, MAX_KMER_SIZE);
            }
            break;
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'p':
            prev_k = atoi(thisOpt->argument);
            break;
        case 'C':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'l':
            load_factor = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        case 'N':
            cores_per_node = atoi(thisOpt->argument);
            SET_CORES_PER_NODE(cores_per_node);
            break;
        case 'D':
            dynamicMinDepth = atof(thisOpt->argument);
            break;
        default:
            break;
        }
    }

    UPC_TICK_T start, end, timer_start, timer_end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
        printf("Compiled with MAX_KMER_SIZE %d\n", MAX_KMER_SIZE);
        printf("Running with %d cores per node\n", cores_per_node);
    }

    double con_time;

#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif
    /* Build UFX hashtable */

    int64_t myshare;
    int dsize;

    if (MYSV.checkpoint_path) {
        serial_printf("Forcing a global checkpoint of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        // force a restore from previous checkpoint, if needed
        if (!doesLocalCheckpointExist(tmp)) {
            restoreLocalCheckpoint(tmp);
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) {
        DIE("Could not load UFX: %s is_per_thread: %d base_dir: %s!  %s\n", input_UFX_name, is_per_thread, base_dir, strerror(errno));
    }
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    original_size = size;
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more total memory / nodes\n", (lld)minMemory, (lld)size);
    }

    /* Add logic to handle input Contig files */
    char my_contig_file_name[MAX_FILE_PATH];
    sprintf(my_contig_file_name, "%s.fasta" GZIP_EXT, contig_file_name);
    timer_start = UPC_TICKS_NOW();

    GZIP_FILE contigFile = openCheckpoint(my_contig_file_name, "r");

    Buffer contigBuffer = initBuffer(MAX_CONTIG_SIZE_2);
    int contigLen;
    int64_t nMers;
    int64_t nMersFromContigs = 0;
    int64_t nPrevMersFromContigs = 0;

    while (gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE_2, contigFile) != NULL) {
        DBG2("Read %s\n", getStartBuffer(contigBuffer));
        if (contigBuffer->buf[0] != '>') {
            DIE("Improper fasta file (expected header), reading %.60s from %s\n", getStartBuffer(contigBuffer), my_contig_file_name);
        }
        resetBuffer(contigBuffer);
        /* Read a contig and its length */
        if (!gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE_2, contigFile)) {
            break;
        }
        if (contigBuffer->buf[0] == '>') {
            DIE("Improper fasta file (expected sequence), reading %.60s from %s\n", getStartBuffer(contigBuffer), my_contig_file_name);
        }
        chompBuffer(contigBuffer);
        contigLen = getLengthBuffer(contigBuffer);
        if (contigLen < prev_k) {
            DIE("Invalid contig of size %lld: %s\n", (lld)contigLen, my_contig_file_name);
        }
        if (contigLen >= MAX_CONTIG_SIZE_2) {
            LOGF("You would have missed a long contig without using Buffer.h: %lld vs %lld MAX_CONTIG_SIZE_2\n", (lld)contigLen, (lld)MAX_CONTIG_SIZE_2);
        }
        /* We will not use the first and the last kmer since we dont know their extensions... */
        nMers = contigLen - kmer_len - 1;
        if (nMers < 0) {
            nMers = 0;
        }

        nPrevMersFromContigs += contigLen - prev_k + 1;
        nMersFromContigs += nMers;
        resetBuffer(contigBuffer);
    }
    LOGF("nPrevMersFromContigs = %lld, nMersFromContigs = %lld\n", (lld)nPrevMersFromContigs, (lld)nMersFromContigs);
    freeBuffer(contigBuffer);

    GZIP_REWIND(contigFile);

    myNmersFromContigs[MYTHREAD] = nMersFromContigs;
    upc_fence;
    UPC_LOGGED_BARRIER;
    timer_end = UPC_TICKS_NOW();
    serial_printf("Time to read contigs and calculate cardinality of inherited kmers is: %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = timer_end;

    for (i = 0; i < THREADS; i++) {
        size += myNmersFromContigs[i];
    }

    timer_end = UPC_TICKS_NOW();
    serial_printf("Time for naive all to all reduction is: %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = timer_end;

    LIST_T *newKmerList;
    int64_t nNewKmers = 0;
    shared[] LIST_T * freeList;

    char my_prefixSumDepth_file_name[MAX_FILE_PATH];
    sprintf(my_prefixSumDepth_file_name, "merDepth_%s.bin" GZIP_EXT, contig_file_name);
    GZIP_FILE prefixSumDepthFile = openCheckpoint(my_prefixSumDepth_file_name, "r");

    dist_hashtable = BUILD_UFX_HASH_NEW(size, contigFile, &memory_heap, myshare, nMersFromContigs, dsize, dmin, dynamicMinDepth, chunk_size, load_factor, &newKmerList, &freeList, &nNewKmers, prefixSumDepthFile, nPrevMersFromContigs, prev_k, kmer_len, UFX_f);

    closeCheckpoint(prefixSumDepthFile);
    closeCheckpoint(contigFile);

    //printf("Thread %d found %lld new k-mers, size of new local mer-set is %lld Mbytes\n", MYTHREAD, (lld) nNewKmers, (lld) nNewKmers * (KMER_PACKED_LENGTH+1) / (1024*1024));

    /* Now we traverse the list of new k-mers and will be appending them to the existing UFX file */

    char *newEntries = (char *)malloc_chk(nNewKmers * (kmer_len + 2) * sizeof(char));
    int *newEntriesDepths = (int *)malloc_chk(nNewKmers * sizeof(int));

    int *leftCounts = (int *)malloc_chk(nNewKmers * 4 * sizeof(int));
    int *rightCounts = (int *)malloc_chk(nNewKmers * 4 * sizeof(int));


    myNmersFromContigs[MYTHREAD] = nNewKmers;

    UPC_LOGGED_BARRIER;
    timer_end = UPC_TICKS_NOW();
    serial_printf("Added the kmers in the hash table in: %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = timer_end;
    // serial_printf("Added the kmers in the hash table...\n");

    int64_t my_offset = 0;

#ifdef UFX_WRITE_SINGLE_FILE
    if (!is_per_thread) {
        if (MYTHREAD == 0) {
            int64_t prev, sum, final_size;
            sum = original_size;
            prev = myNmersFromContigs[0];
            myNmersFromContigs[0] = sum;
            for (i = 1; i < THREADS; i++) {
                sum += prev;
                prev = myNmersFromContigs[i];
                myNmersFromContigs[i] = sum;
            }
            final_size = sum + prev;

            if (!is_per_thread) {
                assert(UFX_f.f != NULL);
                assert(UFX_f.gz == NULL);
                assert(final_size * dsize >= get_file_size(input_UFX_name));
                if (ftruncate(fileno(UFX_f.f), final_size * dsize) == -1) {
                    DIE("Could not truncate %s: %s\n", input_UFX_name, strerror(errno));
                }
            }
            serial_printf("Truncated the UFX file to %lld (%lld entries).\n", (lld)final_size * (lld)dsize, (lld)final_size);
        }

        UPC_LOGGED_BARRIER;

        my_offset = myNmersFromContigs[MYTHREAD] * dsize;
    } else
#endif
    {

        shared int64_t *tot_num_entries = NULL;
        UPC_ALL_ALLOC_CHK(tot_num_entries, 1, sizeof(int64_t));
        // must write the .entries files to update the total number of kmers
        if (!MYTHREAD) {
            *tot_num_entries = original_size;
            for (int i = 0; i < THREADS; i++) {
                (*tot_num_entries) += myNmersFromContigs[i];
            }
        }
        UPC_LOGGED_BARRIER;
        // now each thread writes out the sizes
        char fname1[MAX_FILE_PATH];
        sprintf(fname1, "%s" GZIP_EXT ".entries", input_UFX_name); // remember UFX on is_pre_thread is compressed
        serial_printf("saving entries to %s\n", fname1);
        FILE *f = openCheckpoint0(fname1, "w");
        fprintf(f, "%lld\n", (lld)nNewKmers + myshare);
        closeCheckpoint0(f);
        UPC_LOGGED_BARRIER;
        UPC_ALL_FREE_CHK(tot_num_entries);
    }

    int64_t pos = 0;
    int64_t posDepths = 0;
    int64_t posCounts = 0;

    LIST_T *cur_entry = newKmerList;
    kmer_and_ext_t cur_kmer;

    assert(kmer_len < MAX_KMER_SIZE);
    char auxKmer[MAX_KMER_SIZE];
    memset(auxKmer, 0, MAX_KMER_SIZE); // initialize bytes
    auxKmer[kmer_len] = '\0';

    while (cur_entry != NULL) {
        newEntriesDepths[posDepths] = cur_entry->used_flag;
        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(cur_entry, &cur_kmer, kmer_len);
        memcpy((char *)&newEntries[pos], getKmer(&cur_kmer), kmer_len * sizeof(unsigned char));

        newEntries[pos + kmer_len] = *getLeftExt(&cur_kmer);
        newEntries[pos + kmer_len + 1] = *getRightExt(&cur_kmer, kmer_len);

        leftCounts[posCounts] = cur_entry->lefts[0];
        leftCounts[posCounts + 1] = cur_entry->lefts[1];
        leftCounts[posCounts + 2] = cur_entry->lefts[2];
        leftCounts[posCounts + 3] = cur_entry->lefts[3];

        rightCounts[posCounts] = cur_entry->rights[0];
        rightCounts[posCounts + 1] = cur_entry->rights[1];
        rightCounts[posCounts + 2] = cur_entry->rights[2];
        rightCounts[posCounts + 3] = cur_entry->rights[3];

        posCounts += 4;
        posDepths++;
        pos += kmer_len + 2;
        cur_entry = (LIST_T *)cur_entry->next;
    }

    timer_end = UPC_TICKS_NOW();
    serial_printf("Prepared list of new kmers for writing in: %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = timer_end;

    UFXClose(UFX_f);

    int status = writeNewUFX(input_UFX_name, newEntriesDepths, newEntries, nNewKmers, MYTHREAD, base_dir, leftCounts, rightCounts,
                             kmer_len, dmin, dynamicMinDepth);
    if (status != 0) {
        DIE("writeNewUFX failed! %s\n", strerror(errno));
    }

    if (MYSV.checkpoint_path) {
        serial_printf("Forcing a global checkpoint of the new per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        unlinkGlobalCheckpoint(tmp);
        restoreCheckpoint(tmp);
        strcat(tmp, ".entries");
        unlinkGlobalCheckpoint(tmp);
        restoreCheckpoint(tmp);
    }

    timer_end = UPC_TICKS_NOW();
    serial_printf("Wrote new UFX in: %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));

    /*char outputfile_name[255];
     * sprintf(outputfile_name,"%s/%d-mers_from_%s-merContigs_%d", base_dir, kmer_len, output_name, MYTHREAD);
     * FILE *pFile = fopen_chk(outputfile_name, "wb");
     * fwrite_chk(newEntries, nNewKmers * (KMER_PACKED_LENGTH+1) * sizeof(unsigned char), 1, pFile);
     * fclose_track(pFile);*/
    //printf("Thread %d wrote kmers %lld k-mers with total file size %lld bytes\n", MYTHREAD, (lld) nNewKmers, (lld) nNewKmers * (KMER_PACKED_LENGTH+1) * sizeof(unsigned char) );

    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);

    UPC_ALL_FREE_CHK(myNmersFromContigs);
    UPC_FREE_CHK(freeList);
    if (newEntries) {
        free_chk(newEntries);
    }
    if (newEntriesDepths) {
        free_chk(newEntriesDepths);
    }
    if (leftCounts) {
        free_chk(leftCounts);
    }
    if (rightCounts) {
        free_chk(rightCounts);
    }


#ifdef PROFILE
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();

        con_time = UPC_TICKS_TO_SECS(end - start);

        printf("\nTime for merging DB graphs is : %f seconds\n", con_time);
    }
#endif

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
