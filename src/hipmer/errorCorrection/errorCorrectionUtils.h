#include "upc_compatibility.h"

#ifndef MAX_READ_LEN
#define MAX_READ_LEN DEFAULT_READ_LEN
#endif

#define MAX_CONTIG_SIZE 900000

#define ALIGNMENT_CHUNK_SIZE 100
#define MAX_LINE_SIZE 1000
#define MAX_READ_FILES 20
#define PLUS 0
#define MINUS 1

/* Parses a single alignment */
/* Returns 1 if the alignemnt is valid. Returns 0 if the alignment is a guard alignment */
static inline int splitAlignment(char *input_map, char *read_id, int *qStart, int *qStop, int *qLength, int *subject, int *sStart, int *sStop, int *sLength, int *strand)
{
    char *token;
    char *aux;
    int result = 0;

    token = strtok_r(input_map, "\t", &aux);
    if (strcmp(token, "MERALIGNER-F") == 0) {
        token = strtok_r(NULL, "\t", &aux);
        strcpy(read_id, token);
        return result;
    }

    if (strcmp(token, "MERALIGNER-0") == 0) {
        result = 1;
    }

    if (strcmp(token, "MERALIGNER-1") == 0) {
        result = 2;
    }

    if (strcmp(token, "MERALIGNER-2") == 0) {
        result = 3;
    }

    token = strtok_r(NULL, "\t", &aux);
    strcpy(read_id, token);
    token = strtok_r(NULL, "\t", &aux);
    (*qStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*qStop) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*qLength) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*subject) = atoi(token + 6);
    token = strtok_r(NULL, "\t", &aux);
    (*sStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*sStop) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*sLength) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*strand) = (strcmp(token, "Plus") == 0) ? PLUS : MINUS;

    return result;
}

typedef struct contig_t contig_t;
typedef shared[] contig_t *contig_ptr_t;

struct contig_t {
    int64_t length;
    shared[] char *sequence;
    char *  localSequence;
    int *   baseCount;
    char *  consensus;
};

typedef struct alignment_t alignment_t;
struct alignment_t {
    int64_t local_read_ID;
    int     owner_id;
    char    readSeq[MAX_READ_LEN];
    int     qStart;
    int     qStop;
    int     qLength;
    int     subject;
    int     sStart;
    int     sStop;
    int     sLength;
    int     strand;
    char    basesDiff;
    char    type;
};

typedef shared[] alignment_t *shared_alignment_ptr;

/* Alignment heap data structure */
typedef struct alignment_heap_t alignment_heap_t;
struct alignment_heap_t {
    shared_alignment_ptr *alignment_ptr;           // Pointers to shared memory heaps
    shared[1] int64_t * heap_indices;              // Indices of remote heaps
    shared[1] shared_alignment_ptr * tmp_heap_ptr;
};

/* Creates shared heaps that required for alignment heaps - IMPORTANT: This is a collective function */
int create_alignment_heaps(int64_t size, alignment_heap_t *alignment_heap)
{
    int i;

    UPC_ALL_ALLOC_CHK(alignment_heap->tmp_heap_ptr, THREADS, sizeof(shared_alignment_ptr));
    UPC_ALL_ALLOC_CHK(alignment_heap->heap_indices, THREADS, sizeof(int64_t));

    UPC_ALLOC_CHK(alignment_heap->tmp_heap_ptr[MYTHREAD], size * sizeof(alignment_t));

    alignment_heap->heap_indices[MYTHREAD] = 0;
    upc_fence;

    alignment_heap->alignment_ptr = (shared_alignment_ptr *)malloc_chk(THREADS * sizeof(shared_alignment_ptr));
    upc_barrier;

    for (i = 0; i < THREADS; i++) {
        alignment_heap->alignment_ptr[i] = alignment_heap->tmp_heap_ptr[i];
    }

    return 0;
}

void destroy_alignment_heaps(alignment_heap_t *alignment_heap)
{
    UPC_FREE_CHK(alignment_heap->tmp_heap_ptr[MYTHREAD]);
    UPC_ALL_FREE_CHK(alignment_heap->tmp_heap_ptr);
    UPC_ALL_FREE_CHK(alignment_heap->heap_indices);
}

/* Allocate local arrays used for book-keeping when using aggregated upc_memputs */
int allocate_alignment_local_buffs(alignment_t **local_buffs, int64_t **local_index)
{
    (*local_buffs) = (alignment_t *)malloc_chk(THREADS * ALIGNMENT_CHUNK_SIZE * sizeof(alignment_t));
    (*local_index) = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    memset((*local_index), 0, THREADS * sizeof(int64_t));

    return 0;
}

/* Adds alignment to the appropriate shared alignment heap */
int add_alignment_to_shared_heaps(alignment_t *new_entry, int remote_thread, int64_t *local_index, alignment_t *local_buffs, alignment_heap_t alignment_heap)
{
    int64_t store_pos;

    /* Store alignment first to local buffer designated for remote thread */
    if (local_index[remote_thread] <= ALIGNMENT_CHUNK_SIZE - 1) {
        memcpy(&(local_buffs[local_index[remote_thread] + remote_thread * ALIGNMENT_CHUNK_SIZE]), new_entry, sizeof(alignment_t));
        local_index[remote_thread]++;
    }

    /* If buffer for that thread is full, do a remote upc_memput() */
    if (local_index[remote_thread] == ALIGNMENT_CHUNK_SIZE) {
        UPC_ATOMIC_FADD_I64(&store_pos, &(alignment_heap.heap_indices[remote_thread]), ALIGNMENT_CHUNK_SIZE);
        upc_memput((shared[] alignment_t *)(alignment_heap.alignment_ptr[remote_thread] + store_pos), &(local_buffs[remote_thread * ALIGNMENT_CHUNK_SIZE]), (ALIGNMENT_CHUNK_SIZE)*sizeof(alignment_t));
        local_index[remote_thread] = 0;
    }

    return 0;
}

/* Adds remaining alignments to the shared heaps. */
int add_rest_alignments_to_shared_heaps(int64_t *local_index, alignment_t *local_buffs, alignment_heap_t alignment_heap)
{
    int i;
    int64_t store_pos;

    for (i = 0; i < THREADS; i++) {
        if (local_index[i] != 0) {
            UPC_ATOMIC_FADD_I64(&store_pos, &(alignment_heap.heap_indices[i]), local_index[i]);
            upc_memput((shared[] alignment_t *)((alignment_heap.alignment_ptr[i]) + store_pos), &(local_buffs[i * ALIGNMENT_CHUNK_SIZE]), (local_index[i]) * sizeof(alignment_t));
        }
    }

    return 0;
}

int findBaseScorePos(char base, int pos_in_contig)
{
    int result;

    if (base == 'A') {
        result = pos_in_contig * 4;
    } else if (base == 'C') {
        result = pos_in_contig * 4 + 1;
    } else if (base == 'G') {
        result = pos_in_contig * 4 + 2;
    } else if (base == 'T') {
        result = pos_in_contig * 4 + 3;
    }

    return result;
}

char reverseComplementBaseStrict(const char base)
{
    char rc;

    switch (base) {
    case 'A':
        rc = 'T'; break;
    case 'C':
        rc = 'G'; break;
    case 'G':
        rc = 'C'; break;
    case 'T':
        rc = 'A'; break;
    default:
        DIE("Unexpected base in revereseComplementBaseStrict: %c %d\n", base, (int)base);
    }
    return rc;
}

/* Based on the read to contig alignment, we vote on the aligned bases of the contig */
char countBases(alignment_t a, char *seq, int *scores)
{
    int i_q, i_s;
    char differences = 0;
    char cur_base;
    char rc_cur_base;
    int actual_pos = 0;

    if (a.strand == PLUS) {
        i_s = a.sStart - 1;
        for (i_q = a.qStart - 1; i_q < a.qStop; i_q++) {
            cur_base = a.readSeq[i_q];
            if (cur_base != 'N') {
                actual_pos = findBaseScorePos(cur_base, i_s);
                scores[actual_pos]++;
                if (cur_base != seq[i_s]) {
                    differences++;
                }
            }
            i_s++;
        }
    } else if (a.strand == MINUS) {
        i_s = a.sStop - 1;
        for (i_q = a.qStart - 1; i_q < a.qStop; i_q++) {
            cur_base = a.readSeq[i_q];
            if (cur_base != 'N') {
                rc_cur_base = reverseComplementBaseStrict(cur_base);
                actual_pos = findBaseScorePos(rc_cur_base, i_s);
                scores[actual_pos]++;
                if (rc_cur_base != seq[i_s]) {
                    differences++;
                }
            }
            i_s--;
        }
    }

    return differences;
}

/* Corrects a read based on the consensus in the corresponding contig */
int correctRead(alignment_t *a, contig_t *c, int length_threshold, int64_t *reject_shortie, int64_t *reject_indel, int64_t *reject_many_diffs)
{
    int strand = a->strand;
    int i_s, i_q;
    char cur_base, consensus_base, rc_consensus_base, rc_cur_base;
    char differences = 0;
    int corrected_read = 0;
    char *seq = c->consensus;

    /* Avoid correcting indels */
    if ((a->sStop - a->sStart) != (a->qStop - a->qStart)) {
        (*reject_indel)++;
        return 0;
    }

    /* Do not correct if alignment is type MERALIGNER-0, already agree in all bases */
    if (a->type == 1) {
        return 0;
    }

    /* Do not correct if the length of the contig is too short... */
    if (a->sLength < length_threshold) {
        (*reject_shortie)++;
        return 0;
    }

#ifndef CONSENSUS_FINDING
    // Have to count in how many bases the sequences differ

    if (a->strand == PLUS) {
        i_s = a->sStart - 1;
        for (i_q = a->qStart - 1; i_q < a->qStop; i_q++) {
            cur_base = a->readSeq[i_q];
            if (cur_base != 'N') {
                if (cur_base != seq[i_s]) {
                    differences++;
                }
            }
            i_s++;
        }
    } else if (a->strand == MINUS) {
        i_s = a->sStop - 1;
        for (i_q = a->qStart - 1; i_q < a->qStop; i_q++) {
            cur_base = a->readSeq[i_q];
            if (cur_base != 'N') {
                rc_cur_base = reverseComplementBaseStrict(cur_base);
                if (rc_cur_base != seq[i_s]) {
                    differences++;
                }
            }
            i_s--;
        }
    }

    a->basesDiff = differences;
#endif


    if (a->basesDiff > 3) {
        (*reject_many_diffs)++;
        return 0;
    }

    if (strand == PLUS) {
        i_s = a->sStart - 1;
        for (i_q = a->qStart - 1; i_q < a->qStop; i_q++) {
            cur_base = a->readSeq[i_q];
            consensus_base = c->consensus[i_s];
            if ((cur_base != 'N') && (consensus_base != 'N')) {
                if (cur_base != consensus_base) {
                    a->readSeq[i_q] = consensus_base;
                    corrected_read = 1;
                }
            }
            i_s++;
        }
    } else if (strand == MINUS) {
        i_s = a->sStop - 1;
        for (i_q = a->qStart - 1; i_q < a->qStop; i_q++) {
            cur_base = a->readSeq[i_q];
            consensus_base = c->consensus[i_s];
            if ((cur_base != 'N') && (consensus_base != 'N')) {
                rc_consensus_base = reverseComplementBaseStrict(consensus_base);
                if (cur_base != rc_consensus_base) {
                    a->readSeq[i_q] = rc_consensus_base;
                    corrected_read = 1;
                }
            }
            i_s--;
        }
    }

    return corrected_read;
}

/* Stores the consensus in the contig's positions, otherwise stores 'N' if there is not consensus in a position  */
int findConsensusInContig(contig_t c, int64_t contigID)
{
    int64_t i, len = c.length;
    int64_t sum, max;
    int *scores = c.baseCount;
    char *consensus = c.consensus;
    char *seq = c.localSequence;
    char maxBase;

    for (i = 0; i < len; i++) {
        sum = scores[4 * i];
        max = scores[4 * i];
        maxBase = 'A';

        sum += scores[4 * i + 1];
        if (scores[4 * i + 1] > max) {
            max = scores[4 * i + 1];
            maxBase = 'C';
        }

        sum += scores[4 * i + 2];
        if (scores[4 * i + 2] > max) {
            max = scores[4 * i + 2];
            maxBase = 'G';
        }

        sum += scores[4 * i + 3];
        if (scores[4 * i + 3] > max) {
            max = scores[4 * i + 3];
            maxBase = 'T';
        }

        if ((max >= 0.8 * sum) && (max != 0)) {
            consensus[i] = maxBase;
            if (maxBase != seq[i]) {
                printf("WARNING: (Contig %lld) consensus is %c while contig-base %lld is %c (count is %lld and max is %lld)\n", (lld)contigID, maxBase, (lld)i + 1, seq[i], (lld)sum, (lld)max);
            }
        } else {
            consensus[i] = 'N';
        }
    }

    return 0;
}

int cmpFunc(const void *a, const void *b)
{
    const alignment_t *p1 = (alignment_t *)a;
    const alignment_t *p2 = (alignment_t *)b;

    if (p1->local_read_ID > p2->local_read_ID) {
        return 1;
    } else if (p1->local_read_ID < p2->local_read_ID) {
        return -1;
    }

    return 0;
}
