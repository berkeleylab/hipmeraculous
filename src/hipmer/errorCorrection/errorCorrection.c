#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "errorCorrectionUtils.h"
#include "../fqreader/fq_reader.h"
#include "Buffer.h"
#include "../meraligner/fasta.h"
#include "utils.h"

shared int64_t total_shorties = 0;
shared int64_t total_indels = 0;
shared int64_t total_multialigns = 0;
shared int64_t total_many_diffs = 0;
shared int64_t total_reads = 0;
shared int64_t total_reads_corrected = 0;

int main(int argc, char **argv)
{
    INIT_STATIC_VARS;
    upc_tick_t start_time = upc_ticks_now();
    int64_t totalContigs = 0;
    shared[] char *contigSeqShared;
    shared[1] contig_t * contigArray = NULL;
    contig_t currentContigEntry;
    int64_t reject_shortie = 0;
    int64_t reject_indel = 0;
    int64_t reject_multialign = 0;
    int64_t reject_many_diffs = 0;
    int64_t errorCorrected = 0;
    int64_t readsProcessed = 0;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "B:c:n:R:l:t:m:P");
    print_args(optList, __func__);
    char *readMeta = NULL, *alignmentMeta = NULL;
    FILE *readMetaFD = NULL, *alignmentMetaFD = NULL;
    int use_paired_libs = 0;
    char *libname = NULL;
    char *base_dir, *contig_file_name;
    int length_threshold = 100;
    int libnum = 0;
    int correction_method = 1; // this is consensus finding method

    /* Process the input arguments */
    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 'R':
            readMeta = thisOpt->argument;
            readMetaFD = fopen(readMeta, "r");
            break;
        case 'l':
            libname = thisOpt->argument;
            break;
        case 'n':
            totalContigs = atoi(thisOpt->argument);
            break;
        case 'm':
            correction_method = atoi(thisOpt->argument);
            break;
        case 'P':
            use_paired_libs = 1;
            break;
        case 't':
            length_threshold = atoi(thisOpt->argument);
            break;
        default:
            break;
        }
    }
    FreeOptList(optList);

    upc_barrier;
    start_time = UPC_TICKS_NOW();

    /* Read the read file names and the alignments filenames */
    int nReadFiles = 0;
    char line[MAX_LINE_SIZE];
    char readFileNameArray[MAX_LINE_SIZE * MAX_READ_FILES];
    char outputMerAligner[MAX_LINE_SIZE];
    int nAlignmentFiles = 0;

    char alignmentFileNameArray[MAX_LINE_SIZE * MAX_READ_FILES];
    int q = 0;
    char aux_line[MAX_LINE_SIZE];

    while (fgets(line, MAX_LINE_SIZE, readMetaFD) != NULL) {
        /* Strip new line character */
        line[strlen(line) - 1] = '\0';
        strcpy(readFileNameArray + nReadFiles * MAX_LINE_SIZE, line);
        if (MYTHREAD == 0) {
            printf("Registered file %s \n", line);
        }
        if ((use_paired_libs == 1) && (nReadFiles % 2 == 0)) {
            sprintf(outputMerAligner, "%s/%s_Read2" GZIP_EXT, base_dir, libname);
            get_rank_path(outputMerAligner, MYTHREAD);
            strcpy(alignmentFileNameArray + q * MAX_LINE_SIZE, outputMerAligner);
            if (MYTHREAD == 0) {
                printf("Registered file %s \n", outputMerAligner);
            }
            q++;
            nAlignmentFiles++;
            sprintf(outputMerAligner, "%s/%s_Read1" GZIP_EXT, base_dir, libname);
            get_rank_path(outputMerAligner, MYTHREAD);
            strcpy(alignmentFileNameArray + q * MAX_LINE_SIZE, outputMerAligner);
            if (MYTHREAD == 0) {
                printf("Registered file %s \n", outputMerAligner);
            }
            q++;
            nAlignmentFiles++;
        }
        nReadFiles++;
    }
    fclose(readMetaFD);
    UPC_ALL_ALLOC_CHK(contigArray, totalContigs, sizeof(contig_t));

    /* Read the contigs and store them in shared memory. Contig i will be mapped to processor i mod P. */
    char my_contig_file_name[255];
    char *contigBuffer = NULL;
    char *name = NULL;
    sprintf(my_contig_file_name, "%s/%s_%d.fasta" GZIP_EXT, base_dir, contig_file_name, MYTHREAD);
    get_rank_path(my_contig_file_name, MYTHREAD);
    FASTAFILE *contigFile;
    int64_t contigID;
    int contigLen;
    int64_t myContigs = 0;
    int64_t i, k;
    if (MYTHREAD == 0) {
        printf("Reading contig file %s ...\n", my_contig_file_name);
    }

    contigFile = OpenFASTA(my_contig_file_name);
    while (ReadFASTA(contigFile, &contigBuffer, &name, &contigLen)) {
        /* Read a contig and its length */
        contigID = atol(name + 7);
        contigSeqShared = NULL;
        UPC_ALLOC_CHK(contigSeqShared, contigLen * sizeof(char));
        memcpy((char *)contigSeqShared, contigBuffer, contigLen * sizeof(char));
        currentContigEntry.length = contigLen;
        currentContigEntry.sequence = contigSeqShared;
        currentContigEntry.localSequence = NULL;
        currentContigEntry.baseCount = NULL;
        contigArray[contigID] = currentContigEntry;
        free_chk0(contigBuffer);
        free_chk0(name);
        contigBuffer = NULL;
        name = NULL;
        myContigs++;
    }
    CloseFASTA(contigFile);

    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done reading contig file %s ...\n", my_contig_file_name);
    }

    /* Each processor iterates over the contigs it owns and makes local copies for later use */
    for (i = MYTHREAD; i < totalContigs; i += THREADS) {
        contigLen = contigArray[i].length;
        contigArray[i].localSequence = (char *)malloc(contigLen * sizeof(char));
        upc_memget((char *)contigArray[i].localSequence, (shared[] char *)contigArray[i].sequence, contigLen * sizeof(char));
        contigArray[i].baseCount = (int *)calloc(4 * contigLen, sizeof(int));
        contigArray[i].consensus = (char *)calloc(contigLen, sizeof(char));
    }
    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done with local copying of contigs...\n");
    }

    /* Read the alignment results along with the read files and send the reads along with the alignment info to the processor owning the corresponding contigs. */
    char *readFileName, *alignmentFileName;
    int64_t total_read = 0;
    int cached_io = 0;
    if (strstr(base_dir, "/dev/shm")) {
        cached_io = 1;
    }
    Buffer idBuf = initBuffer(MAX_READ_NAME_LEN + 1);
    Buffer seqBuf = initBuffer(DEFAULT_READ_LEN + 1);
    Buffer qualsBuf = initBuffer(DEFAULT_READ_LEN + 1);
    char *id = getStartBuffer(idBuf);
    char *seq = getStartBuffer(seqBuf);
    char *quals = getStartBuffer(qualsBuf);
    char align[MAX_LINE_SIZE];
    char *fgets_result = NULL;
    int splitRes, qStart, qStop, qLength, subject, sStart, sStop, sLength, strand, cur_pos;
    char cur_read[MAX_LINE_SIZE];
    char prev_read[MAX_LINE_SIZE];
    alignment_t new_entry;
    alignment_t *local_buffs;
    int64_t *local_index, foo;
    alignment_heap_t alignment_heap;
    GZIP_FILE alignmentFD;
    shared[1] int64_t * total_entries = NULL;
    UPC_ALL_ALLOC_CHK(total_entries, THREADS, sizeof(int64_t));
    shared[1] int64_t * total_corrected_entries = NULL;
    UPC_ALL_ALLOC_CHK(total_corrected_entries, THREADS, sizeof(int64_t));
    total_entries[MYTHREAD] = 0;
    total_corrected_entries[MYTHREAD] = 0;
    upc_barrier;
    int store_guard;

    int64_t *entriesPerThread = (int64_t *)malloc(THREADS * sizeof(int64_t));
    for (i = 0; i < THREADS; i++) {
        entriesPerThread[i] = 0;
    }

    /* Find memory requirements for shared heaps and the size of each read file name */
    int64_t *myReadFilesSizes = (int64_t *)malloc(nReadFiles * sizeof(int64_t));
    for (i = 0; i < nReadFiles; i++) {
        myReadFilesSizes[i] = 0;
    }

    char enhancedFileName[MAX_LINE_SIZE];

    for (i = 0; i < nAlignmentFiles; i++) {
        alignmentFileName = alignmentFileNameArray + i * MAX_LINE_SIZE;
        strcpy(enhancedFileName, alignmentFileName);
        alignmentFD = GZIP_OPEN(enhancedFileName, "r");
        if (MYTHREAD == 0) {
            printf("Processing alignment file %s\n", alignmentFileName);
        }
        fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
        while (fgets_result != NULL) {
            splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
            if (correction_method == 1) {
                store_guard = (splitRes > 0) ? 1 : 0;
            } else {
                store_guard = (splitRes == 2) ? 1 : 0;
            }
            if (store_guard) {
                entriesPerThread[subject % THREADS]++;
            }
            fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
        }
        if (MYTHREAD == 0) {
            printf("Done processing alignment file %s\n", alignmentFileName);
        }
        GZIP_CLOSE(alignmentFD);
    }
    for (i = 0; i < THREADS; i++) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(total_entries[i]), entriesPerThread[i]);
    }
    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done with finding memory requirements\n");
    }

    /* Allocate data structures for aggregating stores optimization */
    allocate_alignment_local_buffs(&local_buffs, &local_index);
    create_alignment_heaps(total_entries[MYTHREAD], &alignment_heap);
    fq_reader_t fqr;
    int64_t indels = 0;
    int nAligns = 0;
    uint64_t read1ID = MYTHREAD, read2ID = MYTHREAD;
    int add_to_heap = 0;

    /* Here starts read scatter for eacn ReadFile*/
    for (i = 0; i < nReadFiles; i++) {
        readFileName = readFileNameArray + i * MAX_LINE_SIZE;
        alignmentFileName = alignmentFileNameArray + i * MAX_LINE_SIZE;
        strcpy(enhancedFileName, alignmentFileName);
        if (MYTHREAD == 0) {
            printf("Start with file %s\n", enhancedFileName);
        }
        alignmentFD = GZIP_OPEN(enhancedFileName, "r");
        fqr = create_fq_reader();
        open_fq(fqr, readFileName, cached_io, base_dir, cached_io ? -1 : broadcast_file_size(readFileName));
        int found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
        if (found_next) {
            hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
            id = getStartBuffer(idBuf);
            id[getLengthBuffer(idBuf)] = '\0';
            seq = getStartBuffer(seqBuf);
            seq[getLengthBuffer(seqBuf)] = '\0';
            quals = getStartBuffer(qualsBuf);
            quals[getLengthBuffer(qualsBuf)] = '\0';
            fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
            splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
        }
        /* Pocess the reads of the current read File */
        while (found_next) {
            myReadFilesSizes[i] += 2 * strlen(id) + 2 * strlen(seq) + 4;
            readsProcessed++;
            nAligns = 0;
            int split_low_limit;
            if (correction_method == 1) {
                split_low_limit = 0;
            } else {
                split_low_limit = 1;
            }
            /* Keep consuming the alignments for this read */
            while ((strcmp(id, cur_read) == 0) && (fgets_result != NULL)) {
                if (splitRes > split_low_limit) {
                    nAligns++;
                    new_entry.local_read_ID = total_read;
                    new_entry.owner_id = MYTHREAD;
                    memcpy(new_entry.readSeq, seq, qLength);
                    new_entry.qStart = qStart;
                    new_entry.qStop = qStop;
                    new_entry.qLength = qLength;
                    new_entry.subject = subject;
                    new_entry.sStart = sStart;
                    new_entry.sStop = sStop;
                    new_entry.sLength = sLength;
                    new_entry.strand = strand;
                    new_entry.type = splitRes;
                }
                total_read++;
                /* Read next alignment in the file */
                fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, alignmentFD);
                if (fgets_result != NULL) {
                    splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);
                }
            }
            /* Send alignment to the appropriate destination (based on subject ID) only if the read is uniquely aligned! */
            if (nAligns == 1) {
                add_to_heap++;
                add_alignment_to_shared_heaps(&new_entry, (int)((new_entry.subject) % THREADS), local_index, local_buffs, alignment_heap);
            } else if (nAligns > 1) {
                reject_multialign++;
            }

            /* Read next read */
            found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
            if (found_next) {
                hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
                id = getStartBuffer(idBuf);
                id[getLengthBuffer(idBuf)] = '\0';
                seq = getStartBuffer(seqBuf);
                seq[getLengthBuffer(seqBuf)] = '\0';
                quals = getStartBuffer(qualsBuf);
                quals[getLengthBuffer(qualsBuf)] = '\0';
            }
        }
        GZIP_CLOSE(alignmentFD);
        close_fq(fqr);
        destroy_fq_reader(fqr);
    }
    /* Flush outgoing dedicated buffers */
    add_rest_alignments_to_shared_heaps(local_index, local_buffs, alignment_heap);
    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done with storing the alignments to the appropriate processors \n");
    }

    /* Each processor processes the received alignment info and performs base-counting on the contigs */
    alignment_t *my_alignment_entries = (alignment_t *)alignment_heap.alignment_ptr[MYTHREAD];
    int64_t nEntries = alignment_heap.heap_indices[MYTHREAD];
    char *contig_seq;
    int *contig_bases_scores;
    int64_t corrected_reads = 0;
    if (correction_method == 1) {
        for (i = 0; i < nEntries; i++) {
            new_entry = my_alignment_entries[i];
            contig_seq = (char *)contigArray[new_entry.subject].localSequence;
            contig_bases_scores = (int *)contigArray[new_entry.subject].baseCount;
            /* Consider only perfect alignments for the base counting */
            if (new_entry.type == 1) {
                new_entry.basesDiff = countBases(new_entry, contig_seq, contig_bases_scores);
            }
        }
        /* Find consensus on contig bases */
        for (i = MYTHREAD; i < totalContigs; i += THREADS) {
            findConsensusInContig((contig_t)contigArray[i], i);
        }
    } else {
        int cl;
        contig_t cur_contig;
        /* Just copy in the consensus the actual contig */
        for (i = MYTHREAD; i < totalContigs; i += THREADS) {
            cur_contig = contigArray[i];
            cl = cur_contig.length;
            memcpy(cur_contig.consensus, cur_contig.localSequence, cl * sizeof(char));
        }
    }
    char *correctedFlags = (char *)malloc(nEntries * sizeof(char));
    int64_t *correctedReads = (int64_t *)malloc(THREADS * sizeof(int64_t));
    for (i = 0; i < THREADS; i++) {
        correctedReads[i] = 0;
    }
    int correction_result;
    /* Each processor reads the received reads and corrects candidate reads based on the contigs's base-counting */
    int64_t total_corrected_reads = 0;
    for (i = 0; i < nEntries; i++) {
        new_entry = my_alignment_entries[i];
        correction_result = correctRead(&(my_alignment_entries[i]), (contig_t *)&(contigArray[new_entry.subject]), length_threshold, &reject_shortie, &reject_indel, &reject_many_diffs);
        if (correction_result == 1) {
            correctedFlags[i] = 1;
            correctedReads[new_entry.owner_id]++;
            total_corrected_reads++;
            errorCorrected++;
        } else {
            correctedFlags[i] = 0;
        }
    }
    /* Send the corrected reads back to the owners */
    for (i = 0; i < THREADS; i++) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(total_corrected_entries[i]), correctedReads[i]);
    }
    upc_barrier;

    if (MYTHREAD == 0) {
        printf("Done with correcting the reads\n");
    }
    int64_t my_total_corrected_reads = total_corrected_entries[MYTHREAD];
    alignment_t *local_corrected_buffs;
    int64_t *local_corrected_index;
    alignment_heap_t alignment_corrected_heap;
    /* Allocate data structures for aggregating stores optimization (in regard to corrected reads) */
    allocate_alignment_local_buffs(&local_corrected_buffs, &local_corrected_index);
    create_alignment_heaps(my_total_corrected_reads, &alignment_corrected_heap);
    /* TODO: Can send back minimal data structure for read  */
    for (i = 0; i < nEntries; i++) {
        if (correctedFlags[i]) {
            new_entry = my_alignment_entries[i];
            add_alignment_to_shared_heaps(&new_entry, new_entry.owner_id, local_corrected_index, local_corrected_buffs, alignment_corrected_heap);
        }
    }
    /* Flush outgoing dedicated buffers */
    add_rest_alignments_to_shared_heaps(local_corrected_index, local_corrected_buffs, alignment_corrected_heap);
    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done with sending back the corrected reads\n");
    }
    /* Update read files with the corrected reads */
    alignment_t *my_corrected_entries = (alignment_t *)alignment_corrected_heap.alignment_ptr[MYTHREAD];
    int64_t nCorrectedEntries = total_corrected_entries[MYTHREAD];
    /* First sort the received corrected reads based on the local_read_ID */
    qsort(my_corrected_entries, nCorrectedEntries, sizeof(alignment_t), cmpFunc);
    /* Propagate the sizes of the partial read files */
    shared[1] int64_t * ourSizes = NULL;
    UPC_ALL_ALLOC_CHK(ourSizes, THREADS * (int64_t)nReadFiles, sizeof(int64_t));
    for (i = 0; i < nReadFiles; i++) {
        ourSizes[i * ((int64_t)THREADS) + MYTHREAD] = myReadFilesSizes[i];
    }
    upc_barrier;
    int64_t *local_copy_ourSizes = (int64_t *)malloc(THREADS * (int64_t)nReadFiles * sizeof(int64_t));
    for (i = 0; i < THREADS * (int64_t)nReadFiles; i++) {
        local_copy_ourSizes[i] = ourSizes[i];
    }
    /* Calculate my offsets */
    int64_t *myOffsets = (int64_t *)malloc(nReadFiles * sizeof(int64_t));
    for (i = 0; i < nReadFiles; i++) {
        myOffsets[i] = 0;
        for (k = 0; k < MYTHREAD; k++) {
            myOffsets[i] += local_copy_ourSizes[i * ((int64_t)THREADS) + k];
        }
    }
    UPC_ALL_FREE_CHK(ourSizes);

    /* Write the error corrected fastq files */
    char correctedReadFileName[MAX_LINE_SIZE];
    int64_t local_read_id = 0;
    int64_t index_in_heap = 0;
    alignment_t cur_entry;
    int found_next, m;
    FILE *outFD;
    char *myMap, *myStart;
    size_t myPageOffset, myPos;

    /* FIXME: Need to fix logic for read error correction */
    for (i = 0; i < nReadFiles; i++) {
        readFileName = readFileNameArray + i * MAX_LINE_SIZE;
        sprintf(correctedReadFileName, "%.*s_coorected.fq", MAX_LINE_SIZE-15, readFileName);
        if (MYTHREAD == 0) {
            printf("Start with file %s corrected into --> %s\n", readFileName, correctedReadFileName);
        }
        outFD = fopen_chk(correctedReadFileName, "w+");
        upc_barrier;
        if (MYTHREAD == THREADS - 1) {
            int res = ftruncate(fileno(outFD), myOffsets[i] + myReadFilesSizes[i]);
        }
        upc_barrier;
        myPageOffset = myOffsets[i] % sysconf(_SC_PAGESIZE);
        myMap = mmap(NULL, myReadFilesSizes[i] + myPageOffset, PROT_WRITE, MAP_SHARED | MAP_FILE, fileno(outFD), myOffsets[i] - myPageOffset);
        myStart = myMap + myPageOffset;
        myPos = 0;
        //if (myMap != NULL) {
        //  printf("Thread %d successfully performed mmap()\n", MYTHREAD);
        //}
        fqr = create_fq_reader();
        read1ID = MYTHREAD, read2ID = MYTHREAD;
        open_fq(fqr, readFileName, cached_io, base_dir, cached_io ? -1 : broadcast_file_size(readFileName));
        found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
        if (found_next) {
            hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
        }
        id = getStartBuffer(idBuf);
        id[getLengthBuffer(idBuf)] = '\0';
        seq = getStartBuffer(seqBuf);
        seq[getLengthBuffer(seqBuf)] = '\0';
        quals = getStartBuffer(qualsBuf);
        quals[getLengthBuffer(qualsBuf)] = '\0';
        int id_length, read_length, quality_length;
        int64_t corrected_local_id;
        while (found_next) {
            if (index_in_heap < nCorrectedEntries) {
                cur_entry = my_corrected_entries[index_in_heap];
                corrected_local_id = cur_entry.local_read_ID;
                while (corrected_local_id == local_read_id) {
                    /* Fix read in the corrected portion */
                    for (m = cur_entry.qStart - 1; m < cur_entry.qStop; m++) {
                        seq[m] = cur_entry.readSeq[m];
                    }
                    /* Ensure end of string is there */
                    seq[cur_entry.qLength] = '\0';
                    index_in_heap++;
                    if (index_in_heap < nCorrectedEntries) {
                        cur_entry = my_corrected_entries[index_in_heap];
                        corrected_local_id = cur_entry.local_read_ID;
                    } else {
                        corrected_local_id = -1;
                    }
                }
            }

            id_length = strlen(id);
            memcpy(myStart + myPos, id, id_length * sizeof(char));
            myPos += id_length;
            myStart[myPos] = '\n';
            myPos++;
            read_length = strlen(seq);
            memcpy(myStart + myPos, seq, read_length * sizeof(char));
            myPos += read_length;
            myStart[myPos] = '\n';
            myPos++;
            id[0] = '+';
            memcpy(myStart + myPos, id, id_length * sizeof(char));
            myPos += id_length;
            myStart[myPos] = '\n';
            myPos++;
            quality_length = strlen(quals);
            memcpy(myStart + myPos, quals, quality_length * sizeof(char));
            myPos += quality_length;
            myStart[myPos] = '\n';
            myPos++;

            local_read_id++;
            found_next = get_next_fq_record(fqr, idBuf, seqBuf, qualsBuf);
            if (found_next) {
                hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
            }
            id = getStartBuffer(idBuf);
            id[getLengthBuffer(idBuf)] = '\0';
            seq = getStartBuffer(seqBuf);
            seq[getLengthBuffer(seqBuf)] = '\0';
            quals = getStartBuffer(qualsBuf);
            quals[getLengthBuffer(qualsBuf)] = '\0';
        }

        munmap(myMap, myReadFilesSizes[i] + myPageOffset);
        fclose_track(outFD);
        close_fq(fqr);
        destroy_fq_reader(fqr);
    }

    upc_fence;
    upc_barrier;

    // TODO replace with reduce
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_shorties, reject_shortie);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_indels, reject_indel);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_multialigns, reject_multialign);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_reads, readsProcessed);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_reads_corrected, errorCorrected);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &total_many_diffs, reject_many_diffs);

    /* Each processor iterates over the contigs it owns and frees corresponding allocated memory */
    for (i = MYTHREAD; i < totalContigs; i += THREADS) {
        free(contigArray[i].localSequence);
        UPC_FREE_CHK(contigArray[i].sequence);
        free(contigArray[i].baseCount);
        free(contigArray[i].consensus);
    }
    upc_barrier;
    free(local_buffs);
    free(local_index);
    free(local_corrected_buffs);
    free(local_corrected_index);
    free(entriesPerThread);
    free(myReadFilesSizes);
    free(correctedFlags);
    free(correctedReads);
    free(local_copy_ourSizes);
    free(myOffsets);
    UPC_ALL_FREE_CHK(contigArray);
    UPC_ALL_FREE_CHK(total_entries);
    UPC_ALL_FREE_CHK(total_corrected_entries);
    destroy_alignment_heaps(&alignment_corrected_heap);
    destroy_alignment_heaps(&alignment_heap);
    upc_barrier;

    if (MYTHREAD == 0) {
        printf("Done with writing fastq files\n");
        printf("=========================================\n");
        printf("\nRead ERROR CORRECTION statistics\n");
        printf("Total reads %lld , corrected %lld (fraction : %.2f %% )\n", (lld)total_reads, (lld)total_reads_corrected, (1.0 * total_reads_corrected) / (1.0 * total_reads) * 100.0);
        printf("Rejected SHORTIES :\t %lld (fraction : %.2f %% of uncorrected reads)\n", (lld)total_shorties, (1.0 * total_shorties) / (1.0 * (total_reads - total_reads_corrected)) * 100.0);
        printf("Rejected MULTI-ALIGN:\t %lld (fraction : %.2f %% of uncorrected reads)\n", (lld)total_multialigns, (1.0 * total_multialigns) / (1.0 * (total_reads - total_reads_corrected)) * 100.0);
        printf("Rejected SIMILARITY:\t %lld (fraction : %.2f %% of uncorrected reads)\n", (lld)total_many_diffs, (1.0 * total_many_diffs) / (1.0 * (total_reads - total_reads_corrected)) * 100.0);
        printf("Rejected INDELS:\t %lld (fraction : %.2f %% of uncorrected reads)\n", (lld)total_indels, (1.0 * total_indels) / (1.0 * (total_reads - total_reads_corrected)) * 100.0);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }
    return 0;
}
