#ifndef __WALK_GRAPH__
#define __WALK_GRAPH__

#include "options.hpp"
#include "ctg_graph.hpp"

void walk_graph(shared_ptr<Options> options, shared_ptr<CtgGraph> graph);


#endif
