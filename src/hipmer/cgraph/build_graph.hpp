#ifndef __BUILD_GRAPH__
#define __BUILD_GRAPH__

#include "options.hpp"
#include "ctg_graph.hpp"

void build_graph(shared_ptr<Options> options, shared_ptr<CtgGraph> graph);


#endif
