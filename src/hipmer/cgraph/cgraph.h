#ifndef _CGRAPH_H_
#define _CGRAPH_H_

#ifdef __cplusplus
extern "C" {
#endif

int cgraph_main(int argc, char **argv);
void init_upcxx();
void finalize_upcxx();
  
#ifdef __cplusplus
}
#endif
#endif
