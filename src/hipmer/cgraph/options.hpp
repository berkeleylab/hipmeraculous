#ifndef __OPTIONS_H
#define __OPTIONS_H

#include <iostream>
#include <regex>
#include <sys/stat.h>
#include <upcxx/upcxx.hpp>
#include "upcxx_utils/log.hpp"

#include "argh.h"

using std::cout;
using std::endl;
using std::vector;

using upcxx_utils::find_rank_files;

enum class QualityTuning {MIN_ERROR, NORMAL, MAX_CONTIGUITY};

class Options {
private:
  vector<string> splitter(string in_pattern, string& content)
  {
    vector<string> split_content;
    std::regex pattern(in_pattern);
    copy(std::sregex_token_iterator(content.begin(), content.end(), pattern, -1),
         std::sregex_token_iterator(),back_inserter(split_content));
    return split_content;
  }

  void get_options(argh::parser &parser, string &usage)
  {
    vector<string> lines = splitter(R"(\n)", usage);
    for (string line: lines) {
      parser.add_param(line.substr(0, 2));
    }
  }
  
public:
  string ctgs_fname;
  vector<string> alns_fname_list;
  vector<string> reads_fname_list;
  vector<string> depths_fname_list;
  vector<string> spans_fname_list;
  string out_dirname = "";
  int kmer_len = 99;
  int max_kmer_len = 99;
  int assm_scaff_len_cutoff = 500;
  double depth_diff_thres = 0.1;
  bool cached_io = false;
  string local_tmp_dir = "/dev/shm";
  bool verbose = false;
  int break_scaffolds = 10;
  uint64_t max_aggr_mb = 1;
  QualityTuning quality_tuning = QualityTuning::NORMAL;
  //double support_thres = 2.0;
  int min_ctg_len = 300;
  
  void load(int argc, char **argv) {
    auto quality_tuning_to_string = [](QualityTuning qt) {
      if (qt == QualityTuning::MIN_ERROR) return "minerror";
      if (qt == QualityTuning::MAX_CONTIGUITY) return "maxctgy";
      return "normal";
    };      
      
    string usage = string(argv[0]) + "\n" +
      "-c    ctgsfile        File containing contigs in FASTA format\n" + 
      "-a    alnsfile        Files containing alignments from merAligner (comma separated)\n" +
      "-r    readsfile       Files containing merged and unmerged reads in FASTQ format (comma separated)\n" + 
      "-d    depthsfile      Files containing contig depths (comma separated) - optional\n" + 
      "-s    spansfile       Files containing span information (comma separated) - optional\n" +
      "-o    outdir          Directory to which output files will be written\n" +
      "-k    kmerlen         kmer length used for alignment\n" +
      "-K    maxkmerlen      maximum kmer length used for building contigs\n" +
      "-L    scaffoldlen     Minimum scaffold length to output\n" +
      "-t    depthdiffthres  Fraction at which depth difference is too great to connect long contigs\n" +
      "-B                    Use cached IO\n" +
      "-l    localTmpDir     local directory for storing files (default /dev/shm)" +
      "-x    numNs           Break scaffolds at 'numNs' Ns (0 - don't break)\n" +
      "-q    qualitytuning   Quality tuning: minerror, normal, maxctgy\n" +
      //"-p    supportthres    Threshold to for ratio of support difference\n" +
      "-m    minctglen       Min. contig length for starting walks\n" +
      "-M    max_aggr_mb     Maximum memory for aggreating stores\n" + 
      "-v                    Verbose mode\n" + 
      "-h                    Display help message\n";
    argh::parser args;
    get_options(args, usage);
    args.parse(argc, argv);

    args("-o") >> out_dirname;
    
    string alns_fnames, reads_fnames, depths_fnames, spans_fnames;
    
    if (!(args("-c") >> ctgs_fname) || !(args("-a") >> alns_fnames) || !(args("-r") >> reads_fnames) || args["-h"]) {
      SOUT(usage);
      exit(0);
    }
    if (args["-B"]) cached_io = true;
    args("-l") >> local_tmp_dir;
    ctgs_fname = find_rank_files(ctgs_fname, ".fasta", cached_io, local_tmp_dir)[0];
    alns_fname_list = find_rank_files(alns_fnames, "", cached_io, local_tmp_dir);
    reads_fname_list = find_rank_files(reads_fnames, "", cached_io, local_tmp_dir);
    if (args("-d") >> depths_fnames) depths_fname_list = find_rank_files(depths_fnames, "", cached_io, local_tmp_dir);
    if (args("-s") >> spans_fnames) spans_fname_list = find_rank_files(spans_fnames, "", cached_io, local_tmp_dir);
    args("-k") >> kmer_len;
    args("-K") >> max_kmer_len;
    args("-L") >> assm_scaff_len_cutoff;
    args("-t") >> depth_diff_thres;
    args("-M") >> max_aggr_mb;
    if (args["-v"]) verbose = true;
    args("-x") >> break_scaffolds;
    string qt_str;
    if (args("-q") >> qt_str) {
      if (qt_str == "minerror") quality_tuning = QualityTuning::MIN_ERROR;
      else if (qt_str == "maxctgy") quality_tuning = QualityTuning::MAX_CONTIGUITY;
      else if (qt_str == "normal") quality_tuning = QualityTuning::NORMAL;
      else {
        SOUT("Quality tuning (-q) must be one of minerror, maxctgy, normal\n");
        SOUT(usage);
        exit(0);
      }
    }
    //args("-p") >> support_thres;
    args("-m") >> min_ctg_len;
    if (upcxx::rank_me() == 0) {
      cout << KLBLUE << "----\n";
      cout << "cgraph options:\n";
      cout << "  (-c) ctgs file:             " << ctgs_fname.c_str() << endl;
      cout << "  (-a) alns files:            " << alns_fnames.c_str() << endl;
      cout << "  (-r) reads files:           " << reads_fnames.c_str() << endl;
      if (!depths_fname_list.empty()) cout << "  (-d) depths files:          " << depths_fnames.c_str() << endl;
      if (!spans_fname_list.empty()) cout << "  (-s) spans files:           " << spans_fnames.c_str() << endl;
      if (out_dirname != "") cout << "  (-o) out directory:         " << out_dirname.c_str() << endl;
      cout << "  (-k) kmer length:           " << kmer_len << endl;
      cout << "  (-K) max kmer length:       " << max_kmer_len << endl;
      cout << "  (-t) depth diff thres:      " << depth_diff_thres << endl;
      cout << "  (-B) cached IO:             " << (cached_io ? "YES" : "NO") << endl;
      cout << "  (-l) local tmp dir:         " << local_tmp_dir << endl;
      cout << "  (-x) break scaffolds:       " << break_scaffolds << endl;
      cout << "  (-q) quality tuning:        " << (quality_tuning_to_string(quality_tuning)) << endl;
      //cout << "  (-p) support threshold:     " << support_thres << endl;
      cout << "  (-m) min contig length:     " << min_ctg_len << endl;
      cout << "  (-M) max aggr mb:           " << max_aggr_mb << endl;
      cout << "  (-v) verbose:               " << (verbose ? "YES" : "NO") << endl;
      cout << "----\n" << KNORM;
      /*
      // if output directory exists, abort
      if (out_dirname != "") {
        struct stat stbuf;
        if (stat(out_dirname.c_str(), &stbuf) != -1 && S_ISDIR(stbuf.st_mode)) {
          std::cerr << KRED << "Warning: output directory '" << out_dirname
                    << "' already exists, files could be overwritten\n" << KNORM;
        } else {
          if (mkdir(out_dirname.c_str(), S_IRWXU|S_IRGRP|S_IXGRP) == -1) {
            std::cerr << "Could not create output directory " << out_dirname.c_str() << ": " << strerror(errno);
            exit(0);
          }
          cout << "Created output directory " << out_dirname.c_str() << endl;
        }
      }
      */
      cout << std::flush;
    }
    upcxx::barrier();
  }
};


#endif
