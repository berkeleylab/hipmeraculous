// cgraph
// Steven Hofmeyr, LBNL, Aug 2018

#ifndef __CTG_GRAPH
#define __CTG_GRAPH

#include <iostream>
#include <map>
#include <fstream>
#include <stdarg.h>
#include <utility>
#include <string>
#include <vector>
#include <string>
#include <climits>
#include <unordered_map>

#include <upcxx/upcxx.hpp>

using upcxx::dist_object;
using upcxx::dist_id;
using upcxx::future;
using upcxx::make_future;

#include "upcxx_utils/progress_bar.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"
#include "upcxx_utils/two_tier_aggr_store.hpp"

using namespace upcxx_utils;

using std::pair;
using std::string;
using std::vector;
using std::unordered_map;
using std::endl;
using std::istream;
using std::ostream;
using std::istringstream;
using std::ostringstream;
using std::shared_ptr;
using std::make_shared;
using std::tuple;
using std::get;
using std::make_tuple;
using std::min;
using std::max;

//#define DBG_BUILD DBG
//#define IS_DBG_BUILD
#define DBG_BUILD(...)

//#define DBG_WALK DBG
#define DBG_WALK(...)

//#define DBG_CORRECT_SPANS DBG
#define DBG_CORRECT_SPANS(...)

#ifdef DEBUG
#define DBG_DUMP_GRAPH
#endif

#define STRICT_SPANS

using cid_t = int64_t;


template<typename T>
void serialize_vector(ostringstream &os, vector<T> &vec) 
{
  uint64_t sz = vec.size();
  os.write((char*)&sz, sizeof(sz));
  for (auto elem : vec) os.write((char*)&elem, sizeof(elem));
}


template<typename T>
void serialize_vector_of_vectors(ostringstream &os, vector<vector<T> > &vec_vec) 
{
  uint64_t sz = vec_vec.size();
  os.write((char*)&sz, sizeof(sz));
  for (auto vec : vec_vec) serialize_vector(os, vec);
}


template<typename T>
void deserialize_vector(istringstream &is, vector<T> &vec) 
{
  uint64_t sz;
  is.read((char*)&sz, sizeof(sz));
  vec.clear();
  for (auto i = 0; i < sz; i++) {
    T elem;
    is.read((char*)&elem, sizeof(elem));
    vec.push_back(elem);
  }
}


template<typename T>
void deserialize_vector_of_vectors(istringstream &is, vector<vector<T> > &vec_vec) 
{
  uint64_t sz;
  is.read((char*)&sz, sizeof(sz));
  vec_vec.clear();
  for (auto i = 0; i < sz; i++) {
    vector<T> vec;
    deserialize_vector(is, vec);
    vec_vec.push_back(vec);
  }
}


inline void serialize_string(ostringstream &os, const string &s) 
{
  uint64_t l = s.length();
  os.write((char*)&l, sizeof(l));
  if (l) os.write(s.c_str(), l);
}


inline void deserialize_string(istringstream &is, string &s) 
{
  uint64_t l;
  is.read((char*)&l, sizeof(l));
  if (l) {
    s.resize(l);
    is.read(&s[0], l);
  }
}


enum class Dirn {FORWARD, BACKWARD};
enum class Orient {NORMAL, REVCOMP};

inline string dirn_str(Dirn dirn) 
{
  return (dirn == Dirn::FORWARD ? "forward" : "backward");
}

inline string orient_str(Orient orient)
{
  return (orient == Orient::NORMAL ? "+" : "-");
}


inline Orient flip_orient(Orient orient)
{
  return (orient == Orient::NORMAL ? Orient::REVCOMP : Orient::NORMAL);
}


static const uint64_t MAX_RNAME_LEN = 14; // from src/hipmer/defines.h

struct GapRead {
  char read_name[MAX_RNAME_LEN];
  cid_t cid;
  // used for resolving positive splints
  int gap_start;
  // used for resolving positive spans
  int rstart, rstop;
  char orient;

  GapRead() {}
  
  GapRead(const string &read_name, int gap_start, int rstart, int rstop, int orient, cid_t cid) {
    if (read_name.size() >= MAX_RNAME_LEN)
      DIE("Read name exceeds buffer size: ", read_name.size(), " > ", MAX_RNAME_LEN, "\n");
    strcpy(this->read_name, read_name.c_str());
    this->gap_start = gap_start;
    this->rstart = rstart;
    this->rstop = rstop;
    this->orient = orient;
    this->cid = cid;
  }

  bool operator==(const GapRead &other) const {
    return strcmp(read_name, other.read_name) == 0;
  }
};


struct CidPair {
  cid_t cid1, cid2;

  bool operator==(const CidPair &other) const {
    return (cid1 == other.cid1 && cid2 == other.cid2);
  }

  bool operator!=(const CidPair &other) const {
    return (cid1 != other.cid1 || cid2 != other.cid2);
  }

  friend ostream &operator<<(ostream &os, const CidPair &cids) {
    os << "(" << cids.cid1 << ", " << cids.cid2 << ")";
    return os;
  }
};

static const CidPair NULL_CIDS = CidPair{-1, -1};

namespace std {
  template <>
  struct hash<CidPair> {
    uint64_t operator()(const CidPair& cids) const {
      return (std::hash<cid_t>()(cids.cid1) ^ (std::hash<cid_t>()(cids.cid2) << 1));
    }
  };
}


enum class EdgeType {SPLINT, SPAN, SPAN_UNUSED};

inline string edge_type_str(EdgeType edge_type) 
{
  switch (edge_type) {
    case EdgeType::SPLINT: return "splint";
    case EdgeType::SPAN: return "span";
    case EdgeType::SPAN_UNUSED: return "span-unused";
    default: return "unknown";
  }
}


struct Edge {
  // the cids of the vertices connected by this edge. Note that the largest number cid is always first
  CidPair cids;
  // the ends correspond to the cids above. 
  EdgeType edge_type;
  int end1, end2;
  int gap;
  int gap_uncertainty;
  int support;
  int aln_len;
  // the sequence filling a positive gap - if the gap is non-positive, this is empty
  string seq;
  // contains information of reads that map to a positive gap - used for filling the gap
  vector<GapRead> gap_reads;
  // the best aln score that generated this edge
  int aln_score;
  // these flags are set during graph construction to keep track of errors in edge construction
  bool mismatch_error, conflict_error, excess_error;
  bool gap_updated; // edge has been checked for span inconsistency

  string serialize() {
    ostringstream os(stringstream::binary);
    os.write((char*)&cids, sizeof(cids));
    os.write((char*)&end1, sizeof(end1));
    os.write((char*)&end2, sizeof(end2));
    os.write((char*)&gap, sizeof(gap));
    os.write((char*)&gap_uncertainty, sizeof(gap_uncertainty));
    os.write((char*)&support, sizeof(support));
    os.write((char*)&aln_len, sizeof(aln_len));
    os.write((char*)&aln_score, sizeof(aln_score));
    os.write((char*)&edge_type, sizeof(edge_type));
    serialize_string(os, seq);
    os.write((char*)&mismatch_error, sizeof(mismatch_error));
    os.write((char*)&conflict_error, sizeof(conflict_error));
    os.write((char*)&excess_error, sizeof(excess_error));
    os.write((char*)&gap_updated, sizeof(gap_updated));
    serialize_vector(os, gap_reads);
    return os.str();
  }

  void deserialize(const string &s) {
    istringstream is(s, stringstream::binary);
    is.read((char*)&cids, sizeof(cids));
    is.read((char*)&end1, sizeof(end1));
    is.read((char*)&end2, sizeof(end2));
    is.read((char*)&gap, sizeof(gap));
    is.read((char*)&gap_uncertainty, sizeof(gap_uncertainty));
    is.read((char*)&support, sizeof(support));
    is.read((char*)&aln_len, sizeof(aln_len));
    is.read((char*)&aln_score, sizeof(aln_score));
    is.read((char*)&edge_type, sizeof(edge_type));
    deserialize_string(is, seq);
    is.read((char*)&mismatch_error, sizeof(mismatch_error));
    is.read((char*)&conflict_error, sizeof(conflict_error));
    is.read((char*)&excess_error, sizeof(excess_error));
    is.read((char*)&gap_updated, sizeof(gap_updated));
    deserialize_vector(is, gap_reads);
  }
};
using Edge_ptr = shared_ptr<Edge>;

struct Vertex {
  cid_t cid;
  int clen;
  float depth;
  upcxx::global_ptr<char> seq_gptr;
  // the neighbors at the different ends
  vector<cid_t> end5;
  vector<cid_t> end3;
  // the merged series of nbs
  // FIXME: when using spans make sure these are valid
  vector<vector<cid_t> > end5_merged;
  vector<vector<cid_t> > end3_merged;
  // book-keeping fields for resolving walk conflicts between ranks - 
  // choose the walk with the longest scaffold, and if there is a tie, choose the highest rank
  int walk_len;
  int walk_rank;
  // set to true if visited in previous round
  bool visited;

  string serialize() {
    ostringstream os(stringstream::binary);
    os.write((char*)&cid, sizeof(cid));
    os.write((char*)&clen, sizeof(clen));
    os.write((char*)&depth, sizeof(depth));
    os.write((char*)&visited, sizeof(visited));
    os.write((char*)&seq_gptr, sizeof(seq_gptr));
    serialize_vector(os, end5);
    serialize_vector(os, end3);
    serialize_vector_of_vectors(os, end5_merged);
    serialize_vector_of_vectors(os, end3_merged);
    os.write((char*)&walk_len, sizeof(walk_len));
    os.write((char*)&walk_rank, sizeof(walk_rank));
    return os.str();
  }

  void deserialize(const string &s) {
    istringstream is(s, stringstream::binary);
    is.read((char*)&cid, sizeof(cid));
    is.read((char*)&clen, sizeof(clen));
    is.read((char*)&depth, sizeof(depth));
    is.read((char*)&visited, sizeof(visited));
    is.read((char*)&seq_gptr, sizeof(seq_gptr));
    deserialize_vector(is, end5);
    deserialize_vector(is, end3);
    deserialize_vector_of_vectors(is, end5_merged);
    deserialize_vector_of_vectors(is, end3_merged);
    is.read((char*)&walk_len, sizeof(walk_len));
    is.read((char*)&walk_rank, sizeof(walk_rank));
  }
};
using Vertex_ptr = shared_ptr<Vertex>;


class CtgGraph {
private:
  using vertex_map_t = unordered_map<cid_t, Vertex>;
  using edge_map_t = unordered_map<CidPair, Edge>;
  using reads_map_t = unordered_map<string, string>;
  using spans_map_t = unordered_map<cid_t, CidPair>;
  dist_object<vertex_map_t> vertices;
  dist_object<edge_map_t> edges;
  dist_object<reads_map_t> read_seqs;
  dist_object<spans_map_t> spans_cids;
  using vertex_cache_t = unordered_map<cid_t, future< Vertex_ptr > >;
  using edge_cache_t = unordered_map<CidPair, future< Edge_ptr > >;
  vertex_cache_t vertex_cache;
  edge_cache_t edge_cache;
  const int MAX_CACHE_SIZE = 2000000;

  struct VertexDepthInfo {
    cid_t cid;
    float depth;
    int clen;
  };
    

  struct EdgeGapReadInfo {
    CidPair cids;
    GapRead gap_read;
    int aln_len;
    int aln_score;
  };

  edge_map_t::iterator edge_iter;
  vertex_map_t::iterator vertex_iter;

  uint64_t get_vertex_target_rank(cid_t cid) {
    return std::hash<cid_t>{}(cid) % upcxx::rank_n();
  }

  uint64_t get_edge_target_rank(CidPair &cids) {
    return std::hash<CidPair>{}(cids) % upcxx::rank_n();
  }

  uint64_t get_read_target_rank(const string &r) {
    return std::hash<string>{}(r) % upcxx::rank_n();
  }

  class UpdateDepthFunc {
    vertex_map_t &vertices;
  public:
    UpdateDepthFunc(vertex_map_t &v) : vertices(v) {}
    void operator()(VertexDepthInfo &vertex_depth_info) {
      const auto it = vertices.find(vertex_depth_info.cid);
      if (it == vertices.end()) DIE("could not fetch vertex ", vertex_depth_info.cid, "\n");
      auto v = &it->second;
      v->depth += (vertex_depth_info.depth / vertex_depth_info.clen);
      if (v->clen != vertex_depth_info.clen)
        DIE("Mismatched clen for ctg ", vertex_depth_info.cid, ": ", vertex_depth_info.clen, " != ", v->clen, "\n");
    }
  };
  dist_object<UpdateDepthFunc> update_depth_func;
  TwoTierAggrStore<dist_object<UpdateDepthFunc>, VertexDepthInfo> vertex_depth_store;


  class AddEdgeGapReadFunc {
    edge_map_t &edges;
  public:
    AddEdgeGapReadFunc(edge_map_t &e) : edges(e) {}
    void operator()(EdgeGapReadInfo &edge_gap_read_info) {
      const auto it = edges.find(edge_gap_read_info.cids);
      if (it == edges.end()) DIE("SPAN edge not found ", edge_gap_read_info.cids.cid1, " ", edge_gap_read_info.cids.cid2, "\n");
      auto edge = &it->second;
      edge->gap_reads.push_back(edge_gap_read_info.gap_read);
      edge->aln_len = max(edge->aln_len, edge_gap_read_info.aln_len);
      edge->aln_score = max(edge->aln_score, edge_gap_read_info.aln_score);
    }
  };
  dist_object<AddEdgeGapReadFunc> add_edge_gap_read_func;
  TwoTierAggrStore<dist_object<AddEdgeGapReadFunc>, EdgeGapReadInfo> edge_gap_read_store;
  
public:
  int max_read_len;

  CtgGraph(uint64_t max_aggr_mb) : vertices({}), edges({}), read_seqs({}), spans_cids({}), vertex_cache({}), edge_cache({}),
               update_depth_func(UpdateDepthFunc(*vertices)), vertex_depth_store(update_depth_func, "vertex_depth"),
               add_edge_gap_read_func(*edges), edge_gap_read_store(add_edge_gap_read_func, "edge_gap_read") {
    vertex_cache.reserve(MAX_CACHE_SIZE);
    edge_cache.reserve(MAX_CACHE_SIZE);
    vertex_depth_store.set_size(max_aggr_mb * ONE_MB / 2);
    edge_gap_read_store.set_size(max_aggr_mb * ONE_MB / 2);
  }

  void clear() {
    for (auto it = vertices->begin(); it != vertices->end(); ) {
      it = vertices->erase(it);
    }
    for (auto it = edges->begin(); it != edges->end(); ) {
      it = edges->erase(it);
    }
    edge_gap_read_store.clear();
    vertex_depth_store.clear();
  }
  
  ~CtgGraph() {
    clear();
  }
  
  int64_t get_num_vertices(bool all = false) {
    if (!all) return upcxx::reduce_one(vertices->size(), upcxx::op_fast_add, 0).wait();
    else return upcxx::reduce_all(vertices->size(), upcxx::op_fast_add).wait();
  }
  
  int64_t get_local_num_vertices(void) {
    return vertices->size();
  }
  
  int64_t get_num_edges(bool all = false) {
    if (!all) return upcxx::reduce_one(edges->size(), upcxx::op_fast_add, 0).wait();
    else return upcxx::reduce_all(edges->size(), upcxx::op_fast_add).wait();
  }

  int64_t get_local_num_edges(void) {
    return edges->size();
  }

  Vertex_ptr get_vertex(cid_t cid) {
    return get_vertex_fut(cid).wait();
  }
  future< Vertex_ptr > get_vertex_fut(cid_t cid) {
    uint64_t target_rank = get_vertex_target_rank(cid);
    if (target_rank == upcxx::rank_me()) {
      const auto it = vertices->find(cid);
      if (it == vertices->end()) return make_future(Vertex_ptr());
      return make_future( make_shared<Vertex>(it->second) );
    }
    return upcxx::rpc(target_rank, 
                      [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid) {
                        const auto it = vertices->find(cid);
                        if (it == vertices->end()) return string("");
                        return it->second.serialize();
                      }, vertices, cid).then(
                        [](string s) -> Vertex_ptr {
                          if (s == "") return nullptr;
                          auto v = make_shared<Vertex>();
                          v->deserialize(s);
                          return v;
                        });
  }

  int get_vertex_clen(cid_t cid) {
    return get_vertex_clen_fut(cid).wait();
  }
  future< int > get_vertex_clen_fut(cid_t cid) {
    return upcxx::rpc(get_vertex_target_rank(cid),
                      [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid) {
                        const auto it = vertices->find(cid);
                        if (it == vertices->end()) return -1;
                        return it->second.clen;
                      }, vertices, cid);
  }

  Vertex_ptr get_local_vertex(cid_t cid) {
    const auto it = vertices->find(cid);
    if (it == vertices->end()) return nullptr;
    return make_shared<Vertex>(it->second);
  }
  
  void update_vertex_depth(cid_t cid, float depth, int clen) {
    auto target_rank = get_vertex_target_rank(cid);
    VertexDepthInfo vertex_depth_info = { cid, depth, clen };
    vertex_depth_store.update(target_rank, vertex_depth_info);
  }

  void flush_vertex_depth_updates_first() {
    vertex_depth_store.flush_inter_updates();
  }
  void flush_vertex_depth_updates() {
    vertex_depth_store.flush_updates();
  }
  
  void set_vertex_visited(cid_t cid) {
    set_vertex_visited_fut(cid).wait();
  }
  future<> set_vertex_visited_fut(cid_t cid) {
    return upcxx::rpc(get_vertex_target_rank(cid),
               [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid) {
                 const auto it = vertices->find(cid);
                 if (it == vertices->end()) DIE("could not fetch vertex ", cid, "\n");
                 auto v = &it->second;
                 v->visited = true;
               }, vertices, cid);
  }

  void set_vertex_depth(cid_t cid, float depth) {
    set_vertex_depth_fut(cid, depth).wait();
  }
  future<> set_vertex_depth_fut(cid_t cid, float depth) {
    return upcxx::rpc(get_vertex_target_rank(cid),
               [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid, float depth) {
                 const auto it = vertices->find(cid);
                 if (it == vertices->end()) DIE("could not fetch vertex ", cid, "\n");
                 auto v = &it->second;
                 v->depth = depth;
                 v->visited = false;
               }, vertices, cid, depth);
  }
  
  void update_vertex_walk(cid_t cid, int64_t walk_len) {
    update_vertex_walk_fut(cid, walk_len).wait();
  }
  future<> update_vertex_walk_fut(cid_t cid, int64_t walk_len) {
    return upcxx::rpc(get_vertex_target_rank(cid),
               [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid, int64_t walk_len, int myrank) {
                 const auto it = vertices->find(cid);
                 if (it == vertices->end()) DIE("could not fetch vertex ", cid, "\n");
                 auto v = &it->second;
                 if ((walk_len == v->walk_len && myrank > v->walk_rank) || walk_len > v->walk_len) {
                   v->walk_len = walk_len;
                   v->walk_rank = myrank;
                 }
               }, vertices, cid, walk_len, upcxx::rank_me());
  }
  
  Vertex *get_first_local_vertex() {
    vertex_iter = vertices->begin();
    if (vertex_iter == vertices->end()) return nullptr;
    auto v = &vertex_iter->second;
    vertex_iter++;
    return v;
  }
  
  Vertex *get_next_local_vertex() {
    if (vertex_iter == vertices->end()) return nullptr;
    auto v = &vertex_iter->second;
    vertex_iter++;
    return v;
  }

  void add_vertex(Vertex &v, const string &seq) {
    add_vertex_fut(v, seq).wait();
  }
  future<> add_vertex_fut(Vertex &v, const string &seq) {
    v.clen = seq.length();
    v.seq_gptr = upcxx::allocate<char>(v.clen + 1);
    strcpy(v.seq_gptr.local(), seq.c_str());
    return upcxx::rpc(get_vertex_target_rank(v.cid),
               [](upcxx::dist_object<vertex_map_t> &vertices, string s) {
                 Vertex v;
                 v.deserialize(s);
                 vertices->insert({v.cid, v});
               }, vertices, v.serialize());
  }

  void add_vertex_nb(cid_t cid, cid_t nb, char end) {
    add_vertex_nb_fut(cid,nb,end).wait();
  }
  future<> add_vertex_nb_fut(cid_t cid, cid_t nb, char end) {
    return upcxx::rpc(get_vertex_target_rank(cid),
               [](upcxx::dist_object<vertex_map_t> &vertices, cid_t cid, cid_t nb, int end) {
                 const auto it = vertices->find(cid);
                 if (it == vertices->end()) DIE("could not fetch vertex ", cid, "\n");
                 auto v = &it->second;
                 if (end == 5) v->end5.push_back(nb);
                 else v->end3.push_back(nb);
               }, vertices, cid, nb, end);
  }

  string get_vertex_seq(upcxx::global_ptr<char> seq_gptr, int64_t seq_len) {
    char buf[seq_len + 1];
    upcxx::rget(seq_gptr, buf, seq_len + 1).wait();
    string s(buf);
    return s;
  }

  Edge_ptr get_edge(cid_t cid1, cid_t cid2) {
    return get_edge_fut(cid1, cid2).wait();
  }
  future< Edge_ptr > get_edge_fut(cid_t cid1, cid_t cid2) {
    CidPair cids = { .cid1 = cid1, .cid2 = cid2 };
    if (cid1 < cid2) std::swap(cids.cid1, cids.cid2);
    uint64_t target_rank = get_edge_target_rank(cids);
    if (target_rank == upcxx::rank_me()) {
      const auto it = edges->find(cids);
      if (it == edges->end()) return make_future(Edge_ptr());
      return make_future( make_shared<Edge>(it->second) );
    }
    return upcxx::rpc(target_rank,
                      [](upcxx::dist_object<edge_map_t> &edges, CidPair cids) -> string {
                        const auto it = edges->find(cids);
                        if (it == edges->end()) return string("");
                        return it->second.serialize();
                      }, edges, cids).then(
                        [](string s) -> Edge_ptr {
                          if (s == "") return nullptr;
                          auto edge = make_shared<Edge>();
                          edge->deserialize(s);
                          return edge;
                        });
  }

  Edge *get_first_local_edge() {
    edge_iter = edges->begin();
    if (edge_iter == edges->end()) return nullptr;
    auto edge = &edge_iter->second;
    edge_iter++;
    return edge;
  }
  
  Edge *get_next_local_edge() {
    if (edge_iter == edges->end()) return nullptr;
    auto edge = &edge_iter->second;
    edge_iter++;
    return edge;
  }

  void add_or_update_edge(Edge &edge) {
    add_or_update_edge_fut(edge).wait();
  }
  future<> add_or_update_edge_fut(Edge &edge) {
    return upcxx::rpc(get_edge_target_rank(edge.cids),
               [](upcxx::dist_object<edge_map_t> &edges, string s) {
                 Edge new_edge;
                 new_edge.deserialize(s);
                 const auto it = edges->find(new_edge.cids);
                 if (it == edges->end()) {
                   // not found, always insert
                   edges->insert({new_edge.cids, new_edge});
                 } else {
                   auto edge = &it->second;
                   // check for conflicts in the ends
                   if (edge->end1 != new_edge.end1 || edge->end2 != new_edge.end2) {
                     if (edge->edge_type == EdgeType::SPLINT) {
                       if (new_edge.edge_type != EdgeType::SPLINT) {
                         DBG_BUILD("conflict error in splint/span: ", edge->cids, " ", edge->end1, " ", edge->end2, "\n");
                       } else {
                         // new edge replaces old one if it has a higher aln score
                         //if (new_edge.aln_score > edge->aln_score) *edge = new_edge;
                         edge->conflict_error = true;
                       }
                     }
                   } else {
                     if (edge->edge_type == EdgeType::SPLINT && new_edge.edge_type != EdgeType::SPLINT) {
                       DBG_BUILD("span confirms splint: ", edge->cids, "\n");
                     }
                     if (edge->edge_type == EdgeType::SPLINT && new_edge.edge_type == EdgeType::SPLINT) {
                       // check for mismatches in gap size if they're both splints
                       if (abs(new_edge.gap - edge->gap) > 2) {
                         DBG_BUILD("gap mismatch for ", new_edge.cids, " ", new_edge.gap, " != ", edge->gap, "\n");
                         edge->gap = min(new_edge.gap, edge->gap);
                         edge->mismatch_error = true;
                       }
                       if (new_edge.gap > 0) {
                         // add reads to positive gap for splints
                         if (new_edge.gap_reads.size() == 0) DIE("trying to get a read from an empty array\n");
                         edge->gap_reads.push_back(new_edge.gap_reads.front());
                       }
                     }
                     // always update support and aln length
                     edge->support++;
                     edge->aln_len = max(edge->aln_len, new_edge.aln_len);
                     edge->aln_score = max(edge->aln_score, new_edge.aln_score);
                   }
                 }
               }, edges, edge.serialize());
  }

  void update_edge_gap(Edge &edge, int gap) {
    update_edge_gap_fut(edge, gap).wait();
  }
  future<> update_edge_gap_fut(Edge &edge, int gap) {
    return upcxx::rpc(get_edge_target_rank(edge.cids),
               [](upcxx::dist_object<edge_map_t> &edges, CidPair cids, int gap) {
                 const auto it = edges->find(cids);
                 if (it == edges->end()) {
                   DIE("Edge for ", cids.cid1, "-", cids.cid2, " not found\n");
                 } else {
                   auto edge = &it->second;
                   if (!edge->gap_updated) {
                     DBG("\tCorrected final gap size for edge ", edge->cids.cid1, "<=>", edge->cids.cid2 ,"\n");
                     edge->gap = gap;
                     edge->gap_updated = true;
                   } else {
                     DBG("\tRACE: edge ", edge->cids.cid1, "<=>", edge->cids.cid2, " already corrected!\n");
                   }
                 }
               }, edges, edge.cids, gap);
  }

  void update_edge_uncertainty(Edge &edge, int gap_uncertainty) {
    update_edge_uncertainty_fut(edge, gap_uncertainty).wait();
  }
  future<> update_edge_uncertainty_fut(Edge &edge, int gap_uncertainty) {
    return upcxx::rpc(get_edge_target_rank(edge.cids),
               [](upcxx::dist_object<edge_map_t> &edges, CidPair cids, int gap_uncertainty) {
                 const auto it = edges->find(cids);
                 if (it == edges->end()) {
                   DIE("Edge for ", cids.cid1, "-", cids.cid2, " not found\n");
                 } else {
                   auto edge = &it->second;
                   edge->gap_uncertainty = gap_uncertainty;
                 }
               }, edges, edge.cids, gap_uncertainty);
  }
  
  void update_edge_type(Edge &edge, EdgeType edge_type) {
    update_edge_type_fut(edge, edge_type).wait();
  }
  future<> update_edge_type_fut(Edge &edge, EdgeType edge_type) {
    return upcxx::rpc(get_edge_target_rank(edge.cids),
               [](upcxx::dist_object<edge_map_t> &edges, CidPair cids, EdgeType edge_type) {
                 const auto it = edges->find(cids);
                 if (it == edges->end()) {
                   DIE("Edge for ", cids.cid1, "-", cids.cid2, " not found\n");
                 } else {
                   auto edge = &it->second;
                   edge->edge_type = edge_type;
                 }
               }, edges, edge.cids, edge_type);
  }
  
  void add_edge_gap_read(CidPair cids, GapRead gap_read, int aln_len, int aln_score) {
    auto target_rank = get_edge_target_rank(cids);
    EdgeGapReadInfo edge_gap_read_info = { cids, gap_read, aln_len, aln_score };
    edge_gap_read_store.update(target_rank, edge_gap_read_info);
  }

  void flush_edge_gap_reads_first() {
    edge_gap_read_store.flush_inter_updates();
  }
  void flush_edge_gap_reads() {
    edge_gap_read_store.flush_updates();
  }

  void purge_error_edges(int64_t *mismatched, int64_t *conflicts, int64_t *questionable_spans) {
    for (auto it = edges->begin(); it != edges->end(); ) {
      auto edge = make_shared<Edge>(it->second);
      if (edge->mismatch_error) {
        (*mismatched)++;
        it = edges->erase(it);
        //++it;
      } else if (edge->conflict_error) {
        (*conflicts)++;
        // Weirdly enough, removing conflicts actually increases the msa
        //it = edges->erase(it);
        ++it;
      } else if (edge->edge_type == EdgeType::SPAN_UNUSED) {
        (*questionable_spans)++;
        it = edges->erase(it);
      } else {
        ++it;
      }
    }
  }
  
  void purge_excess_edges(int64_t *excess) {
    for (auto it = edges->begin(); it != edges->end(); ) {
      auto edge = make_shared<Edge>(it->second);
      if (edge->excess_error) {
        (*excess)++;
        it = edges->erase(it);
      } else {
        it++;
      }
    }
  }
  
  void add_pos_gap_read(const string &read_name) {
    add_pos_gap_read_fut(read_name).wait();
  }
  future<> add_pos_gap_read_fut(const string &read_name) {
    return upcxx::rpc(get_read_target_rank(read_name),
               [](upcxx::dist_object<reads_map_t> &read_seqs, string read_name) {
                 read_seqs->insert({read_name, ""});
               }, read_seqs, read_name);
  }

  bool update_read_seq(const string &read_name, const string &seq) {
    return update_read_seq_fut(read_name, seq).wait();
  }
  future<bool> update_read_seq_fut(const string &read_name, const string &seq) {
    return upcxx::rpc(get_read_target_rank(read_name),
                      [](upcxx::dist_object<reads_map_t> &read_seqs, string read_name, string seq) {
                        auto it = read_seqs->find(read_name);
                        if (it == read_seqs->end()) return false;
                        (*read_seqs)[read_name] = seq;
                        return true;
                      }, read_seqs, read_name, seq);
  }
  
  string get_read_seq(const string &read_name) {
    return get_read_seq_fut(read_name).wait();
  }
  future<string> get_read_seq_fut(const string &read_name) {
    return upcxx::rpc(get_read_target_rank(read_name),
                      [](upcxx::dist_object<reads_map_t> &read_seqs, string read_name) -> string {
                        const auto it = read_seqs->find(read_name);
                        if (it == read_seqs->end()) return string("");
                        return it->second;
                      }, read_seqs, read_name);
  }
  
  uint64_t get_num_read_seqs(bool all = false) {
    if (!all) return upcxx::reduce_one(read_seqs->size(), upcxx::op_fast_add, 0).wait();
    else return upcxx::reduce_all(read_seqs->size(), upcxx::op_fast_add).wait();
  }

  void add_span_cids(cid_t cid, CidPair cids) {
    add_span_cids_fut(cid, cids).wait();
  }
  future<> add_span_cids_fut(cid_t cid, CidPair cids) {
    return upcxx::rpc(get_vertex_target_rank(cid),
               [](upcxx::dist_object<spans_map_t> &spans_cids, cid_t cid, CidPair cids) {
                 spans_cids->insert({cid, cids});
               }, spans_cids, cid, cids);
  }

  CidPair get_span_cids(cid_t cid) {
    return get_span_cids_fut(cid).wait();
  }
  future< CidPair > get_span_cids_fut(cid_t cid) {
    return upcxx::rpc(get_vertex_target_rank(cid),
                      [](upcxx::dist_object<spans_map_t> &spans_cids, cid_t cid) -> CidPair {
                        const auto it = spans_cids->find(cid);
                        if (it == spans_cids->end()) return {-1, -1};
                        return it->second;
                      }, spans_cids, cid);
  }

  int64_t purge_no_read_spans() {
    int64_t num_purged = 0;
    for (auto it = edges->begin(); it != edges->end(); ) {
      auto edge = make_shared<Edge>(it->second);
      if (edge->edge_type == EdgeType::SPAN && edge->gap > 0 && edge->gap_reads.empty()) {
        it = edges->erase(it);
        num_purged++;
      } else {
        ++it;
      }
    }
    return num_purged;
  }

  int get_other_end(Vertex_ptr v1, Vertex_ptr v2, Edge_ptr edge = nullptr) {
    if (!edge) edge = get_edge_cached(v1->cid, v2->cid);
    // the cids in the edge are organized as (largest, smallest), so the edges are determined that way too
    return v1->cid >= v2->cid ? edge->end2 : edge->end1;
  }
  future< int > get_other_end_fut(Vertex_ptr v1, Vertex_ptr v2, Edge_ptr edge = nullptr) {
    future< Edge_ptr > fut_edge;
    if (edge) fut_edge = to_future(edge);
    else fut_edge = get_edge_cached_fut(v1->cid, v2->cid);
    auto _this = this;
    return when_all(make_future(v1, v2), fut_edge).then(
            [&_this](Vertex_ptr v1, Vertex_ptr v2, Edge_ptr edge)
               { return _this->get_other_end(v1, v2, edge); });
  }


  int get_other_end_local(Vertex* v1, Vertex_ptr v2, Edge_ptr edge  = nullptr) {
    if (!edge) edge = get_edge(v1->cid, v2->cid);
    // the cids in the edge are organized as (largest, smallest), so the edges are determined that way too
    return v1->cid >= v2->cid ? edge->end2 : edge->end1;
  }
  future< int > get_other_end_local_fut(Vertex* v1, Vertex_ptr v2, Edge_ptr edge  = nullptr) {
    future< Edge_ptr > fut_edge;
    if (!edge) fut_edge = get_edge_fut(v1->cid, v2->cid);
    else fut_edge = to_future(edge);
    auto _this = this;
    return when_all(make_future(v1, v2), fut_edge).then(
           [&_this](Vertex* v1, Vertex_ptr v2, Edge_ptr edge)
              { return _this->get_other_end_local(v1,v2,edge); });
  }
  
  bool edge_exists(const cid_t cidA, int endA, const cid_t cidB, int endB) {
    auto edge = get_edge(cidA, cidB);
    if (edge) {
      if ((cidA > cidB && edge->end1 == endA && edge->end2 == endB)
          ||
          (cidB > cidA && edge->end1 == endB && edge->end2 == endA) ) {
        return true;
      }
    }
    return false;
  }
  
  bool edge_exists(Vertex_ptr v1, int endA, Vertex_ptr v2, int endB) {
    return edge_exists(v1->cid, endA, v2->cid, endB);
  }

  void clear_caches() {
    vertex_cache.clear();
    edge_cache.clear();
  }

  Vertex_ptr get_vertex_cached(cid_t cid) {
    return get_vertex_cached_fut(cid).wait();
  }
  future< Vertex_ptr > get_vertex_cached_fut(cid_t cid) {
    //return get_vertex(cid);
    auto it = vertex_cache.find(cid);
    if (it != vertex_cache.end()) {
      return it->second;
    }
    auto fut_v = get_vertex_fut(cid);
    // load factor around .5
    if (vertex_cache.size() < MAX_CACHE_SIZE / 2) vertex_cache[cid] = fut_v;
    return fut_v;
  }

  Edge_ptr get_edge_cached(cid_t cid1, cid_t cid2) {
    return get_edge_cached_fut(cid1, cid2).wait();
  }
  future< Edge_ptr > get_edge_cached_fut(cid_t cid1, cid_t cid2) {
    //return get_edge(cid1, cid2);
    CidPair cids = { .cid1 = cid1, .cid2 = cid2 };
    if (cid1 < cid2) std::swap(cids.cid1, cids.cid2);
    auto it = edge_cache.find(cids);
    if (it != edge_cache.end()) {
      return it->second;
    }
    auto fut_edge = get_edge_fut(cids.cid1, cids.cid2);
    if (edge_cache.size() < MAX_CACHE_SIZE / 2) edge_cache[cids] = fut_edge;
    return fut_edge;
  }

  void print_stats(double *av_clen) {
    const int CLEN_THRES = 500;
    BarrierTimer timer(__func__);
    auto get_avg_min_max = [](vector<int64_t> &vals, double *avg = nullptr) {
      double total = 0;
      int64_t max_val = 0;
      int64_t min_val = LONG_MAX;
      for (auto val : vals) {
        total += val;
        min_val = min(min_val, val);
        max_val = max(max_val, val);
      }
      int64_t all_min_val =  upcxx::reduce_one(min_val, upcxx::op_fast_min, 0).wait();
      int64_t all_max_val =  upcxx::reduce_one(max_val, upcxx::op_fast_max, 0).wait();
      double all_total = upcxx::reduce_one(total, upcxx::op_fast_add, 0).wait();
      uint64_t all_nvals = upcxx::reduce_one(vals.size(), upcxx::op_fast_add, 0).wait();
      if (avg) *avg = all_total / all_nvals;
      ostringstream os;
      os.precision(2);
      os << std::fixed;
      os << (all_total / all_nvals) << " [" << all_min_val << ", " << all_max_val << "]";
      return os.str();
    };

#ifdef DBG_DUMP_GRAPH
    time_t curr_t = std::time(nullptr);
    string graph_fname = "ctggraph-" + std::to_string(curr_t) + ".graph"; 
    get_rank_path(graph_fname, rank_me());
    ofstream graph_stream(graph_fname);
#endif    
    vector<int64_t> depths;
    depths.reserve(get_local_num_vertices());
    vector<int64_t> clens;
    clens.reserve(get_local_num_vertices());
    for (auto v = get_first_local_vertex(); v != nullptr; v = get_next_local_vertex()) {
      if (v->clen >= CLEN_THRES) {
        depths.push_back(round(v->depth));
        clens.push_back(v->clen);
      }
#ifdef DBG_DUMP_GRAPH
      graph_stream << v->cid << "," << v->clen << "," << v->depth << endl;
#endif
    }
    vector<int64_t> supports;
    supports.reserve(get_local_num_edges());
    vector<int64_t> aln_lens;
    aln_lens.reserve(get_local_num_edges());
    vector<int64_t> aln_scores;
    aln_scores.reserve(get_local_num_edges());
    vector<int64_t> gaps;
    gaps.reserve(get_local_num_edges());
    {
      ProgressBar progbar(get_local_num_edges(), "Compute graph stats");
      for (auto edge = get_first_local_edge(); edge != nullptr; edge = get_next_local_edge()) {
        aln_lens.push_back(edge->aln_len);
        aln_scores.push_back(edge->aln_score);
        auto clen1 = get_vertex_clen(edge->cids.cid1);
        auto clen2 = get_vertex_clen(edge->cids.cid2);
        if (clen1 >= CLEN_THRES || clen2 >= CLEN_THRES) {
          supports.push_back(edge->support);
          gaps.push_back(edge->gap);
        }
#ifdef DBG_DUMP_GRAPH
        graph_stream << edge->cids.cid1 << "," << edge->cids.cid2 << "," << edge->end1 << "," << edge->end2 << "," << edge->gap
                     << "," << edge->gap_uncertainty << "," << edge->support << "," << edge->aln_len << "," << edge->aln_score
                     << "," << edge_type_str(edge->edge_type) << endl; 
#endif
        progbar.update();
      }
      progbar.done();
    }
  
    SOUT("Graph statistics:\n");
    SOUT("    vertices:  ", get_num_vertices(), "\n");
    SOUT("    edges:     ", get_num_edges(), "\n");
    SOUT("    aln_len:   ", get_avg_min_max(aln_lens), "\n");
    SOUT("    aln_score: ", get_avg_min_max(aln_scores), "\n");
    SOUT("  for contigs >= ", CLEN_THRES, " length:\n");
    SOUT("    depth:     ", get_avg_min_max(depths), "\n");
    SOUT("    clen:      ", get_avg_min_max(clens, av_clen), "\n");
    SOUT("    support:   ", get_avg_min_max(supports), "\n");
    SOUT("    gap:       ", get_avg_min_max(gaps), "\n");
    // implicit barrier() when BarrierTimer is destroyed //
  }

};

#endif
