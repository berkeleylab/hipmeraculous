#include <iostream>
#include <fstream>
#include <regex>
#include <upcxx/upcxx.hpp>

#ifdef MPI_WRITE
#include <mpi.h>
#endif

#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"
#include "misc_utils.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "build_graph.hpp"
#include "zstr.hpp"

using std::string;
using std::stringstream;
using std::shared_ptr;
using std::tuple;
using std::fixed;
using std::setprecision;
using std::swap;
using std::regex;
using std::smatch;
using std::stoi;


using namespace upcxx;
using namespace upcxx_utils;

#define MAX_DEGREE 100

static shared_ptr<Options> _options(nullptr);
static shared_ptr<CtgGraph> _graph(nullptr);
static shared_ptr<TrackRPCs> _track_rpcs(nullptr);

typedef tuple<cid_t, shared_ptr<Edge>> tie_t; 

// comparator functions
struct by_dist {
  bool operator() (tie_t&, tie_t&) const;
};

void parse_ctgs()
{
  BarrierTimer timer(__func__);
  string line;
  string ctg_prefix = ">Contig_";
  int64_t num_ctgs_found = 0;
  cid_t max_cid = 0;
  {
    stringstream ctgs_file_buf;
    {
      Timer t_io("ctgs file IO");
      // reading line by line on a single node is very inefficient - much faster to read complete file as a block,
      // although it uses more memory
      zstr::ifstream ctgs_file(_options->ctgs_fname);
      ctgs_file_buf << ctgs_file.rdbuf();
    }
    //ProgressBar progbar(_options->ctgs_fname, &ctgs_file, "Parsing ctgs file");
    promise<> prom;
    string seq;
    cid_t cid = -1;
    while (getline(ctgs_file_buf, line)) {
      if (line.substr(0, ctg_prefix.size()) == ctg_prefix) {
        // new fasta record
        if (!seq.empty()) {
          Vertex v{};
          v.cid = cid;
          _graph->add_vertex(v, seq);
          seq.clear();
        }
        cid = stol(line.substr(ctg_prefix.size()));
        max_cid = max(max_cid, cid);
        num_ctgs_found++;
      } else {
        // now read in the sequence - possibly multi-line
        seq += line;
      }
      //progbar.update();
    }
    if (!seq.empty()) {
      Vertex v{};
      v.cid = cid;
      _graph->add_vertex(v, seq);
      seq.clear();
    }
    //progbar.done();
    barrier();
  }
  SOUT("Found ", reduce_one(num_ctgs_found, op_fast_add, 0).wait(), " contigs, max id ",
       reduce_one(max_cid, op_fast_max, 0).wait(), "\n");
                
  auto num_vertices = _graph->get_num_vertices();
  SOUT("Added ", num_vertices, " vertices\n");
  // implicit barrier() when BarrierTimer is destroyed //
}


struct Aln {
  string read_id;
  cid_t cid;
  int rstart, rstop, rlen, cstart, cstop, clen;
  char orient;
  int score1, score2;
  
  void parse(const string &line) {
    // format for meraligner outputs
    // key        rid     rstart+1 rstop rlen cid            cstart  cstop   clen    orient  dummy dummy dummy dummy score1  score2
    // MERALIGNER-0 @000/1  1        149   149  Contig2247753  413995  414143  458874  Plus    0     0     0     0   220     104
    stringstream ss(line);
    string key;
    getline(ss, key, '\t');
    getline(ss, read_id, '\t');
    replace_spaces(read_id);
    string cid_str, orient_str;
    int dummy;
    ss >> rstart >> rstop >> rlen >> cid_str >> cstart >> cstop >> clen >> orient_str;
    for (int i = 0; i < 4; i++) ss >> dummy;
    ss >> score1 >> score2;
    rstart--;
    cstart--;
    if (cid_str[6] == '_') cid = stol(cid_str.substr(7));
    else cid = stol(cid_str.substr(6));
    orient = orient_str == "Plus" ? '+' : '-';
    if (orient == '-') {
      int tmp = cstart;
      cstart = clen - cstop;
      cstop = clen - tmp;
    }
  }

  string to_string() {
    ostringstream os;
    os << read_id << " " << rstart << " " << rstop << " " << rlen << " "
       << cid << " " << cstart << " " << cstop << " " << clen << " " << orient << " " << score1 << " " << score2;
    return os.str();
  }
};


// gets all the alns for a single read, and returns an aln for the next read id following
pair<shared_ptr<Aln>, bool> get_alns_for_read(istream &f, shared_ptr<Aln> prev_aln, vector<Aln> &alns, int64_t *nalns, int64_t *unaligned) 
{
  const int ALN_SLOP = 0;
  alns.clear();
  string start_read_id;
  if (prev_aln) {
    start_read_id = prev_aln->read_id;
    if (prev_aln->clen > 0) alns.push_back(*prev_aln);
  }
  string line;
  while (getline(f, line)) {
    (*nalns)++;
    if (line == "") DIE("line is empty at offset ", f.tellg(), "\n");
    // for MERALIGNER-F
    if (line[11] == 'F') continue;
    auto aln = make_shared<Aln>();
    aln->parse(line);
    // use all alns to update vertex depths
    _graph->update_vertex_depth(aln->cid, (aln->rstop - aln->rstart), aln->clen);
    // skip this aln if there is too much unaligned with the contig
    int unaligned_left = min(aln->rstart, aln->cstart);
    int unaligned_right = min(aln->rlen - aln->rstop, aln->clen - aln->cstop);
    bool is_unaligned = (unaligned_left > ALN_SLOP) || (unaligned_right > ALN_SLOP);
    if (is_unaligned) (*unaligned)++;
    // alns for a new read
    if (start_read_id != "" && aln->read_id != start_read_id) {
      if (is_unaligned) return {nullptr, false};
      return {aln, false};
    }
    start_read_id = aln->read_id;
    // clen could be 0 for MERALIGNER-F placeholder, which should have been skipped already
    if (!aln->clen) DIE("Contig length is 0 for contig ", aln->cid, "\n");
    if (!is_unaligned) alns.push_back(*aln);
    progress();
  }
  return {nullptr, true};
}


struct AlnStats {
  int64_t nalns, unaligned, short_alns, containments, circular, mismatched, conflicts, bad_overlaps, questionable_spans, excess_edges;
  
  string to_string() const {
    int64_t tot_nalns = reduce_one(nalns, op_fast_add, 0).wait();
    int64_t tot_unaligned = reduce_one(unaligned, op_fast_add, 0).wait();
    int64_t tot_containments = reduce_one(containments, op_fast_add, 0).wait();
    int64_t tot_short_alns = reduce_one(short_alns, op_fast_add, 0).wait();
    int64_t tot_circular = reduce_one(circular, op_fast_add, 0).wait();
    int64_t tot_mismatched = reduce_one(mismatched, op_fast_add, 0).wait();
    int64_t tot_conflicts = reduce_one(conflicts, op_fast_add, 0).wait();
    int64_t tot_bad_overlaps = reduce_one(bad_overlaps, op_fast_add, 0).wait();
    int64_t tot_questionable_spans = reduce_one(questionable_spans, op_fast_add, 0).wait();
    int64_t tot_excess_edges = reduce_one(excess_edges, op_fast_add, 0).wait();
    int64_t nbad = tot_unaligned + tot_containments + tot_short_alns + tot_circular +
      tot_mismatched + tot_conflicts + tot_bad_overlaps;
    
    ostringstream os;
    os.precision(2);
    os << fixed;
    os << "Splints:\n";
    //os << "    used:               " << perc_str(tot_nalns - nbad, tot_nalns) << endl;
    os << "    unaligned:          " << perc_str(tot_unaligned, tot_nalns) << endl;
    os << "    containments:       " << perc_str(tot_containments, tot_nalns) << endl;
    os << "    short alns:         " << perc_str(tot_short_alns, tot_nalns) << endl;
    os << "    circular:           " << perc_str(tot_circular, tot_nalns) << endl;
    os << "    mismatched:         " << perc_str(tot_mismatched, tot_nalns) << endl;
    os << "    conflicts:          " << perc_str(tot_conflicts, tot_nalns) << endl;
    os << "    bad overlaps:       " << perc_str(tot_bad_overlaps, tot_nalns) << endl;
    os << "Excess edges: " << perc_str(tot_excess_edges, _graph->get_num_edges()) << endl;
    return os.str();
  }
};


void add_splint(const Aln *aln1, const Aln *aln2, AlnStats &stats)
{
//TESTING  
//  if (aln1->score1 < 99 || aln2->score1 < 99) return;
  
  struct AlnCoords {
    int start, stop;
  };

  auto get_aln_coords = [](const Aln *aln) -> AlnCoords {
    // get contig start and end positions in read coordinate set
    return { .start = aln->rstart - aln->cstart, .stop = aln->rstop + (aln->clen - aln->cstop) };
  };

  AlnCoords ctg1 = get_aln_coords(aln1);
  AlnCoords ctg2 = get_aln_coords(aln2);
  
  auto is_contained = [](const AlnCoords &ctg1, const AlnCoords &ctg2) -> bool {
    if (ctg1.start >= ctg2.start && ctg1.stop <= ctg2.stop) return true;
    else return false;
  };
  
  if (is_contained(ctg1, ctg2) || is_contained(ctg2, ctg1)) {
    stats.containments++;
    return;
  }
  if (aln1->cid == aln2->cid) {
    stats.circular++;
    return;
  }
  int end1, end2;
  int gap;
  int gap_start;
  if (ctg1.start <= ctg2.start) {
    end1 = aln1->orient == '+' ? 3 : 5;
    end2 = aln2->orient == '+' ? 5 : 3;
    gap = ctg2.start - ctg1.stop;
    gap_start = ctg1.stop;
  } else {
    end1 = aln1->orient == '+' ? 5 : 3;
    end2 = aln2->orient == '+' ? 3 : 5;
    gap = ctg1.start - ctg2.stop;
    gap_start = ctg2.stop;
  }
  int min_aln_len = min(aln1->rstop - aln1->rstart, aln2->rstop - aln2->rstart);

  if (gap < -min_aln_len) {
    stats.short_alns++;
    return;
  }
  /*
  if (gap < -(_options->max_kmer_len - 1)) {
    stats.short_alns++;
    return;
  }
  */
  if (gap < -aln1->rlen) DIE("Gap is too small: ", gap, ", read length ", aln1->rlen, "\n");
//  int max_gap_size = aln1->rlen - 2 * (_options->max_kmer_len - 1);
//  if (gap > max_gap_size) WARN("Gap is too big: ", gap, " > ", max_gap_size, "\n(Did you pass in the correct max kmer length?)\n");
  // check for bad overlaps
  if (gap < 0 && (aln1->clen < -gap || aln2->clen < -gap)) {
    stats.bad_overlaps++;
    return;
  }
  char orient1 = aln1->orient;
  CidPair cids = { .cid1 = aln1->cid, .cid2 = aln2->cid };
  if (aln1->cid < aln2->cid) {
    swap(end1, end2);
    swap(cids.cid1, cids.cid2);
    orient1 = aln2->orient;
  }
  Edge edge = { .cids = cids, .edge_type = EdgeType::SPLINT, .end1 = end1, .end2 = end2, 
                .gap = gap, .gap_uncertainty = 0, .support = 1, .aln_len = min_aln_len,
                .seq = "", .gap_reads = {}, .aln_score = min(aln1->score1, aln2->score1),
                .mismatch_error = false, .conflict_error = false, .excess_error = false };
  if (gap > 0) {
    edge.gap_reads = vector<GapRead>{GapRead(aln1->read_id, gap_start, -1, -1, orient1, cids.cid1)};
    _graph->add_pos_gap_read(aln1->read_id);
  } else {
    edge.gap_reads = vector<GapRead>{};
  }
  _graph->add_or_update_edge(edge);
}


void purge_and_set_nbs(AlnStats &stats)
{
  BarrierTimer timer(__func__);
  // purge mismatches and conflicts
  _graph->purge_error_edges(&stats.mismatched, &stats.conflicts, &stats.questionable_spans);
  barrier();
  int64_t tot_clen = 0;
  int64_t num_excess = 0;
  int max_excess = 0;
  ProgressBar progbar(_graph->get_local_num_edges(), "Updating edges");
  // add edges to vertices
  for (auto edge = _graph->get_first_local_edge(); edge != nullptr; edge = _graph->get_next_local_edge()) {
    progbar.update();
    auto v1 = _graph->get_vertex(edge->cids.cid1);
    if (v1->end5.size() + v1->end3.size() > MAX_DEGREE) {
      edge->excess_error = true;
      tot_clen += v1->clen;
      num_excess++;
      max_excess = std::max(max_excess, v1->clen);
      //_graph->mark_vertex_excess_error(edge->cids.cid1);
      continue;
    }
    auto v2 = _graph->get_vertex(edge->cids.cid2);
    if (v2->end5.size() + v2->end3.size() > MAX_DEGREE) {
      edge->excess_error = true;
      tot_clen += v2->clen;
      num_excess++;
      max_excess = std::max(max_excess, v2->clen);
      continue;
    }
    // minor race condition here but it shouldn't lead to very high degrees
    _graph->add_vertex_nb(edge->cids.cid1, edge->cids.cid2, edge->end1);
    _graph->add_vertex_nb(edge->cids.cid2, edge->cids.cid1, edge->end2);
  }
  progbar.done();
  auto avg_excess = (num_excess > 0 ? tot_clen / num_excess : 0);
  int64_t avg_excess_clen = reduce_one(avg_excess, op_fast_add, 0).wait() / rank_n();
  int64_t max_excess_clen = reduce_one(max_excess, op_fast_max, 0).wait();
  SOUT("Avg excess clen ", avg_excess_clen, " max ", max_excess_clen, "\n");
  barrier();
  _graph->purge_excess_edges(&stats.excess_edges);
  barrier();
  int64_t num_edges_found = 0;
  int64_t num_zero_depth = 0;
  int64_t zero_depth_len = 0;
  int64_t ctg_lens = 0;

#ifdef OUTPUT_CONTIG_GRAPH
  string graph_fname = string(_options->cached_io ? _options->local_tmp_dir + "/" : "") + "contig_graph";
  get_rank_path(graph_fname, upcxx::rank_me());
  FILE *f = fopen(graph_fname.c_str(), "w");
#endif
  {
    ProgressBar progbar(_graph->get_local_num_vertices(), "Updating edge depths");
    // adjust the depths for all vertices
    for (auto v = _graph->get_first_local_vertex(); v != nullptr; v = _graph->get_next_local_vertex()) {
      if (v->depth == 0) {
        num_zero_depth++;
        zero_depth_len += v->clen;
      }
      ctg_lens += v->clen;
      // sanity check
      num_edges_found += v->end5.size() + v->end3.size();
#ifdef OUTPUT_CONTIG_GRAPH
      //fprintf(f, "Contig%ld\t%d\t%f\n", v->cid, v->clen, v->depth);
      fprintf(f, "Contig %ld  Five ", v->cid);
      for (auto nb_cid : v->end5) fprintf(f, "%ld ", nb_cid);
      fprintf(f, "  Three ");
      for (auto nb_cid : v->end3) fprintf(f, "%ld ", nb_cid);
      fprintf(f, "\n");
#endif
      progbar.update();
    }
#ifdef OUTPUT_CONTIG_GRAPH
    fclose(f);
#endif
    barrier();
    progbar.done();
  }
  auto num_edges = _graph->get_num_edges();
  auto tot_num_edges_found = reduce_one(num_edges_found, op_fast_add, 0).wait();
  if (!rank_me() && tot_num_edges_found / 2 != num_edges)
    SDIE("Mismatch between edges expected, ", num_edges, " and those in vertices, ", num_edges_found, "\n");
  int64_t tot_num_zero_depth = reduce_all(num_zero_depth, op_fast_add).wait();
  if (tot_num_zero_depth) {
    int64_t tot_zero_depth_len = reduce_one(zero_depth_len, op_fast_add, 0).wait();
    int64_t tot_ctg_lens = reduce_one(ctg_lens, op_fast_add, 0).wait();
    auto num_vertices = _graph->get_num_vertices();
    SOUT("Found ", perc_str(tot_num_zero_depth, num_vertices), " zero depth contigs (no read maps) ",
         "total length ", perc_str(tot_zero_depth_len, tot_ctg_lens), "\n");
  }
  SOUT(stats.to_string());
  // implicit barrier() when BarrierTimer is destroyed //
}


AlnStats parse_alns()
{
  BarrierTimer timer(__func__);
  AlnStats stats = {0};
  IntermittentTimer t_get_alns_splints("get alns splints"), t_io("alns file IO");
  for (auto alns_fname : _options->alns_fname_list) {
    stringstream alns_file_buf;
    {
      t_io.start();
      zstr::ifstream alns_file(alns_fname);
      alns_file_buf << alns_file.rdbuf();
      t_io.stop();
    }
    //ProgressBar progbar(alns_fname, &alns_file_buf, "Parsing alns file " + alns_fname);
    shared_ptr<Aln> next_aln(nullptr);
    while (!alns_file_buf.eof()) {
      vector<Aln> alns;
      bool is_done = false;
      t_get_alns_splints.start();
      tie(next_aln, is_done) = get_alns_for_read(alns_file_buf, next_aln, alns, &stats.nalns, &stats.unaligned);
      t_get_alns_splints.stop();
      if (is_done) break;
      for (int i = 0; i < alns.size(); i++) {
        auto aln = &alns[i];
        for (int j = i + 1; j < alns.size(); j++) {
          progress();
          auto other_aln = &alns[j];
          if (other_aln->read_id != aln->read_id) DIE("Mismatched read ids: ", other_aln->read_id, " != ", aln->read_id, "\n");
          add_splint(other_aln, aln, stats);
        }
      }
    }
  }
  _graph->flush_vertex_depth_updates();
  SOUT("Read ", reduce_one(stats.nalns, op_fast_add, 0).wait(), " lines from alns file\n");
  // implicit barrier() when BarrierTimer is destroyed //
  return stats;
}


void parse_spans() 
{
  // the file looks like this 
  // span    contig1.end      contig2.end     anom spans|tot spans    span gap      uncertainty in gap
  // SPAN    Contig430937.5<=>Contig64012.3            0|8             -60             11

  if (_options->spans_fname_list.empty()) return;
  BarrierTimer timer(__func__);
  string span_prefix = "SPAN\t";
  string ctg_prefix = "Contig";
  int64_t num_spans = 0;
  IntermittentTimer t_io("spans file IO");
  for (auto spans_fname : _options->spans_fname_list) {
    stringstream spans_file_buf;
    {
      t_io.start();
      zstr::ifstream spans_file(spans_fname);
      spans_file_buf << spans_file.rdbuf();
      t_io.stop();
    }
    //ProgressBar progbar(spans_fname, &spans_file, "Parsing spans file");
    string line;
    while (!spans_file_buf.eof()) {
      num_spans++;
      getline(spans_file_buf, line);
      if (line.length() == 0) break;
      regex rex("SPAN\\tContig(\\d+)\\.(\\d)<=>Contig(\\d+)\\.(\\d)\\t(\\d+)\\|(\\d+)\\t(-*\\d+)\\t(-*\\d+)");
      smatch sm;
      if (!regex_match(line, sm, rex)) {
        WARN("Failed to find correct format string in links file ", spans_fname, " on line ", num_spans, " line: ", line);
        break;
      } 
      cid_t cid1 = stoi(sm[1]);
      int end1 = stoi(sm[2]);
      cid_t cid2 = stoi(sm[3]);
      int end2 = stoi(sm[4]);
      int anom_spans = stoi(sm[5]);
      int good_spans = stoi(sm[6]);
      int gap = stoi(sm[7]);
      int gap_uncertainty = stoi(sm[8]);

      if (gap < -(_options->max_kmer_len - 1)) {
        DBG_BUILD("excessive negative gap in span ", gap, "\n");
        continue;
      }

      CidPair cids = { .cid1 = cid1, .cid2 = cid2 };
      if (cid1 < cid2) {
        swap(end1, end2);
        swap(cids.cid1, cids.cid2);
      }
      Edge edge = { .cids = cids, .edge_type = EdgeType::SPAN, .end1 = end1, .end2 = end2, 
                    .gap = gap, .gap_uncertainty = gap_uncertainty, .support = good_spans, .aln_len = _options->kmer_len,
                    .seq = "", .gap_reads = {}, .aln_score = _options->kmer_len,
                    .mismatch_error = false, .conflict_error = false, .excess_error = false };
      _graph->add_or_update_edge(edge);

      progress();
    }
  }
  Timings::print_barrier_timings("Loaded spans file(s)");
  // now add all the new span-only edges
  int64_t num_spans_only = 0;
  int64_t num_pos_spans = 0;
  for (auto edge = _graph->get_first_local_edge(); edge != nullptr; edge = _graph->get_next_local_edge()) {
    if (edge->edge_type == EdgeType::SPAN) {
      num_spans_only++;
      if (edge->gap > 0) {
        _graph->add_span_cids(edge->cids.cid1, edge->cids);
        _graph->add_span_cids(edge->cids.cid2, edge->cids);
        num_pos_spans++;
      }
    }
  }
  Timings::print_barrier_timings("added new spans");
  auto tot_num_spans = reduce_one(num_spans, op_fast_add, 0).wait();
  auto tot_num_spans_only = reduce_one(num_spans_only, op_fast_add, 0).wait();
  SOUT("Found ", perc_str(tot_num_spans, _graph->get_num_edges()), " spans, of which ",
       perc_str(tot_num_spans_only, _graph->get_num_edges()), " formed span-only edges\n");
  SOUT("Found ", perc_str(reduce_one(num_pos_spans, op_fast_add, 0).wait(), tot_num_spans_only), " pos gap span-only edges\n");
  // implicit barrier() when BarrierTimer is destroyed //
}


void parse_alns_for_spans()
{
  if (_options->spans_fname_list.empty()) return;
  BarrierTimer timer(__func__);
  int64_t nalns, unaligned;
  shared_ptr<CtgGraph> &graph = _graph;
  IntermittentTimer t_get_alns_for_reads("get alns for reads"), t_io("alns file IO");
  for (auto alns_fname : _options->alns_fname_list) {
    stringstream alns_file_buf;
    {
      t_io.start();
      zstr::ifstream alns_file(alns_fname);
      alns_file_buf << alns_file.rdbuf();
      t_io.stop();
    }
    //ProgressBar progbar(alns_fname, &alns_file, "Parsing alns file for spans");
    shared_ptr<Aln> next_aln(nullptr);
    while (!alns_file_buf.eof()) {
      vector<Aln> alns;
      bool is_done = false;
      t_get_alns_for_reads.start();      
      tie(next_aln, is_done) = get_alns_for_read(alns_file_buf, next_aln, alns, &nalns, &unaligned);
      t_get_alns_for_reads.stop();
      if (is_done) break;
      for (auto aln : alns) {
        progress();
        auto cids = _graph->get_span_cids(aln.cid);
        if (cids.cid1 == -1) continue;
        // check that read overlaps the ends
        if (aln.rstart == 0 && aln.rstop == aln.rlen) continue;
        // check that read does not overlap contig on both sides
        if (aln.rstart > 0 && aln.rstop < aln.rlen) continue;
        auto edge = _graph->get_edge(cids.cid1, cids.cid2);
        if (edge->edge_type != EdgeType::SPAN) DIE("this should be a span edge!\n");
        int end = (aln.cid == cids.cid1 ? edge->end1 : edge->end2);
        int gap_start;
        if (aln.cid == edge->cids.cid1) {
          // tail
          if ((end == 3 && aln.orient == '+') || (end == 5 && aln.orient == '-')) gap_start = aln.rstop;
          else gap_start = aln.rlen - aln.rstart;
          if (gap_start == aln.rlen) continue;
        } else if (aln.cid == edge->cids.cid2) {
          // head
          if ((end == 5 && aln.orient == '+') || (end == 3 && aln.orient == '-')) gap_start = aln.rstart;
          else gap_start = aln.rlen - aln.rstop;
          if (gap_start == 0) continue;
        } else {
          DIE("cid doesn't match in pos edge\n");
        }
        //DBG("Found aln cid ", aln.cid, ".", end, " in spans, (", cids.cid1, ", ", cids.cid2, ") readid ", aln.read_id,
        //    " rlen ", aln.rlen, " rstart ", aln.rstart, " rstop ", aln.rstop,
        //    " clen ", aln.clen, " cstart ", aln.cstart, " cstop ", aln.cstop, " orient ", aln.orient,
        //    " gap_start ", gap_start, "\n");
        // now update the edge's gap reads, push the new data on
        _graph->add_edge_gap_read(cids, { aln.read_id, gap_start, aln.rstart, aln.rstop, aln.orient, aln.cid },
                                  aln.rstop - aln.rstart, aln.score1);
        _graph->add_pos_gap_read(aln.read_id);

      }
    }
  }
  Timings::print_barrier_timings("before flushes"); // to avoid possible deadlock between aggr_stores
  _graph->flush_edge_gap_reads_first();
  _graph->flush_vertex_depth_updates_first();
  _graph->flush_edge_gap_reads();
  _graph->flush_vertex_depth_updates();
  _track_rpcs->flush();
  auto num_purged = _graph->purge_no_read_spans();
  SOUT("Purged ", reduce_one(num_purged, op_fast_add, 0).wait(), " pos gap spans with no reads\n");
  // implicit barrier() when BarrierTimer is destroyed //
}


static void adjust_read_name(string &read_name)
{
  auto len = read_name.length();
  // strip trailing whitespace
  auto end_pos = read_name.find_last_not_of(' ');
  if (end_pos != string::npos) read_name.erase(end_pos + 1);
  // convert if new illumina 2 format  or HudsonAlpha format
  if (read_name[len - 2] != '/') {
    if (read_name[len - 2] == 'R') {
      // HudsonAlpha format  (@pair-R1,  @pair-R2)   len -3 == 0, len -2 == 1, len - 1 == 2
      // replace with @pair/1 or @pair/2 */
      char rnum = read_name[len - 1];
      read_name[len - 3] = '/';
      read_name[len - 2] = rnum;
      read_name.erase(len - 1);
    } else {
      // Latest Illumina header format
      auto pos = read_name.find(" ");
      // no comment, just return the name without modification
      if (pos == string::npos) return;
      read_name[pos] = '/';
      read_name.erase(pos + 2);
    }
  }
}


string get_consensus_seq(const vector<string> &seqs, int max_len)
{
  static char bases[5] = {'A', 'C', 'G', 'T', 'N'};
  auto base_freqs = new int[max_len][4]();
  for (auto seq : seqs) {
    for (int i = 0; i < seq.size(); i++) {
      switch (seq[i]) {
        case 'A': base_freqs[i][0]++; break;
        case 'C': base_freqs[i][1]++; break;
        case 'G': base_freqs[i][2]++; break;
        case 'T': base_freqs[i][3]++; break;
        case 'N': break;
        default:
          WARN("Unknown base at pos ", i, " (", seq.size(), "): ", seq[i], "\n", seq);
      }
    }
  }
  string consensus_seq = "";
  for (int i = 0; i < max_len; i++) {
    int max_freq = 0;
    // start off with N
    int highest_idx = 4;
    for (int j = 0; j < 4; j++) {
      if (base_freqs[i][j] > max_freq) {
        max_freq = base_freqs[i][j];
        highest_idx = j;
      }
    }
    consensus_seq += bases[highest_idx];
  }
  delete[] base_freqs;
  // trim any Ns off the front
  consensus_seq.erase(0, consensus_seq.find_first_not_of('N'));
  return consensus_seq;
}

  
string get_splint_edge_seq(Edge *edge)
{
  vector<string> seqs;
  // tail and end for checking primer matches
  int gap_size = edge->gap + 2 * (_options->kmer_len - 1);
  for (auto gap_read : edge->gap_reads) {
    auto seq = _graph->get_read_seq(gap_read.read_name);
    if (seq == "") DIE("Could not find read seq for read ", gap_read.read_name, "\n");
    if (gap_read.gap_start < _options->kmer_len) {
      WARN("Positive gap overlap is less than kmer length, ", gap_read.gap_start, " < ", _options->kmer_len, "\n");
      continue;
    }
    int rstart = gap_read.gap_start - (_options->kmer_len - 1);
    string gap_seq = seq.substr(rstart, gap_size);
    if (edge->gap_reads.size() > 1) {
      string gap_seq_rc = revcomp(gap_seq);
      // these sequences should all be similar, so choosing the lexicographically least should ensure they have the same orientation
      if (gap_seq > gap_seq_rc) gap_seq = gap_seq_rc;
    }
    seqs.push_back(gap_seq);
  }
  return get_consensus_seq(seqs, gap_size);
}


string get_span_edge_seq(Edge *edge, bool tail)
{
  string ctg_seq = "";
  // this is all just for debugging output
#ifdef IS_DEBUG_BUILD
  if (tail) {
    auto vertex = _graph->get_vertex(edge->cids.cid1);
    ctg_seq = _graph->get_vertex_seq(vertex->seq_gptr, vertex->clen);
    if (edge->end1 == 5) ctg_seq = revcomp(ctg_seq);
    int tail_len = vertex->clen - _options->kmer_len;
    if (tail_len < 0) tail_len = 0;
    ctg_seq = ctg_seq.substr(tail_len);
    char buf[100];
    sprintf(buf, "TAIL contig%7ld.%d   %s\n", vertex->cid, edge->end1, ctg_seq.c_str());
    DBG_BUILD(buf);
  } else {
    auto vertex = _graph->get_vertex(edge->cids.cid2);
    ctg_seq = _graph->get_vertex_seq(vertex->seq_gptr, vertex->clen);
    if (edge->end2 == 3) ctg_seq = revcomp(ctg_seq);
    ctg_seq = ctg_seq.substr(0, _options->kmer_len);
    char buf[400];
    string padding(4 + _graph->max_read_len - _options->kmer_len, ' ');
    sprintf(buf, "HEAD contig%7ld.%d%s%s\n", vertex->cid, edge->end2, padding.c_str(), ctg_seq.c_str());
    DBG_BUILD(buf);
  }
#endif
  cid_t cid = (tail ? edge->cids.cid1 : edge->cids.cid2);
  vector<string> seqs;
  char buf[100];
  int max_len = 0;
  for (auto gap_read : edge->gap_reads) {
    if (gap_read.cid != cid) continue;
    sprintf(buf, "gs %3d rs %3d rp %3d %c ", gap_read.gap_start, gap_read.rstart, gap_read.rstop, gap_read.orient);
    auto gap_seq = _graph->get_read_seq(gap_read.read_name);
    if (gap_seq == "") DIE("Could not find read seq for read ", gap_read.read_name, "\n");
    if (tail) {
      if ((edge->end1 == 5 && gap_read.orient == '+') || (edge->end1 == 3 && gap_read.orient == '-')) gap_seq = revcomp(gap_seq);
      if (gap_read.gap_start > _options->kmer_len) {
        gap_seq.erase(0, gap_read.gap_start - _options->kmer_len);
        gap_read.gap_start = _options->kmer_len;
      }
      DBG_BUILD(buf, gap_seq, "\n");
    } else {
      if ((edge->end2 == 3 && gap_read.orient == '+') || (edge->end2 == 5 && gap_read.orient == '-')) gap_seq = revcomp(gap_seq);
      if (gap_read.gap_start + _options->kmer_len < gap_seq.size()) gap_seq.erase(gap_read.gap_start + _options->kmer_len);
      // pad the front of the gap sequence with Ns to make them all the same length
      string offset_padding(1 + _graph->max_read_len - _options->kmer_len - gap_read.gap_start, 'N');
      gap_seq = offset_padding + gap_seq;
      DBG_BUILD(buf, gap_seq, "\n");
    }
    seqs.push_back(gap_seq);
    if (gap_seq.size() > max_len) max_len = gap_seq.size();
  }
  if (seqs.empty()) return ctg_seq;
  return get_consensus_seq(seqs, max_len);
}


void parse_reads()
{
  BarrierTimer timer(__func__);
  int64_t num_found = 0;
  string read_record[4];
  int max_read_len = 0;
  int64_t num_seqs_added = 0;
  IntermittentTimer t_io("reads file IO");
  for (auto reads_fname : _options->reads_fname_list) {
    stringstream reads_file_buf;
    {
      t_io.start();
      zstr::ifstream reads_file(reads_fname);
      reads_file_buf << reads_file.rdbuf();
      t_io.stop();
    }
    //ProgressBar progbar(reads_fname, &reads_file, "Parsing reads file " + reads_fname);
    promise<> prom;
    uint64_t bytes_read = 0;
    while (!reads_file_buf.eof()) {
      bool done = false;
      for (int i = 0; i < 4; i++) {
        getline(reads_file_buf, read_record[i]);
        if (read_record[i] == "") {
          done = true;
          break;
        }
        bytes_read += read_record[i].length();
        //progbar.update(bytes_read);
        num_found++;
      }
      if (done) break;
      adjust_read_name(read_record[0]);
      if (_graph->update_read_seq(read_record[0], read_record[1])) num_seqs_added++;
      if (read_record[1].length() > max_read_len) max_read_len = read_record[1].length();
      progress();
    }
    //progbar.done();
    barrier();
  }
  
  _graph->max_read_len = reduce_all(max_read_len, op_fast_max).wait();  
  auto tot_num_found = reduce_one(num_found, op_fast_add, 0).wait();
  SOUT("Processed a total of ", tot_num_found, " reads, found max read length ", _graph->max_read_len, "\n");
  SOUT("Extracted ", perc_str(reduce_one(num_seqs_added, op_fast_add, 0).wait(), tot_num_found), " read sequences for pos gaps\n");

  int64_t num_pos_gaps = 0;
  int64_t num_pos_spans = 0;
  int64_t num_pos_spans_closed = 0;
  int64_t num_pos_spans_w_ns = 0;
  {
    ProgressBar progbar(_graph->get_local_num_edges(), "Fill pos gaps");
    for (auto edge = _graph->get_first_local_edge(); edge != nullptr; edge = _graph->get_next_local_edge()) {
      progbar.update();
      if (edge->gap <= 0) continue;
      num_pos_gaps++;
      if (edge->edge_type == EdgeType::SPAN) {
        num_pos_spans++;
        DBG_BUILD("SPAN pos gap ", edge->gap, " with ", edge->gap_reads.size(), " reads\n");
        string tail_seq = get_span_edge_seq(edge, true);
        if (tail_seq == "") continue;
        string head_seq = get_span_edge_seq(edge, false);
        if (head_seq == "") continue;
        DBG_BUILD("tail consensus         ", tail_seq, "\n");
        int offset = _options->kmer_len + edge->gap - head_seq.size() + _options->kmer_len;
        if (offset < 1) offset = 1;
        DBG_BUILD("head consensus         ", string(offset, ' '), head_seq, "\n");
        // now merge tail_seq and head_seq using the best (lowest hamming dist) overlap
        int min_len = min(tail_seq.size(), head_seq.size());
        int max_len = max(tail_seq.size(), head_seq.size());
        auto min_dist = min_hamming_dist(tail_seq, head_seq, min_len);
        if (is_overlap_mismatch(min_dist.first, min_dist.second)) {
          min_dist.first = min_len;
          min_dist.second = -1;
          for (int i = 0; i < max_len - min_len; i++) {
            int dist = hamming_dist(tail_seq.substr(0, min_len), head_seq.substr(0, min_len));
            if (dist < min_dist.first) {
              min_dist.first = dist;
              min_dist.second = i;
            }
          }
          if (is_overlap_mismatch(min_dist.first, min_dist.second)) {
            DBG_BUILD("overlap mismatch: hdist ", min_dist.first, " best overlap ", min_dist.second, " original gap ", edge->gap, "\n");
            if (tail_seq.size() + head_seq.size() < 2 * _options->kmer_len + edge->gap) {
              // the gap is not closed - fill with Ns
              int num_ns = 2 * _options->kmer_len + edge->gap - tail_seq.size() - head_seq.size();
              edge->seq = tail_seq + string(num_ns, 'N') + head_seq;
              // remove one of either end because the later checking will use kmer_len - 1
              edge->seq = edge->seq.substr(1, edge->seq.size() - 2);
              DBG_BUILD("using orig gap ", edge->gap, " with ", num_ns, " Ns: ", edge->seq, "\n");
              num_pos_spans_w_ns++;
              num_pos_spans_closed++;
            }
            continue;
          }
        }
        num_pos_spans_closed++;
        int gap_size = tail_seq.size() + head_seq.size() - min_dist.second - 2 * _options->kmer_len;
        DBG_BUILD("overlap is ", min_dist.second, " original gap is ", edge->gap, " corrected gap is ", gap_size, "\n");
        edge->gap = gap_size;
        DBG_BUILD(tail_seq, "\n");
        DBG_BUILD(string(tail_seq.size() - min_dist.second, ' '), head_seq, "\n");
        tail_seq.erase(tail_seq.size() - min_dist.second);
        edge->seq = tail_seq + head_seq;
        edge->seq = edge->seq.substr(1, edge->seq.size() - 2);
        DBG_BUILD(edge->seq, "\n");
        if (edge->seq.size() != 2 * (_options->kmer_len - 1) + gap_size)
          WARN("fill mismatch ", edge->seq.size(), " != ", 2 * _options->kmer_len + gap_size);
      } else {
        edge->seq = get_splint_edge_seq(edge);
      }
    }
    progbar.done();
    barrier();
  }
  auto tot_pos_gaps = reduce_one(num_pos_gaps, op_fast_add, 0).wait();
  SOUT("Filled ", tot_pos_gaps, " positive gaps with ", _graph->get_num_read_seqs(), " reads\n");
  auto tot_pos_spans = reduce_one(num_pos_spans, op_fast_add, 0).wait();
  auto tot_pos_spans_closed = reduce_one(num_pos_spans_closed, op_fast_add, 0).wait();
  auto tot_pos_spans_w_ns = reduce_one(num_pos_spans_w_ns, op_fast_add, 0).wait();
  SOUT("Found ", perc_str(tot_pos_spans, tot_pos_gaps), " positive spans, of which ",
       perc_str(tot_pos_spans_closed, tot_pos_spans), " were closed - ",
       perc_str(tot_pos_spans_w_ns, tot_pos_spans_closed), " with Ns\n");
  // implicit barrier() when BarrierTimer is destroyed //
}


void parse_depths() 
{
  BarrierTimer timer(__func__);
  if (_options->depths_fname_list.empty()) return;
  string ctg_prefix = "Contig";
  IntermittentTimer t_io("depths file IO");
  for (auto depths_fname : _options->depths_fname_list) {
    stringstream depths_file_buf;
    {
      t_io.start();
      zstr::ifstream depths_file(depths_fname);
      depths_file_buf << depths_file.rdbuf();
      t_io.stop();
    }
    //ProgressBar progbar(depths_fname, &depths_file, "Parsing depths file");
    string line;
    while (!depths_file_buf.eof()) {
      getline(depths_file_buf, line);
      if (line == "") break;
      stringstream ss(line);
      string ctg_name;
      int clen;
      double depth;
      ss >> ctg_name >> clen >> depth;
      if (ctg_name.length() > 0) {
        cid_t cid = stol(ctg_name.substr(ctg_prefix.size()));
        _graph->set_vertex_depth(cid, depth);
      }
      //progbar.update();
      progress();
    }
    //progbar.done();
    barrier();
  }
  // implicit barrier() when BarrierTimer is destroyed //
}


bool merge_end(Vertex *curr_v, const vector<cid_t> &nb_cids, vector<vector<cid_t> > &nb_cids_merged, 
               IntermittentTimer &t_merge_get_nbs, IntermittentTimer &t_merge_sort_nbs, IntermittentTimer &t_merge_output_nbs) 
{
  nb_cids_merged.clear();

  // dead-end
  if (!nb_cids.size()) {
    DBG_BUILD("\tdead end\n");
    progress();
    return false;
  }

  struct NbPair {
    shared_ptr<Vertex> vertex;
    shared_ptr<Edge> edge;
  };
  
  t_merge_get_nbs.start();
  vector<NbPair> nbs;
  // select the neighbors that are valid for connections
  for (auto nb_cid : nb_cids) {
    auto nb_edge = _graph->get_edge_cached(curr_v->cid, nb_cid);
    // drop low support for high degree nodes - this reduces computational overhead and load imbalance, which can get severe
    if (nb_cids.size() > 50 && nb_edge->support < 2) continue;
    if (nb_cids.size() > 100 && nb_edge->support < 3) continue;
    if (nb_cids.size() > 150 && nb_edge->support < 4) continue;
    nbs.push_back({_graph->get_vertex_cached(nb_cid), nb_edge});
  }
  t_merge_get_nbs.stop();
  if (nbs.empty()) return false;
  // just one nb found
  if (nbs.size() == 1) {
    nb_cids_merged.push_back({nbs[0].vertex->cid});
    DBG_BUILD("\t", nbs[0].vertex->cid, " gap ", nbs[0].edge->gap, " len ", nbs[0].vertex->clen, " depth ", nbs[0].vertex->depth, "\n");
    return false;
  }
  t_merge_sort_nbs.start();
  // found multiple nbs, check for overlaps that can be merged
  // first, sort nbs by gap size
  sort(nbs.begin(), nbs.end(), 
       [](const auto &elem1, const auto &elem2) {
         return elem1.edge->gap < elem2.edge->gap;
       });
  t_merge_sort_nbs.stop();

  // gather a vector of merged paths (there can be more than one because of forks)
  vector<vector<NbPair*> > all_next_nbs = {};
  // attempt to merge all neighbors as overlaps 
  for (int i = 0; i < nbs.size(); i++) {
    NbPair *nb = &nbs[i];
    DBG_BUILD("\t", nb->vertex->cid, " gap ", nb->edge->gap, " len ", nb->vertex->clen, " depth ", nb->vertex->depth, "\n");
    if (i == 0) {
      all_next_nbs.push_back({nb});
      continue;
    }
    bool found_merge = false;
    for (auto &next_nbs : all_next_nbs) {
      progress();
      auto prev_nb = next_nbs.back();
      int g1 = -prev_nb->edge->gap;
      int g2 = -nb->edge->gap;
      int gdiff = g1 - g2;
      // check gap spacing to see if current nb overlaps previous nb
      if ((prev_nb->edge->gap == nb->edge->gap) || (gdiff >= prev_nb->vertex->clen - 10) || 
          (gdiff + nb->vertex->clen <= prev_nb->vertex->clen)) {
        DBG_BUILD("\tMerge conflict ", prev_nb->vertex->cid, " ", nb->vertex->cid, "\n");
        continue;
      }
      // now check that there is an edge from this vertex to the previous one
      auto intermediate_edge = _graph->get_edge_cached(nb->vertex->cid, prev_nb->vertex->cid);
      if (!intermediate_edge) {
        DBG_BUILD("\tNo edge found between ", prev_nb->vertex->cid, " and ", nb->vertex->cid, "\n");
        continue;
      } 
      // now check the overlaps are correct
      DBG_BUILD("\tMERGE ", prev_nb->vertex->cid, " ", nb->vertex->cid, "\n");
      next_nbs.push_back(nb);
      found_merge = true;
      break;
    }
    if (!found_merge) all_next_nbs.push_back({nb});
  }
  if (all_next_nbs.empty()) DIE("empty all_next_nbs. How?\n");

  t_merge_output_nbs.start();
  // update the merged nbs and write out the merged paths found
  for (auto &next_nbs : all_next_nbs) {
    progress();
    nb_cids_merged.push_back({});
    auto last_nb = next_nbs.back();
    DBG_BUILD("\tmerged path (len ", last_nb->vertex->clen + last_nb->edge->gap, ", depth ", last_nb->vertex->depth, "): ");
    for (auto &next_nb : next_nbs) {
      DBG_BUILD(next_nb->vertex->cid, " ");
      nb_cids_merged.back().push_back(next_nb->vertex->cid);
    }
    DBG_BUILD("\n");
  }    
  t_merge_output_nbs.stop();

  return true;
}

void merge_nbs()
{
  BarrierTimer timer(__func__);
  int64_t num_merges = 0;
  int64_t num_orphans = 0;
  int max_orphan_len = 0;
  int64_t num_nbs = 0, num_merged_nbs = 0, max_nbs = 0;
  double max_orphan_depth = 0;
  {
    BarrierTimer timer2("merge_nbs inner");
    IntermittentTimer t_merge_ends("merge ends"), t_merge_get_nbs("merge get nbs"), t_merge_sort_nbs("merge sort nbs"), 
      t_merge_output_nbs("merge output nbs");
    ProgressBar progbar(_graph->get_local_num_vertices(), "Merge nbs");
    // mark all the vertices that have forks and the side of the forks. Note that in many cases what look like forks are
    // actually vertices that should be merged into a single neighbor
    for (auto v = _graph->get_first_local_vertex(); v != nullptr; v = _graph->get_next_local_vertex()) {
      DBG_BUILD(v->cid,  " len ",  v->clen,  " depth ",  v->depth,  ":\n");
      if (v->end5.empty() && v->end3.empty()) {
        num_orphans++;
        if (v->clen > max_orphan_len) max_orphan_len = v->clen;
        if (v->depth > max_orphan_depth) max_orphan_depth = v->depth;
        DBG_BUILD("\torphan: ", v->cid, " len ", v->clen, " depth ", v->depth, "\n");
      }
      t_merge_ends.start();
      DBG_BUILD("  5-end:\n");
      if (merge_end(v, v->end5, v->end5_merged, t_merge_get_nbs, t_merge_sort_nbs, t_merge_output_nbs)) num_merges++;
      DBG_BUILD("  3-end:\n");
      if (merge_end(v, v->end3, v->end3_merged, t_merge_get_nbs, t_merge_sort_nbs, t_merge_output_nbs)) num_merges++;
      t_merge_ends.stop();
      int v_num_nbs = v->end5.size() + v->end3.size();
      num_nbs += v_num_nbs;
      num_merged_nbs += v->end5_merged.size() + v->end3_merged.size();
      if (v_num_nbs > max_nbs) max_nbs = v_num_nbs;
      progbar.update();
    }
    //DBG_CORRECT_SPANS("Number of nbs ", num_nbs, " avg degree ", ((double)num_nbs / _graph->get_local_num_vertices()), 
    DBG("Number of nbs ", num_nbs, " avg degree ", ((double)num_nbs / _graph->get_local_num_vertices()), 
        " merged ", num_merged_nbs, " avg degree ", ((double)num_merged_nbs / _graph->get_local_num_vertices()), 
        " max degree ", max_nbs, "\n");
    progbar.done();
    // implicit ~BarrierTimer
  }
  auto tot_merges = reduce_one(num_merges, op_fast_add, 0).wait();
  SOUT("Merged ", perc_str(tot_merges, 2 *_graph->get_num_vertices()), " vertices\n");
  auto tot_orphans = reduce_one(num_orphans, op_fast_add, 0).wait();
  auto all_max_orphan_len = reduce_one(max_orphan_len, op_fast_max, 0).wait();
  auto all_max_orphan_depth = reduce_one(max_orphan_depth, op_fast_max, 0).wait();
  SOUT("Found ", perc_str(tot_orphans, _graph->get_num_vertices()), " orphaned vertices (no edges), max length ", 
       all_max_orphan_len, ", max depth ", all_max_orphan_depth, "\n");
  // implicit barrier() when BarrierTimer is destroyed //
}


void build_graph(shared_ptr<Options> options, shared_ptr<CtgGraph> graph)
{
  BarrierTimer timer(__func__);
  _options = options;
  _graph = graph;
  _track_rpcs = make_shared<TrackRPCs>("build_cgraph TrackRPCs");
  parse_ctgs();
  auto aln_stats = parse_alns();
  parse_spans();
  parse_alns_for_spans();
  purge_and_set_nbs(aln_stats);
  parse_reads();
  parse_depths();
  merge_nbs();
  _track_rpcs.reset();
  _graph.reset();
  _options.reset();
  // implicit barrier() when BarrierTimer is destroyed //
}
