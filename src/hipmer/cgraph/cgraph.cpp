// cgraph 
// Steven Hofmeyr, LBNL, Aug 2018

#include <iostream>
#include <math.h>
#include <algorithm>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <upcxx/upcxx.hpp>
#ifdef MPI_WRITE
#include <mpi.h>
#endif

#include "version.h"

#include "cgraph.h"

#include "ctg_graph.hpp"
#include "options.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "build_graph.hpp"
#include "walk_graph.hpp"

//#define DEBUG

using std::string;
using std::to_string;
using std::shared_ptr;

using namespace upcxx;

//#define HASH_BAR "########################################################################"

void init_upcxx()
{
  upcxx::init();
}

void finalize_upcxx()
{
  upcxx::finalize();
}

int cgraph_main(int argc, char **argv)
{
  auto start_t = std::chrono::high_resolution_clock::now();

#ifdef DEBUG
  time_t curr_t = std::time(nullptr);
  string dbg_fname = "cgraph" + to_string(curr_t) + ".dbg"; // never in cached_io
  open_dbg(dbg_fname);
#endif


  //string stage_name = "cgraph";
  //SOUT("\n", HASH_BAR, "\n");
  //SOUT("# Starting stage ", stage_name, " ");
  //for (int i = 1; i < argc; i++) SOUT(argv[i], " ");

  //SOUT("at ", get_current_time(), "\n");
  //SOUT(HASH_BAR, "\n\n");
  SOUT("Version: UPCXX-", HIPMER_VERSION, " ", HIPMER_BUILD_DATE, "\n");
  SOUT("Running on ", rank_n(), " processes\n");
#ifdef DBG_ON
  SOUT(KRED, "DEBUG mode - expect low performance\n", KNORM);
#endif
#ifdef MPI_WRITE
  MPI_Init(&argc, &argv);
#endif
  {
    BarrierTimer timer(__func__);
    Options options;
    options.load(argc, argv);
    shared_ptr<CtgGraph> graph = make_shared<CtgGraph>(options.max_aggr_mb);
    build_graph(make_shared<Options>(options), graph);
    barrier();
    double av_clen;
    graph->print_stats(&av_clen);
    barrier();
    walk_graph(make_shared<Options>(options), graph);
    // implicit barrier() when BarrierTimer is destroyed //
  }

#ifdef MPI_WRITE
  barrier();
  MPI_Finalize();
  //SOUT("Called MPI_Finalize");
  barrier();
#endif

#ifdef DEBUG
  close_dbg();
#endif
  
  Timings::print_barrier_timings("Finished cgraph");

  return 0;
}

