#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "kseq.h"

static const char *USAGE = "interleave_fastq input_1.fq[.gz] input_2.fq[.gz] output.fq\n\n";

int main(int argc, char **argv)
{
    if (argc != 4) {
        fprintf(stderr, "%s", USAGE); exit(0);
    }
    gzFile in1 = gzopen(argv[1], "r");
    gzFile in2 = gzopen(argv[2], "r");
    FILE *out = fopen(argv[3], "w");
    if (in1 == NULL || in2 == NULL || out == NULL) {
        fprintf(stderr, "ERROR: Could not open one or more of the files input1: %s input2: %s and/or output: %s\n%s", argv[1], argv[2], argv[3], USAGE);
        exit(1);
    }
    kseq_t *ks1 = kseq_init(in1);
    kseq_t *ks2 = kseq_init(in2);
    if (ks1 == NULL || ks2 == NULL) {
        fprintf(stderr, "Could not create kseq objects for intput files!\n"); exit(1);
    }
    while (kseq_read(ks1) >= 0 && kseq_read(ks2) >= 0) {
        const char *areDiff = ks1->name.l == ks2->name.l ? NULL : "Different lengths";
        int l = ks1->name.l;
        if (!areDiff) {
            if (strncmp(ks1->name.s, ks2->name.s, l) == 0 && (ks1->comment.l == 0 || ks2->comment.l == 0 || ks1->comment.s[0] != '1' || ks2->comment.s[0] != '2')) {
                areDiff = "comments are not Illumina 1.8";
            } else if (l > 1 && (strncmp(ks1->name.s, ks2->name.s, l - 1) != 0 || ks1->name.s[l - 1] != '1' || ks2->name.s[l - 1] != '2')) {
                areDiff = "Paired reads are wrong";
            }
        }
        if (areDiff) {
            fprintf(stderr, "ERROR these files are not properly interleaved (%s): '%s %s' vs '%s %s'\n", areDiff, ks1->name.s, ks1->comment.s, ks2->name.s, ks2->comment.s);
            exit(1);
        }
        if (ks1->comment.l && ks2->comment.l) {
            fprintf(out, "@%s %s\n%s\n+\n%s\n@%s %s\n%s\n+\n%s\n", ks1->name.s, ks1->comment.s, ks1->seq.s, ks1->qual.s, ks2->name.s, ks2->comment.s, ks2->seq.s, ks2->qual.s);
        } else {
            fprintf(out, "@%s\n%s\n+\n%s\n@%s\n%s\n+\n%s\n", ks1->name.s, ks1->seq.s, ks1->qual.s, ks2->name.s, ks2->seq.s, ks2->qual.s);
        }
    }
    if (fclose(out) != 0) {
        fprintf(stderr, "ERROR: closing output file %s! %s\n", argv[3], strerror(errno)); exit(1);
    }
    kseq_destroy(ks1);
    kseq_destroy(ks2);
    gzclose(in1);
    gzclose(in2);
    return 0;
}
