/*
 * Note: this fastq reader code is not specific to upc. It is also used by kMerCount with mpi.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <time.h>

#include "fq_reader.h"

#ifndef DEFAULT_READ_LEN
#define DEFAULT_READ_LEN 1024
#endif

static int64_t num_ambig = 0;
static int64_t num_chars_read = 0;

#define PARTIAL_STR(str, maxlen) (strlen(str) > maxlen ? (int)maxlen : (int)((strchr(str, '\n') ? (strchr(str, '\n') - str)  : strlen(str))))

#ifdef __MACH__
#include <mach/mach_time.h>

static double InitConversionFactor()
{
    mach_timebase_info_data_t timebase;

    mach_timebase_info(&timebase);
    double conversion_factor = (double)timebase.numer / (double)timebase.denom;
    return conversion_factor;
}

static double now()
{
    static double conversion_factor = 0.0;

    if (conversion_factor == 0.0) {
        conversion_factor = InitConversionFactor();
    }
    return mach_absolute_time() * conversion_factor;
}

#else

static double now()
{
    struct timespec ts;

    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + ts.tv_nsec / 1000000000.0;
}

#endif


fq_reader_t create_fq_reader(void)
{
    fq_reader_t fqr = (fq_reader_t)calloc_chk(1, sizeof(struct fq_reader));

    fqr->buf = initBuffer(DEFAULT_READ_LEN);
    assert(isValidBuffer(fqr->buf));
    return fqr;
}

void destroy_fq_reader(fq_reader_t fqr)
{
    if (fqr) {
        if (fqr->buf) {
            freeBuffer(fqr->buf);
        }
        fqr->buf = NULL;

        if (fqr->gz) {
            close_fq(fqr);
        }
        fqr->gz = NULL;
        if (fqr->f) {
            close_fq(fqr);
        }
        fqr->f = NULL;
        fqr->size = 0;
        free_chk(fqr);
    }
}


static void strip_trailing_spaces(char *s)
{
    char *end = s + strlen(s) - 1;

    while (end >= s && isspace(*end)) {
        end--;
    }
    *(end + 1) = '\0';
}

inline static char *get_fq_name(char *header)
{
    assert(header != NULL);
    assert(header[0] == '@');
    if (header[0] != '@') return NULL;
    int len = strlen(header);
//    if (len >= MAX_READ_NAME_LEN - 1) {
//       return NULL;
//    }
    strip_trailing_spaces(header);
    len = strlen(header);


    // convert if new illumina 2 format  or HudsonAlpha format
    if (header[len - 2] != '/') {
        if (header[len - 2] == 'R') {
            // HudsonAlpha format  (@pair-R1,  @pair-R2)
            char *end = &header[len - 3];
            // replace with @pair/1 or @pair/2 */
            char rNum = end[2];
            end[0] = '/';
            end[1] = rNum;
            end[2] = '\0';

            return header;
        } else {
            // Latest Illumina header format
            char *end = strchr(header, '\t');
            if (!end) {
                end = strchr(header, ' ');
                if (!end) {
                    // no comment, just return the name without modification
                    return header;
                }
            }
            // truncate name at comment
            assert(*end == ' ' || *end == '\t');
            end[0] = '\0';
            // check for @pair/1 @/pair2 vs @pair 1:Y:... @pair 2:Y:....
            int commentpos = end - header;
            if (commentpos > 3 && header[commentpos - 2] == '/' && (header[commentpos - 1] == '1' || header[commentpos - 1] == '2')) {
                // @pair/1 or @pair/2
                return header;
            } else if ((len < commentpos + 7) || end[2] != ':' || end[4] != ':' || end[6] != ':' || (end[1] != '1' && end[1] != '2')) {
                // unknown pairing format -- hopefully this is a single file or one of a paired file set
                return header;
            }
            // @pair 1:Y:.:.: or @pair 2:Y:.:.:
            // replace with @pair/1 or @pair/2
            end[0] = '/';
            end[2] = '\0';
        }
    }


    assert(header[0] == '@');
    return header;
}

// returns 0 when unknown 1 for known pairing
// if pairing is known, end is set to 1 or 2 for first and second pair respectively
// modifies name to be the base pair name will affect header too
int get_fq_name_dirn(char *header, char **name, int8_t *end)
{
    *name = get_fq_name(header);
    *end = 0;
    if (!*name) {
        return 0;
    }
    int len = strlen(*name);
    if (len < 3) {
        return 0;
    }
    char end_char = (*name)[len - 1];
    if ((*name)[len - 2] != '/' || (end_char != '1' && end_char != '2')) {
        return 0;
    }
    *end = (end_char == '1' ? 1 : 2);
    (*name)[len - 2] = '\0';
    return 1;
}

// returns 0 for unpaired (offset to '@'), or the offset to '1' or '2' for paired
// does not modify s, copies the unpaired name into subName if it is not NULL
int8_t get_pairidx_from_name_line(const char *s, char *subName)
{
    // Supports these types of paired formats:
    //
    // Illumina:
    // @name/1
    // @name/2
    //
    // Casava 1.8:
    // @HISEQ15:136:C6G2YANXX:4:1101:1074:2037 1:N:0:AGTTCC
    // @HISEQ15:136:C6G2YANXX:4:1101:1074:2037 2:N:0:AGTTCC
    //
    // Further addition:
    // We've seen data in the Casava format (the second one above) without the final sequence
    // fragment. So we support that also now.
    //
    // HudsonAlpha naming:
    // @D00688R_136_HLJLJBCXY_1_1102_18488_10863-R1
    // @D00688R_136_HLJLJBCXY_1_1102_18488_10863-R2


    DBG2("get_pairidx_from_name_line(%s, %p)\n", s, subName);
    assert(s[0] == '@');
    int pairIdx = 0, max = 2047;
    int endIdx = 1, spaceIdx = 0;
    if (subName) {
        strncpy(subName, s, 255);
    }
    while (s[endIdx] != '\n' && s[endIdx] != '\0') {
        if (endIdx >= max) {
            break;
        }
        if (spaceIdx == 0 && isspace(s[endIdx])) {
            spaceIdx = endIdx;
        }
        endIdx++;
    }
    DBG3("Found endIdx=%d '%c'\n", endIdx, s[endIdx]);
    // line too long to determine pairing!
    if (s[endIdx] != '\n' && s[endIdx] != '\0') {
        DBG("Line too long! %s\n", s);
        return 0;
    }
    // something is wrong - the line is too short
    if (endIdx <= 3) {
        DBG("Line too short! %s\n", s);
        return 0;
    }
    // Illumina with random comment
    if (spaceIdx > 3 && s[spaceIdx - 2] == '/' && (s[spaceIdx - 1] == '1' || s[spaceIdx - 1] == '2')) {
        if (subName) {
            subName[spaceIdx - 2] = '\0';
        }
        return spaceIdx - 1;
    }
    // Illumina without comment
    if (s[endIdx - 2] == '/' && (s[endIdx - 1] == '1' || s[endIdx - 1] == '2')) {
        if (subName) {
            subName[endIdx - 2] = '\0';
        }
        return endIdx - 1;
    }
    // Casava
    if (s[spaceIdx + 2] == ':' && s[spaceIdx + 4] == ':' && s[spaceIdx + 6] == ':' &&
        (s[spaceIdx + 1] == '1' || s[spaceIdx + 1] == '2') &&
        (s[spaceIdx + 3] == 'Y' || s[spaceIdx + 3] == 'N')) {
        if (subName) {
            subName[spaceIdx] = '\0';
        }
        return spaceIdx + 1;
    }
    // HudsonAlpha
    if (s[endIdx - 2] == 'R' && (s[endIdx - 1] == '1' || s[endIdx - 1] == '2')) {
        if (subName) {
            subName[endIdx - 3] = '\0';
        }
        return endIdx - 1;
    }
    return 0;
}

static int64_t get_fptr_for_next_record(fq_reader_t fqr, int64_t offset)
{
    if (fqr->bufferInputStream) {
        assert(!fqr->gz);
        assert(!fqr->f);
    } else {
        if (fqr->gz || !fqr->f) {
            DIE("Can not fseek in a compressed file! (%s)\n", fqr->name);
        }
        if (offset == 0) {
            return 0;             // first record is the first record, include it.  Every other partition will be at least 1 full record after offset.
        }
    }
    char lastName[256];
    lastName[0] = '\0';
    if (offset >= fqr->size) {
        return fqr->size;
    }
    int ret;
    if (fqr->f) {
        ret = fseek(fqr->f, offset, SEEK_SET);
    } else {
        fqr->bufferInputStream->pos = offset;
        ret = 0;
    }
    if (ret != 0) {
        DIE("fseek could not execute on %s to %lld: %s", fqr->name, (lld)offset, strerror(errno));
        return -1;
    }

    LOGF("Finding fptr for next record of %s after offset=%lld\n", fqr->name, (lld)offset);

    resetBuffer(fqr->buf); // start with an empty buffer
    // skip first (likely parial) line after this offset to ensure we start at the beginning of a line
    if (fqr->f) {
        if (!fgetsBuffer(fqr->buf, 2047, fqr->f)) {
            return ftell(fqr->f);
        }
    } else {
        if (!getsBufferIntoBuffer(fqr->bufferInputStream, fqr->buf)) {
            return -1;
        }
    }
    DBG("Read %d chars to first eol: %.*s\n", (int)getLengthBuffer(fqr->buf), PARTIAL_STR(getStartBuffer(fqr->buf), 50), getStartBuffer(fqr->buf));
    int64_t new_offset = offset + getLengthBuffer(fqr->buf);
    int count = 0, last_pair_line = 0;
    char prev_pair = 0;
    int64_t prev_offset;
    if (fqr->f) {
        prev_offset = ftell(fqr->f);
    } else {
        prev_offset = fqr->bufferInputStream->pos;
    }

    Buffer readLines = initBuffer(4096);
    printfBuffer(readLines, "%s", getStartBuffer(fqr->buf));
    int possibleHeaderLine = -1;
    char prevName[256]; prevName[0] = '\0';
    while (1) {
        if (fqr->f) {
            new_offset = ftell(fqr->f);
        } else {
            new_offset = fqr->bufferInputStream->pos;
        }
        resetBuffer(fqr->buf);
        if (fqr->f) {
            if (!fgetsBuffer(fqr->buf, 2047, fqr->f)) {
                break;
            }
        } else {
            if (!getsBufferIntoBuffer(fqr->bufferInputStream, fqr->buf)) {
                break;
            }
        }
        count++;
        char *buf = getStartBuffer(fqr->buf);
        printfBuffer(readLines, "%s", buf);
        DBG("Read line of %lld length at offset %lld (prev=%lld) count %d: %s", (lld)getLengthBuffer(fqr->buf), (lld)new_offset, (lld)prev_offset, count, buf);
        if (buf[0] == '@') {
            // '@' immediately after newline yields two possiblities: This is a header line or a quality line (or the file is invalid).  Use logic to determine
            // if it is a quality line, the next line must be a header
            // if it is a header line the 4 lines from now will be a header too (3 lines may also be a quality line with starting '@' too)
            // once the first header is determined, look for interleaved pairs, but allow unpaired reads
            if (possibleHeaderLine > 0 && count == possibleHeaderLine + 1) {
                // possibleHeaderLine was actually a quality line.  so this MUST be a header line
                possibleHeaderLine = count; // the next header line we expect will be 4 after (and should never come back here)
                LOGF("header line after quality line at new_offset=%lld count=%d: %s", (lld)new_offset, count, buf);
                prev_pair = 0;              // reset any bogus pair
                prevName[0] = '\0';         // reset the previous name
            } else if (possibleHeaderLine > 0 && count == possibleHeaderLine + 4) {
                // the first possibleHeaderLine was actualy a header.  good from here. this MUST be a header line
                possibleHeaderLine = count; // the next header line we expect will be 4 after (and will hit here again)
                LOGF("header line after 4 lines after first header at new_offset=%lld count=%d: %s", (lld)new_offset, count, buf);
            } else if (possibleHeaderLine > 0 && count == possibleHeaderLine + 3) {
                // this must be a quality line after a real header line
                LOGF("Found quality line after 3 lines after the first header new_offset=%lld count=%d: %s", (lld)new_offset, count, buf);
                continue; // not interested in this
            } else {
                // first '@' line this could be either header or quality lets skip it
                if (possibleHeaderLine > 0) {
                    DIE("This should not happen!  possibleHeaderLine=%d at %lld.  Read from %lld offset: %.*s ...\n", possibleHeaderLine, (lld)new_offset, (lld)offset, 60, getStartBuffer(readLines));
                }
                possibleHeaderLine = count;
                LOGF("possibleHeaderLine may be quality line at new_offset=%lld count=%d: %.*s\n", (lld)new_offset, count, PARTIAL_STR(buf, 50), buf);
                // Try to use this line anyway, even if it happens to be quality
            }
            // now look to pairs (or not)
            int pairIdx = get_pairidx_from_name_line(buf, lastName);
            char pair = 'X'; // 'X' for unpaired, '\0' for not assigned yet, '1', '2' for read 1, read 2 respectively
            if (pairIdx) {
                pair = buf[pairIdx];
            }
            LOGF("Discovered pair: '%c' lastName=%.*s prevName=%.*s\n", pair, PARTIAL_STR(lastName, 50), lastName, PARTIAL_STR(prevName, 50), prevName);
            if (!prev_pair) {
                prev_pair = pair;
                prev_offset = new_offset;
                last_pair_line = count;
                DBG("no prev_pair at count %d\n", count);
            } else {
                if (last_pair_line + 1 == count) {
                    // last pair was actually a quality line
                    prev_pair = pair;
                    prev_offset = new_offset;
                    last_pair_line = count;
                    LOGF("Last line was actually a quality line using next at new_offset=%lld: '%.*s'\n", (lld)new_offset, PARTIAL_STR(buf, 50), buf);
                    DIE("This should no longer happen! %s\n", getStartBuffer(readLines));
                } else if (last_pair_line + 4 == count) {
                    // not interleaved or interleaved and prev pair was 1 and this is 2
                    if (prev_pair == '1' && pair == '2' && strcmp(prevName, lastName) == 0) {
                        new_offset = prev_offset;
                        LOGF("Interleaved this record completes the pair, using prev_offset %lld at count %d, lastName: %.*s prevName: %.*s\n", (lld)prev_offset, count, PARTIAL_STR(lastName, 50), lastName, PARTIAL_STR(prevName, 50), prevName);
                    } else if (prev_pair == '2' && pair == '1' && strcmp(prevName, lastName) != 0) {
                        LOGF("Interleaved this record starts a new pair, using new_offset %lld at count %d, lastName: %.*s prevName: %.*s\n", (lld)new_offset, count, PARTIAL_STR(lastName, 50), lastName, PARTIAL_STR(prevName, 50), prevName);
                    } else {
                        new_offset = prev_offset;
                        LOGF("Not interleaved at count %d, using prev_offset %lld, lastName: %.*s prevName: %.*s\n", count, (lld)prev_offset, PARTIAL_STR(lastName, 30), lastName, PARTIAL_STR(prevName, 50), prevName);
                    }
                    break;
                }
            }
            strncpy(prevName, lastName, 255);
        }
        if (count > 13) {
            DIE("Could not find a valid line in the fastq file %s, last line: %s\n", fqr->name, buf);
        }
    }
    assert(new_offset >= offset);
    LOGF("Found record at %lld (count=%d %lld bytes after requested offset=%lld):\n%s", (lld)new_offset, count, (lld)(new_offset - offset), (lld)offset, getStartBuffer(readLines) + (new_offset - offset));
    DBG("...Skipped: %.*s\n", (int)(new_offset - offset), getStartBuffer(readLines));
    freeBuffer(readLines);
    // for interleawed, make sure to the next record starts with a /1
    DBG("Chose %lld as my offset '%s' (prev_offset: %lld, startOffset: %lld)\n", (lld)new_offset, lastName, (lld)prev_offset, (lld)offset);
    return new_offset;
}

void open_fq(fq_reader_t fqr, const char *fname, int cached_io, const char *base_dir, int64_t fileSize)
{
    fqr->line = 0;
    fqr->max_read_len = 0;
    if (cached_io) {
        // in this case, we have one file per thread
        assert(base_dir);
        char bname[MAX_FILE_PATH];
        sprintf(fqr->name, "%s/%s%s", base_dir, get_basename(bname, fname), (strstr(fname, GZIP_EXT) != NULL ? "" : GZIP_EXT));
        get_rank_path(fqr->name, MYTHREAD);
        fqr->f = NULL;
        LOGF("Opening cached_io fqfile: %s\n", fqr->name);
        fqr->gz = gzopen_chk(fqr->name, "r");

        // get the size from a secondary file
        char fname2[MAX_FILE_PATH + 30];
        snprintf(fname2, MAX_FILE_PATH+30, "%s.uncompressedSize", fqr->name);
        FILE *f = fopen_chk(fname2, "r");
        int64_t s;
        fqr->size = 0;
        while (fread(&s, sizeof(int64_t), 1, f) == 1) {
            fqr->size += s;
        }
        if (!feof(f)) {
            DIE("Invalid uncompressedSize file: %s %s\n", fname2, strerror(errno));
        }
        fclose_track(f);
        fqr->start_read = 0;
        fqr->end_read = fqr->size;
        fqr->fpos = fqr->start_read;
        LOGF("Opened %s gzipped fastq (size=%lld) start_read=%lld end_read=%lld fpos=%lld size=%lld (uncompressed)\n", fqr->name, (lld)get_file_size(fqr->name), (lld)fqr->start_read, (lld)fqr->end_read, (lld)fqr->fpos, (lld)fqr->size);
        return;
    } else {
        // in this case, we have a single file for all threads to read a part
        strcpy(fqr->name, fname);
        if (fileSize < 0) {
            fqr->size = get_file_size(fqr->name);
        } else {
            fqr->size = fileSize;
        }
    }

    fqr->f = fopen_chk(fqr->name, "r");
    if (cached_io) {
        /* The entire file is read by this thread */
        fqr->start_read = 0;
        fqr->end_read = fqr->size;
    } else {
        /* just a part of the file is read by this thread */
        int64_t read_block = INT_CEIL(fqr->size, THREADS);
        fqr->start_read = read_block * MYTHREAD;
        fqr->end_read = read_block * (MYTHREAD + 1);
        if (MYTHREAD > 0) {
            fqr->start_read = get_fptr_for_next_record(fqr, fqr->start_read);
        }
        if (MYTHREAD == THREADS - 1) {
            fqr->end_read = fqr->size;
        } else {
            fqr->end_read = get_fptr_for_next_record(fqr, fqr->end_read);
        }
    }

    assert(fqr->end_read >= fqr->start_read);
    if (setvbuf(fqr->f, NULL, _IOFBF, FQ_READER_BUFFER_SIZE) != 0) {
        WARN("Could not setvbuf on %s to %d! %s", fqr->name, FQ_READER_BUFFER_SIZE, strerror(errno));
    }
    if (fseek(fqr->f, fqr->start_read, SEEK_SET) != 0) {
        long long int start_read = fqr->start_read;
        DIE("reset_my_partition could not fseek on %s to %lld: %s\n", fqr->name, start_read, strerror(errno));
    }
    fqr->fpos = fqr->start_read;
    assert(fqr->fpos == ftell(fqr->f));
#ifndef __APPLE__
    if (fqr->f) {
        posix_fadvise(fileno(fqr->f), fqr->start_read, fqr->end_read - fqr->start_read,
                      POSIX_FADV_SEQUENTIAL);
    }
#endif
    LOGF("Opened %s start_read=%lld end_read=%lld fpos=%lld size=%lld\n", fqr->name, (lld)fqr->start_read, (lld)fqr->end_read, (lld)fqr->fpos, (lld)fqr->size);
    serial_printf("Reading FASTQ file %s %s\n", fqr->name, cached_io ? " (cached)" : "");
}

// open a buffer instead of a file and return the offset to the first record
int64_t open_Buffer(fq_reader_t fqr, Buffer bufferInputStream)
{
    assert(fqr->f == NULL);
    fqr->bufferInputStream = bufferInputStream;
    fqr->size = getLengthBuffer(fqr->bufferInputStream);
    fqr->start_read = 0;
    fqr->end_read = fqr->size;
    if (MYTHREAD > 0) {
        fqr->start_read = get_fptr_for_next_record(fqr, fqr->start_read);
    }
    fqr->fpos = fqr->start_read;
    fqr->bufferInputStream->pos = fqr->fpos;
    LOGF("Found record at offset fpos=%lld %.20s\n", fqr->fpos, getCurBuffer(fqr->bufferInputStream));
    return fqr->fpos;
}

// read just a portion of the file (from fqr->start to fqr->end) and store to the local_tmp_dir (/dev/shm)
// this function assums fqr is already at the start position
int load_fq(fq_reader_t fqr, char *fname, const char *base_dir, int64_t fileSize)
{
    open_fq(fqr, fname, 0, NULL, fileSize); // never cached_io so the file is read in partitions
    assert(fqr->f);
    int64_t my_fsize = fqr->end_read - fqr->start_read;
    DBG("load_fq storing %lld bytes from %s (%lld - %lld)\n", (lld)my_fsize, fqr->name, (lld)fqr->start_read, (lld)fqr->end_read);
    int err = 0;
    // create shm file
    char my_fname[MAX_FILE_PATH*2+20];
    char bname[MAX_FILE_PATH];
    sprintf(my_fname, "%s/%s", base_dir, get_basename(bname, fname));
    get_rank_path(my_fname, MYTHREAD);
    if (does_file_exist(my_fname)) {
        serial_printf("Skipping already loaded file %s - %s\n", fname, my_fname);
        return 0;
    }
    strcat(my_fname, GZIP_EXT);
    long long int blockSize = 16 * 1024 * 1024;
    char *buf = (char *)malloc_chk(blockSize);
    LOGF("Writing cached (gzip) partition of %lld uncompressed bytes: %s\n", my_fsize, my_fname);
    gzFile f = gzopen_chk(my_fname, "w1");
    int64_t bytes_remaining = my_fsize;
    double t1, t2;
    double readTime = 0.0, writeTime = 0.0;
    t1 = now();
    do {
        int64_t readBlock = bytes_remaining < blockSize ? bytes_remaining : blockSize;
        int64_t bytesRead = fread(buf, 1, readBlock, fqr->f);
        if (bytesRead == 0 && readBlock > 0 && ferror(fqr->f)) {
            DIE("And error occured while reading %s! ferror=%d\n", my_fname, ferror(fqr->f));
        }
        t2 = now();
        readTime += t2 - t1;
        DBG("load_fq read %lld bytes from %s\n", (lld)bytesRead, fqr->name);
        int64_t bytesWritten = 0;
        while (bytesWritten < bytesRead) {
            int64_t thisWrite = gzwrite(f, buf + bytesWritten, bytesRead - bytesWritten);
            DBG("load_fq wrote %lld uncompressed bytes to %s\n", (lld)thisWrite, my_fname);
            if (thisWrite <= 0) {
                int errnum=0;
                const char *errmsg = gzerror(f, &errnum);
                DIE("Could not write %lld bytes to %s! %s - %d (possibly %s)\n", (lld)bytesRead - bytesWritten, my_fname, errmsg, errnum, strerror(errno));
            }
            bytesWritten += thisWrite;
        }
        bytes_remaining -= bytesRead;
        t1 = now();
        writeTime += t1 - t2;
    } while (bytes_remaining > 0);
    gzclose(f);
    t2 = now();
    writeTime += t2 - t1;
    free_chk(buf);
    int64_t compressed_size = get_file_size(my_fname);

    // store the uncompressed size in a secondary file
    strcat(my_fname, ".uncompressedSize");
    FILE *fsizefd = fopen_chk(my_fname, "w");
    fwrite_chk(&my_fsize, sizeof(int64_t), 1, fsizefd);
    fclose_track(fsizefd);
    DBG("Wrote uncompressed size bytes to %s\n", my_fname);
    LOGF("Compressed %lld bytes to %lld (to %0.3f %%) in %0.3f s read / %0.3f s write\n", (lld)my_fsize, (lld)compressed_size, 100.0 * compressed_size / my_fsize, readTime, writeTime);
    fqr->readTime = readTime;
    fqr->writeTime = writeTime;

    close_fq(fqr);
    return -err;
}

int unload_fq(char *fname, const char *base_dir)
{
    char my_fname[MAX_FILE_PATH*2+20];
    char bname[MAX_FILE_PATH];

    sprintf(my_fname, "%s/%s" GZIP_EXT, base_dir, get_rank_path(get_basename(bname, fname), MYTHREAD));
    if (unlink(my_fname) == -1) {
        return -errno;
    }
    return 0;
}

// include newline char
static char *get_next_line(fq_reader_t fqr)
{
    char *buf = NULL;

    if (fqr->fpos > fqr->end_read) {
        return NULL;
    }
    size_t len = 0, maxLen = (fqr->end_read - fqr->fpos);
    assert(isValidBuffer(fqr->buf));
    resetBuffer(fqr->buf);
    assert(getStartBuffer(fqr->buf) == getEndBuffer(fqr->buf));
    if (fqr->fpos >= fqr->end_read) {
        return NULL;
    }
    if (fqr->gz) {
        assert(!fqr->f);
        buf = gzgetsBuffer(fqr->buf, (maxLen > 2048 ? 2048 : maxLen), fqr->gz);
    } else
    buf = fgetsBuffer(fqr->buf, (maxLen > 2048 ? 2048 : maxLen), fqr->f);
    if (!buf) {
        return NULL;
    }

    len = getLengthBuffer(fqr->buf);
    fqr->fpos += len;
    if (fqr->gz) {
        assert(fqr->fpos == gztell(fqr->gz));
    } else
    assert(fqr->fpos == ftell(fqr->f));
    assert(buf != NULL);
    assert(buf == getStartBuffer(fqr->buf));
    if (len == 0) {
        return NULL;
    }
    assert(buf[len] == '\0');
    if (buf[len - 1] != '\n') {
        WARN("should be end of line, not '%c' %d: '%s' at line %lld\n",
             buf[len], buf[len], buf, fqr->line);
    }
    fqr->line++;
    assert(len > 0);
    assert(len == getLengthBuffer(fqr->buf));
    return buf;
}

void hexifyId(char *name, uint8_t libnum, uint64_t *id1, uint64_t *id2, uint64_t step)
{
    if (name[0] != '@') {
        DIE("Could not hexify fastq read header '%s' (%lld, %lld step %lld)\n", name, (lld) * id1, (lld) * id2, (lld)step);
    }
    int len = strlen(name);
    DBG2("hexify: Read read len=%d %.*s\n", len, len, name);
    int pairlen = len;
    int pair = 0;
    uint64_t *id = id1;
    // first allow id1 & id2 to designate the pair
    if (id1 == NULL) {
        id = id2;
        pair = 2;
    } else if (id2 == NULL) {
        pair = 1;
    } else if (len > 3 && name[len - 2] == '/') {
        // autodetect the pair from the name
        pairlen -= 2;
        if (name[len - 1] == '1') {
            pair = 1;
        } else if (name[len - 1] == '2') {
            pair = 2;
            id = id2;
        }
    }
    char newname[32];
    sprintf(newname, "%02x%llx%s", libnum, (unsigned long long int)*id, (pair ? (pair == 1 ? "/1" : "/2") : ""));
    if (strlen(newname) >= MAX_READ_NAME_LEN) {
        DIE("Please increase MAX_READ_NAME_LEN as %s it too large now (from %s)\n", newname, name);
    }
    DBG2("Hexify len=%d strlen=%d pair=%d '%s' -> '@%s'\n", len, strlen(name), pair, name, newname);
    strcpy(name + 1, newname);
    *id += step;
}

int get_next_fq_record(fq_reader_t fqr, Buffer id, Buffer nts, Buffer quals)
{
    resetBuffer(id);
    resetBuffer(nts);
    resetBuffer(quals);
    int i;
    if (fqr->fpos >= fqr->end_read) {
        DBG("at end: fpos=%lld end_read=%lld\n", (lld)fqr->fpos, (lld)fqr->end_read);
        return 0;
    }
    Buffer lastBuf = fqr->buf;
    if (getSizeBuffer(id) <= MAX_READ_NAME_LEN) {
        growBuffer(id, MAX_READ_NAME_LEN + 3);
    }

    // get all four lines, one for each field
    for (i = 0; i < 4; i++) {
        char *tmp = NULL;
        switch (i) {
        case 0: fqr->buf = lastBuf; break;
        case 1: fqr->buf = nts;     break;
        case 2: fqr->buf = lastBuf; break;
        case 3: fqr->buf = quals;   break;
        default: assert(0);
        }
        ;
        tmp = get_next_line(fqr);
        if (!tmp) {
            // return the proper buffer back
            DBG("no next_line(%d): fpos=%lld end_read=%lld lastBuf=%s\n", i, (lld)fqr->fpos, (lld)fqr->end_read, getStartBuffer(lastBuf));
            fqr->buf = lastBuf;
            return 0;
        }

        char *buf = getStartBuffer(fqr->buf);
        if (getLengthBuffer(fqr->buf) == 0 || buf[0] == '\n') {
            // empty
            if (i != 0) {
                DIE("Invalid FASTQ at line %lld, expected empty or just newline at the end: '%s'\n", (lld)fqr->line, buf);
            }
            // return the proper buffer back
            fqr->buf = lastBuf;
            DBG("nothing in buffer(%d): fpos=%lld end_read=%lld lastBuf=%s\n", i, (lld)fqr->fpos, (lld)fqr->end_read, getStartBuffer(lastBuf));
            return 0;
        }

        if (i == 0) {
            // header line
            if (buf[0] != '@') {
                DIE("Invalid FASTQ at line %lld, expected read name (@):\n%s\n", (lld)fqr->line, buf);
            }
            // construct universally formatted name (illumina 1 format)
            strip_trailing_spaces(buf);
            char *read_name = get_fq_name(buf);
            if (!read_name) {
                DIE("Invalid FASTQ name format: %s\n", buf);
            }
            strcpyBuffer(id, read_name);
        } else if (i == 1) {
            // sequence
            chompBuffer(fqr->buf);
#ifdef CONFIG_CHECK_SEQS
            char label[256];
            sprintf(label, "  in %s:%d at line %lld\n", __FILE__, __LINE__, (lld)fqr->line);
            assert(strlen(buf) == getLengthBuffer(fqr->buf));
            int len = check_seqs(buf, label, &num_ambig);
            num_chars_read += len;
#endif
            assert(fqr->buf == nts);
        } else if (i == 2) {
            if (buf[0] != '+') {
                DIE("Invalid FASTQ at line %lld, expected '+':\n%s\n", (lld)fqr->line, buf);
            }
        } else if (i == 3) {
            chompBuffer(fqr->buf);
            assert(fqr->buf == quals);
        } else {
            DIE("This should never happen -- i (%d) >=4?", i);
        }
    }
    if (getLengthBuffer(nts) != getLengthBuffer(quals)) {
        DIE("Sequence and quals differ in length at line %lld: %llu != %llu\n%s\n%s\n",
            (lld)fqr->line, (llu)getLengthBuffer(nts), (llu)getLengthBuffer(quals),
            getStartBuffer(nts), getStartBuffer(quals));
    }
    int read_len = getLengthBuffer(nts);
    if (read_len > fqr->max_read_len) {
        fqr->max_read_len = read_len;
    }
    // reset to the original Buffer
    fqr->buf = lastBuf;
    assert(getStartBuffer(id)[0] == '@');
    return 1;
}

void close_fq(fq_reader_t fqr)
{
    DBG("close_fq(%s)\n", fqr->name);
    if (fqr->gz) {
        gzclose_track(fqr->gz);
        fqr->name[0] = '\0';
        fqr->gz = NULL;
    }
    if (fqr->f) {
        if (fclose_track(fqr->f) != 0) {
            WARN("Could not fclose_track fastq %s! %s\n", fqr->name, strerror(errno));
        }
        fqr->name[0] = '\0';
        fqr->f = NULL;
        double frac_ambig = (double)num_ambig / num_chars_read;
        if (frac_ambig > 0.01) {
            serial_printf("Thread %d found %lld ambiguous characters (out of %lld)\n",
                          MYTHREAD, (lld) num_ambig, (lld) num_chars_read);
        }
    }
}


int64_t estimate_fq(char *fname, int cached_io, const char *base_dir, int sampledReads, int64_t *estimatedReads, int64_t *estimatedBases)
{
    fq_reader_t fqr = create_fq_reader();

    open_fq(fqr, fname, cached_io, base_dir, -1);
    int max = sampledReads;
    int i;
    int64_t bases = 0;
    int64_t startPos = fqr->fpos;
    for (i = 0; i < max; i++) {
        if (fqr->fpos >= fqr->end_read) {
            break;
        }
        if (!get_next_line(fqr)) {
            break;
        }
        char *buf = getStartBuffer(fqr->buf);
        if (buf[0] != '@') {
            long long fpos = fqr->fpos;
            DIE("Improper fastq file (%s:%lld) - First line does not start with '@': %.*s ...\n", fname, fpos, 60, buf);
        }
        if (!get_next_line(fqr)) {
            buf = getStartBuffer(fqr->buf);
            long long fpos = fqr->fpos;
            DIE("Improper fastq file (%s:%lld) - no second line after %.*s ...\n", fname, fpos, 60, buf);
        }
        int seqLen = getLengthBuffer(fqr->buf);
        bases += seqLen - 1;
        if (!get_next_line(fqr)) {
            buf = getStartBuffer(fqr->buf);
            long long fpos = fqr->fpos;
            DIE("Improper fastq file (%s:%lld) - no third line after: %.*s ...\n", fname, fpos, 60, buf);
        }
        buf = getStartBuffer(fqr->buf);
        if (buf[0] != '+') {
            long long fpos = fqr->fpos;
            DIE("Improper fastq file (%s:%lld) - third line does not start with '+': %.*s ...\n", fname, fpos, 60, buf);
        }
        if (!get_next_line(fqr)) {
            buf = getStartBuffer(fqr->buf);
            long long fpos = fqr->fpos;
            DIE("Improper fastq file (%s:%lld) - no fourth line after: %.*s ...\n", fname, fpos, 60, buf);
        }
        if (seqLen != getLengthBuffer(fqr->buf)) {
            buf = getStartBuffer(fqr->buf);
            long long fpos = fqr->fpos;
            int qualLen = getLengthBuffer(fqr->buf);
            DIE("Improper fastq file (%s:%lld) - seq and qual lengths mismatch: %d vs %d: %.*s ...\n",
                fname, fpos, seqLen, qualLen, 60, buf);
        }
    }
    int64_t fileSize = fqr->size;
    int64_t bytesRead = fqr->fpos - startPos;
    if (bytesRead > 0) {
        *estimatedReads = (fileSize * i + bytesRead - 1) / bytesRead;
        *estimatedBases = (fileSize * bases + bytesRead - 1) / bytesRead;
    } else {
        WARN("No reads in %s\n", fname);
        *estimatedReads = 0;
        *estimatedBases = 0;
    }
    destroy_fq_reader(fqr);
    return fileSize;
}
