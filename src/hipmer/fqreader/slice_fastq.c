#define _POSIX_C_SOURCE  200112L

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "poc_common.h"
#include "common_base.h"
#include "common.h"
#include "memory_chk.h"
#include "Buffer.h"


#include "fq_reader.h"
#include "fq_reader.c"

static const char *USAGE = "slice_fastq <rank> <num_ranks> *.fastq";

int main(int argc, char **argv)
{
    if (argc < 4) {
        fprintf(stderr, "USAGE: %s\n", USAGE);
        exit(1);
    }
    int rank = atoi(argv[1]);
    int size = atoi(argv[2]);
    set_rank_and_size(rank, size);

    uint64_t id1 = MYTHREAD, id2 = MYTHREAD;
    int i;
    for (i = 3; i < argc; i++) {
        fq_reader_t fqr = create_fq_reader();
        open_fq(fqr, argv[i], 0, ".", -1);

        Buffer id = initBuffer(128), nts = initBuffer(512), quals = initBuffer(512);

        while (get_next_fq_record(fqr, id, nts, quals)) {
            printf("%s\t", getStartBuffer(id));
            hexifyId(getStartBuffer(id), 0, &id1, &id2, THREADS);
            printf("%s\n", getStartBuffer(id));
        }
        freeBuffer(id);
        freeBuffer(nts);
        freeBuffer(quals);

        close_fq(fqr);
        destroy_fq_reader(fqr);
    }

    return 0;
}
