#ifndef SHARED_BUFFER_H_
#define SHARED_BUFFER_H_

#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "log.h"
#include "Buffer.h"

typedef struct __SharedBuffer {
    SharedCharPtr str;
    int32_t len, size;
} SharedBuffer;
typedef shared [] SharedBuffer *SharedBufferPtr;
typedef shared [1] SharedBuffer *AllSharedBufferPtr;

// copy a remote SharedBufferPtr to a local Buffer
void getSharedBuffer(Buffer local_dst, const SharedBufferPtr remote_src);
void getAllSharedBuffer(Buffer local_dst, const AllSharedBufferPtr remote_src);

void getSharedBuffer2(Buffer local_dst, const SharedBuffer local_copy_of_remote_source);

// upc_alloc a new SharedBuffer out of an existing Buffer
SharedBuffer newSharedBuffer(const Buffer local_source);
SharedBuffer allocSharedBuffer(int32_t capacity);

// upc_alloc or realloc a SharedBuffer from an exiting Buffer
#define putSharedBuffer(remote_dest, local_source) do { \
    if (!isValidBuffer(local_source)) { \
        DIE("You must have initialized the Buffer here\n"); \
    } \
    SharedBuffer _local_sb = *(remote_dest), _local_sb2; \
    if (_local_sb.len != 0) DIE("Can not putSharedBuffer to an already used shared buffer! len=%d size=%d str=%d,%p\n", _local_sb.len, _local_sb.size, upc_threadof(_local_sb.str), upc_addrfield(_local_sb.str)); \
    _local_sb2 = _local_sb; \
    putSharedBuffer2(&_local_sb, local_source); \
    if (_local_sb.size != _local_sb2.size || _local_sb.str != _local_sb2.str) { \
        /* Two step, update size & str, then len */ \
        _local_sb2 = _local_sb; \
        _local_sb2.len = 0; \
        *(remote_dest) = _local_sb2; \
        upc_fence; \
    } \
    (remote_dest)->len = _local_sb.len; \
    upc_fence; \
} while (0)

void putSharedBuffer2(SharedBuffer *local_ptr_of_remote_dest, const Buffer local_source);

// upc_free the memory of a remote SharedBuffer -- does not deallocate the SharedBufferPtr itself
#define freeSharedBuffer( sb ) _freeSharedBuffer( (SharedBufferPtr) &(sb) )
void _freeSharedBuffer(SharedBufferPtr remote_ptr);

#endif // SHARED_BUFFER_H_
