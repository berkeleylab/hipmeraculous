#ifndef HASHTABLE_DDS_H
#define HASHTABLE_DDS_H

#include <upc.h>

#ifndef CONTIGS_DDS_TYPE
#error "You must specify CONTIGS_DDS_TYPE before including this file"
#endif

#include "upc_compatibility.h"
#include "common.h"

/* Hash-table entries data structure (i.e. kmers and extensions) */
#ifndef LIST_T
#  ifndef LIST_STRUCT_CONTENTS
#  error "You must specify either LIST_T or LIST_STRUCT_CONTENTS before including this file"
#  endif
#  define LIST_T JOIN(CONTIGS_DDS_TYPE, list_t)
typedef struct LIST_T LIST_T;
struct LIST_T {
    shared [] LIST_T *next;
    LIST_STRUCT_CONTENTS
};

#endif

/* Bucket data structure */
#define BUCKET_T JOIN(CONTIGS_DDS_TYPE, bucket_t)
typedef struct BUCKET_T BUCKET_T;
struct BUCKET_T {
    shared[] LIST_T * head;                          // Pointer to the first entry of the hashtable
    // optmization to save a remote access
    shared[] LIST_T *cachedEntryPtr;                  // The cachedEntry live pointer
    LIST_T cachedEntry;                               // The a copy of *cachedEntryPtr
};

/* Hash-table data structure */
#define HASH_TABLE_T JOIN(CONTIGS_DDS_TYPE, hash_table_t)
typedef struct HASH_TABLE_T HASH_TABLE_T;
struct HASH_TABLE_T {
    int64_t size;                                       // Size of the hashtable
    shared[BS] BUCKET_T * table;                        // Entries of the hashtable are pointers to buckets
};

#define SHARED_HEAP_PTR JOIN(CONTIGS_DDS_TYPE, shared_heap_ptr)
typedef shared[] LIST_T *SHARED_HEAP_PTR;

#define HEAP_STRUCT JOIN(CONTIGS_DDS_TYPE, heap_struct)
struct HEAP_STRUCT {
    SHARED_HEAP_PTR  ptr;
    UPC_INT64_T      len;
};
typedef struct HEAP_STRUCT HEAP_STRUCT;

/* Memory heap data structure */
#ifndef MEMORY_HEAP_EXTRA_CONTENTS
#define MEMORY_HEAP_EXTRA_CONTENTS
#endif

#define MEMORY_HEAP_T JOIN(CONTIGS_DDS_TYPE, memory_heap_t)
typedef struct MEMORY_HEAP_T MEMORY_HEAP_T;
struct MEMORY_HEAP_T {
    shared[BS] HEAP_STRUCT * heap_struct;                  // Pointers to shared memory heaps
    SHARED_HEAP_PTR        * cached_heap_ptrs;
    LIST_T                 * local_buffers;
    int64_t                * local_indicies;
    int64_t                  local_chunk_size;
    int64_t                  heap_size;
    MEMORY_HEAP_EXTRA_CONTENTS
};

#endif // HASHTABLE_DDS_H
