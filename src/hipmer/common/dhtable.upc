/*
 * Distributed hash table
 *
 * This form of hash table uses atomics and not locks. But it will only work if the gets and puts
 * are not interleaved. This is the case here, as all the puts are done when reading the meraligner
 * file, and all the gets are done when reading the fastq file.
 * There are expected to be duplicate keys, since there are multiple mappings of read names to gap
 * ids. An alternative would be to have each read_orient_t store an array of gap ids. However,
 * such data cannot be updated with atomics alone, so instead we just use duplicates.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <upc.h>
#include <upc_nb.h>

#include "utils.h"
#include "tracing.h"
#include "dhtable.h"
#include "htable.h"
#include "timers.h"
#include "common.h"

static double _load_factor = 1.0;
static int _max_depth = 0;
static long _nentries = 0;
static shared [1] UPC_ATOMIC_MIN_T * _entry_counts;
static shared [1] entry_t * _entries;


void dhtable_init(long tot_reads, double load_factor)
{
    _load_factor = load_factor;
    _nentries = tot_reads / load_factor + 3;
    _max_depth = 0;
    //tprintf_flush("dhtable_init(%ld, %f) for %ld entries\n", tot_reads, load_factor, _nentries);
    UPC_ALL_ALLOC_CHK(_entry_counts, _nentries, sizeof(UPC_ATOMIC_MIN_T));
    UPC_ALL_ALLOC_CHK(_entries, _nentries, sizeof(entry_t));
    if (_entry_counts == NULL || _entries == NULL) {
        DIE("Could not allocate %ld entries for dhtable!\n", _nentries);
    }
    for (int64_t i = MYTHREAD; i < _nentries; i += THREADS) {
        _entry_counts[i] = 0;
    }

    UPC_LOGGED_BARRIER;
}

void dhtable_free(void)
{
    UPC_ALL_FREE_CHK(_entries);
    UPC_ALL_FREE_CHK(_entry_counts);
    UPC_LOGGED_BARRIER;
}

// this will store duplicate entries
int dhtable_put(char *key, int64_t value)
{
    size_t h = htable_hash(key);
    long pos = -1;
    int depth = 0;

    for (long n = _nentries / 2, offset = 0; n > 1; offset += n, n >>= 1) {
        pos = h % n + offset;
        UPC_ATOMIC_MIN_T oldVal;
        UPC_ATOMIC_CSWAP_MIN_T(&oldVal, &_entry_counts[pos], 0, 1);
        if (!oldVal) {
            break;
        }
        pos = -1;
        depth++;
    }
    if (depth > _max_depth) {
        _max_depth = depth;
    }
    if (pos == -1) {
        return 0;
    }

    upc_memput(_entries[pos].key, key, strlen(key) + 1);
    _entries[pos].value = value;
    return 1;
}

// returns the number of values and fills the list with those values
int dhtable_get(char *key, int64_t *values, int max_values)
{
    size_t h = htable_hash(key);
    long pos = -1;
    char local_key[MAX_READ_NAME_LEN + 2] = "";
    int nvals = 0;

    //char buf[200];
    for (long n = _nentries / 2, depth = 0, offset = 0; n > 1; offset += n, n >>= 1, depth++) {
        pos = h % n + offset;
        if (!_entry_counts[pos]) {
            break;
        }
        upc_memget(local_key, _entries[pos].key, MAX_READ_NAME_LEN + 1);
        if (strcmp(local_key, key) == 0) {
            if (nvals == max_values) {
                DIE("Too many values > %d for dhtable, key %s\n", max_values, key);
            }
            values[nvals] = _entries[pos].value;
            nvals++;
        }
    }
    return nvals;
}

void dhtable_check(void)
{
    UPC_LOGGED_BARRIER;
    upc_tick_t t = upc_ticks_now();
    serial_printf("Checking read data hash...\n");

    long nelems = 0;
    long max_depth = 0;
    long tot_depth = 0;

    const int HISTDEPTH = 30;
    int *histogram = calloc_chk(HISTDEPTH, sizeof(int));
    //char name[100];
    for (int64_t i = MYTHREAD; i < _nentries; i += THREADS) {
        nelems += _entry_counts[i];
        if (_entry_counts[i]) {
            //upc_memget(name, _entries[i].name, 99);
            int depth = 0;
            for (long n = _nentries / 2, offset = 0; n > 1; offset += n, n >>= 1, depth++) {
                if (i < offset + n) {
                    break;
                }
            }
            if (depth >= HISTDEPTH) {
                depth = HISTDEPTH - 1;
            }
            histogram[depth]++;
            if (depth > max_depth) {
                max_depth = depth;
            }
            tot_depth += max_depth;
        }
    }
    UPC_LOGGED_BARRIER;

    long max_possible_depth = 0;
    if (!MYTHREAD) {
        for (long n = _nentries / 2, offset = 0; n > 1; offset += n, n >>= 1, max_possible_depth++) {
            ;
        }
    }

    long all_nelems = reduce_long(nelems, UPC_ADD, SINGLE_DEST);
    long all_max_depth = reduce_long(max_depth, UPC_MAX, SINGLE_DEST);
    long all_tot_depth = reduce_long(tot_depth, UPC_ADD, SINGLE_DEST);

    serial_printf("\t_entries hash table stats:\n"
                  "\t  capacity:        %ld\n"
                  "\t  elements:        %ld\n"
                  "\t  expected load:   %.3f\n"
                  "\t  load:            %.3f\n"
                  "\t  max poss depth:  %ld\n"
                  "\t  max depth:       %ld\n"
                  "\t  av depth:        %.3f\n",
                  _nentries, all_nelems, _load_factor,
                  (double)all_nelems / _nentries, max_possible_depth,
                  all_max_depth, (double)all_tot_depth / all_nelems);

    if (!MYTHREAD) {
        for (int i = 0; i < all_max_depth; i++) {
            serial_printf("\t  depth frac %d:    %.5f\n",
                          i, (double)histogram[i] / (_nentries / 2 / THREADS));
        }
    }
    serial_printf("\tmemory used (gb):  %.3f\n"
                  "\ttime for stats:    %.3f\n",
                  (double)get_max_mem_usage_mb() / 1024,
                  ELAPSED_TIME(t));
    free_chk(histogram);
    UPC_LOGGED_BARRIER;
}

long dhtable_depth(void)
{
    long max_possible_depth = 0;

    for (long n = _nentries / 2, offset = 0; n > 1; offset += n, n >>= 1, max_possible_depth++) {
        ;
    }
    return max_possible_depth;
}
