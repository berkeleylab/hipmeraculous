#ifndef COMMON_SIGNAL_H
#define COMMON_SIGNAL_H

#include <signal.h>

void common_noop_handler(int signal);
int set_common_signal_handler(int signal, void (*sa_handler)(int));
void unset_common_signal_handler(int signal);

#endif
