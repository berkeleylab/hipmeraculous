#ifndef _UPC_DISTRIBUTED_BUFFER_H_
#define _UPC_DISTRIBUTED_BUFFER_H_

#include <stdint.h>
#include <upc.h>
#include "upc_common.h"
#include "Buffer.h"

// offsets and sizes are in units of blockSize
typedef struct {
    SharedCharPtr buffer;
    int64_t       globalOffset;  // the prefix sum of localCounts     0 - (totalSize-1)
    int64_t       threadCount;   // mySize used at initialization
    int64_t       threadOffset;  // the prefix sum of threadCounts    0 - (totalSize-1)
    int64_t       localCount;    // the filled data of buffer         0 - localCapacity (eof when == localCapacity)
    int64_t       localCapacity; // the allocated capacity of buffer  >=0, <totalSize
} _DB_Element;
typedef _DB_Element *_DB_ElementPtr;
typedef shared [] _DB_Element *_DB_ElementShPtr;
typedef shared [1] _DB_Element *_DB_ElementAllShPtr;

// offsets and sizes are in units of blockSize
typedef struct {
    _DB_ElementAllShPtr elements;
    int64_t             fixedCapacity;  // optimization for fixedCapacity buffers.  never 0, >0 if fixed size, <0 if variable lengths
    int64_t             totalCount;     // the sum of all localCount entries
    int64_t             totalCapacity;  // sum of all localCapacity entries
    int32_t             blockSize;      // the bytes per entry
    int32_t             lastElementIdx; // cache
    _DB_Element         lastElement;    // cache
} _DB_Handle;
typedef _DB_Handle *DistributedBuffer;

// collective functions

// initializes a DistributedBuffer with fixed allocation/capacity on every THREAD
// calculates totalCapacity, totalSize, and elements
// initializes all elements with
//        localCapacity = (totalCapcity+THREADS-1) / THREADS + 1
//        localSize <= (totalSize + THREADS - 1) / THREADS
//        globalOffset = prefixSum(localSize)
//        threadOffset = prefixSum(mySize)
DistributedBuffer DistributedBuffer_initFixed(int64_t myCount, int32_t blockSize);

// same as initFixed except per-thread allocations are not fixed sized
DistributedBuffer DistributedBuffer_initVariable(int64_t myCount, int32_t blockSize);

// releases all memory
void DistributedBuffer_free(DistributedBuffer *db);

// non-collective functions

// returns the totalSize of the DistributedBuffer
int64_t DistributedBuffer_getTotalCount(DistributedBuffer db);

// returns the position of the first block local to this thread
int64_t DistributedBuffer_getLocalGlobalOffset(DistributedBuffer db, int thread);

// returns the position of the first block allocated for this thread to populate
int64_t DistributedBuffer_getThreadGlobalOffset(DistributedBuffer db, int thread);

// returns the count used to initialize DB by thread
int64_t DistributedBuffer_getThreadCount(DistributedBuffer db, int thread);

// returns the size in blocks of the local buffer
int64_t DistributedBuffer_getLocalCount(DistributedBuffer db, int thread);

// writes the entire contents of Buffer at the globalOffset, accounting for element edges -- may call itself recursively
// Buffer must be in units of blockSize!
// returns the count of blockSize elements written
int64_t DistributedBuffer_writeBufferAt(DistributedBuffer db, Buffer b, int64_t globalOffset);

// writes writeCount blocks into Buffer starting at globalOffset, accounting for element edges -- may call itself recursively
// returns the count of blockSize elements written
int64_t DistributedBuffer_writeAt(DistributedBuffer db, int64_t globalOffset, void *, int64_t writeCount);

// reads the readCount blocks into Buffer starting at globalOffset, accounting for element edges -- may call itself recursively
// Buffer must be in units of blockSize!
// returns the count of blockSize elements read
int64_t DistributedBuffer_readBufferAt(DistributedBuffer db, Buffer b, int64_t globalOffset, int64_t readCount);

// returns a shared char pointer to the first byte at the globalOffset block
SharedCharPtr DistributedBuffer_getSharedPtr(DistributedBuffer db, int64_t globalOffset);

// returns a local void pointer to the first byte at the globalOffset block that in in local memory of MYTHREAD
void *DistributedBuffer_getLocalStartPtr(DistributedBuffer db);

#endif // _UPC_DISTRIBUTED_BUFFER_H_
