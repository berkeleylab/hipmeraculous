#ifndef MERACULOUS_H
#define MERACULOUS_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#ifndef MAX_KMER_SIZE
#define MAX_KMER_SIZE 64
#endif

#define STR_HELPER(x) # x
#define STR(x) STR_HELPER(x)
#define getCompileParameters() \
    printf("Kmer: " STR(MAX_KMER_SIZE) " Packed: " STR(MAX_KMER_PACKED_LENGTH) "\n")

/* TODO: Define different block sizes according to data structure */
#define BS 1

#ifndef EXPANSION_FACTOR
#define EXPANSION_FACTOR 2
#endif

/* minimum allocation for contig sequences */
#ifndef MINIMUM_CONTIG_SIZE
#define MINIMUM_CONTIG_SIZE 192
#endif

#define INCR_FACTOR 2

/* Define segment length for FASTA sequences */
#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

#define oracle_t unsigned char

#endif // MERACULOUS_H
