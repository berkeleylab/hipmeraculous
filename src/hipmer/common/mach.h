#ifndef __MACH_H
#define __MACH_H

#if defined (__cplusplus)
extern "C" {
#endif

// return the threadsPer where distance is 0 == PTHREAD / NUMA, 1 == NODE
int _divineThreadsPer(int distance);
#define divineThreadsPerNode() _divineThreadsPer(1)
#define divineThreadsPerNumaNode() _divineThreadsPer(0)

typedef struct {
    size_t vm_size;
    size_t vm_rss;
    size_t share;
} memory_usage_t;

// prototypes
int _get_cores_per_node_HW(void);
void get_mem_usage(memory_usage_t *mem);
int get_max_mem_usage_mb(void);
double get_free_mem_gb(void);
double get_used_mem_gb(void);
int get_cores_per_node(void);
void print_memory_used(void);

#if defined (__cplusplus)
}
#endif


#endif // __MACH_H
