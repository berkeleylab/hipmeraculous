#ifndef _UPC_DISTRIBUTED_FILE_H_
#define _UPC_DISTRIBUTED_FILE_H_

#include <stdint.h>

#if defined (__cplusplus)
extern "C" {
#endif

/*
 * A Distributed file can be in two mutually-inclusive states:
 *
 * 1) each thread has a unique filepath with 0 or more bytes (best if in /dev/shm)
 * 2) each thread has a 0 or more byte block of a sigle global file
 *
 * A global file should have a corresponding .idx of with int64_t offsets
 *   if THREADS is different from the number of offsets (for example a restart with a
 *   a different thread count), then load balancing will be approximate but all
 *   boundaries will be along the original idx int64_t offsets
 */

typedef struct {
    char * myFileName, *ourFileName;
    size_t myOffset, mySize, globalSize;
    int8_t isGlobal, isLocal, hasIdx;
} DistributedFileBase;
typedef DistributedFileBase *DistributedFile;

// a collective function
// if neither exist, just build names and prep rankpath. offsets and sizes will be 0
// if myFileName exists, calculate globalSize, myOffset and mySize
// if ourFileName exists and read index, read the index in thread 0 and set globalSize, myOffset and mySize over all
//    assert myFileName does not exist or yields the same offsets and sizes
// if ourFileName exists and there is no index then error
// filePattern must be the same for all threads - no need for thread uniqueness
DistributedFile initDistributedFile(const char *filePattern);

// just free data structures, do nothing on filesystem
void freeDistributedFile(DistributedFile *df);

// simple methods
const char *getMyFileName(DistributedFile df);
const char *getOurFileName(DistributedFile df);
int64_t getMyOffset(DistributedFile df);
int64_t getMySize(DistributedFile df);
int64_t getOurSize(DistributedFile df);

//
// collective functions
//

// copy myFileName into the single globalFileName and create a .idx file
// error if globalFileName already exists
void copyOutOurFile(DistributedFile df);

// copy my block(s) from globalFileName into myFileName
// error if myFileName exists already
void copyInMyFile(DistributedFile df);

#if defined (__cplusplus)
}
#endif

#endif // _UPC_DISTRIBUTED_FILE_H_
