#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <upc.h>

#include "StaticVars.h"
#include "upc_common.h"
#include "timers.h"

void _set_rank_and_size(int myRank, int ourSize, const char * file, int line)
{
    if (myRank != MYTHREAD || ourSize != THREADS) {
        fprintf(stderr, "ERROR: set_rank_and_size(%d,%d) called with improper values: (%d,%d)\n", myRank, ourSize, MYTHREAD, THREADS);
        _common_exit(1, file, line);
    }
}

int _get_rank(const char * file, int line)
{
    return MYTHREAD;
}
int _get_num_ranks(const char * file, int line)
{
    return THREADS;
}

void _common_exit(int x, const char * file, int line)
{
    upc_common_exit(x, file, line); // macro defined in upc_common.h
}

// return the threadsPer where distance is 0 == PTHREAD / NUMA, 1 == NODE
int divineThreadsPerByBUPC(int distance)
{
    int cpn = 0;

#ifdef __BERKELEY_UPC__
    int dist = BUPC_THREADS_SAME, thread = 0;
    if (distance == 0) {
        dist = BUPC_THREADS_VERYNEAR;
    } else if (distance == 1) {
        dist = BUPC_THREADS_NEAR;
    }
    for (thread = 0; thread < THREADS; thread++) {
        if (bupc_thread_distance(0, thread) <= dist) {
            cpn++;
        } else {
            break;
        }
    }
#else
#endif
    return cpn;
}

int isOnSameNode(int threada, int threadb)
{
    int isIt = (_sv) ? ((threada / MYSV.cores_per_node) == (threadb / MYSV.cores_per_node)) : -1;

#ifdef __BERKELEY_UPC__
#ifdef DEBUG
    int isIt2 = bupc_thread_distance(threada, threadb) <= BUPC_THREADS_NEAR;
    if (isIt >= 0 && isIt2 != isIt) {
        WARN("isOnSameNode(%d,%d): returns different answers between cores_per_node (%d) and bupc_thread_distance(%d > %d)\n", threada, threadb, (_sv) ? MYSV.cores_per_node : -1, bupc_thread_distance(threada, threadb), BUPC_THREADS_FAR);
    }
#endif
#endif
    return isIt;
}

int isOnMyNode(int threada)
{
    return isOnSameNode(MYTHREAD, threada);
}

void print_args(option_t *optList, const char *stage)
{
    if (!MYTHREAD) {
        option_t *olist = optList;
        option_t *opt = NULL;
        //serial_printf(KLWHITE "STAGE %s ", stage);
        serial_printf("STAGE %s ", stage);
        while (olist) {
            opt = olist;
            olist = olist->next;
            serial_printf("-%c ", opt->option);
            if (opt->argument) {
                serial_printf("%s ", opt->argument);
            }
        }
        //serial_printf(KNORM "\n");
        serial_printf("\n");
    }
    upc_barrier;
}

int get_shared_heap_mb(void)
{
    char *shared_heap_size = getenv("UPC_SHARED_HEAP_SIZE");

    if (!shared_heap_size) {
        return 0;
    }
    int len = strlen(shared_heap_size);
    if (!len) {
        return 0;
    }
    int64_t shared_heap_bytes;
    if (shared_heap_size[len - 1] == 'G' || shared_heap_size[len - 1] == 'g') {
        shared_heap_bytes = ONE_GB;
    } else if (shared_heap_size[len - 1] == 'M' || shared_heap_size[len - 1] == 'm') {
        shared_heap_bytes = ONE_MB;
    } else if (shared_heap_size[len - 1] == 'K' || shared_heap_size[len - 1] == 'k') {
        shared_heap_bytes = ONE_KB;
    } else {
        shared_heap_bytes = ONE_MB;
    }
    shared_heap_bytes *= atoi(shared_heap_size);
    return shared_heap_bytes / ONE_MB;
}

// for printing out diagnostics
void init_diags(void)
{
    if (!_sv) {
        return;
    }
    if (!MYTHREAD) {
        MYSV._my_diags = fopen_chk("diags.log", "a");
    }
}

void fini_diags(void)
{
    if (!_sv) {
        return;
    }
    if (MYSV._my_diags) {
        fclose(MYSV._my_diags);
        MYSV._my_diags = NULL;
    }
}


// appends heap, potentially extending the size ***UNSAFE if multiple threads will access this data****
// returns a pointer to the newly copied data.
// Any pointer returned may become invalid upon the next operation, though the data will be preserved
SharedCharPtr _appendHeap(HeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line)
{
    assert(heapPtr != NULL);
    assert(heapPtr->ptr != NULL);
    Heap oldHeap = *heapPtr;
    if (oldHeap.position + length >= oldHeap.size) {
        // Must reallocate heap
        int64_t newSize = (heapPtr->size + length) * 3 / 2 + 64;
        LOGF("Reallocating heap from thread %llu (locally) from %llu to %llu bytes (%s:%d)\n", (llu)upc_threadof(oldHeap.ptr), (llu)oldHeap.size, (llu)newSize, whichFile, line);
        Heap newHeap;
        initHeap(&newHeap, newSize);
        assert(newHeap.size >= newSize);
        assert(newHeap.ptr != NULL);
        assert(upc_threadof(newHeap.ptr) == MYTHREAD);
        SharedCharPtr tmp1 = newHeap.ptr;
        char *dest = (char *)tmp1;
        SharedCharPtr src = oldHeap.ptr;

        if (upc_threadof(oldHeap.ptr) == MYTHREAD) {
            memcpy(dest, (const char *)src, oldHeap.position);
        } else {
            upc_memget(dest, src, oldHeap.position);
        }
        newHeap.position = oldHeap.position;
        assert(newHeap.position <= newHeap.size);
        freeHeap(heapPtr);
        *heapPtr = newHeap;
        upc_fence;
    }

    assert(heapPtr->ptr != NULL);
    assert(heapPtr->position + length <= heapPtr->size);
    SharedCharPtr result = heapPtr->ptr + heapPtr->position;
    upc_memput(result, data, length);
    heapPtr->position += length;
    assert(heapPtr->position == oldHeap.position + length); // ensure no race has happened.
    return result;
}

// Threadsafe version of _appendHeap
SharedCharPtr _safeAppendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line)
{
    assert(heapPtr != NULL);
    Heap localHeap, oldHeap;
    int64_t oldPos = -1, oldSize = -1;
    // if heapPtr is already past maximum, wait for another thread to fix
    while (1) {
        localHeap = *heapPtr;
        if (localHeap.position < localHeap.size) {
            // potentially okay. cached heap has some room
            if (oldPos == -1) {
                UPC_ATOMIC_FADD_I64(&oldPos, &(heapPtr->position), length);
            }
            UPC_ATOMIC_GET_I64(&oldSize, &(heapPtr->size));
            if (oldPos < oldSize) {
                // still okay oldPos is less than existing size
                if (oldPos < oldSize) {
                    // ready to append
                    if (localHeap.size != oldSize) {
                        // need to refresh pointer
                        localHeap = *heapPtr;
                        assert(localHeap.size == oldSize);
                    }
                    break;
                } // else wait for another thread to reallocate
            }     // else wait for another thread to reallocate
        }         // else wait for another thread to reallocate
          // try again - some other thread pushed size boundary and will reallocate
        UPC_POLL;
    }
    CHECK_BOUNDS(oldPos, localHeap.size);

    // no other thread has modified heapPtr
    assert(localHeap.size == heapPtr->size);
    assert(localHeap.ptr == heapPtr->ptr);

    // remember the old
    oldHeap = localHeap;

    int isNew = 0;
    int64_t newPos = oldPos + length;
    if (newPos >= localHeap.size) {
        isNew = 1;
        // this thread must re-allocate (since it pushed the size boundary)
        int64_t newSize = (localHeap.size + length) * 2 / 3 + 64;
        assert(newPos < newSize);
        Heap newHeap;
        initHeap(&newHeap, newSize);
        assert(upc_threadof(newHeap.ptr) == MYTHREAD);
        while (1) {
            int64_t oldFencePos;
            UPC_ATOMIC_CSWAP_I64(&oldFencePos, &(heapPtr->fencePosition), oldPos, newPos);
            if (oldPos == oldFencePos) {
                break;
            }
            // wait for all threads to finish upc_memput to the old heap!
            assert(heapPtr->fencePosition <= oldPos);
            UPC_POLL;
        }
        assert(heapPtr->fencePosition == newPos);

        SharedCharPtr src = localHeap.ptr;
        SharedCharPtr dst = newHeap.ptr;
        assert(heapPtr->ptr == src);
        if (upc_threadof(src) == MYTHREAD) {
            memcpy((char *)dst, (char *)src, oldPos);
        } else {
            upc_memget((char *)dst, src, oldPos);
        }
        newHeap.position = newPos;
        newHeap.fencePosition = oldPos;
        assert(heapPtr->ptr == localHeap.ptr);
        assert(heapPtr->ptr == oldHeap.ptr);
        localHeap = newHeap;
    }
    SharedCharPtr newData = localHeap.ptr + oldPos;
    upc_memput(newData, data, length);
    UPC_FENCE;
    if (isNew) {
        *heapPtr = localHeap;
        UPC_FENCE;
        UPC_FREE_CHK(oldHeap.ptr);
    }
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(heapPtr->fencePosition), length);
    return newData;
}

// appends heap, potentially extending the size *** UNSAFE if multiple threads will access this data concurrently ***
// returns a pointer to the newly copied data.
SharedCharPtr _appendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line)
{
    assert(heapPtr != NULL);
    Heap heap = *heapPtr;
    SharedCharPtr result = _appendHeap(&heap, data, length, whichFile, line);
    *heapPtr = heap;
    return result;
}

void _extendHeapList(SharedHeapListPtr *ptr, uint64_t bytes)
{
    assert(ptr != NULL);
    assert(*ptr != NULL);
    assert(upc_threadof(*ptr) == MYTHREAD);
    SharedHeapListPtr newHeapList = NULL;
    initHeapList(newHeapList, bytes);
    assert(newHeapList);
    assert(upc_threadof(newHeapList) == MYTHREAD);
    assert(newHeapList->next == NULL);
    assert(newHeapList->capacity >= bytes);
    LOGF("Extended HeapList with %lld bytes: %d,%lld with new %d,%lld\n", (lld) bytes, upc_threadof(*ptr), (lld) upc_addrfield(*ptr), upc_threadof(newHeapList), (lld) upc_addrfield(newHeapList));
    newHeapList->next = *ptr;
    *ptr = newHeapList;
}

SharedCharPtr _allocFromHeapList(SharedHeapListPtr *ptr, uint64_t bytes)
{
    assert(ptr != NULL);
    assert(*ptr != NULL);
    assert(upc_threadof(*ptr) == MYTHREAD);
    SharedHeapListPtr heapList = *ptr;
    if (heapList->size + bytes >= heapList->capacity) {
        _extendHeapList(ptr, heapList->capacity > bytes * 100 ? heapList->capacity : bytes * 100);
        assert(*ptr != heapList);
        heapList = *ptr;
    }
    assert(heapList->size + bytes < heapList->capacity);
    uint64_t pos = heapList->size;
    heapList->size += bytes;
    SharedCharPtr scp = ((SharedCharPtr) & (heapList->_data[0])) + pos;
    assert(upc_threadof(scp) == MYTHREAD);
    return scp;
}

GlobalFinished initGlobalFinished(int coresPerNode)
{
    GlobalFinished gf = (_GlobalFinished *)calloc_chk(1, sizeof(_GlobalFinished));

    gf->startTime = NOW;
    gf->coresPerNode = coresPerNode;
    UPC_ALL_ALLOC_CHK(gf->globalFinished, THREADS, sizeof(RankCount));
    gf->globalFinished[MYTHREAD] = 0;
    gf->extraIterations = 1;
    upc_barrier;
    LOGF("initGlobalFinished(%d) %0.3f s since init\n", coresPerNode, NOW - gf->startTime);
    return gf;
}

void freeGlobalFinished(GlobalFinished *gf)
{
    assert(gf);
    if (*gf) {
        LOGF("freeGlobalFinished() %0.3f s elapsed since init\n", NOW - (*gf)->startTime);
        UPC_ALL_FREE_CHK((*gf)->globalFinished);
        free_chk(*gf);
        *gf = NULL;
    }
}

// just increments. explicitly does not return the count -- use isGlobalFinished for checking
void incrementGlobalFinished(GlobalFinished gf)
{
    UPC_POLL;
    double start = NOW;
    assert(gf);
    RankCount myLeaderThread = MYTHREAD - (MYTHREAD % gf->coresPerNode);
    assert(myLeaderThread <= MYTHREAD);
    assert(myLeaderThread < THREADS);
    assert(myLeaderThread >= 0);
    LOGF("incrementGlobalFinished() %0.3f s elapsed since init\n", NOW - gf->startTime);
    RankCount myCount;
    UPC_ATOMIC_FADD_I32(&myCount, &(gf->globalFinished[MYTHREAD]), 1);
    if (myCount != 0) {
        // this function should only be called once per thread
        // Thread 0 and other leaderNode threads are okay to be non-zero under some conditions
        if ((MYTHREAD % gf->coresPerNode) == 0) {
            if (myCount >= THREADS) {
                DIE("Invalid incrementGlobalFinished.  Thread 0 should never be incremented over THREADS(==%d)! %d \n", myCount + 1, THREADS);
            } else if (MYTHREAD != 0) {
                // leader thread, not global leader
            }
        } else {
            DIE("Invalid incrementGlobalFinished this thread finished more than once: %d!\n", myCount + 1);
        }
    }
    RankCount nodeCount = myCount;
    if (MYTHREAD != myLeaderThread) { // do not double count leaders!
        UPC_ATOMIC_FADD_I32(&nodeCount, &(gf->globalFinished[myLeaderThread]), 1);
    }
    RankCount threadsDoneOnNode = (nodeCount + 1); // start + myself
    char amNodeFinishingThread = 0, amGlobalFinishingThread = 0;
    if ((threadsDoneOnNode % gf->coresPerNode) == 0) {
        // This thread was the last on this node, notify all other nodes
        assert(threadsDoneOnNode >= gf->coresPerNode);
        amNodeFinishingThread = 1;
        RankCount masterCount = 0;
        RankCount lastNotifyThread = THREADS; // notify all leader threads
        for (RankCount nodeThread = 0; nodeThread < lastNotifyThread; nodeThread += gf->coresPerNode) {
            RankCount count;
            if (nodeThread == myLeaderThread) {
                // already counted this node!
#ifdef DEBUG
                // debug check the value holds certain assertions
                UPC_ATOMIC_GET_I32(&count, &(gf->globalFinished[nodeThread]));
                assert(count >= gf->coresPerNode);
                assert(count >= threadsDoneOnNode);
#endif
                count = threadsDoneOnNode; // use the atomic value we actually set
            } else {
                UPC_ATOMIC_FADD_I32(&count, &(gf->globalFinished[nodeThread]), gf->coresPerNode);
                count += gf->coresPerNode;
                DBG("incrementGlobalFinished signaled nodeThread %d (was %d is now %d)\n", nodeThread, count - gf->coresPerNode, count);
            }
            if (count > THREADS) {
                DIE("Invalid assumption in incrementGlobalFinished! count %d for nodeThread %d should not exceed THREADS! (%d)\n", count, nodeThread, THREADS - gf->coresPerNode);
            }
            if (nodeThread == 0) {
                masterCount = count;
            }
        }
        assert(masterCount >= 1);
        if (masterCount == THREADS) {
            // this was the first to signal master thread ... nothing special here
            amGlobalFinishingThread = 1;
        }
        upc_fence;
    }
    double end = NOW;
    LOGF("Completed incrementGlobalFinished in %0.3f s (threadsDoneOnNode = %d).  %s %s\n", end - start, threadsDoneOnNode, amNodeFinishingThread ? " finished For Node " : "", amGlobalFinishingThread ? " finished for World " : "");
}

// only trust this number if it == THREADS
RankCount getGlobalFinishedCountFast(GlobalFinished gf)
{
    upc_fence;
    UPC_POLL;
    if (gf->allAreFinished) {
        // quick return
        return THREADS;
    }
    RankCount count;
    UPC_ATOMIC_GET_I32(&count, &(gf->globalFinished[MYTHREAD]));
    DBG2("getGlobalFinishedCountFast() got %d\n", count);
    if (count == THREADS && (MYTHREAD % gf->coresPerNode) == 0) {
        // this is a leader thread, let all other threads on this node see the finished value
        // set remaining threads
        LOGF("getGlobalFinishedCountFast detected finished and setting remaining threads on this node\n");
        for (RankCount checkThread = MYTHREAD + 1; checkThread < MYTHREAD + gf->coresPerNode; checkThread++) { // only my local threads (and not me)
            RankCount got;
            UPC_ATOMIC_CSWAP_I32(&got, &(gf->globalFinished[checkThread]), 1, count);
            if (got == THREADS) {
                break;                 // already updated my local threads
            }
            if (got != 1) {
                DIE("Invalid assumption in getGlobalFinishedCountFast!  Updating local threads, but found count %d on thread %d, expected 1 or THREADS (%d)\n", got, checkThread, THREADS);
            }
        }
        upc_fence;
    }
    if (count == THREADS && !gf->allAreFinished) {
        LOGF("getGlobalFinishedCountFast detected finished event at %0.3f after init.\n", NOW - gf->startTime);
        gf->allAreFinished = 1;
    }
    return count;
}

// return 0 when all threads on a node have completed
int16_t getThreadsRunningOnNode(GlobalFinished gf, int thread)
{
    int mod = thread % gf->coresPerNode;
    int leaderNodeThread = thread - mod;
    RankCount count;

    UPC_ATOMIC_GET_I32(&count, &(gf->globalFinished[leaderNodeThread]));
    return count % gf->coresPerNode;
}

// Use this for an approximation of the number or threads that are finished
RankCount getGlobalFinishedCountSlow(GlobalFinished gf)
{
    RankCount count = getGlobalFinishedCountFast(gf);

    if (count != THREADS) { /* get the slightly more accurate approximation from this node leader's count */
        RankCount myLeaderThread = MYTHREAD - (MYTHREAD % gf->coresPerNode);
        if (myLeaderThread != MYTHREAD) {
            UPC_ATOMIC_GET_I32_RELAXED(&count, &(gf->globalFinished[myLeaderThread]));
        }
    }
    return count;
}

char isGlobalFinished(GlobalFinished gf)
{
    RankCount count = getGlobalFinishedCountFast(gf);

    if (count == THREADS) {
        // sync up with a barrier if there are extra iterations
        if (gf->extraIterations-- > 0) {
           UPC_LOGGED_BARRIER;
           return 0;
        } 
    }
    return count == THREADS;
}

#include "upc_compatibility.h"

static void _initAtomicMetadataThreads(HipMerAtomicMetadata_t *amd)
{
    if (UC_LOCKS_PER_THREAD == 0) DIE("Invalid assumption!\n");
    UPC_ALL_ALLOC_CHK0(amd->threadLocks, THREADS * UC_LOCKS_PER_THREAD, sizeof(upc_lock_t_ptr));
    for (int64_t i = MYTHREAD; i < THREADS * UC_LOCKS_PER_THREAD; i += THREADS) {
        amd->threadLocks[i] = upc_global_lock_alloc();
    }
    UPC_FENCE;
}
static void _destroyAtomicMetadataThreads(HipMerAtomicMetadata_t *amd)
{
    if (!amd->threadLocks) return;
    if (UC_LOCKS_PER_THREAD == 0) DIE("Invalid assumption!\n");
    upc_barrier;
    for (int64_t i = MYTHREAD; i < THREADS * UC_LOCKS_PER_THREAD; i += THREADS) {
        if ( !amd->threadLocks[i] ) continue;
        if (upc_lock_attempt( amd->threadLocks[i] )) {
            upc_unlock( amd->threadLocks[i] );
            upc_lock_free(amd->threadLocks[i]);
        } else {
            fprintf(stdout, "Th%d: _destroyAtomicMetadataThreads could not acquire lock(%lld)\n", MYTHREAD, (lld) i); fflush(stdout);
        }
        amd->threadLocks[i] = NULL;
    }
    upc_barrier;
    UPC_ALL_FREE_CHK0(amd->threadLocks);
    upc_barrier;
}

#if USE_UPC_ATOMIC_API

void initAtomicMetadata(_StaticVars *mySV)
{
    HipMerAtomicMetadata_t *amd = (HipMerAtomicMetadata_t *)mySV->optionalAtomicMetadata;

    if (amd == NULL) {
        amd = calloc(1, sizeof(HipMerAtomicMetadata_t));
        _initAtomicMetadataThreads(amd);
        amd->I32_ = upc_all_atomicdomain_alloc(UPC_INT32, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_DEFAULT);
        amd->I64_ = upc_all_atomicdomain_alloc(UPC_INT64, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_DEFAULT);

        amd->I32_LL = upc_all_atomicdomain_alloc(UPC_INT32, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_LATENCY);
        amd->I64_LL = upc_all_atomicdomain_alloc(UPC_INT64, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_LATENCY);

        amd->I32_HT = upc_all_atomicdomain_alloc(UPC_INT32, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_THROUGHPUT);
        amd->I64_HT = upc_all_atomicdomain_alloc(UPC_INT64, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UPC_ATOMIC_HINT_THROUGHPUT);

        amd->SHPTR_ = upc_all_atomicdomain_alloc(UPC_PTS, UPC_GET | UPC_SET | UPC_CSWAP, UPC_ATOMIC_HINT_DEFAULT);
        amd->SHPTR_LL = upc_all_atomicdomain_alloc(UPC_PTS, UPC_GET | UPC_SET | UPC_CSWAP, UPC_ATOMIC_HINT_LATENCY);
        amd->SHPTR_HT = upc_all_atomicdomain_alloc(UPC_PTS, UPC_GET | UPC_SET | UPC_CSWAP, UPC_ATOMIC_HINT_THROUGHPUT);

        amd->I64_NEAR = upc_all_atomicdomain_alloc(UPC_INT64, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UC_HINT_NEAR);
        amd->SHPTR_NEAR = upc_all_atomicdomain_alloc(UPC_PTS, UPC_GET | UPC_SET | UPC_CSWAP, UC_HINT_NEAR);

        amd->I64_FAR = upc_all_atomicdomain_alloc(UPC_INT64, UPC_GET | UPC_SET | UPC_CSWAP | UPC_ADD | UPC_INC, UC_HINT_FAR);
        amd->SHPTR_FAR = upc_all_atomicdomain_alloc(UPC_PTS, UPC_GET | UPC_SET | UPC_CSWAP, UC_HINT_FAR);

        mySV->optionalAtomicMetadata = amd;
    }
}

void destroyAtomicMetadata(_StaticVars *mySV)
{
    HipMerAtomicMetadata_t *amd = (HipMerAtomicMetadata_t *)mySV->optionalAtomicMetadata;

    if (amd != NULL) {
        _destroyAtomicMetadataThreads(amd);
        upc_all_atomicdomain_free(amd->I32_);
        upc_all_atomicdomain_free(amd->I32_LL);
        upc_all_atomicdomain_free(amd->I32_HT);
        upc_all_atomicdomain_free(amd->I64_);
        upc_all_atomicdomain_free(amd->I64_LL);
        upc_all_atomicdomain_free(amd->I64_HT);
        upc_all_atomicdomain_free(amd->I64_NEAR);
        upc_all_atomicdomain_free(amd->I64_FAR);
        upc_all_atomicdomain_free(amd->SHPTR_);
        upc_all_atomicdomain_free(amd->SHPTR_LL);
        upc_all_atomicdomain_free(amd->SHPTR_HT);
        upc_all_atomicdomain_free(amd->SHPTR_NEAR);
        upc_all_atomicdomain_free(amd->SHPTR_FAR);
        free(amd);
        mySV->optionalAtomicMetadata = NULL;
    }
}
#else
void initAtomicMetadata(_StaticVars *mySV)
{
    HipMerAtomicMetadata_t *amd = (HipMerAtomicMetadata_t *)mySV->optionalAtomicMetadata;

    if (amd == NULL) {
        amd = calloc(1, sizeof(HipMerAtomicMetadata_t));
        _initAtomicMetadataThreads(amd);
        mySV->optionalAtomicMetadata = amd;
    }
}
void destroyAtomicMetadata(_StaticVars *mySV)
{
    HipMerAtomicMetadata_t *amd = (HipMerAtomicMetadata_t *)mySV->optionalAtomicMetadata;

    if (amd != NULL) {
        _destroyAtomicMetadataThreads(amd);
        free(amd);
        mySV->optionalAtomicMetadata = NULL;
    }
}
#endif
