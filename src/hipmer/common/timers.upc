#include <string.h>
#include <assert.h>

#include "utils.h"
#include "tracing.h"
#include "timers.h"

typedef struct {
    char *     description;
    upc_tick_t t_start;
    upc_tick_t t_stop;
    upc_tick_t t_elapsed;
} upc_timer_t;

static upc_timer_t _timers[MAX_TIMERS];

void init_timers(void)
{
    for (int i = 0; i < MAX_TIMERS; i++) {
        memset(&_timers[i], 0, sizeof(upc_timer_t));
    }
}

double start_timer(int t, const char *desc)
{
    assert(t >= 0 && t < MAX_TIMERS);
    _timers[t].t_start = upc_ticks_now();
    if (!_timers[t].description) {
        _timers[t].description = strdup(desc);
    }
    return TICKS_TO_S(_timers[t].t_start);
}

double stop_timer(int t)
{
    assert(t >= 0 && t < MAX_TIMERS);
    _timers[t].t_stop = upc_ticks_now();
    double duration = (_timers[t].t_stop - _timers[t].t_start);
    _timers[t].t_elapsed += duration;
    return TICKS_TO_S(_timers[t].t_stop);
}

double now()
{
    return TICKS_TO_S(upc_ticks_now());
}

double get_elapsed_time(int t)
{
    assert(t >= 0 && t < MAX_TIMERS);
    return TICKS_TO_S(_timers[t].t_elapsed);
}

double get_timer_all_max(int t)
{
    assert(t >= 0 && t < MAX_TIMERS);
    return TICKS_TO_S(reduce_long(_timers[t].t_elapsed, UPC_MAX, SINGLE_DEST));
}

typedef struct {
    double t[MAX_TIMERS];
} all_timers_t;

void print_timers(int serial_only)
{
    upc_tick_t start = upc_ticks_now();
    shared all_timers_t *per_thread_timer = NULL;

    UPC_ALL_ALLOC_CHK(per_thread_timer, THREADS, sizeof(all_timers_t));

    serial_printf("  %-25s %10s %10s %10s %10s\n", "Timers", "min", "av", "max", "bln");
    for (int i = 0; i < MAX_TIMERS; i++) {
        if (!_timers[i].description) {
            continue;
        }
        per_thread_timer[MYTHREAD].t[i] = get_elapsed_time(i);
    }
    upc_barrier;
    for (int i = 0; i < MAX_TIMERS; i++) {
        if (!_timers[i].description) {
            continue;
        }
        if (!MYTHREAD) {
            double t_min = 10000000000;
            double t_max = 0;
            double t_avg = 0;
            for (int thread = 0; thread < THREADS; thread++) {
                double tval = per_thread_timer[thread].t[i];
                t_avg += tval;
                if (tval > t_max) {
                    t_max = tval;
                }
                if (tval < t_min) {
                    t_min = tval;
                }
            }
            t_avg /= THREADS;
            double bln = 0.0;
            if (t_max > 0.0) {
                bln = t_avg / t_max;
            }
            serial_printf("  %-25s %10.2f %10.2f %10.2f %10.2f\n", _timers[i].description,
                          t_min, t_avg, t_max, bln);
        }
        LOGF("My timer:  %-25s %10.2f\n", _timers[i].description, get_elapsed_time(i));
    }
    serial_printf("print_timers took %0.3f s\n", TICKS_TO_S(upc_ticks_now() - start));
    upc_barrier;
    UPC_ALL_FREE_CHK(per_thread_timer);
}
