#ifndef _UPC_COMMON_H
#define _UPC_COMMON_H

#include <assert.h>

#include <upc.h>

#include "defines.h"
#include "memory_chk.h"
#include "upc_compatibility.h" // upc_compatibility loads first
#include "common_base.h"
#include "Buffer.h"

#ifdef DEBUG
#define upc_common_exit(x,file,line) do { \
    if (x) fprintf(stderr, "Th%d [%s:%d] calling upc_global_exit(%d)\n", get_rank(), file, line, x); \
    fflush(stdout); \
    fflush(stderr); \
    if (x) { \
        assert(0); \
    } \
    upc_global_exit(x); \
} while (0)
#else
#define upc_common_exit(x,file,line) do { \
    fflush(stdout); \
    fflush(stderr); \
    upc_global_exit(x); \
} while (0)
#endif

// override EXIT FUNC
#undef EXIT_FUNC
#define EXIT_FUNC(x) upc_common_exit(x, __FILENAME__, __LINE__)

#include "common.h"            // common loads next
#include "StaticVars.h"
#include "log.h"


int divineThreadsPerByBUPC(int distance);

void print_args(option_t *optList, const char *stage);

int get_shared_heap_mb(void);

// for printing out diagnostics
void init_diags(void);

void fini_diags(void);

#ifndef LOOP_UNTIL_WAIT_MS
#define LOOP_UNTIL_WAIT_MS 5   /* 5 ms */
#endif

#define loop_until(STMT, COND) \
    { \
        int64_t __attempts = 0; \
        UPC_TICK_T __start = UPC_TICKS_NOW(), __lastPrint, __now, __waiter; \
        __lastPrint = __start; \
        do { \
            STMT; \
            if (COND) { break; } \
            __attempts++; \
            __waiter = __now = UPC_TICKS_NOW(); \
            do { \
                if (__now > __lastPrint && UPC_TICKS_TO_SECS(__now - __lastPrint) >= 5.0) { /* ignore time adjustments, but print every 5 secs */ \
                    WARN("possible infinite loop in loop_until(" # STMT ", " # COND "): blocked for %0.3f s! %lld attempts %s:%u\n", UPC_TICKS_TO_SECS(__now - __start), (lld)__attempts, __FILE__, __LINE__); \
                    __lastPrint = __now; \
                } \
                UPC_POLL; \
                __now = UPC_TICKS_NOW(); \
            } while (__attempts % 2 == 0 && UPC_TICKS_TO_SECS(__now - __waiter) < (LOOP_UNTIL_WAIT_MS / 1000.0)); \
        } while (1); \
        assert(COND); \
    }

#ifndef REMOTE_ASSERT
#define REMOTE_ASSERT 0
#endif

#if REMOTE_ASSERT != 0

#define remote_assert(cond) do { \
        UPC_FENCE; \
        UPC_POLL; \
        loop_until(((void)0), cond); if (!(cond)) { WARN("remote assert " # cond " failed!\n"); } assert(cond); \
} while (0)

#else
#define remote_assert(cond)     /* noop(cond) */
#endif

#define LOAD_BALANCED_FOR(idx, count) \
    for(int64_t idx = MYTHREAD, idx ## tmp = 0; \
       (idx ## tmp < (count) && ((idx = idx ## tmp % (count)) < count)); \
       idx ## tmp ++ )


#define LOAD_BALANCED_GLOBAL_START(count) (MYTHREAD * (((((int64_t)(count))/THREADS)/THREADS)*THREADS))
#define LOAD_BALANCED_FORALL(idx, count) \
    for(int64_t idx = MYTHREAD, \
                idx ## startOffset = LOAD_BALANCED_GLOBAL_START(count), \
                idx ## tmp = (LOAD_BALANCED_GLOBAL_START(count) + MYTHREAD), \
                idx ## tmp2 = (((count) % THREADS) > MYTHREAD) ? (((count)/THREADS+1)*THREADS) : (((count)/THREADS)*THREADS); \
        (idx ## tmp < (count) + idx ## startOffset) && ((idx = (idx ## tmp < (count) ? idx ## tmp : (idx ## tmp - idx ## tmp2))) < (count)) ; \
        idx ## tmp += THREADS)

typedef shared [] double *SharedDoublePtr;
typedef shared [] float *SharedFloatPtr;
typedef shared [] int64_t *SharedLongPtr;
typedef shared [] int8_t *SharedInt8Ptr;
typedef shared [] uint8_t *SharedUInt8Ptr;
typedef shared [] char *SharedCharPtr;
typedef shared [1] double *AllSharedDoublePtr;
typedef shared [1] float *AllSharedFloatPtr;
typedef shared [1] int64_t *AllSharedLongPtr;
typedef shared [1] int8_t *AllSharedInt8Ptr;
typedef shared [1] uint8_t *AllSharedUInt8Ptr;
typedef shared [1] char *AllSharedCharPtr;
typedef shared void *SharedVoidPtr; // UPC forbids blocksize in void*
typedef shared [1] SharedVoidPtr *AllSharedVoidPtr;

/* cache local to get and put remote data into and out of private variables

    shared [] type *remote;

    // only a single remote get and put will occur per iteration
    INIT_CACHE_LOCAL(type, var);
    while ( remote = getNextVar() ) {
       CACHE_LOCAL(var, remote);
       var->val = changed;
       var->val2 = changed;
       x = var->val3;
    }
    FINISH_CACHE_LOCAL(var);
    
    // only a single get
    CACHE_LOCAL_RO(type, var2, remote);
    x = var2->val;
    x2 = var2-val2;
    var2->val = new; // illegal - remote values can not change
    var2->val[3] = new; // okay and remote->val[3] will be put to
    
    // only a single get
    CACHE_LOCAL_TMP(type, var3, remote);
    x = var3->val;
    x2 = var3-val2;
    var3->val = new; // will be transient.  i.e. remote will not change
    var3->val[3] = new; // okay and remote->val[3] will be put to

*/

#define _INIT_CACHE_LOCAL(_type, _ptr) \
    /* Define variables */ \
    _type _ptr ## _obj, *_ptr = NULL; \
    _ptr = &_ptr ## _obj; \
    memset(_ptr, 0, sizeof(_type)); \

#define INIT_CACHE_LOCAL(_type, _ptr) \
    _INIT_CACHE_LOCAL(_type, _ptr); \
    shared _type *_ptr ## _shared = NULL; \

#define INIT_CACHE_LOCAL_ARRAY(_type, bs, _ptr) \
    _INIT_CACHE_LOCAL(_type, _ptr); \
    shared [bs] _type * _ptr ## _shared = NULL; \

#define CACHE_LOCAL_SHARED_PTR(_ptr) _ptr ## _shared

#define PUT_CACHE_LOCAL(_ptr, _sharedPtr) do { \
        if (_ptr ## _shared != NULL) { \
            /* put cached data to remote */ \
            *_ptr ## _shared = *_ptr; \
        } \
} while (0)

#define GET_CACHE_LOCAL(_ptr, _sharedPtr) do { \
        _ptr ## _shared = (_sharedPtr); \
        if ((_sharedPtr) == NULL) { \
            /* all subsequent calls will be to an invalid NULL pointer! */ \
            _ptr = NULL; \
        } else { \
            /* cache a copy to local object */ \
            *_ptr = *(_sharedPtr); \
        } \
} while (0)

#define CACHE_LOCAL(_ptr, _sharedPtr) do { \
        if ((_sharedPtr) != _ptr ## _shared) { \
            PUT_CACHE_LOCAL(_ptr, _sharedPtr); \
        } \
        if (_ptr ## _shared != (_sharedPtr)) { \
            GET_CACHE_LOCAL(_ptr, _sharedPtr); \
        } \
} while (0)

#define FINISH_CACHE_LOCAL(_ptr) PUT_CACHE_LOCAL(_ptr, (shared void *)NULL)

// copy the remote to private, can not modify
#define CACHE_LOCAL_RO(_type, _ptr, _sharedPtr) \
    _type _ptr ## obj = *(_sharedPtr); \
    const _type *const _ptr = &_ptr ## obj;

// copy the remote to private, modifications will be lost
#define CACHE_LOCAL_TMP(_type, _ptr, _sharedPtr) \
    _type _ptr ## obj = *(_sharedPtr); \
    _type * _ptr = &_ptr ## obj;


#ifndef ZERO_INIT_UPC_ALLOC
#define ZERO_INIT_UPC_ALLOC 1
#endif

#ifndef LOG_ALL_UPC_ALLOC
#ifdef DEBUG
#define LOG_ALL_UPC_ALLOC 1
#else
#define LOG_ALL_UPC_ALLOC 0
#endif
#endif

#ifndef PROTECT_UPC_ALLOCATIONS
#ifdef DEBUG
#define PROTECT_UPC_ALLOCATIONS
#endif
#endif

#ifndef TRACK_ALL_UPC_ALLOCATIONS
#define TRACK_ALL_UPC_ALLOCATIONS 0
#endif

#define _UPC_ALL_ALLOC_CHK(ptr, nblocks, nbytes, log) do { \
        if (((int64_t)nblocks) <= 0 || ((int64_t)nbytes) <= 0) { DIE("upc_all_alloc_chk(%llu, %llu) can not be called with <=0 allocation!\n", (llu)(nblocks), (llu)(nbytes)); } \
        if (MYTHREAD == 0 && (ptr) != NULL) { WARN("upc_all_alloc(%llu, %llu) called on pointer that already has a value!\n", (llu)(nblocks), (llu)(nbytes)); } \
        LOGF("All-allocating " #ptr " nblocks=%lld nbytes=%lld %0.3f MB (%0.3fM globally)\n", (lld) (nblocks), (lld) (nbytes), 1.0 * (nblocks) * (nbytes) / THREADS / ONE_MB, 1.0 * (nblocks) * (nbytes) / ONE_MB); \
        (ptr) = upc_all_alloc((uint64_t)nblocks, (uint64_t)nbytes); \
        if (!ptr) { WARN("Could not upc_all_alloc(%llu, %llu), %0.3f MB globally!\n", (llu)nblocks, (llu)nbytes, 1.0 * (nblocks) * (nbytes) / ONE_MB); } \
        MEMCHECK_UPCALLCOUNT(1); \
        if (ZERO_INIT_UPC_ALLOC) { \
            for (int64_t _aai = MYTHREAD; _aai < nblocks; _aai += THREADS) { assert(upc_threadof(&((ptr)[_aai])) == MYTHREAD); memset((char *)&((ptr)[_aai]), 0, nbytes); } \
            upc_barrier; \
        } \
        if (log && (_sv != NULL) && (LOG_ALL_UPC_ALLOC && MYSV.logMemcheck > 0)) { \
            LOGFN("[%s:%d] %lld upc_all_alloc_chk(%llu, %llu) %llu\n", __FILENAME__, __LINE__, (lld)MYSV.outstandingUPCAllAllocs, (llu)nblocks, (llu)nbytes, (llu)upc_addrfield(ptr)); \
        } \
        if (log && (_sv != NULL) && ((nblocks) >= THREADS)) { \
            int64_t __tmpPtrAddr = upc_addrfield(ptr); \
            if (MYSV.upc_all_alloc_watermark_first == 0) { \
                MYSV.upc_all_alloc_watermark_first = __tmpPtrAddr + (nblocks)*(nbytes)/THREADS; \
                MYSV.upc_all_alloc_watermark = MYSV.upc_all_alloc_watermark_first; \
            } \
            if (__tmpPtrAddr < MYSV.upc_all_alloc_watermark) { \
                LOGFN("[%s:%d] upc_all_alloc raised watermark by %0.3f MB + %0.3f MB to %lld: %0.3f MB\n", __FILENAME__, __LINE__, 1.0 / ONE_MB * (MYSV.upc_all_alloc_watermark - __tmpPtrAddr), 1.0/ONE_MB * ((nblocks) * (nbytes)/THREADS), (lld) __tmpPtrAddr, 1.0 / ONE_MB * ( MYSV.upc_all_alloc_watermark_first - __tmpPtrAddr)); \
                MYSV.upc_all_alloc_watermark = __tmpPtrAddr; \
            } \
        } \
} while (0)

#define _UPC_ALL_FREE_CHK(ptr, log) do { \
        if (ptr) { \
            MEMCHECK_UPCALLCOUNT(-1); \
            if (log && (_sv != NULL) && (LOG_ALL_UPC_ALLOC && MYSV.logMemcheck > 0)) { \
                LOGFN("[%s:%d] %lld upc_all_free_chk(%llu)\n", __FILENAME__, __LINE__, (lld)MYSV.outstandingUPCAllocs, (llu)upc_addrfield(ptr)); \
                if (MYSV.outstandingUPCAllAllocs < 0) { WARN("More upc_all_free_chk than upc_all_alloc: %lld\n", (lld)MYSV.outstandingUPCAllAllocs); } \
            } \
            upc_all_free(ptr); \
        } else { \
            SWARN("upc_all_free_chk called on NULL pointer: %s\n", # ptr); \
        } \
        (ptr) = NULL; \
} while (0)

#define _UPC_ALLOC_CHK(ptr, nbytes, PAD_ALLOC, log) do { \
        if ((nbytes) == 0) { LOGF("upc_alloc_chk(%llu) called with 0 allocation!\n", (llu)nbytes); (ptr) = NULL; break; } \
        int64_t testBytes = (int64_t)nbytes; \
        if (testBytes < 0) { DIE("upc_alloc_chk(%llu (%lld)) called with <0 allocation!\n", (llu)nbytes, (lld)testBytes); } \
        if ((ptr) != NULL) { WARN("upc_alloc_chk(%llu) called on pointer that already has a value!\n", ((llu)nbytes)); } \
        if (nbytes >= 64*ONE_MB) { LOGFN("[%s:%d] upc_alloc LARGE %0.3f MB\n", __FILENAME__, __LINE__, 1.0 / ONE_MB * nbytes); } \
        uint64_t __realSize = ((uint64_t)nbytes) + (PAD_ALLOC) * 2; \
        SharedCharPtr ___ptr = upc_alloc(__realSize); \
        SharedCharPtr __ptr = ___ptr; \
        if (!__ptr) { WARN("Could not upc_alloc(%llu) %0.3f MB!\n", (llu)nbytes, 1.0 * __realSize / ONE_MB); (ptr) = NULL; break; } \
        assert(upc_threadof(__ptr) == MYTHREAD); \
        if (ZERO_INIT_UPC_ALLOC) { memset((char *)__ptr, 0, __realSize); } \
        if (PAD_ALLOC) { \
            char *newPos = (char *)pad_memory_at_line((char *)__ptr, __realSize, PAD_ALLOC, __FILENAME__, __LINE__); \
            __ptr += newPos - ((char *)__ptr); \
        } \
        if (log) { MEMCHECK_UPCCOUNT(PAD_ALLOC ? __realSize : 1); } else { MEMCHECK_UPCCOUNT0(PAD_ALLOC ? __realSize : 1); } \
        if ((log) && (_sv != NULL) && (LOG_ALL_UPC_ALLOC && MYSV.logMemcheck > 0)) { \
            LOGFN("[%s:%d] %lld upc_alloc_chk(%llu) %d,%lld realPtr=%d,%lld/%p realSize=%llu PAD=%llu endMem=%p\n", __FILENAME__, __LINE__, (lld)MYSV.outstandingUPCAllocs, (llu)nbytes, (int)upc_threadof(__ptr), (lld)upc_addrfield(__ptr), (int)upc_threadof(___ptr), (lld)upc_addrfield(___ptr), (char *)___ptr, (llu)__realSize, (llu)PAD_ALLOC, ((char *)___ptr) + __realSize); \
        } \
        if (_sv != NULL) { \
            int64_t __tmpPtrAddr = upc_addrfield(__ptr);  __tmpPtrAddr += __realSize; \
            if (MYSV.upc_alloc_watermark == 0) { \
                LOGFN("[%s:%d] upc_alloc raised watermark by %0.3f MB to %lld (first)\n", __FILENAME__, __LINE__, 1.0 / ONE_MB * (nbytes/THREADS), (lld) __tmpPtrAddr); \
                MYSV.upc_alloc_watermark = __tmpPtrAddr; \
            } \
            if (__tmpPtrAddr > MYSV.upc_alloc_watermark + 1024*1024) { \
                LOGFN("[%s:%d] upc_alloc raised watermark by %0.3f MB + %0.3f MB to %lld (cumulatively %lld): %0.3f MB\n", __FILENAME__, __LINE__, 1.0 / ONE_MB * (-MYSV.upc_alloc_watermark + __tmpPtrAddr), 1.0/ONE_MB * (nbytes), (lld) __tmpPtrAddr, (lld) MYSV.upc_alloc_watermark_count,  1.0 / ONE_MB * (__tmpPtrAddr)); \
                MYSV.upc_alloc_watermark = __tmpPtrAddr; \
                MYSV.upc_alloc_watermark_count = 0; \
            } else { \
                MYSV.upc_alloc_watermark_count++; \
            } \
        } \
        (ptr) = (SharedVoidPtr)__ptr; \
} while (0)

#if (PAD_ALLOC_BYTES == 0)
// necessary to make get all compilers to work
#define PAD_ALLOC_BUF *__buf = NULL
#else
#define PAD_ALLOC_BUF __buf[PAD_ALLOC_BYTES]
#endif

#define __UPC_VALIDATE_ALLOC(__ptr, ptr, PAD_ALLOC, log) do { \
        if (ptr) { \
            assert(ptr != NULL); \
            uint64_t __s = 0; \
            char *__origPtr = NULL, *__padPtr = NULL; \
            char __msg[512]; \
            __msg[0] = 0; \
            if (upc_threadof(ptr) == MYTHREAD) { \
                __origPtr = __padPtr = (char *)(ptr); \
                assert(__origPtr != NULL); \
                if (PAD_ALLOC > 0) { \
                    __origPtr = pad_memory_check_at_line(__padPtr, __msg, PAD_ALLOC, __FILENAME__, __LINE__); \
                    __ptr = __ptr - (__padPtr - __origPtr); \
                    assert(upc_threadof(__ptr) == MYTHREAD); \
                    assert(__ptr == ((SharedCharPtr)(ptr)) - PAD_ALLOC); \
                } \
                __s = pad_memory_real_size((char *)(ptr), PAD_ALLOC); \
            } else { \
                /* check the remote thread padding */ \
                __ptr = __ptr - PAD_ALLOC; \
                if (PAD_ALLOC_BYTES > 0 && PAD_ALLOC > 0 && (int)PAD_ALLOC <= (int)PAD_ALLOC_BYTES) { \
                    char PAD_ALLOC_BUF; \
                    upc_memget(__buf, __ptr, PAD_ALLOC); \
                    int64_t __testAddr = 0, __testSize = 0; \
                    void *__testPtr = pad_memory_check_prefix_at_line(__buf, PAD_ALLOC, &__testAddr, &__testSize, __msg, __FILENAME__, __LINE__); \
                    if (__testPtr != NULL && __testSize > 0 && __testAddr != 0) { \
                        upc_memget(__buf, __ptr + __testSize - PAD_ALLOC, PAD_ALLOC); \
                        __testPtr = pad_memory_check_suffix_at_line(__buf - __testSize + PAD_ALLOC, PAD_ALLOC, &__testAddr, &__testSize, __msg, __FILENAME__, __LINE__); \
                    } else { \
                        __testPtr = NULL; \
                    } \
                    __s = __testSize; \
                    if (__testPtr == NULL || strlen(__msg)) { WARN("detected memory corruption in upc_free_chk pading (%d,%lld realPtr=%d,%lld): %s\n", (int)upc_threadof(ptr), (lld)upc_addrfield(ptr), (int)upc_threadof(__ptr), (lld)upc_addrfield(__ptr), __msg); } \
                } \
            } \
            if (log) { MEMCHECK_UPCCOUNT(-(PAD_ALLOC ? __s : 1)); } else { MEMCHECK_UPCCOUNT0(-(PAD_ALLOC ? __s : 1)); } \
            if ((log) && (_sv != NULL) && (LOG_ALL_UPC_ALLOC && MYSV.logMemcheck > 0)) { \
                LOGFN("[%s:%d] %lld %lld upc_free_chk(%d,%lld) realPtr=%d,%lld/%p PAD=%llu size=%llu%s\n", __FILENAME__, __LINE__, (lld)MYSV.outstandingUPCAllocs, (lld)MYSV.untracked_outstandingUPCAllocs, (int)upc_threadof(ptr), (lld)upc_addrfield(ptr), (int)upc_threadof(__ptr), (lld)upc_addrfield(__ptr), __origPtr, (llu)PAD_ALLOC, (llu)__s, ((upc_threadof(__ptr) == MYTHREAD) ? "" : " NOT MY THREAD")); \
            } \
        } else { \
            LOGF("UPC_VALIDATE_ALLOC called on NULL pointer: %s\n", # ptr); \
        } \
} while (0)

#define _UPC_VALIDATE_ALLOC(ptr, PAD_ALLOC) do { \
        SharedCharPtr __ptr = (SharedCharPtr)(ptr); \
        __UPC_VALIDATE_ALLOC(__ptr, ptr, PAD_ALLOC, 0); \
} while (0)

#define _UPC_FREE_CHK(ptr, PAD_ALLOC, log) do { \
        if (ptr) { \
            SharedCharPtr __ptr = (SharedCharPtr)(ptr); \
            __UPC_VALIDATE_ALLOC(__ptr, ptr, PAD_ALLOC, log); \
            upc_free(__ptr); \
            (ptr) = NULL; \
        } else { \
            LOGF("upc_free_chk called on NULL pointer: %s\n", # ptr); \
        } \
} while (0)

#define UPC_ALL_ALLOC_CHK(ptr, nBlocks, nbytes) _UPC_ALL_ALLOC_CHK(ptr, nBlocks, nbytes, 1)
#define UPC_ALL_FREE_CHK(ptr) _UPC_ALL_FREE_CHK(ptr, 1)
#define UPC_ALL_ALLOC_CHK0(ptr, nBlocks, nbytes) _UPC_ALL_ALLOC_CHK(ptr, nBlocks, nbytes, TRACK_ALL_UPC_ALLOCATIONS)
#define UPC_ALL_FREE_CHK0(ptr) _UPC_ALL_FREE_CHK(ptr, TRACK_ALL_UPC_ALLOCATIONS)

#ifndef PROTECT_UPC_ALLOCATIONS

#define UPC_ALLOC_CHK(ptr, nbytes) _UPC_ALLOC_CHK(ptr, nbytes, 0, 1)
#define UPC_ALLOC_CHK0(ptr, nbytes) _UPC_ALLOC_CHK(ptr, nbytes, 0, TRACK_ALL_UPC_ALLOCATIONS)

#define UPC_VALIDATE_ALLOC(ptr) _UPC_VALIDATE_ALLOC(ptr, 0)
#define UPC_VALIDATE_ALLOC0(ptr) _UPC_VALIDATE_ALLOC(ptr, 0)

#define UPC_FREE_CHK(ptr) _UPC_FREE_CHK(ptr, 0, 1)
#define UPC_FREE_CHK0(ptr) _UPC_FREE_CHK(ptr, 0, TRACK_ALL_UPC_ALLOCATIONS)

#else // i.e. defined PROTECT_UPC_ALLOCATIONS

#define UPC_ALLOC_CHK(ptr, nbytes) _UPC_ALLOC_CHK(ptr, nbytes, PAD_ALLOC_BYTES, 1)
#define UPC_ALLOC_CHK0(ptr, nbytes) _UPC_ALLOC_CHK(ptr, nbytes, PAD_ALLOC_BYTES, TRACK_ALL_UPC_ALLOCATIONS)

#define UPC_VALIDATE_ALLOC(ptr) _UPC_VALIDATE_ALLOC(ptr, PAD_ALLOC_BYTES)
#define UPC_VALIDATE_ALLOC0(ptr) _UPC_VALIDATE_ALLOC(ptr, PAD_ALLOC_BYTES)

#define UPC_FREE_CHK(ptr) _UPC_FREE_CHK(ptr, PAD_ALLOC_BYTES, 1)
#define UPC_FREE_CHK0(ptr) _UPC_FREE_CHK(ptr, PAD_ALLOC_BYTES, TRACK_ALL_UPC_ALLOCATIONS)

#endif // PROTECT_UPC_ALLOCATIONS

// Tracked allocations can be used to optimize the upc_free when the free will be mostly remote.
// allocations are recoreded in a Buffer and then freed in bulk
// This is an efficient work around for a bug in the inifiniband iplementation that hangs
// when many remote upc_free calls are made
#define UPC_ALLOC_TRACK(ptr, nbytes, buffer) do { \
    UPC_ALLOC_CHK0(ptr, nbytes); \
    SharedVoidPtr __ptr = ptr; \
    memcpyBuffer(buffer, &__ptr, sizeof(SharedVoidPtr)); \
} while(0)

#define UPC_FREE_TRACK(ptr) do { \
    (ptr) = NULL; \
} while(0)

#define UPC_FREE_ALL_TRACKED(buffer) do { \
    int64_t __num_allocs = getLengthBuffer(buffer) / sizeof(SharedVoidPtr); \
    SharedVoidPtr * __tmp = (SharedVoidPtr *) getStartBuffer(buffer); \
    for(int64_t __iter = 0; __iter < __num_allocs; __iter++ ) { \
        UPC_FREE_CHK0(__tmp[__iter]); \
    } \
    LOGF("Freed %lld allocations that this thread originated\n", (lld) __num_allocs); \
    resetBuffer(buffer); \
} while(0)

// generic Heap structure for distributed memory applications

typedef struct _Heap {
    int64_t       size;
    SharedCharPtr ptr;
    int64_t       position, fencePosition;
} Heap;
typedef Heap *HeapPtr;
typedef shared Heap *SharedHeapPtr;

#define USE_NEW_HEAP 1

#if USE_NEW_HEAP

#define initHeap(heapPtr, bytes) do { \
        Heap heap; \
        SharedCharPtr tmp = NULL; \
        UPC_ALLOC_CHK0(tmp, bytes); \
        heap.ptr = tmp; \
        heap.position = 0; \
        heap.fencePosition = 0; \
        heap.size = bytes; \
        *(heapPtr) = heap; \
        DBG2("initHeap(%llu): %s at %llu\n", (llu)(bytes), # heapPtr, (llu)(upc_addrfield(tmp))); \
} while (0)


#define freeHeap(heapPtr) do { \
        if ((heapPtr) != NULL) { \
            SharedCharPtr tmp = (heapPtr)->ptr; \
            DBG2("freeHeap(%s)\n", # heapPtr); \
            Heap empty; empty.ptr = NULL; empty.size = 0; empty.position = 0; empty.fencePosition = 0; \
            *(heapPtr) = empty; \
            if (tmp != NULL) { \
                UPC_FREE_CHK0(tmp); \
            } \
        } else { \
            WARN("Attempt to freeHeap of null ptr: %s\n", # heapPtr); \
        } \
} while (0)

// appends heap, potentially extending the size ***UNSAFE if multiple threads will access this data****
// returns a pointer to the newly copied data.
// Any pointer returned may become invalid upon the next operation, though the data will be preserved
SharedCharPtr _appendHeap(HeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line);
#define appendHeap(heap, data, length) _appendHeap((HeapPtr) & (heap), (const char *)data, length, __FILE__, __LINE__)

// Threadsafe version of _appendHeap
SharedCharPtr _safeAppendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line);
#define safeAppendSharedHeap(heap, data, length) _safeAppendSharedHeap((SharedHeapPtr) & (heap), (const char *)data, length, __FILE__, __LINE__)

// appends heap, potentially extending the size *** UNSAFE if multiple threads will access this data concurrently ***
// returns a pointer to the newly copied data.
SharedCharPtr _appendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line);
#define appendSharedHeap(heap, data, length) _appendSharedHeap((SharedHeapPtr) & (heap), (const char *)data, length, __FILE__, __LINE__)

#define LOCALIZE_HEAP(localPtr, remoteHeap, len) do { \
        Heap __lcopy = remoteHeap; \
        len = __lcopy.position; \
        if (len > __lcopy.size) { DIE("LOCALIZE_HEAP detected a heap that exceeds its size: .position=%lld .size==%lld\n", (lld)__lcopy.position, (lld)__lcopy.size); } \
        localPtr = realloc_chk(localPtr, len + 1); \
        if (len > 0) { \
            upc_memget((char *)localPtr, __lcopy.ptr, len); \
        } \
        ((char *)localPtr)[len] = 0; \
} while (0)

#endif // USE_NEW_HEAP

typedef struct _HeapList HeapList;
typedef HeapList *HeapListPtr;
typedef shared [] HeapList *SharedHeapListPtr;
struct _HeapList {
    int64_t           capacity;
    int64_t           size;
    SharedHeapListPtr next;
    char              _data[8]; // potentially much larger after upc_alloc
};

#define initHeapList(sharedHeapListPtr, bytes) do { \
        UPC_ALLOC_CHK(sharedHeapListPtr, sizeof(HeapList) + bytes); \
        memset((HeapList *)sharedHeapListPtr, 0, sizeof(HeapList)); \
        sharedHeapListPtr->capacity = bytes; \
        DBG("initHeapList(%llu): %s at %llu\n", (llu)(bytes), # sharedHeapListPtr, (llu)(upc_addrfield(sharedHeapListPtr))); \
        assert(sharedHeapListPtr->size == 0); \
        assert(sharedHeapListPtr->next == NULL); \
} while (0)

#define freeHeapList(sharedHeapListPtr) do { \
        SharedHeapListPtr head = sharedHeapListPtr; \
        while (head != NULL) { \
            SharedHeapListPtr next = head->next; \
            DBG("freeHeapList(%s at %llu)\n", # sharedHeapListPtr, (llu)upc_addrfield(sharedHeapListPtr)); \
            UPC_FREE_CHK(head); \
            head = next; \
        } \
        sharedHeapListPtr = NULL; \
} while (0)

void _extendHeapList(SharedHeapListPtr *ptr, uint64_t bytes);

SharedCharPtr _allocFromHeapList(SharedHeapListPtr *ptr, uint64_t bytes);
#define allocFromHeapList(sharedHeapListPtr, bytes) _allocFromHeapList(&(sharedHeapListPtr), bytes)


/* GlobalFinished
 * a two step barrier that can be set with incrementGlobalFinished
 * and checked periodically with isGlobalFinished()
 * it lightly uses atomics and is efficient both intra and inter node
 */
typedef int32_t RankCount;
typedef struct {
    shared [1] RankCount * globalFinished;
    RankCount coresPerNode;
    double    startTime;
    int       allAreFinished;
    int       extraIterations;
} _GlobalFinished;
typedef _GlobalFinished *GlobalFinished;

GlobalFinished initGlobalFinished(int coresPerNode);

void freeGlobalFinished(GlobalFinished *gf);

// just increments. explicitly does not return the count -- use isGlobalFinished for checking
void incrementGlobalFinished(GlobalFinished gf);

// only trust this number if it == THREADS
RankCount getGlobalFinishedCountFast(GlobalFinished gf);

// return 0 when all threads on a node have completed
int16_t getThreadsRunningOnNode(GlobalFinished gf, int thread);

// Use this for an approximation of the number or threads that are finished
RankCount getGlobalFinishedCountSlow(GlobalFinished gf);

char isGlobalFinished(GlobalFinished gf);

#endif // UPC_COMMON_H
