#ifndef __DEFINES_H_
#define __DEFINES_H_

#include <stdlib.h>
#include <limits.h>

typedef long long int lld;
typedef unsigned long long int llu;

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define EXIT_FUNC(x) do { common_exit(x); } while (0)
#ifndef  __UPC_VERSION__
#define MYTHREAD get_rank()
#define THREADS get_num_ranks()
#endif

#ifndef BS
#define BS 1
#endif

#ifndef DEFAULT_READ_LEN
#define DEFAULT_READ_LEN 1024
#endif

/* Maximum number of alignments is read_length - kmer_length + 1 */
/* For human short reads 101bp long and k=51 MAX_ALIGN = 51  */
/* But this could be a lot bigger too, so we make it large just to be safe */
#ifndef MAX_ALIGN
#define MAX_ALIGN 500
#endif

#define GET_KMER_PACKED_LEN(k) ((k + 3) / 4)

#ifndef ONE_KB
#define ONE_KB 1024LL
#define ONE_MB 1048576LL
#define ONE_GB 1073741824LL
#define PER_KB (1.0 / ONE_KB)
#define PER_MB (1.0 / ONE_MB)
#define PER_GB (1.0 / ONE_GB)
#endif

#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

#ifndef TARGET_CHUNK_PER_THREAD
#define TARGET_CHUNK_PER_THREAD 8192LL
#endif

#ifndef MIN_CHUNK_SIZE
#define MIN_CHUNK_SIZE 4LL
#endif

#ifndef MAX_CHUNK_MEMORY_PER_NODE
#define MAX_CHUNK_MEMORY_PER_NODE (1024 * ONE_MB)
#endif

#ifndef MAX_MER_SIZES
#define MAX_MER_SIZES 16
#endif

#ifndef MAX_LIBRARIES
#define MAX_LIBRARIES 256
#endif

#ifndef LIB_NAME_LEN
#define LIB_NAME_LEN 15
#endif

#ifndef MAX_USER_LIB_NAME_LEN
#define MAX_USER_LIB_NAME_LEN (LIB_NAME_LEN - 3)
#endif

#ifndef MAX_FILE_PATH
#define MAX_FILE_PATH 384
#endif

#ifndef MAX_READ_NAME_LEN
#define MAX_READ_NAME_LEN 14
#endif

#ifndef MAX_EFFECTIVE_CONTIG_LENGTH
#define MAX_EFFECTIVE_CONTIG_LENGTH 1024
#endif

#endif /* __DEFINES_H_ */
