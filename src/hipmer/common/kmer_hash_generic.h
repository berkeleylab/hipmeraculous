#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <assert.h>

#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"
#include "meraculous.h"

#ifndef CONTIGS_DDS_TYPE
#error "You must specify CONTIGS_DDS_TYPE before including this file"
#endif

#include "../contigs/kmer_handling.h"
#include "../contigs/packingDNAseq.h"

#include "hashtable_dds.h"

#define FREE_MEMORY_HEAP JOIN(CONTIGS_DDS_TYPE, free_memory_heap)
#define CREATE_HASH_TABLE JOIN(CONTIGS_DDS_TYPE, create_hash_table)
#define DESTROY_HASH_TABLE JOIN(CONTIGS_DDS_TYPE, destroy_hash_table)
#define LOOKUP_KMER_AND_COPY JOIN(CONTIGS_DDS_TYPE, lookup_kmer_and_copy)
#define LOOKUP_KMER JOIN(CONTIGS_DDS_TYPE, lookup_kmer)
#define LOOKUP_LEAST_KMER_AND_COPY JOIN(CONTIGS_DDS_TYPE, lookup_least_kmer_and_copy)
#define LOOKUP_LEAST_KMER JOIN(CONTIGS_DDS_TYPE, lookup_least_kmer)
#define SET_EXT_OF_KMER JOIN(CONTIGS_DDS_TYPE, set_ext_of_kmer)
#define LOOKUP_AND_GET_EXT_OF_KMER JOIN(CONTIGS_DDS_TYPE, lookkup_and_get_ext_of_kmer)
#define ADD_KMER JOIN(CONTIGS_DDS_TYPE, add_kmer)
#define ADD_TO_HEAP JOIN(CONTIGS_DDS_TYPE, push_to_heap)
#define FLUSH_HEAP JOIN(CONTIGS_DDS_TYPE, flush_heap)
#define FINAL_FLUSH_HEAP JOIN(CONTIGS_DDS_TYPE, final_flush_heap)
#define FREE_LOCAL_BUFFERS JOIN(CONTIGS_DDS_TYPE, free_local_buffers)

void ADD_TO_HEAP(MEMORY_HEAP_T *memory_heap, LIST_T data, int destThread);
void FLUSH_HEAP(MEMORY_HEAP_T *memory_heap, LIST_T *data, int64_t *len, int destThread);
void FINAL_FLUSH_HEAP(MEMORY_HEAP_T *memory_heap);
void FREE_LOCAL_BUFFERS(MEMORY_HEAP_T *memory_heap);


#ifndef COLL_ALLOC

void FREE_MEMORY_HEAP(MEMORY_HEAP_T *memory_heap);

/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *CREATE_HASH_TABLE(int64_t size, MEMORY_HEAP_T *memory_heap, int64_t my_heap_size);

void DESTROY_HASH_TABLE(HASH_TABLE_T **_ht, MEMORY_HEAP_T *_memory_heap);

#endif // ndef COL_ALOC

#ifdef COLL_ALLOC
/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *CREATE_HASH_TABLE(int64_t size, MEMORY_HEAP_T *memory_heap);

#endif // def COLL_ALLOC

/* Use this lookup function when no writes take place in the distributed hashtable */
shared[] LIST_T *LOOKUP_KMER_AND_COPY(HASH_TABLE_T *hashtable, const unsigned char *kmer, LIST_T *cached_copy, int kmer_len);

shared[] LIST_T *LOOKUP_KMER(HASH_TABLE_T *hashtable, const unsigned char *kmer, int kmer_len);

/* find the entry for this kmer or reverse complement */
shared[] LIST_T *LOOKUP_LEAST_KMER_AND_COPY(HASH_TABLE_T *dist_hashtable, const char *next_kmer, LIST_T *cached_copy, int *is_least, int kmer_len);

shared[] LIST_T *LOOKUP_LEAST_KMER(HASH_TABLE_T *dist_hashtable, const char *next_kmer, int *was_least, int kmer_len);

void SET_EXT_OF_KMER(LIST_T *lookup_res, int is_least, char *new_seed_le, char *new_seed_re);

/* find the entry for this kmer or reverse complement, set the left and right extensions */
shared[] LIST_T *LOOKUP_AND_GET_EXT_OF_KMER(HASH_TABLE_T *dist_hashtable, const char *next_kmer, char *new_seed_le, char *new_seed_re, int kmer_len);

#ifndef AGGREGATE_STORES
int ADD_KMER(HASH_TABLE_T *hashtable, unsigned char *kmer, unsigned char *extensions, MEMORY_HEAP_T *memory_heap, int64_t *ptrs_to_stack, int64_t *ptrs_to_stack_limits, int CHUNK_SIZE, int kmer_len);
#endif // AGGREGATE_STORES

#ifdef AGGREGATE_STORES
int ADD_KMER(HASH_TABLE_T *hashtable, unsigned char *kmer, unsigned char *extensions, MEMORY_HEAP_T *memory_heap, int64_t *local_buffs_hashvals, LIST_T *local_buffs, int64_t *local_index, int CHUNK_SIZE, int kmer_len);
#endif // AGGREGATE_STORES
