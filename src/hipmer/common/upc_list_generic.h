// UPC_LIST_GENERIC_H can be loaded multiple times with different UPC_LIST_TYPE macros

#include <upc.h>
#include <string.h>
#include <assert.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"

#ifndef UPC_LIST_TYPE
#error "You must specify UPC_LIST_TYPE before including this file"
#endif

#ifndef UPC_LIST_NAME
#error "You must specify UPC_LIST_NAME before including this file"
#endif

#define LIST_T                  JOIN(UPC_LIST_NAME, list_t)
#define LIST_T_SHPTR            JOIN(UPC_LIST_NAME, list_t_shptr)
#define LIST_T_ALL_SHPTR        JOIN(UPC_LIST_NAME, list_t_allshptr)
#define LIST_T_HEAD             JOIN(UPC_LIST_NAME, list_t_head)
#define LIST_T_HEAD_SHPTR       JOIN(UPC_LIST_NAME, list_t_head_ptr)
#define LIST_T_HEAD_ALL_SHPTR   JOIN(UPC_LIST_NAME, list_t_all_head_ptr)

#define ALLOC_NODE      JOIN(UPC_LIST_NAME, allocNode)
#define FREE_NODE       JOIN(UPC_LIST_NAME, freeNode)
#define FREE_LIST       JOIN(UPC_LIST_NAME, freeList)
#define PUSH_NODE_FRONT JOIN(UPC_LIST_NAME, pushNodeFront)
#define POP_NODE_FRONT  JOIN(UPC_LIST_NAME, popNodeFront)


typedef struct LIST_T LIST_T; // forward decl
typedef shared []  LIST_T *LIST_T_SHPTR;     // shared pointer to LIST_T
typedef shared [1] LIST_T *LIST_T_ALL_SHPTR; // all shared pointer to LIST_T
struct LIST_T {
    LIST_T_SHPTR next;
    UPC_LIST_TYPE data;
};
typedef LIST_T_SHPTR LIST_T_HEAD;
typedef shared []  LIST_T_HEAD *LIST_T_HEAD_SHPTR;     // shared pointer to shared pointer to LIST_T
typedef shared [1] LIST_T_HEAD *LIST_T_HEAD_ALL_SHPTR; // all shared pointer to shared pointer to LIST_T

// allocate a new node with next == NULL
LIST_T_SHPTR ALLOC_NODE(const UPC_LIST_TYPE data);

// free memory for this single node.  No checks against the value of next
void FREE_NODE(LIST_T_SHPTR node);

// free all nodes in the list and sets NULL into head
// returns the count that was freed
size_t FREE_LIST(LIST_T_HEAD_SHPTR head);

void PUSH_NODE_FRONT(LIST_T_HEAD_SHPTR head, LIST_T_SHPTR node);

LIST_T_SHPTR POP_NODE_FRONT(LIST_T_HEAD_SHPTR head);

