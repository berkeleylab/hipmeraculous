#include "UU_traversal_generic.h"

#ifdef MARK_POS_IN_CONTIG
/* Stores the exact position of each kmer in the eventual contig */
int MARK_POSITION_IN_CONTIG(contig_ptr_t in_contig, HASH_TABLE_T *hashtable, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    assert(upc_threadof(in_contig) == MYTHREAD);
    contig_t *cur_contig = (contig_t *)in_contig;
    int i;
    int contig_length = cur_contig->end - cur_contig->start + 1;
    char *contig = (char *)(cur_contig->sequence + cur_contig->start);
    char cur_kmer[MAX_KMER_SIZE], auxiliary_kmer[MAX_KMER_SIZE];
    char *kmer_to_search;
    auxiliary_kmer[kmer_len] = '\0';
    cur_kmer[kmer_len] = '\0';
    shared[] LIST_T * lookup_res;

    for (i = 0; i <= contig_length - kmer_len; i++) {
        memcpy(cur_kmer, contig + i, kmer_len * sizeof(char));
        kmer_to_search = getLeastKmer(curr_kmer, auxiliary_kmer);
        lookup_res = LOOKUP_KMER(hashtable, (unsigned char *)kmer_to_search, kmer_len);

        /* Mark the k-mer with the position in the contig */
        lookup_res->posInContig = i;
        // USE ATOMICS: lookup_res->my_contig = cur_contig->myself;
        shared[] contig_ptr_box_list_t * myContig = NULL;
        UPC_ATOMIC_GET_SHPTR(&myContig, &(cur_contig->myself));
        UPC_ATOMIC_SET_SHPTR(NULL, &(lookup_res->my_contig), myContig);
    }

    upc_fence;
    return 0;
}
#endif

/* unpacks left_ext + kmer + right_ext into a single contiguous string of length kmer_len+2 */
void UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int kmer_len)
{
    assert(new_kmer_seed_ptr != NULL);

    unpackSequence(new_kmer_seed_ptr->packed_key, (unsigned char *)getKmer(kmer_and_ext), kmer_len);
#ifdef MERACULOUS
    convertPackedCodeToExtension(new_kmer_seed_ptr->packed_extensions, getLeftExt(kmer_and_ext), getRightExt(kmer_and_ext, kmer_len));
#endif
    setEnd(kmer_and_ext, kmer_len);
}

void UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int is_least, int kmer_len)
{
    UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(new_kmer_seed_ptr, kmer_and_ext, kmer_len);
    if (!is_least) {
        reverseComplementINPLACE(getLeftKmer(kmer_and_ext), kmer_len + 2);
    }
}

void UNPACK_KMER_AND_EXTENSIONS(shared[] LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int kmer_len)
{
    LIST_T copy;

    assert(new_kmer_seed_ptr != NULL);
    copy = *new_kmer_seed_ptr;
    UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(&copy, kmer_and_ext, kmer_len);
}


/* TODO: Add function that computes hash using the packed form */


shared[] LIST_T *GET_NEXT_HASH_TABLE_ENTRY(HASH_TABLE_T *hashtable, int64_t *seed_index, shared[] LIST_T *old_kmer_seed_ptr, int kmer_len)
{
    /***********************************************************************************************/
    /* Pick a kmer as seed from local kmers' buckets. We assume cyclic distribution of the buckets */
    /***********************************************************************************************/
    assert(upc_threadof(&(hashtable->table[*seed_index])) == MYTHREAD);
    assert(*seed_index < hashtable->size);

    if (old_kmer_seed_ptr != NULL && HIPMER_VERBOSITY >= LL_DBG3) {
        kmer_and_ext_t kmer_and_ext;
        UNPACK_KMER_AND_EXTENSIONS(old_kmer_seed_ptr, &kmer_and_ext, kmer_len);
        long long ll_seed_index_ptr = *seed_index;
        DBG2("GET_NEXT_HASH_TABLE_ENTRY(%lld): starting with %s %d\n", ll_seed_index_ptr, getLeftKmer(&kmer_and_ext), (int)old_kmer_seed_ptr->used_flag);
    }

    shared[] LIST_T * new_kmer_seed_ptr = old_kmer_seed_ptr;

    if (new_kmer_seed_ptr == NULL) {
        new_kmer_seed_ptr = hashtable->table[*seed_index].head;
    } else {
        new_kmer_seed_ptr = new_kmer_seed_ptr->next;
    }

    while (new_kmer_seed_ptr == NULL && (*seed_index + THREADS) < hashtable->size) {
        *seed_index += THREADS;
        new_kmer_seed_ptr = hashtable->table[*seed_index].head;
    }

    if (HIPMER_VERBOSITY >= LL_DBG3) {
        if (new_kmer_seed_ptr != NULL) {
            kmer_and_ext_t kmer_and_ext;
            UNPACK_KMER_AND_EXTENSIONS(new_kmer_seed_ptr, &kmer_and_ext, kmer_len);
            long long ll_seed_index_ptr = *seed_index;
            DBG2("GET_NEXT_HASH_TABLE_ENTRY(%lld): ending with %s %d\n", ll_seed_index_ptr, getLeftKmer(&kmer_and_ext), (int)new_kmer_seed_ptr->used_flag);
        } else {
            long long ll_seed_index_ptr = *seed_index;
            DBG2("GET_NEXT_HASH_TABLE_ENTRY(%lld): reached the end\n", ll_seed_index_ptr);
        }
    }
    assert(new_kmer_seed_ptr == NULL || upc_threadof(new_kmer_seed_ptr) == MYTHREAD);

    return new_kmer_seed_ptr;
}


int IS_LEFT_KMER_UU(HASH_TABLE_T *hashtable, kmer_and_ext_t *kmer_and_ext, int kmer_len)
{
    char seed_le, seed_re;

    shared[] LIST_T * lookup_res;
    if (!isACGT(*getLeftExt(kmer_and_ext))) {
        DBG("Thread %d: IS_LEFT_KMER_UU(%s): not unique left ext\n", MYTHREAD, getLeftKmer(kmer_and_ext));
        return 0;
    }
    lookup_res = LOOKUP_AND_GET_EXT_OF_KMER(hashtable, getLeftKmer(kmer_and_ext), &seed_le, &seed_re, kmer_len);
    if (lookup_res == NULL) {
        DBG("Thread %d: IS_LEFT_KMER_UU(%s): no left kmer. okay\n", MYTHREAD, getLeftKmer(kmer_and_ext));
        return 1;
    } else if (seed_re != getKmer(kmer_and_ext)[kmer_len - 1] || !isACGT(seed_re)) {
        DBG("Thread %d: IS_LEFT_KMER_UU(%s): invalid seed_re (%c,%c) exp: %c\n", MYTHREAD, getLeftKmer(kmer_and_ext), seed_le, seed_re, getKmer(kmer_and_ext)[kmer_len - 1]);
        return 0;
    }
    return 1;
}

/* expects kmer_len+2 string kmer_and_ext: le-kmer-re */
int IS_RIGHT_KMER_UU(HASH_TABLE_T *hashtable, kmer_and_ext_t *kmer_and_ext, int kmer_len)
{
    char seed_le, seed_re;

    shared[] LIST_T * lookup_res;
    if (!isACGT(*getRightExt(kmer_and_ext, kmer_len))) {
        DBG("Thread %d: IS_RIGHT_KMER_UU(%s): not unique right ext\n", MYTHREAD, getLeftKmer(kmer_and_ext));
        return 0;
    }
    lookup_res = LOOKUP_AND_GET_EXT_OF_KMER(hashtable, getRightKmer(kmer_and_ext), &seed_le, &seed_re, kmer_len);
    if (lookup_res == NULL) {
        DBG("Thread %d: IS_RIGHT_KMER_UU(%s): no right kmer. okay\n", MYTHREAD, getLeftKmer(kmer_and_ext));
        return 1;
    } else if (seed_le != getKmer(kmer_and_ext)[0] || !isACGT(seed_le)) {
        DBG("Thread %d: IS_RIGHT_KMER_UU(%s): invalid seed_le (%c,%c) exp: %c\n", MYTHREAD, getLeftKmer(kmer_and_ext), seed_le, seed_re, getKmer(kmer_and_ext)[0]);
        return 0;
    }
    return 1;
}

int IS_KMER_UU_LOCAL_COPY(HASH_TABLE_T *hashtable, LIST_T *copy, char *new_seed_le, char *new_seed_re, int kmer_len)
{
    kmer_and_ext_t kmer_and_ext;

    UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(copy, &kmer_and_ext, kmer_len);
    *new_seed_le = *getLeftExt(&kmer_and_ext);
    *new_seed_re = *getRightExt(&kmer_and_ext, kmer_len);
    return IS_LEFT_KMER_UU(hashtable, &kmer_and_ext, kmer_len) && IS_RIGHT_KMER_UU(hashtable, &kmer_and_ext, kmer_len);
}

int IS_KMER_UU(HASH_TABLE_T *hashtable, shared[] LIST_T *new_kmer_seed_ptr, char *new_seed_le, char *new_seed_re, int kmer_len)
{
    LIST_T copy = *new_kmer_seed_ptr;

    return IS_KMER_UU_LOCAL_COPY(hashtable, &copy, new_seed_le, new_seed_re, kmer_len);
}

shared[] LIST_T *GET_NEXT_UNUSED_UU_KMER(HASH_TABLE_T *hashtable, int64_t *seed_index, shared[] LIST_T *new_kmer_seed_ptr, char *new_seed_le, char *new_seed_re, int kmer_len)
{
    kmer_and_ext_t kmer_and_ext;
    LIST_T copy;

    shared[] LIST_T * lookup_res = NULL;
    UPC_ATOMIC_MIN_T seed_used_flag = USED;
    UPC_ATOMIC_MIN_T accumulator;

    while (seed_used_flag == USED) {
        new_kmer_seed_ptr = GET_NEXT_HASH_TABLE_ENTRY(hashtable, seed_index, new_kmer_seed_ptr, kmer_len);

        if (new_kmer_seed_ptr == NULL) {
            break;
        }

        copy = *new_kmer_seed_ptr;
        //seed_used_flag = copy.used_flag;
        seed_used_flag = (copy.used_flag > 0) ? USED : UNUSED;

        if (seed_used_flag == USED) {
            if (HIPMER_VERBOSITY >= LL_DBG2) {
                UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(&copy, &kmer_and_ext, kmer_len);
                DBG2("GET_NEXT_UNUSED_UU_KMER already used kmer_and_ext: %s\n", getLeftKmer(&kmer_and_ext));
            }
            continue;
        }

        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(&copy, &kmer_and_ext, kmer_len);
        *new_seed_le = *getLeftExt(&kmer_and_ext);
        *new_seed_re = *getRightExt(&kmer_and_ext, kmer_len);

        /* Do not use as seed unless both left and rigth kmers exist and agree */
        if (!(IS_LEFT_KMER_UU(hashtable, &kmer_and_ext, kmer_len) && IS_RIGHT_KMER_UU(hashtable, &kmer_and_ext, kmer_len))) {
            DBG("Thread %d: kmer is not UU to the left or right: %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            seed_used_flag = USED;
        }

        /*********************************************************************************************/
        /* If seed_used_flag == UNUSED thus far then the seed is valid and we should try to visit it */
        /*********************************************************************************************/

        if (seed_used_flag == UNUSED) {
#ifndef USE_CSWAP_FOR_USED
            UPC_ATOMIC_FADD_MIN_T(&accumulator, &(new_kmer_seed_ptr->used_flag), 1);
#else
            UPC_ATOMIC_CSWAP_MIN_T(&accumulator, &(new_kmer_seed_ptr->used_flag), 0, 1);
#endif
            seed_used_flag = (accumulator == 0) ? UNUSED : USED;
        }
        if (HIPMER_VERBOSITY >= LL_DBG) {
            if (seed_used_flag == UNUSED) {
                DBG("Thread %d: GET_NEXT_UNUSED_UU_KMER claimed kmer_and_ext: %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            } else {
                DBG("Thread %d: GET_NEXT_UNUSED_UU_KMER was just used! kmer_and_ext: %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            }
        }
        upc_fence;
    }
    return new_kmer_seed_ptr;
}



/* Performs the right walk on the UU-graph traversal */
int WALK_RIGHT(HASH_TABLE_T *dist_hashtable, contig_ptr_t cur_contig, char seed_re, char *last_right_extension_found, contig_ptr_t *right_contig, int kmer_len)
{
    kmer_and_ext_t kmer_and_ext;

    shared[] LIST_T * lookup_res = NULL, *right_kmer_ptr = NULL;
    LIST_T copy, right_copy;
    int is_least, is_right_least;
    UPC_ATOMIC_MIN_T kmer_used_flag;
    UPC_ATOMIC_MIN_T accumulator;
    char new_seed_re, new_seed_le, right_le, right_re;
    int result;
    char cur_base, *tmpKmer;
    shared[] contig_ptr_box_list_t * contig_ptr_box = NULL;
    UPC_ATOMIC_GET_SHPTR(&contig_ptr_box, &(cur_contig->myself));

    kmer_used_flag = UNUSED;
    result = UNFINISHED;
    cur_base = '\0';

    assert(upc_threadof(cur_contig) == MYTHREAD);
    assert(cur_contig->end - cur_contig->start + 1 >= kmer_len);
    assert(upc_threadof(cur_contig->sequence) == MYTHREAD);

    // get the last kmer
    tmpKmer = (char *)&(cur_contig->sequence[cur_contig->end - kmer_len + 1]);
    lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, tmpKmer, &copy, &is_least, kmer_len);
#ifdef LHS_PERF
    lookups++;
    if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(lookup_res) / MYSV.cores_per_node)) {
        success++;
    }
#endif
    assert(lookup_res != NULL);
    //assert(copy.used_flag == USED);
    assert(copy.used_flag > 0);
    assert(copy.my_contig != NULL);
    remote_assert(lookup_res->my_contig->contig == cur_contig);

    UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(&copy, &kmer_and_ext, is_least, kmer_len);
    DBG2("Thread %d: WALK_RIGHT: Starting %.*s ext %s with %c.  kmer: %.*s is_least: %d\n", MYTHREAD, kmer_len, (char *)&(cur_contig->sequence[cur_contig->start]), getLeftKmer(&kmer_and_ext), seed_re, kmer_len, tmpKmer, is_least);
    assert(getKmer(&kmer_and_ext)[kmer_len - 1] == cur_contig->sequence[cur_contig->end]);
//	assert( *getRightExt(&kmer_and_ext) == seed_re );

    // get the next kmer to the right
    lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, getRightKmer(&kmer_and_ext), &copy, &is_least, kmer_len);
#ifdef LHS_PERF
    lookups++;
    if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(lookup_res) / MYSV.cores_per_node)) {
        success++;
    }
#endif
    if (lookup_res == NULL) {
        DBG("Thread %d: no right kmer for %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
        return FINISHED;
    }

    while (kmer_used_flag == UNUSED && result == UNFINISHED) {
        assert(isACGT(seed_re));
        assert(lookup_res != NULL);
        right_kmer_ptr = NULL;
        tmpKmer = (char *)&(cur_contig->sequence[cur_contig->end - kmer_len + 1]);

        UPC_POLL;

        /* build the sequence */
        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(&copy, &kmer_and_ext, is_least, kmer_len);
        assert(getKmer(&kmer_and_ext)[kmer_len - 2] == cur_contig->sequence[cur_contig->end]);
        assert(getKmer(&kmer_and_ext)[kmer_len - 1] == seed_re);
        new_seed_le = *getLeftExt(&kmer_and_ext);
        new_seed_re = *getRightExt(&kmer_and_ext, kmer_len);

        /*
         * If the new kmer's left extension is not unique
         * or left extension is not compatible with the current contig,
         * do not visit */
        if (!isACGT(new_seed_le)
            || new_seed_le != cur_contig->sequence[cur_contig->end - kmer_len + 1]) {
            DBG("Thread %d: WALK_RIGHT finished. %s right has invalid extensions le (%c) l:%c r:%c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), cur_contig->sequence[cur_contig->end - kmer_len + 1], new_seed_le, new_seed_re);
            // Done with extension
            return FINISHED;
        }
        assert(memcmp(getLeftKmer(&kmer_and_ext), tmpKmer, kmer_len) == 0);

        // good so far...
        // build and find the next kmer, check for UU
        right_kmer_ptr = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, getRightKmer(&kmer_and_ext), &right_copy, &is_right_least, kmer_len);
#ifdef LHS_PERF
        lookups++;
        if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(right_kmer_ptr) / MYSV.cores_per_node)) {
            success++;
        }
#endif
        if (right_kmer_ptr == NULL) {
            DBG("Thread %d: WALK_RIGHT finished. no right kmer %s, adding last base %c\n", MYTHREAD, getRightKmer(&kmer_and_ext), new_seed_re);
            // terminate walk, but add the last base, after locking kmer
            result = FINISHED;
        } else {
            /* get and validate the next kmer's extensions */
            if (is_right_least) {
                convertPackedCodeToExtension(right_copy.packed_extensions, &right_le, &right_re);
            } else {
                convertPackedCodeToExtension(right_copy.packed_extensions, &right_re, &right_le);
                reverseComplementBase(&right_le);
                reverseComplementBase(&right_re);
            }

            if (!isACGT(right_le) || right_le != getKmer(&kmer_and_ext)[0]) {
                DBG("Thread %d: WALK_RIGHT finished. right ext kmer (%s) is not UU. exp_le:%c l:%c r:%c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), getKmer(&kmer_and_ext)[0], new_seed_le, new_seed_re);
                // terminate walk, do not add the last base
                return FINISHED;
            }

            if (!isACGT(right_re)) {
                DBG("Thread %d: WALK_RIGHT finished. right kmer %s, has invalid right ext.  adding last base %c\n", MYTHREAD, getRightKmer(&kmer_and_ext), new_seed_re);
                // terminate walk, but add the last base, after locking kmer
                result = FINISHED;
            }
        }

        //kmer_used_flag = copy.used_flag;
        kmer_used_flag = (copy.used_flag > 0) ? USED : UNUSED;

        if (kmer_used_flag == USED) {
            assert(IS_VALID_UPC_PTR(copy.my_contig));
            DBG("Thread %d: WALK_RIGHT kmer %s is used by %lld!\n", MYTHREAD, getLeftKmer(&kmer_and_ext), (lld)(copy.my_contig == NULL ? -1 : (copy.my_contig->contig == NULL ? -2 : IS_VALID_UPC_PTR(copy.my_contig->contig) ? copy.my_contig->contig->contig_id : -3)));
            break;
        }

        /* Now attempt to lock this kmer as used by this contig and add the base */
#ifndef USE_CSWAP_FOR_USED
        UPC_ATOMIC_FADD_MIN_T(&accumulator, &(lookup_res->used_flag), 1);
#else
        UPC_ATOMIC_CSWAP_MIN_T(&accumulator, &(lookup_res->used_flag), 0, 1);
#endif
        kmer_used_flag = (accumulator == 0) ? UNUSED : USED;

        if (kmer_used_flag != UNUSED) {
            DBG("Thread %d: WALK_RIGHT kmer %s is now used! ... expecting a lock in my future\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            break;
        } else {
            DBG2("Thread %d: WALK_RIGHT flaged kmer %.*s as used\n", MYTHREAD, kmer_len, getLeftKmer(&kmer_and_ext));
        }

        /* Fix the kmer to point to current contig box */
        assert(upc_threadof(contig_ptr_box) == MYTHREAD);
        assert(contig_ptr_box->contig != NULL);
        assert(upc_threadof(contig_ptr_box->contig) == MYTHREAD);
        assert(contig_ptr_box->contig->state == ACTIVE);
        assert(contig_ptr_box->next == NULL);
        assert(copy.my_contig == NULL);
        remote_assert(lookup_res->my_contig == NULL);
        // EGEOR: Atomic write of lookup_res->my_contig
        UPC_ATOMIC_SET_SHPTR(NULL, &(lookup_res->my_contig), contig_ptr_box);

        /* Add the next base */
        reallocateContigIfNeeded(cur_contig);
        cur_contig->end++;

        /* Cur_base is the base to be added in the contig */
        cur_base = seed_re;
        /* Seed_re should be used to extend the contig with a right walk */
        seed_re = new_seed_re;
        cur_contig->sequence[cur_contig->end] = cur_base;
        cur_contig->sequence[cur_contig->end + 1] = '\0';

        DBG2("WALK_RIGHT %s added %c to %lld\n", getLeftKmer(&kmer_and_ext), cur_base, (lld)cur_contig->contig_id);

        upc_fence;

        if (result == FINISHED) {
            break;
        }

        remote_assert(IS_RIGHT_KMER_UU(dist_hashtable, &kmer_and_ext));

        // shift for the next kmer
        lookup_res = right_kmer_ptr;
        copy = right_copy;
        is_least = is_right_least;

#ifdef PROFILE
        bases_added++;
#endif
    }

    /* Return pointer to the right contig as well (needed for the synchronization protocol) */
    if (kmer_used_flag == USED) {
        DBG("Thread %d: WALK_RIGHT Kmer is used %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
        (*right_contig) = GET_CONTIG_OF_KMER(lookup_res, 0);
        if (result == FINISHED) {
            // the last base was not added because the kmer was not acquired
            result = UNFINISHED;
        }
    }

    // disallow USED & FINSHED
    assert(kmer_used_flag == UNUSED || result == UNFINISHED);

    (*last_right_extension_found) = seed_re;
    upc_fence;

    return result;
}

/* Performs the left walk on the UU-graph traversal */
int WALK_LEFT(HASH_TABLE_T *dist_hashtable, contig_ptr_t cur_contig, char seed_le, char *last_left_extension_found, contig_ptr_t *left_contig, int kmer_len)
{
    kmer_and_ext_t kmer_and_ext;

    shared[] LIST_T * lookup_res = NULL, *left_kmer_ptr = NULL;
    LIST_T copy, left_copy;
    int is_least, is_left_least;
    UPC_ATOMIC_MIN_T kmer_used_flag;
    UPC_ATOMIC_MIN_T accumulator;
    char new_seed_re, new_seed_le, left_le, left_re;
    int result;
    char cur_base, *tmpKmer;
    shared[] contig_ptr_box_list_t * contig_ptr_box = NULL;
    UPC_ATOMIC_GET_SHPTR(&contig_ptr_box, &(cur_contig->myself));

    kmer_used_flag = UNUSED;
    result = UNFINISHED;
    cur_base = '\0';

    assert(upc_threadof(cur_contig) == MYTHREAD);
    assert(cur_contig->end - cur_contig->start + 1 >= kmer_len);
    assert(upc_threadof(cur_contig->sequence) == MYTHREAD);

    // get the last kmer
    tmpKmer = (char *)&(cur_contig->sequence[cur_contig->start]);
    lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, tmpKmer, &copy, &is_least, kmer_len);
#ifdef LHS_PERF
    lookups++;
    if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(lookup_res) / MYSV.cores_per_node)) {
        success++;
    }
#endif
    assert(lookup_res != NULL);
    //assert(copy.used_flag == USED);
    assert(copy.used_flag > 0);
    assert(copy.my_contig != NULL);
    remote_assert(lookup_res->my_contig->contig == cur_contig);

    UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(&copy, &kmer_and_ext, is_least, kmer_len);
    DBG("Thread %d: WALK_LEFT: Starting %.*s ext %s with %c\n", MYTHREAD, kmer_len, (char *)&(cur_contig->sequence[cur_contig->start]), getLeftKmer(&kmer_and_ext), seed_le);
    assert(*getLeftExt(&kmer_and_ext) == seed_le);
    assert(getKmer(&kmer_and_ext)[0] == cur_contig->sequence[cur_contig->start]);

    // get the next kmer to the right
    lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, getLeftKmer(&kmer_and_ext), &copy, &is_least, kmer_len);
#ifdef LHS_PERF
    lookups++;
    if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(lookup_res) / MYSV.cores_per_node)) {
        success++;
    }
#endif
    if (lookup_res == NULL) {
        DBG("Thread %d: no left kmer for %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
        return FINISHED;
    }

    while (kmer_used_flag == UNUSED && result == UNFINISHED) {
        assert(isACGT(seed_le));
        assert(lookup_res != NULL);
        left_kmer_ptr = NULL;
        tmpKmer = (char *)&(cur_contig->sequence[cur_contig->start]);

        UPC_POLL;

        /* build the sequence */
        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(&copy, &kmer_and_ext, is_least, kmer_len);
        assert(getKmer(&kmer_and_ext)[0] == seed_le);
        assert(getKmer(&kmer_and_ext)[1] == cur_contig->sequence[cur_contig->start]);
        new_seed_le = *getLeftExt(&kmer_and_ext);
        new_seed_re = *getRightExt(&kmer_and_ext, kmer_len);

        /*
         * If the new kmer's right extension is not unique
         * or right extension is not compatible with the current contig,
         * do not visit */
        if (!isACGT(new_seed_le)
            || new_seed_re != cur_contig->sequence[cur_contig->start + kmer_len - 1]) {
            DBG("Thread %d: WALK_LEFT finished. %s left has invalid extensions le (%c) l:%c r:%c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), cur_contig->sequence[cur_contig->start], new_seed_le, new_seed_re);
            // Done with extension
            return FINISHED;
        }
        assert(memcmp(getRightKmer(&kmer_and_ext), tmpKmer, kmer_len) == 0);

        // good so far...
        // build and find the next kmer, check for UU
        left_kmer_ptr = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, getLeftKmer(&kmer_and_ext), &left_copy, &is_left_least, kmer_len);
#ifdef LHS_PERF
        lookups++;
        if ((MYTHREAD / MYSV.cores_per_node) == (upc_threadof(left_kmer_ptr) / MYSV.cores_per_node)) {
            success++;
        }
#endif
        if (left_kmer_ptr == NULL) {
            DBG("Thread %d: WALK_LEFT finished. no left kmer %s, adding last base %c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), new_seed_le);
            // terminate walk, but add the last base, after locking kmer
            result = FINISHED;
        } else {
            if (is_left_least) {
                convertPackedCodeToExtension(left_copy.packed_extensions, &left_le, &left_re);
            } else {
                convertPackedCodeToExtension(left_copy.packed_extensions, &left_re, &left_le);
                reverseComplementBase(&left_le);
                reverseComplementBase(&left_re);
            }

            if (!isACGT(left_re) || left_re != getKmer(&kmer_and_ext)[kmer_len - 1]) {
                DBG("Thread %d: WALK_LEFT finished. left ext kmer (%s) is not UU. exp_le:%c l:%c r:%c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), cur_contig->sequence[cur_contig->end - kmer_len + 1], new_seed_le, new_seed_re);
                // terminate walk, do not add the last base
                return FINISHED;
            }

            if (!isACGT(left_le)) {
                DBG("Thread %d: WALK_LEFT finished. left kmer %s, has invalid left ext.  adding last base %c\n", MYTHREAD, getLeftKmer(&kmer_and_ext), new_seed_le);
                // terminate walk, but add the last base, after locking kmer
                result = FINISHED;
            }
        }

        /* Now attempt to lock this kmer as used by this contig and add the base */

        //kmer_used_flag = copy.used_flag;
        kmer_used_flag = (copy.used_flag > 0) ? USED : UNUSED;

        if (kmer_used_flag == USED) {
            assert(IS_VALID_UPC_PTR(copy.my_contig));
            DBG("Thread %d: WALK_LEFT kmer %s is used by %lld!\n", MYTHREAD, getLeftKmer(&kmer_and_ext), (lld)(copy.my_contig == NULL ? -1 : (copy.my_contig->contig == NULL ? -2 : IS_VALID_UPC_PTR(copy.my_contig->contig) ? copy.my_contig->contig->contig_id : -3)));
            break;
        }

#ifndef USE_CSWAP_FOR_USED
        UPC_ATOMIC_FADD_MIN_T(&accumulator, &(lookup_res->used_flag), 1);
#else
        UPC_ATOMIC_CSWAP_MIN_T(&accumulator, &(lookup_res->used_flag), 0, 1);
#endif
        kmer_used_flag = (accumulator == 0) ? UNUSED : USED;

        if (kmer_used_flag != UNUSED) {
            DBG("Thread %d: WALK_LEFT kmer %s is now used! ... expecting a lock in my future!\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            break;
        } else {
            DBG2("Thread %d: WALK_LEFT flaged kmer %.*s as used!\n", MYTHREAD, kmer_len, getLeftKmer(&kmer_and_ext));
        }
        //remote_assert( kmer_used_flag == UNUSED && lookup_res->used_flag == USED );
        remote_assert(kmer_used_flag == UNUSED && lookup_res->used_flag > 0);

        /* Fix the kmer to point to current contig box */
        assert(upc_threadof(contig_ptr_box) == MYTHREAD);
        assert(contig_ptr_box->contig != NULL);
        assert(upc_threadof(contig_ptr_box->contig) == MYTHREAD);
        assert(contig_ptr_box->contig->state == ACTIVE);
        assert(contig_ptr_box->next == NULL);
        assert(copy.my_contig == NULL);
        remote_assert(lookup_res->my_contig == NULL);
        // EGEOR: Atomic write of lookup_res->my_contig
        UPC_ATOMIC_SET_SHPTR(NULL, &(lookup_res->my_contig), contig_ptr_box);

        /* Add the next base */
        reallocateContigIfNeeded(cur_contig);
        cur_contig->start--;

        /* Set the new leftmost base */
        cur_base = seed_le;
        seed_le = new_seed_le;
        cur_contig->sequence[cur_contig->start] = cur_base;

        DBG2("Thread %d: WALK_LEFT %s added %c to %lld\n", MYTHREAD, getLeftKmer(&kmer_and_ext), cur_base, (lld)cur_contig->contig_id);

        upc_fence;

        if (result == FINISHED) {
            break;
        }

        remote_assert(IS_LEFT_KMER_UU(dist_hashtable, &kmer_and_ext));

        // shift for the next kmer
        lookup_res = left_kmer_ptr;
        copy = left_copy;
        is_least = is_left_least;

#ifdef PROFILE
        bases_added++;
#endif
    }

    /* Return pointer to the left contig as well (needed for the synchronization protocol) */
    if (kmer_used_flag == USED) {
        DBG("Thread %d: WALK_LEFT Kmer is used %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
        (*left_contig) = GET_CONTIG_OF_KMER(lookup_res, 0);
        if (result == FINISHED) {
            // the last base was not added because the kmer was not acquired
            result = UNFINISHED;
        }
    }

    // disallow USED & FINSHED
    assert(kmer_used_flag == UNUSED || result == UNFINISHED);

    (*last_left_extension_found) = seed_le;
    upc_fence;

    return result;
}

void CLEAN_KMER_CONTIG_PTR(HASH_TABLE_T *hashtable, int kmer_len)
{
    /* cleanup kmer boxes */
    int64_t seed_index = MYTHREAD;

    shared[] LIST_T * new_kmer_seed_ptr = NULL, *kmer_iterator = NULL;
    LIST_T copy;
    kmer_and_ext_t kmer_and_ext;
    contig_t contig_copy;
    contig_ptr_t best_contig = NULL;

    while ((kmer_iterator = GET_NEXT_HASH_TABLE_ENTRY(hashtable, &seed_index, kmer_iterator, kmer_len)) != NULL) {
        new_kmer_seed_ptr = kmer_iterator;

        assert(upc_threadof(new_kmer_seed_ptr) == MYTHREAD);
        copy = *new_kmer_seed_ptr;
        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(&copy, &kmer_and_ext, kmer_len);

        if (copy.used_flag > 0) {
            assert(copy.my_contig != NULL);
            DBG2("Thread %d: CLEAN_KMER_CONTIG_PTR, kmer is used %s\n", MYTHREAD, getLeftKmer(&kmer_and_ext));
            best_contig = GET_CONTIG_OF_KMER(new_kmer_seed_ptr, 0);
            assert(best_contig != NULL);
            contig_copy = *best_contig;

            remote_assert(contig_copy.myself->original == best_contig);

            // To be used at least one direction needs to be UU
            if (!(IS_LEFT_KMER_UU(hashtable, &kmer_and_ext, kmer_len) || IS_RIGHT_KMER_UU(hashtable, &kmer_and_ext, kmer_len))) {
                DBG("Thread %d: ERROR! Kmer is used in contig %lld but is not UU to the left or the right! %s\n", MYTHREAD, (lld)best_contig->contig_id, getLeftKmer(&kmer_and_ext));
            }
            remote_assert(IS_LEFT_KMER_UU(hashtable, &kmer_and_ext) && IS_RIGHT_KMER_UU(hashtable, &kmer_and_ext));

#ifdef DEBUG
            // test the tail of the contig pointer box
            contig_ptr_t best_contig2 = GET_CONTIG_OF_KMER(new_kmer_seed_ptr, 1);
            assert(best_contig == best_contig2);
            assert(best_contig2 != NULL);
            remote_assert(best_contig2 == best_contig->myself->contig);
            remote_assert(best_contig2->myself->next == NULL);
            remote_assert(best_contig2->myself->original == best_contig);
            remote_assert(best_contig2->state == COMPLETE);
#endif

            // assign the proper pointer box to this kmer
            if (copy.my_contig != best_contig->myself) {
                // USE ATOMICS: new_kmer_seed_ptr->my_contig = best_contig->myself;
                shared [] contig_ptr_box_list_t * best;
                UPC_ATOMIC_GET_SHPTR(&best, &(best_contig->myself));
                UPC_ATOMIC_SET_SHPTR(NULL, &(new_kmer_seed_ptr->my_contig), best);
            }

            DBG3("Thread %d: state for kmer %s contig: %lld (%lld)\n", MYTHREAD, getLeftKmer(&kmer_and_ext), (lld)new_kmer_seed_ptr->my_contig->contig->state, (lld)new_kmer_seed_ptr->my_contig->contig->contig_id);
        } else { // UNUSED
            assert(copy.my_contig == NULL);
        }
    }

    upc_barrier;
    SLOG("Thread %d: Cleaned all kmer contig references\n", MYTHREAD);
}

void VALIDATE_KMERS_AND_CONTIGS(HASH_TABLE_T *hashtable, int kmer_len)
{
    int64_t seed_index = MYTHREAD;

    shared[] LIST_T * kmer_iterator = NULL;
    kmer_and_ext_t kmer_and_ext;

    while ((kmer_iterator = GET_NEXT_HASH_TABLE_ENTRY(hashtable, &seed_index, kmer_iterator, kmer_len)) != NULL) {
        assert(upc_threadof(kmer_iterator) == MYTHREAD);

        LIST_T copy = *kmer_iterator;
        //if (copy.used_flag != USED)
        if (copy.used_flag == 0) {
            continue;
        }

        UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(&copy, &kmer_and_ext, kmer_len);
        if (IS_LEFT_KMER_UU(hashtable, &kmer_and_ext, kmer_len) || IS_RIGHT_KMER_UU(hashtable, &kmer_and_ext, kmer_len)) {
            DBG2("Thread %d: state for kmer %s contig: %lld (%lld)\n", MYTHREAD, getLeftKmer(&kmer_and_ext), (lld)kmer_iterator->my_contig->contig->state, (lld)kmer_iterator->my_contig->contig->contig_id);

            //assert(copy.used_flag == USED);
            assert(copy.used_flag > 0);
            assert(copy.my_contig != NULL);
            remote_assert(copy.my_contig->contig != NULL);
            remote_assert(copy.my_contig->next == NULL);
            remote_assert(copy.my_contig->contig->state == COMPLETE);
        }
    }
    upc_barrier;
}


contig_ptr_t INITIALIZE_NEW_CONTIG(shared[] LIST_T *new_kmer_seed_ptr, int kmer_len)
{
    kmer_and_ext_t kne;

    //remote_assert(new_kmer_seed_ptr != NULL && new_kmer_seed_ptr->used_flag == USED);
    remote_assert(new_kmer_seed_ptr != NULL && new_kmer_seed_ptr->used_flag > 0);
    /***********************************/
    /* Initialize a new contig         */
    /* Assumes new_kmer_seed_ptr is UU */
    /***********************************/
    int minimum_size = (MINIMUM_CONTIG_SIZE > 3 * kmer_len) ? MINIMUM_CONTIG_SIZE : 3 * kmer_len;

    contig_ptr_t cur_contig = NULL;
    UPC_ALLOC_CHK0(cur_contig, sizeof(contig_t));

    UPC_ALLOC_CHK0(cur_contig->sequence, minimum_size * sizeof(char));

    cur_contig->start = minimum_size / 2 - kmer_len / 2; // index of first base in contig
    cur_contig->end = cur_contig->start + kmer_len - 1;  // index of last base in contig
    cur_contig->size = minimum_size;                     // available size in the contig

    // kmerAndEExtension is sized kmer_len+2, so write just before start of contig...
    int kmer_ext_start = cur_contig->start - 1;
    assert(kmer_ext_start >= 0);
    assert(upc_threadof(&(cur_contig->sequence[kmer_ext_start])) == MYTHREAD);

    UNPACK_KMER_AND_EXTENSIONS(new_kmer_seed_ptr, &kne, kmer_len);
    memcpy((char *)&(cur_contig->sequence[kmer_ext_start]), getLeftKmer(&kne), kmer_len + 2);

#ifdef PROFILE
    bases_added += kmer_len;
#endif
    cur_contig->is_circular = 0;
    cur_contig->state = ACTIVE;
    cur_contig->state_lock = upc_global_lock_alloc();
    cur_contig->contig_id = -1 - MYTHREAD;

    cur_contig->sequence[cur_contig->end + 2] = '\0';
    DBG("Thread %d: initialized new contig. %c %.*s %c\n", MYTHREAD, cur_contig->sequence[kmer_ext_start], kmer_len, (char *)&(cur_contig->sequence[kmer_ext_start + 1]), cur_contig->sequence[kmer_ext_start + kmer_len + 1]);

    // EGEOR: Atomic write
    // RE: no need for atomic set, not other thread can use cur_contig until this function returns
    cur_contig->myself = alloc_contig_ptr_box_list(cur_contig);
    upc_fence;
    DBG("Thread %d: allocated new_contig_ptr_list_box\n", MYTHREAD);

    return cur_contig;
}

/* Performs the UU-graph traversal using the WALK_RIGHT and WALK_LEFT routines */
int UU_GRAPH_TRAVERSAL(HASH_TABLE_T *hashtable, GZIP_FILE output_file, int min_contig_length, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    int64_t seed_index, cur_length, concat_length, new_end, new_start, new_size;
    int lex_ind;
    char unpacked_kmer[MAX_KMER_SIZE];
    char seed_aux[MAX_KMER_SIZE];
    seed_aux[kmer_len] = '\0';
    char auxiliary_unpacked_kmer[MAX_KMER_SIZE];
    char packed_extensions;
    shared[] char *prev_buffer;
    shared[] LIST_T * kmer_iterator, *new_kmer_seed_ptr, *lookup_res;
    contig_ptr_t cur_contig;
    int64_t myThreadContigId;
    int64_t v, first, second, third;
    char seed_le, seed_re;
    contig_ptr_t right_contig, left_contig, first_contig, second_contig, third_contig;
    right_contig = NULL;
    left_contig = NULL;
    first_contig = NULL;
    second_contig = NULL;
    third_contig = NULL;
    int my_id, l_neighbor_id, r_neighbor_id, locks_to_obtain;
    char contig_tail[MAX_KMER_SIZE];
    char fixing_endpoint[MAX_KMER_SIZE];
    char auxiliary_fixing_kmer[MAX_KMER_SIZE];
    char *kmer_to_search, *tmpKmer;
    contig_t copy_of_contig;
    int skip_right_check, left_state, right_state, right_ret_value, left_ret_value, walking;
    int64_t ovlp_pos, my_start, chars_to_copy, my_end;
    char last_right_extension_found, last_left_extension_found, foo;
    char *real_contig;
    auxiliary_unpacked_kmer[kmer_len] = '\0';
    auxiliary_fixing_kmer[kmer_len] = '\0';
    unpacked_kmer[kmer_len] = '\0';
    char scratchpad[MAX_KMER_SIZE];
    char auxiliary_kmer[MAX_KMER_SIZE];
    auxiliary_kmer[kmer_len] = '\0';
    scratchpad[kmer_len] = '\0';
    contig_tail[kmer_len] = '\0';
    int attach_straight, attach_rev_compl, attaching_left, attaching_right;
    shared[] contig_ptr_box_list_t * my_contigs = NULL, *my_short_contigs = NULL, *failed_contigs = NULL, *tmp_contig_ptr_box;
    UPC_TICK_T start_UU, end_UU, start_walking, end_walking;

#ifdef DEBUG
    // first verify all kmers are UNUSED
    seed_index = MYTHREAD;
    kmer_iterator = NULL;
    int64_t numKmers = 0;
    while ((kmer_iterator = GET_NEXT_HASH_TABLE_ENTRY(hashtable, &seed_index, kmer_iterator, kmer_len)) != NULL) {
        assert(kmer_iterator->used_flag == UNUSED);
        assert(kmer_iterator->my_contig == NULL);
        assert(upc_threadof(kmer_iterator) == MYTHREAD);
        numKmers++;
    }
    DBG("Thread %d: Has %lld kmers in my part of the hashtable\n", MYTHREAD, (lld)numKmers);
    upc_barrier;
#endif

#ifdef UU_TRAV_PROFILE
    start_UU = UPC_TICKS_NOW();
#endif


    myThreadContigId = -1 - MYTHREAD;
    /* initialze seed_index and new_kmer_seed_ptr */
    seed_index = MYTHREAD;
    kmer_iterator = NULL;

    while (1) {
        DBG("Thread %d: getting next unused kmer...\n", MYTHREAD);
        kmer_iterator = GET_NEXT_UNUSED_UU_KMER(hashtable, &seed_index, kmer_iterator, &seed_le, &seed_re, kmer_len);
        if (kmer_iterator == NULL) {
            break;
        }
        assert(upc_threadof(kmer_iterator) == MYTHREAD);
        new_kmer_seed_ptr = kmer_iterator;

        /**********************************************/
        /* Start building contig using the found seed */
        /**********************************************/
        UPC_POLL;
        //assert(new_kmer_seed_ptr->used_flag == USED);
        assert(new_kmer_seed_ptr->used_flag > 0);

        cur_contig = INITIALIZE_NEW_CONTIG(new_kmer_seed_ptr, kmer_len);

        // incomplete contigs get temporary, unique, negative contig_id assigned
        cur_contig->contig_id = myThreadContigId;
        myThreadContigId -= THREADS;

        // USE ATOMICS: new_kmer_seed_ptr->my_contig = cur_contig->myself;
        shared[] contig_ptr_box_list_t * myContig = NULL;
        UPC_ATOMIC_GET_SHPTR(&myContig, &(cur_contig->myself));
        UPC_ATOMIC_SET_SHPTR(NULL, &(new_kmer_seed_ptr->my_contig), myContig);
        upc_fence;

        assert(upc_threadof(cur_contig->sequence) == MYTHREAD);
        DBG("Thread %d: initialize walk with contig %lld: %.*s...\n", MYTHREAD, (lld)cur_contig->contig_id, kmer_len, (char *)(cur_contig->sequence + cur_contig->start));
        /* Initialize a walk */
        walking = TRUE;
        right_ret_value = isACGT(seed_re) ? UNFINISHED : FINISHED;
        left_ret_value = isACGT(seed_le) ? UNFINISHED : FINISHED;
        walking = (right_ret_value == UNFINISHED) | (left_ret_value == UNFINISHED);
        while (walking == TRUE) {
            assert(upc_threadof(cur_contig) == MYTHREAD);
            assert(upc_threadof(new_kmer_seed_ptr) == MYTHREAD);
            //assert(new_kmer_seed_ptr->used_flag == USED);
            assert(new_kmer_seed_ptr->used_flag > 0);
            assert(upc_threadof(cur_contig->myself) == MYTHREAD);
            assert(new_kmer_seed_ptr->my_contig->contig == cur_contig);
            assert(cur_contig->state == ACTIVE);

            DBG("Thread %d: Walking with contig sized %lld. %d %d\n", MYTHREAD, (lld)(cur_contig->end - cur_contig->start + 1), left_ret_value, right_ret_value);
            UPC_POLL;

#ifdef UU_TRAV_PROFILE
            start_walking = UPC_TICKS_NOW();
#endif
            /* Walk right */
            if (right_ret_value == UNFINISHED) {
                right_contig = NULL;
                right_ret_value = WALK_RIGHT(hashtable, cur_contig, seed_re, &last_right_extension_found, &right_contig, kmer_len);
                seed_re = last_right_extension_found;
            }

            UPC_POLL;

            /* Walk left */
            if (left_ret_value == UNFINISHED) {
                left_contig = NULL;
                left_ret_value = WALK_LEFT(hashtable, cur_contig, seed_le, &last_left_extension_found, &left_contig, kmer_len);
                seed_le = last_left_extension_found;
            }

            UPC_POLL;

#ifdef UU_TRAV_PROFILE
            end_walking = UPC_TICKS_NOW();
            walking_time += UPC_TICKS_TO_SECS(end_walking - start_walking);
#endif

            /* Examine the states of the walks & store contig if both endpoints have been reached */
            /* otherwise follow synchronization protocol                                          */
            if ((right_ret_value == FINISHED) && (left_ret_value == FINISHED)) {
                LOCK_CONTIG(cur_contig);
                finish_contig(cur_contig, output_file, min_contig_length, &my_contigs, &my_short_contigs, kmer_len);

#ifdef MARK_POS_IN_CONTIG
                MARK_POSITION_IN_CONTIG(cur_contig, hashtable);
#endif
                UNLOCK_CONTIG(cur_contig);
                walking = FALSE;
            } else { // walking needs to lock -- at least one kmer is already used
                /* At least have to obtain my contig's lock */
                locks_to_obtain = 1;

                if (left_ret_value == UNFINISHED) {
                    locks_to_obtain++;
                    if (left_contig == NULL) {
                        DIE("FATAL ERROR: Left contig should be != NULL\n"); assert(0);
                    }
                    l_neighbor_id = (int)upc_threadof(left_contig);
                    assert(l_neighbor_id >= 0 && l_neighbor_id < THREADS);
                } else {
                    l_neighbor_id = -2;
                }

                if (right_ret_value == UNFINISHED) {
                    locks_to_obtain++;
                    skip_right_check = FALSE;
                    if (right_contig == NULL) {
                        DIE("FATAL ERROR: Right contig should be != NULL\n"); assert(0);
                    }
                    r_neighbor_id = (int)upc_threadof(right_contig);
                    assert(r_neighbor_id >= 0 && r_neighbor_id < THREADS);
                } else {
                    r_neighbor_id = -1;
                    skip_right_check = TRUE;
                }

                my_id = MYTHREAD;

                /* Naive way to sort the three thread_ids in descending order */
                if (my_id < l_neighbor_id) {
                    if (my_id < r_neighbor_id) {
                        if (l_neighbor_id < r_neighbor_id) {
                            first = r_neighbor_id;
                            first_contig = right_contig;
                            second = l_neighbor_id;
                            second_contig = left_contig;
                            third = my_id;
                            third_contig = cur_contig;
                        } else {
                            first = l_neighbor_id;
                            first_contig = left_contig;
                            second = r_neighbor_id;
                            second_contig = right_contig;
                            third = my_id;
                            third_contig = cur_contig;
                        }
                    } else {
                        first = l_neighbor_id;
                        first_contig = left_contig;
                        second = my_id;
                        second_contig = cur_contig;
                        third = r_neighbor_id;
                        third_contig = right_contig;
                    }
                } else {
                    if (l_neighbor_id < r_neighbor_id) {
                        if (my_id < r_neighbor_id) {
                            first = r_neighbor_id;
                            first_contig = right_contig;
                            second = my_id;
                            second_contig = cur_contig;
                            third = l_neighbor_id;
                            third_contig = left_contig;
                        } else {
                            first = my_id;
                            first_contig = cur_contig;
                            second = r_neighbor_id;
                            second_contig = right_contig;
                            third = l_neighbor_id;
                            third_contig = left_contig;
                        }
                    } else {
                        first = my_id;
                        first_contig = cur_contig;
                        second = l_neighbor_id;
                        second_contig = left_contig;
                        third = r_neighbor_id;
                        third_contig = right_contig;
                    }
                }

                assert(locks_to_obtain > 1);
                DBG("Thread %d: obtaining %d locks. %lld %lld %lld\n", MYTHREAD, locks_to_obtain, (lld)first_contig->contig_id, (lld)second_contig->contig_id, (lld)(locks_to_obtain > 2 ? third_contig->contig_id : 0));

                /* make sure all changes are refreshed before lock */
                upc_fence;

                /* Case where there are three locks to obtain */
                if (locks_to_obtain == 3) {
                    if ((first_contig == second_contig) && (second_contig == third_contig)) {
                        if (first_contig == NULL) {
                            WARN("ERROR: Thread %d exhibits unexpected behavior while obtaining locks\n", MYTHREAD);
                        }
                        DBG("Thread %d: Found circular contig - 3 locks\n", MYTHREAD);
                        /* Found a circlular contig */
                        cur_contig->is_circular = 1;
                        LOCK_CONTIG(cur_contig);

                        finish_contig(cur_contig, output_file, min_contig_length, &my_contigs, &my_short_contigs, kmer_len);

#ifdef MARK_POS_IN_CONTIG
                        MARK_POSITION_IN_CONTIG(cur_contig, hashtable);
#endif

                        UNLOCK_CONTIG(cur_contig);
                        /* Should terminate walk */
                        left_ret_value = FINISHED;
                        right_ret_value = FINISHED;
                        walking = FALSE;

                        // lock is released and contig is finished, so break
                        break;
                    } else if (first_contig == second_contig) {
                        LOCK_CONTIG(first_contig);
                        LOCK_CONTIG(third_contig);
                    } else if (third_contig == second_contig) {
                        LOCK_CONTIG(first_contig);
                        LOCK_CONTIG(second_contig);
                    } else if ((first_contig != second_contig) && (second_contig != third_contig) && (first_contig != third_contig)) {
                        LOCK_CONTIG(first_contig);
                        LOCK_CONTIG(second_contig);
                        LOCK_CONTIG(third_contig);
                    }
                }

                /* Case where there are two locks to obtain */
                if (locks_to_obtain == 2) {
                    if (first_contig == second_contig) {
                        if (first_contig == NULL) {
                            WARN("ERROR: Thread %d exhibits unexpected behavior while obtaining locks\n", MYTHREAD);
                        }
                        DBG("Thread %d: Found circular contig\n", MYTHREAD);
                        /* Found a circlular contig */
                        cur_contig->is_circular = 1;
                        LOCK_CONTIG(cur_contig);

                        finish_contig(cur_contig, output_file, min_contig_length, &my_contigs, &my_short_contigs, kmer_len);

#ifdef MARK_POS_IN_CONTIG
                        MARK_POSITION_IN_CONTIG(cur_contig, hashtable);
#endif

                        /* Should terminate walk */
                        UNLOCK_CONTIG(cur_contig);
                        left_ret_value = FINISHED;
                        right_ret_value = FINISHED;
                        walking = FALSE;

                        // lock is released, contig is finished, so break
                        break;
                    } else {
                        LOCK_CONTIG(first_contig);
                        LOCK_CONTIG(second_contig);
                    }
                }

                /* Have obtained all locks in the neighborhood of contigs, so check condition of neighbors and decide what to do (state of current contig is ACTIVE) */
                assert(upc_threadof(cur_contig) == MYTHREAD);
                assert(cur_contig->state == ACTIVE);

                UPC_POLL;
                upc_fence;
                if (left_ret_value == UNFINISHED) {
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                    left_state = left_contig->state;
#else
                    UPC_ATOMIC_GET_I32(&left_state, &left_contig->state);
                    DBG("Thread %d: left_state: %d contig(%lld)\n", MYTHREAD, left_state, (lld)left_contig->contig_id);
#endif
                }
                if (right_ret_value == UNFINISHED) {
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                    right_state = right_contig->state;
#else
                    UPC_ATOMIC_GET_I32(&right_state, &right_contig->state);
                    DBG("Thread %d: right_state: %d contig(%lld)\n", MYTHREAD, right_state, (lld)right_contig->contig_id);
#endif
                }

                if (left_ret_value == UNFINISHED) {
                    if (left_state == CLAIMED) {
                        UPC_POLL;
                        upc_fence;
                        /* Just release locks and retry */
                        skip_right_check = TRUE;
                        DBG("Thread %d: Found left CLAIMED state, retrying!!!\n", MYTHREAD);
                    } else if (left_state == ABORTED) {
                        UPC_POLL;
                        DBG("Thread %d: Found left ABORTED, claiming and attaching %lld\n", MYTHREAD, (lld)left_contig->contig_id);

                        /* Attach aborted contig to my left */
                        copy_of_contig = *left_contig;
                        ovlp_pos = copy_of_contig.end - kmer_len + 1;
                        my_start = cur_contig->start;
                        assert(upc_threadof(cur_contig->sequence) == MYTHREAD);
                        real_contig = (char *)cur_contig->sequence;
                        chars_to_copy = ovlp_pos - copy_of_contig.start + 1;
                        attaching_left = TRUE;

                        /* Check if last kmer_len chars in left contigs are valid (since we don't know if the rc of subcontig has been generated) */
                        upc_memget(scratchpad, &(copy_of_contig.sequence[copy_of_contig.end - (kmer_len - 1)]), kmer_len * sizeof(char));
                        contig_tail[0] = last_left_extension_found;
                        for (v = 1; v < kmer_len; v++) {
                            contig_tail[v] = real_contig[my_start + v - 1];
                        }

                        if (memcmp(contig_tail, scratchpad, kmer_len * sizeof(char)) == 0) {
                            attach_straight = TRUE;
                            attach_rev_compl = FALSE;
                        } else {
                            upc_memget(scratchpad, &(copy_of_contig.sequence[copy_of_contig.start]), kmer_len * sizeof(char));
                            reverseComplementINPLACE((char *)scratchpad, kmer_len);
                            if (memcmp(contig_tail, scratchpad, kmer_len * sizeof(char)) == 0) {
                                attach_rev_compl = TRUE;
                                attach_straight = FALSE;
                            } else {
                                /* Should just set left_ret_value = FINISHED since I am falling in the "middle" of another contig and do NOT concatenate ABORTED */
                                WARN("Concatenation in middle for current contig %lld and remote contig %lld\n", (lld)cur_contig->contig_id, (lld)copy_of_contig.contig_id);
                                attaching_left = FALSE;
                                left_ret_value = FINISHED;
                            }
                        }

                        if (attaching_left == TRUE) {
                            /* Assure there is enough space to attach the contig in the current allocated buffer */
                            reallocateContigIfNeededLR(cur_contig, chars_to_copy + MINIMUM_CONTIG_SIZE, MINIMUM_CONTIG_SIZE, INCR_FACTOR);

                            // reset cached variables as they may have changed...
                            my_start = cur_contig->start;
                            assert(upc_threadof(cur_contig->sequence) == MYTHREAD);
                            real_contig = (char *)cur_contig->sequence;
                            assert(((int)my_start - (int)chars_to_copy) >= 1);

                            if (attach_straight == TRUE) {
                                upc_memget((char *)&(cur_contig->sequence[my_start - chars_to_copy]), &(copy_of_contig.sequence[copy_of_contig.start]), chars_to_copy * sizeof(char));
                            }

                            if (attach_rev_compl == TRUE) {
                                upc_memget((char *)&(cur_contig->sequence[my_start - chars_to_copy]), &(copy_of_contig.sequence[copy_of_contig.end - chars_to_copy + 1]), chars_to_copy * sizeof(char));
                                reverseComplementINPLACE((char *)&(cur_contig->sequence[my_start - chars_to_copy]), chars_to_copy);
                            }

                            // free aborted contig sequence
                            assert(left_contig->state == ABORTED);
                            assert(left_contig->sequence == copy_of_contig.sequence);
                            UPC_FREE_CHK0(copy_of_contig.sequence);
                            left_contig->sequence = NULL;

                            cur_contig->start = cur_contig->start - chars_to_copy;
                            assert(cur_contig->start > 0);

                            /* FIX CONTIG POINTER OF THE ATTACHED KMERS */
                            for (v = 0; v < kmer_len; v++) {
                                scratchpad[v] = cur_contig->sequence[cur_contig->start + v];
                            }
                            scratchpad[kmer_len] = '\0';
                            lookup_res = LOOKUP_AND_GET_EXT_OF_KMER(hashtable, scratchpad, &seed_le, &foo, kmer_len);

                            if (lookup_res == NULL) {
                                WARN("FATAL ERROR in left CONCAT protocol from thread %d\n", MYTHREAD);
                            }
                            assert(lookup_res != NULL);
                            //remote_assert(lookup_res->used_flag == USED);
                            remote_assert(lookup_res->used_flag > 0);

#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                            left_contig->state = CLAIMED;
#else
                            int oldState;
                            UPC_ATOMIC_SET_I32(&oldState, &left_contig->state, CLAIMED);
                            if (oldState != ABORTED) DIE("left_contig->state should be ABORTED before becoming CLAIMED: was %d\n", oldState);
#endif
                            upc_fence;
                            contig_ptr_t tmpContig = NULL;
                            do {
                                DBG("Thread %d: attach left changing kmer %s from %lld to %lld\n", MYTHREAD, scratchpad, (lld)left_contig->contig_id, (lld)cur_contig->contig_id);
                                tmp_contig_ptr_box = GET_CONTIG_BOX_OF_KMER(lookup_res, 0);
                                remote_assert(tmp_contig_ptr_box->contig == left_contig);
                                remote_assert(tmp_contig_ptr_box->next == NULL);
                                assign_contig_to_box_list(left_contig->myself, cur_contig);
                                shared[] contig_ptr_box_list_t * lookupMyContigBox = NULL;
                                UPC_ATOMIC_GET_SHPTR(&lookupMyContigBox, &(lookup_res->my_contig));
                                assert(lookupMyContigBox != NULL);
                                UPC_ATOMIC_GET_SHPTR(&tmpContig, &(lookupMyContigBox->contig));
                            } while (tmpContig != cur_contig);

                            add_contig_to_box_list(&failed_contigs, alloc_contig_ptr_box_list(left_contig));

                            upc_fence;
                            remote_assert(GET_CONTIG_OF_KMER(lookup_res, 0) == cur_contig);
                            remote_assert(GET_CONTIG_OF_KMER(lookup_res, 1) == cur_contig);

                            DBG("Thread %d: claimed left %lld into active %lld. +%lld = %lld\n", MYTHREAD, (lld)left_contig->contig_id, (lld)cur_contig->contig_id, (lld)chars_to_copy, (lld)(cur_contig->end - cur_contig->start + 1));
                        }

                        if (right_ret_value == UNFINISHED) {
                            skip_right_check = FALSE;
                        } else {
                            /* No right neighbor */
                            seed_re = last_right_extension_found;
                        }
                    } else if (left_state == ACTIVE) {
                        DBG("Thread %d: Found left ACTIVE state.  Setting my contig %lld to aborted. Skipping right check.\n", MYTHREAD, (lld)cur_contig->contig_id);
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                        cur_contig->state = ABORTED;
#else
                        int oldState;
                        UPC_ATOMIC_SET_I32(&oldState, &cur_contig->state, ABORTED);
                        if (oldState != ACTIVE) DIE("cur_contig->state must be ACTIVE before becoming ABORTED. was %d\n", oldState);
#endif
                        upc_fence;
                        skip_right_check = TRUE;
                        walking = FALSE;
                    } else if (left_state == COMPLETE) {
                        UPC_POLL;
                        upc_fence;
                        DIE("ERROR! Found left COMPLETE state! THIS SHOULD NOT HAPPEN! left_contig->state=%d COMPLETE=%d\n", left_contig->state, COMPLETE);
                    } else {
                        DIE("Thread %d: ERROR! Found UNDEFINED left state!!!\n", MYTHREAD);
                        assert(0);
                    }
                }

                if ((right_ret_value == UNFINISHED) && (skip_right_check == FALSE)) {
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                    right_state = right_contig->state;
#else
                    UPC_ATOMIC_GET_I32(&right_state, &right_contig->state);
#endif
                    if (right_state == CLAIMED) {
                        UPC_POLL;
                        upc_fence;
                        /* Just release locks and retry!!! */
                        right_ret_value = UNFINISHED;
                        DBG("Thread %d: Found right CLAIMED state, retrying!!!\n", MYTHREAD);
                    } else if (right_state == ABORTED) {
                        UPC_POLL;
                        DBG("Thread %d: Found right ABORTED, claiming and attaching %lld\n", MYTHREAD, (lld)right_contig->contig_id);
                        /* Attach aborted contig to my right */
                        copy_of_contig = *right_contig;
                        ovlp_pos = copy_of_contig.start;
                        my_end = cur_contig->end;
                        real_contig = (char *)cur_contig->sequence;
                        chars_to_copy = copy_of_contig.end - (ovlp_pos + kmer_len - 2);
                        attaching_right = TRUE;

                        /* Check if first kmer_len chars in right contigs are valid (since we don't know if the rc of subcontig has been generated) */
                        upc_memget(scratchpad, &(copy_of_contig.sequence[copy_of_contig.start]), kmer_len * sizeof(char));
                        contig_tail[kmer_len - 1] = last_right_extension_found;
                        for (v = 0; v < kmer_len - 1; v++) {
                            contig_tail[v] = real_contig[my_end - (kmer_len - 2 - v)];
                        }

                        if (memcmp(contig_tail, scratchpad, kmer_len * sizeof(char)) == 0) {
                            attach_straight = TRUE;
                            attach_rev_compl = FALSE;
                        } else {
                            upc_memget(scratchpad, &(copy_of_contig.sequence[copy_of_contig.end - (kmer_len - 1)]), kmer_len * sizeof(char));
                            reverseComplementINPLACE((char *)scratchpad, kmer_len);
                            if (memcmp(contig_tail, scratchpad, kmer_len * sizeof(char)) == 0) {
                                attach_straight = FALSE;
                                attach_rev_compl = TRUE;
                            } else {
                                WARN("Concatenation in middle for current contig %lld and remote contig %lld\n", (lld)cur_contig->contig_id, (lld)copy_of_contig.contig_id);
                                right_ret_value = FINISHED;
                                attaching_right = FALSE;
                            }
                        }

                        if (attaching_right == TRUE) {
                            reallocateContigIfNeededLR(cur_contig, MINIMUM_CONTIG_SIZE, MINIMUM_CONTIG_SIZE + chars_to_copy, INCR_FACTOR);

                            // resets cached variables as they might have changed
                            my_end = cur_contig->end;
                            real_contig = (char *)cur_contig->sequence;

                            assert(cur_contig->size - 1 - cur_contig->end > chars_to_copy);
                            /* There is enough space to attach the contig */

                            if (attach_straight == TRUE) {
                                upc_memget((char *)&(cur_contig->sequence[my_end + 1]), &(copy_of_contig.sequence[ovlp_pos - 1 + kmer_len]), chars_to_copy * sizeof(char));
                            }

                            if (attach_rev_compl == TRUE) {
                                upc_memget((char *)&(cur_contig->sequence[my_end + 1]), &(copy_of_contig.sequence[copy_of_contig.start]), chars_to_copy * sizeof(char));
                                reverseComplementINPLACE((char *)&(cur_contig->sequence[my_end + 1]), chars_to_copy);
                            }

                            // free aborted contig sequence
                            assert(right_contig->state == ABORTED);
                            assert(right_contig->sequence == copy_of_contig.sequence);
                            UPC_FREE_CHK0(copy_of_contig.sequence);
                            right_contig->sequence = NULL;

                            cur_contig->end = cur_contig->end + chars_to_copy;

                            /* FIX CONTIG POINTER OF THE ATTACHED KMERS */
                            for (v = 0; v < kmer_len; v++) {
                                scratchpad[v] = cur_contig->sequence[cur_contig->end - (kmer_len - v - 1)];
                            }
                            scratchpad[kmer_len] = '\0';

                            lookup_res = LOOKUP_AND_GET_EXT_OF_KMER(hashtable, scratchpad, &foo, &seed_re, kmer_len);

                            if (lookup_res == NULL) {
                                WARN("FATAL ERROR in right CONCAT protocol from thread %d\n", MYTHREAD);
                            }
                            assert(lookup_res != NULL);

#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                            right_contig->state = CLAIMED;
#else
                            int oldState;
                            UPC_ATOMIC_SET_I32(&oldState, &right_contig->state, CLAIMED);
                            if (oldState != ABORTED) DIE("right_contig->state must be ABORTED before becoming CLAIMED. was %d\n", oldState);
#endif
                            
                            upc_fence;
                            contig_ptr_t tmpContig = NULL;
                            do {
                                DBG("Thread %d: attach right changing kmer %s from %lld to %lld\n", MYTHREAD, scratchpad, (lld)right_contig->contig_id, (lld)cur_contig->contig_id);

                                tmp_contig_ptr_box = GET_CONTIG_BOX_OF_KMER(lookup_res, 0);
                                remote_assert(tmp_contig_ptr_box->contig == right_contig);
                                remote_assert(tmp_contig_ptr_box->next == NULL);
                                assign_contig_to_box_list(right_contig->myself, cur_contig);
                                shared[] contig_ptr_box_list_t * lookupMyContigBox = NULL;
                                UPC_ATOMIC_GET_SHPTR(&lookupMyContigBox, &(lookup_res->my_contig));
                                assert(lookupMyContigBox != NULL);
                                UPC_ATOMIC_GET_SHPTR(&tmpContig, &(lookupMyContigBox->contig));
                            } while (tmpContig != cur_contig);

                            add_contig_to_box_list(&failed_contigs, alloc_contig_ptr_box_list(right_contig));

                            upc_fence;
                            remote_assert(GET_CONTIG_OF_KMER(lookup_res, 0) == cur_contig);
                            remote_assert(GET_CONTIG_OF_KMER(lookup_res, 1) == cur_contig);

                            DBG("Thread %d: claimed right %lld into active %lld. +%lld = %lld\n", MYTHREAD, (lld)right_contig->contig_id, (lld)cur_contig->contig_id, (lld)chars_to_copy, (lld)(cur_contig->end - cur_contig->start + 1));
                        }
                    } else if (right_state == ACTIVE) {
                        UPC_POLL;
                        /* Abort my contig */
                        DBG("Thread %d: Found right ACTIVE. Setting my contig %lld to aborted\n", MYTHREAD, (lld)cur_contig->contig_id);
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                        cur_contig->state = ABORTED;
#else
                        int oldState;
                        UPC_ATOMIC_SET_I32(&oldState, &cur_contig->state, ABORTED);
                        if (oldState != ACTIVE) DIE("cur_contig->state must be ACTIVE before becoming ABORTED. was %d\n", oldState);
#endif
                        upc_fence;
                        walking = FALSE;
                    } else if (right_state == COMPLETE) {
                        UPC_POLL;
                        upc_fence;
                        DIE("ERROR! Found right COMPLETE state! THIS SHOULD NOT HAPPEN: right_contig->state=%d COMPLETE=%d\n", right_contig->state, COMPLETE);
                    } else {
                        DIE("Thread %d: ERROR! Found UNDEFINED right state!!!\n", MYTHREAD);
                        assert(0);
                    }
                }

                // release all acquired locks
                if (locks_to_obtain == 3) {
                    if (first_contig == second_contig) {
                        UNLOCK_CONTIG(third_contig);
                        UNLOCK_CONTIG(first_contig);
                    } else if (third_contig == second_contig) {
                        UNLOCK_CONTIG(second_contig);
                        UNLOCK_CONTIG(first_contig);
                    } else if ((first_contig != second_contig) && (second_contig != third_contig) && (first_contig != third_contig)) {
                        UNLOCK_CONTIG(third_contig);
                        UNLOCK_CONTIG(second_contig);
                        UNLOCK_CONTIG(first_contig);
                    }
                }

                if (locks_to_obtain == 2) {
                    UNLOCK_CONTIG(second_contig);
                    UNLOCK_CONTIG(first_contig);
                }
            } // walking needs to lock -- at least one kmer is already used
        }     // while walking
        assert(cur_contig->state != ACTIVE);
        upc_fence;
    } // while there are seeds to process

    // write remainder of contigs
    write_final_contigs(output_file, contig_offset);

    assert(kmer_iterator == NULL);

#ifdef UU_TRAV_PROFILE
    end_UU = UPC_TICKS_NOW();
    UU_time = UPC_TICKS_TO_SECS(end_UU - start_UU);
#endif

    UPC_POLL;
    upc_fence;
    LOGF("Thread %d: Finished UU traversal\n", MYTHREAD);

    upc_barrier;

#ifdef DEBUG
    /* validate all contigs */
    CLEAN_KMER_CONTIG_PTR(hashtable, kmer_len);
    // last verify all USED kmers are UU in one direction and have contig set and COMPLETE
    VALIDATE_KMERS_AND_CONTIGS(hashtable, kmer_len);

#endif

#ifdef STORE_CONTIGS_IN_ARRAY

    /* store all contigs in a globally indexed array by contig_id */
    contig_list = NULL;
    UPC_ALL_ALLOC_CHK(contig_list, contig_id + 1, sizeof(contig_ptr_t));
    while (my_contigs != NULL) {
        tmp_contig_ptr_box = my_contigs;
        contig_ptr_t contig;
        UPC_ATOMIC_GET_SHPTR(&contig, &(my_contigs->contig));
        assert(upc_threadof(contig) == MYTHREAD);
        assert(contig->state == COMPLETE);
        assert(contig->contig_id >= 0);
        assert(upc_threadof(contig->myself) == MYTHREAD);
        assert(contig->myself->next == NULL);
        contig_list[contig->contig_id] = contig;
        // FIXME my_contigs = my_contigs->next;
        UPC_ATOMIC_GET_SHPTR(&my_contigs, &(my_contigs->next));
        UPC_FREE_CHK(tmp_contig_ptr_box);
    }
    upc_barrier;
    SLOG("Thread %d: Stored all long contigs (%lld)\n", MYTHREAD, (lld)contig_id);
    // TODO is this ever used downstream?
#else
    destroy_contig_ptr_box_list(&my_contigs, 1);
#endif /* STORE_CONTIGS_IN_ARRAY */

    /* cleanup remaining contigs and box pointers */

    destroy_contig_ptr_box_list(&my_short_contigs, 1);

    destroy_contig_ptr_box_list(&failed_contigs, 1);

    upc_barrier;

    SLOG("All Finished UU traversal\n");

    return 1;
}

/* get the contig_ptr "box" associated with a kmer -- must be USED already to work */
shared[] contig_ptr_box_list_t *GET_CONTIG_BOX_OF_KMER(shared[] LIST_T *lookup_res, int follow_list)
{
    shared[] contig_ptr_box_list_t * box_ptr = NULL;
    if (lookup_res == NULL) {
        DIE("FATAL ERROR: K-mer should not be NULL here (right walk)\n");
    }
    assert(lookup_res != NULL);
#ifdef MERACULOUS
    //assert(lookup_res->used_flag == USED);
    assert(lookup_res->used_flag > 0);
    // EGEOR: Atomic read of lookup_res->my_contig
    //loop_until ( box_ptr = lookup_res->my_contig, box_ptr != NULL && IS_VALID_UPC_PTR(box_ptr)  );
    loop_until(UPC_ATOMIC_GET_SHPTR(&box_ptr, &(lookup_res->my_contig)), box_ptr != NULL && IS_VALID_UPC_PTR(box_ptr)); // FIXME this is where swan hangs
#endif
    // follow box to tail
    while (follow_list && box_ptr->next != NULL) {
        box_ptr = box_ptr->next;
    }

    return box_ptr;
}

/* get the contig associated with a kmer -- must be USED already to work */
shared[] contig_t *GET_CONTIG_OF_KMER(shared[] LIST_T *lookup_res, int follow_list)
{
    shared[] contig_ptr_box_list_t * box_ptr;
    shared[] contig_t * contig = NULL;

    assert(lookup_res != NULL);
#ifdef MERACULOUS
    //assert(lookup_res->used_flag == USED);
    assert(lookup_res->used_flag > 0);
#endif
    // EGEOR: Use atomic reads for box_ptr and box_ptr->contig
    loop_until(
        //box_ptr = GET_CONTIG_BOX_OF_KMER(lookup_res, follow_list); contig = box_ptr->contig,
        box_ptr = GET_CONTIG_BOX_OF_KMER(lookup_res, follow_list); UPC_ATOMIC_GET_SHPTR(&contig, &(box_ptr->contig)),
        contig != NULL && IS_VALID_UPC_PTR(contig)
        );

    // can not make this assertion in parallel as the list can grow
    // assert( follow_list == 0 || box_ptr->next == NULL );

    return contig;
}

