
#ifndef __HIPMER_TIMEMORY_HPP
#define __HIPMER_TIMEMORY_HPP

//----------------------------------------------------------------------------//
#if defined(HIPMER_USE_TIMEMORY)

#    include <timemory/library.h>
#    include <timemory/timemory.hpp>

#else

#    include <ostream>
#    include <string>

namespace tim
{
template <typename... _Args>
void
timemory_init(_Args...)
{
}
inline void
timemory_finalize()
{
}
inline void
print_env()
{
}

/// this provides "functionality" for *_HANDLE macros
/// and can be omitted if these macros are not utilized
struct dummy
{
    template <typename... _Args>
    dummy(_Args&&...)
    {
    }
    ~dummy()            = default;
    dummy(const dummy&) = default;
    dummy(dummy&&)      = default;
    dummy& operator=(const dummy&) = default;
    dummy& operator=(dummy&&) = default;

    void start() {}
    void stop() {}
    void conditional_start() {}
    void conditional_stop() {}
    void report_at_exit(bool) {}
    template <typename... _Args>
    void mark_begin(_Args&&...)
    {
    }
    template <typename... _Args>
    void mark_end(_Args&&...)
    {
    }
    friend std::ostream& operator<<(std::ostream& os, const dummy&) { return os; }
};

}  // namespace tim

// startup/shutdown/configure
#    define TIMEMORY_INIT(...)
#    define TIMEMORY_FINALIZE()
#    define TIMEMORY_CONFIGURE(...)

// label creation
#    define TIMEMORY_BASIC_LABEL(...) std::string("")
#    define TIMEMORY_LABEL(...) std::string("")
#    define TIMEMORY_JOIN(...) std::string("")

// define an object
#    define TIMEMORY_BLANK_MARKER(...)
#    define TIMEMORY_BASIC_MARKER(...)
#    define TIMEMORY_MARKER(...)

// define an unique pointer object
#    define TIMEMORY_BLANK_POINTER(...)
#    define TIMEMORY_BASIC_POINTER(...)
#    define TIMEMORY_POINTER(...)

// define an object with a caliper reference
#    define TIMEMORY_BLANK_CALIPER(...)
#    define TIMEMORY_BASIC_CALIPER(...)
#    define TIMEMORY_CALIPER(...)

// define a static object with a caliper reference
#    define TIMEMORY_STATIC_BLANK_CALIPER(...)
#    define TIMEMORY_STATIC_BASIC_CALIPER(...)
#    define TIMEMORY_STATIC_CALIPER(...)

// invoke member function on caliper reference or type within reference
#    define TIMEMORY_CALIPER_APPLY(...)
#    define TIMEMORY_CALIPER_TYPE_APPLY(...)

// get an object
#    define TIMEMORY_BLANK_HANDLE(...) tim::dummy()
#    define TIMEMORY_BASIC_HANDLE(...) tim::dummy()
#    define TIMEMORY_HANDLE(...) tim::dummy()

// get a pointer to an object
#    define TIMEMORY_BLANK_POINTER_HANDLE(...) nullptr
#    define TIMEMORY_BASIC_POINTER_HANDLE(...) nullptr
#    define TIMEMORY_POINTER_HANDLE(...) nullptr

// debug only
#    define TIMEMORY_DEBUG_BLANK_MARKER(...)
#    define TIMEMORY_DEBUG_BASIC_MARKER(...)
#    define TIMEMORY_DEBUG_MARKER(...)

// auto-timers
#    define TIMEMORY_BLANK_AUTO_TIMER(...)
#    define TIMEMORY_BASIC_AUTO_TIMER(...)
#    define TIMEMORY_AUTO_TIMER(...)
#    define TIMEMORY_BLANK_AUTO_TIMER_HANDLE(...)
#    define TIMEMORY_BASIC_AUTO_TIMER_HANDLE(...)
#    define TIMEMORY_AUTO_TIMER_HANDLE(...)
#    define TIMEMORY_DEBUG_BASIC_AUTO_TIMER(...)
#    define TIMEMORY_DEBUG_AUTO_TIMER(...)

//--------------------------------------------------------------------------------------//

struct timemory_scoped_record
{
    timemory_scoped_record(const char* name) {}
    timemory_scoped_record(const char* name, const char* components) {}
    ~timemory_scoped_record() {}
};

//--------------------------------------------------------------------------------------//

#endif  // defined(HIPMER_USE_TIMEMORY)

//----------------------------------------------------------------------------//

#endif  // __HIPMER_TIMEMORY_HPP
