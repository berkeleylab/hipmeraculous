#ifndef __FILE_H
#define __FILE_H

#include <stdlib.h>
#include <stdio.h>
#include <zlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// INCLUDE other common header files too

#include "version.h"
#include "common_base.h"

#if defined (__cplusplus)
extern "C" {
#endif

/****************************************************************************************/

#define fclose_track(f) fclose_track_at_line(f, __FILENAME__, __LINE__)
int fclose_track_at_line(FILE *f, const char *which_file, int which_line);

#define fopen_track(p, m) fopen_track_at_line(p, m, __FILENAME__, __LINE__)
FILE *fopen_track_at_line(const char *path, const char *mode, const char *which_file, int which_line);

#define fopen_chk(p, m) fopen_chk_at_line(p, m, __FILENAME__, __LINE__)
FILE *fopen_chk_at_line(const char *path, const char *mode, const char *which_file, int which_line);

#define close_track(f) close_track_at_line(f, __FILENAME__, __LINE__)
int close_track_at_line(int fd, const char *which_file, int which_line);

#define open_chk(p, flags, mode) open_chk_at_line(p, flags, mode, __FILENAME__, __LINE__)
int open_chk_at_line(const char *path, int flags, mode_t mode, const char *which_file, int which_line);

#define fread_chk(ptr, size, nmemb, stream) fread_chk_at_line(ptr, size, nmemb, stream, __FILENAME__, __LINE__)
size_t fread_chk_at_line(void *ptr, size_t size, size_t nmemb, FILE *stream, const char *which_file, int which_line);

#define fwrite_chk(ptr, size, nmemb, stream) fwrite_chk_at_line(ptr, size, nmemb, stream, __FILENAME__, __LINE__)
size_t fwrite_chk_at_line(const void *ptr, size_t size, size_t nmemb, FILE *stream, const char *which_file, int which_line);

typedef gzFile GZIP_FILE;
#define GZIP_GETS(b, l, f) gzgets(f, b, l)
#define GZIP_OPEN gzopen_track
#define GZIP_OPEN_RANK_PATH gzopen_rank_path
#define GZIP_CLOSE gzclose_track
#define GZIP_REWIND gzrewind
#define GZIP_FWRITE(ptr, size, n, f) gzwrite_chk(f, ptr, ((lld)size) * ((lld)n))
#define GZIP_FREAD(ptr, size, n, f) gzread_chk(f, ptr, ((lld)size) * ((lld)n))
#define GZIP_FTELL gztell
#define GZIP_EOF gzeof
#define GZIP_PRINTF /*** WARNING gzprintf is limited to 8191 bytes! ***/ gzprintf
#define GZIP_EXT ".gz"

#define gzopen_track(p, m) gzopen_track_at_line(p, m, __FILENAME__, __LINE__)
gzFile gzopen_track_at_line(const char *path, const char *_mode, const char *which_file, int which_line);

#define gzopen_chk(p, m) gzopen_chk_at_line(p, m, __FILENAME__, __LINE__)
gzFile gzopen_chk_at_line(const char *path, const char *mode, const char *which_file, int which_line);

#define gzclose_track(gz) gzclose_track_at_line(gz, __FILENAME__, __LINE__)
int gzclose_track_at_line(gzFile gz, const char *which_file, int which_line);

#define gzclose_chk(gz) gzclose_chk_at_line(gz, __FILENAME__, __LINE__)
void gzclose_chk_at_line(gzFile gz, const char *which_file, int which_line);

#define fopen_rank_path(p, m, r) fopen_rank_path_at_line(p, m, r, __FILENAME__, __LINE__)
FILE *fopen_rank_path_at_line(char *buf, const char *mode, int rank, const char *which_file, int which_line);

#define gzopen_rank_path(p, m, r) gzopen_rank_path_at_line(p, m, r, __FILENAME__, __LINE__)
gzFile gzopen_rank_path_at_line(char *buf, const char *mode, int rank, const char *which_file, int which_line);

#define gzread_chk(file, buf, len) gzread_chk_at_line(file, (char*) buf, len, __FILENAME__, __LINE__)
int64_t gzread_chk_at_line(gzFile file, char * buf, uint64_t len, const char *which_file, int which_line);

#define gzwrite_chk(file, buf, len) gzwrite_chk_at_line(file, (const char*) buf, len, __FILENAME__, __LINE__)
int64_t gzwrite_chk_at_line(gzFile file, const char * buf, uint64_t len, const char *which_file, int which_line);

#define link_chk(o, n) link_chk_at_line(o, n, __FILENAME__, __LINE__)
void link_chk_at_line(const char *old_file, const char *new_file, const char *which_file, int which_line);

size_t copyFile(const char *source, const char *dest);
size_t copyFile2(FILE *source, FILE *dest);

#if defined (__cplusplus)
}
#endif


#endif // __FILE_H
