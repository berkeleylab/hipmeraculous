#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

#if defined (__cplusplus)
extern "C" {
#endif

typedef enum BUFFER_FLAGS { FIXED_BUFFER = 1, READONLY_BUFFER = 2, EXTERNAL_ALLOC_BUFFER = 4, EOF_BUFFER = 8 } BUFFER_FLAGS_TYPE;
typedef struct {
    char * buf;
    size_t pos, len, size, flags;
} BufferBase;
typedef BufferBase *Buffer;

typedef struct _BufferList {
    Buffer              buffer;
    struct _BufferList *next;
} _BufferList;
typedef struct {
    _BufferList *head;
    _BufferList *tail;
} BufferList;

Buffer initBuffer(size_t initSize);
void attachBuffer(Buffer b, char *buf, size_t pos, size_t len, size_t size);
size_t growBuffer(Buffer b, size_t appendSize);
size_t growBufferMax(Buffer b, size_t requestedSize);
void reserveBuffer(Buffer b, size_t size);
void *allocFromBuffer(Buffer b, size_t bytes);
void resetBuffer1(Buffer b);
void resetBuffer(Buffer b);
char *resetRawBuffer(Buffer b, size_t newSize);
void rewindBuffer(Buffer b);

#define freeBuffer(b) _freeBuffer(&(b), __FILENAME__, __LINE__)
void _freeBuffer(Buffer *b, const char *which_file, int which_line);
char *releaseBuffer(Buffer b);
int isValidBuffer(Buffer b);
int isValidBufferNullOK(Buffer b);
int isEmptyBuffer(Buffer b);

BufferList initBufferList();
void freeBufferList(BufferList bl);
Buffer extendBufferList(BufferList bl, size_t initSize);
Buffer getBuffer(BufferList bl);

size_t getPosBuffer(Buffer b);
size_t getReadLengthBuffer(Buffer b);
size_t getLengthBuffer(Buffer b);
size_t getSizeBuffer(Buffer b);
size_t appendNullBuffer(Buffer b);
char *getStartBuffer(Buffer b);
char *getEndBuffer(Buffer b);
char *appendBuffer(Buffer b, size_t bytes);
char *getCurBuffer(Buffer b);
size_t moveCurBuffer(Buffer b, size_t bytes);
void truncateBuffer(Buffer b, size_t pos);

void *getElementBuffer(Buffer b, size_t elementSize, size_t idx);

size_t writeBuffer(Buffer b, const void *data, size_t size);
size_t readBuffer(Buffer b, void *data, size_t size);

size_t writeFileBuffer(Buffer b, FILE *f, size_t myOffset);
size_t writeFileBuffer2(Buffer b, FILE *f);
size_t readFileBuffer(Buffer b, FILE *f, size_t myOffset, size_t len);
size_t readFileBuffer2(Buffer b, FILE *f, size_t len);

size_t writeFDBuffer(Buffer b, int fd, size_t myOffset);

size_t scanfBuffer(Buffer b, const char *fmt, ...);

size_t printfBuffer(Buffer b, const char *fmt, ...);
char *fgetsBuffer(Buffer b, size_t size, FILE *stream);
char *getsBufferIntoBuffer(Buffer src, Buffer dst);

char *getsBuffer(Buffer src, char * dst, int n);

#include <zlib.h>
char *gzgetsBuffer(Buffer b, size_t size, gzFile gz);
void *memcpyBuffer(Buffer b, const void *src, size_t n);
void *memsetBuffer(Buffer b, int c, size_t n);
char *strcatBuffer(Buffer b, const char *src);
char *strcpyBuffer(Buffer b, const char *src);
char *strncpyBuffer(Buffer b, const char *src, size_t n);
size_t chompBuffer(Buffer b);
void copyBuffer(Buffer dest, Buffer src);

void setBufferForFile(Buffer b, FILE *f);

void swapBuffer(Buffer a, Buffer b);

uint32_t getAdler32Buffer(Buffer b);


// BufferVector dynamically growing typed array
#define BV_TYPE(TYPE) Buffer

#define BV_VAR(VAR) VAR ## _BV_BUF

#define BV_TYPE2(VAR) VAR ## _BV_TYPE

#define BV_INIT2(TYPE, VAR, INIT_VAL) \
    typedef TYPE BV_TYPE2(VAR); \
    Buffer BV_VAR(VAR) = INIT_VAL

#define BV_INIT(TYPE, VAR) \
    BV_INIT2(TYPE, VAR, initBuffer(sizeof(TYPE) * 16))

#define BV_PTR(VAR, IDX) ( (BV_TYPE2(VAR) *) getElementBuffer(BV_VAR(VAR), sizeof( BV_TYPE2(VAR) ), (IDX)) )

#define BV_GET(VAR, IDX) (*BV_PTR(VAR, IDX))

#define BV_RESET(VAR) resetBuffer( BV_VAR(VAR) )

#define BV_FREE(VAR) freeBuffer( BV_VAR(VAR) )


#if defined (__cplusplus)
}
#endif


#endif
