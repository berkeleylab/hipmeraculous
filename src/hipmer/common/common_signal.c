#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>


void common_noop_handler(int signal)
{
}

int set_common_signal_handler(int signal, void (*sa_handler)(int))
{
    struct sigaction new_action;

    new_action.sa_handler = sa_handler;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;

    if (sigaction(signal, &new_action, NULL) != 0) {
        fprintf(stderr, "WARNING: can't install signal handler for signal %s!\n", signal);
        fflush(stderr);
        return 1;
    }
    return 0;
}

void unset_common_signal_handler(int signal)
{
    if (sigaction(signal, NULL, NULL) != 0) {
        fprintf(stderr, "WARNING: can't install signal handler for signal %s!\n", signal);
        fflush(stderr);
    }
}
