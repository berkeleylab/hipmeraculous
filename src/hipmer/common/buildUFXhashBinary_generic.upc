#include "buildUFXhashBinary_generic.h"

HASH_TABLE_T *BUILD_UFX_HASH(int64_t size, MEMORY_HEAP_T *memory_heap_res, int64_t myShare, int64_t dsize, int64_t dmin, double dynamic_dmin, int64_t CHUNK_SIZE, int load_factor, int kmer_len, struct ufx_file_t *UFX_f)
{
    HASH_TABLE_T *dist_hashtable = NULL;
    MEMORY_HEAP_T memory_heap;
    shared LIST_T *lookup_res = NULL;
    int64_t chars_read, cur_chars_read, ptr, retval, i;
    int64_t buffer_size, offset;
    int64_t num_chars;

    shared[1] int64_t * heap_sizes = NULL;
    int64_t *my_heap_sizes, my_heap_size, my_ufx_lines, idx;
    int *ufx_remote_thread = NULL, remote_thread;
    int64_t hashval;

    UPC_TICK_T start_read, end_read, start_storing, end_storing, start_setup, end_setup, start_calculation, end_calculation;

    UPC_ALL_ALLOC_CHK(heap_sizes, THREADS, sizeof(int64_t));
    heap_sizes[MYTHREAD] = 0;

    my_ufx_lines = myShare;

    if (my_ufx_lines) {
        ufx_remote_thread = (int *)malloc_chk(my_ufx_lines * sizeof(int));
    }

    DBG("Thread %d: Preparing to read %lld UFX lines\n", MYTHREAD, (lld)my_ufx_lines);

    int64_t kmer_count = 0;

    /* Initialize lookup-table --> necessary for packing routines */
    init_LookupTable();
    char **kmersarr = NULL;
    int *counts = NULL;
    char *lefts = NULL;
    char *rights = NULL;

    start_read = UPC_TICKS_NOW();

    int64_t kmers_read = UFXRead(&kmersarr, &counts, &lefts, &rights, my_ufx_lines, dmin, dynamic_dmin, 0, MYTHREAD, kmer_len, UFX_f);
    if (kmers_read < 0) {
        DIE("There was a problem reading from ufx\n");
    }
    DBG("Thread %d: Loaded %lld kmers\n", MYTHREAD, (lld)kmers_read);

    //FILE *aux = fopen_chk("conerted.txt", "w");
    //char curkmer[kmer_len+1];
    //curkmer[kmer_len] = '\0';
    //for (i=0; i< kmers_read; i++) {
    //   fprintf( aux ,"%s\t%c%c\n", kmersarr[i], lefts[i], rights[i]);

    //}

    UPC_LOGGED_BARRIER;
    serial_printf("Threads done with I/O - loaded %lld UFX (%0.3f MB private)\n", (lld) kmers_read, kmers_read * (1.0 / ONE_MB) * (kmer_len+1 + sizeof(char*) + sizeof(int) + 2*sizeof(char)) );

    end_read = UPC_TICKS_NOW();
    if (_sv != NULL) {
        MYSV.fileIOTime = UPC_TICKS_TO_SECS(end_read - start_read);
    }

    my_heap_sizes = (int64_t *)calloc_chk(THREADS, sizeof(int64_t));

    start_calculation = UPC_TICKS_NOW();
    /* calculate the exact number of UFX entries for each thread */
    /* previously estimated as EXPANSION_FACTOR * (size + size % THREADS) / THREADS */
    idx = 0;
    char rc_kmer[MAX_KMER_SIZE];
    assert(kmer_len < MAX_KMER_SIZE);
    memset(rc_kmer, 0, MAX_KMER_SIZE);
    rc_kmer[kmer_len] = '\0';
    int is_least;
    int64_t hashTableSize = load_factor * size;

    for (ptr = 0; ptr < kmers_read; ptr++) {
        reverseComplementKmer(kmersarr[ptr], rc_kmer, kmer_len);
        is_least = 0;
        is_least = (strcmp(kmersarr[ptr], rc_kmer) > 0) ? 0  : 1;
        if (!is_least) {
            WARN("Found invalid kmer at %lld of %lld: %.*s vs %.*s\n",
                 (lld)ptr, (lld)kmers_read, kmer_len, kmersarr[ptr], kmer_len, rc_kmer);
        }
        assert(is_least);


#ifdef MERACULOUS
        if (is_least && (lefts[ptr] != 'F') && (lefts[ptr] != 'X') && (rights[ptr] != 'F') && (rights[ptr] != 'X'))
#endif

#ifdef MERDEPTH
        if (is_least)
#endif

#ifdef CEA
        if (is_least)
#endif

#ifdef NB_EXPLORE
        if (is_least)
#endif

        {
            hashval = hashkmer(hashTableSize, (char *)kmersarr[ptr], kmer_len);
            remote_thread = hashval % (THREADS * BS);
            my_heap_sizes[remote_thread]++;
            ufx_remote_thread[idx] = remote_thread;
        } else {
            if (!is_least) {
                DIE("Encountered non-canonical kmer=%.*s should_be=%.*s ptr=%lld idx=%lld ufx_remote_thread[idx]=%d\n", kmer_len, kmersarr[ptr], kmer_len, rc_kmer, (lld)ptr, (lld)idx, ufx_remote_thread[idx]);
            }
            ufx_remote_thread[idx] = -1;
        }
        idx++;
    }

    assert(idx == my_ufx_lines);
    if (idx != my_ufx_lines) {
        DIE("Did not read all the lines in my ufx: idx=%lld my_ufx_lines=%lld\n", (lld) idx, (lld) my_ufx_lines);
    }

    /* now atomically add all my_sizes to global size heap */
    /* TODO replace with all reduce? */
    for (i = MYTHREAD; i < THREADS + MYTHREAD; i++) {
        if (HIPMER_VERBOSITY >= LL_DBG) {
            long long ll_my_heap_sizes = my_heap_sizes[i % THREADS];
            DBG2("sending %lld to thread %d\n", ll_my_heap_sizes, (int)(i % THREADS));
        }
        assert(upc_threadof(heap_sizes + (i % THREADS)) == (i % THREADS));
        if (my_heap_sizes[i % THREADS] > 0) {
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, heap_sizes + (i % THREADS), my_heap_sizes[i % THREADS]);
        }
    }
    free_chk(my_heap_sizes);
    UPC_LOGGED_BARRIER;
    assert(upc_threadof(heap_sizes + MYTHREAD) == MYTHREAD);
    my_heap_size = heap_sizes[MYTHREAD];
    UPC_ALL_FREE_CHK(heap_sizes);     // should clean up after last thread gets here
    DBG("Thread %d: allocating memory for %lld kmers\n", MYTHREAD, (lld)my_heap_size);

    end_calculation = UPC_TICKS_NOW();
    if (_sv != NULL) {
        MYSV.cardCalcTime = UPC_TICKS_TO_SECS(end_calculation - start_calculation);
    }

    start_setup = UPC_TICKS_NOW();

    /* Create and initialize hashtable */
    dist_hashtable = CREATE_HASH_TABLE(hashTableSize, &memory_heap, my_heap_size);
    UPC_LOGGED_BARRIER;

    end_setup = UPC_TICKS_NOW();
    if (_sv != NULL) {
        MYSV.setupTime = UPC_TICKS_TO_SECS(end_setup - start_setup);
    }

    if (MYTHREAD == 0) {
        printf("Threads done with setting-up\n");
    }

    start_storing = UPC_TICKS_NOW();

    ptr = 0;
    idx = 0;
    LIST_T new_entry;
    memset(&new_entry, 0, sizeof(LIST_T));     // initialize all bytes, including padding

    UPC_INT64_T store_pos;
    char exts[2];

    int64_t skipped = 0;
    while (ptr < kmers_read) {
        /* Set end of string chars to the appropriate positions:       */
        /* Pointer: working_buffer+ptr                ---> current kmer        */
        /* Pointer: working_buffer+ptr+kmer_len+1  ---> current extensions  */

        if (ufx_remote_thread[idx] >= 0) {
            // get cached remote thread
            remote_thread = ufx_remote_thread[idx];

#ifdef DEBUG
            /* if DEBUG, verify the calculated remote thread is actually the correct one */
            assert(dist_hashtable->size == hashTableSize);
            int64_t hashval2 = hashkmer(dist_hashtable->size, (char *)kmersarr[ptr], kmer_len);
            assert(remote_thread == upc_threadof(&(dist_hashtable->table[hashval2])));
#endif
            packSequence((unsigned char *)(kmersarr[ptr]), new_entry.packed_key, kmer_len);

#ifdef MERACULOUS
            exts[0] = lefts[ptr];
            exts[1] = rights[ptr];
            new_entry.packed_extensions = convertExtensionsToPackedCode((unsigned char *)exts);
            new_entry.used_flag = UNUSED;
            new_entry.my_contig = NULL;
#endif

#ifdef MERDEPTH
            new_entry.count = counts[ptr];
            new_entry.left_ext = lefts[ptr];
            new_entry.right_ext = rights[ptr];
#endif

#ifdef CEA
            new_entry.left_ext = lefts[ptr];
            new_entry.right_ext = rights[ptr];
#endif

#ifdef NB_EXPLORE
            new_entry.count = counts[ptr];
            new_entry.left_ext = lefts[ptr];
            new_entry.right_ext = rights[ptr];
            new_entry.contID = -1;
#endif

            new_entry.next = NULL;

            ADD_TO_HEAP(&memory_heap, new_entry, remote_thread);
#ifdef DEBUG
            kmer_count++;
#endif
        } else {
            skipped++;
            DBG2("Thread %d: Skipping %.*s\n", MYTHREAD, kmer_len + 3, kmersarr[ptr]);
        }

        ptr++;
        idx++;
    }
    LOGF("Skipped %lld of %lld kmers not assigned to a thread. kmer_count=%lld\n", skipped, kmers_read, kmer_count);

    assert(idx == my_ufx_lines);

    if (ufx_remote_thread) free_chk(ufx_remote_thread);

    FINAL_FLUSH_HEAP(&memory_heap);
    FREE_LOCAL_BUFFERS(&memory_heap);

    UPC_LOGGED_BARRIER;

    assert(memory_heap.heap_struct[MYTHREAD].len == my_heap_size);
    /* Now each thread will iterate over its local heap and will add nodes to the local part of the hashtable - NO NEED FOR LOCKS!!! */
    int64_t heap_entry;

#ifdef DEBUG
    for (i = 0; i < THREADS; i++) {
        if (memory_heap.cached_heap_ptrs[i] != memory_heap.heap_struct[i].ptr) {
            DIE("Invalid cached heap for thread %d\n", (int)i);
        }
    }
#endif
    assert(upc_threadof(memory_heap.cached_heap_ptrs[MYTHREAD]) == MYTHREAD);
    LIST_T *local_filled_heap = (LIST_T *)memory_heap.cached_heap_ptrs[MYTHREAD];
    assert(hashTableSize == dist_hashtable->size);
    shared[] LIST_T * shared_local_filled_heap = (memory_heap.heap_struct[MYTHREAD].ptr);
    //shared[] LIST_T* local_filled_heap = (memory_heap.heap_struct[MYTHREAD].ptr);
    unsigned char unpacked_kmer[MAX_KMER_SIZE];
    assert(kmer_len < MAX_KMER_SIZE);
    memset(unpacked_kmer, 0, MAX_KMER_SIZE);
    unpacked_kmer[kmer_len] = '\0';

#ifdef MERACULOUS

    LOGF("Processing %lld heap_entries for MERACULOUS LIST_T type (%d bytes)\n", (lld)memory_heap.heap_struct[MYTHREAD].len, (int)sizeof(LIST_T));
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
        unpackSequence((unsigned char *)&(local_filled_heap[heap_entry].packed_key), (unsigned char *)unpacked_kmer, kmer_len);
        hashval = hashkmer(dist_hashtable->size, (char *)(unpacked_kmer), kmer_len);
        local_filled_heap[heap_entry].next = dist_hashtable->table[hashval].head;
        dist_hashtable->table[hashval].head = (shared[] LIST_T *) & (shared_local_filled_heap[heap_entry]);
        assert(upc_threadof(&dist_hashtable->table[hashval]) == upc_threadof(dist_hashtable->table[hashval].head));
        assert(upc_threadof(&dist_hashtable->table[hashval]) == MYTHREAD);
    }

#endif

#ifndef MERACULOUS

    int found;
    LIST_T *result;

    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
    assert(kmer_packed_len <= MAX_KMER_PACKED_LENGTH);
    assert(local_filled_heap == (LIST_T *)memory_heap.cached_heap_ptrs[MYTHREAD]);
    assert(upc_threadof(memory_heap.heap_struct[MYTHREAD].ptr) == MYTHREAD);
    assert(local_filled_heap == (LIST_T *)memory_heap.heap_struct[MYTHREAD].ptr);
    if (memory_heap.heap_struct[MYTHREAD].ptr != memory_heap.cached_heap_ptrs[MYTHREAD]) {
        DIE("invalid cached_heap_ptrs\n");
    }
    LOGF("Processing %lld heap_indicies for non MERACULOUS LIST_T type (%d bytes)\n", (lld)memory_heap.heap_struct[MYTHREAD].len, (int)sizeof(LIST_T));
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
        unpackSequence((unsigned char *)&(local_filled_heap[heap_entry].packed_key), (unsigned char *)unpacked_kmer, kmer_len);
        hashval = hashkmer(dist_hashtable->size, (char *)(unpacked_kmer), kmer_len);

        /* Find if the entry is not there (an entry might be there due to either (1) ufx file or (2) kmers generated by contigs */

        found = 0;
        if (hashval % (THREADS * BS) != MYTHREAD) {
            WARN("my heap has a kmer for a different thread (%lld)! heap_entry=%lld, hashval=%lld, kmer=%.*s\n", (lld)(hashval % (THREADS * BS)), (lld)heap_entry, (lld)hashval, kmer_len, unpacked_kmer);
        }
        assert(upc_threadof(&(dist_hashtable->table[hashval])) == MYTHREAD);
        assert(hashval % (THREADS * BS) == MYTHREAD);
        if (dist_hashtable->table[hashval].head != NULL) {
            assert(upc_threadof(dist_hashtable->table[hashval].head) == MYTHREAD);
        }
        result = (LIST_T *)dist_hashtable->table[hashval].head;

        for (; result != NULL; ) {
            if (comparePackedSeq((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)result->packed_key, kmer_packed_len) == 0) {
                found = 1;
                break;
            }
            if (result->next != NULL) {
                assert(upc_threadof(result->next) == MYTHREAD);
            }
            result = (LIST_T *)result->next;
        }

        if (found == 0) {
            /* Entry is from UFX, just add it in the hash table */
            local_filled_heap[heap_entry].next = dist_hashtable->table[hashval].head;
            dist_hashtable->table[hashval].head = (shared[] LIST_T *) & (shared_local_filled_heap[heap_entry]);
            assert(upc_threadof(&dist_hashtable->table[hashval]) == upc_threadof(dist_hashtable->table[hashval].head));
        } else {
            /* Prioritize the non FX entry because this was used in the traversal by default! */
            if ((local_filled_heap[heap_entry].right_ext != 'F') && (local_filled_heap[heap_entry].right_ext != 'X') && (local_filled_heap[heap_entry].left_ext != 'F') && (local_filled_heap[heap_entry].left_ext != 'X')) {
                result->right_ext = local_filled_heap[heap_entry].right_ext;
                result->left_ext = local_filled_heap[heap_entry].left_ext;
            }
        }
    }


#endif

    UPC_LOGGED_BARRIER;

    end_storing = UPC_TICKS_NOW();
    if (_sv != NULL) {
        MYSV.storeTime = UPC_TICKS_TO_SECS(end_storing - start_storing);
    }

#ifdef PROFILE
    UPC_LOGGED_BARRIER;

    if (MYTHREAD == 0) {
        printf("\n************* SET - UP TIME *****************");
        printf("\nTime spent on setting up the distributed hash table is %f seconds\n", UPC_TICKS_TO_SECS(end_setup - start_setup));

#ifdef DETAILED_IO_PROFILING
        printf("\n\n************* DETAILED TIMINGS FOR I/O AND STORING KMERS *****************\n");
#endif
    }

    UPC_LOGGED_BARRIER;

#ifdef DETAILED_IO_PROFILING

    printf("Thread %d spent %f seconds on read I/O and %f seconds on storing %lld kmers\n", MYTHREAD, UPC_TICKS_TO_SECS(end_read - start_read), UPC_TICKS_TO_SECS(end_storing - start_storing));

#endif

    UPC_LOGGED_BARRIER;

    DeAllocateAll(&kmersarr, &counts, &lefts, &rights, kmers_read);

#ifdef BUCKET_BALANCE_PROFILING
    if (MYTHREAD == 0) {
        printf("\n--------------------------------\n");
        printf("--------- Bucket balance -------\n");
        printf("--------------------------------\n");
    }
    if (MYTHREAD == 0) {
        for (i = 0; i < THREADS; i++) {
            printf("Thread %lld has %lld elements in its buckets\n", i, (lld) memory_heap.heap_struct[i].len);
        }
    }
#endif

#endif

    UPC_LOGGED_BARRIER;

    (*memory_heap_res) = memory_heap;
    return dist_hashtable;
}
