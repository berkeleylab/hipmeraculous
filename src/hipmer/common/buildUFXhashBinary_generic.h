#ifndef BUILD_UFX_HASH_GENERIC_H
#define BUILD_UFX_HASH_GENERIC_H

#include <stdio.h>
#include <sys/stat.h>
#include <assert.h>

#include "meraculous.h"
#include "StaticVars.h"
#include "common.h"
#include "timers.h"

#ifndef CONTIGS_DDS_TYPE
#error "You must specify CONTIGS_DDS_TYPE before including this code"
#endif

#include "kmer_hash_generic.h"
#include "../contigs/contigs_dds.h"
#include "../contigs/packingDNAseq.h"

#include "../kcount/readufx.h"

#define BUILD_UFX_HASH JOIN(CONTIGS_DDS_TYPE, build_ufx_hash)
HASH_TABLE_T *BUILD_UFX_HASH(int64_t size, MEMORY_HEAP_T *memory_heap_res, int64_t myShare, int64_t dsize, int64_t dmin, double dynamic_dmin, int64_t CHUNK_SIZE, int load_factor, int kmer_len, struct ufx_file_t *UFX_f);

#endif // BUILD_UFX_HASH_GENERIC_H
