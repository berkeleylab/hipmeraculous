
#ifndef __HIPMER_TIMEMORY_H
#define __HIPMER_TIMEMORY_H

#if defined(HIPMER_USE_TIMEMORY)
#    include <timemory/ctimemory.h>
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

extern uint64_t
timemory_get_unique_id(void);
extern void
timemory_create_record(const char* name, uint64_t* id, int n, int* ct);
extern void
timemory_delete_record(uint64_t nid);
extern void
timemory_init_library(int argc, char** argv);
extern void
timemory_finalize_library(void);
extern void
timemory_set_default(const char* components);
extern void
timemory_push_components(const char* components);
extern void
timemory_pop_components(void);
extern void
timemory_begin_record(const char* name, uint64_t* id);
extern void
timemory_begin_record_types(const char* name, uint64_t*, const char*);
extern uint64_t
timemory_get_begin_record(const char* name);
extern uint64_t
timemory_get_begin_record_types(const char* name, const char* ctypes);
extern void
timemory_end_record(uint64_t id);

typedef void (*timemory_create_func_t)(const char*, uint64_t*, int, int*);
typedef void (*timemory_delete_func_t)(uint64_t);

extern timemory_create_func_t timemory_create_function;
extern timemory_delete_func_t timemory_delete_function;

#endif  // __HIPMER_TIMEMORY_H
