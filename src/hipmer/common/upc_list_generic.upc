#include "upc_list_generic.h"

// allocate a new node with next == NULL
LIST_T_SHPTR ALLOC_NODE(const UPC_LIST_TYPE data)
{
    LIST_T_SHPTR newNode = NULL;
    UPC_ALLOC_CHK(newNode, sizeof(LIST_T));
    newNode->next = NULL;
    upc_memput(&newNode->data, &data, sizeof(UPC_LIST_TYPE));
    return newNode;
}

// free memory for this single node.  No checks against the value of next
void FREE_NODE(LIST_T_SHPTR node)
{
    node->next = NULL;
    UPC_FREE_CHK(node);
}

// free all nodes in the list
// returns the count that was freed
size_t FREE_LIST(LIST_T_HEAD_SHPTR head)
{
    size_t count = 0;
    if (head == NULL) DIE("a LIST_T_HEAD should never be NULL\n");
    LIST_T_SHPTR node = NULL, next = NULL;
    UPC_ATOMIC_GET_SHPTR(&node, head);
    while( node != NULL ) {
        UPC_ATOMIC_GET_SHPTR(&next, &(node->next));
        FREE_NODE(node);
        node = next;
        count++;
    }
    *head = NULL;
    return count;
}

void PUSH_NODE_FRONT(LIST_T_HEAD_SHPTR head, LIST_T_SHPTR node)
{
    assert(node != NULL);
    if (head == NULL) DIE("a LIST_T_HEAD_SHPTR should never be NULL\n");
    DBG2("PUSH_NODE_FRONT( head = %d:%d:%x -> %d:%d:%x, node = %d:%d:%x)\n", upc_threadof(head), upc_phaseof(head), upc_addrfield(head), upc_threadof(*head), upc_phaseof(*head), upc_addrfield(*head), upc_threadof(node), upc_phaseof(node), upc_addrfield(node));
    LIST_T_SHPTR oldHead = NULL, test = NULL;
    long iterations = 0;
    UPC_ATOMIC_GET_SHPTR(&test, head);
    while(1) {
        oldHead = test;
        node->next = oldHead;
        upc_fence;
        UPC_ATOMIC_CSWAP_SHPTR(&test, head, oldHead, node);
        if (test == oldHead) {
            break;
        }
        if (++iterations % 100000 == 0) {
            WARN("Possible infinite loop PUSH_NODE_FRONT: iterations=%lld\n", (lld) iterations);
        }
        UPC_POLL;
    }
    if (iterations) {
        DBG("Missed %lld CSWAP attempts\n", (lld) iterations);
    }
}

LIST_T_SHPTR POP_NODE_FRONT(LIST_T_HEAD_SHPTR head)
{
    if (head == NULL) DIE("a LIST_T_HEAD_SHPTR should never be NULL\n");
    LIST_T_SHPTR node = NULL, tmp = NULL, newHead = NULL;
    long iterations = 0;
    UPC_ATOMIC_GET_SHPTR(&node, head);
    while(1) {
        if (node == NULL) {
            break;
        }
        UPC_ATOMIC_GET_SHPTR(&newHead, &(node->next));
        UPC_ATOMIC_CSWAP_SHPTR(&tmp, head, node, newHead);
        if (tmp == node) {
            break;
        }
        if (++iterations % 100000 == 0) {
            WARN("Possible infinite loop POP_NODE_FRONT: iterations=%lld\n", (lld) iterations);
        }
        node = tmp;
        UPC_POLL;
    }
    if (iterations) {
        DBG("Missed %lld CSWAP attempts\n", (lld) iterations);
    }
    return node;
}


