#ifndef _POC_COMMON_H_
#define _POC_COMMON_H_

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "common_base.h"

int *__get_rank()
{
    static int _ = 0;

    return &_;
}
int *__get_num_ranks()
{
    static int _ = 1;

    return &_;
}

void _set_rank_and_size(int myRank, int ourSize, const char * file, int line)
{
    *__get_rank() = myRank;
    *__get_num_ranks() = ourSize;
}

int _get_rank(const char *file, int line)
{
    return *__get_rank();
}
int _get_num_ranks(const char *file, int line)
{
    return *__get_num_ranks();
}

void _common_exit(int x, const char * file, int line)
{
    if (x) fprintf(stderr, "[%s:%d] called common_exit with value (%d)\n", file, line, x);
    fflush(stdout);
    fflush(stderr);
    if (x) assert(!x);
    exit(x);
}

#include "StaticVars.h"
void initAtomicMetadata(_StaticVars *mySV)
{
}
void destroyAtomicMetadata(_StaticVars *mySV)
{
}
void delete_all_checkpoints(_StaticVars *mySV)
{
}

int isOnSameNode(int threada, int threadb)
{
    if (_sv) {
        return threada / MYSV.cores_per_node == threadb / MYSV.cores_per_node;
    } else { return 1; /* poc_common should only be used in smp contexts... */
    }
}

int isOnMyNode(int threada)
{
    return isOnSameNode(MYTHREAD, threada);
}

#endif // _POC_COMMON_H_
