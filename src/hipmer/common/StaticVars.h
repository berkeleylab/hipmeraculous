#ifndef __STATIC_VARS_H
#define __STATIC_VARS_H

#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include "defines.h"
#include "Buffer.h"
#include "rb_tree.h"

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct {
    int64_t count;
    double  time;
} CountTimer;
CountTimer initCountTimer(CountTimer *ct);
void addCountTime(CountTimer *ct, double t);

struct _HipMerAtomicMetadata;                                // forward declaration
typedef struct _HipMerAtomicMetadata HipMerAtomicMetadata_t; // forward declaration
typedef struct {
    FILE *                  _my_log;
    FILE *                  _my_diags;
    FILE *                  _dbg_file;
    double                  fileIOTime, cardCalcTime, setupTime, storeTime;
    CountTimer              barriers, fadds, cswaps, gets, sets, atomicLocks, checkpointSetup, checkpointSync, contigLocks;
    int                     _logTime;
    int                     logVerbosity;
    int                     _num_warnings;
    int                     cores_per_node;
    char                    flushLog;
    char                    syncAllCheckpoints;
    char                    logMemcheck;
    char                    _logMemcheckLast;
    char                    initByMethod;
    char                    testCheckpointing;
    char *                  LOG_PREFIX;
    char *                  cached_io_path;
    char *                  checkpoint_path;
    Buffer                  logBuffer;
    int64_t                 outstandingMallocs;
    int64_t                 untracked_outstandingMallocs;
    int64_t                 outstandingUPCAllocs;
    int64_t                 untracked_outstandingUPCAllocs;
    int64_t                 outstandingUPCAllAllocs;
    int64_t                 upc_alloc_watermark;
    int64_t                 upc_alloc_watermark_count;
    int64_t                 upc_all_alloc_watermark;
    int64_t                 upc_all_alloc_watermark_first;
    HipMerAtomicMetadata_t *optionalAtomicMetadata;
    void *                  interModuleState;
    struct rb_tree *        checkpointFiles;
} _StaticVars;

#define MYSV _sv[MYTHREAD]
#define LOG_SET_FLUSH(x) do { if (_sv) { MYSV.flushLog = x; } } while (0)
#define LOG_SET_MEMCHECK(x) do { \
        if (_sv) { \
            char tmp = MYSV._logMemcheckLast; \
            if (MYSV.logMemcheck) { MYSV._logMemcheckLast = MYSV.logMemcheck; } \
            MYSV.logMemcheck = ((x) < 0) ? tmp : (x); \
        } \
} while (0)

#ifdef DEBUG
#define LOG_RESET_MEMCHECK LOG_SET_MEMCHECK(1)
#else
#define LOG_RESET_MEMCHECK LOG_SET_MEMCHECK(0)
#endif
#define MEMCHECK_COUNT0(x) do { if (_sv) { MYSV.untracked_outstandingMallocs += x; } } while (0)
#define MEMCHECK_COUNT(x) do { if (_sv) { MYSV.outstandingMallocs += x; } } while (0)
#define MEMCHECK_UPCCOUNT0(x) do { if (_sv) { MYSV.untracked_outstandingUPCAllocs += x; } } while (0)
#define MEMCHECK_UPCCOUNT(x) do { if (_sv) { MYSV.outstandingUPCAllocs += x; } } while (0)
#define MEMCHECK_UPCALLCOUNT(x) do { if (_sv) { MYSV.outstandingUPCAllAllocs += x; } } while (0)

/* set up StaticVars */

void initAtomicMetadata(_StaticVars *);
void destroyAtomicMetadata(_StaticVars *);
void delete_all_checkpoints(_StaticVars *);

#define TIDY_UP_STATIC_VARS do { \
        serial_printf("Cleaning up global variables\n"); \
        destroyAtomicMetadata(&(MYSV)); \
        delete_all_checkpoints(&(MYSV)); \
    } while (0)

#if 0 // enable when MPI is gone...

typedef shared[1] _StaticVars *StaticVars;

#define INIT_STATIC_VARS \
    do { \
        assert(_sv == NULL); \
        UPC_ALL_ALLOC_CHK(_sv, THREADS, sizeof(_StaticVars)); \
        memset((char *)&_sv[MYTHREAD], 0, sizeof(_StaticVars)); \
        assert(MYSV._my_log == NULL); \
        MYSV.cores_per_node = get_cores_per_node(); \
        assert(MYSV.cores_per_node <= THREADS); \
        LOG_RESET_MEMCHECK; \
        initAtomicMetadata(&(MYSV)); \
        MYSV.logBuffer = initBuffer(128); \
    } while (0)

#define FREE_STATIC_VARS \
    do { \
        if (_sv != NULL) { \
            upc_barrier; \
            TIDY_UP_STATIC_VARS; \
            freeBuffer(MYSV.logBuffer); \
            UPC_ALL_FREE_CHK(_sv); \
            _sv = NULL; \
        } \
    } while (0)

#else
typedef _StaticVars *StaticVars;
#if defined(__BERKELEY_UPC__) && (__BERKELEY_UPC_PTHREADS__ == 1)
#ifndef HIPMER_PTHREADS
#error "__BERKELEY_UPC_PTHREADS__ is defined but HIPMER_PTHREADS is not!\n"
#endif

#define INIT_STATIC_VARS \
    do { \
        int cpn = get_cores_per_node(); \
        if (MYTHREAD % cpn == 0) { \
            assert(_sv == NULL); \
            _sv = (StaticVars)calloc(cpn, sizeof(_StaticVars)); \
            for (int i = 0; i < cpn; i++) { _sv[i].cores_per_node = cpn; } \
            _sv -= MYTHREAD; \
        } \
        upc_barrier; \
        if (_sv == NULL) { DIE("Could not initialize static variables!\n"); } \
        assert(MYSV._my_log == NULL && MYSV.cores_per_node = cpn); \
        LOG_RESET_MEMCHECK; \
        initAtomicMetadata(&(MYSV)); \
        MYSV.logBuffer = initBuffer(128); \
    } while (0)

#define FREE_STATIC_VARS \
    do { \
        upc_barrier; /* wait for all threads to get past this test */ \
        TIDY_UP_STATIC_VARS; \
        freeBuffer(MYSV.logBuffer); \
        upc_barrier; /* wait for all threads to get past this test */ \
        if (_sv == NULL) { \
        } else if (MYTHREAD % MYSV.cores_per_node == 0) { \
            _sv += MYTHREAD; \
            free(_sv); \
            _sv = NULL; \
        } else { \
        } \
        assert(_sv == NULL); \
    } while (0)

#else   // NO PTHREADS

#define INIT_STATIC_VARS \
    do { \
        assert(_sv == NULL); \
        _sv = (StaticVars)calloc(1, sizeof(_StaticVars)); \
        assert(_sv != NULL); \
        _sv -= MYTHREAD; /* backtrack to make x[MYTHREAD] valid in this thread... */ \
        assert(MYSV._my_log == NULL); \
        MYSV.cores_per_node = get_cores_per_node(); \
        LOG_RESET_MEMCHECK; \
        initAtomicMetadata(&(MYSV)); \
        MYSV.logBuffer = initBuffer(128); \
    } while (0)

#define FREE_STATIC_VARS \
    do { \
        if (_sv != NULL) { \
            TIDY_UP_STATIC_VARS; \
            freeBuffer(MYSV.logBuffer); \
            _sv += MYTHREAD; \
            free(_sv); \
            _sv = NULL; \
        } \
    } while (0)

#endif
#endif

#define SET_CORES_PER_NODE(cpn) \
    do { \
        if (!_sv) { DIE("Can not SET_CORES_PER_NODE before _sv has been initialized!\n"); } \
        int oldcpn = MYSV.cores_per_node; \
        if (oldcpn != 0 && oldcpn != cpn) { SWARN("cores_per_node has a discrepancy!  Discovered %d but user overrides with %d\n", oldcpn, cpn); } \
        MYSV.cores_per_node = cpn; \
    } while (0)

#define ADD_BARRIER_TIME(time) do { \
        if (_sv) { \
            addCountTime(&MYSV.barriers, time); \
        } \
} while (0)

#define GET_BARRIER_TIME() (_sv ? MYSV.barriers.time : 0.0)

extern StaticVars _sv; // instantiated by StaticVars.c -- can be local or shared ptr

#if defined (__cplusplus)
}
#endif

#endif // __STATIC_VARS_H
