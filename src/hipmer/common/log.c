#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

#include "log.h"
#include "Buffer.h"

int setLogPrefix(char *_LOG_prefix, int size, _LOG_LEVELS_TYPE level, int _printPrefix, time_t _LOG_ltime, const char *fmt, const char *filename, int line)
{
    assert(size > 1);
    int prefixSize = 0;
    _LOG_prefix[0] = '\0';
    if (_printPrefix) {
        struct tm _LOG_result;
        char _LOG_stime[32];
        localtime_r(&_LOG_ltime, &_LOG_result);
        strftime(_LOG_stime, 32, "%F %T", &_LOG_result);
        //_LOG_stime[strlen(_LOG_stime) - 1] = '\0';
        char _LOG_logLabel[32];
        strcpy(_LOG_logLabel, "INFO");
        if (level >= LL_DBG) {
            snprintf(_LOG_logLabel, 32, "%s DBG%d %s", KLBLUE, (int)(level - LL_DBG + 1), KNORM);
        }
        if (level < LL_LOG) {
            snprintf(_LOG_logLabel, 32, "%s %s %s", (level <= LL_DIE ? KLRED : KRED), (level <= LL_DIE ? "DIE" : "WARN"), KNORM);
        }
        prefixSize = snprintf(_LOG_prefix, size, "[Th%d %s %s %s:%d]: ", (int)MYTHREAD, _LOG_logLabel, _LOG_stime, filename, (int)line);
        if (prefixSize >= size) {
            // truncated!
            fprintf(stderr, "setLogPrefix size of %d is insufficient for the length (%d)!\n", size, prefixSize);
        }
    }
    return prefixSize;
}

void _LOG_at_file_line(const char *FILENAME, int LINE, int level, const char *fmt, ...)
{
    time_t _startLog = time(NULL);
    enum _LOG_LEVELS _level = (enum _LOG_LEVELS)((level) & ~(NO_LOG_PREFIX));
    int _printPrefix = ((level) & (NO_LOG_PREFIX)) == 0;

    if (HIPMER_VERBOSITY >= _level) {
        char _LOG_prefix[MAX_LOG_PREFIX];
        _LOG_prefix[0] = '\0';
        int _haveLogFile = 0;
        int _flushLogFile = 0;
        if (_sv) {
            if (MYSV._my_log) {
                _haveLogFile = 1;
            }
            if (MYSV.flushLog > 0 && (MYTHREAD % MYSV.flushLog) == 0) {
                _flushLogFile = 1;
            }
        }
        int willPrint = _haveLogFile || (_level < LL_LOG) || ((_level == LL_LOG) || ((!_haveLogFile) && (!MYTHREAD)));
        int size = 0, prefixSize = 0;
        Buffer p = (_sv && MYSV.logBuffer) ? MYSV.logBuffer : initBuffer(128);
        va_list ap;
        if (willPrint) {
            prefixSize = setLogPrefix(_LOG_prefix, MAX_LOG_PREFIX, _level, _printPrefix, _startLog, fmt, FILENAME, LINE);

            /* Determine required size */

            va_start(ap, fmt);
            size = vsnprintf(NULL, 0, fmt, ap);
            va_end(ap);

            if (size >= 0) {
                size++;          /* For '\0' */
                growBuffer(p, size + prefixSize);
            }
        }

        if (willPrint) {
            memcpyBuffer(p, _LOG_prefix, prefixSize);
            va_start(ap, fmt);
            size = vsnprintf(getEndBuffer(p), size, fmt, ap);
            if (size < 0) {
                willPrint = 0;
            }
            va_end(ap);
        }
        if (willPrint) {
            if (_level < LL_LOG) {
                fwrite(getStartBuffer(p), 1, prefixSize + size, stderr); // do not check for errors and possibly call DIE as we may be in an error state
                fflush(stderr);
            } else if (_level == LL_LOG || ((!_haveLogFile) && (!MYTHREAD))) {
                fwrite_chk(getStartBuffer(p), 1, prefixSize + size, stdout);
                fflush(stdout);
            }
            if (_haveLogFile) {
                fwrite_chk(getStartBuffer(p), 1, prefixSize + size, MYSV._my_log);
                if (_level < LL_LOG || HIPMER_VERBOSITY >= LL_DBG || ((MYTHREAD) == 0 || MYTHREAD == THREADS/2 || MYTHREAD == (THREADS-1)) || _flushLogFile > 0) {
                    fflush(MYSV._my_log);
                }
            }
        }
        (_sv && MYSV.logBuffer) ? resetBuffer(MYSV.logBuffer) : freeBuffer(p);
    }
    if (_sv) {
        MYSV._logTime += time(NULL) - _startLog;
    }
}

void log_type_sizes()
{
    LOGF("char:%ld int:%ld long:%d ptr:%d\n", sizeof(char), sizeof(int), sizeof(long), sizeof(void *));
}

void log_mem()
{
    FILE *self_stat = NULL;
    char * line = NULL;
    size_t len = 0, ret = 0;
 
    self_stat = fopen("/proc/self/stat", "r");
    if (self_stat) {
        rewind(self_stat);
        ret = getline(&line, &len, self_stat);
        if (ret > 0)
            LOGF("stat: %s", line);
        free(line);
        fclose(self_stat);
    }
}
