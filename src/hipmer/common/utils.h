#ifndef __UTILS_H
#define __UTILS_H

#include <stdio.h>
#include <stdint.h>

#include <upc.h>
#include <upc_collective.h>
#include <upc_tick.h>

#include "upc_common.h"
#include "common.h"
#include "Buffer.h"
#include "file.h"

#define CHECK_LINE_LEN(buf, buf_size, fname)                            \
    do {                                                                \
        if (strlen(buf) == buf_size - 1) {                                \
            DIE("line too long in file %s: > %d", fname, buf_size - 1); } \
    } while (0)

#ifndef ONE_KB
#define ONE_KB 1024L
#define ONE_MB 1048576L
#define ONE_GB 1073741824L
#endif

int bankers_round(double x);

char *reverse(char *s);
int startswith(char *s1, char *s2);
int endswith(char *s1, char *s2);
void switch_code(char *seq);

int get_tokens(char *s, char *tokens[], int *tot_len, int ntoks);

enum reduce_dest { SINGLE_DEST, ALL_DEST };

int64_t broadcast_long(int64_t val, int srcThread);
int64_t reduce_long(int64_t myval, upc_op_t op, enum reduce_dest dst);
int64_t reduce_prefix_long(int64_t myval, upc_op_t op, int64_t *opt_global_total);

// gathers count chunks of private data from every thread to a new allocation of private count*THREADS memory on the gatherThread
int64_t *gather_long(const int64_t *mydata, int countPerThread, int gatherThread);

// scatters count*THREADS chunks of private data from scatterThread to a new allocation of count private memory to every thread
int64_t *scatter_long(const int64_t *allvals, int countPerThread, int scatterThread);

int32_t broadcast_int(int32_t val, int srcThread);
int32_t reduce_int(int32_t myval, upc_op_t op, enum reduce_dest dst);

// returns the load imbalance and average, min & max of a number to thread 0
double avg_min_max(double myVal, double *avg, double *min, double *max);

double broadcast_double(double val, int srcThread);
double reduce_double(double myval, upc_op_t op, enum reduce_dest dst);

// reduces a block of values. If min_vals, max_vals and tot_vals arrays are not null, then they are
// set to the appropriate reduced value
void reduce_block(double *myvals, int num_vals, double *min_vals, double *max_vals, double *tot_vals);

// Thread 0 reads the file and broadcasts the contents to all threads into a new Buffer
Buffer broadcast_Buffer(Buffer buf, int srcThread);
Buffer broadcast_file(const char *fname);
int all_does_file_exist(const char *fname);

int64_t broadcast_file_size(const char *fname);

//
// LazyAtomic API - aggregated atomics that may leave some ranges untouched if myVal != 1 and the THREADS last stepSize
//
typedef struct {
    shared [1] int64_t * global;
    int64_t myCache;
    int32_t stepSize, myStep;
} _LazyAtomic;
typedef _LazyAtomic *LazyAtomic;

// initialize the opaque structure
LazyAtomic initLazyAtomic(int stepSize);
// gets the latest atomic, flushes the cached steps
int64_t getLazyAtomic(LazyAtomic lazy);
// gets the next value for this thread, may cache up to stepSize values before accessing global counter
int64_t faddLazyAtomic(LazyAtomic lazy, int64_t myVal);
// frees the resources, returns the global atomic value (there may be holes)
int64_t freeLazyAtomic(LazyAtomic *lazy);

void *upc_memget_alloc(shared const void *src, size_t n);

void report_memory_allocations(char *buffer);

#endif
