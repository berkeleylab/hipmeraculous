#ifndef UPC_COMPATIBILITY_H
#define UPC_COMPATIBILITY_H

#include <upc.h>
#include <stdlib.h>

#include "common_base.h"
#include "StaticVars.h"
#include "log.h"
#include "file.h"

// Polling ticks and types
#if defined(USE_BUPC) || defined(__BERKELEY_UPC__)
#   define UPC_POLL bupc_poll()
#   define UPC_INT64_T int64_t
#   define UPC_ATOMIC_MIN_T int32_t
#   ifndef __UPC_TICK__
#     define UPC_TICK_T bupc_tick_t
#     define UPC_TICKS_NOW bupc_ticks_now
#     define UPC_TICKS_TO_SECS(t) (bupc_ticks_to_us((t) <= 0 ? 1 : (t)) / 1000000.0)
#   else
#     include <upc_tick.h>
#     define UPC_TICK_T upc_tick_t
#     define UPC_TICKS_NOW upc_ticks_now
#     define UPC_TICKS_TO_SECS(t) (upc_ticks_to_ns((t) <= 0 ? 1 : (t)) / 1000000000.0)
#   endif
#elif defined USE_CRAY_UPC
#   include <upc_tick.h>
#   define UPC_POLL upc_fence
#   define UPC_INT64_T long
#   define UPC_ATOMIC_MIN_T long
#   define UPC_TICK_T upc_tick_t
#   define UPC_TICKS_NOW upc_ticks_now
#   define UPC_TICKS_TO_SECS(t) (upc_ticks_to_ns((t) <= 0 ? 1 : (t)) / 1000000000.0)
#endif

typedef upc_lock_t * upc_lock_t_ptr;
typedef shared [1] upc_lock_t_ptr * all_shared_upc_lock_t_ptr;

#define NOW (UPC_TICKS_TO_SECS(UPC_TICKS_NOW()))

#define UPC_FENCE upc_fence

#define ELAPSED_SECS(t) UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - (t))

// This check is necessary to ensure that shared pointers of 16 bytes are unlikely to be corrupted.
#define IS_VALID_UPC_PTR(ptr) (ptr == NULL || (upc_threadof(ptr) < THREADS && upc_phaseof(ptr) == 0 && upc_addrfield(ptr) < (64LL*1024*1024*1024)))
// 8 byte pointers will never be subject to a race condition
#define IS_VALID_UPC_PTR_SKIP8(ptr) (sizeof(UC_SHPTR) == 8 ? 1 : IS_VALID_UPC_PTR(ptr))


//
// Atomic API smashing...
//

#ifndef USE_UPC_ATOMIC_API
#  define USE_UPC_ATOMIC_API __UPC_ATOMIC__
#else
#  define USE_UPC_ATOMIC_API 0
#endif

typedef shared void *UC_SHPTR;
typedef UC_SHPTR *UC_LPTR_TO_SHPTR;
inline void UC_assign_I32(int32_t *ptr, const int32_t val, const char *file, int line)
{
    if (ptr == NULL) {
        WARN("[%s:%d] assign to null? %lld\n", file, line, (lld)val); return;
    }
    *ptr = val;
}
inline void UC_assign_I64(int64_t *ptr, const int64_t val, const char *file, int line)
{
    if (ptr == NULL) {
        WARN("[%s:%d] assign to null? %lld\n", file, line, (lld)val); return;
    }
    *ptr = val;
}
inline void UC_assign_SHPTR(UC_LPTR_TO_SHPTR ptr, const UC_SHPTR val, const char *file, int line)
{
    if (ptr == NULL) {
        WARN("[%s:%d] assign to null? %p\n", file, line, upc_addrfield(val)); return;
    }
    if (!IS_VALID_UPC_PTR(val)) {
        DIE("[%s:%d] UC_assign_SHPTR is assigning an invalid shared pointer thread:%d phaseof:%d addrfield:%lld\n", file, line, (int)upc_threadof(val), (int)upc_phaseof(val), (lld)upc_addrfield(val));
    }
    *ptr = val;
}

#define UC_TYPEN_I32 32
#define UC_TYPEN_I64 64
#define UC_TYPEN_SHPTR 0
#define UC_TYPE_I32 int32_t
#define UC_TYPE_I64 int64_t
#define UC_TYPE_SHPTR shared void *

typedef union _UC_VAR_MASH {
    int64_t  I64;
    int32_t  I32;
    UC_SHPTR SHPTR;
} UC_VAR_MASH;

#define UC_LOCKS_PER_THREAD (128LL)
#define UC_LOCKS_OFFSET(TARGETPTR) (1LL * upc_threadof(TARGETPTR) + THREADS * ((upc_addrfield(TARGETPTR) >> 6) % UC_LOCKS_PER_THREAD))
#define UC_GET_LOCK(TARGETPTR) ((upc_lock_t_ptr) ((HipMerAtomicMetadata_t *)MYSV.optionalAtomicMetadata)->threadLocks[UC_LOCKS_OFFSET(TARGETPTR)])

#if USE_UPC_ATOMIC_API

#include <upc_types.h>
#include <upc_tick.h>
#include <upc_atomic.h>

#ifdef UPC_ATOMIC_HINT_FAVOR_FAR
#  define  UC_HINT_FAR UPC_ATOMIC_HINT_FAVOR_FAR
#else
#  define  UC_HINT_FAR 0
#endif

#ifdef UPC_ATOMIC_HINT_FAVOR_NEAR
#  define  UC_HINT_NEAR UPC_ATOMIC_HINT_FAVOR_NEAR
#else
#  define  UC_HINT_NEAR 0
#endif

struct _HipMerAtomicMetadata {
    upc_atomicdomain_t *I32_;
    upc_atomicdomain_t *I32_LL;
    upc_atomicdomain_t *I32_HT;
    upc_atomicdomain_t *I64_;
    upc_atomicdomain_t *I64_LL;
    upc_atomicdomain_t *I64_HT;
    upc_atomicdomain_t *I64_NEAR;
    upc_atomicdomain_t *I64_FAR;
    upc_atomicdomain_t *SHPTR_;
    upc_atomicdomain_t *SHPTR_LL;
    upc_atomicdomain_t *SHPTR_HT;
    upc_atomicdomain_t *SHPTR_NEAR;
    upc_atomicdomain_t *SHPTR_FAR;
    all_shared_upc_lock_t_ptr threadLocks; /* UC_LOCKS_PER_THREAD * THREADS */
};

#define GET_AD(TYPE, HINT) (((HipMerAtomicMetadata_t *)MYSV.optionalAtomicMetadata)->TYPE ## _ ## HINT)

#   define _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, TYPE, HINT, MODE) do { \
        if (UC_TYPEN_ ## TYPE == UC_TYPEN_SHPTR) { DIE("Atomic ADD of pointer not supported with shared pointer: " # TARGETPTR "\n"); } \
        double _start = NOW; \
        DBG2("upc _ATOMIC_FADD atomic %lld on Thr%d: " # FETCHPTR ", " # TARGETPTR ", " # DELTAVAL ", " # TYPE ", " # HINT ", " # MODE "\n", (lld)(DELTAVAL), upc_threadof(TARGETPTR)); \
        DBG3("upc _ATOMIC_FADD GET_AD: " # TYPE "_" # HINT " at %p mem is %d bytes from MYSV.optionalAtomicMetadata (%p)\n", &GET_AD(TYPE, HINT), (char *)&GET_AD(TYPE, HINT) - (char *)MYSV.optionalAtomicMetadata, MYSV.optionalAtomicMetadata); \
        UC_VAR_MASH _delta; \
        _delta.TYPE = DELTAVAL; \
        upc_atomic_ ## MODE(GET_AD(TYPE, HINT), (FETCHPTR), (_delta.TYPE == 1 ? UPC_INC : UPC_ADD), (TARGETPTR), (_delta.TYPE == 1 ? NULL : &_delta.TYPE), NULL); \
        if (_sv) { addCountTime(&MYSV.fadds, NOW - _start); } \
} while (0)
#   define _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, HINT, MODE) do { \
        DBG2("upc _ATOMIC_CSWAP " #TYPE "(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        double _start = NOW; \
        UC_VAR_MASH _oldval, _newval; \
        _oldval.TYPE = OLDVAL; \
        _newval.TYPE = NEWVAL; \
        upc_atomic_ ## MODE(GET_AD(TYPE, HINT), (FETCHPTR), UPC_CSWAP, (TARGETPTR), &_oldval.TYPE, &_newval.TYPE); \
        if (_sv) { addCountTime(&MYSV.cswaps, NOW - _start); } \
} while (0)
#   define _ATOMIC_GET(FETCHPTR, TARGETPTR, TYPE, HINT, MODE) do { \
        assert((FETCHPTR) != NULL); \
        DBG2("upc _ATOMIC_GET " #TYPE "(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        double _start = NOW; \
        upc_atomic_ ## MODE(GET_AD(TYPE, HINT), (FETCHPTR), UPC_GET, (TARGETPTR), NULL, NULL); \
        if (_sv) { addCountTime(&MYSV.gets, NOW - _start); } \
} while (0)
#   define _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, TYPE, HINT, MODE) do { \
        DBG2("upc _ATOMIC_SET " #TYPE "(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        double _start = NOW; \
        UC_VAR_MASH _newval; \
        _newval.TYPE = NEWVAL; \
        upc_atomic_ ## MODE(GET_AD(TYPE, HINT), (FETCHPTR), UPC_SET, (TARGETPTR), &_newval.TYPE, NULL); \
        if (_sv) { addCountTime(&MYSV.sets, NOW - _start); } \
} while (0)

// 64-bit strict
#   define UPC_ATOMIC_FADD_I64(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, , strict)
#   define UPC_ATOMIC_CSWAP_I64(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, , strict)
#   define UPC_ATOMIC_GET_I64(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, , strict)
#   define UPC_ATOMIC_SET_I64(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, , strict)
// relaxed
#   define UPC_ATOMIC_FADD_I64_RELAXED(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, HT, relaxed)
#   define UPC_ATOMIC_CSWAP_I64_RELAXED(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, HT, relaxed)
#   define UPC_ATOMIC_GET_I64_RELAXED(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, HT, relaxed)
#   define UPC_ATOMIC_SET_I64_RELAXED(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, HT, relaxed)

// 32-bit strict
#   define UPC_ATOMIC_FADD_I32(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I32, , strict)
#   define UPC_ATOMIC_CSWAP_I32(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I32, , strict)
#   define UPC_ATOMIC_GET_I32(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I32, , strict)
#   define UPC_ATOMIC_SET_I32(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I32, , strict)
// relaxed
#   define UPC_ATOMIC_FADD_I32_RELAXED(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I32, HT, relaxed)
#   define UPC_ATOMIC_CSWAP_I32_RELAXED(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I32, HT, relaxed)
#   define UPC_ATOMIC_GET_I32_RELAXED(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I32, HT, relaxed)
#   define UPC_ATOMIC_SET_I32_RELAXED(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I32, HT, relaxed)

// minimum type strict
#   define UPC_ATOMIC_FADD_MIN_T UPC_ATOMIC_FADD_I32
#   define UPC_ATOMIC_CSWAP_MIN_T UPC_ATOMIC_CSWAP_I32
#   define UPC_ATOMIC_GET_MIN_T UPC_ATOMIC_GET_I32
#   define UPC_ATOMIC_SET_MIN_T UPC_ATOMIC_SET_I32
// relaxed
#   define UPC_ATOMIC_FADD_MIN_T_RELAXED UPC_ATOMIC_FADD_I32_RELAXED
#   define UPC_ATOMIC_CSWAP_MIN_T_RELAXED UPC_ATOMIC_CSWAP_I32_RELAXED

// Shared Pointers
#   define UPC_ATOMIC_CSWAP_SHPTR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, , strict)
#   define UPC_ATOMIC_GET_SHPTR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, , strict)
#   define UPC_ATOMIC_SET_SHPTR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, , strict)

// Near
#   define UPC_ATOMIC_FADD_I64_NEAR(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, NEAR, strict)
#   define UPC_ATOMIC_CSWAP_I64_NEAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, NEAR, strict)
#   define UPC_ATOMIC_GET_I64_NEAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, NEAR, strict)
#   define UPC_ATOMIC_SET_I64_NEAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, NEAR, strict)
#   define UPC_ATOMIC_CSWAP_SHPTR_NEAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, NEAR, strict)
#   define UPC_ATOMIC_GET_SHPTR_NEAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, NEAR, strict)
#   define UPC_ATOMIC_SET_SHPTR_NEAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, NEAR, strict)

// FAR
#   define UPC_ATOMIC_FADD_I64_FAR(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, FAR, strict)
#   define UPC_ATOMIC_CSWAP_I64_FAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, FAR, strict)
#   define UPC_ATOMIC_GET_I64_FAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, FAR, strict)
#   define UPC_ATOMIC_SET_I64_FAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, FAR, strict)
#   define UPC_ATOMIC_CSWAP_SHPTR_FAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, FAR, strict)
#   define UPC_ATOMIC_GET_SHPTR_FAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, FAR, strict)
#   define UPC_ATOMIC_SET_SHPTR_FAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, FAR, strict)

#elif defined(USE_BUPC) || defined(__BERKELEY_UPC__) // Berkely UPC!
struct _HipMerAtomicMetadata {
    all_shared_upc_lock_t_ptr threadLocks; /* UC_LOCKS_PER_THREAD * THREADS */
};
typedef union {
    shared void *shptr;
    int64_t      int64;
} ShPtr2Int64;

#define NEED_ACC_LOCK(TARGETPTR) ((sizeof(TARGETPTR) != 8 || (upc_addrfield(TARGETPTR) & 0x07)))

#   define _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, TYPE, MODE) do { \
        if (UC_TYPEN_ ## TYPE == UC_TYPEN_SHPTR) { DIE("Atomic ADD of pointer not supported with shared pointer: " # TARGETPTR "\n"); } \
        double _start = NOW; \
        DBG2("bupc _ATOMIC_FADD atomic %lld on Thr%d: " # FETCHPTR ", " # TARGETPTR ", " # DELTAVAL ", " # TYPE ", " # MODE "\n", (lld)(DELTAVAL), upc_threadof(TARGETPTR)); \
        UC_TYPE_ ## TYPE _val = bupc_atomic ## TYPE ## _fetchadd_  ## MODE((TARGETPTR), DELTAVAL); \
        if ((FETCHPTR) != NULL) { UC_assign_ ## TYPE(FETCHPTR, _val, __FILE__, __LINE__); } \
        if (_sv) { addCountTime(&MYSV.fadds, NOW - _start); } \
} while (0)

#   define __ATOMIC_CSWAP_I32(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc _ATOMIC_CSWAP_I32" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        *(FETCHPTR) = bupc_atomic ## I32 ## _cswap_  ## MODE((TARGETPTR), OLDVAL, NEWVAL); \
} while (0)
#   define __ATOMIC_CSWAP_I64(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc _ATOMIC_CSWAP_I64" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        *(FETCHPTR) = bupc_atomic ## I64 ## _cswap_  ## MODE((TARGETPTR), OLDVAL, NEWVAL); \
} while (0)
#   define __ATOMIC_CSWAP_SHPTR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE) do { \
        if (NEED_ACC_LOCK(TARGETPTR)) { \
            DBG2("bupc _ATOMIC_CSWAP_SHPTR" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            *(FETCHPTR) = *(TARGETPTR); \
            if (*(FETCHPTR) == (UC_TYPE_ ## TYPE)OLDVAL) { *(TARGETPTR) = NEWVAL; } \
            UPC_FENCE; \
            DBG2("bupc _ATOMIC_CSWAP fence\n"); \
        } else { \
            DBG2("bupc _ATOMIC_CSWAP_SHPTR as I64" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            ShPtr2Int64 __old, __new; \
            __old.shptr = OLDVAL; \
            __new.shptr = NEWVAL; \
            __ATOMIC_CSWAP_I64( (int64_t*) FETCHPTR, (shared [] int64_t *) TARGETPTR, __old.int64, __new.int64, I64, MODE); \
        } \
} while (0)

#   define _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc _ATOMIC_CSWAP %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        double _start = NOW; \
        if (UC_TYPEN_ ## TYPE == UC_TYPEN_SHPTR) { \
            /* must implement with locking, if shared pointer size > int64_t, otherwise cast to I64... */ \
            DBG2("bupc _ATOMIC_CSWAP on Thr%d using lockoffset %lld: (" # FETCHPTR ", " # TARGETPTR ", " # OLDVAL ", " # NEWVAL ", " # TYPE ", " # MODE ")\n", upc_threadof(TARGETPTR), (lld)UC_LOCKS_OFFSET(TARGETPTR)); \
            double _lockTime = 0.0; \
            if (NEED_ACC_LOCK(TARGETPTR)) { \
                double _startLock = NOW; \
                upc_lock(UC_GET_LOCK(TARGETPTR)); \
                DBG2("bupc _ATOMIC_CSWAP got lock\n"); \
                _lockTime += NOW - _startLock; \
            } \
            __ATOMIC_CSWAP_ ## TYPE(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE); \
            if (NEED_ACC_LOCK(TARGETPTR)) { \
                double _startLock = NOW; \
                upc_unlock(UC_GET_LOCK(TARGETPTR)); \
                DBG2("bupc _ATOMIC_CSWAP unlock\n"); \
                _lockTime += NOW - _startLock; \
                if (_sv) { addCountTime(&MYSV.atomicLocks, _lockTime); } \
            } \
        } else { \
            DBG2("bupc _ATOMIC_CSWAP if %lld to %lld on Thr%d: " # FETCHPTR ", " # TARGETPTR ", " # OLDVAL ", " # NEWVAL ", " # TYPE ", " # MODE "\n", (lld)(OLDVAL), (lld)(NEWVAL), upc_threadof(TARGETPTR)); \
            __ATOMIC_CSWAP_ ## TYPE(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, TYPE, MODE); \
        } \
        if (_sv) { addCountTime(&MYSV.cswaps, NOW - _start); } \
} while (0)

#   define __ATOMIC_GET_I32(FETCHPTR, TARGETPTR, TYPE, MODE) do { \
        DBG2("bupc __ATOMIC_GET_I32" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        UC_assign_ ## TYPE(FETCHPTR, bupc_atomic ## I32 ## _read_  ## MODE(TARGETPTR), __FILE__, __LINE__); \
} while (0)

#   define __ATOMIC_GET_I64(FETCHPTR, TARGETPTR, TYPE, MODE) do { \
        DBG2("bupc __ATOMIC_GET_I64" #FETCHPTR ", " #TARGETPTR " %d:%d:%x\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        UC_assign_ ## TYPE(FETCHPTR, bupc_atomic ## I64 ## _read_  ## MODE(TARGETPTR), __FILE__, __LINE__); \
} while (0)

#   define __ATOMIC_GET_SHPTR(FETCHPTR, TARGETPTR, TYPE, MODE) do { \
        if (NEED_ACC_LOCK(TARGETPTR)) { \
            DBG2("bupc __ATOMIC_GET_SHPTR: Getting shptr(%d:%d:%x) with lock\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            double _lockTime = 0.0; \
            double _startLock = NOW; \
            upc_lock(UC_GET_LOCK(TARGETPTR)); \
            _lockTime += NOW - _startLock; \
            UC_assign_SHPTR((UC_SHPTR *)(FETCHPTR), *(TARGETPTR), __FILE__, __LINE__); \
            _startLock = NOW; \
            upc_unlock(UC_GET_LOCK(TARGETPTR)); \
            _lockTime += NOW - _startLock; \
            if (_sv) { addCountTime(&MYSV.atomicLocks, _lockTime); } \
        } else { \
            DBG2("bupc __ATOMIC_GET_SHPTR: Getting shptr(%d:%d:%x) as int64\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            __ATOMIC_GET_I64( (int64_t*) FETCHPTR, (shared [] int64_t *) TARGETPTR, I64, MODE); \
        } \
} while (0)

#   define _ATOMIC_GET(FETCHPTR, TARGETPTR, TYPE, MODE) do { \
        DBG2("bupc _ATOMIC_GET: Getting shptr(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        if ((FETCHPTR) == NULL) { DIE("Atomic GET can not be used with a NULL fetchptr: " # FETCHPTR "\n"); } \
        double _start = NOW; \
        __ATOMIC_GET_ ## TYPE(FETCHPTR, TARGETPTR, TYPE, MODE); \
        if (_sv) { addCountTime(&MYSV.gets, NOW - _start); } \
} while (0)

#   define __ATOMIC_SET_I32(TARGETPTR, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc __ATOMIC_SET_I32: Getting shptr(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        bupc_atomic ## I32 ## _set_  ## MODE((TARGETPTR), NEWVAL); \
} while (0)

#   define __ATOMIC_SET_I64(TARGETPTR, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc __ATOMIC_SET_I64: Getting shptr(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        bupc_atomic ## I64 ## _set_  ## MODE((TARGETPTR), NEWVAL); \
} while (0)

#   define __ATOMIC_SET_SHPTR(TARGETPTR, NEWVAL, TYPE, MODE) do { \
        if (NEED_ACC_LOCK(TARGETPTR)) { \
            DBG2("bupc __ATOMIC_SET_SHPTR: Getting shptr(%d:%d:%x) with lock\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            double _lockTime = 0.0; \
            double _startLock = NOW; \
            upc_lock(UC_GET_LOCK(TARGETPTR)); \
            _lockTime += NOW - _startLock; \
            *(TARGETPTR) = NEWVAL; \
            UPC_FENCE; \
            _startLock = NOW; \
            upc_unlock(UC_GET_LOCK(TARGETPTR)); \
            _lockTime += NOW - _startLock; \
            if (_sv) { addCountTime(&MYSV.atomicLocks, _lockTime); } \
        } else { \
            DBG2("bupc __ATOMIC_SET_SHPTR: Getting shptr(%d:%d:%x) as I64\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
            ShPtr2Int64 __new; \
            __new.shptr = NEWVAL; \
            __ATOMIC_SET_I64((shared [] int64_t *) TARGETPTR, __new.int64, I64, MODE); \
        } \
} while (0)

#   define _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, TYPE, MODE) do { \
        DBG2("bupc _ATOMIC_SET: Getting shptr(%d:%d:%x)\n", upc_threadof(TARGETPTR), upc_phaseof(TARGETPTR), upc_addrfield(TARGETPTR)); \
        if ((FETCHPTR) != NULL) { _ATOMIC_GET((FETCHPTR), (TARGETPTR), TYPE, MODE); } \
        double _start = NOW; \
        __ATOMIC_SET_ ## TYPE(TARGETPTR, NEWVAL, Type, MODE); \
        if (_sv) { addCountTime(&MYSV.sets, NOW - _start); } \
} while (0)


// 64-bit strict
#   define UPC_ATOMIC_FADD_I64(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, strict)
#   define UPC_ATOMIC_CSWAP_I64(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, strict)
#   define UPC_ATOMIC_GET_I64(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, strict)
#   define UPC_ATOMIC_SET_I64(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, strict)
// relaxed
#   define UPC_ATOMIC_FADD_I64_RELAXED(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, relaxed)
#   define UPC_ATOMIC_CSWAP_I64_RELAXED(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, relaxed)
#   define UPC_ATOMIC_GET_I64_RELAXED(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, relaxed)
#   define UPC_ATOMIC_SET_I64_RELAXED(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, relaxed)

// 32-bit strict
#   define UPC_ATOMIC_FADD_I32(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I32, strict)
#   define UPC_ATOMIC_CSWAP_I32(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I32, strict)
#   define UPC_ATOMIC_GET_I32(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I32, strict)
#   define UPC_ATOMIC_SET_I32(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I32, strict)
// relaxed
#   define UPC_ATOMIC_FADD_I32_RELAXED(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I32, relaxed)
#   define UPC_ATOMIC_CSWAP_I32_RELAXED(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I32, relaxed)
#   define UPC_ATOMIC_GET_I32_RELAXED(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I32, relaxed)
#   define UPC_ATOMIC_SET_I32_RELAXED(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I32, relaxed)

// minimum type strict
#   define UPC_ATOMIC_FADD_MIN_T UPC_ATOMIC_FADD_I32
#   define UPC_ATOMIC_CSWAP_MIN_T UPC_ATOMIC_CSWAP_I32
#   define UPC_ATOMIC_GET_MIN_T UPC_ATOMIC_GET_I32
#   define UPC_ATOMIC_SET_MIN_T UPC_ATOMIC_SET_I32
// relaxed
#   define UPC_ATOMIC_FADD_MIN_T_RELAXED UPC_ATOMIC_FADD_I32_RELAXED
#   define UPC_ATOMIC_CSWAP_MIN_T_RELAXED UPC_ATOMIC_CSWAP_I32_RELAXED
#   define UPC_ATOMIC_GET_MIN_T_RELAXED UPC_ATOMIC_GET_I32_RELAXED
#   define UPC_ATOMIC_SET_MIN_T_RELAXED UPC_ATOMIC_SET_I32_RELAXED

// Shared Pointers
#   define UPC_ATOMIC_CSWAP_SHPTR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, strict)
#   define UPC_ATOMIC_GET_SHPTR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, strict)
#   define UPC_ATOMIC_SET_SHPTR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, strict)

// near 64 & shptr
#   define UPC_ATOMIC_FADD_I64_NEAR(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, strict)
#   define UPC_ATOMIC_CSWAP_I64_NEAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, strict)
#   define UPC_ATOMIC_GET_I64_NEAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, strict)
#   define UPC_ATOMIC_SET_I64_NEAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, strict)
#   define UPC_ATOMIC_CSWAP_SHPTR_NEAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, strict)
#   define UPC_ATOMIC_GET_SHPTR_NEAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, strict)
#   define UPC_ATOMIC_SET_SHPTR_NEAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, strict)

// far 64 & shptr
#   define UPC_ATOMIC_FADD_I64_FAR(FETCHPTR, TARGETPTR, DELTAVAL) _ATOMIC_FADD(FETCHPTR, TARGETPTR, DELTAVAL, I64, strict)
#   define UPC_ATOMIC_CSWAP_I64_FAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, I64, strict)
#   define UPC_ATOMIC_GET_I64_FAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, I64, strict)
#   define UPC_ATOMIC_SET_I64_FAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, I64, strict)
#   define UPC_ATOMIC_CSWAP_SHPTR_FAR(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL) _ATOMIC_CSWAP(FETCHPTR, TARGETPTR, OLDVAL, NEWVAL, SHPTR, strict)
#   define UPC_ATOMIC_GET_SHPTR_FAR(FETCHPTR, TARGETPTR) _ATOMIC_GET(FETCHPTR, TARGETPTR, SHPTR, strict)
#   define UPC_ATOMIC_SET_SHPTR_FAR(FETCHPTR, TARGETPTR, NEWVAL) _ATOMIC_SET(FETCHPTR, TARGETPTR, NEWVAL, SHPTR, strict)

#else
#   error "Please specify USE_UPC_ATOMIC_API or USE_BUPC or USE_CRAY_UPC"
#endif

#endif // UPC_COMPATIBLITY_H
