#ifndef _UPC2OBJ_H
#define _UPC2OBJ_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef UPC_SHARED_PTR_SIZE
#define UPC_SHARED_PTR_SIZE 8
#endif

#ifdef __UPC__
typedef shared[] uint8_t *SharedChar;
typedef struct {
    SharedChar _ptr;
} upc2obj_SharedChar;
typedef struct {
    uint8_t *          _local_ptr;
    upc2obj_SharedChar _shared_ptr;
} upc2obj_tuple;
#else
typedef uint8_t SharedChar[UPC_SHARED_PTR_SIZE];
typedef struct {
    SharedChar _ptr;
} upc2obj_SharedChar;
typedef struct {
    uint8_t *          _local_ptr;
    upc2obj_SharedChar _shared_ptr;
} upc2obj_tuple;
#endif

uint8_t *upc2obj_upc_alloc(size_t n);
void upc2obj_upc_free(uint8_t *ptr);
upc2obj_tuple upc2obj_getTuple(uint8_t *ptr);
uint8_t *upc2obj_cast_local(upc2obj_tuple tuple);
void upc2obj_startUPC();
void upc2obj_endUPC();

#ifdef __cplusplus
}
#endif

#endif /* _UPC2OBJ_H */
