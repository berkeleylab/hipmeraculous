#include "upc_circular_buffer.h"

// constructor -- collective, every thread has a different buffer
CircularBuffer initCircularBuffer(int32_t count, int32_t blockSize)
{
    CircularBuffer cb = (CircularBuffer)calloc_chk(1, sizeof(_CircularBuffer));

    blockSize += sizeof(Header);
    UPC_ALL_ALLOC_CHK(cb->posUnions, THREADS, sizeof(PosUnion));
    UPC_ALL_ALLOC_CHK(cb->buffers, THREADS, sizeof(SharedCharPtr));
    UPC_ALL_ALLOC_CHK(cb->counts, THREADS, sizeof(int32_t));
    UPC_ALLOC_CHK(cb->buffers[MYTHREAD], count * blockSize);
    // initialize headers as empty
    for (int i = 0; i < count; i++) {
        SharedCharPtr tmp = cb->buffers[MYTHREAD] + i * blockSize;
        *((Header *)(tmp)) = CB_HEADER_EMPTY;
    }
    cb->posUnions[MYTHREAD].i = 0;
    cb->counts[MYTHREAD] = count;
    cb->blockSize = blockSize;
    upc_barrier;
    return cb;
}

// destructor
void freeCircularBuffer(CircularBuffer *_cb)
{
    assert(_cb && *_cb);
    CircularBuffer cb = *_cb;
    PosUnion mypos;
    mypos.i = cb->posUnions[MYTHREAD].i;
    if (mypos.pos.head != mypos.pos.tail) {
        WARN("head (%lld) != tail (%lld)\n", (lld)mypos.pos.head, (lld)mypos.pos.tail);
    }
    UPC_FREE_CHK(cb->buffers[MYTHREAD]);
    UPC_ALL_FREE_CHK(cb->buffers);
    UPC_ALL_FREE_CHK(cb->posUnions);
    UPC_ALL_FREE_CHK(cb->counts);
    LOGF("freeCircularBuffer (%d): push try=%0.3f wait=%0.3f busy=%0.3f send=%0.3f - pop try=%0.3f busy=%0.3fs send=%0.3fs\n", mypos.pos.head, cb->pushTry, cb->pushWait, cb->pushBusyWait, cb->pushSend, cb->popTry, cb->popBusyWait, cb->popSend);
    free_chk(cb);
    *_cb = NULL;
}

Positions readPosCircularBuffer(CircularBuffer cb, int thread)
{
    PosUnion _pos;

    UPC_ATOMIC_GET_I64(&_pos.i, &(cb->posUnions[thread].i));
    return _pos.pos;
}

int getNumEntriesCircularBuffer(CircularBuffer cb, int thread)
{
    Positions pos = readPosCircularBuffer(cb, thread);

    DBG2("getNumEntriesCircularBuffer thread=%d head=%d tail=%d\n", thread, pos.head, pos.tail);
    return pos.head - pos.tail;
}

int isEmptyCircularBuffer(CircularBuffer cb, int thread)
{
    return getNumEntriesCircularBuffer(cb, thread) == 0;
}

int isFullCircularBuffer(CircularBuffer cb, int thread)
{
    Positions pos = readPosCircularBuffer(cb, thread);
    int32_t count = cb->counts[thread];

    return (pos.head >= pos.tail + count) | (pos.head >= CB_MAX_ENTRY);
}

// 1 if success, 0 if fail
int tryPushCircularBuffer(CircularBuffer cb, int thread, void *src, BlockingCallback callback, void *callbackData)   // one blockSize of data
{
    double t1 = now();
    // get the latest positions
    Positions pos = readPosCircularBuffer(cb, thread);
    int32_t count = cb->counts[thread];

    if (pos.head >= pos.tail + count) {
        // no room
        cb->pushTry += now() - t1;
        return 0;
    }
    if (pos.head >= CB_MAX_ENTRY) {
        // approaching overflow... wait for pop to drain and reset positions
        cb->pushTry += now() - t1;
        return 0;
    }

    // probably room; commit to push

    // push, block if necessary
    int32_t myhead;
    UPC_ATOMIC_FADD_I32(&myhead, &(cb->posUnions[thread].pos.head), 1);
    DBG("tryPushCircularBuffer: attempted. myhead=%d thread=%d head>=%d tail=%d\n", myhead, thread, myhead + 1, pos.tail);
    double t2 = now();
    cb->pushTry += t2 - t1;
    while (myhead >= pos.tail + count) {
        // this thread would exceed buffer capacity
        // do some useful work then check again
        if (callback) {
            (*callback)(callbackData);
        } else {
            UPC_POLL;
        }

        // referesh positions
        pos = readPosCircularBuffer(cb, thread);
        DBG("tryPushCircularBuffer: waiting for room or backout. myhead=%d thread=%d head=%d tail=%d\n", myhead, thread, pos.head, pos.tail);
        if (myhead >= pos.tail + count && myhead + 1 == pos.head) {
            // insufficient movement on tail and this is the maximum head.
            // Attempt to backout the fadd on head with compare swap
            // This can only succeed if head remains the maxium and tail has not changed since last atomic read
            PosUnion backoutPos, testPos, curPos;
            curPos.pos.tail = pos.tail;
            curPos.pos.head = pos.head;
            backoutPos.pos.head = myhead; // the value it was before atomic add
            backoutPos.pos.tail = pos.tail;
            assert(backoutPos.pos.head == pos.head + 1);
            UPC_ATOMIC_CSWAP_I64(&testPos.i, &(cb->posUnions[thread].i), curPos.i, backoutPos.i);
            if (testPos.i == curPos.i) {
                // successfully backedout of push!
                LOGF("tryPushCircularBuffer: backedout! myhead=%d thread=%d set -- head=%d tail=%d\n", myhead, thread, backoutPos.pos.head, backoutPos.pos.tail);
                cb->pushWait += now() - t2;
                return 0;
            } else {
                // use the newest values
                pos.head = testPos.pos.head;
                pos.tail = testPos.pos.tail;
                LOGF("tryPushCircularBuffer: could not backout... maybe there is room now... myhead=%d thread=%d head=%d tail=%d\n", myhead, thread, pos.head, pos.tail);
            }
        }
    }
    double t3 = now();
    cb->pushWait += t3 - t2;

    // space is now allocated.. take care that pop is protected against incomplete data which is updated atomically last after a fence
    // Header: EMPTY -> WRITING -> FULL
    shared [] char *pushPos = cb->buffers[thread] + (myhead % count) * cb->blockSize;

    // now verify that space is not actively being popped, check header until it is 0
    Header headerVal = CB_HEADER_INVALID;
    while (headerVal != CB_HEADER_EMPTY) {
        UPC_ATOMIC_CSWAP_I64(&headerVal, (shared [] Header *)pushPos, CB_HEADER_EMPTY, CB_HEADER_WRITING);
        if (headerVal == CB_HEADER_EMPTY) {
            break;
        }
        if (callback) {
            (*callback)(callbackData);
        } else {
            UPC_POLL;
        }
        DBG("tryPushCircularBuffer: Waiting for CB_HEADER_EMPTY (%lld is %lld) thread=%d myhead=%d\n", (lld)CB_HEADER_EMPTY, (lld)headerVal, thread, myhead);
    }
    double t4 = now();
    cb->pushBusyWait += t4 - t3;

    // now put
    upc_memput(pushPos + sizeof(Header), (char *)src, cb->blockSize - sizeof(Header));
    upc_fence;

    // now update header entry // TODO can this be an atomic set?
    UPC_ATOMIC_CSWAP_I64(&headerVal, (shared [] Header *)pushPos, CB_HEADER_WRITING, CB_HEADER_FULL);
    if (headerVal != CB_HEADER_WRITING) {
        DIE("got %lld not 0!\n", (lld)headerVal);
    }
    double t5 = now();
    cb->pushSend += t5 - t4;
    return 1;
}

// 1 if success, 0 if fail
int tryPopCircularBuffer(CircularBuffer cb, int thread, void *dst, BlockingCallback callback, void *callbackData)
{
    // get the latest positions
    double t1 = now();
    PosUnion _pos, _newPos;

    _pos.pos = readPosCircularBuffer(cb, thread);
    int32_t mytail = -1;
    do {
        if (_pos.pos.tail >= _pos.pos.head) {
            if (_pos.pos.tail > _pos.pos.head) {
                DIE("Invalid state! tail %ll > head %lld\n", (lld)_pos.pos.tail, (lld)_pos.pos.head);
            }
            cb->popTry += now() - t1;
            return 0; // empty
        }
        DBG("tryPopCircularBuffer: got thread=%d head=%d tail=%d i=%lld\n", thread, _pos.pos.head, _pos.pos.tail, (lld)_pos.i);

        // use cswap so as to never increment tail beyond head
        _newPos.i = _pos.i;
        _newPos.pos.tail++;
        assert(_newPos.pos.tail <= _newPos.pos.head);
        PosUnion val;
        UPC_ATOMIC_CSWAP_I64(&val.i, &(cb->posUnions[thread].i), _pos.i, _newPos.i);
        if (val.i != _pos.i) {
            DBG("tryPopCircularBuffer: attempt FAILED thread=%d head=%d tail=%d i=%lld\n", thread, val.pos.head, val.pos.tail, (lld)val.i);
            // head or tail has changed try again
            _pos.i = val.i;
        } else {
            DBG("tryPopCircularBuffer: attempt SUCCEEDED mytail=%d thread=%d head=%d tail>=%d i=%lld\n", mytail, thread, _newPos.pos.head, _newPos.pos.tail, (lld)_newPos.i);
            // got it
            mytail = val.pos.tail; // the value it was before the add
            assert(val.pos.head == _pos.pos.head);
            assert(val.pos.tail == _pos.pos.tail);
            assert(_newPos.pos.tail == mytail + 1);
            assert(val.pos.head >= val.pos.tail); // equal is okay when this pop drained it
            break;
        }
    } while (1);
    double t2 = now();
    cb->popTry += t2 - t1;

    assert(mytail >= 0);

    // data may still be in flight, wait for it, checking until header == CB_HEADER_FULL
    int32_t count = cb->counts[thread];
    shared [] char *popPos = cb->buffers[thread] + (mytail % count) * cb->blockSize;
    Header testHeader = CB_HEADER_INVALID;
    while (testHeader != CB_HEADER_FULL) {
        UPC_ATOMIC_CSWAP_I64(&testHeader, (shared [] Header *)popPos, CB_HEADER_FULL, CB_HEADER_READING);
        if (testHeader == CB_HEADER_FULL) {
            break;
        }
        if (callback) {
            (*callback)(callbackData);
        } else {
            UPC_POLL;
        }
        DBG("tryPopCircularBuffer: waiting for CB_HEADER_FULL(%lld is %lld) thread=%d mytail=%d\n", (lld)CB_HEADER_FULL, (lld)testHeader, thread, mytail);
    }
    double t3 = now();
    cb->popBusyWait += t3 - t2;

    // now get
    // Header: FULL -> READING -> EMPTY
    upc_memget(dst, popPos + sizeof(Header), cb->blockSize - sizeof(Header));

    // now update header  // TODO can this be an atomic set?
    UPC_ATOMIC_CSWAP_I64(&testHeader, (shared [] Header *)popPos, CB_HEADER_READING, CB_HEADER_EMPTY);
    if (testHeader != CB_HEADER_READING) {
        DIE("Header was not %lld: %lld!\n", (lld)CB_HEADER_READING, (lld)testHeader);
    }

    if (_newPos.pos.tail >= CB_MAX_ENTRY && _newPos.pos.tail == _newPos.pos.head) {
        // overflow condition
        // this thread just fully drained and all pushes are failing, waiting for a reset
        // verify, again that we are the last, as expected
        _pos.pos = readPosCircularBuffer(cb, thread);
        if (_newPos.pos.tail != _pos.pos.tail || _newPos.pos.head != _pos.pos.head) {
            DIE("!\n");
        }
        UPC_ATOMIC_SET_I64(NULL, &(cb->posUnions[thread].i), 0);
    }
    double t4 = now();
    cb->popSend += t4 - t3;

    return 1;
}
