#ifndef __COMMON_RANKDIR_H
#define __COMMON_RANKDIR_H

#include "defines.h"

#ifndef MAX_RANKS_PER_DIR
#define MAX_RANKS_PER_DIR 1000
#endif

#if defined (__cplusplus)
extern "C" {
#endif

char *get_basename(char *bname, const char *path);

char *get_dirname(char *dname, const char *path);

// returns 1 when it created the directory, 0 otherwise, -1 if there is an error
int check_dir(const char *path);

// replaces the given path with a rank based path, inserting a rank-based directory
// example:  get_rank_path("path/to/file_output_data.txt", rank) -> "path/to/per_rank/<rankdir>/<rank>/file_output_data.txt"
// of if rank == -1, "path/to/per_rank/file_output_data.txt"
char *get_rank_path(char *buf, int rank);

#if defined (__cplusplus)
} // extern "C"

#include <string>
#include <string.h>

static inline std::string getRankPath(const char *path, int rank)
{
    char buf[MAX_FILE_PATH];

    strcpy(buf, path);
    get_rank_path(buf, rank);
    return std::string(buf);
}
#endif

#endif // __COMMON_RANKDIR_H
