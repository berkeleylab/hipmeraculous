#ifndef __DHTABLE_H
#define __DHTABLE_H

#include "defines.h"

typedef struct {
    char    key[MAX_READ_NAME_LEN + 3];
    int64_t value;
} entry_t;

void dhtable_init(long tot_reads, double load_factor);
void dhtable_free(void);
int dhtable_put(char *key, int64_t value);
int dhtable_get(char *key, int64_t *values, int max_values);
void dhtable_check(void);
long dhtable_depth(void);

#endif
