#include "SharedBuffer.h"

#include <assert.h>

void getSharedBuffer(Buffer local_dst, const SharedBufferPtr remote_src)
{
    assert(remote_src != NULL);
    SharedBuffer sb = *remote_src;
    getSharedBuffer2(local_dst, sb);
}

void getAllSharedBuffer(Buffer local_dst, const AllSharedBufferPtr remote_src)
{
    assert(remote_src != NULL);
    SharedBuffer sb = *remote_src;
    getSharedBuffer2(local_dst, sb);
}

void getSharedBuffer2(Buffer local_dst, const SharedBuffer local_copy_of_remote_source)
{
    if (getSizeBuffer(local_dst) <= local_copy_of_remote_source.len) {
        growBuffer(local_dst, local_copy_of_remote_source.len);
    }
    resetBuffer(local_dst);

    upc_memget(appendBuffer(local_dst, local_copy_of_remote_source.len), local_copy_of_remote_source.str, local_copy_of_remote_source.len);
    appendNullBuffer(local_dst);
}

SharedBuffer newSharedBuffer(const Buffer local_source)
{
    if (!isValidBuffer(local_source)) {
        DIE("You must have initialized the Buffer here\n");
    }
    int32_t capacity = getLengthBuffer(local_source);
    SharedBuffer sb = allocSharedBuffer( capacity );
    assert(sb.size == capacity);
    sb.len = getReadLengthBuffer(local_source);
    upc_memput(sb.str, getCurBuffer(local_source), sb.len);
    return sb;
}

SharedBuffer allocSharedBuffer(int32_t capacity) {
    SharedBuffer sb;
    memset(&sb, 0, sizeof(SharedBuffer));
    sb.size = capacity;
    UPC_ALLOC_CHK(sb.str, capacity);
    return sb;
}

void putSharedBuffer2(SharedBuffer *local_copy_of_remote_dest, const Buffer local_source)
{
    int32_t len = getLengthBuffer(local_source);
    if (local_copy_of_remote_dest->size < len) {
        if (local_copy_of_remote_dest->size > 0) {
            UPC_FREE_CHK(local_copy_of_remote_dest->str);
        } else {
            assert(local_copy_of_remote_dest->str == NULL);
        }
        UPC_ALLOC_CHK(local_copy_of_remote_dest->str, len);
        local_copy_of_remote_dest->size = len;
    }
    upc_memput(local_copy_of_remote_dest->str, getStartBuffer(local_source), len);
    local_copy_of_remote_dest->len = len;
}

void _freeSharedBuffer(SharedBufferPtr remote_ptr) {
    if (remote_ptr == NULL) {
        return;
    }
    UPC_FREE_CHK(remote_ptr->str);
    remote_ptr->len = remote_ptr->size = 0;
}

