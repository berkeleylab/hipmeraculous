#ifndef __COMMON_H
#define __COMMON_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <zlib.h>
#include <unistd.h>
#include <sys/types.h>

#include "optlist.h"

#include "defines.h"
#include "version.h"
#include "common_base.h"
#include "defines.h"

#include "colors.h"
#include "StaticVars.h"

#include "common_rankpath.h"
#include "memory_chk.h"

#include "log.h"
#include "file.h"
#include "mach.h"

#define JOIN(x, y) JOIN_AGAIN(x, y)
#define JOIN_AGAIN(x, y) x ## y

#if defined (__cplusplus)
extern "C" {
#endif

#define CHECK_ERR(cmd)                                                  \
    do {                                                                \
        int err;                                                        \
        if ((err = cmd) != 0) {                                           \
            DIE("Thread %d, [%s:%d-%s]: " # cmd " failed, error %d: %s\n", MYTHREAD, __FILENAME__, __LINE__, HIPMER_VERSION, err, strerror(err)); } \
    } while (0)


#define ASSERT_BOUNDS(x, bound) do { \
        assert(x >= 0); \
        assert(x < bound); \
} while (0)

#ifdef DEBUG

#define CHECK_BOUNDS(x, bound) do {                                     \
        if (((x) >= (bound)) || (((lld)(x)) < 0)) {                               \
            lld x1 = x, bound1 = bound;                                    \
            DIE("index out of range for %s >= %s: %lld > %lld\n", # x, # bound, x1, bound1); \
        } \
} while (0)

#define CHECK_UPPER_BOUND(x, bound) do {                                \
        if ((x) >= (bound)) {                                       \
            lld x1 = x, bound1 = bound;                             \
            DIE("index out of range for %s >= %s: %lld > %lld\n", # x, # bound, x1, bound1); \
        }                                                           \
} while (0)
#else

#define CHECK_BOUNDS(x, bound)
#define CHECK_UPPER_BOUND(x, bound)

#endif

#define INT_CEIL(numerator, denominator) (((numerator) - 1) / (denominator) + 1)

#define get_file_size(fname) _get_file_size(fname, 1)
#define get_file_size_if_exists(fname) _get_file_size(fname, 0)
off_t _get_file_size(const char *fname, int warnIfStatError);

#define does_file_exist(fname) does_file_exist_at_line(fname, __FILENAME__, __LINE__)
int does_file_exist_at_line(const char *fname, const char * which_file, int which_line);

#define check_file_does_not_exist(fname) check_file_does_not_exist_at_line(fname, __FILENAME__, __LINE__)
void check_file_does_not_exist_at_line(const char *fname, const char * which_file, int which_line);

int is_same_file(const char *fname, const char *fname2);

// true if file1 is newer by mtime than file 2 or if file1 exists and file2 does not
int is_file_newer_than(const char *file1, const char *file2);

int64_t __atoi64(const char *nptr);

/* Memory helpers (from memory_chk.h too */

#ifndef LOG_ALL_MEM
#ifdef DEBUG
#ifdef LOG_SOME_MEM
#define LOG_ALL_MEM ((MYTHREAD == 0 || MYTHREAD == THREADS / 2 || MYTHREAD == THREADS - 1) && (_sv != NULL) && (MYSV._my_log != NULL)) /* just first, middle and last threads and only if a log is defined */
#else                                                                                                                                  /* ndef LOG_SOME_MEM */
#define LOG_ALL_MEM ((_sv != NULL) && (MYSV._my_log != NULL))
#endif                                                                                                                                 /* LOG_SOME_MEM */
#else                                                                                                                                  /* ndef DEBUG */
#define LOG_ALL_MEM 0
#endif                                                                                                                                 /* DEBUG */
#endif /* ndef LOG_ALL_MEM */

#define _free_chk(p) free_chk_at_line((void **)p, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
#define free_chk(p) _free_chk(&(p))
void free_chk_at_line(void **_ptr, const char *which_file, int which_line, int PAD_ALLOC, int log);

#define malloc_chk(s) malloc_chk_at_line(s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
void *malloc_chk_at_line(size_t _size, const char *which_file, int which_line, int PAD_ALLOC, int log);

#define calloc_chk(n, s) calloc_chk_at_line(n, s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
void *calloc_chk_at_line(size_t nmemb, size_t size, const char *which_file, int which_line, int PAD_ALLOC, int log);

#define realloc_chk(p, s) realloc_chk_at_line(p, s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
void *realloc_chk_at_line(void *_ptr, size_t _size, const char *which_file, int which_line, int PAD_ALLOC, int log);

#define strndup_chk(s, n) strndup_chk_at_line(s, n, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
#define strdup_chk(s) strndup_chk_at_line(s, strlen(s), __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 1)
char *strndup_chk_at_line(const char *src, size_t n, const char *which_file, int which_line, int PAD_ALLOC, int log);

#ifndef FULL_MEMCHECK
#define malloc_chk0(s) malloc_chk_at_line(s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define calloc_chk0(n, s) calloc_chk_at_line(n, s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define realloc_chk0(p, s) realloc_chk_at_line(p, s, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define strndup_chk0(s, n) strndup_chk_at_line(s, n, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define strdup_chk0(s) strndup_chk_at_line(s, strlen(s), __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define _free_chk0(p) free_chk_at_line((void **)p, __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#define free_chk0(p) free_chk_at_line((void **)&(p), __FILENAME__, __LINE__, PAD_ALLOC_BYTES, 0)
#else
#define malloc_chk0(s) malloc_chk(s)
#define calloc_chk0(n, s) calloc_chk(n, s)
#define realloc_chk0(p, s) realloc_chk(p, s)
#define strndup_chk0(s, n) strndup_chk(s, n)
#define strdup_chk0(s) strdup_chk(s)
#define _free_chk0(p) _free_chk(p)
#define free_chk0(p) free_chk(p)
#endif

size_t get_chunk_size(size_t element_size, int cores_per_node);

int purgeDir(const char *dir, lld *bytes, int *files, int *dirs);
int rmFiles(const char *fnames);

int isACGT(const char base);
int isACGTN(const char base);

int check_seqs(char *seq, char *label, int64_t *num_ambig);

/*
 * int check_seq_with_len(const char *seq, int len, char *label);
 */

char *createSHM(char *my_fname, int64_t fileSize);

#ifdef __UPC_VERSION__
#define UPC_MEMGET_STR(dst, src, len) do {                              \
        upc_memget(dst, src, len);                                      \
        if (strlen(dst) != len - 1) {                                     \
            DIE("Length mismatch when getting remote NULL terminated string: %lld != %lld\n", (lld)strlen(dst), (lld)len - 1); } \
} while (0)
#endif

size_t printFoldedSequence(GZIP_FILE out, const char *finalSequence, size_t cur_length);

int16_t fastCountMismatches(const char *a, const char *b, int len, int16_t max);

#define ADD_DIAG(type, key, val)                                        \
    do {                                                                \
        if (_sv != NULL) {                                              \
            if (MYSV._my_diags != NULL) {                      \
                char *fname = strdup(__FILENAME__);                     \
                char *s = strchr(fname, '.');                           \
                if (s) {                                                  \
                    s[0] = '\0'; }                                        \
                fprintf(MYSV._my_diags, "%s.%s\t" type "\n", fname, key, val); \
                fflush(MYSV._my_diags);                         \
                free(fname);                                            \
            }                                                           \
        }                                                               \
    } while (0)

#define FOR_BY_DIR(ITER, FIRST_IDX, LAST_IDX, DIRECTION) for( int64_t ITER = (DIRECTION) ? (FIRST_IDX) : (LAST_IDX) ; (DIRECTION) ? (ITER <= (LAST_IDX)) : (ITER >= (FIRST_IDX)); (DIRECTION) ? ITER++ : ITER-- )

#if defined (__cplusplus)
}
#endif

#endif // __COMMON_H
