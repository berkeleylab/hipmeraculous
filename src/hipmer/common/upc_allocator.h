#ifndef _UPC_ALLOCATOR_H
#define _UPC_ALLOCATOR_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <mpi.h>

#include "upc2obj.h"
#include "log.h"

static uint8_t *upc_allocator_alloc(size_t n)
{
    return upc2obj_upc_alloc(n);
}
static void upc_allocator_free(uint8_t *ptr)
{
    upc2obj_upc_free(ptr);
}
static void upc_allocator_startUPC()
{
    DBG("upc_allocator_startUPC() starting barrier\n");
    if (MPI_Barrier(MPI_COMM_WORLD) != MPI_SUCCESS) {
        fprintf(stderr, "WARNING MPI_Barrier failed!\n");
    }
    upc2obj_startUPC();
}
static void upc_allocator_endUPC()
{
    upc2obj_endUPC();
}

#endif /* _UPC_ALLOCATOR_H */
