#ifndef FASTA_PARTITION_H
#define FASTA_PARTITION_H

#include <stdio.h>

#include "common.h"
#include "Buffer.h"

typedef struct fasta_t {
    char *name;
    char *seq;
} fasta_t;

// reads the first fasta record >= startOfPartition and reads through the last record that ends at endOfPartition - 1  or crosses endOfPartition
// returns a Buffer of fasta_t
Buffer readFastaPartition(const char *filename, int64_t startOfPartition, int64_t endOfPartition);

// releases memory for of a fasta entry
void freeFasta(fasta_t fasta);

#endif // FASTA_PARTITION_H
