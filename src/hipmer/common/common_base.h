#ifndef _COMMON_BASE_H_
#define _COMMON_BASE_H_

#include "defines.h"

#if defined (__cplusplus)
extern "C" {
#endif

#define set_rank_and_size(myRank, ourSize) _set_rank_and_size(myRank, ourSize, __FILE__, __LINE__)
void _set_rank_and_size(int myRank, int ourSize, const char * file, int line);
#define get_rank() _get_rank(__FILE__, __LINE__)
int _get_rank(const char * file, int line);
#define get_num_ranks() _get_num_ranks(__FILE__, __LINE__)
int _get_num_ranks(const char * file, int line);
#define common_exit(x) _common_exit(x, __FILE__, __LINE__);
void _common_exit(int x, const char * file, int line);

#ifndef __UPC_VERSION__
#ifndef MYTHREAD
#define MYTHREAD get_rank()
#define THREADS get_num_ranks()
#endif
#endif

int isOnSameNode(int threada, int threadb);
int isOnMyNode(int threada);

#if defined (__cplusplus)
}
#endif

#endif // _COMMON_BASE_H_
