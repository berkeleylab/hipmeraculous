#ifndef UPC_ATOMIC_DATA_H_
#define UPC_ATOMIC_DATA_H_

#include <stdint.h>
#include <upc.h>
#include <string.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"

#define UPCAD_HASH_BITS        (40)
#define UPCAD_PAYLOAD_BITS     (12)
#define UPCAD_VERSION_BITS     (64 - 1 - UPCAD_HASH_BITS - UPCAD_PAYLOAD_BITS)
#define UPCAD_MAX_HASH         ((1L<<UPCAD_HASH_BITS)-1)
#define UPCAD_MAX_PAYLOAD_SIZE ((1L<<UPCAD_PAYLOAD_BITS)-1)
#define UPCAD_MAX_VERSION      ((1L<<UPCAD_VERSION_BITS)-1)

typedef struct {
    uint64_t hash         : UPCAD_HASH_BITS;
    uint64_t version      : UPCAD_VERSION_BITS;
    uint64_t inTransit    : 1;
    uint64_t payloadSize  : UPCAD_PAYLOAD_BITS;
} CheckSizeVersion;

union _AtomicDataChecksum {
    CheckSizeVersion csv;
    int64_t          val;
};
typedef union _AtomicDataChecksum AtomicDataChecksum;

typedef struct {
    uint8_t data[8];
} AtomicDataPayload;

typedef struct {
    AtomicDataChecksum checksum;
    AtomicDataPayload  payload; // data may be much larger depending on allocation of struct and payloadSize variables ... and programmer diligence...
} AtomicData;

typedef shared AtomicData *SharedAtomicDataPtr;
typedef shared [1] AtomicData *AllSharedAtomicDataPtr;
typedef shared const AtomicData *ConstSharedAtomicDataPtr;
typedef shared [1] const AtomicData *ConstAllSharedAtomicDataPtr;

uint16_t getVersionAtomicData(uint64_t largeNumber);

void initAtomicData(AtomicData *atomicData);

uint64_t hashAtomicData(const AtomicData *atomicData);

int isEmptyAtomicData(const AtomicData *atomicData);

int isValidAtomicData(const AtomicData *atomicData);

int isValidAtomicDataVersion(const AtomicData *atomicData, uint64_t version);

void setChecksumAtomicData(AtomicData *atomicData, uint16_t payloadSize, uint64_t version, const char * filename, int line);

void setAtomicData(AtomicData *atomicData, void *payload, uint16_t payloadSize, uint64_t version, const char * filename, int line);

AtomicData *newAtomicData(void *payload, uint16_t payloadSize, uint64_t version, const char * filename, int line);

void freeAtomicData(AtomicData **localCopy);

// attempts to get the remote data to local memory
// localDest->csv.payloadSize must already be populated, and will throw an error if remoteSource->csv.payloadSize is different!
// returns dest on success, NULL on failure
AtomicData * tryGetAtomicData(AtomicData * localDest, ConstSharedAtomicDataPtr remoteSource, const char * filename, int line);

void getAtomicData(AtomicData * localDest, ConstSharedAtomicDataPtr remoteSource, const char * filename, int line);

// sends localSource to remoteDest.  If optionalCSWAP is not NULL, performs an atomic CSWAP to avoid clobbering another value
int tryPutAtomicData(SharedAtomicDataPtr remoteDest, const AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line);
int tryPutAtomicData_Near(SharedAtomicDataPtr remoteDest, const AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line);

/*
 * Define specific typed functions for use in the wild
 * of fixed size atomic data
 *
 * see test_atomic_data.upc for an example
 */

#define ADD_ATOMIC_TYPED_FORWARD_DEFS(NAME, TYPE) \
    void NAME ## _initAtomicData( NAME ## _AtomicData *atomicData);\
    void NAME ## _checkAtomicData(const NAME ## _AtomicData *atomicData);\
    const AtomicData * NAME ## _convertConstAtomicData(const NAME ## _AtomicData *atomicData);\
    AtomicData * NAME ## _convertAtomicData( NAME ## _AtomicData * atomicData); \
    uint64_t NAME ## _hashAtomicData(const NAME ## _AtomicData *atomicData); \
    int NAME ## _isEmptyAtomicData(const NAME ## _AtomicData *atomicData); \
    int NAME ## _isValidAtomicData(const NAME ## _AtomicData *atomicData); \
    int NAME ## _isValidAtomicDataVersion(const NAME ## _AtomicData *atomicData, uint64_t version);\
    void NAME ## _setChecksumAtomicData(NAME ## _AtomicData *atomicData, uint64_t version, const char * filename, int line);\
    void NAME ## _setAtomicData(NAME ## _AtomicData *atomicData, TYPE *payload, uint64_t version, const char * filename, int line);\
    NAME ## _AtomicData * NAME ## _newAtomicData(TYPE *payload, uint64_t version, const char * filename, int line);\
    void NAME ## _freeAtomicData(NAME ## _AtomicData **localCopy);\
    NAME ## _AtomicData * NAME ## _tryGetAtomicData(NAME ## _AtomicData * localDest, NAME ## _ConstSharedAtomicDataPtr remoteSource, const char * filename, int line);\
    void NAME ## _getAtomicData(NAME ## _AtomicData * localDest, NAME ## _ConstSharedAtomicDataPtr remoteSource, const char * filename, int line);\
    int NAME ## _tryPutAtomicData(NAME ## _SharedAtomicDataPtr remoteDest, const NAME ## _AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line); \
    int NAME ## _tryPutAtomicData_Near(NAME ## _SharedAtomicDataPtr remoteDest, const NAME ## _AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line); \

#define ADD_ATOMIC_TYPED_DEFS(NAME, TYPE) \
    void NAME ## _initAtomicData( NAME ## _AtomicData *atomicData) { \
        if ( atomicData == NULL ) { \
            DIE(#NAME "_initAtomicData called with a NULL\n"); \
        } \
        memset(atomicData, 0, sizeof( NAME ## _AtomicData )); \
        atomicData->checksum.csv.payloadSize = sizeof(TYPE); \
    } \
    void NAME ## _checkAtomicData(const NAME ## _AtomicData *atomicData) { \
        if ( atomicData == NULL ) { \
            DIE(#NAME "_checkAtomicData called with a NULL\n"); \
        } \
        if ( atomicData->checksum.csv.payloadSize != sizeof(TYPE) ) { \
            DIE(#NAME "_checkAtomicData called with an incorrect payload size (%d) vs %d for " #TYPE "\n", atomicData->checksum.csv.payloadSize, (int) sizeof(TYPE)); \
        } \
    } \
    const AtomicData * NAME ## _convertConstAtomicData(const NAME ## _AtomicData *atomicData) { \
        NAME ## _checkAtomicData(atomicData); \
        const AtomicData *ad = (const AtomicData *) atomicData; \
        return ad; \
    } \
    \
    AtomicData * NAME ## _convertAtomicData( NAME ## _AtomicData * atomicData) { \
        NAME ## _checkAtomicData(atomicData); \
        AtomicData *ad = (AtomicData *) atomicData; \
        return ad; \
    } \
    \
    uint64_t NAME ## _hashAtomicData(const NAME ## _AtomicData *atomicData) { \
        return hashAtomicData( NAME ## _convertConstAtomicData( atomicData ) ); \
    } \
    \
    int NAME ## _isEmptyAtomicData(const NAME ## _AtomicData *atomicData) { \
        CheckSizeVersion csv = atomicData->checksum.csv; \
        return ( (csv.hash == 0) & (csv.version == 0) & ((csv.payloadSize == 0) | (csv.payloadSize == sizeof(TYPE))) ); \
    } \
    int NAME ## _isValidAtomicData(const NAME ## _AtomicData *atomicData) { \
        return isValidAtomicData( NAME ## _convertConstAtomicData( atomicData ) ); \
    } \
    \
    int NAME ## _isValidAtomicDataVersion(const NAME ## _AtomicData *atomicData, uint64_t version) { \
        return isValidAtomicDataVersion( NAME ## _convertConstAtomicData( atomicData ), version ); \
    } \
    \
    void NAME ## _setChecksumAtomicData(NAME ## _AtomicData *atomicData, uint64_t version, const char * filename, int line) { \
        NAME ## _initAtomicData(atomicData); \
        setChecksumAtomicData( NAME ## _convertAtomicData( atomicData ), sizeof(TYPE), version, filename, line); \
    } \
    \
    void NAME ## _setAtomicData(NAME ## _AtomicData *atomicData, TYPE *payload, uint64_t version, const char * filename, int line) { \
        NAME ## _initAtomicData(atomicData); \
        setAtomicData( NAME ## _convertAtomicData( atomicData ), payload, sizeof(TYPE), version, filename, line); \
    } \
    \
    NAME ## _AtomicData * NAME ## _newAtomicData(TYPE *payload, uint64_t version, const char * filename, int line) { \
        AtomicData *ad = newAtomicData(payload, sizeof(TYPE), version, filename, line); \
        NAME ## _AtomicData *nameAd = (NAME ## _AtomicData *) ad; \
        return nameAd; \
    } \
    \
    void NAME ## _freeAtomicData(NAME ## _AtomicData **localCopy)  { \
        free_chk( localCopy ); \
    } \
    \
    NAME ## _AtomicData * NAME ## _tryGetAtomicData(NAME ## _AtomicData * localDest, NAME ## _ConstSharedAtomicDataPtr remoteSource, const char * filename, int line) { \
        NAME ## _initAtomicData(localDest); \
        ConstSharedAtomicDataPtr tmp = (ConstSharedAtomicDataPtr) remoteSource; \
        AtomicData *tmp2 = tryGetAtomicData( NAME ## _convertAtomicData( localDest ), tmp, filename, line); \
        return (NAME ## _AtomicData *) tmp2; \
    } \
    \
    void NAME ## _getAtomicData(NAME ## _AtomicData * localDest, NAME ## _ConstSharedAtomicDataPtr remoteSource, const char * filename, int line) { \
        ConstSharedAtomicDataPtr tmp = (ConstSharedAtomicDataPtr) remoteSource; \
        return getAtomicData( NAME ## _convertAtomicData(localDest), tmp, filename, line); \
    } \
    \
    int NAME ## _tryPutAtomicData(NAME ## _SharedAtomicDataPtr remoteDest, const NAME ## _AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line) { \
        SharedAtomicDataPtr tmp = (SharedAtomicDataPtr) remoteDest; \
        return tryPutAtomicData( tmp, NAME ## _convertConstAtomicData(localSource), oldVal, filename, line); \
    } \
    \
    int NAME ## _tryPutAtomicData_Near(NAME ## _SharedAtomicDataPtr remoteDest, const NAME ## _AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line) { \
        SharedAtomicDataPtr tmp = (SharedAtomicDataPtr) remoteDest; \
        return tryPutAtomicData_Near( tmp, NAME ## _convertConstAtomicData(localSource), oldVal, filename, line); \
    } \


#define INIT_ATOMIC_DATA(NAME, TYPE) \
    typedef struct { \
        AtomicDataChecksum checksum; \
        TYPE payload; \
    } NAME ## _AtomicData; \
    typedef shared NAME ## _AtomicData * NAME ## _SharedAtomicDataPtr; \
    typedef shared [1] NAME ## _AtomicData * NAME ## _AllSharedAtomicDataPtr; \
    typedef shared const NAME ## _AtomicData * NAME ## _ConstSharedAtomicDataPtr; \
    typedef shared [1] const NAME ## _AtomicData * NAME ## _ConstAllSharedAtomicDataPtr; \
    ADD_ATOMIC_TYPED_FORWARD_DEFS(NAME, TYPE) \




#endif // UPC_ATOMIC_DATA_H_
