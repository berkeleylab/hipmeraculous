/*-
 * Copyright (c) 2013 Cosku Acay, http://www.coskuacay.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Taken from https://github.com/cacay/MemoryPool.git
 * modified to work with upc_allocator by Rob Egan
 */

#ifndef MEMORY_BLOCK_TCC
#define MEMORY_BLOCK_TCC

#include "../kmercount/mpi_common.h"
#include "common.h"

#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline typename MemoryPool<T, BlockCount>::size_type
MemoryPool<T, BlockCount>::padPointer(data_pointer_ p, size_type align)
#else
template <typename T>
inline typename MemoryPool<T>::size_type
MemoryPool<T>::padPointer(data_pointer_ p, size_type align)
#endif

const 
{
  uintptr_t result = reinterpret_cast<uintptr_t>(p);
  return ((align - result) % align);
}



#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
MemoryPool<T, BlockCount>::MemoryPool()
#else
template <typename T>
MemoryPool<T>::MemoryPool()
#endif

{
  currentBlock_ = nullptr;
  currentSlot_ = nullptr;
  lastSlot_ = nullptr;
  freeSlots_ = nullptr;
  DBG2("MemoryPool() this=%p\n", this);
}



#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
MemoryPool<T, BlockCount>::MemoryPool(const MemoryPool& memoryPool)
#else
template <typename T>
MemoryPool<T>::MemoryPool(const MemoryPool& memoryPool)
#endif
 :
MemoryPool()
{
  DBG2("MemoryPool(& %p): %p\n", &memoryPool, this);
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
MemoryPool<T, BlockCount>::MemoryPool(MemoryPool&& memoryPool)
#else
template <typename T>
MemoryPool<T>::MemoryPool(MemoryPool&& memoryPool)
#endif

{
  currentBlock_ = memoryPool.currentBlock_;
  memoryPool.currentBlock_ = nullptr;
  currentSlot_ = memoryPool.currentSlot_;
  lastSlot_ = memoryPool.lastSlot_;
  freeSlots_ = memoryPool.freeSlots_;
  DBG2("MemoryPool(&& %p): %p\n", &memoryPool, this);
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
template<class U>
MemoryPool<T, BlockCount>::MemoryPool(const MemoryPool<U>& memoryPool)
#else
template <typename T>
template<class U>
MemoryPool<T>::MemoryPool(const MemoryPool<U>& memoryPool)
#endif
 :
MemoryPool()
{
  DBG2("MemoryPool(const MemoryPool<U>& %p): %p\n", &memoryPool, this);
}



#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
MemoryPool<T, BlockCount>&
MemoryPool<T, BlockCount>::operator=(MemoryPool&& memoryPool)
#else
template <typename T>
MemoryPool<T>&
MemoryPool<T>::operator=(MemoryPool&& memoryPool)
#endif

{
  if (this != &memoryPool)
  {
    std::swap(currentBlock_, memoryPool.currentBlock_);
    currentSlot_ = memoryPool.currentSlot_;
    lastSlot_ = memoryPool.lastSlot_;
    freeSlots_ = memoryPool.freeSlots_;
    DBG2("MemoryPool::operator=(&& %p): %p\n", &memoryPool, this);
  }
  return *this;
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
MemoryPool<T, BlockCount>::~MemoryPool()
#else
template <typename T>
MemoryPool<T>::~MemoryPool()
#endif
{
  slot_pointer_ curr = currentBlock_;
  size_t count = 0;
  while (curr != nullptr) {
    slot_pointer_ prev = curr->next;
    DBG2("~MemoryPool(): deallocating %p\n", curr);
    UPCAllocator::deallocate( reinterpret_cast<pointer>(curr) );
    curr = prev;
    count++;
  }
  DBG2("~MemoryPool(): this=%p. Deallocated %lld blocks\n", this, (lld) count);
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline typename MemoryPool<T, BlockCount>::pointer
MemoryPool<T, BlockCount>::address(reference x)
#else
template <typename T>
inline typename MemoryPool<T>::pointer
MemoryPool<T>::address(reference x)
#endif
const 
{
  return &x;
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline typename MemoryPool<T, BlockCount>::const_pointer
MemoryPool<T, BlockCount>::address(const_reference x)
#else
template <typename T>
inline typename MemoryPool<T>::const_pointer
MemoryPool<T>::address(const_reference x)
#endif
const 
{
  return &x;
}



#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
void
MemoryPool<T, BlockCount>::allocateBlock()
#else
template <typename T>
void
MemoryPool<T>::allocateBlock()
#endif
{
  DBG2("MemoryPool::allocateBlock() this=%p\n", this);
  // Allocate space for the new block and store a pointer to the previous one
  size_t allocSize = BlockSize + sizeof(slot_type_);
  size_t numObjects = (allocSize + sizeof(T) - 1) / sizeof(T);
  data_pointer_ newBlock = reinterpret_cast<data_pointer_> ( UPCAllocator::allocate( numObjects, NULL) );

  if (newBlock == NULL) DIE("Could not allocate memory for another block of size %lld\n", (lld) allocSize);

  reinterpret_cast<slot_pointer_>(newBlock)->next = currentBlock_;
  currentBlock_ = reinterpret_cast<slot_pointer_>(newBlock);
  // Pad block body to staisfy the alignment requirements for elements
  data_pointer_ body = newBlock + sizeof(slot_pointer_);
  size_type bodyPadding = padPointer(body, alignof(slot_type_));
  currentSlot_ = reinterpret_cast<slot_pointer_>(body + bodyPadding);
  lastSlot_ = reinterpret_cast<slot_pointer_>
              (newBlock + BlockSize - sizeof(slot_type_) + 1);
  DBG("MemoryPool::allocateBlock(): this=%p, newBlock=%p, size=%lld %s\n", this, newBlock, (lld) allocSize, (UPCAllocator::isSharedAllocation( reinterpret_cast<pointer>(newBlock) ) ? "upc-shared" : "private"));
}



#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline typename MemoryPool<T, BlockCount>::pointer
MemoryPool<T, BlockCount>::allocate(size_type n, const_pointer hint)
#else
template <typename T>
inline typename MemoryPool<T>::pointer
MemoryPool<T>::allocate(size_type n, const_pointer hint)
#endif
{
  DBG3("MemoryPool<T>::allocate(%ld, %p)\n", (long) n, hint);
  if (n == 1 && freeSlots_ != nullptr) {
    pointer result = reinterpret_cast<pointer>(freeSlots_);
    freeSlots_ = freeSlots_->next;
    DBG3("MemoryPool::allocate %p (%lld): %p\n", this, (lld) n, result);
    return result;
  }
  else {
    pointer result = NULL;
    if (n > 1) {
        DBG("MemoryPool<T>::allocate(%ld, %p)\n", (long) n, hint);
        // allocate a stand-alone perfectly sized block outside of the linked list of blocks
        result = reinterpret_cast<pointer>( UPCAllocator::allocate( n, NULL) );
    } else {
        if (currentSlot_ >= lastSlot_)
          allocateBlock();
        result = reinterpret_cast<pointer>(currentSlot_++);
    }
    DBG3("MemoryPool::allocate %p (%lld): (newblock) %p\n", this, (lld) n, result);
    return result;
  }
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline void
MemoryPool<T, BlockCount>::deallocate(pointer p, size_type n)
#else
template <typename T>
inline void
MemoryPool<T>::deallocate(pointer p, size_type n)
#endif

{
DBG("MemoryPool<T>::deallocate(%p, %ld)\n", p, (long) n);
  if (p != nullptr) {
    assert(n != 0);
    if (n > 1) {
        // free the stand alone block
        UPCAllocator::deallocate(p, n);
    } else {
        reinterpret_cast<slot_pointer_>(p)->next = freeSlots_;
        freeSlots_ = reinterpret_cast<slot_pointer_>(p);
    }
    DBG2("MemoryPool::deallocate(%p, %lld)\n", p, (lld) n);
  }
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline typename MemoryPool<T, BlockCount>::size_type
MemoryPool<T, BlockCount>::max_size()
#else
template <typename T>
inline typename MemoryPool<T>::size_type
MemoryPool<T>::max_size()
#endif
const 
{
  size_type maxBlocks = -1 / BlockSize;
  return (BlockSize - sizeof(data_pointer_)) / sizeof(slot_type_) * maxBlocks;
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
template <class U, class... Args>
inline void
MemoryPool<T, BlockCount>::construct(U* p, Args&&... args)
#else
template <typename T>
template <class U, class... Args>
inline void
MemoryPool<T>::construct(U* p, Args&&... args)
#endif
{
  new (p) U (std::forward<Args>(args)...);
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
template <class U>
inline void
MemoryPool<T, BlockCount>::destroy(U* p)
#else
template <typename T>
template <class U>
inline void
MemoryPool<T>::destroy(U* p)
#endif
{
  p->~U();
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
template <class... Args>
inline typename MemoryPool<T, BlockCount>::pointer
MemoryPool<T, BlockCount>::newElement(Args&&... args)
#else
template <typename T>
template <class... Args>
inline typename MemoryPool<T>::pointer
MemoryPool<T>::newElement(Args&&... args)
#endif
{
  pointer result = allocate();
  construct<value_type>(result, std::forward<Args>(args)...);
  return result;
}


#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount>
inline void
MemoryPool<T, BlockCount>::deleteElement(pointer p)
#else
template <typename T>
inline void
MemoryPool<T>::deleteElement(pointer p)
#endif
{
  if (p != nullptr) {
    p->~value_type();
    deallocate(p);
  }
}



#endif // MEMORY_BLOCK_TCC
