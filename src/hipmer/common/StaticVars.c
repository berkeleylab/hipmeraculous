#include "StaticVars.h"

StaticVars _sv = NULL;

CountTimer initCountTimer(CountTimer *ct) {
    CountTimer empty = {0,0.0};
    if (ct) *ct = empty;
    return empty;
}

void addCountTime(CountTimer *ct, double t)
{
    ++(ct->count);
    ct->time += t;
}
