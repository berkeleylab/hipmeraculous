#ifndef UU_TRAVERSAL_GENERIC_H
#define UU_TRAVERSAL_GENERIC_H

#include <assert.h>
#include <upc.h>

#ifndef CONTIGS_DDS_TYPE
#error "You must specify CONTIGS_DDS_TYPE before including this file"
#endif

#include "meraculous.h"

#include "common.h"

#include "../contigs/contigs_common.h"

extern shared int64_t contig_id;
#ifdef STORE_CONTIGS_IN_ARRAY
extern shared[BS] contig_ptr_t * contig_list;
#endif

#ifdef PROFILE
extern int64_t bases_added;
#endif

#ifdef UU_TRAV_PROFILE
extern double walking_time;
extern double UU_time;
#endif

extern int64_t myContigs;

#define MARK_POSITION_IN_CONTIG JOIN(CONTIGS_DDS_TYPE, markPosInContig)
#define UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY JOIN(CONTIGS_DDS_TYPE, unpackKmerAndExtensionsLocalCopy)
#define UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT JOIN(CONTIGS_DDS_TYPE, unpackKmerAndExtensionsLocalCopyOrient)
#define UNPACK_KMER_AND_EXTENSIONS JOIN(CONTIGS_DDS_TYPE, unpackKmerAndExtensions)
#define GET_NEXT_HASH_TABLE_ENTRY JOIN(CONTIGS_DDS_TYPE, getNextHashTableEntry)
#define IS_LEFT_KMER_UU JOIN(CONTIGS_DDS_TYPE, isLeftKmerUU)
#define IS_RIGHT_KMER_UU JOIN(CONTIGS_DDS_TYPE, isRightKmerUU)
#define IS_KMER_UU_LOCAL_COPY JOIN(CONTIGS_DDS_TYPE, isKmerUULocalCopy)
#define IS_KMER_UU JOIN(CONTIGS_DDS_TYPE, isKmerUU)
#define GET_NEXT_UNUSED_UU_KMER JOIN(CONTIGS_DDS_TYPE, getNextUnusedUUKmer)
#define WALK_RIGHT JOIN(CONTIGS_DDS_TYPE, walk_right)
#define WALK_LEFT JOIN(CONTIGS_DDS_TYPE, walk_left)
#define CLEAN_KMER_CONTIG_PTR JOIN(CONTIGS_DDS_TYPE, clean_kmer_contig_ptr)
#define VALIDATE_KMERS_AND_CONTIGS JOIN(CONTIGS_DDS_TYPE, validate_kmers_and_contigs)
#define INITIALIZE_NEW_CONTIG JOIN(CONTIGS_DDS_TYPE, initialize_new_contig)
#define UU_GRAPH_TRAVERSAL JOIN(CONTIGS_DDS_TYPE, UU_graph_traversal)

#define GET_CONTIG_BOX_OF_KMER JOIN(CONTIGS_DDS_TYPE, get_contig_box_of_kmer)
#define GET_CONTIG_OF_KMER JOIN(CONTIGS_DDS_TYPE, get_contig_of_kmer)
/* get the contig_ptr "box" associated with a kmer -- must be USED already to work */
shared[] contig_ptr_box_list_t *GET_CONTIG_BOX_OF_KMER(shared[] LIST_T *lookup_res, int follow_list);

/* get the contig associated with a kmer -- must be USED already to work */
shared[] contig_t *GET_CONTIG_OF_KMER(shared[] LIST_T *lookup_res, int follow_list);




#ifdef MARK_POS_IN_CONTIG
/* Stores the exact position of each kmer in the eventual contig */
int MARK_POSITION_IN_CONTIG(contig_ptr_t in_contig, HASH_TABLE_T *hashtable, int kmer_len);
#endif

/* unpacks left_ext + kmer + right_ext into a single contiguous string of length kmer_len+2 */
void UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY(LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int kmer_len);

void UNPACK_KMER_AND_EXTENSIONS_LOCAL_COPY_ORIENT(LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int is_least, int kmer_len);

void UNPACK_KMER_AND_EXTENSIONS(shared[] LIST_T *new_kmer_seed_ptr, kmer_and_ext_t *kmer_and_ext, int kmer_len);


/* TODO: Add function that computes hash using the packed form */


shared[] LIST_T *GET_NEXT_HASH_TABLE_ENTRY(HASH_TABLE_T *hashtable, int64_t *seed_index, shared[] LIST_T *old_kmer_seed_ptr, int kmer_len);


int IS_LEFT_KMER_UU(HASH_TABLE_T *hashtable, kmer_and_ext_t *kmer_and_ext, int kmer_len);

/* expects kmer_len+2 string kmer_and_ext: le-kmer-re */
int IS_RIGHT_KMER_UU(HASH_TABLE_T *hashtable, kmer_and_ext_t *kmer_and_ext, int kmer_len);

int IS_KMER_UU_LOCAL_COPY(HASH_TABLE_T *hashtable, LIST_T *copy, char *new_seed_le, char *new_seed_re, int kmer_len);

int IS_KMER_UU(HASH_TABLE_T *hashtable, shared[] LIST_T *new_kmer_seed_ptr, char *new_seed_le, char *new_seed_re, int kmer_len);

shared[] LIST_T *GET_NEXT_UNUSED_UU_KMER(HASH_TABLE_T *hashtable, int64_t *seed_index, shared[] LIST_T *new_kmer_seed_ptr, char *new_seed_le, char *new_seed_re, int kmer_len);

/* Performs the right walk on the UU-graph traversal */
int WALK_RIGHT(HASH_TABLE_T *dist_hashtable, contig_ptr_t cur_contig, char seed_re, char *last_right_extension_found, contig_ptr_t *right_contig, int kmer_len);

/* Performs the left walk on the UU-graph traversal */
int WALK_LEFT(HASH_TABLE_T *dist_hashtable, contig_ptr_t cur_contig, char seed_le, char *last_left_extension_found, contig_ptr_t *left_contig, int kmer_len);

void CLEAN_KMER_CONTIG_PTR(HASH_TABLE_T *hashtable, int kmer_len);

void VALIDATE_KMERS_AND_CONTIGS(HASH_TABLE_T *hashtable, int kmer_len);

contig_ptr_t INITIALIZE_NEW_CONTIG(shared[] LIST_T *new_kmer_seed_ptr, int kmer_len);

/* Performs the UU-graph traversal using the WALK_RIGHT and WALK_LEFT routines */
int UU_GRAPH_TRAVERSAL(HASH_TABLE_T *hashtable, GZIP_FILE output_file, int min_contig_length, int kmer_len);

#endif // UU_TRAVERSAL_GENERIC_H
