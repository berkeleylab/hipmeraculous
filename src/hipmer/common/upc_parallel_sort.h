// This header is designed to be included potentially multiple times
//   but with a different value of UPC_PARAALLEL_SORT_TYPE macro definion
// The api is close to the qsort function -- an in-place sorting routine
// The api will be UPC_PARALLEL_SORT_TYPE ## _parallel_sort(Buffer myBuffer, PSORT_CMP cmp)
// where buffer is an array of 0+ UPC_PARALLEL_SORT_TYPE 
// and PSORT_CMP is a function pointer identical to qsort cmp function that works on a private pointer to that type
// the sorted results will be replaced into myBuffer ordered by thread

#ifndef UPC_PARALLEL_SORT_TYPE
#error "You must specify UPC_PARALLEL_SORT_TYPE before including this file"
#endif

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif

#ifdef UPC_PARALLEL_SORT_H

// erase the definitions from the previous loading

#undef PSORT_T
#undef PSORT_CMP
#undef PSORT_T_PTR
#undef PSORT_T_SHPTR
#undef PSORT_T_ALLSHPTR
#undef PSORT_T_ALLSH_SHPTR

#undef _PSORT_STRUCT
#undef PSORT_STRUCT
#undef PARALLEL_SORT

#endif

// now protect against double loading
#ifndef UPC_PARALLEL_SORT_H
#define UPC_PARALLEL_SORT_H UPC_PARALLEL_SORT_TYPE

#include <stdlib.h>
#include <stdint.h>

#include "upc.h"

#include "upc_common.h"
#include "upc_compatibility.h"
#include "common.h"
#include "utils.h"
#include "log.h"
#include "timers.h"
#include "Buffer.h"

#define PSORT_T                  JOIN(UPC_PARALLEL_SORT_TYPE, _ps_t)
#define PSORT_CMP                JOIN(UPC_PARALLEL_SORT_TYPE, _ps_cmp)
#define PSORT_T_PTR              JOIN(UPC_PARALLEL_SORT_TYPE, _ps_t_ptr)
#define PSORT_T_SHPTR            JOIN(UPC_PARALLEL_SORT_TYPE, _ps_t_shptr)
#define PSORT_T_ALLSHPTR         JOIN(UPC_PARALLEL_SORT_TYPE, _ps_t_allshptr)
#define PSORT_T_ALLSH_SHPTR      JOIN(UPC_PARALLEL_SORT_TYPE, _ps_t_allsh_shptr)

#define _PSORT_STRUCT            JOIN(UPC_PARALLEL_SORT_TYPE, __ps_struct)
#define PSORT_STRUCT             JOIN(UPC_PARALLEL_SORT_TYPE, _ps_struct)

#define PARALLEL_SORT            JOIN(UPC_PARALLEL_SORT_TYPE, _parallel_sort)

typedef UPC_PARALLEL_SORT_TYPE PSORT_T;
typedef int (*PSORT_CMP)(const void *a, const void *b); 
typedef UPC_PARALLEL_SORT_TYPE *PSORT_T_PTR;
typedef shared [] UPC_PARALLEL_SORT_TYPE *PSORT_T_SHPTR;
typedef shared [1] UPC_PARALLEL_SORT_TYPE *PSORT_T_ALLSHPTR;
typedef shared [1] PSORT_T_SHPTR *PSORT_T_ALLSH_SHPTR;

typedef struct _PSORT_STRUCT {
    PSORT_CMP cmp;
    PSORT_T_ALLSH_SHPTR globalData; // THREADS where all the first entries are local to thread 0
    PSORT_T_ALLSHPTR    partitions; // THREADS*THREADS with a copy of each entry at partition boundaries known by each thread
    shared [1] int64_t *partitionCounts;
    shared [1] int64_t *totalCount;
    shared [1] int64_t *buf;
    PSORT_T_PTR globalPartitions; // THREADS entries
    int64_t *myOffsets, *myCounts;
} _PSORT_STRUCT;
typedef _PSORT_STRUCT *PSORT_STRUCT;

void PARALLEL_SORT(Buffer myArray, PSORT_CMP cmp);

#endif // UPC_PARALLEL_SORT_H

