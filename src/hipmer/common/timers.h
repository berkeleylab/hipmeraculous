#ifndef __TIMERS_H
#define __TIMERS_H

#include "log.h"
// #include "hipmer_timemory.h"

#define MAX_TIMERS 200

#ifndef T_BARRIER
#define T_BARRIER (MAX_TIMERS - 1)
#endif

#define UPC_LOGGED_BARRIER do { \
        double _timedBarrierStart = now();                       \
        LOGF("myBarrier entered.\n");  \
        upc_barrier; \
        double _duration = now() - _timedBarrierStart; \
        ADD_BARRIER_TIME(_duration); \
        LOGF("myBarrier time: %0.3f s (since start %0.3f)\n", _duration, GET_BARRIER_TIME());  \
        log_mem(); \
} while (0)

#define UPC_TIMED_BARRIER do {                        \
        double _timedBarrierStart = START_TIMER(T_BARRIER);                       \
        upc_barrier;                                  \
        double _timedBarrierEnd = stop_timer(T_BARRIER);                        \
        double _duration = _timedBarrierEnd - _timedBarrierStart; \
        ADD_BARRIER_TIME(_duration); \
        LOGF("myBarrier time: %0.3f s (since start %0.3f)\n", _duration, GET_BARRIER_TIME()); \
        log_mem(); \
} while (0)

#define TICKS_TO_S(t) ((double)upc_ticks_to_ns(t) / 1000000000.0)
#define ELAPSED_TIME(t) TICKS_TO_S(upc_ticks_now() - (t))

void init_timers(void);
#define START_TIMER(t) start_timer(t, # t)
double start_timer(int t, const char *desc);
double stop_timer(int t);
double now();
double get_elapsed_time(int t);
double get_timer_all_max(int t);
void print_timers(int serial_only);



#endif
