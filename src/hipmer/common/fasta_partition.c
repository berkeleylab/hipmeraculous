#include "fasta_partition.h"

#include "log.h"

Buffer readFastaPartition(const char *filename, int64_t startOfPartition, int64_t endOfPartition)
{
    Buffer seqs = initBuffer(1024*sizeof(fasta_t));
    Buffer seq = initBuffer(8192);
    Buffer line = initBuffer(100);
    fasta_t tmp;
    memset(&tmp, 0, sizeof(tmp));
    FILE *f = fopen_chk(filename, "r");
    fseek(f, startOfPartition, SEEK_SET);
    int64_t pos = ftell(f);
    int64_t bytes = 0, bases = 0;
    while(fgetsBuffer(line, 4096, f)) {
        bytes += getLengthBuffer(line);
        if ( *getStartBuffer(line) == '>' ) {
            // new record
            if (pos < endOfPartition) {
                if (tmp.name) {
                    tmp.seq = strdup_chk0(getStartBuffer(seq));
                    memcpyBuffer(seqs, &tmp, sizeof(tmp));
                }
                memset(&tmp, 0, sizeof(tmp));
                tmp.name = strdup_chk0(getStartBuffer(line) + 1);
            } else break;
            resetBuffer(seq);
        } else {
            strncpyBuffer(seq, getStartBuffer(line), getLengthBuffer(line) - 1); // no newline
            bases += getLengthBuffer(line);
        }
        resetBuffer(line);
        pos = ftell(f);
    }
    if (tmp.name) {
        tmp.seq = strdup_chk0(getStartBuffer(seq));
        memcpyBuffer(seqs, &tmp, sizeof(tmp));
    } else {
        assert(!tmp.seq);
    }
    fclose_track(f);
    freeBuffer(line);
    freeBuffer(seq);
    LOGF("readFastaPartition(%s,%lld,%lld) partition=%lld, sequences=%lld, bytes=%lld, bases=%lld\n", filename, (lld) startOfPartition, (lld) endOfPartition, (lld) endOfPartition-startOfPartition, (lld) getLengthBuffer(seqs) / sizeof(tmp), (lld) bytes, (lld) bases);
    return seqs;
}

void freeFasta(fasta_t fasta) {
    free_chk0(fasta.name);
    free_chk0(fasta.seq);
}

