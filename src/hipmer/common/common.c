#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/stat.h>
#ifdef __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#else
#include <sys/vfs.h>
#endif
#include <sys/types.h>
#include <dirent.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>
#include <stdarg.h>
#include <time.h>
#include <zlib.h>
#include <glob.h>

#include "version.h"
#include "defines.h"
#include "common.h"

off_t _get_file_size(const char *fname, int warnIfStatError)
{
    struct stat s;

    if (stat(fname, &s) != 0) {
        if (warnIfStatError) {
            WARN("could not stat %s: %s\n", fname, strerror(errno));
        }
        return 0;
    }
    return s.st_size;
}

int does_file_exist_at_line(const char *fname,  const char * which_file, int which_line)
{
    struct stat s;

    if (stat(fname, &s) != 0) {
        LOGF("(%s:%d) Could not find file: %s\n", which_file, which_line, fname);
        return 0;
    }
    return 1;
}

void check_file_does_not_exist_at_line(const char *fname, const char * which_file, int which_line)
{
    off_t osize = _get_file_size(fname, 0);

    if (osize > 0) {
        char newfpath[MAX_FILE_PATH];
        sprintf(newfpath, "%s.old", fname);
        WARN("(%s:%d) Renaming existing file %s to %s\n", which_file, which_line, fname, newfpath);
        rename(fname, newfpath);
    }
}

int is_same_file(const char *fname, const char *fname2)
{
    struct stat s, s2;

    if (stat(fname, &s) != 0) {
        LOGF("Could not find %s\n", fname);
        return 0;
    }
    if (stat(fname2, &s2) != 0) {
        LOGF("Could not find %s\n", fname2);
        return 0;
    }
    return (s.st_dev == s2.st_dev) & (s.st_ino == s2.st_ino);
}

// true if file1 is newer by mtime than file 2 or if file1 exists and file2 does not
int is_file_newer_than(const char *file1, const char *file2)
{
    struct stat s, s2;
    if (stat(file1, &s) != 0) {
        LOGF("is_newer: Could not find %s\n", file1);
        return 0;
    }
    if (stat(file2, &s2) != 0) {
        LOGF("is_newer: Could not find %s\n", file2);
        return 1;
    }
    return s.st_mtime > s2.st_mtime;
}


int64_t __atoi64(const char *nptr)
{
    int c;
    int64_t value;
    int sign;

    while (isspace((int)*nptr)) {
        ++nptr;
    }

    c = (int)*nptr++;
    sign = c;
    if (c == '-' || c == '+') {
        c = (int)*nptr++;
    }

    value = 0;

    while (isdigit(c)) {
        value = 10 * value + (c - '0');
        c = (int)*nptr++;
    }

    if (sign == '-') {
        return -value;
    } else {
        return value;
    }
}

/* Memory helpers (from memory_chk.h too */

void free_chk_at_line(void **_ptr, const char *which_file, int which_line, int PAD_ALLOC, int log)
{
    if (!_ptr) {
        DIE("[%s:%d-%s]: free_chk: called with invalid pointer to pointer!\n", which_file, which_line, HIPMER_VERSION);
    }
    void *ptr = *_ptr;
    if (ptr) {
        char msg[256];
        size_t s = 0;
        if (PAD_ALLOC > 0) {
            ptr = pad_memory_check_at_line(ptr, msg, PAD_ALLOC, which_file, which_line);
        }
        if (ptr == NULL) {
            DIE("[%s:%d-%s] Request to free a pointer, but padding is corrupt! %s\n", which_file, which_line, HIPMER_VERSION, msg);
        }
        s = pad_memory_real_size(*_ptr, PAD_ALLOC);
        if (log) {
            MEMCHECK_COUNT(-(PAD_ALLOC ? s : 1));
        } else { MEMCHECK_COUNT0(-(PAD_ALLOC ? s : 1)); }
        if (log && (_sv != NULL) && LOG_ALL_MEM && MYSV.logMemcheck > 0) {
            LOGFN("[%s:%d] %lld %lld free_chk(%p) realPtr=%p PAD=%llu size=%llu\n", which_file, which_line, (lld)MYSV.outstandingMallocs, (lld)MYSV.untracked_outstandingMallocs, *_ptr, ptr, (llu)PAD_ALLOC, (llu)s);
        }
        if ((_sv != NULL) && MYSV.outstandingMallocs < 0) {
            WARN("[%s-%d] more freed than malloc: %lld\n", which_file, which_line, (lld)MYSV.outstandingMallocs);
        }
        free(ptr);
    } else {
        WARN("[%s:%d-%s]: free_chk called on NULL pointer!\n", which_file, which_line, HIPMER_VERSION);
    }
    *_ptr = NULL;
}

void *malloc_chk_at_line(size_t _size, const char *which_file, int which_line, int PAD_ALLOC, int log)
{
    size_t size = _size;

    if (!size) {
        LOGF("[%s:%d-%s]: Request to malloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size);
        return NULL;
    }
    size += PAD_ALLOC * 2;
    void *_x = malloc(size);
    void *x = _x;
    if (!x) {
        DIE("[%s:%d-%s]: Could not malloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size);
    }
    if (PAD_ALLOC > 0) {
        x = pad_memory_at_line(x, size, PAD_ALLOC, which_file, which_line);
    }
    if (log) {
        MEMCHECK_COUNT(PAD_ALLOC ? size : 1);
    } else {
        MEMCHECK_COUNT0(PAD_ALLOC ? size : 1);
    }
    if (log && (_sv != NULL) && LOG_ALL_MEM && MYSV.logMemcheck > 0) {
        LOGFN("[%s:%d] %lld malloc_chk(%llu) %p realPtr=%p realSize=%llu PAD=%llu\n", which_file, which_line, (lld)MYSV.outstandingMallocs, (llu)_size, x, _x, (llu)size, (llu)PAD_ALLOC);
    }
    return x;
}

void *calloc_chk_at_line(size_t nmemb, size_t size, const char *which_file, int which_line, int PAD_ALLOC, int log)
{
    if (!size) {
        LOGF("[%s:%d-%s]: Request to calloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size);
        return NULL;
    }
    if (!nmemb) {
        DIE("[%s:%d-%s]: Request to calloc %llu elements\n", which_file, which_line, HIPMER_VERSION, (llu)nmemb);
    }
    size_t realSize = nmemb * size + 2 * PAD_ALLOC;
    void *_x = malloc(realSize);
    void *x = _x;
    if (!x) {
        DIE("[%s:%d-%s]: Could not calloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size * nmemb);
    }
    memset(x, 0, realSize);
    if (PAD_ALLOC > 0) {
        x = pad_memory_at_line(x, realSize, PAD_ALLOC, which_file, which_line);
    }
    if (log) {
        MEMCHECK_COUNT(PAD_ALLOC ? realSize : 1);
    } else { MEMCHECK_COUNT0(PAD_ALLOC ? realSize : 1); }
    if (log && (_sv != NULL) && LOG_ALL_MEM && MYSV.logMemcheck > 0) {
        LOGFN("[%s:%d] %lld calloc_chk(%llu, %llu) %p realPtr=%p realSize=%llu PAD=%llu\n", which_file, which_line, (lld)MYSV.outstandingMallocs, (llu)nmemb, (llu)size, x, _x, (llu)realSize, (llu)PAD_ALLOC);
    }
    return x;
}

void *realloc_chk_at_line(void *_ptr, size_t _size, const char *which_file, int which_line, int PAD_ALLOC, int log)
{
    size_t size = _size;
    void *ptr = _ptr;

    if (ptr != NULL) {
        // old memory will be freed, account for that
        char msg[256];
        if (PAD_ALLOC > 0) {
            ptr = pad_memory_check_at_line(ptr, msg, PAD_ALLOC, which_file, which_line);
        }
        if (ptr == NULL) {
            DIE("[%s:%d-%s]: Request to realloc %llu bytes, but padding is corrupt! %s\n", which_file, which_line, HIPMER_VERSION, (llu)size, msg);
        }
        size_t oldSize = PAD_ALLOC ? pad_memory_real_size(_ptr, PAD_ALLOC) : 0;
        if (size > 0) {
            if (log) {
                MEMCHECK_COUNT(-(PAD_ALLOC ? oldSize : 0));
            } else {
                MEMCHECK_COUNT0(-(PAD_ALLOC ? oldSize : 0));
            }
        }
    }
    if (size == 0) {
        // really just free then
        LOGF("[%s:%d-%s]: Request to realloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size);
        if (ptr) {
            free_chk_at_line(&ptr, which_file, which_line, PAD_ALLOC, log);
        }
        return NULL;
    }
    size += PAD_ALLOC * 2;
    void *_x = realloc(ptr, size);
    void *x = _x;
    if (!x) {
        DIE("[%s:%d-%s]: Could not realloc %llu bytes\n", which_file, which_line, HIPMER_VERSION, (llu)size);
    }
    if (PAD_ALLOC > 0) {
        x = pad_memory_at_line(x, size, PAD_ALLOC, which_file, which_line);
    }
    if (log) {
        MEMCHECK_COUNT(PAD_ALLOC ? size : (_ptr == NULL ? 1 : 0));
    } else {
        MEMCHECK_COUNT0(PAD_ALLOC ? size : (_ptr == NULL ? 1 : 0));
    }
    if (log && (_sv != NULL) && LOG_ALL_MEM && MYSV.logMemcheck > 0) {
        LOGFN("[%s:%d] %lld realloc_chk(%llu) %p -> %p realPtrs: %p -> %p realSize=%llu PAD=%llu\n", which_file, which_line, (lld)MYSV.outstandingMallocs, (llu)_size, _ptr, x, ptr, _x, (llu)size, (llu)PAD_ALLOC);
    }
    return x;
}


char *strndup_chk_at_line(const char *src, size_t n, const char *which_file, int which_line, int PAD_ALLOC, int log)
{
    if (log && (_sv != NULL) && LOG_ALL_MEM && MYSV.logMemcheck > 0) {
        LOGFN("[%s:%d] strdup(%lld)\n", which_file, which_line, (lld)n);
    }
    char *ptr = (char *)malloc_chk_at_line((n * sizeof(char)) + 1, which_file, which_line, PAD_ALLOC, log);
    if (!strncpy(ptr, src, n * sizeof(char))) {
        DIE("[%s:%d]: Could not strncpy(%lld)\n", which_file, which_line, (lld)n);
    }
    ptr[n] = '\0';
    return ptr;
}

size_t get_chunk_size(size_t element_size, int cores_per_node)
{
    size_t chunk_size = (TARGET_CHUNK_PER_THREAD + element_size - 1) / element_size;

    while (chunk_size >= 2 && chunk_size * THREADS * element_size * cores_per_node > MAX_CHUNK_MEMORY_PER_NODE) {
        chunk_size /= 2;
    }
    assert(chunk_size >= 1);
    if (chunk_size < MIN_CHUNK_SIZE) {
        chunk_size = 1;
    }
    return chunk_size;
}

int purgeDir(const char *dir, lld *bytes, int *files, int *dirs)
{
    uid_t uid = getuid(), euid = geteuid();

    DBG2("purgeDir(%s) as %d(%d)\n", dir, (int)uid, (int)euid);
    int count = 0, _files = 0, _dirs = 0, __files = 0, __dirs = 0;
    lld _bytes = 0, __bytes = 0;
    if (bytes == NULL) {
        bytes = &__bytes;
    }
    if (files == NULL) {
        files = &__files;
    }
    if (dirs == NULL) {
        dirs = &__dirs;
    }
    DIR *d = opendir(dir);
    if (!d && errno == ENOENT) {
        // okay dir does not exist
        return 0;
    }
    // ignore permission denied errors - means directory owned by someone else
    if (!d && errno != EACCES) {
        WARN("Could not opendir %s! %s\n", dir, strerror(errno));
        return count;
    }

    //fprintf(stderr, "Opened dir %s\n", dir);
    struct dirent *result;
    int path_length;
    char path[MAX_FILE_PATH];
    struct stat s;
    while ((result = readdir(d)) != NULL) {
        path_length = snprintf(path, MAX_FILE_PATH,
                               "%s/%s", dir, result->d_name);
        if (path_length >= MAX_FILE_PATH) {
            WARN("Path length has got too long.\n");
            continue;
        }
        stat(path, &s);
        if (s.st_uid != uid && s.st_uid != euid) {
            DBG2("Skipping %s/%s as I do not own it (%d)\n", dir, result->d_name, (int)s.st_uid);
            continue;
        }
        if (S_ISDIR(s.st_mode)) {
            _dirs++;
            if (result->d_name[0] != '.') { // skip '.', '..', and anything starting with '.'
                /* Recursively call purgeDir  */
                count += purgeDir(path, bytes, files, dirs);
            }
        } else if (S_ISREG(s.st_mode) || S_ISLNK(s.st_mode)) {
            DBG("Unlinking %s\n", path);
            _files++;
            if (unlink(path) == 0) {
                _bytes += s.st_size;
                count++;
            } else {
              WARN("Unlink of %s returned an error: %s (%d)\n", path, strerror(errno), errno);
            }
        }
    }
    if (closedir(d) == 0) {
        if (strncmp(dir, "/dev/shm", 8) == 0) {
            DBG("Finished purgeDir %s\n", dir);
        } else {
            DBG("rmdir %s\n", dir);
            if (rmdir(dir) != 0) {
                WARN("Could not rmdir(%s)\n", dir);
            }
        }
    }

    *bytes += _bytes;
    *files += _files;
    *dirs += _dirs;
    DBG("Exiting %s: purged %lld bytes, %d files, %d dirs (total %lld %d %d)\n", dir, (lld)_bytes, (int)_files, (int)_dirs, (lld) * bytes, (int)*files, (int)*dirs);
    return count;
}

int rmFiles(const char *fnames)
{
  glob_t globbuf;
  globbuf.gl_offs = 1;
  glob(fnames, 0, NULL, &globbuf);
  for (int i = 0; i < globbuf.gl_pathc; i++) {
    unlink(globbuf.gl_pathv[i]);
  }
  globfree(&globbuf);
  return 0;
}

int isACGT(const char base)
{
    if (base == 'A' || base == 'C' || base == 'G' || base == 'T') {
        return 1;
    }
    return 0;
}
int isACGTN(const char base)
{
    if (base == 'A' || base == 'C' || base == 'G' || base == 'T' || base == 'N') {
        return 1;
    }
    DBG("isACGTN(%c) is not ACGT or N\n", base);
    return 0;
}

int check_seqs(char *seq, char *label, int64_t *num_ambig)
{
    int i;
    int len = strlen(seq);

    for (i = 0; i < len; i++) {
        char c = toupper(seq[i]);
        switch (c) {
        case 'A': case 'C': case 'G': case 'T': case 'N': case 'U':
            break;
        case 'R': //seq[i] = 'A'; break;
        case 'Y': //seq[i] = 'C'; break;
        case 'K': //seq[i] = 'T'; break;
        case 'M': //seq[i] = 'C'; break;
        case 'S': //seq[i] = 'G'; break;
        case 'W': //seq[i] = 'T'; break;
        case 'B': //seq[i] = 'C'; break;
        case 'D': //seq[i] = 'A'; break;
        case 'H': //seq[i] = 'T'; break;
        case 'V': //seq[i] = 'G'; break;
        case '-':
            // could be IUB/IUPAC coding with ambigous characters
            // if so, convert to an N.
            (*num_ambig)++;
            seq[i] = 'N';
            break;
        default:
            DIE("Invalid base %c %d in sequence (len %d) %.80s\n%s\n", c, (int)c, len, seq, label);
        }
    }
    return len;
}

/*
 * int check_seq_with_len(const char *seq, int len, char *label)
 * {
 *  for (int i = 0; i < len; i++) {
 *      char c = toupper(seq[i]);
 *      if (c != 'A' && c != 'C' && c != 'G' && c != 'T' && c != 'N') {
 *          DIE("Invalid base %c %d in sequence at pos %d (len %d) %.80s\n%s\n", c, c, i, len, seq, label);
 *      }
 *  }
 *  return len;
 * }
 */
char *createSHM(char *my_fname, int64_t fileSize)
{
    // create shm file
    char *ret = NULL;
    int fd = shm_open(my_fname, O_CREAT | O_TRUNC | O_RDWR, S_IRUSR | S_IWUSR);

    if (fd != -1) {
        if (ftruncate(fd, fileSize) == -1) {
            WARN("Failed to truncate to %lld %s: %s\n", (lld)fileSize, my_fname, strerror(errno));
        } else if (fileSize > 0) {
            LOGF("Opened shm file %s for writing of size %lld\n", my_fname, (lld)fileSize);
            void *tmpaddr = mmap(NULL, fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
            if (tmpaddr == MAP_FAILED) {
                WARN("Could not mmap %s for writing: %s, fileSize %lld\n", my_fname, strerror(errno), (lld)fileSize);
            } else {
                ret = (char *)tmpaddr;
            }
        }
        close(fd);
    }
    return ret;
}


size_t printFoldedSequence(GZIP_FILE out, const char *finalSequence, size_t cur_length)
{
    /* Print contig in FASTA FORMAT */
    assert(SEGMENT_LENGTH < 8192);
    size_t total_written = 0, towrite;
    const char *seqF = (char *)finalSequence;
    char fastaSegment[SEGMENT_LENGTH];
    while (total_written < cur_length) {
        if (total_written + SEGMENT_LENGTH - 1 < cur_length) {
            towrite = SEGMENT_LENGTH - 1;
        } else {
            towrite = cur_length - total_written;
        }
        memcpy(fastaSegment, seqF + total_written, towrite * sizeof(char));
        fastaSegment[towrite] = '\0';
        GZIP_PRINTF(out, "%s\n", fastaSegment);
        total_written += towrite;
    }
    assert(total_written == cur_length);
    return total_written;
}

#ifdef __x86_64__
//#include <nmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#include <x86intrin.h>
#endif

// returns the number of mismatches if it is <= max or a number greater than max (but no the actual count)
int16_t fastCountMismatches(const char *a, const char *b, int len, int16_t max) {
    assert(len < 32768);
    int16_t mismatches = 0;
    int16_t jumpSize, jumpLen;

#if defined(__x86_64__) && defined(__POPCNT__)
    // 128-bit SIMD
    if (len >= 16) {
      jumpSize = sizeof(__m128i);
      jumpLen = len/jumpSize;
      for(int16_t i = 0; i < jumpLen ; i++) {
          __m128i aa = _mm_loadu_si128((const __m128i *)a); // load 16 bytes from a
          __m128i bb = _mm_loadu_si128((const __m128i *)b); // load 16 bytes from b
          __m128i matched = _mm_cmpeq_epi8(aa, bb); // bytes that are equal are now 0xFF, not equal are 0x00
          uint32_t myMaskMatched = _mm_movemask_epi8(matched); // mask of most significant bit for each byte
          // count mismatches
          mismatches += _popcnt32( (~myMaskMatched) & 0xffff ); // over 16 bits
          if (mismatches > max) break;
          a += jumpSize;
          b += jumpSize;
      }
      len -= jumpLen * jumpSize;
    }
#endif

    // CPU version and fall through 8 bytes at a time
    if (mismatches <= max) {
        assert(len >= 0);
        jumpSize = sizeof(int64_t);
        jumpLen = len/jumpSize;
        for(int16_t i = 0; i < jumpLen ; i++) {
            int64_t *aa = (int64_t*) a, *bb = (int64_t*) b;
            if (*aa != *bb) { // likely
                for (int j =0; j<jumpSize; j++) {
                    if (a[j] != b[j]) mismatches++;
                }
                if (mismatches > max) break;
            } // else it matched
            a += jumpSize;
            b += jumpSize;
        }
        len -= jumpLen * jumpSize;
    }

    // do the remaining bytes, if needed
    if (mismatches <= max) {
        assert(len >= 0);
        for(int j = 0; j < len; j++) {
            mismatches += ((a[j] == b[j]) ? 0 : 1);
        }
    }
    return mismatches;
}
