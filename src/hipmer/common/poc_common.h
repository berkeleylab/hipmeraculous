#ifndef _POC_COMMON_H_
#define _POC_COMMON_H_

#include "common_base.h"

#if defined (__cplusplus)
extern "C" {
#endif

#ifndef __UPC_VERSION__
#define MYTHREAD get_rank()
#define THREADS get_num_ranks()
#endif

#if defined (__cplusplus)
}
#endif

#endif // _POC_COMMON_H_
