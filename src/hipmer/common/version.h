#pragma once

#ifdef __cplusplus
extern "C" {
#endif

extern const char *HIPMER_VERSION;
extern const char *HIPMER_BUILD_DATE;
extern const char *HIPMER_VERSION_DATE;

#ifdef __cplusplus
}
#endif
