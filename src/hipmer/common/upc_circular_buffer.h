#ifndef UPC_CIRCULAR_BUFFER_H
#define UPC_CIRCULAR_BUFFER_H

#include <stdint.h>
#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "Buffer.h"
#include "timers.h"

#define CB_MAX_ENTRY (1 << 30)

// states
#define CB_HEADER_INVALID (0L)
#define CB_HEADER_EMPTY (1L)
#define CB_HEADER_WRITING (2L)
#define CB_HEADER_FULL (4L)
#define CB_HEADER_READING (8L)

// 64bit struct
typedef struct {
    int32_t head, tail; // always < 1<<30.  Care is taken to keep head and tail <= CB_MAX_ENTRY, tryPush will fail and wait for pop to drain the CB in that case
} Positions;

typedef int64_t Header;

typedef union {
    int64_t   i;
    Positions pos;
} PosUnion;

typedef struct {
    shared [1] PosUnion * posUnions;
    shared [1] SharedCharPtr * buffers;
    shared [1] int32_t * counts;
    int32_t blockSize;
    double  pushTry, pushWait, pushBusyWait, pushSend, popTry, popBusyWait, popSend;
} _CircularBuffer;
typedef _CircularBuffer *CircularBuffer;
typedef void (*BlockingCallback)(void *data);

// constructor -- collective, every thread has a different buffer
CircularBuffer initCircularBuffer(int32_t count, int32_t blockSize);

// destructor
void freeCircularBuffer(CircularBuffer *_cb);

Positions readPosCircularBuffer(CircularBuffer cb, int thread);

int getNumEntriesCircularBuffer(CircularBuffer cb, int thread);

int isEmptyCircularBuffer(CircularBuffer cb, int thread);

int isFullCircularBuffer(CircularBuffer cb, int thread);

// 1 if success, 0 if fail
int tryPushCircularBuffer(CircularBuffer cb, int thread, void *src, BlockingCallback callback, void *callbackData);

// 1 if success, 0 if fail
int tryPopCircularBuffer(CircularBuffer cb, int thread, void *dst, BlockingCallback callback, void *callbackData);

#endif /* UPC_CIRCULAR_BUFFER_H */
