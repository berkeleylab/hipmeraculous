#include <assert.h>
#include "upc_common.h"
#include "upc_distributed_buffer.h"
#include "utils.h"
#include "timers.h"

#include "Buffer.h"

// forward declarations
_DB_ElementPtr DistributedBuffer_getElementPtr(DistributedBuffer db, int idx);
int64_t _DistributedBuffer_getElementIdx(DistributedBuffer db, int64_t globalOffset, _DB_Element *dbe);

// collective functions
DistributedBuffer _DistributedBuffer_initStep1(int64_t myCount, int32_t blockSize)
{
    if (myCount < 0) {
        DIE("Underflow!\n");
    }
    int64_t myOffset = reduce_prefix_long(myCount, UPC_ADD, NULL) - myCount;
    int64_t totalCount = broadcast_long(myOffset + myCount, THREADS - 1);
    DBG("myCount=%lld: myOffset=%lld totalCount=%lld\n", (lld)myCount, (lld)myOffset, (lld)totalCount);
    if (totalCount <= 0) {
        DIE("Invalid totalCount: %lld\n", (lld)totalCount);
    }
    if (myOffset > totalCount) {
        DIE("myOffset %lld > totalCount %lld!\n", (lld)myOffset, (lld)totalCount);
    }
    DistributedBuffer db = (DistributedBuffer)calloc_chk(sizeof(_DB_Handle), 1);
    UPC_ALL_ALLOC_CHK(db->elements, THREADS, sizeof(_DB_Element));
    db->totalCount = totalCount;
    db->totalCapacity = totalCount + THREADS;
    db->fixedCapacity = 0 - (myOffset + 1); // return the negative offset minus 1 for calling function
    db->lastElementIdx = -1;
    db->blockSize = blockSize;
    return db;
}

// initializes a DistributedBuffer with fixed allocation/capacity on every THREAD
// calculates totalCapacity, totalCount, and elements
// initializes all elements with
//        localCapacity = (totalCapcity+THREADS-1) / THREADS + 1
//        localCount <= (totalCount + THREADS - 1) / THREADS
//        globalOffset = prefixSum(localCount)
//        threadCount = myCount used to initialize
//        threadOffset = prefixSum(myCount)
DistributedBuffer DistributedBuffer_initFixed(int64_t myCount, int32_t blockSize)
{
    DistributedBuffer db = _DistributedBuffer_initStep1(myCount, blockSize);

    if (db->totalCount <= 0) {
        DIE("Underflow totalCount!\n");
    }
    if (db->totalCapacity <= 0) {
        DIE("Underflow totalCapacity!\n");
    }
    int64_t myOffset = -1 - db->fixedCapacity;
    db->fixedCapacity = (db->totalCount + THREADS - 1) / THREADS;

    assert(upc_threadof(&(db->elements[MYTHREAD])) == MYTHREAD);
    _DB_ElementPtr dbe = (_DB_ElementPtr) & (db->elements[MYTHREAD]);
    LOGF("DB_initFixed(myCount=%lld, blockSize=%d): allocating local buffer of %lld capacity: %0.3f MB\n", (lld) myCount, blockSize, (lld) db->fixedCapacity, 1.0/ONE_MB*(db->fixedCapacity+1)*blockSize);
    UPC_ALLOC_CHK(dbe->buffer, (db->fixedCapacity + 1) * blockSize);
    assert(db->elements[MYTHREAD].buffer != NULL);
    dbe->localCapacity = db->fixedCapacity;
    dbe->localCount = (db->totalCount + THREADS - 1) / THREADS;
    dbe->globalOffset = dbe->localCount * MYTHREAD;
    dbe->threadCount = myCount;
    dbe->threadOffset = myOffset;
    if (dbe->globalOffset > db->totalCount) {
        dbe->globalOffset = db->totalCount;
    }
    if (dbe->globalOffset + dbe->localCount > db->totalCount) {
        dbe->localCount = db->totalCount - dbe->globalOffset;
        if (dbe->localCount < 0) {
            dbe->localCount = 0;
        }
    }
    if (dbe->localCount > dbe->localCapacity) {
        DIE("localcount %ll > localcapacity %lld!\n", (lld)dbe->localCount, (lld)dbe->localCapacity);
    }
    db->lastElementIdx = MYTHREAD;
    db->lastElement = *dbe;
    LOGF("created fixed DistributedBuffer(blockSize=%d, totalCount=%lld, threadOffset=%lld, localCount=%lld, globalOffset=%lld) %0.3f MB perThread, %0.3f MB Total\n", db->blockSize, (lld)db->totalCount, (lld)dbe->threadOffset, (lld)dbe->localCount, (lld)dbe->globalOffset, 1.0 * db->blockSize * dbe->localCount / ONE_MB, 1.0 * db->blockSize * db->totalCount / ONE_MB);
    UPC_LOGGED_BARRIER;
    return db;
}

DistributedBuffer DistributedBuffer_initVariable(int64_t myCount, int32_t blockSize)
{
    DistributedBuffer db = _DistributedBuffer_initStep1(myCount, blockSize);

    if (db->totalCount <= 0) {
        DIE("Undeflow totalCount!\n");
    }
    if (db->totalCapacity <= 0) {
        DIE("Underflow totalCapacity!\n");
    }
    int64_t myOffset = -1 - db->fixedCapacity;
    // keep fixedCapacity < 0

    assert(upc_threadof(&(db->elements[MYTHREAD])) == MYTHREAD);
    _DB_ElementPtr dbe = (_DB_ElementPtr) & (db->elements[MYTHREAD]);
    dbe->localCapacity = myCount + 1;
    UPC_ALLOC_CHK(dbe->buffer, dbe->localCapacity * blockSize);
    assert(db->elements[MYTHREAD].buffer != NULL);
    dbe->localCount = myCount;
    dbe->globalOffset = myOffset;
    dbe->threadCount = myCount;
    dbe->threadOffset = myOffset;
    db->lastElementIdx = MYTHREAD;
    db->lastElement = *dbe;
    if (dbe->localCount > dbe->localCapacity) {
        DIE("localCount %lld > localCapcity %lld!\n", (lld)dbe->localCount, (lld)dbe->localCapacity);
    }
    LOGF("created variable DistributedBuffer(blockSize=%d, totalCount=%lld, threadOffset=%lld, localCount=%lld, globalOffset=%lld) %0.3f MB perThread, %0.3f MB Total\n", db->blockSize, (lld)db->totalCount, (lld)dbe->threadOffset, (lld)dbe->localCount, (lld)dbe->globalOffset, 1.0 * dbe->localCount * db->blockSize / ONE_MB, 1.0 * db->totalCount * db->blockSize / ONE_MB);
    UPC_LOGGED_BARRIER;
    return db;
}

// releases all memory
void DistributedBuffer_free(DistributedBuffer *_db)
{
    assert(_db);
    DistributedBuffer db = *_db;
    if (!db) {
        WARN("DistributedBuffer_free() called on NULL DistributedBuffer!\n"); return;
    }
    assert(upc_threadof(&(db->elements[MYTHREAD])) == MYTHREAD);
    _DB_ElementPtr dbe = (_DB_ElementPtr) & (db->elements[MYTHREAD]);
    UPC_LOGGED_BARRIER;
    UPC_FREE_CHK(dbe->buffer);
    UPC_ALL_FREE_CHK(db->elements);
    db->totalCapacity = db->totalCount = 0;
    free_chk(db);
    *_db = NULL;
}

// returns the totalCount of the DistributedBuffer
int64_t DistributedBuffer_getTotalCount(DistributedBuffer db)
{
    return db->totalCount;
}

// returns the requested element.  checks and updates cache as necesary
_DB_ElementPtr DistributedBuffer_getElementPtr(DistributedBuffer db, int idx)
{
    if (db->lastElementIdx != idx) {
        db->lastElementIdx = idx;
        db->lastElement = db->elements[idx];
    }
    return &(db->lastElement);
}

// returns the position of the first byte local to this idx
int64_t DistributedBuffer_getLocalGlobalOffset(DistributedBuffer db, int idx)
{
    return DistributedBuffer_getElementPtr(db, idx)->globalOffset;
}

int64_t DistributedBuffer_getLocalCount(DistributedBuffer db, int idx)
{
    return DistributedBuffer_getElementPtr(db, idx)->localCount;
}

int64_t DistributedBuffer_getThreadCount(DistributedBuffer db, int idx)
{
    return DistributedBuffer_getElementPtr(db, idx)->threadCount;
}

// returns the position of the first byte allocated for this idx to populate
int64_t DistributedBuffer_getThreadGlobalOffset(DistributedBuffer db, int idx)
{
    return DistributedBuffer_getElementPtr(db, idx)->threadOffset;
}

// non-collective functions

// writes the entire contents of Buffer at the globalOffset, accounting for element edges -- may call itself recursively
int64_t DistributedBuffer_writeBufferAt(DistributedBuffer db, Buffer b, int64_t globalOffset)
{
    int64_t bytesWritten = 0, bytesToWrite = getReadLengthBuffer(b);

    if (db->blockSize > 1 && bytesToWrite % db->blockSize != 0) {
        DIE("Invalid write of %lld length\n", (lld)bytesToWrite);
    }
    _DB_Element dbe;
    int64_t elementIdx = _DistributedBuffer_getElementIdx(db, globalOffset, &dbe);
    int64_t readLenBytes = 0;
    while ((readLenBytes = getReadLengthBuffer(b)) > 0) {
        if (elementIdx >= THREADS) {
            DIE("Overflow. element %lld can not be THREADS %lld (readLenBytes=%lld bytesWritten=%lld globalOffset=%lld totalCount=%lld)\n", (lld)elementIdx, (lld)THREADS, (lld)readLenBytes, (lld)bytesWritten, (lld)globalOffset, (lld)db->totalCount);
        }
        if (!((elementIdx >= 0) & (elementIdx < THREADS))) {
            DIE("elementIdx=%lld is invalid!\n", (lld)elementIdx);
        }
        dbe = *(DistributedBuffer_getElementPtr(db, elementIdx));
        if (!(globalOffset >= dbe.globalOffset)) {
            DIE("globalOffset=%lld < dbe.globalOffset=%lld elementIdx=%lld\n", (lld)globalOffset, (lld)dbe.globalOffset, (lld)elementIdx);
        }
        // write the lesser of the available bytes to read in Buffer or the available bytes to write in the element buffer
        int64_t localOffset = globalOffset - dbe.globalOffset;
        //DBG2("localOffset=%lld globalOffset=%lld dbe.globalOffset=%lld dbe.localCount=%lld elementIdx=%lld, readLenBytes=%lld\n", localOffset, globalOffset, dbe.globalOffset, dbe.localCount, elementIdx, readLenBytes);
        if (!((localOffset >= 0) & (localOffset <= dbe.localCount))) {
            DIE("localOffset=%lld dbe.localCount=%lld elementIdx=%lld\n", (lld)localOffset, (lld)dbe.localCount, (lld)elementIdx);
        }
        int64_t maxWriteBytes = (dbe.localCount - localOffset) * db->blockSize;
        if (maxWriteBytes == 0) {
            elementIdx++; continue;
        }
        if (maxWriteBytes < 0) {
            DIE("maxWriteBytes=%lld elenentIdx=%lld dbe.localCount=%lld localOffset=%lld\n", (lld)maxWriteBytes, (lld)elementIdx, (lld)dbe.localCount, (lld)localOffset);
        }
        if (readLenBytes > maxWriteBytes) {
            readLenBytes = maxWriteBytes;
        }
        assert(dbe.buffer != NULL);
        //DBG2("Reading %lld from Buffer pos=%lld len=%lld size=%lld (localOffset=%lld byteOffset=%lld)\n", readLenBytes, b->pos, b->len, b->size, (lld) localOffset, (lld) localOffset * db->blockSize);
        upc_memput(dbe.buffer + (localOffset * db->blockSize), getCurBuffer(b), readLenBytes);

        // adjust counters
        assert(isValidBuffer(b));
        readBuffer(b, NULL, readLenBytes);
        bytesWritten += readLenBytes;
        globalOffset += readLenBytes / db->blockSize;
        elementIdx++; // go to the next element
    }
    if (bytesToWrite != bytesWritten) {
        DIE("Ack!\n");
    }
    assert((bytesWritten % db->blockSize) == 0);
    return bytesWritten / db->blockSize;
}
int64_t DistributedBuffer_writeAt(DistributedBuffer db, int64_t globalOffset, void *src, int64_t writeCount)
{
    if (writeCount == 0) {
        return 0;
    }
    assert(src != NULL);
    assert(writeCount > 0);
    BufferBase b;
    memset(&b, 0, sizeof(BufferBase));
    int64_t writeLen = writeCount * db->blockSize;
    attachBuffer(&b, (char *)src, 0, writeLen, writeLen);
    return DistributedBuffer_writeBufferAt(db, &b, globalOffset);
}

// reads the readLen bytes into Buffer starting at globalOffset, accounting for element edges -- may call itself recursively
int64_t DistributedBuffer_readBufferAt(DistributedBuffer db, Buffer b, int64_t globalOffset, int64_t readCount)
{
    int64_t bytesRead = 0, bytesToRead = readCount * db->blockSize;
    int64_t readLenBytes = bytesToRead;

    if (db->blockSize > 1 && readLenBytes % db->blockSize != 0) {
        DIE("Invalid read of %lld bytes\n", (lld)readLenBytes);
    }
    _DB_Element dbe;
    int64_t elementIdx = _DistributedBuffer_getElementIdx(db, globalOffset, &dbe);
    int64_t availBytes = growBuffer(b, readLenBytes + 1);
    assert(availBytes >= readLenBytes);
    while (readLenBytes > 0) {
        assert(elementIdx >= 0);
        assert(elementIdx < THREADS);
        dbe = *(DistributedBuffer_getElementPtr(db, elementIdx));
        assert(globalOffset >= dbe.globalOffset);
        // calculate the lesser of readLenBytes and the bytes remainingin element buffer
        int64_t localOffset = globalOffset - dbe.globalOffset;
        int64_t maxReadBytes = (dbe.localCount - localOffset) * db->blockSize;
        assert(maxReadBytes >= 0);
        int64_t putBytes = readLenBytes > maxReadBytes ? maxReadBytes : readLenBytes;
        assert(dbe.buffer != NULL);
        upc_memget(appendBuffer(b, putBytes), dbe.buffer + (localOffset * db->blockSize), putBytes);
        appendNullBuffer(b); // just ensure no string overruns happen

        // adjust counters
        bytesRead += putBytes;
        globalOffset += putBytes;
        readLenBytes -= putBytes;
        assert(readLenBytes >= 0);
        elementIdx++; // go to the next element
    }
    if (bytesToRead != bytesRead) {
        DIE("Ack!\n");
    }
    assert((bytesRead % db->blockSize) == 0);
    return bytesRead / db->blockSize;
}

SharedCharPtr DistributedBuffer_getSharedPtr(DistributedBuffer db, int64_t globalOffset)
{
    _DB_Element dbe;

    assert(globalOffset >= 0);
    if (globalOffset >= db->totalCount) {
        DIE("Invalid globalOffset=%lld totalCount=%lld\n", (lld)globalOffset, (lld)db->totalCount);
    }
    int64_t elementIdx = _DistributedBuffer_getElementIdx(db, globalOffset, &dbe);
    int64_t localOffset = globalOffset - dbe.globalOffset;
    //DBG2("getSharedPtr %lld: localOffset=%lld elementIdx=%lld byteOffset=%lld\n", globalOffset, localOffset, elementIdx, localOffset * db->blockSize);
    return dbe.buffer + (localOffset * db->blockSize);
}

void *DistributedBuffer_getLocalStartPtr(DistributedBuffer db)
{
    int64_t globalOffset = DistributedBuffer_getLocalGlobalOffset(db, MYTHREAD);
    SharedCharPtr scp = DistributedBuffer_getSharedPtr(db, globalOffset);

    if (upc_threadof(scp) != MYTHREAD) {
        DIE("Invalid distributed buffer state!\n");
    }
    return (char *)scp;
}

// internal functions
// binary search elements for the one that contains globalOffset, first checking local element(s)
int64_t _DistributedBuffer_getElementIdx(DistributedBuffer db, int64_t globalOffset, _DB_Element *dbe)
{
    //DBG2("Finding element that contains %lld\n", (lld) globalOffset);
    _DB_Element _dbe;

    if (dbe == NULL) {
        dbe = &_dbe;
    }

    // prep the cache if it is not ready
    if (db->lastElementIdx < 0) {
        // There is no cache, so start with the local data
        *dbe = *(DistributedBuffer_getElementPtr(db, MYTHREAD));
    } else {
        *dbe = db->lastElement;
    }
    int64_t min = 0, max = THREADS - 1, mid = db->lastElementIdx;

    // first check the cached element
    //DBG2("Examining offset=%lld: mid=%lld globalOffset=%lld localCount=%lld maxOffset=%lld\n", (lld) globalOffset, (lld) mid, (lld) dbe->globalOffset, (lld) dbe->localCount, (lld) dbe->globalOffset + dbe->localCount - 1);
    if (globalOffset < dbe->globalOffset) {
        if (mid == 0) {
            DIE("Impossible");
        }
        max = mid - 1;
    } else if (globalOffset >= dbe->globalOffset) {
        if (globalOffset < dbe->globalOffset + dbe->localCount) {
            //DBG2("Found element at %lld\n", (lld) mid);
            return mid;
        } else {
            min = mid + 1;
        }
    }

    if (db->fixedCapacity > 0) {
        // No need for binary search
        mid = globalOffset / db->fixedCapacity;
        *dbe = *(DistributedBuffer_getElementPtr(db, mid));
        //DBG2("Jumped to offset=%lld: mid=%lld globalOffset=%lld localCount=%lld maxOffset=%lld fixedCapacity=%lld\n", (lld) globalOffset, (lld) mid, (lld) dbe->globalOffset, (lld) dbe->localCount, (lld) dbe->globalOffset + dbe->localCount - 1, (lld) db->fixedCapacity);
        assert(globalOffset >= dbe->globalOffset);
        if (dbe->localCount > 0) {
            assert(globalOffset < dbe->globalOffset + dbe->localCount);
        }
        return mid;
    }

    assert(max >= 0);
    assert(min >= 0);
    assert(max < THREADS);
    assert(min < THREADS);
    assert(max - min <= THREADS);
    while (min <= max) {
        //DBG2("min=%lld max=%lld\n", (lld) min, (lld) max);
        int64_t mid = (min + max) / 2;
        *dbe = *(DistributedBuffer_getElementPtr(db, mid));
        if (globalOffset >= dbe->globalOffset && globalOffset < dbe->globalOffset + dbe->localCount) {
            //DBG2("Found offset=%lld: mid=%lld globalOffst=%lld localCount=%lld maxOffset=%lld\n", (lld) globalOffset, (lld) mid, (lld) dbe->globalOffset, (lld) dbe->localCount, (lld) dbe->globalOffset + dbe->localCount - 1);
            // update the cache
            return mid;
        } else if (globalOffset < dbe->globalOffset) {
            max = mid - 1;
        } else {
            min = mid + 1;
        }
    }

    DIE("Failed to find %lld within 0 to %lld... perhaps not all globalOffsets and localCount are accurate?\n", (lld)globalOffset, (lld)db->totalCount);
    return -1;
}
