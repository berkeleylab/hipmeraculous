#include <stdint.h>
#include <upc.h>

#include "hash_funcs.h"
#include "upc_atomic_data.h"
#include "StaticVars.h"
#include "common.h"
#include "log.h"

uint16_t getVersionAtomicData(uint64_t largeNumber) {
    return (largeNumber & UPCAD_MAX_VERSION);
}

void initAtomicData(AtomicData *atomicData) {
    memset(atomicData, 0, sizeof(AtomicData));
    atomicData->checksum.csv.payloadSize = sizeof(atomicData->payload);
}

uint64_t hashAtomicData(const AtomicData *atomicData)
{
    return MurmurHash3_x64_64(&(atomicData->payload), atomicData->checksum.csv.payloadSize) & UPCAD_MAX_HASH;
}

int isEmptyAtomicData(const AtomicData *atomicData) {
    CheckSizeVersion csv = atomicData->checksum.csv;
    return ( (csv.hash == 0) & (csv.version == 0) & ( (csv.payloadSize == 0) | (csv.payloadSize == sizeof(atomicData->payload)) ) ); 
}

int isValidAtomicData(const AtomicData *atomicData) {
    AtomicDataChecksum original = atomicData->checksum;
    return (original.csv.inTransit == 0)
           & (original.csv.payloadSize > 0)
           & (hashAtomicData(atomicData) == original.csv.hash);
}

int isValidAtomicDataVersion(const AtomicData *atomicData, uint64_t version) {
    return (atomicData->checksum.csv.version == getVersionAtomicData(version))
           & isValidAtomicData(atomicData);
}

void setChecksumAtomicData(AtomicData *atomicData, uint16_t payloadSize, uint64_t version, const char *filename, int line)
{
    if (payloadSize > UPCAD_MAX_PAYLOAD_SIZE) {
        DIE("[%s:%d] Can not use UPCAtomicData with a payload larger than %lld (attempted %lld)\n", filename, line, (lld) UPCAD_MAX_PAYLOAD_SIZE, (lld) payloadSize);
    }
    if (sizeof(AtomicDataChecksum) != sizeof(int64_t)) {
        DIE("[%s:%d] Invalid AtomicDataChecksum struct / bitfield packing: size is %d not 8 bytes (64-bit)!\n", filename, line, sizeof(AtomicDataChecksum));
    }
    AtomicDataChecksum checksum;
    checksum.csv.hash = hashAtomicData(atomicData);
    checksum.csv.inTransit = 0;
    checksum.csv.version = getVersionAtomicData(version);
    checksum.csv.payloadSize = payloadSize;
    atomicData->checksum = checksum;
}

void setAtomicData(AtomicData *atomicData, void *data, uint16_t payloadSize, uint64_t version, const char * filename, int line)
{
    memcpy(&(atomicData->payload), data, payloadSize);
    setChecksumAtomicData(atomicData, payloadSize, version, filename, line);
}

AtomicData *newAtomicData(void *data, uint16_t payloadSize, uint64_t version, const char * filename, int line)
{
    AtomicData *ad = NULL;
    int allocSize = sizeof(AtomicDataChecksum) + payloadSize;

    ad = (AtomicData *)calloc_chk(1, allocSize);
    setAtomicData(ad, data, payloadSize, version, filename, line);
    return ad;
}

void freeAtomicData(AtomicData **localCopy)
{
    free_chk(*localCopy);
}

// attempts to get the remote data to local memory
// localDest->csv.payloadSize must already be populated, and will throw an error if remoteSource->csv.payloadSize is different!
// returns dest on success, NULL on failure
AtomicData * tryGetAtomicData(AtomicData * localDest, ConstSharedAtomicDataPtr remoteSource, const char * filename, int line)
{
    if (localDest == NULL || remoteSource == NULL) {
        DIE("[%s:%d] tryGetAtomicData called with null localDest or remoteSource", filename, line);
    }
    AtomicDataChecksum originalChecksum = localDest->checksum;
    uint16_t payloadSize = originalChecksum.csv.payloadSize;
    void *data = &(localDest->payload);

    upc_memget(localDest, remoteSource, sizeof(AtomicDataChecksum) + payloadSize);
    if (localDest->checksum.csv.inTransit || localDest->checksum.csv.payloadSize == 0) {
        // AtomicData payload may not be ready
        // reset localDest to originalChecksum
        localDest->checksum = originalChecksum;
        return NULL;
    }
    if (localDest->checksum.csv.payloadSize != payloadSize) {
        // this should never happen but Issue #211 .. do an atomic get before a die to verify the data
        AtomicDataChecksum realChecksum;
        UPC_ATOMIC_GET_I64(&(realChecksum.val), (shared void *) &(remoteSource->checksum.val));
        DIE("[%s:%d] NOTICE: tryGetAtomcData expected to get a payload of %d bytes but remoteSource had a payload of %d (hash: %lld, ver: %d, inTransit: %d, payloadSize: %d) - live(hash: %lld, ver: %d, inTransit: %d, payloadSize: %d)\n", filename, line, payloadSize, localDest->checksum.csv.payloadSize, (lld) localDest->checksum.csv.hash, localDest->checksum.csv.version, localDest->checksum.csv.inTransit, localDest->checksum.csv.payloadSize, (lld) realChecksum.csv.hash, realChecksum.csv.version, realChecksum.csv.inTransit, realChecksum.csv.payloadSize);
        return NULL;
    }

    if (isValidAtomicData(localDest)) {
        // passed
        return localDest;
    } else {
        // checksum or other fields failed
        // reset localDest to originalChecksum
        localDest->checksum = originalChecksum;
        return NULL;
    }
}


// read until checksum matches the data
void getAtomicData(AtomicData * localDest, ConstSharedAtomicDataPtr remoteSource, const char * filename, int line)
{
    AtomicData *test = NULL;
    do {
        test = tryGetAtomicData(localDest, remoteSource, filename, line);
        if (test != NULL) {
            break;
        }
        UPC_POLL;
    } while (1);
}

// sends localSource to remoteDest.  If optionalCSWAP is not NULL, performs an atomic CSWAP to avoid clobbering another value
static int _tryPutAtomicData(SharedAtomicDataPtr remoteDest, const AtomicData * localSource, AtomicDataChecksum oldVal, int isAlwaysOnNode, const char * filename, int line)
{
    AtomicDataChecksum orig = localSource->checksum;
    if (!isValidAtomicData(localSource)) {
        CheckSizeVersion csv = orig.csv;
        DIE("[%s:%d] tryPutAtomicData called with an invalid localSource: hash=%d actual=%d payloadSize=%d version=%d\n", filename, line,
            csv.hash, hashAtomicData(localSource), csv.payloadSize, csv.version);
    }

    AtomicDataChecksum newVal = localSource->checksum;
    AtomicDataChecksum inTransit = newVal;
    inTransit.csv.inTransit = 1;

    // only set if remoteDest checksum == optionalCSWAP
    AtomicDataChecksum test;
    if (isAlwaysOnNode) {
        UPC_ATOMIC_CSWAP_I64_NEAR(&test.val, &(remoteDest->checksum.val), oldVal.val, inTransit.val);
    } else {
        UPC_ATOMIC_CSWAP_I64(&test.val, &(remoteDest->checksum.val), oldVal.val, inTransit.val);
    }
    if (test.val != oldVal.val) {
        // lost the cswap
        return 0;
    }

    // now deliver the data payload
    upc_memput( &(remoteDest->payload), &(localSource->payload), orig.csv.payloadSize );
    upc_fence;

    // now signal the data is ready
    if (isAlwaysOnNode) {
        UPC_ATOMIC_CSWAP_I64_NEAR(&test.val, &(remoteDest->checksum.val), inTransit.val, newVal.val);
    } else {
        UPC_ATOMIC_CSWAP_I64(&test.val, &(remoteDest->checksum.val), inTransit.val, newVal.val);
    }
    if (test.val != inTransit.val) {
        DIE("[%s:%d] A race here should be impossible\n", filename, line);
    }

    return 1;
}

int tryPutAtomicData(SharedAtomicDataPtr remoteDest, const AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line) {
    return _tryPutAtomicData(remoteDest, localSource, oldVal, 0, filename, line);
}
int tryPutAtomicData_Near(SharedAtomicDataPtr remoteDest, const AtomicData * localSource, AtomicDataChecksum oldVal, const char * filename, int line) {
    return _tryPutAtomicData(remoteDest, localSource, oldVal, 1, filename, line);
}
