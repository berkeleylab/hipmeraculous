#ifndef _UPC_OUTPUT_H_
#define _UPC_OUTPUT_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <upc.h>
#include <upc_collective.h>
#include <stdio.h>
#include <stdint.h>

#ifdef WRITE_WITH_MMAP
#  include <unistd.h>
#  ifndef MMAP_PAGESISE
#    define MMAP_PAGESIZE sysconf(_SC_PAGESIZE);
#  endif
#endif

#ifndef HIPMER_NO_AIO
#  include <aio.h>
#endif

#include "defines.h"
#include "upc_common.h"
#include "upc_compatibility.h"
#include "common.h"
#include "Buffer.h"

#define WRITE_BLOCK_SIZE (1LL * ONE_MB)
#define MINIMUM_WRITE_BLOCK (16LL * ONE_MB)
#define READ_BLOCK_SIZE (16LL * ONE_MB)

typedef struct {
    uint8_t buf[ READ_BLOCK_SIZE ];
} _ReadBuffer;
typedef _ReadBuffer ReadBuffer;
typedef shared [1] ReadBuffer *SharedReadBuffer;

typedef shared[] int8_t *shared_buf;

typedef struct {
    Buffer       b;
    int          fd;
    char         ourFileName[MAX_FILE_PATH];
    shared[1] shared_buf * ourBuffers;
    shared[1] int32_t * finishedCount;
    double       start;
    size_t       mySize;
    size_t       totalSize;
#ifndef HIPMER_NO_AIO
    struct aiocb _aio;
#endif
} __ioptr;
typedef __ioptr *ioptr;

typedef struct {
    int64_t offset, size, uncompressedSize;
} _IndexElement;
typedef _IndexElement IndexElement;

// an unfortunate hack around NERSC INC0144398
void clear_lustre_caches();

// write an index file
void writeIndexFile(const char *ourFileName, int64_t myOrigOffset, int64_t myOrigSize, int64_t myUncompressedSize);

// collective call that re-orgianizes data in all threads myBuffers to be
// both > MINIMUM_WRITE_BLOCK and aligned to WRITE_BLOCK_SIZE
// some threads may have empty myBuffers after this call
// returns the original global offset
int64_t consolidateSmallBuffers(Buffer myBuffer, shared [1] int64_t *ourSizes, int targetSize);

// starts all threads writing to ourFileName the contents of private _myBuffer
// optionally writes an index file with an int64_t offset for each thread
ioptr start_allWriteFile(const char *ourFileName, const char *mode, Buffer myBuffer, int writeIndexFile);

// tests operation, closes file if complete
char test_aio(ioptr _ioptr);

// blocks until test_aio is true, then cleans datastructures
int64_t finish_aio(ioptr _ioptr);

// calls start_allWriteFile and finish_aio.
int64_t allWriteFile(const char *ourFileName, const char *mode, Buffer myBuffer, int writeIndexFile);

#include "rb_tree.h"
int rb_tree_cmp_checkpoint(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b);
void rb_tree_delete_checkpoint(struct rb_tree *self, struct rb_node *node);

typedef struct {
    char name[MAX_FILE_PATH];
    GZIP_FILE local; // access as gzipped
    ioptr inTransit;
    int mode; // 0 - reading, 1 - writing, 3 - was writing, now reading, sync on next check.... append is not supported 
    IndexElement myidx;
} _CheckpointFile;
typedef _CheckpointFile *CheckpointFile;

// translates checkpoint name to local and global file paths
void checkpointToLocal(const char *name, char *localfilename);
void checkpointToGlobal(const char *name, char *globalfilename);
#define doesCheckpointExist(n) _doesCheckpointExist(n, __FILENAME__, __LINE__)
int _doesCheckpointExist(const char *name, const char *filename, int line);
#define doesLocalCheckpointExist(n) _doesLocalCheckpointExist(n, __FILENAME__, __LINE__)
int _doesLocalCheckpointExist(const char *name, const char *filename, int line);
#define doesGlobalCheckpointExist(n) _doesGlobalCheckpointExist(n, __FILENAME__, __LINE__)
int _doesGlobalCheckpointExist(const char *name, const char *filename, int line);

// checkpoint routines are collective, and access a tree stored in StaticVars

// when writing, opens the local file
// when reading, opens the local file, if it does not exist, recreates it from the global file
#define openCheckpoint(n,m) openCheckpoint_at_file_line(n,m, __FILENAME__, __LINE__)
GZIP_FILE openCheckpoint_at_file_line(const char *name, char *mode, const char *filename, int line);
#define openCheckpoint1(n,m) openCheckpoint1_at_file_line(n,m, __FILENAME__, __LINE__)
gzFile openCheckpoint1_at_file_line(const char *name, char *mode, const char *filename, int line);
#define openCheckpoint0(n,m) openCheckpoint0_at_file_line(n,m, __FILENAME__, __LINE__)
FILE * openCheckpoint0_at_file_line(const char *name, char *mode, const char *filename, int line);

// when writing, closes the local file and consolidates to the global file in the background (re-reads local file)
// when reading, closes the local file
#define closeCheckpoint(fh) _closeCheckpoint(fh, __FILENAME__, __LINE__)
void _closeCheckpoint(GZIP_FILE fh, const char *filename, int line);
#define closeCheckpoint1(fh) _closeCheckpoint1(fh, __FILENAME__, __LINE__)
void _closeCheckpoint1(gzFile fh, const char *filename, int line);
#define closeCheckpoint0(fh) _closeCheckpoint0(fh, __FILENAME__, __LINE__)
void _closeCheckpoint0(FILE *, const char *filename, int line);
#define closeCheckpoint2(name) _closeCheckpoint2(name, __FILENAME__, __LINE__)
void _closeCheckpoint2(const char *name, const char *filename, int line);

// initiates a global->local or local->global restore
void restoreCheckpoint(const char *name);

// initiates a local -> global save
void saveGlobalCheckpoint(const char *name);
void saveGlobalCheckpoint2(CheckpointFile ckpt);

// forces a global -> local restore and blocks
void restoreLocalCheckpoint(const char *name);

// checks inTransit and possibly closes global.  Returns 1 if all threads are finished with io
// changes mode to 0 if all are done writing
int checkCheckpointState(CheckpointFile ckpt);

// finishes any writing IO on the global checkpoint, blocks until all threads complete
void syncCheckpoint(const char *name);
void syncCheckpoint2(CheckpointFile ckpt);

// finds all the potential local checkpoints and saves and syncs them to global
// used for run recovery
void syncMissingGlobalCheckpoints();

// calls syncCheckpoint and unlinks the local file
void unlinkLocalCheckpoint(const char *name);

// unlinks global checkpoint and cancels any pending I/O
void unlinkGlobalCheckpoint(const char *name);

// unlinks all global and local checkpoints - cancels any pending I/O 
// removes checkpoint from the tree
void unlinkCheckpoint(const char *name);

// just syncs and removes from the tree
void deleteCheckpoint(const char *name);
void deleteCheckpoint2(CheckpointFile ckpt);

// sync any active IO and prune the tree
void cleanupCheckpoints(int forceSync);

// links two checkpoints, with hard links to global, idx and local files (whichever exist)
// returns 1 on success 0 otherwise
int linkCheckpoints(const char *oldName, const char *newName);

// store a signal to not create an aggregate file even when saveAggregatedFile is called
int isNoAggregate(const char *ourFile, int isGlobal);
void setNoAggregate(const char *ourFile);

// Starts the generation of an aggregated file from an exising per-thread file
ioptr saveAggregatedFile(const char *myfile, const char *ourFile, IndexElement *optIdx);
void saveAggregatedFileSync(const char *myfile, const char *ourFile, IndexElement *optIdx);

// restores a per-thread file from an existing aggregated file
void restoreAggregatedFile(const char *myfile, const char *ourFile);

typedef struct {
    shared [1] int64_t   * mergedOffset;
    char                   ourFileName[MAX_FILE_PATH];
    int                    fd;
    Buffer                 buffering, inFlight;
#ifndef HIPMER_NO_AIO
    struct aiocb       *_aio;
#endif
} _AppendFile;
typedef _AppendFile *AppendFile;

AppendFile openAppendFile(const char *ourFileName);
void closeAppendFile(AppendFile *file);
void flushInFlightAppendFile(AppendFile file);
void flushAppendFile(AppendFile file);
#define printfAppendFile(file, fmt_and_args...) do { \
    size_t paf_len = printfBuffer(file->buffering, fmt_and_args); \
    if (getLengthBuffer(file->buffering) + 3*paf_len >= MINIMUM_WRITE_BLOCK) { \
        flushAppendFile(file); \
        assert(getLengthBuffer(file->buffering) == 0); \
    } \
  } while (0)

#endif
