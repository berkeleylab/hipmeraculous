#ifndef __LOG_H
#define __LOG_H

#include <stdint.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>


#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

// INCLUDE other common header files too

#include "version.h"
#include "defines.h"
#include "common_base.h"
#include "colors.h"
#include "StaticVars.h"
#include "Buffer.h"
#include "file.h"

#if defined (__cplusplus)
extern "C" {
#endif

/* set up LOG, SLOG, DBG, DBG2, DIE, SDIE, WARN, etc macros */

/* verbosity levels: */
typedef enum _LOG_LEVELS {
    LL_DIE = 0,              /*  FATAL/ERROR/DIE write to file and close file and print to stderr and flush and exit program */
    LL_WARN,                 /*   WARN to file and print to stderr and flush */
    LL_LOG,                  /*   LOG/INFO to file and print to stderr */
    LL_LOGF,                 /*   LOG/INFO just log to file */
    LL_DBG,                  /*   DEBUG1 to file only */
    LL_DBG2,                 /*   DEBUG2 to file only */
    LL_DBG3,                 /*   DEBUG3 to file only */
    LL_MAX
} _LOG_LEVELS_TYPE;
/*
 * SLOG   only !MYTHREAD outputs ... all log if there is a file
 * SDIE   only !MYTHREAD outputs ... all terminate
 * SWARN  only !MYTHREAD outputs ... all log if there is a file
 *
 * everything >= HIPMER_VERBOSITY gets either printed to stderr or writen to _my_log or both
 * everything <= 1 gets printed and flushed to stderr
 */

#ifdef HIPMER_VERBOSE
#define DEFAULT_HIPMER_VERBOSITY (HIPMER_VERBOSE + LL_WARN)
#else
#ifndef DEBUG
#define DEFAULT_HIPMER_VERBOSITY LL_LOGF
#else
#define DEFAULT_HIPMER_VERBOSITY LL_DBG
#endif
#endif

#define HIPMER_VERBOSITY ((_sv) ? MYSV.logVerbosity : DEFAULT_HIPMER_VERBOSITY)

#define SET_HIPMER_VERBOSITY(level) do { \
    if (_sv) { \
        MYSV.logVerbosity = level; \
    }; \
} while(0)

#define MAX_LOG_PREFIX 192
#define NO_LOG_PREFIX 256

int setLogPrefix(char *_LOG_prefix, int size, _LOG_LEVELS_TYPE level, int _printPrefix, time_t _LOG_ltime, const char *fmt, const char *filename, int line);
void _LOG_at_file_line(const char *FILENAME, int LINE, int level, const char *fmt, ...);

#define _LOG(level, fmt, ...) if (HIPMER_VERBOSITY >= ((NO_LOG_PREFIX - 1) & (level))) { _LOG_at_file_line(__FILENAME__, __LINE__, (level), fmt, ## __VA_ARGS__); }

#define LOG(fmt, ...)  _LOG(LL_LOG, fmt, ## __VA_ARGS__)
#define LOGN(fmt, ...)  _LOG((LL_LOG | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define LOGF(fmt, ...) _LOG(LL_LOGF, fmt, ## __VA_ARGS__)
#define LOGFN(fmt, ...) _LOG((LL_LOGF | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define TLOGF(test, fmt, ...) _LOG(((test) ? LL_LOG : LL_LOGF), fmt, ## __VA_ARGS__)
#define TLOGFN(test, fmt, ...) _LOG((((test) ? LL_LOG : LL_LOGF) | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define SLOG(fmt, ...) _LOG((MYTHREAD == 0 ? LL_LOG : (((_sv != NULL) && (MYSV._my_log != NULL)) ? LL_LOGF : LL_DBG2)), fmt, ## __VA_ARGS__)
#define SLOGN(fmt, ...) _LOG(((MYTHREAD == 0 ? LL_LOG : (((_sv != NULL) && (MYSV._my_log != NULL)) ? LL_LOGF : LL_DBG2)) | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define SDBG(fmt, ...) if (MYTHREAD == 0) { _LOG(LL_DBG, fmt, ## __VA_ARGS__); }
#define SDBGN(fmt, ...) if (MYTHREAD == 0) { _LOG((LL_DBG | NO_LOG_PREFIX), fmt, ## __VA_ARGS__); }

#define DBG(fmt, ...)  _LOG(LL_DBG, fmt, ## __VA_ARGS__)
#define DBGN(fmt, ...)  _LOG((LL_DBG | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define DBG1(fmt, ...) _LOG(LL_DBG, fmt, ## __VA_ARGS__)
#define DBG1N(fmt, ...) _LOG((LL_DBG | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define DBG2(fmt, ...) _LOG(LL_DBG2, fmt, ## __VA_ARGS__)
#define DBG2N(fmt, ...) _LOG((LL_DBG2 | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define DBG3(fmt, ...) _LOG(LL_DBG3, fmt, ## __VA_ARGS__)
#define DBG3N(fmt, ...) _LOG((LL_DBG3 | NO_LOG_PREFIX), fmt, ## __VA_ARGS__)

#define LOG_VERSION LOGF("Version %s Built on %s\n", HIPMER_VERSION, HIPMER_BUILD_DATE)

#ifndef REDUCED_LOG_COUNT
#ifdef DEBUG
#define REDUCED_LOG_COUNT (THREADS)
#else
#define REDUCED_LOG_COUNT 512
#endif
#endif

#ifndef REDUCED_LOG_MODULUS
#define REDUCED_LOG_MODULUS ((THREADS + (REDUCED_LOG_COUNT - 1)) / (REDUCED_LOG_COUNT)) /* only up to REDUCED_LOG_COUNT (+3) logs are created */
#endif

#define OPEN_MY_LOG(fmt, ...) \
    do { \
        if (!_sv) { INIT_STATIC_VARS; MYSV.initByMethod = 'L'; } \
        assert(_sv); \
        SET_HIPMER_VERBOSITY(DEFAULT_HIPMER_VERBOSITY); \
        if ((MYTHREAD % (REDUCED_LOG_MODULUS)) != 0 && \
            MYTHREAD != 0 && MYTHREAD != (THREADS - 1) && MYTHREAD != THREADS / 2 \
            ) { break; /* only some threads will have a log: 0, mid, last & some capped modulus */ \
        } \
        char myLog_prefix[MAX_FILE_PATH+40], myLog_logfile_name[MAX_FILE_PATH*2+80]; \
        snprintf(myLog_prefix, MAX_FILE_PATH+40, fmt, ## __VA_ARGS__); \
        snprintf(myLog_logfile_name, MAX_FILE_PATH*2+80, "%s.log", myLog_prefix); \
        char *rankpath = get_rank_path(myLog_logfile_name, MYTHREAD); \
        mode_t oldumask = umask(0); umask( (S_IWGRP|S_IWOTH) | oldumask); \
        MYSV._my_log = fopen(rankpath, "a");                                \
        umask(oldumask); \
        if (!MYSV._my_log) { \
            fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not open file '%s' (mylog): %s\n" KNORM, \
                    MYTHREAD, __FILENAME__, __LINE__, HIPMER_VERSION, rankpath, strerror(errno)); \
            EXIT_FUNC(1); \
        }                \
        fflush(MYSV._my_log); \
    } while (0)

#define FLUSH_MY_LOG \
    do { \
        if (_sv) { if (MYSV._my_log) { fflush(MYSV._my_log); } } \
    } while (0)

#define LOG_STATS \
    do { \
        if (_sv) { \
                LOGF("Outstanding Memory: mallocs=%lld \tupc_all_alloc=%lld \tupc_alloc=%lld.  Untracked mallocs=%lld upc_allocs=%lld (FINAL)\n", (lld)MYSV.outstandingMallocs, (lld)MYSV.outstandingUPCAllAllocs, (lld)MYSV.outstandingUPCAllocs, (lld)MYSV.untracked_outstandingMallocs, (lld)MYSV.untracked_outstandingUPCAllocs); \
                LOGF("Atomics: ADD %lld %0.6fs, CSWAP %lld %0.6fs, GET %lld %0.6fs, SET %lld %0.6fs, LOCKS %lld %0.6fs\n", (lld)MYSV.fadds.count, MYSV.fadds.time, (lld)MYSV.cswaps.count, MYSV.cswaps.time, (lld)MYSV.gets.count, MYSV.gets.time, (lld)MYSV.sets.count, MYSV.sets.time, (lld)MYSV.atomicLocks.count, MYSV.atomicLocks.time); \
                LOGF("Barriers: %lld %0.6fs, CheckpointSetup %lld %0.6fs, CheckpointSync %lld %0.6fs, contigLocks %lld %0.6fs\n", (lld)MYSV.barriers.count, MYSV.barriers.time, (lld)MYSV.checkpointSetup.count, MYSV.checkpointSetup.time, (lld)MYSV.checkpointSync.count, MYSV.checkpointSync.time, MYSV.contigLocks.count, MYSV.contigLocks.time); \
      } \
    } while(0)

// call CLOSE_MY_LOG collectively near the termination
#define CLOSE_MY_LOG \
    do { \
        FILE *__mylog = (_sv ? MYSV._my_log : NULL); \
        if (_sv) { \
            LOG_STATS; \
            if (MYSV.initByMethod == 'L') { \
                FREE_STATIC_VARS; \
            } \
        } \
        if(__mylog) { fclose_track(__mylog); } \
        fflush(stderr); fflush(stdout); \
    } while (0)
        
// call JUST_CLOSE_MY_LOG when aborting or you just want to close the log file for some reason
#define JUST_CLOSE_MY_LOG(_mylog)  \
    do { \
        FILE *__mylog = _mylog; \
        if (_sv) { \
            if (MYSV._my_log) { \
                fclose_track(MYSV._my_log); \
                MYSV._my_log = NULL; \
            } \
        } else if (__mylog) { \
            fclose_track(__mylog); \
        } \
        fflush(stderr); fflush(stdout); \
    } while (0)

#define DIE(fmt, ...) \
    do { \
        _LOG(LL_DIE, fmt, ## __VA_ARGS__); \
        JUST_CLOSE_MY_LOG(_sv ? MYSV._my_log : NULL); \
        EXIT_FUNC(1); \
    } while (0)

#define SDIE(fmt, ...) \
    do { \
        if (!MYTHREAD) { \
            _LOG(LL_DIE, fmt, ## __VA_ARGS__); } \
        JUST_CLOSE_MY_LOG(_sv ? MYSV._my_log : NULL); \
        EXIT_FUNC(1); \
    } while (0)

#define WARN(fmt, ...) \
    do { \
        if (_sv) { MYSV._num_warnings++; } \
        _LOG(LL_WARN, fmt, ## __VA_ARGS__); \
    } while (0)

#define SWARN(fmt, ...) \
    do { \
        if (_sv && !MYTHREAD) { MYSV._num_warnings++; } \
        _LOG((!MYTHREAD ? LL_WARN : (((_sv) && MYSV._my_log) ? LL_LOGF : LL_DBG)), fmt, ## __VA_ARGS__); \
    } while (0)

#define serial_printf(fmt, ...)                     \
    do {                                            \
        if (MYTHREAD == 0) {                        \
            fprintf(stdout, fmt, ## __VA_ARGS__);    \
            fflush(stdout);                         \
        } \
        if (_sv != NULL) { if (MYSV._my_log != NULL) { LOGF(fmt, ## __VA_ARGS__); if (!strchr(fmt,'\n')) fprintf(MYSV._my_log, "\n"); } }  \
    } while (0)

#define any_printf(fmt, ...)                     \
    do {                                            \
        fprintf(stdout, fmt, ## __VA_ARGS__);    \
        fflush(stdout);                         \
        if (_sv != NULL) { if (MYSV._my_log != NULL) { LOGF(fmt, ## __VA_ARGS__); if (!strchr(fmt,'\n')) fprintf(MYSV._my_log, "\n"); } }  \
    } while (0)

#define sprintf_chk(s, max, fmt, ...) \
    do { \
        int linelen = snprintf(s, max, fmt, ## __VA_ARGS__); \
        if (linelen >= max) { \
            WARN("Thread %d, buffer overflow printing '%s' at %s:%d-%s: (" fmt ")\n", MYTHREAD, fmt, __FILENAME__, __LINE__, HIPMER_VERSION, ## __VA_ARGS__); } \
    } while (0)

void log_type_sizes();
void log_mem();

#if defined (__cplusplus)
}
#endif


#endif // __LOG_H
