#ifndef __TRACING_H
#define __TRACING_H

#define tprintf_flush(fmt, ...)                  \
    do {                                        \
        tprintf(fmt, ## __VA_ARGS__);            \
        tflush();                               \
    } while (0)

//        fprintf(stderr, "[%d] " fmt, MYTHREAD, ##__VA_ARGS__);    \
//
void tprintf(const char *fmt, ...);
void tflush(void);
void dbg(const char *fmt, ...);
void set_output_dir(const char *dirname);
void set_log_prefix(const char *logname);

#endif
