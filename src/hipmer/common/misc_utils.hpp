#ifndef __UTILS_H
#define __UTILS_H

#include <sys/stat.h>
#include <unistd.h>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <limits>
#include <memory>
#include <upcxx/upcxx.hpp>

using upcxx::barrier;
using upcxx::rank_n;
using upcxx::rank_me;
using upcxx::reduce_one;
using upcxx::reduce_all;
using upcxx::op_fast_add;
using upcxx::op_fast_min;
using upcxx::op_fast_max;
using upcxx::future;
using upcxx::make_future;
using upcxx::to_future;
using upcxx::progress;

#include "colors.h"

using std::string;
using std::stringstream;
using std::ostringstream;
using std::ifstream;
using std::ofstream;
using std::to_string;

#include "upcxx_utils/log.hpp"

using upcxx_utils::head;
using upcxx_utils::tail;
using upcxx_utils::split;
using upcxx_utils::get_file_size;
using upcxx_utils::get_current_time;

inline void check_file_exists(const string &fname)
{
  auto fnames = split(fname, ',');
  for (auto fname : fnames) {
    ifstream ifs(fname);
    if (!ifs.is_open()) SDIE("File ", fname, " cannot be accessed: ", strerror(errno), "\n");
  }
  upcxx::barrier();
}


inline string revcomp(const string &seq)
{
  string seq_rc = "";
  seq_rc.reserve(seq.size());
  for (int i = seq.size() - 1; i >= 0; i--) {
    switch (seq[i]) {
      case 'A': seq_rc += 'T'; break;
      case 'C': seq_rc += 'G'; break;
      case 'G': seq_rc += 'C'; break;
      case 'T': seq_rc += 'A'; break;
      case 'N': seq_rc += 'N'; break;
      default:
        DIE("Illegal char in revcomp of '", seq, "'\n"); 
    }
  }
  return seq_rc;
}

inline char comp_nucleotide(char ch)
{
  switch (ch) {
      case 'A': return 'T';
      case 'C': return 'G'; 
      case 'G': return 'C';
      case 'T': return 'A';
      case 'N': return 'N';
      case '0': return '0';
      default: DIE("Illegal char in revcomp of '", ch, "'\n"); 
  }
  return 0;
}



inline int hamming_dist(const string &s1, const string &s2) 
{
  if (s2.size() != (s1.size() + 1) && s1.size() != s2.size()) 
    DIE("Hamming distance substring lengths don't match, ", s1.size(), ", ", s2.size(), "\n");
  int d = 0;
  for (int i = 0; i < s1.size(); i++) 
    if (s1[i] != s2[i]) d++;
  return d;
}

inline bool is_overlap_mismatch(int dist, int overlap) 
{
  const int MISMATCH_THRES = 5;
  if (dist > MISMATCH_THRES || dist > overlap / 10) return true;
  return false;
}

inline std::pair<int, int> min_hamming_dist(const string &s1, const string &s2, int max_overlap, int expected_overlap=-1)
{
  int min_dist = max_overlap;
  if (expected_overlap != -1) {
    int min_dist = hamming_dist(tail(s1, expected_overlap), head(s2, expected_overlap));
    if (!is_overlap_mismatch(min_dist, expected_overlap)) return {min_dist, expected_overlap};
  }
  for (int d = std::min(max_overlap, (int)std::min(s1.size(), s2.size())); d >= 10; d--) {
    int dist = hamming_dist(tail(s1, d), head(s2, d));
    if (dist < min_dist) {
      min_dist = dist;
      expected_overlap = d;
      if (dist == 0) break;
    }
  }
  return {min_dist, expected_overlap};
}

static double get_free_mem_gb(void)
{
  double mem_free = 0;
#ifdef __APPLE__
// TODO
#else
  string buf;
  ifstream f("/proc/meminfo");
  if (!f.good()) return mem_free;
  while (!f.eof()) {
    getline(f, buf);
    if (buf.find("MemFree") == 0 || buf.find("Buffers") == 0 || buf.find("Cached") == 0) {
      stringstream fields;
      string units;
      string name;
      double mem;
      fields << buf;
      fields >> name >> mem >> units;
      if (units[0] == 'k') mem /= ONE_MB;
      mem_free += mem;
    }
  }
#endif
  return mem_free;
}

static int64_t get_uncompressed_file_size(string fname)
{
  string uncompressedSizeFname = fname + ".uncompressedSize";
  ifstream f(uncompressedSizeFname, std::ios::binary);
  if (!f) return get_file_size(fname) * 5; // assume 5x compression
  int64_t sz;
  f.read((char*)&sz, sizeof(int64_t));
  if (!f) return 0;
  return sz;
}


static bool hasEnding (string const &fullString, string const &ending) {
  if (fullString.length() >= ending.length()) {
    return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
  } else {
    return false;
  }
}

static int does_file_exist(string fname)
{
  struct stat s;
  if (stat(fname.c_str(), &s) != 0) {
    return 0;
  }
  return 1;
}

static void write_num_file(string fname, int64_t maxReadLength)
{
  ofstream out(fname);
  if (!out) { std::string error("Could not write to " + fname); throw error; }
  out << std::to_string(maxReadLength);
  out.close();
}

#endif
