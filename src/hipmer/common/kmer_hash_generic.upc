#include "kmer_hash_generic.h"
#include "timers.h"

void ADD_TO_HEAP(MEMORY_HEAP_T *memory_heap, LIST_T data, int destThread) 
{
    if (memory_heap->local_buffers) {
        // append local buffers, flush as needed
        assert(memory_heap->local_chunk_size > 0);
        int64_t local_index = memory_heap->local_indicies[destThread]++;
        assert(local_index < memory_heap->local_chunk_size);
        memcpy( memory_heap->local_buffers + local_index + destThread * memory_heap->local_chunk_size, &data, sizeof(LIST_T) );
        if (local_index + 1 == memory_heap->local_chunk_size) {
            assert(memory_heap->local_indicies[destThread] == memory_heap->local_chunk_size);
            FLUSH_HEAP(memory_heap, memory_heap->local_buffers + destThread * memory_heap->local_chunk_size, memory_heap->local_indicies + destThread, destThread);
            assert(memory_heap->local_indicies[destThread] == 0);
        }
    } else {
        // Add directly
        int64_t store_pos;
        UPC_ATOMIC_FADD_I64(&store_pos, &(memory_heap->heap_struct[destThread].len), 1);
        upc_memput((shared[] LIST_T *) ((memory_heap->cached_heap_ptrs[destThread]) + store_pos), &data, sizeof(LIST_T));
    }
}
void FLUSH_HEAP(MEMORY_HEAP_T *memory_heap, LIST_T *data, int64_t *len, int destThread)
{
    int64_t store_pos;
    assert(*len > 0);
    UPC_ATOMIC_FADD_I64(&store_pos, &(memory_heap->heap_struct[destThread].len), *len);
    upc_memput((shared[] LIST_T *) ((memory_heap->cached_heap_ptrs[destThread]) + store_pos), data, *len * sizeof(LIST_T));
    *len = 0;
}
void FINAL_FLUSH_HEAP(MEMORY_HEAP_T *memory_heap)
{
    if (!memory_heap->local_buffers)
        return;
    int pushedThreads = 0;
    int64_t pushedRecords = 0;
    upc_tick_t start = UPC_TICKS_NOW();
    for(int i = MYTHREAD; i < THREADS + MYTHREAD; i++) {
        int destThread = i % THREADS;
        if (memory_heap->local_indicies[destThread] > 0) {
            pushedThreads++;
            pushedRecords += memory_heap->local_indicies[destThread];
            FLUSH_HEAP(memory_heap, memory_heap->local_buffers + destThread * memory_heap->local_chunk_size, memory_heap->local_indicies + destThread, destThread);
        }
    }
    LOGF("Final flush of %d threads, %lld records took %0.3f s\n", pushedThreads, (lld) pushedRecords, ELAPSED_TIME(start));
}
void FREE_LOCAL_BUFFERS(MEMORY_HEAP_T *memory_heap)
{
    if (memory_heap->local_buffers) {
        assert(memory_heap->local_chunk_size >= MIN_CHUNK_SIZE);
        free_chk(memory_heap->local_buffers);
        free_chk(memory_heap->local_indicies);
        memory_heap->local_buffers = NULL;
        memory_heap->local_indicies = NULL;
        memory_heap->local_chunk_size = 0;
    }
}


#ifndef COLL_ALLOC

void FREE_MEMORY_HEAP(MEMORY_HEAP_T *memory_heap)
{
    upc_barrier;
    UPC_FREE_CHK(memory_heap->heap_struct[MYTHREAD].ptr);
    memory_heap->heap_struct[MYTHREAD].ptr = NULL;
    upc_barrier;
    UPC_ALL_FREE_CHK(memory_heap->heap_struct);
    free_chk(memory_heap->cached_heap_ptrs);
    memory_heap->heap_struct = NULL;
    memory_heap->cached_heap_ptrs = NULL;
    FREE_LOCAL_BUFFERS(memory_heap);
    memset(memory_heap, 0, sizeof(MEMORY_HEAP_T));
}

/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *CREATE_HASH_TABLE(int64_t size, MEMORY_HEAP_T *memory_heap, int64_t my_heap_size)
{
    HASH_TABLE_T *result;
    int64_t i;
    int64_t n_buckets = size;
    UPC_TICK_T timer_start, timer_end;

    if (my_heap_size <= 0) {
        my_heap_size = 1;
    }

    timer_start = UPC_TICKS_NOW();
    result = (HASH_TABLE_T *)calloc_chk(1, sizeof(HASH_TABLE_T));
    result->size = n_buckets;
    UPC_ALL_ALLOC_CHK(result->table, n_buckets, sizeof(BUCKET_T));
    timer_end = UPC_TICKS_NOW();
    serial_printf("Time spent in all_alloc() for buckets is %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = UPC_TICKS_NOW();

    for (i = MYTHREAD; i < n_buckets; i += THREADS) {
        assert(upc_threadof(&(result->table[i])) == MYTHREAD);
        result->table[i].head = NULL;
    }
    timer_end = UPC_TICKS_NOW();
    serial_printf("Time spent in NULL-ing buckets is %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));
    timer_start = UPC_TICKS_NOW();

    memset(memory_heap, 0, sizeof(MEMORY_HEAP_T));
    UPC_ALL_ALLOC_CHK(memory_heap->heap_struct, THREADS, sizeof(HEAP_STRUCT));

    timer_end = UPC_TICKS_NOW();
    serial_printf("Time spent in all_alloc() for heap ptrs and indices is %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));

    timer_start = UPC_TICKS_NOW();
    memory_heap->heap_size = my_heap_size; // local variable
    memory_heap->heap_struct[MYTHREAD].len = 0;
    UPC_ALLOC_CHK(memory_heap->heap_struct[MYTHREAD].ptr, my_heap_size * sizeof(LIST_T));
    timer_end = UPC_TICKS_NOW();
    serial_printf("Time spent in alloc() for my shared heap %lld entries (%0.2f MB) is %f seconds\n", (lld) my_heap_size, 1.0 / ONE_MB * my_heap_size * sizeof(LIST_T), UPC_TICKS_TO_SECS(timer_end - timer_start));

    upc_barrier;

    timer_start = UPC_TICKS_NOW();
    memory_heap->cached_heap_ptrs = (SHARED_HEAP_PTR *)malloc_chk(THREADS * sizeof(SHARED_HEAP_PTR));
    memory_heap->local_chunk_size = get_chunk_size( sizeof(LIST_T), MYSV.cores_per_node);
    if (memory_heap->local_chunk_size >= MIN_CHUNK_SIZE) {
        serial_printf("Allocating %0.3f MB per node %lld elements (%lld bytes) per-thread for local chunks\n", 1.0/ONE_MB * memory_heap->local_chunk_size * THREADS * MYSV.cores_per_node * sizeof(LIST_T), (lld) memory_heap->local_chunk_size, (lld) sizeof(LIST_T));
        memory_heap->local_buffers = malloc_chk(memory_heap->local_chunk_size * sizeof(LIST_T) * THREADS);
        memory_heap->local_indicies = calloc_chk(THREADS, sizeof(int64_t));
    } else {
        memory_heap->local_chunk_size = 0;
        serial_printf("Local chunk allocation is disabled because scale is too large and memory is too tight.\n");
    }

    for (i = MYTHREAD; i < THREADS+MYTHREAD; i++) {
        int idx = i%THREADS;
        memory_heap->cached_heap_ptrs[idx] = memory_heap->heap_struct[idx].ptr;
    }
    timer_end = UPC_TICKS_NOW();
    serial_printf("Time spent for caching remote pointers is %f seconds\n", UPC_TICKS_TO_SECS(timer_end - timer_start));

    return result;
}

void DESTROY_HASH_TABLE(HASH_TABLE_T **_ht, MEMORY_HEAP_T *_memory_heap)
{
    assert(_memory_heap->heap_size >= _memory_heap->heap_struct[MYTHREAD].len);
    if (_memory_heap != NULL) {
        FREE_MEMORY_HEAP(_memory_heap);
    }
    if (_ht != NULL && *_ht != NULL) {
        HASH_TABLE_T *ht = *_ht;
        UPC_ALL_FREE_CHK(ht->table);
        free_chk(ht);
        *_ht = NULL;
    }
}

#else // def COLL_ALLOC

/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *CREATE_HASH_TABLE(int64_t size, MEMORY_HEAP_T *memory_heap)
{
    HASH_TABLE_T *result;
    int64_t i;
    int64_t n_buckets = size;

#ifdef ALLOC_TIMING
    UPC_TICK_T start_timer, end_timer;
    upc_barrier;
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // def ALLOC_TIMING

    result = (HASH_TABLE_T *)malloc_chk(sizeof(HASH_TABLE_T));
    result->size = n_buckets;
    UPC_ALL_ALLOC_CHK(result->table, n_buckets, sizeof(BUCKET_T));

#ifdef ALLOC_TIMING
    upc_barrier;
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nupc_all_alloc()time for buckets is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // def ALLOC_TIMING

#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING

    for (i = MYTHREAD; i < n_buckets; i += THREADS) {
        result->table[i].head = NULL;
    }

#ifdef ALLOC_TIMING
    upc_barrier;
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nTime for initializing heads is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // ALLOC_TIMING

#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING
    shared[BS] LIST_T * init_ptr;
    UPC_ALL_ALLOC_CHK(init_ptr, EXPANSION_FACTOR * n_buckets, sizeof(LIST_T));

#ifdef ALLOC_TIMING
    upc_barrier;
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nupc_all_alloc()time for entries is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // ALLOC_TIMING

#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING
    UPC_ALL_ALLOC_CHK(memory_heap->heap_struct, THREADS, sizeof(HEAP_STRUCT));
#ifdef ALLOC_TIMING
    upc_barrier;
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nupc_all_alloc()time for heap_ptrs is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // ALLOC_TIMING

#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING

#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING
    memory_heap->heap_struct[MYTHREAD].ptr = (shared[] LIST_T *)(init_ptr + MYTHREAD);
#ifdef ALLOC_TIMING
    upc_barrier;
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nTime for initializing heap ptrs is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // ALLOC_TIMING

    /* Make sure that memory is allocated before copying local object */
#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        start_timer = UPC_TICKS_NOW();
    }
#endif // ALLOC_TIMING
    memory_heap->heap_struct[MYTHREAD].len = 0;
    upc_barrier;
#ifdef ALLOC_TIMING
    if (MYTHREAD == 0) {
        end_timer = UPC_TICKS_NOW();
        printf("\nTime for initializing heap indices is %f seconds\n", UPC_TICKS_TO_SECS(end_timer - start_timer));
    }
#endif // ALLOC_TIMING

    return result;
}
#endif // def COLL_ALLOC


/* Use this lookup function when no writes take place in the distributed hashtable */
shared[] LIST_T *LOOKUP_KMER_AND_COPY(HASH_TABLE_T *hashtable, const unsigned char *kmer, LIST_T *cached_copy, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
    assert(kmer_packed_len <= MAX_KMER_PACKED_LENGTH);
    // TODO: Fix hash functions to compute on packed kmers and avoid conversions
    int64_t hashval = hashkmer(hashtable->size, (char *)kmer, kmer_len);
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH];
    memset(packed_key, 0, MAX_KMER_PACKED_LENGTH);

    packSequence(kmer, packed_key, kmer_len);
    shared[] LIST_T * result = NULL;
    BUCKET_T local_buc;

    local_buc = hashtable->table[hashval];
    result = local_buc.head;


    for (; result != NULL; ) {
        //upc_memget(remote_packed_key, result->packed_key, kmer_packed_len * sizeof(char));
        //upc_memget(local_res, result, sizeof(LIST_T));
        assert(upc_threadof(result) == hashval % THREADS);
#ifdef MERACULOUS
        // EGEOR: Atomic read of field result->my_contig
        //loop_until( *cached_copy = *result , IS_VALID_UPC_PTR(cached_copy->my_contig) );
        *cached_copy = *result; /* get most of the data, ensure the my_contig member is atomically refereshed */
        loop_until(UPC_ATOMIC_GET_SHPTR(&(cached_copy->my_contig), &(result->my_contig)), IS_VALID_UPC_PTR(cached_copy->my_contig));
#endif
#ifdef MERDEPTH
        *cached_copy = *result;
#endif
#ifdef CEA
        *cached_copy = *result;
#endif
#ifdef NB_EXPLORE
        *cached_copy = *result;
#endif
        //if (local_res.structid != 1988) {
        //	printf("FATAL error in lookup, struct id is %d!!!\n", local_res.structid);
        //}
        if (comparePackedSeq(packed_key, cached_copy->packed_key, kmer_packed_len) == 0) {
#ifdef MERACULOUS
            DBG2("LOOKUP_KMER found '%.*s' as %s\n", kmer_len, (char*) kmer, cached_copy->used_flag == 0 ? "UNUSED" : "USED");
#endif
            return result;
        }
        result = cached_copy->next;
    }
    assert(result == NULL);
    if (HIPMER_VERBOSITY >= LL_DBG) {
        assert(kmer_len < MAX_KMER_SIZE);
        unsigned char tmp[MAX_KMER_SIZE];
        memset(tmp, 0, MAX_KMER_SIZE); // initialize bytes
        tmp[kmer_len] = '\0';
        unpackSequence(packed_key, tmp, kmer_len);
        DBG2("Thread %d: LOOKUP_KMER(): did not find %s (%s)\n", MYTHREAD, tmp, kmer);
        assert(strncmp((const char *)tmp, (const char *)kmer, kmer_len) == 0);
    }
#ifdef MERACULOUS
    DBG2("LOOKUP_KMER did not find '%.*s'\n", kmer_len, (char*) kmer);
#endif
    return result;
}

shared[] LIST_T *LOOKUP_KMER(HASH_TABLE_T *hashtable, const unsigned char *kmer, int kmer_len)
{
    LIST_T cached_copy;

    return LOOKUP_KMER_AND_COPY(hashtable, kmer, &cached_copy, kmer_len);
}

/* find the entry for this kmer or reverse complement */
shared[] LIST_T *LOOKUP_LEAST_KMER_AND_COPY(HASH_TABLE_T *dist_hashtable, const char *next_kmer,
                                            LIST_T *cached_copy, int *is_least, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    char auxiliary_kmer[MAX_KMER_SIZE];
    char *kmer_to_search;
    memset(auxiliary_kmer, 0, MAX_KMER_SIZE);     // initialize bytes
    auxiliary_kmer[kmer_len] = '\0';
    /* Search for the canonical kmer */
    kmer_to_search = getLeastKmer(next_kmer, auxiliary_kmer, kmer_len);
    *is_least = (kmer_to_search == next_kmer) ? 1 : 0;
    shared[] LIST_T * res = LOOKUP_KMER_AND_COPY(dist_hashtable, (unsigned char *)kmer_to_search, cached_copy, kmer_len);
    DBG2("Thread %d: LOOKUP_LEAST_KMER2(%s) is_least: %d res: %s\n", MYTHREAD, next_kmer, *is_least, res == NULL ? " not found" : "found");
    return res;
}

shared[] LIST_T *LOOKUP_LEAST_KMER(HASH_TABLE_T *dist_hashtable, const char *next_kmer, int *was_least, int kmer_len)
{
    LIST_T cached_copy;

    return LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, next_kmer, &cached_copy, was_least, kmer_len);
}

void SET_EXT_OF_KMER(LIST_T *lookup_res, int is_least, char *new_seed_le, char *new_seed_re)
{
    if (is_least) {
#ifdef MERACULOUS
        convertPackedCodeToExtension(lookup_res->packed_extensions, new_seed_le, new_seed_re);
#endif
    } else {
#ifdef MERACULOUS
        convertPackedCodeToExtension(lookup_res->packed_extensions, new_seed_re, new_seed_le);
#endif
        *new_seed_re = reverseComplementBaseExt(*new_seed_re);
        *new_seed_le = reverseComplementBaseExt(*new_seed_le);
    }
    DBG2("Thread %d: SET_EXT_OF_KMER: is_least: %d l:%c r:%c\n", MYTHREAD, is_least, *new_seed_le, *new_seed_re);
}

/* find the entry for this kmer or reverse complement, set the left and right extensions */
shared[] LIST_T *LOOKUP_AND_GET_EXT_OF_KMER(HASH_TABLE_T *dist_hashtable, const char *next_kmer, char *new_seed_le, char *new_seed_re, int kmer_len)
{
    int is_least;

    shared[] LIST_T * lookup_res;
    LIST_T copy;
    *new_seed_le = '\0';
    *new_seed_re = '\0';

    /* Search for the canonical kmer */
    lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, next_kmer, &copy, &is_least, kmer_len);

    if (lookup_res == NULL) {
        return lookup_res;
    }

    /* Find extensions of the new kmer found in the hashtable */
    SET_EXT_OF_KMER(&copy, is_least, new_seed_le, new_seed_re);

    return lookup_res;
}

#ifndef AGGREGATE_STORES
int ADD_KMER(HASH_TABLE_T *hashtable, unsigned char *kmer, unsigned char *extensions, MEMORY_HEAP_T *memory_heap, int64_t *ptrs_to_stack, int64_t *ptrs_to_stack_limits, int CHUNK_SIZE, int kmer_len)
{
    LIST_T new_entry;

    shared[] LIST_T * insertion_point = NULL;

    int64_t hashval = hashkmer(hashtable->size, (char *)kmer, kmer_len);
    int64_t pos;
    int64_t new_init_pos, new_limit;
    int remote_thread = upc_threadof(&(hashtable->table[hashval]));

    if (ptrs_to_stack[remote_thread] < ptrs_to_stack_limits[remote_thread]) {
        pos = ptrs_to_stack[remote_thread];
        ptrs_to_stack[remote_thread]++;
    } else {
        UPC_ATOMIC_FADD_I64(&new_init_pos, &(memory_heap->heap_struct[remote_thread].len), CHUNK_SIZE);

        new_limit = new_init_pos + CHUNK_SIZE;
        if (new_limit > EXPANSION_FACTOR * (hashtable->size / THREADS)) {
            new_limit = EXPANSION_FACTOR * (hashtable->size / THREADS);
        }
        if (new_limit <= new_init_pos) {
            pos = EXPANSION_FACTOR * (hashtable->size / THREADS);
        } else {
            pos = new_init_pos;
        }
        new_init_pos++;
        ptrs_to_stack[remote_thread] = new_init_pos;
        ptrs_to_stack_limits[remote_thread] = new_limit;
    }

    if (pos < EXPANSION_FACTOR * (hashtable->size / THREADS)) {
        // Case 1: There is more space in the remote heap
        insertion_point = (shared[] LIST_T *)((memory_heap->heap_struct[remote_thread].ptr) + pos);
    } else {
        // Case 2; There is NOT more space in the remote heap, allocate locally space
        UPC_ALLOC_CHK(insertion_point, sizeof(LIST_T));
#ifdef MERACULOUS
        UPC_ALLOC_CHK(insertion_point->my_contig, sizeof(contig_ptr_box_list_t));
        insertion_point->my_contig->contig = NULL;
#endif  // MERACULOUS
    }

    /* Pack and insert kmer info into list - use locks to avoid race conditions */
    packSequence(kmer, new_entry.packed_key, kmer_len);
#ifdef MERACULOUS
    new_entry.packed_extensions = convertExtensionsToPackedCode(extensions);
    new_entry.used_flag = UNUSED;
#endif // MERACULOUS
    new_entry.next = NULL;

#ifdef MERACULOUS
    new_entry.my_contig = NULL;
#endif // MERACULOUS
    upc_memput(insertion_point, &new_entry, sizeof(LIST_T));
    //printf("Thread %d returned from upc_memput() and hashval is %lld \n", MYTHREAD, hashval);

    insertion_point->next = hashtable->table[hashval].head;
    hashtable->table[hashval].head = insertion_point;

    return 0;
}
#endif // AGGREGATE_STORES

#ifdef AGGREGATE_STORES
int ADD_KMER(HASH_TABLE_T *hashtable, unsigned char *kmer, unsigned char *extensions, MEMORY_HEAP_T *memory_heap, int64_t *local_buffs_hashvals, LIST_T *local_buffs, int64_t *local_index, int CHUNK_SIZE, int kmer_len)
{
    const double EXPANSION_FACTOR = 1.2;

    LIST_T new_entry;

    shared[] LIST_T * insertion_point;
    int64_t hashval = hashkmer(hashtable->size, (char *)kmer, kmer_len);
    int64_t j, excess;
    int64_t store_pos;
    int remote_thread = upc_threadof(&(hashtable->table[hashval]));
    int64_t cur_hashval;

    /* Pack and insert kmer info into local_list_t buffer */
    packSequence(kmer, new_entry.packed_key, kmer_len);
    new_entry.packed_extensions = convertExtensionsToPackedCode(extensions);
#ifdef MERACULOUS
    new_entry.used_flag = UNUSED;
#endif
    new_entry.next = NULL;
    new_entry.my_contig = NULL;

    memcpy(&(local_buffs[local_index[remote_thread] + remote_thread * CHUNK_SIZE]), &new_entry, sizeof(LIST_T));
    local_buffs_hashvals[local_index[remote_thread] + remote_thread * CHUNK_SIZE] = hashval;

    if (local_index[remote_thread] == CHUNK_SIZE - 1) {
        /* Now we should do a batch upc_memput */
        UPC_ATOMIC_FADD_I64(&store_pos, &(memory_heap->heap_struct[remote_thread].len), CHUNK_SIZE);

        if (store_pos >= (EXPANSION_FACTOR * ((hashtable->size + hashtable->size % THREADS) / THREADS))) {
            store_pos = (EXPANSION_FACTOR * ((hashtable->size + hashtable->size % THREADS) / THREADS));
        }

        /* Check if there is sufficient space left in the remote heap  */
        if ((store_pos + CHUNK_SIZE) > (EXPANSION_FACTOR * ((hashtable->size + hashtable->size % THREADS) / THREADS))) {
            excess = (store_pos + CHUNK_SIZE) - (EXPANSION_FACTOR * ((hashtable->size + hashtable->size % THREADS) / THREADS));
            upc_memput((shared LIST_T *)((memory_heap->heap_struct[remote_thread].ptr) + store_pos), &(local_buffs[remote_thread * CHUNK_SIZE]), (CHUNK_SIZE - excess) * sizeof(LIST_T));
            for (j = 0; j < CHUNK_SIZE - excess; j++) {
                /* Use locks to update the head pointers in the remote buckets */
                cur_hashval = local_buffs_hashvals[j + remote_thread * CHUNK_SIZE];
                upc_lock(hashtable->table[cur_hashval].bucket_lock);
                ((memory_heap->heap_struct[remote_thread].ptr) + store_pos + j)->next = hashtable->table[cur_hashval].head;
                hashtable->table[cur_hashval].head = (shared LIST_T *)((memory_heap->heap_struct[remote_thread].ptr) + store_pos + j);
                upc_unlock(hashtable->table[cur_hashval].bucket_lock);
            }
            UPC_ALLOC_CHK(insertion_point, excess * sizeof(LIST_T));
            upc_memput(insertion_point, &(local_buffs[remote_thread * CHUNK_SIZE + (CHUNK_SIZE - excess)]), excess * sizeof(LIST_T));
            for (j = 0; j < excess; j++) {
                /* Use locks to update the head pointers in the remote buckets */
                cur_hashval = local_buffs_hashvals[CHUNK_SIZE - excess + j + remote_thread * CHUNK_SIZE];
                upc_lock(hashtable->table[cur_hashval].bucket_lock);
                (insertion_point + j)->next = hashtable->table[cur_hashval].head;
                hashtable->table[cur_hashval].head = (shared LIST_T *)(insertion_point + j);
                upc_unlock(hashtable->table[cur_hashval].bucket_lock);
            }
        } else {
            upc_memput((shared LIST_T *)((memory_heap->heap_struct[remote_thread].ptr) + store_pos), &(local_buffs[remote_thread * CHUNK_SIZE]), CHUNK_SIZE * sizeof(LIST_T));
            for (j = 0; j < CHUNK_SIZE; j++) {
                /* Use locks to update the head pointers in the remote buckets */
                cur_hashval = local_buffs_hashvals[j + remote_thread * CHUNK_SIZE];
                upc_lock(hashtable->table[cur_hashval].bucket_lock);
                ((memory_heap->heap_struct[remote_thread].ptr) + store_pos + j)->next = hashtable->table[cur_hashval].head;
                hashtable->table[cur_hashval].head = (shared LIST_T *)((memory_heap->heap_struct[remote_thread].ptr) + store_pos + j);
                upc_unlock(hashtable->table[cur_hashval].bucket_lock);
            }
        }

        local_index[remote_thread] = 0;
    } else {
        local_index[remote_thread]++;
    }

    return 0;
}
#endif // AGGREGATE_STORES
