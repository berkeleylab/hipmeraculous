#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>

#include <upc.h>
#include <upc_collective.h>
#include <upc_tick.h>

#include "upc_common.h"
#include "common.h"

extern StaticVars _sv;

#include "utils.h"
#include "tracing.h"

static char _output_dir[MAX_FILE_PATH] = ".";

void set_output_dir(const char *dirname)
{
    strcpy(_output_dir, dirname);
}

void set_log_prefix(const char *name)
{
    // This should only be necessary when running stand alone modules
    // or a DEBUG build.  Never create extra files in a production run
    char tmp[MAX_FILE_PATH*2+20];

    if (!_sv) {
        INIT_STATIC_VARS;
    } else {
#ifndef DEBUG
        return;
#endif
    }
    if (MYSV.LOG_PREFIX) {
        free(MYSV.LOG_PREFIX);
    }
    strcpy(tmp, name);
    get_rank_path(tmp, MYTHREAD);
    MYSV.LOG_PREFIX = strdup(tmp);
    if (MYSV._my_log) {
        return;
    }
    sprintf(tmp, "%s/%s_%d.log", _output_dir, MYSV.LOG_PREFIX, MYTHREAD);
    MYSV._my_log = fopen_chk(tmp, "a");
}

void tprintf(const char *fmt, ...)
{
    if (!_sv || !MYSV._my_log) {
        return;
    }
    va_list args;
    va_start(args, fmt);
    vfprintf(MYSV._my_log, fmt, args);
    va_end(args);
    if (!strchr(fmt, '\n')) fprintf(MYSV._my_log, "\n");
#ifdef DEBUG
    // always flush messages if debugging
    tflush();
#endif
}

void tflush(void)
{
    if (!_sv || !MYSV._my_log) {
        return;
    }
    fflush(MYSV._my_log);
}

void dbg(const char *fmt, ...)
{
    if (!_sv || !MYSV.LOG_PREFIX || strlen(MYSV.LOG_PREFIX) == 0) {
        return;
    }
    if (!MYSV._dbg_file) {
        char buf[MAX_FILE_PATH*2+20];
        if (MYSV.LOG_PREFIX[0] == '\0') {
            SDIE("Please set the tracing prefix: set_log_prefix in %s\n", __FILE__);
        }
        sprintf(buf, "%s/%s_%d.dbg", _output_dir, (char *)MYSV.LOG_PREFIX, MYTHREAD);
        MYSV._dbg_file = fopen_chk(buf, "w");
    }
    va_list args;
    va_start(args, fmt);
    vfprintf(MYSV._dbg_file, fmt, args);
    va_end(args);
#ifdef DEBUG
    // always flush if debugging
    fflush(MYSV._dbg_file);
#endif
}
