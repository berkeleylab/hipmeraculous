#include "upc_parallel_sort.h"

void PARALLEL_SORT(Buffer myArray, PSORT_CMP cmp) {
    double start = now();
    int64_t nmemb = getLengthBuffer(myArray) / sizeof(PSORT_T);
    int64_t threadsSq = (int64_t) THREADS * (int64_t) THREADS;
    LOGF("starting %s parallel sort with %lld elements in myArray\n", STRINGIFY(UPC_PARALLEL_SORT_TYPE), (lld) nmemb);
    PSORT_STRUCT s = (_PSORT_STRUCT*) calloc_chk(1, sizeof(_PSORT_STRUCT));
    s->cmp = cmp;
    UPC_ALL_ALLOC_CHK(s->buf, threadsSq + 1, sizeof(int64_t));
    s->partitionCounts = s->buf;
    s->totalCount = s->buf + threadsSq;
    UPC_ALL_ALLOC_CHK(s->partitions, threadsSq, sizeof(PSORT_T));
    PSORT_T_SHPTR lastEntry = (PSORT_T_SHPTR) (s->partitions + threadsSq - 1);
    PSORT_T_PTR startPtr = (nmemb == 0) ? NULL : (PSORT_T_PTR) getStartBuffer(myArray);

    // First sort data in myArray
    double startLocalQsort = now();
    if (nmemb) {
        qsort(startPtr, nmemb, sizeof(PSORT_T), s->cmp); 
        int64_t myPrefix;
        UPC_ATOMIC_FADD_I64(&myPrefix, s->totalCount, 1);
        if (myPrefix == 0) {
            // first one, populate the last entry so that threads with empty data can participate
            upc_memput(lastEntry, startPtr + nmemb -1, sizeof(PSORT_T));
        }
    }
    LOGF("local qsort in %0.3f s\n", now() - startLocalQsort);

    UPC_LOGGED_BARRIER;
    PSORT_T tmp;
    if (nmemb == 0) {
        upc_memget(&tmp, lastEntry, sizeof(PSORT_T));
    }

    // partition myArray by THREADS store to s->partitions
    double startPartitions = now();
    int64_t partitionSize = (nmemb+THREADS-1) / THREADS;
    LOAD_BALANCED_FOR(idx, THREADS) {
        int64_t partitionIdx = ((int64_t) MYTHREAD * (int64_t) THREADS) + idx;
        int64_t myIdx = (idx+1) * partitionSize - 1; // the last member of the block 
        if (myIdx >= nmemb) myIdx = nmemb-1; // remainder partitions end with last entry and will be effectively empty
        upc_memput( &(s->partitions[partitionIdx]), myIdx < 0 ? &tmp : startPtr + myIdx, sizeof(PSORT_T));
    }
    LOGF("partitioned in %0.3f s\n", now() - startPartitions);

    UPC_LOGGED_BARRIER;    

    // sort the partitions over each thread individually
    double startSortPartitions = now();
    PSORT_T_PTR myPartitions = (PSORT_T_PTR) &(s->partitions[MYTHREAD]);
    qsort(myPartitions, THREADS, sizeof(PSORT_T), s->cmp);
    LOGF("sorted myPartitions in %0.3fs\n", now() - startSortPartitions);

    UPC_LOGGED_BARRIER;

    // find the median value for each partition
    double startExchangePartitions = now();
    int64_t medianIdx = ((int64_t) THREADS / 2) * THREADS;
    s->globalPartitions = (PSORT_T_PTR) calloc_chk(THREADS, sizeof(PSORT_T));
    LOAD_BALANCED_FOR(idx, THREADS) {
        upc_memget(s->globalPartitions + idx, s->partitions + medianIdx + idx, sizeof(PSORT_T));
    }

    // qsort the partitions as these may be out of order in the pathologial case
    qsort(s->globalPartitions, THREADS, sizeof(PSORT_T), s->cmp);
    LOGF("Got the median partitions in %0.3f s\n", now() - startExchangePartitions);

    // count what will be sent
    s->myOffsets = (int64_t*) calloc_chk(THREADS, sizeof(int64_t));
    s->myCounts = (int64_t*) calloc_chk(THREADS, sizeof(int64_t));
    double startCount = now();
    int64_t curCount = 0, curThread = 0; 
    for(int64_t curIdx = 0; curIdx < nmemb; /*no increment here*/ ) {
        PSORT_T_PTR cur = startPtr + curIdx;
        if (s->cmp(cur, s->globalPartitions + curThread) <= 0) {
            // this will be sent to curThread
            curCount++;
            curIdx++;
            continue;
        } else {
            // recored current and increment thread
            if (curThread == THREADS-1) {
                // this is the last thread, put remaining
                curCount += nmemb - curIdx;
                curIdx += nmemb - curIdx;
            }
            DBG("Sending curCount=%lld to thread %d curIdx=%lld\n", (lld) curCount, curThread, (lld) curIdx);
            s->partitionCounts[ MYTHREAD * THREADS + curThread ] = curCount;
            s->myCounts[curThread] = curCount;
            curCount = 0;
            curThread++;
            if (curThread < THREADS) s->myOffsets[curThread] = curIdx;
            // do not increment curIdx
        }
    }
    // record remainder
    if (curThread < THREADS) {
        DBG("Sending curCount=%lld to thread %d curIdx=(last)\n", (lld) curCount, curThread);
        s->partitionCounts[ MYTHREAD * THREADS + curThread ] = curCount;
        s->myCounts[curThread] = curCount;
    }

    LOGF("counted entries per partition in %0.3f s\n", now() - startCount);

    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(s->partitions);
    free_chk(s->globalPartitions);

    // allocate what will be received
    int64_t myTotal = 0;
    for(int64_t idx = MYTHREAD; idx < threadsSq; idx += THREADS) {
        int64_t count = s->partitionCounts[idx];
        s->partitionCounts[idx] = myTotal; // transform to offsets
        myTotal += count;
    }

    LOGF("Allocating for myTotal=%lld\n", (lld) myTotal)
    UPC_ALL_ALLOC_CHK(s->globalData, THREADS, sizeof(PSORT_T_SHPTR));
    UPC_ALLOC_CHK(s->globalData[MYTHREAD], (myTotal+1) * sizeof(PSORT_T)); // plus 1 to avoid zero allocation

    UPC_LOGGED_BARRIER;
    
    // transfer blocks to threads
    double startTransfer = now();
    LOAD_BALANCED_FOR(thr, THREADS) {
        int64_t partitionOffset = s->partitionCounts[ MYTHREAD * THREADS + thr ];
        if (s->myCounts[thr]) {
            upc_memput(s->globalData[thr] + partitionOffset, startPtr + s->myOffsets[thr], s->myCounts[thr] * sizeof(PSORT_T));
        }
        DBG("Put %lld to thread %d\n", s->myCounts[thr], thr);
    }
    LOGF("Sent %lld entries in %0.3f s\n", nmemb, now() - startTransfer);
    free_chk(s->myOffsets);
    free_chk(s->myCounts);

    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(s->buf);
    s->partitionCounts = NULL;
    s->totalCount = NULL;

    // merge sorted arrays back into private data
    double startSort2 = now();
    resetBuffer(myArray);
    reserveBuffer(myArray, sizeof(PSORT_T) * myTotal);
    if (1 || myTotal / 64 < THREADS) {
        // just qsort it will likely be faster anyway
        startPtr = (PSORT_T_PTR) s->globalData[MYTHREAD];
        qsort(startPtr, myTotal, sizeof(PSORT_T), s->cmp);
        memcpyBuffer(myArray, startPtr, sizeof(PSORT_T) * myTotal);
    } else {
        // merge sorted arrays
        DIE("FIXME - not implemented\n");
    }
    LOGF("Completed local sort of %lld in %0.3f s\n", (lld) myTotal, now() - startSort2);

    // cleanup
    UPC_FREE_CHK(s->globalData[MYTHREAD]);
    UPC_ALL_FREE_CHK(s->globalData);
    free_chk(s);
}

