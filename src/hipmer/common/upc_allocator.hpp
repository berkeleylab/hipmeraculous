#ifndef _UPC_ALLOCATOR_HPP
#define _UPC_ALLOCATOR_HPP

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "upc2obj.h"
#include "../kmercount/mpi_common.h"
#include "common.h"

#define ALLOCATOR_TRAITS(T)                \
    typedef T type;            \
    typedef type value_type;      \
    typedef value_type *pointer;         \
    typedef value_type const *const_pointer;   \
    typedef value_type&       reference;       \
    typedef value_type const& const_reference; \
    typedef std::size_t size_type;       \
    typedef std::ptrdiff_t difference_type; \

template<typename T>
struct max_allocations {
    enum { value = static_cast<std::size_t>(-1) / sizeof(T) };
};

namespace upc_allocator {

/* util */

class memory_distribution {

static uint32_t getModulus()
{
    static uint32_t _ = 0;
// FIXME make atomic when threaded
//#pragma omp atomic
    uint32_t ret = _++;
    return ret;
}

static uint32_t &modIn()
{
    static uint32_t _ = 0;
    return _;
}
static uint32_t &modulo()
{
    static uint32_t _ = 1;
    assert(_>0);
    return _;
}

public:
static void setRatio(uint32_t num, uint32_t outof) {
    if (num >= outof || outof == 0) DIE("Invalid setRatio(%d,%d) first must be less than second!\n", num, outof);
    modIn() = num;
    modulo() = outof;
}

static bool isNextAllocationShared()
{
    return (getModulus() % modulo()) <= modIn();
}

};

/* C++ templates */

template <typename T>
class simple_allocator {
public:

static const uint64_t IS_PRIVATE = 0xAAAAAAAA;
static const uint64_t IS_SHARED = 0x55555555;

/* Member types */
ALLOCATOR_TRAITS(T)
typedef std::false_type propagate_on_container_copy_assignment;
typedef std::true_type propagate_on_container_move_assignment;
typedef std::true_type propagate_on_container_swap;

template<class Other>
struct rebind { typedef simple_allocator<Other> other; };

simple_allocator()
{
}
simple_allocator(const simple_allocator& x)
{
}
simple_allocator(simple_allocator && x)
{
}
simple_allocator& operator=(const simple_allocator& x)
{
    return *this;
}
simple_allocator& operator=(simple_allocator && x)
{
    return *this;
}

virtual ~simple_allocator()
{
}

// Max number of objects that can be allocated in one call
size_type max_size(void) const
{
    return max_allocations<T>::value;
}

pointer address(reference x) const
{
    return &x;
}
const_pointer address(const_reference x) const
{
    return &x;
}

T *allocate(std::size_t n, const T *hint = NULL)
{
    if (n <= std::numeric_limits<std::size_t>::max() / sizeof(T)) {
        if (memory_distribution::isNextAllocationShared()) {
            uint64_t *_mem = (uint64_t*) upc2obj_upc_alloc(n * sizeof(T) + 2 * sizeof(int64_t));
            *(_mem++) = IS_SHARED;
            *(_mem++) = n;
            DBG2("upc_allocator::simple_allocator::allocate(%lld, %p): SHARED %p\n", (lld)n, hint, _mem);
            return (T *)(_mem);
        } else {
            uint64_t *_mem = (uint64_t*) malloc(n * sizeof(T) + 2 * sizeof(int64_t));
            *(_mem++) = IS_PRIVATE;
            *(_mem++) = n;
            DBG2("upc_allocator::simple_allocator::allocate(%lld, %p): PRIVATE %p\n", (lld)n, hint, _mem);
            return (T *)(_mem);
        }
    } else {
        DIE("Invalid allocation of %lld\n", (lld)n);
    }
    return static_cast<T *>(NULL);
}
bool isSharedAllocation(T* ptr) {
    if (ptr != NULL) {
        uint64_t * _mem = (uint64_t*) ptr;
        _mem--;
        _mem--;
        if (*_mem == IS_PRIVATE) {
            return false;
        } else if (*_mem == IS_SHARED) {
            return true;
        }
    }
    DIE("getAllocatedMemoryType called on invalid allocation! %p\n", ptr);
    return false;
}
void deallocate(T *ptr, size_t n = 0)
{
    if (ptr != NULL) {
        DBG2("upc_allocator::simple_allocator::deallocate(%p, %lld)\n", ptr, (lld)n);
        uint64_t * _mem = (uint64_t*) ptr;
        _mem--;
        if (*_mem != n && n != 0) DIE("Invalid upc_allocator::simple_allocator::deallocate count.  got %lld expected %lld for %p\n", (lld) n, (lld) *_mem, ptr);
        _mem--;
        if (*_mem == IS_PRIVATE) {
            free(_mem);
        } else if (*_mem == IS_SHARED) {
            upc2obj_upc_free((uint8_t *)(_mem));
        } else {
            DIE("Invalid upc_allocator::simple_allocator::deallocate type.  got %lld expected %lld or %lld for %p\n", (lld) *_mem, (lld) IS_PRIVATE, (lld) IS_SHARED, ptr);
        }
    } else {
        LOGF("deallocate called on NULL ptr\n");
    }
}

template <class U, class ... Args> void construct(U *p, Args && ... args)
{
    new (p)U(std::forward<Args>(args) ...);
}

template <class U> void destroy(U *p)
{
    p->~U();
}

size_type max_size()
{
    size_type maxBlocks = -1 / sizeof(T);

    assert(maxBlocks > 0);
    return maxBlocks;
}

// These functions may be necessary to mix MPI & UPC calls
static void startUPC()
{
    DBG3("simple_allocator::startUPC\n");
    CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
    upc2obj_startUPC();
}
static void endUPC()
{
    DBG3("simple_allocator::endUPC\n");
    upc2obj_endUPC();
}
};

template <typename T, typename U>
inline bool operator ==(const simple_allocator<T>&, const simple_allocator<U>&)
{
    return true;
}

template <typename T, typename U>
inline bool operator !=(const simple_allocator<T>& a, const simple_allocator<U>& b)
{
    return !(a == b);
}

typedef simple_allocator<uint8_t> CharAllocator;

// For boost compatibility
class upc_allocator_upc_alloc_upc_free
{
public:
typedef std::size_t size_type;
typedef std::ptrdiff_t difference_type;

static uint8_t *malloc(const size_type bytes)
{
    return reinterpret_cast<uint8_t *>(getAllocator().allocate(bytes));
}
static void free(uint8_t *block)
{
    getAllocator().deallocate(block);
}
private:
static CharAllocator &getAllocator()
{
    static CharAllocator _;

    return _;
}
};
}; // upc_allocator namespace

#endif /* _UPC_ALLOCATOR_HPP */
