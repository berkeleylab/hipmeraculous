#ifndef UPC_LARGE_ARRAH_H_
#define UPC_LARGE_ARRAY_H_

#include "common.h"
#include "upc_common.h"

// Large arrays as a replacement for upc_all_alloc(large_count, small_size)
#define LARGE_TYPE(largeArray, Type) *LargeArrayType_ ## largeArray

#define LARGE_TYPE2(Type) shared [] Type *

#define LARGE_TYPEDEF(largeArray, Type) typedef LARGE_TYPE2 (Type) LARGE_TYPE (largeArray, Type)

#define LARGE_UPC_ALL_ALLOC_CHK(largeArray, nBlocks, Type) \
    LARGE_TYPE2 (Type) *_LA_cached_ ## largeArray = NULL; \
    int _LA_last_ ## largeArray = MYTHREAD; \
    shared [1] (LARGE_TYPE2(Type)) * _LA__ ## largeArray = NULL; \
    do { \
        UPC_ALL_ALLOC_CHK(_LA_ ## largeArray, THREADS, sizeof(LARGE_TYPE2(Type))); \
        UPC_ALLOC_CHK(_LA_ ## largeArray[MYTHREAD], sizeof(Type) * ((nblocks) + THREADS + 1) / THREADS); \
        _LA_cached_ ## largeArray = _LA_ ## largeArray[MYTHREAD]; \
    } while (0)

#define LARGE_UPC_ALL_FREE_CHK(largeArray) do { \
        UPC_FREE_CHK(_LA_ ## largeArray[MYTHREAD]); \
        UPC_ALL_FREE_CHK(_LA_ ## largeArray); \
} while (0)

// assuming the compiler has not evaluated the assignments out of order of the evaluated expressions...
// use the cached pointer if the thread is the same as the last access, otherwise update the cached values and return the correct value
#define LARGE_GET(largeArray, idx) (_LA_cached_ ## largeArray = (((idx) % THREADS) == _LA_last_ ## largeArray \
                                                                 ? /* cache hit */ _LA_cached_ ## largeArray \
                                                                 : /* cache miss */ (_LA_ ## largeArray[(_LA_last_ ## largeArray = ((idx) % THREADS))])) \
                                    )[(idx) / THREADS]
/* slow but should always work .. good for unit tests */
#define LARGE_GET_NOCACHE(largeArray, idx) _LA_ ## largeArray[(idx) % THREADS][(idx) / THREADS]

#endif
