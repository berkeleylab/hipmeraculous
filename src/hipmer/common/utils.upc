/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 */

#include <string.h>
#include <upc.h>
#include <upc_collective.h>
#include <unistd.h>
#include <assert.h>

#include "utils.h"


int bankers_round(double x)
{
    int rx = x;

    if (x - rx > 0.5) {
        rx++;
    }
    if (x - rx == 0.5 && rx % 2) {
        rx++;
    }
    return rx;
}

// reverse in place
char *reverse(char *s)
{
    int len = strlen(s);

    for (int i = 0; i < len / 2; i++) {
        char tmp = s[len - i - 1];
        s[len - i - 1] = s[i];
        s[i] = tmp;
    }
    return s;
}

int endswith(char *s1, char *s2)
{
    int s1l = strlen(s1);
    int s2l = strlen(s2);

    if (s1l < s2l) {
        return -1;
    }
    return strcmp(s1 + strlen(s1) - strlen(s2), s2);
}

int startswith(char *s1, char *s2)
{
    return strncmp(s1, s2, strlen(s2));
}

void switch_code(char *seq)
{
    char *p = seq;

    while (*p) {
        switch (*p) {
        case 'a': *p = 't'; break;
        case 'c': *p = 'g'; break;
        case 'g': *p = 'c'; break;
        case 't': *p = 'a'; break;
        case 'A': *p = 'T'; break;
        case 'C': *p = 'G'; break;
        case 'G': *p = 'C'; break;
        case 'T': *p = 'A'; break;
        }
        p++;
    }
}

int get_tokens(char *s, char *tokens[], int *tot_len, int ntoks)
{
    char *s_begin = s;
    char *s_end;
    int tok_i = 0;

    *tot_len = 0;
    while ((s_end = strchr(s_begin, '\t')) != NULL) {
        s_end[0] = '\0';
        tokens[tok_i] = s_begin;
        tok_i++;
        *tot_len = s_end - s;
        if (tok_i == ntoks) {
            break;
        }
        s_begin = s_end + 1;
    }
    // get end of line?
    if (tok_i < ntoks && (s_end = strchr(s_begin, '\n')) != NULL) {
        s_end[0] = '\0';
        tokens[tok_i] = s_begin;
        *tot_len = s_end - s;
        tok_i++;
    }
    return tok_i;
}

static const upc_flag_t SYNCS = UPC_IN_MYSYNC | UPC_OUT_MYSYNC;
static const upc_flag_t ALL_SYNCS = UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC;
static const upc_flag_t NO_SYNCS = UPC_IN_NOSYNC | UPC_OUT_NOSYNC;

static shared SharedCharPtr _scatter_array = NULL; // shared variable (i.e. thread 0) to a shared [] char *
static shared SharedCharPtr _gather_array = NULL;  // shared variable (i.e. thread 0) to a shared [] char *

static shared [1] SharedCharPtr _all_void_ptr_result[THREADS];
static shared SharedCharPtr _per_thread_void_ptr[THREADS];
static shared SharedCharPtr _void_ptr_result;

static shared int64_t _all_long_result[THREADS];
static shared [1] int64_t _per_thread_long[THREADS];
static shared int64_t _long_result;

static shared int32_t _all_int_result[THREADS];
static shared [1] int32_t _per_thread_int[THREADS];
static shared int32_t _int_result;

static shared double _all_double_result[THREADS];
static shared [1] double _per_thread_double[THREADS];
static shared double _double_result;

int64_t broadcast_long(int64_t val, int srcThread)
{
    DBG("Entering broadcast_long(%lld, %d)\n", (lld) val, srcThread);
    if (srcThread == MYTHREAD) {
        _long_result = val;
        upc_fence;
    }
    assert(upc_threadof(&_long_result) == 0);
    assert(upc_threadof(&_all_long_result[MYTHREAD]) == MYTHREAD);
    assert(upc_threadof(&_per_thread_long[MYTHREAD]) == MYTHREAD);
    upc_all_broadcast(_all_long_result, &_long_result, sizeof(int64_t), ALL_SYNCS);
    return _all_long_result[MYTHREAD];
}

int64_t reduce_long(int64_t myval, upc_op_t op, enum reduce_dest dst)
{
    assert(upc_threadof(&_long_result) == 0);
    assert(upc_threadof(&_all_long_result[MYTHREAD]) == MYTHREAD);
    assert(upc_threadof(&_per_thread_long[MYTHREAD]) == MYTHREAD);
    _per_thread_long[MYTHREAD] = myval;
    long result = 0;
    if (dst == ALL_DEST) {
#ifdef __BERKELEY_UPC__
        bupc_all_reduce_allL(_all_long_result, _per_thread_long, op, THREADS, 1, NULL, SYNCS);
        result = _all_long_result[MYTHREAD];
        assert(upc_threadof(&_all_long_result[MYTHREAD]) == MYTHREAD);
#ifdef DEBUG
        upc_barrier;
        assert(result == _all_long_result[MYTHREAD == THREADS - 1 ? 0 : MYTHREAD + 1]);
#endif
#else
        // call self in SINGLE_DEST, then broadcast
        result = reduce_long(myval, op, SINGLE_DEST);
        result = broadcast_long(result, 0);
#endif
    } else {
        upc_all_reduceL(&_long_result, _per_thread_long, op, THREADS, 1, NULL, ALL_SYNCS);
        if (!MYTHREAD) {
            assert(upc_threadof(&_long_result) == MYTHREAD);
            result = _long_result;
        }
    }
    return result;
}

int64_t reduce_prefix_long(int64_t myval, upc_op_t op, int64_t *opt_global_total)
{
    int64_t result;

    assert(upc_threadof(&_all_long_result[MYTHREAD]) == MYTHREAD);
    assert(upc_threadof(&_per_thread_long[MYTHREAD]) == MYTHREAD);
    _all_long_result[MYTHREAD] = 0;
    _per_thread_long[MYTHREAD] = myval;
    upc_all_prefix_reduceL(_all_long_result, _per_thread_long, op, THREADS, 1, NULL, SYNCS);
    result = _all_long_result[MYTHREAD];
    _all_long_result[MYTHREAD] = 0;
    if (opt_global_total != NULL) {
        *opt_global_total = broadcast_long(result, THREADS-1);
    }
    //LOGF("reduce_prefix_long(%lld): %lld (total %lld)\n", (lld) myval, (lld) result, (lld) (MYTHREAD==0?*global:-1));
    return result;
}

// gathers blockSize chunks of private data from every thread to a new allocation of private memory on the gatherThread
int64_t *gather_long(const int64_t *mydata, int countPerThread, int gatherThread)
{
    upc_barrier;
    int blockSize = sizeof(int64_t) * countPerThread;
    if (MYTHREAD == gatherThread) {
        _gather_array = NULL; // it may not be initialized on some platforms
        assert(upc_threadof(&_gather_array) == 0);
        UPC_ALLOC_CHK(_gather_array, blockSize * THREADS);
        assert(upc_threadof(_gather_array) == MYTHREAD);
    }
    shared [1] int64_t *src = NULL, *freesrc = NULL;

    if (countPerThread == 1) {
        src = _all_long_result;
    } else {
        UPC_ALL_ALLOC_CHK(src, THREADS, blockSize);
        freesrc = src;
    }
    assert(upc_threadof( src + MYTHREAD ) == MYTHREAD);
    memcpy((char*) (src + MYTHREAD), mydata, blockSize);
    upc_barrier;
    assert(upc_threadof(_gather_array) == gatherThread);
    upc_all_gather(_gather_array, src, blockSize, UPC_IN_NOSYNC | UPC_OUT_NOSYNC);
    upc_barrier;
    void *gathered = NULL; 
    if (MYTHREAD == gatherThread) {
        gathered = malloc_chk(blockSize * THREADS);
        assert(upc_threadof(_gather_array) == MYTHREAD);
        memcpy(gathered, (char*) _gather_array, blockSize * THREADS);
        UPC_FREE_CHK(_gather_array);
        assert(_gather_array == NULL);
    }
    if (freesrc) UPC_ALL_FREE_CHK(freesrc);
    return gathered;
}

// scatters an array int64_t of private data from scatterThread to a every thread
int64_t *scatter_long(const int64_t *allvals, int countPerThread, int scatterThread)
{
    upc_barrier;
    int blockSize = sizeof(int64_t) * countPerThread;
    if (MYTHREAD == scatterThread) {
        _scatter_array = NULL; // it may not be initialized on some platforms
        assert(upc_threadof(&_scatter_array) == 0);
        UPC_ALLOC_CHK(_scatter_array, blockSize * THREADS);
        assert(upc_threadof(_scatter_array) == MYTHREAD);
        memcpy((char *)_scatter_array, allvals, blockSize * THREADS);
    }
    shared [1] int64_t *dest = NULL, *freedest = NULL;
    if (countPerThread == 1) {
        dest = _all_long_result;
    } else {
        UPC_ALL_ALLOC_CHK(dest, THREADS * countPerThread, sizeof(int64_t));
        freedest = dest;
    }
    assert(upc_threadof( dest + MYTHREAD ) == MYTHREAD);
    upc_barrier;
    assert(upc_threadof(_scatter_array) == scatterThread);
    upc_all_scatter(dest, _scatter_array, blockSize, UPC_IN_NOSYNC | UPC_OUT_NOSYNC);
    upc_barrier;
    void * scattered = malloc_chk(blockSize);
    assert(upc_threadof( dest + MYTHREAD ) == MYTHREAD);
    memcpy(scattered, (char*) (dest + MYTHREAD), blockSize);
    if (MYTHREAD == scatterThread) {
        UPC_FREE_CHK(_scatter_array);
    }
    if (freedest) UPC_ALL_FREE_CHK(freedest);
    return scattered;
}

int32_t broadcast_int(int32_t val, int srcThread)
{
    DBG("Entering broadcast_int(%d, %d)\n", val, srcThread);
    if (srcThread == MYTHREAD) {
        _int_result = val;
        upc_fence;
    }
    assert(upc_threadof(&_int_result) == 0);
    assert(upc_threadof(&_all_int_result[MYTHREAD]) == MYTHREAD);
    assert(upc_threadof(&_per_thread_int[MYTHREAD]) == MYTHREAD);
    upc_all_broadcast(_all_int_result, &_int_result, sizeof(int32_t), ALL_SYNCS);
    assert(upc_threadof(&_all_int_result[MYTHREAD]) == MYTHREAD);
    return _all_int_result[MYTHREAD];
}

int32_t reduce_int(int32_t myval, upc_op_t op, enum reduce_dest dst)
{
    _per_thread_int[MYTHREAD] = myval;
    int32_t result = 0;
    if (dst == ALL_DEST) {
#ifdef __BERKELEY_UPC__
        bupc_all_reduce_allI(_all_int_result, _per_thread_int, op, THREADS, 1, NULL, SYNCS);
        result = _all_int_result[MYTHREAD];
#else
        // call self in SINGLE_DEST, then broadcast
        result = reduce_int(myval, op, SINGLE_DEST);
        result = broadcast_int(result, 0);
#endif
    } else {
        upc_all_reduceI(&_int_result, _per_thread_int, op, THREADS, 1, NULL, ALL_SYNCS);
        if (!MYTHREAD) {
            result = _int_result;
        }
    }
    return result;
}

double broadcast_double(double val, int srcThread)
{
    DBG("Entering broadcast_double(%f, %d)\n", val, srcThread);
    if (srcThread == MYTHREAD) {
        _double_result = val;
        upc_fence;
    }
    assert(upc_threadof(&_double_result) == 0);
    upc_all_broadcast(_all_double_result, &_double_result, sizeof(double), ALL_SYNCS);
    assert(upc_threadof(&_all_double_result[MYTHREAD]) == MYTHREAD);
    return _all_double_result[MYTHREAD];
}

double reduce_double(double myval, upc_op_t op, enum reduce_dest dst)
{
    if (upc_threadof( &_per_thread_double[MYTHREAD] ) != MYTHREAD) {
        DIE("Invalid assumption!\n");
    }
    _per_thread_double[MYTHREAD] = myval;
    upc_fence;
    double result = 0.0;
    if (dst == ALL_DEST) {
#ifdef __BERKELEY_UPC__
        bupc_all_reduce_allD(_all_double_result, _per_thread_double, op, THREADS, 1, NULL, SYNCS);
        result = _all_double_result[MYTHREAD];
#else
        // call self in SINGLE_DEST, then broadcast
        result = reduce_double(myval, op, SINGLE_DEST);
        result = broadcast_double(result, 0);
#endif
    } else {
        upc_all_reduceD(&_double_result, _per_thread_double, op, THREADS, 1, NULL, ALL_SYNCS);
        if (!MYTHREAD) {
            result = _double_result;
        }
    }
    return result;
}

static shared double _double_result1;
static shared double _double_result2;
static shared double _double_result3;
// returns the load balance (avg/max) and average, min & max of a number to thread 0
double avg_min_max(double myVal, double *avg, double *min, double *max) {
    double balance = -1.0;
    _per_thread_double[MYTHREAD] = myVal;
    upc_barrier;
    upc_all_reduceD(&_double_result1, _per_thread_double, UPC_ADD, THREADS, 1, NULL, NO_SYNCS);
    upc_all_reduceD(&_double_result2, _per_thread_double, UPC_MIN, THREADS, 1, NULL, NO_SYNCS);
    upc_all_reduceD(&_double_result3, _per_thread_double, UPC_MAX, THREADS, 1, NULL, NO_SYNCS);
    upc_barrier;
    if (!MYTHREAD) {
        *avg = _double_result1 / THREADS;
        *min = _double_result2;
        *max = _double_result3;
        if (*max != 0.0) 
            balance = *avg / *max;
        else
            balance = 1.0;
    }
    return balance;
}


#define MAX_REDUCE_BLOCK 200

typedef struct {
    double vals[MAX_REDUCE_BLOCK];
} all_block_vals_t;

void reduce_block(double *myvals, int num_vals, double *min_vals, double *max_vals, double *tot_vals)
{
    if (num_vals > MAX_REDUCE_BLOCK) {
        DIE("Too many values for the reduce_block call: %d > %d\n", num_vals, MAX_REDUCE_BLOCK);
    }

    shared all_block_vals_t *per_thread_vals = NULL;
    UPC_ALL_ALLOC_CHK(per_thread_vals, THREADS, sizeof(all_block_vals_t));

    for (int i = 0; i < num_vals; i++) {
        per_thread_vals[MYTHREAD].vals[i] = myvals[i];
    }
    upc_barrier;
    if (!MYTHREAD) {
        for (int i = 0; i < num_vals; i++) {
            if (min_vals) {
                min_vals[i] = 1000000000000;
            }
            if (max_vals) {
                max_vals[i] = 0;
            }
            if (tot_vals) {
                tot_vals[i] = 0;
            }
            for (int thread = 0; thread < THREADS; thread++) {
                double val = per_thread_vals[thread].vals[i];
                if (tot_vals) {
                    tot_vals[i] += val;
                }
                if (max_vals && val > max_vals[i]) {
                    max_vals[i] = val;
                }
                if (min_vals && val < min_vals[i]) {
                    min_vals[i] = val;
                }
            }
        }
    }
    upc_barrier;
    UPC_ALL_FREE_CHK(per_thread_vals);
}

#define BCAST_FILE_BYTES 4096


Buffer broadcast_file(const char *fname)
{
    LOGF("Entering broadcast_file for %s\n", fname);
    Buffer myBuffer = NULL, srcBuffer = NULL;
    int64_t size = 0, bytes;
    if (MYTHREAD == 0) {
        size = get_file_size(fname);
        srcBuffer = initBuffer(size);
        FILE *f = fopen_chk(fname, "r");
        if ( (bytes = readFileBuffer(srcBuffer, f, 0, size)) != size) {
            DIE("Failed to read the entire contents of %s -- %lld of %lld read: %s\n", fname, (lld) bytes, (lld) size, strerror(errno));
        }
        fclose_track(f);
        LOGF("broadcast_file(%s) of %lld bytes\n", fname, (lld) size);
    }
    myBuffer = broadcast_Buffer(srcBuffer, 0);
#ifdef DEBUG
    // verify that the broadcasted buffer is the same size as the file
    int64_t observedFileSize = get_file_size(fname);
    if (getReadLengthBuffer(myBuffer) != observedFileSize) {
        DIE("Error in reading or observing file %s!  observedFileSize=%lld but myBuffer readLength is %lld!\n", fname, (lld) observedFileSize, (lld) getReadLengthBuffer(myBuffer));
    }
    // verify that the broadcasted buffer is the same contents as the file
    Buffer verifyBuffer = initBuffer(observedFileSize);
    FILE *f = fopen_chk(fname, "r");
    if ( (bytes = readFileBuffer(verifyBuffer, f, 0, observedFileSize)) != observedFileSize) {
        DIE("Verify of read. Failed to read the entire contents of %s -- %lld of %lld read: %s\n", fname, (lld) bytes, (lld) observedFileSize, strerror(errno));
    }
    fclose_track(f);
    if (strncmp(getCurBuffer(verifyBuffer), getCurBuffer(myBuffer), observedFileSize) != 0) {
        DIE("Verify failed to read the same bytes as was broadcasted by thread 0!\n");
    }
    freeBuffer(verifyBuffer);
#endif
    return myBuffer;
}

Buffer broadcast_Buffer(Buffer srcBuffer, int srcThread)
{
    LOGF("Entering broadcast_buffer from %d\n", srcThread);
    Buffer myBuffer = NULL;
    uint32_t size = 0, adler = 0;
    if (MYTHREAD == srcThread) {
        size = getLengthBuffer(srcBuffer);
        adler = getAdler32Buffer(srcBuffer);
        LOGF("Got adler=%u size=%u Buffer=%.*s\n", adler, size, 80, getStartBuffer(srcBuffer));
    }
    uint64_t adlerSize = (((uint64_t) adler)<<32) | size;
    int64_t _l = (int64_t) adlerSize;
    adlerSize = (uint64_t) broadcast_long(_l, srcThread);
    adler = (uint32_t) (adlerSize >> 32);
    size = (uint32_t) (adlerSize & 0xffffffff);

    LOGF("broadcast_Buffer of %lld bytes from thread %d adler=%u\n", (lld) size, srcThread, adler);
    int bcastBlock = BCAST_FILE_BYTES/sizeof(int64_t);
    shared [ 1 ] int64_t *buffers = NULL;
    shared [] int64_t *src = NULL;
    UPC_ALL_ALLOC_CHK(buffers, THREADS * 2 * bcastBlock, sizeof(int64_t));  // THREADS * 2 so src is on thread srcThread
    src = (shared [] int64_t *) &buffers[ THREADS*bcastBlock + srcThread];
    assert(upc_threadof(src) == srcThread);
    myBuffer = initBuffer(size+1);
    int64_t bytes = 0;
    while (bytes < size) {
        int64_t bcastBytes = size - bytes;
        if (bcastBytes > BCAST_FILE_BYTES) bcastBytes = BCAST_FILE_BYTES;
        if (MYTHREAD == srcThread) {
            readBuffer(srcBuffer, (char*) src, bcastBytes);
        }
        upc_barrier;
        assert( upc_threadof( &buffers[MYTHREAD] ) == MYTHREAD );
        upc_all_broadcast( buffers, src, bcastBytes, UPC_IN_NOSYNC | UPC_OUT_NOSYNC );
        upc_barrier;
        assert( upc_threadof( &buffers[MYTHREAD] ) == MYTHREAD );
        writeBuffer(myBuffer, (char*) &buffers[MYTHREAD], bcastBytes);
        bytes += bcastBytes;
    }
    assert(getLengthBuffer(myBuffer) == size);
    UPC_ALL_FREE_CHK(buffers);
    if (MYTHREAD == srcThread) {
        // double check contents
        if (memcmp(getStartBuffer(srcBuffer), getStartBuffer(myBuffer), size) != 0) {
            DIE("broadcast_Buffer did not work for - %lld bytes\n", (lld) size);
        }
        freeBuffer(srcBuffer);
    }
    if (size && getAdler32Buffer(myBuffer) != adler) {
        LOGF("Got adler=%u size=%u Buffer=%.*s\n", getAdler32Buffer(myBuffer), size, 80, getStartBuffer(myBuffer));
        DIE("Adler checksum failed after broadcast! expected %u got %u. size %d vs %d\n", adler, getAdler32Buffer(myBuffer), size, getLengthBuffer(myBuffer));
    }
    return myBuffer;
}

int64_t broadcast_file_size(const char *filename) {
    int64_t fileSize = -1;
    if (MYTHREAD == 0) {
         fileSize = get_file_size(filename);
    }
    fileSize = broadcast_long(fileSize, 0);
#ifdef DEBUG
    int64_t observedFileSize = get_file_size(filename);
    if (fileSize != observedFileSize) {
        DIE("Error discrepancy in thread observations of file size of %s!  observedFileSize=%lld but broadcastedFileSize=%lld!\n", filename, (lld) observedFileSize, (lld) fileSize);
    }
#endif
    return fileSize;
}

int all_does_file_exist(const char *fname) {
    int exists = 0;
    if (!MYTHREAD) {
        exists = does_file_exist(fname);
    }
    return broadcast_int(exists, 0);
}

LazyAtomic initLazyAtomic(int stepSize)
{
    LazyAtomic la = calloc_chk(1, sizeof(_LazyAtomic));

    UPC_ALL_ALLOC_CHK(la->global, 1, sizeof(int64_t));
    if (stepSize <= 0) {
        stepSize = 1;
    }
    // initialize first step
    if (MYTHREAD == 0) {
        *(la->global) = stepSize * THREADS;
    }
    la->myCache = MYTHREAD * stepSize;
    la->stepSize = stepSize;
    la->myStep = 0;
    upc_barrier;
    return la;
}

int64_t getLazyAtomic(LazyAtomic la)
{
    assert(la != NULL);
    int64_t myVal = 0;
    assert(la->myStep < la->stepSize);
    if (la->myStep != 0) {
        if (la->myStep != la->stepSize) {
            LOGF("faddLazyAtomic will have a hole from %lld to %lld.  myVal=%lld, myGlobal=%lld, myStep=%lld\n", (lld)la->myCache + la->myStep, (lld)la->myCache + la->stepSize, (lld)myVal, (lld)la->myCache, (lld)la->myStep);
        }
        UPC_ATOMIC_FADD_I64_RELAXED(&(la->myCache), la->global, la->myStep);
        la->myStep = 0;
    }
    return la->myCache + la->myStep;
}

int64_t faddLazyAtomic(LazyAtomic la, int64_t myVal)
{
    assert(myVal < la->stepSize);
    if (la->myStep + myVal > la->stepSize) {
        getLazyAtomic(la);
        assert(la->stepSize == 0);
    }
    la->myStep += myVal;
    if (la->myStep == la->stepSize) {
        // increment the global
        return getLazyAtomic(la);
    } else {
        // just use my block
        return la->myCache + la->myStep;
    }
}

int64_t freeLazyAtomic(LazyAtomic *lazy)
{
    assert(lazy != NULL);
    assert(*lazy != NULL);
    getLazyAtomic(*lazy); // flush any existing increments -- there may be holes towards the end
    upc_barrier;
    int64_t total;
    UPC_ATOMIC_GET_I64(&total, (*lazy)->global);
    UPC_ALL_FREE_CHK((*lazy)->global);
    free_chk(*lazy);
    *lazy = NULL;
    return total;
}


void *upc_memget_alloc(shared const void *src, size_t n)
{
    assert(n > 0);
    assert(src != NULL);
    void *dst = malloc_chk(n);
    upc_memget(dst, src, n);
    return dst;
}

#define upc_memget_free(p) _upc_memget_free(&p)
void _upc_memget_free(void **ptr)
{
    _free_chk(ptr);
}

/*
 * int pin_thread(pid_t pid, int cid)
 * {
 *  cpu_set_t cpu_set;
 *  CPU_ZERO(&cpu_set);
 *  CPU_SET(cid, &cpu_set);
 *  if (sched_setaffinity(pid, sizeof(cpu_set), &cpu_set) == -1) {
 *      if (errno == 3)
 *          WARN("%s, pid: %d", strerror(errno), pid);
 *      return -1;
 *  }
 *  return 0;
 * }
 */
void report_memory_allocations(char *buffer)
{
    int64_t m, ua, utua, uaa;

    if (_sv != NULL) {
        m = reduce_long(MYSV.outstandingMallocs, UPC_ADD, SINGLE_DEST);
        ua = reduce_long(MYSV.outstandingUPCAllocs, UPC_ADD, SINGLE_DEST);
        utua = reduce_long(MYSV.untracked_outstandingUPCAllocs, UPC_ADD, SINGLE_DEST);
        uaa = reduce_long(MYSV.outstandingUPCAllAllocs, UPC_ADD, SINGLE_DEST);
        DBG("my memory allocations: mallocs=%lld upc_alloc=%lld, untracked_upc_alloc=%lld upc_all_alloc=%lld\n", (lld)MYSV.outstandingMallocs, (lld)MYSV.outstandingUPCAllocs, (lld)MYSV.untracked_outstandingUPCAllocs, (lld)MYSV.outstandingUPCAllAllocs);
        sprintf(buffer, "outstanding memory allocations: mallocs=%lld upc_alloc=%lld untracked_upc_alloc=%lld upc_all_alloc=%lld", (lld)m, (lld)ua, (lld)utua, (lld)uaa);
    }
}
