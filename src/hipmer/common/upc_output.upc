#include <upc.h>
#include <upc_collective.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>

#include "Buffer.h"
#include "timers.h"
#include "utils.h"
#include "upc_output.h"
#include "SharedBuffer.h"

#define UPC_ALIGN_WRITES


// clears lustre caches that interfere with malloc() INC0144398
void clear_my_lustre_cache() {
  const char *drop_caches = "/proc/fs/lustre/ldlm/drop_caches";
  if ( does_file_exist(drop_caches) ) {
    LOGF("Clearing lustre caches\n");
    FILE *f = fopen_chk(drop_caches, "w");
    fprintf(f, "1\n");
    fclose(f);
  }
}

void clear_lustre_caches() {
  if (MYTHREAD % MYSV.cores_per_node == 0) {
    clear_my_lustre_cache();
  }
  UPC_LOGGED_BARRIER;
}

void writeIndexFile(const char *ourFileName, int64_t myOrigOffset, int64_t myOrigSize, int64_t myUncompressedSize)
{
    IndexElement idx = {.offset = myOrigOffset, .size = myOrigSize, .uncompressedSize = myUncompressedSize};
    LOGF("my index: offset=%lld size=%lld uncompressedSize=%lld\n", (lld) myOrigOffset, (lld) myOrigSize, (lld) myUncompressedSize);
    if (sizeof(IndexElement) % sizeof(int64_t) != 0) DIE("Invalid assumption: %lld vs %lld\n", (lld) sizeof(IndexElement), (lld) sizeof(int64_t));
    int64_t *indexes = gather_long((int64_t*) &idx, sizeof(IndexElement)/sizeof(int64_t), THREADS - 1);
    if (MYTHREAD == THREADS - 1) {
        assert(indexes != NULL);
        char indexFileName[MAX_FILE_PATH];
        sprintf(indexFileName, "%s.idx", ourFileName);
        if (does_file_exist(indexFileName)) {
            // move the old index if it exists
            char oldindexFileName[MAX_FILE_PATH+10];
            sprintf(oldindexFileName, "%s.old", indexFileName);
            if (rename(indexFileName, oldindexFileName) != 0) {
                WARN("Could not rename existing index %s to %s! .. continuing attempting to overwrite it. errno=%d %s\n", indexFileName, oldindexFileName, errno, strerror(errno));
            }
        }
        FILE *idxFile = fopen_chk(indexFileName, "w");
        fwrite_chk(indexes, sizeof(IndexElement), THREADS, idxFile);
        fclose_track(idxFile);
    }
    UPC_LOGGED_BARRIER;
    if (indexes) {
#ifdef DEBUG
        IndexElement *idx2 = (IndexElement*) indexes;
        for(int i = 0; i < THREADS; i++) {
            DBG("Index %d: offset=%lld size=%lld uncompressedSize=%lld\n", i, (lld) idx2[i].offset, (lld) idx2[i].size, (lld) idx2[i].uncompressedSize);
        }
#endif
        free_chk(indexes);
    }
}

// contents of myBuffer will now be changed so all offsets are aligned to alignSize
// and all sizes but the last are either 0 or a multiple of alignSize 
// returns the original globalOffset
int64_t alignWriteBuffers(Buffer myBuffer, shared [1] int64_t *ourSizes, int64_t alignSize)
{
    double start = now();
    assert(ourSizes != NULL);
    int64_t mySize = getReadLengthBuffer(myBuffer), totalSize = 0, alignedOffset = 0;
    AllSharedBufferPtr ourBuffers = NULL;

    // myBuffer holds all the data

    UPC_ALL_ALLOC_CHK(ourBuffers, THREADS, sizeof(SharedBuffer));
    assert(getReadLengthBuffer(myBuffer) == mySize);
    assert(ourBuffers[MYTHREAD].str == NULL);

    // allocate enough to align to end of block boundaries
    ourBuffers[MYTHREAD] = allocSharedBuffer(alignSize);

    ourSizes[MYTHREAD] = mySize;
    int64_t myGlobalOffset = reduce_prefix_long(mySize, UPC_ADD, &totalSize) - mySize;
    UPC_LOGGED_BARRIER;
    LOGF("alignWriteBuffer(mySize=%lld): myGlobalOffset=%lld, totalSize=%lld\n", (lld) mySize, (lld) myGlobalOffset, (lld) totalSize);
    if (myGlobalOffset % alignSize > 0) {
        if (ourBuffers[MYTHREAD].size < alignSize - (myGlobalOffset % alignSize)) {
            DIE("ourBuffers has insufficient capacity for this alignment: len=%lld size=%lld myGlobalOffset=%lld remainderToAlign=%lld alignSize=%lld\n", (lld) ourBuffers[MYTHREAD].len, (lld) ourBuffers[MYTHREAD].size, (lld) myGlobalOffset, (lld) ourBuffers[MYTHREAD].len + alignSize - (myGlobalOffset % alignSize), (lld) alignSize);
        }
    }

    int64_t alignedStart = myGlobalOffset % alignSize;
    if (alignedStart != 0) {
        alignedOffset = alignSize - alignedStart;
        if (alignedOffset > mySize) {
            alignedOffset = mySize;
        }
    }
    if (MYTHREAD > 0 && mySize > 0) {
        // now ensure bytes are aligned by block boundaries
        if (alignedOffset > 0) {

            // pitch to next lower aligned thread -- that spans a block boundary (some active threads may not in the case of severe load imbalance)

            int destThread = MYTHREAD;
            int64_t destSize = 0;
            int64_t destGlobalOffset = myGlobalOffset;
            int64_t destOffset = 0;
            int destCrossesBoundary;
            int64_t destAlignedStart;
       
            do {
                destThread--;
                destOffset += destSize;
                destSize = ourSizes[destThread];
                destGlobalOffset -= destSize;
                destAlignedStart = destGlobalOffset % alignSize;
                destCrossesBoundary = (destThread == 0) | (destAlignedStart == 0 && destSize > 0) | ((destGlobalOffset / alignSize) < ((destGlobalOffset + destSize - 1) / alignSize));
                LOGF("Testing destThread=%d crossesBoundary=%d destSize=%lld destGlobalOffset=%lld destOffset=%lld destAlignedStart=%lld\n", destThread, destCrossesBoundary, (lld) destSize, (lld) destGlobalOffset, (lld) destOffset, (lld) destAlignedStart);
            } while(destThread > 0 && !destCrossesBoundary);

            // ourBuffers are just the remaining up to alignSize
            SharedBuffer destBuffer = ourBuffers[destThread];

            if (getReadLengthBuffer(myBuffer) < alignedOffset) {
                DIE("Invalid assumption readLength=%lld but alignedOffset=%lld\n", (lld) getReadLengthBuffer(myBuffer), (lld) alignedOffset);
            }
            if (destOffset + alignedOffset > destBuffer.size) {
                DIE("Can not append %lld to ourBuffers[%d] because destOffset=%lld + alignedOffset=%lld > size=%lld\n", (lld) alignedOffset, destThread, (lld) destOffset, (lld) alignedOffset, (lld) destBuffer.size);
            }
            upc_memput(ourBuffers[destThread].str + destOffset, getCurBuffer(myBuffer), alignedOffset);
            moveCurBuffer(myBuffer, alignedOffset); // adjust pos offset

            int32_t newDestLen;
            UPC_ATOMIC_FADD_I32(&newDestLen, &(ourBuffers[destThread].len), alignedOffset);
            newDestLen += alignedOffset;
            LOGF("Sending %lld aligned bytes to Th%d at destOffset=%lld globalOffset=%lld.  newDestLen=%d newMySize=%lld\n", (lld) alignedOffset, destThread, (lld) destOffset, (lld) destGlobalOffset + destOffset, newDestLen, (lld) getReadLengthBuffer(myBuffer));
        }
    }
    UPC_LOGGED_BARRIER;

    // all alignment bytes are copied
    // update ourSizes
    // ourBuffers[] have just the aligned bytes that were caught
    ourSizes[MYTHREAD] = ourSizes[MYTHREAD] - alignedOffset + ourBuffers[MYTHREAD].len;

    // ourBuffers[] have just the aligned bytes that were caught
    // append the caught alignment bytes into myBuffer.  myBuffer read start already adjusted above
    assert(ourBuffers[MYTHREAD].str);
    if (MYTHREAD > 0 && mySize == 0 && ourBuffers[MYTHREAD].len > 0) WARN("mySize == 0 but received data from other threads! (%lld): myGlobalOffset=%lld\n", (lld) ourBuffers[MYTHREAD].len, (lld) myGlobalOffset);
    if (ourBuffers[MYTHREAD].len) {
        int catchSize = ourBuffers[MYTHREAD].len;
        if (catchSize > ourBuffers[MYTHREAD].size) {
            DIE("overflow in ourBuffers! len=%d size=%d\n", ourBuffers[MYTHREAD].len, ourBuffers[MYTHREAD].size);
        }
        memcpy(appendBuffer(myBuffer, catchSize), (char*) ourBuffers[MYTHREAD].str, catchSize);
        LOGF("appended %lld bytes: mySize=%lld pos=%lld newSize=%lld\n", (lld) catchSize, (lld) ourSizes[MYTHREAD], (lld) getPosBuffer(myBuffer), (lld) getReadLengthBuffer(myBuffer));
    }

    if (ourSizes[MYTHREAD] != getReadLengthBuffer(myBuffer)) {
        DIE("Failed to align bytes to boundary! ourSizes=%lld readLength=%lld\n", (lld) ourSizes[MYTHREAD], (lld) getReadLengthBuffer(myBuffer));
    }

    // free ourBuffers -- there is no longer any data in them
    if (ourBuffers[MYTHREAD].size) {
         freeSharedBuffer(ourBuffers[MYTHREAD]);
    }

    UPC_ALL_FREE_CHK(ourBuffers);

#ifdef DEBUG
    // sanity check
    if (ourSizes[MYTHREAD] != getReadLengthBuffer(myBuffer)) DIE("ourSizes=%lld buffer=%lld\n", (lld) ourSizes[MYTHREAD], (lld) getReadLengthBuffer(myBuffer));
    int64_t newTotal = 0;
    int64_t newOffset = reduce_prefix_long(ourSizes[MYTHREAD], UPC_ADD, &newTotal) - ourSizes[MYTHREAD];
    if (ourSizes[MYTHREAD] > 0 && newOffset != myGlobalOffset + alignedOffset) DIE("newOffset=%lld myGlobalOffset=%lld, alignedOffset=%lld\n", (lld) newOffset, (lld) myGlobalOffset, (lld) alignedOffset);
    if (newTotal != totalSize) DIE("newTotal=%lld originaly %lld\n", (lld) newTotal, (lld) totalSize);
#endif

    return myGlobalOffset + alignedOffset;
}

// contents of myBuffer will now be changed -- most likely 0 bytes for most threads
// returns the original globalOffset
int64_t consolidateSmallBuffers(Buffer myBuffer, shared [1] int64_t *ourSizes, int targetSize)
{
    double start = now();
    assert(ourSizes != NULL);
    int64_t mySize = getReadLengthBuffer(myBuffer);
    int64_t catchSize = mySize;
    ourSizes[MYTHREAD] = mySize;
    UPC_LOGGED_BARRIER;

    int64_t total = 0;
    int64_t originalSize = mySize;
    int64_t ourLargest = reduce_long(mySize, UPC_MAX, ALL_DEST);
    int64_t myGlobalOffset = reduce_prefix_long(mySize, UPC_ADD, &total) - mySize;
    
    // reduce the number of threads that are writing to files if small amounts of data are being written
    int activeThreads = THREADS;
    int modulus = 1;
    int myCatcher = MYTHREAD;
    AllSharedBufferPtr ourBuffers = NULL;
    UPC_ALL_ALLOC_CHK(ourBuffers, THREADS, sizeof(SharedBuffer));

    while (activeThreads > 1 && total / activeThreads < targetSize) {
        activeThreads /= 2;
    }

    // check for load imabalance, abort consolidation and just align if imbalance is high
    double imbalance = (double) THREADS * ourLargest / total;
    int64_t alignBlockSize = WRITE_BLOCK_SIZE;
    if (activeThreads != THREADS && activeThreads != 1 && imbalance > 1.1) {
        alignBlockSize = targetSize;
        LOGF("Load imbalance is too high %0.2f (max=%lld, total=%lld), skipping consolidation step to %d threads\n", imbalance, (lld) ourLargest, (lld) total, activeThreads);
        activeThreads = THREADS;
    }

    if (activeThreads != THREADS) {
        LOGF("Consolidating with %d activeThreads (out of %d) %lld bytes\n", activeThreads, THREADS, (lld)total);
        // divide the threads into pitchers (inactive) and catchers (active)
        // send all pitcher data to catcher (in rank order)
        // then have pitchers remain idle while catchers write the file

        // some threads will not participate in opening and writing data, so will send data to other threads
        modulus = (THREADS + activeThreads - 1) / activeThreads;
        assert(modulus >= 1 && modulus <= THREADS);
        int myModulo = MYTHREAD % modulus;

        catchSize = 0;
        int64_t myPitchedOffset = 0;
        myCatcher = MYTHREAD - myModulo;
        if (MYTHREAD == myCatcher) {
            // catcher
            assert(MYTHREAD == myCatcher);
            for (int i = MYTHREAD; i < THREADS && i < MYTHREAD + modulus; i++) {
                catchSize += ourSizes[i];
            }
            size_t allocSize = catchSize + 8 + (8 - catchSize % 8);
            ourBuffers[MYTHREAD] = allocSharedBuffer(allocSize);
            LOGF("catching %lld bytes %lld bytes of mine up to %d threads from %d to %d (allocated %lld)\n", (lld) catchSize, (lld) mySize, modulus, MYTHREAD, MYTHREAD+modulus-1, (lld) allocSize);

            // replace myBuffer to be this new allocation
            assert(upc_threadof(ourBuffers[MYTHREAD].str) == MYTHREAD);
            assert(myPitchedOffset == 0);
            char *myCatcherBuffer = (char *)ourBuffers[MYTHREAD].str;
            // copy my data, then replace myBuffer with the (larger) shared allocation
            if (mySize > 0) {
                assert(catchSize >= mySize);
                memcpy(myCatcherBuffer, getCurBuffer(myBuffer), mySize);
                UPC_ATOMIC_FADD_I32_RELAXED(NULL, &(ourBuffers[MYTHREAD].len), mySize);
                resetBuffer(myBuffer);
            }
            if (catchSize > 0) {
                assert(ourBuffers[MYTHREAD].str);
                assert(ourBuffers[MYTHREAD].len <= ourBuffers[MYTHREAD].size);
                assert(ourBuffers[MYTHREAD].size >= catchSize);
            } else {
                assert(mySize == 0);
            }
        } else {
            // pitcher
            for (int i = myCatcher; i < MYTHREAD; i++) {
                myPitchedOffset += ourSizes[i];
            }
            assert(ourBuffers[MYTHREAD].str == NULL);
            assert(ourBuffers[MYTHREAD].size == 0);
            assert(catchSize == 0);
            LOGF("pitching %lld bytes to %d at %lld\n", (lld) mySize, myCatcher, (lld) myPitchedOffset);
        }
        assert(ourSizes[MYTHREAD] == mySize);
        UPC_LOGGED_BARRIER;
        // now fix ourSizes to reflect the new reality
        ourSizes[MYTHREAD] = catchSize;
        upc_fence;

        if (ourBuffers[myCatcher].str == NULL) {
            DIE("Could not send my data to my catcher!\n");
        }
        if (MYTHREAD != myCatcher) {
            DBG("Pitching %lld bytes to offset %lld for my catcher %d: %.*s...\n", (lld)mySize, (lld)myPitchedOffset, myCatcher, (int)(mySize < 10 ? mySize : 10), getCurBuffer(myBuffer));
            assert(upc_threadof(ourBuffers[myCatcher].str) == myCatcher);
            if (mySize > 0) {
                upc_memput(ourBuffers[myCatcher].str + myPitchedOffset, getCurBuffer(myBuffer), mySize);
                UPC_ATOMIC_FADD_I32_RELAXED(NULL, &(ourBuffers[myCatcher].len), mySize);
            }
            // no more myBuffer
        }
        // all data in myBuffer has been pitched or caught to shared memory
        resetBuffer(myBuffer);

    }
    UPC_LOGGED_BARRIER;

    if (ourBuffers[MYTHREAD].str && catchSize != ourBuffers[MYTHREAD].len) {
        LOGF("catchSize changed from %lld to %lld (diff %lld)\n", (lld) catchSize, (lld) ourBuffers[MYTHREAD].len, ourBuffers[MYTHREAD].len - catchSize);
        catchSize = ourBuffers[MYTHREAD].len;
    }
    if (ourBuffers[MYTHREAD].str) {
        // copy the caught bytes back into myBuffer
        resetBuffer(myBuffer);
        if (catchSize) {
            writeBuffer(myBuffer, (char*) ourBuffers[MYTHREAD].str, catchSize);
            LOGF("Caught catchSize=%lld\n", (lld) catchSize);
        }
        freeSharedBuffer(ourBuffers[MYTHREAD]);
    }

#ifdef DEBUG
    // sanity checks
    int64_t newSize = getReadLengthBuffer(myBuffer), newTotal = 0;
    if (ourSizes[MYTHREAD] != newSize) DIE("ourSizes=%lld newSize=%lld\n", ourSizes[MYTHREAD], (lld) newSize);
    int64_t newOffset = reduce_prefix_long(newSize, UPC_ADD, &newTotal) - newSize;
    if (total != newTotal) DIE("total=%lld newTotal=%lld newOffset=%lld\n", (lld) total, (lld) newTotal, (lld) newOffset);
#endif

    UPC_ALL_FREE_CHK(ourBuffers);
    LOGF("Finished consolidation from %lld bytes to %lld bytes at globalOffset=%lld newOffset=%lld in %0.3f s\n", (lld) originalSize, (lld) getReadLengthBuffer(myBuffer), (lld) myGlobalOffset, (lld) myGlobalOffset + originalSize, now() - start);
    if (getReadLengthBuffer(myBuffer) != ourSizes[MYTHREAD]) DIE("Invalid assumption %lld vs %lld\n", (lld) getReadLengthBuffer(myBuffer), (lld) ourSizes[MYTHREAD]);

#ifdef UPC_ALIGN_WRITES
    int64_t alignGlobalOffset = alignWriteBuffers(myBuffer, ourSizes, alignBlockSize);
    LOGF("After alignment originalOffset=%lld, alignGlobalOffset=%lld, readLengthBuffer=%lld ourSizes=%lld\n", (lld) myGlobalOffset, (lld) alignGlobalOffset, (lld) getReadLengthBuffer(myBuffer), (lld) ourSizes[MYTHREAD]);
#endif

    return myGlobalOffset;
}

ioptr start_allWriteFile(const char *ourFileName, const char *mode, Buffer myBuffer, int writeIdx)
{
    assert(myBuffer != NULL);
    assert(isValidBuffer(myBuffer));
    ioptr _ioptr = calloc_chk(1, sizeof(*_ioptr));
    LOGF("start_allWriteFile(name=%s, mode=%s, myBufferReadLen=%lld): ioptr=%p\n", ourFileName, mode, getReadLengthBuffer(myBuffer), _ioptr);
    if (strlen(ourFileName) == 0) DIE("Can not allWrite a file if it has no name!\n");
    shared[1] int64_t *ourEndWriteSize, *ourTotalsWritten, *ourSizes, *appendOffset, *buf = NULL;
    UPC_ALL_ALLOC_CHK(buf, THREADS * 3L + 1, sizeof(int64_t));
    if (buf == NULL) {
        DIE("Could not allocate memory for ourSizes!\n");
    }
    UPC_ALL_ALLOC_CHK(_ioptr->finishedCount, 1, sizeof(int32_t));
    ourSizes = buf;
    ourEndWriteSize = buf + THREADS;
    ourTotalsWritten = ourEndWriteSize + THREADS;
    appendOffset = ourTotalsWritten + THREADS; // just one entry

    assert(upc_threadof(ourSizes + MYTHREAD) == MYTHREAD);
    assert(upc_threadof(ourEndWriteSize + MYTHREAD) == MYTHREAD);
    assert(upc_threadof(ourTotalsWritten + MYTHREAD) == MYTHREAD);
    assert(upc_threadof(appendOffset) == 0);

    // consolidate small buffers into fewer larger ones
    int64_t myOrigSize = getReadLengthBuffer(myBuffer);
    int64_t myOrigOffset = consolidateSmallBuffers(myBuffer, ourSizes, MINIMUM_WRITE_BLOCK);

    int iAmActive = getReadLengthBuffer(myBuffer) > 0;

    // set every local size to write
    int64_t mySize = getReadLengthBuffer(myBuffer), writeSize = 0;
    ourSizes[MYTHREAD] = mySize;
    ourEndWriteSize[MYTHREAD] = 0;
    ourTotalsWritten[MYTHREAD] = 0;

    upc_fence;

    // set fileOffset, if appending
    int64_t fileOffset = 0;
    if (mode[0] == 'a') {
        if (MYTHREAD == 0) {
            fileOffset = get_file_size(ourFileName);
            appendOffset[0] = fileOffset;
            upc_fence;
        }
        UPC_LOGGED_BARRIER;

        // communicate starting offset to every thread (using ourTotalsWritten)
        upc_all_broadcast(ourTotalsWritten, appendOffset, sizeof(int64_t), UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
        if (MYTHREAD == 0) {
            assert(ourTotalsWritten[MYTHREAD] == fileOffset);
        }
        fileOffset = ourTotalsWritten[MYTHREAD];
        upc_fence;
    }

    // calculate global file offset for each chunk
    upc_all_prefix_reduceL(ourEndWriteSize, ourSizes, UPC_ADD, THREADS, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    // communicate total file size to be written to every thread
    upc_all_broadcast(ourTotalsWritten, ourEndWriteSize + (THREADS - 1), sizeof(int64_t), UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
    if (MYTHREAD == THREADS - 1) {
        assert(ourTotalsWritten[MYTHREAD] == ourEndWriteSize[MYTHREAD]);
    }

    assert(mySize <= ourEndWriteSize[MYTHREAD]);
    int64_t myOffset = fileOffset + ourEndWriteSize[MYTHREAD] - mySize;
    assert(mySize + myOffset <= fileOffset + ourTotalsWritten[MYTHREAD]);
    assert(ourTotalsWritten[MYTHREAD] == ourTotalsWritten[THREADS - 1]);
    assert(ourTotalsWritten[MYTHREAD] == ourEndWriteSize[THREADS - 1]);

    if (writeIdx) {
        writeIndexFile(ourFileName, myOrigOffset, myOrigSize, -1);
    }

    // Now have thread 0 open and truncate the file
    int64_t fileSize = fileOffset + ourTotalsWritten[MYTHREAD];
    _ioptr->fd = -1; // default to not open
    _ioptr->totalSize = ourTotalsWritten[MYTHREAD];
    _ioptr->start = now();
    char tmpFileName[MAX_FILE_PATH+10];
    sprintf(tmpFileName, "%s.tmp", ourFileName);
    if (MYTHREAD == 0) {
        if (does_file_exist(ourFileName)) {
            char oldFileName[MAX_FILE_PATH+13];
            sprintf(oldFileName, "%s.old", ourFileName);
            LOGF("Renaming existing file %s to %s\n", ourFileName, oldFileName);
            if (rename(ourFileName, oldFileName)) {
                WARN("Could not rename existing file that will be overwritten now: %s to %s! errno=%d %s\n", ourFileName, oldFileName, errno, strerror(errno));
            }
        }
        if (does_file_exist(tmpFileName)) {
            if (unlink(tmpFileName)) DIE("Could not unlink temporary file %s! (%s)\n", tmpFileName, strerror(errno));
        }
        LOGF("Creating %s with %lld bytes\n", ourFileName, (lld)fileSize);
        int _mode = O_CREAT|O_WRONLY|O_TRUNC;
        if (mode[0] == 'a') {
            _mode = O_WRONLY;
        }
        _ioptr->fd = open_chk(tmpFileName, _mode, 00666);
        if (_ioptr->fd < 0) {
            DIE("Could not create and open %s for write+reading! %s\n", tmpFileName, strerror(errno));
        }
        if (ftruncate(_ioptr->fd, fileSize) != 0) {
            DIE("Could not expand %s to %lld bytes! %s\n", tmpFileName, (lld)fileSize, strerror(errno));
        }
#ifdef SYNC_OUTPUT
        if (fsync(_ioptr->fd) != 0) {
            WARN("Could not fsync on %s after ftruncate to %lld bytes! %s\n", tmpFileName, (lld)fileSize, strerror(errno));
        }
#endif
        //if (fflush(_ioptr->f) != 0) {
        //    DIE("Could not fflush %s after ftruncate! %s\n", tmpFileName, strerror(errno));
        //}
        serial_printf("Created and allocated %lld bytes output file: %s (%0.3f s)\n", (lld)fileSize, ourFileName, now() - _ioptr->start);
    }
    UPC_LOGGED_BARRIER;

    strcpy(_ioptr->ourFileName, ourFileName);

    // commence writing
    if (iAmActive) {
        assert(mySize >= 0);
        assert(isValidBuffer(myBuffer));
        assert(getReadLengthBuffer(myBuffer) == mySize);
        if (_ioptr->fd < 0 && mySize > 0) {
#ifdef VALIDATE_AFTER_FTRUNCATE
            do {
                int64_t testFileSize = get_file_size(tmpFileName);
                if (fileSize == testFileSize) {
                    break;
                }
                WARN("%s is not yet %lld after ftruncate! (it is %lld)\n", tmpFileName, (lld)fileSize, (lld)testFileSize);
                UPC_POLL;
            } while (1);
#endif
            _ioptr->fd = open_chk(tmpFileName, O_WRONLY, 00666); // always just writing. file should exist and let the ftruncating thread determine whether to create, append or truncate
            if (_ioptr->fd < 0) {
                DIE("Could not open %s for read+writing! %s\n", tmpFileName, strerror(errno));
            }
            LOGF("Opened %s as fd=%d to write %lld bytes at %lld offset (before block rounding)\n", tmpFileName, _ioptr->fd, (lld) mySize, (lld) myOffset);
        }
        if (_ioptr->fd < 0) {
            LOGF("Nothing to write\n");
        }
#ifdef WRITE_WITH_MMAP
        int64_t mapOffset = myOffset % MMAP_PAGESIZE;
        assert(myOffset >= mapOffset);

        char *_addr = MAP_FAILED;
        if (_ioptr->fd >= 0) {
            addr = mmap(NULL, mySize + mapOffset, PROT_READ | PROT_WRITE, MAP_SHARED, _ioptr->fd, myOffset - mapOffset);
        }
        if (_addr == MAP_FAILED) {
            // mmap is not supported, so write file with fseek and fwrite instead
#endif

        if (mySize) {
            assert(_ioptr->fd > 2);
            assert(_ioptr->b == NULL);
            // transfer ownership of Buffer
            _ioptr->b = initBuffer(0);
            swapBuffer(_ioptr->b, myBuffer);
            assert(mySize == getReadLengthBuffer(_ioptr->b));

#ifndef HIPMER_NO_AIO
            DBG("writing (aio_write) from %lld for %lld to %s\n", (lld) myOffset, (lld) mySize, tmpFileName);
            _ioptr->_aio.aio_fildes = _ioptr->fd;
            _ioptr->_aio.aio_offset = myOffset;
            _ioptr->_aio.aio_buf = getCurBuffer(_ioptr->b);
            _ioptr->_aio.aio_nbytes = mySize;
            _ioptr->mySize = mySize;
            _ioptr->_aio.aio_reqprio = 0;
            _ioptr->_aio.aio_sigevent.sigev_notify = SIGEV_NONE;
            _ioptr->_aio.aio_lio_opcode = 0;
            int err = aio_write(&(_ioptr->_aio));
            if (err != 0) {
                DIE("Could not call aio_write (return %d) on %lld bytes! %s\n", err, (lld)mySize, strerror(errno));
            }
#else
            DBG("writing (fwrite) from %lld for %lld to %s\n", (lld) myOffset, (lld) mySize, tmpFileName);
            if ((writeSize = writeFDBuffer(_ioptr->b, _ioptr->fd, myOffset)) != mySize) {
                DIE("Could only write %lld bytes of %lld! %s\n", (lld)writeSize, (lld)mySize, strerror(errno));
            }
            _ioptr->mySize = writeSize;
            freeBuffer(_ioptr->b);
            assert(_ioptr->b == NULL);
#endif
        }
#ifdef WRITE_WITH_MMAP
    } else {
        if (_addr == MAP_FAILED || _addr == NULL) {
            long long int moff = myOffset - mapOffset;
            long long int msize = mySize + mapOffset;
            DIE("Could not mmap %s from %lld for %lld bytes! %s\n", tmpFileName, moff, msize, strerror(errno));
        }
        //LOGF("writing (mmap) from %lld (%lld from nearest %lld block) for %lld to %s\n", (lld) myOffset, (lld) mapOffset, (lld) MMAP_PAGESIZE, (lld) mySize, tmpFileName);
        if (mySize > 0) {
            char *addr = _addr + mapOffset;
            assert(isValidBuffer(myBuffer));
            memcpy(addr, getCurBuffer(myBuffer), mySize);
            if (munmap(_addr, mySize + mapOffset) != 0) {
                DIE("Could not unmmap %s! %s\n", tmpFileName, strerror(errno));
            }
            resetBuffer(myBuffer);
        }
    }
    if (_ioptr->fd > 0 && close_track(_ioptr->fd) != 0) {
        WARN("Could not close %s after mmap! %s\n", tmpFileName, strerror(errno));
    }
    _ioptr->fd = -1;
#endif
    } else {
        assert(mySize == 0); // pitcher should be empty handed now
    }
    // return now all operations are in flight (or done if no AIO support)
    UPC_ALL_FREE_CHK(buf);
    assert(_ioptr != NULL);
    return _ioptr;
}

char test_aio(ioptr _ioptr)
{
    if (!_ioptr) {
        return 1;
    }
    // close and cleanup
#ifndef HIPMER_NO_AIO
    int status = aio_error(&(_ioptr->_aio));
    if (status == EINPROGRESS) {
        return 0;
    }
    if (status != 0) {
        if (status == ECANCELED) {
            WARN("aio_write was cancelled?\n");
        } else {
            DIE("aio failed to complete (%d)! on %s writing %lld bytes! %s\n", status, _ioptr->ourFileName, (lld)_ioptr->_aio.aio_nbytes, strerror(status));
        }
    }
    LOGF("test_aio: completed (%d)\n", status);
#endif

    // Complete, close and cleanup!
    if (_ioptr->fd >= 0 && close_track(_ioptr->fd) != 0) {
#ifndef HIPMER_NO_AIO
        DIE("Could not close %s after writing %lld bytes! %s\n", _ioptr->ourFileName, (lld)_ioptr->_aio.aio_nbytes, strerror(errno));
#else
        DIE("Could not close %s after writing! %s\n", _ioptr->ourFileName, strerror(errno));
#endif
    }
    _ioptr->fd = -1;

    if (_ioptr->ourBuffers != NULL) {
        if (_ioptr->ourBuffers[MYTHREAD] != NULL) {
            UPC_FREE_CHK(_ioptr->ourBuffers[MYTHREAD]);
            _ioptr->ourBuffers[MYTHREAD] = NULL;
        }
        UPC_ALL_FREE_CHK(_ioptr->ourBuffers);
        _ioptr->ourBuffers = NULL;
    }

    //LOGF("test_allWriteFile(%p): finished\n", _ioptr);
    return 1;
}

int64_t finish_aio(ioptr _ioptr)
{
    double start = now();
    LOGF("finish_aio(%p=%s)\n", _ioptr, _ioptr->ourFileName);
    while (!test_aio(_ioptr)) {
        UPC_POLL;
    }
#ifndef HIPMER_NO_AIO
    int64_t mySize = aio_return(&(_ioptr->_aio));
    if (mySize < 0) {
        DIE("finish_aio(%p) got %lld return status! errno=%d %s\n", _ioptr, (lld)mySize, errno, strerror(errno));
    }
    if (mySize != _ioptr->mySize) {
        DIE("finish_aio(%p) expected %lld but got %lld! errno=%d %s\n", _ioptr, (lld)_ioptr->mySize, (lld) mySize, errno, strerror(errno));
    }
#else
    int64_t mySize = _ioptr->mySize;
#endif
    assert(_ioptr->fd < 0); // already closed
    // free the buffer if neede
    Buffer myBuffer = _ioptr->b;
    if (myBuffer) {
        freeBuffer(myBuffer);
        myBuffer = NULL;
    }

    // rename the tempoary file to the real path
    int32_t testFinished;
    UPC_ATOMIC_FADD_I32(&testFinished, _ioptr->finishedCount, 1);
    UPC_ALL_FREE_CHK(_ioptr->finishedCount); // okay to all deallocate before all threads have used this
    if (testFinished+1 == THREADS) {
        LOGF("Last to finish, renaming .tmp to %s\n", _ioptr->ourFileName);
        char tmpFileName[MAX_FILE_PATH+10];
        sprintf(tmpFileName, "%s.tmp", _ioptr->ourFileName);
        if (rename(tmpFileName, _ioptr->ourFileName)) {
            DIE("Could not rename tmp %s to %s! (%s)\n", tmpFileName, _ioptr->ourFileName, strerror(errno));
        }
        double t = now() - _ioptr->start;
        if (t == 0.0) { 
            t = 0.001; // avoid div by zero
        }
        any_printf("Finished write to %s of %0.3f MB in %0.3f s at >= %0.3f MB/s, %0.3f MB/s globally (Th%d renamed)\n", _ioptr->ourFileName, _ioptr->totalSize * PER_MB, t, _ioptr->mySize * PER_MB / t, _ioptr->totalSize * PER_MB / t, MYTHREAD);
    }

    clear_my_lustre_cache();

    double end = now();
    if (end == start) { 
        end = start + 0.001; /* avoid div by zero .. at least 1 ms */ 
    }
    LOGF("finish_aio(%p=%s) finished in %0.3f s, total %0.3f s (%0.3f MB/s) (%d other threads have finished)\n", _ioptr, _ioptr->ourFileName, end - start, end - _ioptr->start, _ioptr->mySize * PER_MB / (end - _ioptr->start), testFinished);
    // clear the ioptr
    memset(_ioptr, 0, sizeof(*_ioptr));
    free_chk(_ioptr);
    return mySize;
}

int64_t allWriteFile(const char *ourFileName, const char *mode, Buffer myBuffer, int writeIdx)
{
    ioptr _aio = start_allWriteFile(ourFileName, mode, myBuffer, writeIdx);

    return finish_aio(_aio);
}


int rb_tree_cmp_checkpoint(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b)
{
    CheckpointFile a = (CheckpointFile) node_a->value;
    CheckpointFile b = (CheckpointFile) node_b->value;
    int ret = 0;
    if (strlen(a->name) && strlen(b->name)) {
        ret = strcmp(a->name, b->name);
    }
    DBG2("cmp ret=%d: a=%s %p  b=%s %p\n", ret, a->name, a->local, b->name, b->local);
    return ret;
}

void freeCheckpoint(CheckpointFile ckpt) {
    LOGF("freeCheckpoint(%p=%s) local=%p inTransit=%p mode=%d\n", ckpt, ckpt->name, ckpt->local, ckpt->inTransit, ckpt->mode);
    if (ckpt->local) GZIP_CLOSE(ckpt->local);
    ckpt->local = NULL;
    if (ckpt->mode == 0) { // was reading
       if (ckpt->inTransit) DIE("Was not expecting any IO in transit when mode is reading!\n");
    } else { // was writing
       syncCheckpoint2(ckpt);
       assert(ckpt->local == NULL);
       assert(ckpt->inTransit == NULL);
    }
    free_chk(ckpt);
}

void rb_tree_delete_checkpoint(struct rb_tree *self, struct rb_node *node)
{
    CheckpointFile ckpt = (CheckpointFile) node->value;
    LOGF("Deleting checkpoint(%p=%s)\n", ckpt, ckpt->name);
    freeCheckpoint(ckpt);
    rb_tree_node_dealloc_cb(self, node);
}

int rb_tree_insert_checkpoint(struct rb_tree *self, CheckpointFile ckpt)
{
    CheckpointFile c = (CheckpointFile) rb_tree_find(self, ckpt);

    if (c == NULL) {
        LOGF("Inserting checkpoint(%p=%s)\n", ckpt, ckpt->name);
        return rb_tree_insert(self, ckpt);
    } else {
        DIE("Attempt to add a checkpoint that already exists!\n");
    }
    return 0;
}

void delete_all_checkpoints(_StaticVars *mysv)
{
    LOGF("delete_all_checkpoints(%p): %p\n", mysv, (mysv ? mysv->checkpointFiles : NULL));
    if (mysv && mysv->checkpointFiles) return;
    rb_tree_dealloc(mysv->checkpointFiles, rb_tree_delete_checkpoint);
    MYSV.checkpointFiles = NULL; 
}



CheckpointFile findCheckpoint(const char *name, GZIP_FILE fh) {
    // find checkpoint if it already exists
    if (!MYSV.checkpointFiles) MYSV.checkpointFiles = rb_tree_create(rb_tree_cmp_checkpoint);
    CheckpointFile ckpt = NULL;
    CheckpointFile existing = NULL;
    if (name && strlen(name)) {
        ckpt = (CheckpointFile) calloc_chk(sizeof(_CheckpointFile), 1);
        strcpy(ckpt->name, name);
        existing = rb_tree_find(MYSV.checkpointFiles, ckpt);
        free_chk(ckpt);
        if (existing) {
            ckpt = existing;
            LOGF("Found checkpoint by name (%p=%s)\n", ckpt, ckpt->name);
        } else {
            LOGF("Could not find checkpoint by name: %s\n", name);
        }
    } else {
        // iterate through them all as no name is provided
        struct rb_iter *iter = rb_iter_create();
        for (CheckpointFile test = rb_iter_first(iter, MYSV.checkpointFiles); test ; test = rb_iter_next(iter)) {
           if (test->local == fh) {
               ckpt = test;
               LOGF("Found checkpoint by fh (%p=%s) %p==%p\n", ckpt, ckpt->name, ckpt->local, fh);
               break;
           }
        }
        rb_iter_dealloc(iter);
        if (!ckpt) LOGF("Could not find checkpoint by fh: %p\n", fh);
    }
    return ckpt;
}

CheckpointFile findOrInitCheckpoint(const char *name, char *mode, int forceGzip, const char * filename, int line) {
    double start = now();
    int myMode = -1;
    if (mode[0] == 'r' && !strchr(mode, '+')) myMode = 0;
    else if (mode[0] == 'w') myMode = 1;
    else DIE("[%s:%d] Invalid mode %s - expecting 'r' or 'w'!\n", filename, line, mode);

    CheckpointFile ckpt = (CheckpointFile) calloc_chk(sizeof(_CheckpointFile), 1);
    strcpy(ckpt->name, name);

    // find checkpoint if it already exists
    if (!MYSV.checkpointFiles) MYSV.checkpointFiles = rb_tree_create(rb_tree_cmp_checkpoint);
    CheckpointFile existing = rb_tree_find(MYSV.checkpointFiles, ckpt);
    if (existing) {
        free_chk(ckpt);
        ckpt = existing;
        LOGF("[%s:%d] Found existing checkpoint(%s, \"%s\"): mode=%d inTransit=%p local=%p\n", filename, line, name, mode, ckpt->mode, ckpt->inTransit, ckpt->local);
    } else {
        LOGF("[%s:%d] Creating new checkpoint(ckpt=%p name=%s, \"%s\"): myMode=%d, forceGZip=%d)\n", filename, line, ckpt, name, mode, myMode, forceGzip);
    }

    char localFile[MAX_FILE_PATH];
    checkpointToLocal(name, localFile);
    char globalFile[MAX_FILE_PATH];
    checkpointToGlobal(name, globalFile);
    if (myMode == 0) { // want to read
        int localExists = does_file_exist( localFile );
        if (ckpt->mode == 1 || ckpt->mode == 3) { // want to read, but may still be writing... okay local should still exist 
            // mode of checkpoint is still write
            if( ! localExists ) DIE("[%s:%d] Could not find the local file (%s) to read, but mode was previously writing! \"%s\" mode=%s\n", filename, line, localFile, mode, ckpt->mode);
            ckpt->mode = 3; // on next checkCheckpointState, force a sync
        } else { // not actively writing
            ckpt->mode = myMode;
            int allLocalFilesExist = reduce_int(localExists, UPC_ADD, ALL_DEST); 
            if (allLocalFilesExist != THREADS) {
                // restore from global checkpoint ... DIE if checkpoint is missing... must restart from start or an earlier stage
                if (!does_file_exist(globalFile)) SDIE("[%s:%d] Can not read checkpoint '%s' when not all the local files exist (%d out of %d) and there is no global file!\n", filename, line, ckpt->name, allLocalFilesExist, THREADS);
                restoreLocalCheckpoint(ckpt->name);
            } else if (MYSV.checkpoint_path != NULL) {
                // verify checkpoint global file exists and is newer, if not set mode to write
                int doesGlobalExist = 0;
                if (MYTHREAD == THREADS-1) {
                    doesGlobalExist = does_file_exist( globalFile );
                    if (doesGlobalExist) {
                        struct stat stat1, stat2;
                        int s1 = stat(localFile, &stat1);
                        int s2 = stat(globalFile, &stat2);
                        if (stat1.st_mtime > stat2.st_mtime) {
                            SLOG("[%s:%d] NOTICE: global file %s exists but is older than local file %s\n", filename, line, globalFile, localFile);
                            doesGlobalExist = 0;
                        }
                    }
                }
                doesGlobalExist = broadcast_int(doesGlobalExist, THREADS-1);
                if (!doesGlobalExist) {
                    LOGF("[%s:%d] Creating global checkpoint file %s as it is not newer than localfile %s\n", filename, line, globalFile, localFile);
                    ckpt->mode = 3; // on next checkCheckpointState, force a sync
                    ckpt->myidx.size = get_file_size(localFile);
                    ckpt->myidx.offset = reduce_prefix_long(ckpt->myidx.size, UPC_ADD, NULL) - ckpt->myidx.size;
                    ckpt->inTransit = saveAggregatedFile(localFile, globalFile, &ckpt->myidx);
                    DBG("Set intransit for checkpoint(%p=%s): %p\n", ckpt, ckpt->name, ckpt->inTransit);
                }
                addCountTime(&MYSV.checkpointSetup, now() - start);
            }
        }
        if (ckpt->local) {
            if (forceGzip==-1) {
               rewind((FILE*) ((void*)ckpt->local));
            } else if (forceGzip == 1) {
               gzrewind((gzFile) ((void*)ckpt->local));
            } else {
               GZIP_REWIND(ckpt->local);
            }
        } else {
            if (forceGzip==-1) {
                ckpt->local = (GZIP_FILE) ((void*)fopen_chk(localFile, mode));
            } else if (forceGzip==1) {
                ckpt->local = (GZIP_FILE) ((void*)gzopen_chk(localFile, mode));
            } else {
                ckpt->local = GZIP_OPEN(localFile, mode);
            }
        }
    } else { // want to write
        if (existing && ckpt->mode > 0) {
            SWARN("Waiting for checkpoint of %d to finish writing before writing to it again\n");
            syncCheckpoint2(ckpt); 
        }
        int localExists = does_file_exist( localFile );
        if (localExists) {
            SLOG("NOTICE: Existing local checkpoint '%s' is being overwritten\n", ckpt->name);
        }
        if (doesGlobalCheckpointExist( ckpt->name )) {
            SLOG("NOTICE: Existing global checkpoint '%s' will be overwritten\n", ckpt->name);
        }
        if (ckpt->local != NULL) DIE("Invalid assumption - local file should not be open yet\n");
        if (ckpt->mode != 0) DIE("Invalid assumption - mode should be read if writing is done was %d!\n", ckpt->mode);
        if (ckpt->inTransit) DIE("Invalid assumption - no data should be in flight when writing is started\n");
        ckpt->mode = myMode;
        strcat(localFile, ".tmp"); // write to a temporary file.  Close will rename it
        if (forceGzip==-1) {
            ckpt->local = (GZIP_FILE) ((void*) fopen_chk(localFile, mode));
        } else if (forceGzip==1) {
            ckpt->local = (GZIP_FILE) ((void*) gzopen_chk(localFile, mode));
        } else {
            ckpt->local = GZIP_OPEN(localFile, mode);
        }
    }

    if (!existing) {
        int c = rb_tree_insert_checkpoint( MYSV.checkpointFiles, ckpt );
        if (!c) DIE("Could not insert checkpoint %s into rb_tree!\n", name);
    }

    if (ckpt->inTransit && MYSV.syncAllCheckpoints) {
        LOGF("Forcing an immediate sync of %p=%s\n", ckpt, ckpt->name);
        syncCheckpoint2(ckpt);
    }
    
    return ckpt;
}

// translates checkpoint name to local and global file paths
void checkpointToLocal(const char *name, char *localfilename) {
    char _buf[MAX_FILE_PATH], *buf;
    // handle the case where input and output are the same buffer
    if (name == localfilename) {
        buf = _buf;
    } else {
        buf = localfilename;
    } 
    snprintf(buf, MAX_FILE_PATH - 1, "%s/%s", MYSV.cached_io_path == NULL ? "." : MYSV.cached_io_path, name);
    get_rank_path(buf, MYTHREAD);
    if (name == localfilename) {
        strcpy(localfilename, buf);
    }
}

void checkpointToGlobal(const char *name, char *globalfilename) {
    char _buf[MAX_FILE_PATH], *buf;
    // handle the case where input and output are the same buffer
    if (name == globalfilename) {
        buf = _buf;
    } else {
        buf = globalfilename;
    }
    snprintf(buf, MAX_FILE_PATH - 1, "%s/%s", MYSV.checkpoint_path == NULL ? "intermediates" : MYSV.checkpoint_path, name);
    if (name == globalfilename) {
       strcpy(globalfilename, buf);
    }
}

int _doesLocalCheckpointExist(const char * name, const char *file, int lineno) {
    LOGF("[%s:%d] doesLocalCheckpointExist(%s)...", file, lineno, name);
    char local[MAX_FILE_PATH];
    checkpointToLocal(name, local);
    int localExists = does_file_exist(local);
    if (MYTHREAD == THREADS-1) {
        // check timestamp and sizes against global, if that exists!
        char global[MAX_FILE_PATH];
        checkpointToGlobal(name, global);
        if (does_file_exist(global)) {
            // check the size of the .idx matches THREADS
            char idx[MAX_FILE_PATH+20];
            sprintf(idx, "%s.idx", global);
            if ( get_file_size_if_exists(idx) != THREADS * sizeof(IndexElement) ) {
                WARN("The global file for %s exists but the index is not the correct size -- local needs a restore! %lld vs expected %lld: %s\n", name, (lld) get_file_size_if_exists(idx), (lld) THREADS * sizeof(IndexElement), idx);
                localExists = 0;
            }
            // check that the global is not older than the local
            if (localExists && is_file_newer_than(local, global)) {
                WARN("The global file for %s exists but the local checkpoint is newer than the local file -- global needs a restore! %s vs %s\n", name, global, local);
            }
        }
    }
    int allExist = reduce_int(localExists, UPC_ADD, ALL_DEST);
    LOGFN("\tlocalExists=%d allExist=%d\n", localExists, allExist);
    return allExist == THREADS;
}
    
int _doesGlobalCheckpointExist(const char *name, const char *file, int lineno) {
    LOGF("[%s:%d] doesGlobalCheckpointExist(%s)...", file, lineno, name);
    char global[MAX_FILE_PATH];
    int globalExists = 0;
    if (MYTHREAD == THREADS-1) {
        checkpointToGlobal(name, global);
        globalExists = does_file_exist(global);
        if (globalExists) {
            // check the timestamps
            char local[MAX_FILE_PATH];
            checkpointToLocal(name, local);
            int localExists = does_file_exist(local);
            if (localExists && is_file_newer_than(local, global)) {
                WARN("The global file for %s exists but the local checkpoint is newer than the local file -- global needs a restore! %s vs %s\n", name, global, local);
                globalExists = 0;
            }
            if (globalExists) {
                // check the index file too
                strcat(global, ".idx");
                if (!does_file_exist(global)) {
                    WARN("The global file for %s exists but the .idx file is missing -- global needs a restore!\n", name);
                    globalExists = 0;
                }
            }
        }
    }
    int allExist = broadcast_int(globalExists * THREADS, THREADS-1);
    LOGF("\tallExist=%d - %s\n", allExist, (allExist == THREADS ? "True" : "False"));
    return allExist == THREADS;
}

int _doesCheckpointExist(const char *name, const char *file, int lineno) {
    int doesLocalExist = _doesLocalCheckpointExist(name, file, lineno);
    if (! doesLocalExist ) {
        return _doesGlobalCheckpointExist(name, file, lineno);
    } else {
        return doesLocalExist;
    }
}

// checkpoint routines are collective, and access a tree stored in StaticVars

// when writing, opens the local file
// when reading, opens the local file, if it does not exist, recreates it from the global file
GZIP_FILE openCheckpoint_at_file_line(const char *name, char *mode, const char * file, int lineno) {
    LOGF("[%s:%d] openCheckpoint(%s, %s)\n", file, lineno, name, mode);
    CheckpointFile ckpt = findOrInitCheckpoint(name, mode, 0, file, lineno);
    if (ckpt->local == NULL) DIE("Invalid findOrInit!\n");
    return ckpt->local;
}
gzFile openCheckpoint1_at_file_line(const char *name, char *mode, const char * file, int lineno) {
    LOGF("[%s:%d] openCheckpoint1(%s, %s)\n", file, lineno, name, mode);
    CheckpointFile ckpt = findOrInitCheckpoint(name, mode, 1, file, lineno);
    if (ckpt->local == NULL) DIE("Invalid findOrInit!\n");
    return (gzFile) ((void*)ckpt->local);
}
FILE * openCheckpoint0_at_file_line(const char *name, char *mode, const char * file, int lineno) {
    LOGF("[%s:%d] openCheckpoint0(%s, %s)\n", file, lineno, name, mode);
    CheckpointFile ckpt = findOrInitCheckpoint(name, mode, -1, file, lineno);
    if (ckpt->local == NULL) DIE("Invalid findOrInit!\n");
    return (FILE*) ((void*)ckpt->local);
}

// when writing, closes the local file and consolidates to the global file in the background (re-reads local file)
// when reading, closes the local file
void __closeCheckpoint(CheckpointFile ckpt, int forceGzip, const char *filename, int line) {
    double start = now();
    if (!ckpt || ckpt->local == NULL) DIE("[%s:%d] No local file for checkpoint %p\n", filename, line, ckpt);
    LOGF("closeCheckpoint(%p=%s) mode=%d inTransit=%p checkpoint_path=%s\n", ckpt, ckpt->name, ckpt->mode, ckpt->inTransit, MYSV.checkpoint_path);
    if (forceGzip == -1) {
        ckpt->myidx.uncompressedSize = ftell((FILE*) ((void*) ckpt->local));
        int status = fclose((FILE*) ((void*) ckpt->local)); 
        if (status != 0) {
            DIE("Could not close checkpoint %s -  fclose returned %d at %lld uncompressed size. errno=%d %s\n", ckpt->name, status, (lld) ckpt->myidx.uncompressedSize, errno, strerror(errno));
        }
    } else if (forceGzip == 1) {
        ckpt->myidx.uncompressedSize = gztell((gzFile) ((void*) ckpt->local));
        int status = gzclose((gzFile) ((void*) ckpt->local)); 
        if (status != 0) {
            int err;
            const char * errmsg = gzerror((gzFile) ((void*) ckpt->local), &err);
            DIE("Could not gzclose checkpoint %s -  gzclose returned %d at %lld uncompressed size gzerror=%d %s, errno=%d %s\n", ckpt->name, status, (lld) ckpt->myidx.uncompressedSize, err, errmsg, errno, strerror(errno));
        }
    } else { 
        ckpt->myidx.uncompressedSize = GZIP_FTELL(ckpt->local);
        int status = GZIP_CLOSE(ckpt->local); 
        if (status != 0) {
            int err = 0;
            const char *errmsg = NULL;
            errmsg = gzerror((gzFile) ((void*) ckpt->local), &err);
            DIE("Could not GZIP_CLOSE checkpoint %s -  GZIP_CLOSE returned %d at %lld uncompressed size. gzerror=%d %s, errno=%d %s\n", ckpt->name, status, (lld) ckpt->myidx.uncompressedSize, err, errmsg, errno, strerror(errno));
        }
    }
    ckpt->local = NULL;
    addCountTime(&MYSV.checkpointSync, now() - start);
    if (ckpt->mode == 1) {
        // was writing so rename the temporary file
        char localFile[MAX_FILE_PATH], tmpFile[MAX_FILE_PATH+20];
        checkpointToLocal(ckpt->name, localFile);
        sprintf(tmpFile, "%s.tmp", localFile);
        if (rename(tmpFile, localFile)) {
            DIE("Could not rename temporary checkpointLocal file %s to %s! %s\n", tmpFile, localFile, strerror(errno));
        }
    } 
    if (ckpt->mode == 1 && MYSV.checkpoint_path) { // was writing, so write the global now
        saveGlobalCheckpoint2(ckpt);
    } else if (ckpt->mode == 3 && MYSV.checkpoint_path) {
        // this is either a restore of the global file from local files or local files have been read and closed.  sync global file on close here
        syncCheckpoint2(ckpt);
    } else {
        if (ckpt->inTransit) DIE("read mode (%d) or no checkpoint_path (%s) should not have anything inTransit: %s\n", ckpt->mode, MYSV.checkpoint_path, ckpt->name);
    }
}
void _closeCheckpoint2(const char *name, const char *filename, int line) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    if (!ckpt) DIE("[%s:%d] Could not find checkpoint by an open file handle!\n", filename, line);
    __closeCheckpoint(ckpt, 0, filename, line);
}
void _closeCheckpoint(GZIP_FILE fh, const char *filename, int line) {
    CheckpointFile ckpt = findCheckpoint(NULL, fh);
    if (!ckpt) DIE("[%s:%d] Could not find checkpoint by an open file handle!\n", filename, line);
    __closeCheckpoint(ckpt, 0, filename, line);
}
void _closeCheckpoint1(gzFile fh, const char *filename, int line) {
    CheckpointFile ckpt = findCheckpoint(NULL, (GZIP_FILE) ((void*) fh));
    if (!ckpt) DIE("[%s:%d] Could not find checkpoint by an open file handle!\n", filename, line);
    __closeCheckpoint(ckpt, 1, filename, line);
}
void _closeCheckpoint0(FILE *fh, const char *filename, int line) {
    CheckpointFile ckpt = findCheckpoint(NULL, (GZIP_FILE) ((void*) fh));
    if (!ckpt) DIE("[%s:%d] Could not find checkpoint by an open file handle!\n", filename, line);
    __closeCheckpoint(ckpt, -1, filename, line);
}

void restoreCheckpoint(const char *name) {
    LOGF("Restoring checkpoint %s\n", name);
    closeCheckpoint( openCheckpoint(name, "r") ); // open and close forces both local and global files to be generated
}

// initiates a local -> global save
void saveGlobalCheckpoint(const char *name) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    if (ckpt) {
        saveGlobalCheckpoint2(ckpt);
    } else {
        restoreCheckpoint(name);
    }
}

void saveGlobalCheckpoint2(CheckpointFile ckpt) {
    double start = now();
    LOGF("saveGlobalCheckpoint(%p=%s) inTransit=%p\n", ckpt, ckpt->name, ckpt->inTransit);
    if (ckpt->inTransit) {
        WARN("saveGlobalCheckpoint (%s) called while the save is already in transit!  You must perform a syncCheckpoint before saving it again!\n", ckpt->name); 
        return;
    }
    char localFile[MAX_FILE_PATH];
    checkpointToLocal(ckpt->name, localFile);
    ckpt->myidx.size = get_file_size(localFile);
    ckpt->myidx.offset = reduce_prefix_long(ckpt->myidx.size, UPC_ADD, NULL) - ckpt->myidx.size;

    char globalFile[MAX_FILE_PATH];
    checkpointToGlobal(ckpt->name, globalFile);
    ckpt->inTransit = saveAggregatedFile(localFile, globalFile, &ckpt->myidx);
    double t = now() - start;
    LOGF("Set intransit for checkpoint(%p=%s): %p in %0.3fs\n", ckpt, ckpt->name, ckpt->inTransit, t);
    if (MYSV.syncAllCheckpoints) {
        LOGF("Forcing an immediate sync of %p=%s\n", ckpt, ckpt->name);
        syncCheckpoint2(ckpt);
    }
}

// forces a global -> local restore and blocks
void restoreLocalCheckpoint(const char *name) {
    double start = now();
    LOGF("restoreLocalCheckpoint(%s)\n", name);
    char localFile[MAX_FILE_PATH];
    checkpointToLocal(name, localFile);
    char globalFile[MAX_FILE_PATH];
    checkpointToGlobal(name, globalFile);
    if (!does_file_exist(globalFile)) {
        DIE("Can not read checkpoint '%s' when not all the local files exist (%d out of %d) and there is no global file!\n", name, THREADS);
    }
    restoreAggregatedFile(localFile, globalFile);
    addCountTime(&MYSV.checkpointSync, now() - start);
}


// checks inTransit and possibly closes global.  Returns 1 if all threads are finished with io
// changes mode to 0 if all are done writing
int checkCheckpointState(CheckpointFile ckpt) {
    LOGF("checkCheckpointState(%p=%s): mode=%d inTransit=%p\n", ckpt, ckpt->name, ckpt->mode, ckpt->inTransit);
    if (ckpt->mode == 0) return 1;
    if (ckpt->mode == 3 || (ckpt->mode == 1 && MYSV.syncAllCheckpoints)) {
        // sync on this check
        syncCheckpoint2(ckpt);
    } else {
        // sync on the next call to checkCheckpoint
        ckpt->mode = 3;
    }
    int ret = 1;
    int countFinished = -1;
    if (ckpt->inTransit) {
        double start = now();
        int iAmFinished = test_aio(ckpt->inTransit);
        countFinished = reduce_int(iAmFinished, UPC_ADD, ALL_DEST);
        addCountTime(&MYSV.checkpointSync, now() - start);
        if (countFinished == THREADS) {
           LOGF("All threads have finished aio for %s, finishing now.\n", ckpt->name);
           syncCheckpoint2(ckpt);
           assert(ckpt->inTransit == NULL);
           assert(ckpt->mode == 0);
        }
        ret = (countFinished == THREADS ? 1 : 0);
    }
        
    LOGF("checkCheckpointState(%p=%s): ret=%d countFinished=%d\n", ckpt, ckpt->name, ret, countFinished);
    return ret;
}

// finishes any writing IO on the global checkpoint, blocks until all threads complete
void syncCheckpoint(const char *name) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    if (!ckpt) return;
    syncCheckpoint2(ckpt);
}

void syncCheckpoint2(CheckpointFile ckpt) {
    LOGF("syncCheckpoint2(%p=%s): mode=%d inTransit=%p\n", ckpt, ckpt->name, ckpt->mode, ckpt->inTransit);
    if (ckpt->inTransit) {
       double start = now();
       LOGF("finishing aio for checkpoint %s\n", ckpt->name);
       finish_aio(ckpt->inTransit);
       double t = now() - start;
       LOGF("All threads have finished writing to checkpoint %s in %0.3f s\n", ckpt->name, t);
       addCountTime(&MYSV.checkpointSync, t);
       ckpt->inTransit = NULL;
       ckpt->mode = 0;
   } else {
       // all writes are done, ensure mode is read now
       ckpt->mode = 0;
       DBG("syncCheckpoint2(%p=%s): nothing in transit\n", ckpt, ckpt->name);
   }
}

void syncMissingGlobalCheckpoints() {
   if (MYSV.checkpoint_path == NULL) {
       LOGF("no checkpoint path to sync missing global checkpoints to!\n");
       return;
   } else {
       LOGF("syncMissingGlobalCheckpoints\n");
   }
   Buffer missingCheckpoints = initBuffer(1024);
   if (MYTHREAD == 0) {
       // list the contents of my per_rank directory
       char perThread[MAX_FILE_PATH];
       char global[MAX_FILE_PATH];
       checkpointToLocal(".", perThread);
       DIR *local = opendir(perThread);
       if (!local) DIE("Could not opendir(%s)! %s\n", perThread, strerror(errno));
       struct dirent *f;
       errno = 0;
       while( (f = readdir(local)) != NULL ) {
           checkpointToGlobal(f->d_name, global);
           char localFile[MAX_FILE_PATH];
           checkpointToLocal(f->d_name, localFile);
           assert(does_file_exist(localFile));
           LOGF("Checking possible checkpoint: %s - %s\n", f->d_name, global);
           if (f->d_name[0] == '.' || strstr(f->d_name, ".tmp")) {
               LOGF("Skipping dotfile or tmpfile %s\n", f->d_name);
               continue;
           }
           if ( isNoAggregate(global, 0) ) {
               LOGF("Skipping NO_AGGREGATE file %s\n", global);
               continue;
           }
           if ( (!does_file_exist(global)) || is_file_newer_than(localFile, global) ) {
               LOGF("Will restore missing global for %s\n", f->d_name);
               printfBuffer(missingCheckpoints, "%s\n", f->d_name);
           } else {
               LOGF("Global for %s exists\n", f->d_name);
           }
           errno = 0;
       }
       if (errno) DIE("Could not read entries from %s! %s\n", perThread, strerror(errno));
   }
       
   missingCheckpoints = broadcast_Buffer(missingCheckpoints, 0);
   char ckpt[MAX_FILE_PATH];
   while(getsBuffer(missingCheckpoints, ckpt, MAX_FILE_PATH-1)) {
       ckpt[ strlen(ckpt) - 1 ] = '\0'; // remove newline
       if (! doesLocalCheckpointExist(ckpt) ) {
           SWARN("attempted to restore global checkpoint for %s but not all local files exist!\n", ckpt);
           continue;
       }
       serial_printf("Restoring missing checkpoint: %s\n", ckpt);
       restoreCheckpoint( ckpt );
   }
   cleanupCheckpoints(1);
   freeBuffer(missingCheckpoints);
}

// calls syncCheckpoint and unlinks the local file
// removes checkpoint from the tree
void unlinkLocalCheckpoint(const char *name) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    LOGF("unlinkLocalCheckpoint(%p=%s)\n", ckpt, (ckpt ? ckpt->name : "MISSING"));
    if (ckpt) syncCheckpoint2(ckpt);
    char localFile[MAX_FILE_PATH];
    checkpointToLocal(name, localFile);
    unlink(localFile);
    deleteCheckpoint(name);
}

void unlinkGlobalCheckpoint(const char *name) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    LOGF("unlinkCheckpoint(%p=%s)\n", ckpt, (ckpt ? ckpt->name : "MISSING"));
    if (ckpt && ckpt->inTransit) {
#ifndef HIPMER_NO_AIO
        int status = aio_cancel(ckpt->inTransit->fd, &(ckpt->inTransit->_aio));
#endif
        syncCheckpoint2(ckpt);
    }
    if (MYTHREAD == THREADS-1) {
        char globalFile[MAX_FILE_PATH];
        checkpointToGlobal(name, globalFile);
        int64_t size = get_file_size_if_exists(globalFile);
        if (size) {
            LOG("Unlinking %s of %lld (%0.3f MB)\n", globalFile, size, size * PER_MB);
            if ( unlink(globalFile) ) {
                WARN("Could not unlink %s! (%s)\n", globalFile, strerror(errno));
            }
        }
    }
}

// unlinks all global and local checkpoints - cancels any pending I/O
// removes checkpoint from the tree
void unlinkCheckpoint(const char *name) {
    unlinkGlobalCheckpoint(name);
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    char localFile[MAX_FILE_PATH];
    checkpointToLocal(name, localFile);
    int64_t size = get_file_size_if_exists(localFile);
    if (size) {
        LOGF("Unlinking %s of %lld (%0.3f MB)\n", localFile, (lld) size, size * PER_MB);
        if ( unlink(localFile) ) {
            WARN("Could not unlink %s! (%s)\n", localFile, strerror(errno));
        }
    }
    if (ckpt) rb_tree_remove_with_cb(MYSV.checkpointFiles, ckpt, rb_tree_delete_checkpoint);
}

// just syncs and removes from the tree
void deleteCheckpoint(const char *name) {
    CheckpointFile ckpt = findCheckpoint(name, NULL);
    deleteCheckpoint2(ckpt);
}
void deleteCheckpoint2(CheckpointFile ckpt) {
    LOGF("deleteCheckpoint(%p=%s)\n", ckpt, (ckpt ? ckpt->name : "MISSING"));
    if (ckpt) rb_tree_remove_with_cb(MYSV.checkpointFiles, ckpt, rb_tree_delete_checkpoint);
}

void cleanupCheckpoints(int forceSync) {
    LOGF("cleanupCheckpoints(forceSync=%d)\n", forceSync);
    int deleted = 0;
    if (! (_sv && MYSV.checkpointFiles) ) return;
    struct rb_iter *iter = rb_iter_create();
    int nDel = 0;
    CheckpointFile *del = NULL;
    for (CheckpointFile ckpt = rb_iter_first(iter, MYSV.checkpointFiles); ckpt ; ckpt = rb_iter_next(iter)) {
        if (checkCheckpointState(ckpt)) {
           del = realloc_chk(del, sizeof(CheckpointFile) * (nDel+1));
           del[nDel++] = ckpt;
        }
    }
    for (int i = 0; i < nDel; i++) {
        CheckpointFile ckpt = del[i];
        deleteCheckpoint2(ckpt);
        deleted++;
    }
    if (nDel) free_chk(del);
    if (forceSync) {
        for (CheckpointFile ckpt = rb_iter_first(iter, MYSV.checkpointFiles); ckpt ; ckpt = rb_iter_next(iter)) {
            syncCheckpoint2(ckpt);
            deleteCheckpoint2(ckpt);
            deleted++;
            ckpt = rb_iter_first(iter, MYSV.checkpointFiles);
            if (!ckpt) break;
        }
    }
    rb_iter_dealloc(iter);
    LOGF("cleanupCheckpoints(forceSync=%d): deleted=%d\n", forceSync, deleted);
}

// links two checkpoints, with hard links to global, idx and local files (whichever exist)
int linkCheckpoints(const char *oldName, const char *newName) {
    int success = 0;
    char oldLocalName[MAX_FILE_PATH], newLocalName[MAX_FILE_PATH], oldGlobalName[MAX_FILE_PATH], newGlobalName[MAX_FILE_PATH];
    checkpointToLocal(oldName, oldLocalName);
    checkpointToLocal(newName, newLocalName);
    checkpointToGlobal(oldName, oldGlobalName);
    checkpointToGlobal(newName, newGlobalName);
    if ( does_file_exist(oldLocalName) ) {
        link_chk(oldLocalName, newLocalName);
        LOGF("Linked local files %s to %s\n", oldLocalName, newLocalName);
        success = 1;
    }
    if (MYTHREAD == THREADS-1) {
        if ( does_file_exist(oldGlobalName) ) {
            link_chk(oldGlobalName, newGlobalName);
            strcat(oldGlobalName, ".idx");
            strcat(newGlobalName, ".idx");
            link_chk(oldGlobalName, newGlobalName);
            LOGF("Linked global and index %s to %s\n", oldGlobalName, newGlobalName);
            success = THREADS;
        }
    }
    success = reduce_int(success, UPC_ADD, ALL_DEST);
    LOGF("linkCheckpoints(%s, %s) returns %d (%s)\n", oldName, newName, success, success >= THREADS ? "success" : "FAIL");
    return (success >= THREADS);
}

// returns 1 when ourFile.NO_AGGREGATE exists, otherwise 0
int isNoAggregate(const char *ourFile, int isGlobal)
{
    char test[MAX_FILE_PATH+30];
    sprintf(test, "%s.NO_AGGREGATE", ourFile);
    int ret = 0;
    if (MYTHREAD == 0) {
        if ( does_file_exist(test) ) {
            ret = 1;
        }
    } 
    if (isGlobal) {
        ret = broadcast_int(ret, 0);
    }
    return ret;
}

void setNoAggregate(const char *ourFile)
{
    if (!isNoAggregate(ourFile, 0)) {
        if (MYTHREAD == 0) {
            char test[MAX_FILE_PATH];
            sprintf(test, "%s.NO_AGGREGATE", ourFile);
            serial_printf("Setting %s as NO_AGGREGATE\n", ourFile);
            fclose_track( fopen_chk(test, "w") );
        }
    }
    UPC_LOGGED_BARRIER;
}

ioptr saveAggregatedFile(const char *myfile, const char *ourFile, IndexElement *optidx)
{
    double start = now();
    if ( isNoAggregate(ourFile, 1) ) {
        serial_printf("Skipping aggregate save of %s\n", ourFile);
        return NULL;
    } 
    serial_printf("Saving local files to global %s\n", ourFile);
    LOGF("saving local file %s\n", myfile);
    int64_t mysize = get_file_size(myfile);
    Buffer myBuffer = initBuffer(mysize + WRITE_BLOCK_SIZE);
    FILE *in = fopen_chk(myfile, "r");

    readFileBuffer(myBuffer, in, 0, mysize);
    fclose_track(in);
    LOGF("Read local %lld (%0.3f MB) in %0.3fs\n", (lld) mysize, mysize * PER_MB, now() - start);
    if (optidx) {
        writeIndexFile(ourFile, optidx->offset, optidx->size, optidx->uncompressedSize);
    }
    ioptr ret = start_allWriteFile(ourFile, "w", myBuffer, optidx == NULL);
    freeBuffer(myBuffer);
    LOGF("Read local and started allWrite in %0.3fs ioptr=%p\n", now() - start, ret);
    return ret;
}

void saveAggregatedFileSync(const char *myfile, const char *ourFile, IndexElement *optidx) {
    ioptr io = saveAggregatedFile(myfile, ourFile, optidx);
    finish_aio(io);
}

void restoreAggregatedFile(const char *myfile, const char *ourFile)
{
    // First read the index file
    double start = now();
    IndexElement *idx = NULL;
    int64_t indexCount = THREADS;
    int64_t ourFileSize = 0;

    serial_printf("Restoring local files from %s\n", ourFile);
    LOGF("restoring local %s\n", myfile);
    if (MYTHREAD == THREADS - 1) {
        char indexFileName[MAX_FILE_PATH];
        sprintf(indexFileName, "%s.idx", ourFile);
        // TODO consolidate this to just 2 stat() calls, not 6!
        indexCount = get_file_size(indexFileName) / sizeof(IndexElement);
        if (! does_file_exist(ourFile) ) {
            DIE("Can not restoreAggregatedFile if ourFile=%s does not exist!\n", ourFile);
        }
        if ( is_file_newer_than(indexFileName, ourFile) ) {
            DIE("Can not restoreAggregatedFile if the index file is newer than the aggregated file: %s!  Looks like restoring using a new index from a different version.\n",  ourFile);
        }
        ourFileSize = get_file_size(ourFile);
        if (ourFileSize == 0) {
            DIE("Attempt to restore a zero byte aggregatedFile: %s\n", ourFile);
        }
        FILE *idxFile = fopen_chk(indexFileName, "r");
        idx = calloc_chk(indexCount, sizeof(IndexElement));
        int64_t entries = 0;
        if ((entries = fread_chk(idx, sizeof(IndexElement), indexCount, idxFile)) != indexCount) {
            DIE("Error index file %s did not have %lld entries (%lld)\n", indexFileName, (lld)indexCount, (lld)entries);
        }
        fclose_track(idxFile);
        for(int64_t i = 0; i < indexCount; i++) {
            DBG("Read Index %d: offset=%lld size=%lld\n", i, idx[i].offset, idx[i].size);
        }
        int64_t sizeFromIndex = idx[indexCount-1].offset + idx[indexCount-1].size;
        if (sizeFromIndex != ourFileSize) {
            DIE("The index file for %s believes the file is %lld bytes but it is really %lld\n", ourFile, sizeFromIndex, (lld) ourFileSize);
        }
        if (indexCount != THREADS) {
            LOG("Restoring locals from global %s with a different count! %d to %d\n", ourFile, indexCount, THREADS);
            IndexElement *idx2 = calloc_chk(THREADS, sizeof(IndexElement));
            if (indexCount > THREADS) {
                // some will be merged -- first offset will remain and size will be added
                for(int64_t i = 0 ; i < indexCount; i++) {
                    int64_t i2 = i * THREADS / indexCount;
                    assert(i2 < THREADS);
                    if (i2 == 0 || idx2[ i2 ].offset) { // first thread has no offset
                        idx2[ i2 ].size += idx[i].size;
                        idx2[ i2 ].uncompressedSize += idx[i].uncompressedSize;
                    } else {
                        idx2[ i2 ] = idx[i];
                    }
                }
            } else {
                // some will be empty
                for(int64_t i = 0 ; i < indexCount; i++) {
                    int64_t i2 = i * THREADS / indexCount;
                    assert(i2 < THREADS);
                    idx2[ i2 ] = idx[i];
                }
            }
            free_chk(idx);
            idx = idx2;
            indexCount = THREADS;
            for(int64_t i = 0; i < indexCount; i++) {
                DBG("Converted Index %d: offset=%lld size=%lld\n", i, idx[i].offset, idx[i].size);
            }
        }
        LOGF("Read index file %s in %0.3fs\n", indexFileName, now() - start);
    }
    double startAllOps = now();
    IndexElement *myElement = (IndexElement*) scatter_long((int64_t*) idx, sizeof(IndexElement)/sizeof(int64_t), THREADS-1);
    ourFileSize = broadcast_long(ourFileSize, THREADS-1);
    LOGF("Finished scatter and broadcast in %0.3fs %0.3fs since start\n", now() - startAllOps, now() - start);

    if (idx) free_chk(idx);
    LOGF("got my IndexElement offset=%lld size=%lld\n", (lld) myElement->offset, (lld) myElement->size);

    // aggreagate reads and communicate if sizes are very small
    SharedReadBuffer readBuffer = NULL;
    int64_t activeThreads = (ourFileSize + READ_BLOCK_SIZE - 1) / READ_BLOCK_SIZE;
    if (ourFileSize < READ_BLOCK_SIZE * THREADS) {
        LOGF("Reading ourFile %s via ReadBuffer. activeThreads=%lld ourFileSize=%lld\n", ourFile, (lld) activeThreads, (lld) ourFileSize);
        assert(activeThreads <= THREADS);
        assert(activeThreads > 0);
        // only some threads will be reading and populating the readBuffer
        UPC_ALL_ALLOC_CHK(readBuffer, THREADS, sizeof(ReadBuffer)); // one block per thread
        int64_t myPartition = MYTHREAD * activeThreads / THREADS;
        int64_t myPartitionThread = (myPartition * THREADS + activeThreads - 1) / activeThreads;
        if (MYTHREAD == myPartitionThread) {
            int64_t myBlock = myPartition;
            int64_t myOffset = myBlock * READ_BLOCK_SIZE;
            int64_t myBytes = ourFileSize > myOffset + READ_BLOCK_SIZE ? READ_BLOCK_SIZE : ourFileSize - myOffset;
            if (myBytes > 0) {
                LOGF("Active thread reading block %lld at offset %lld for %lld bytes\n", myBlock, myOffset, myBytes);
                FILE *our = fopen_chk(ourFile, "r");
                if (fseek(our, myOffset, SEEK_SET) != 0) {
                    DIE("Failed to fseek(%lld, SEEK_SET) on %s. errno=%d %s\n", (lld) myOffset, ourFile, errno, strerror(errno));
                }
                int64_t bytes = fread_chk((void*) (&readBuffer[MYTHREAD].buf), 1, myBytes, our);
                if (bytes != myBytes) DIE("Could not read %lld bytes, got %lld at offset=%lld f=%s errno=%d error=%s\n", (lld) myBytes, (lld) bytes, (lld) myOffset, ourFile, errno, strerror(errno));
                fclose_track(our);
            } else {
                LOGF("Active thread not reading any bytes: ourFileSize=%lld myOffset=%lld\n", ourFileSize, myOffset);
            }
        }
        UPC_LOGGED_BARRIER;
    } else {
        assert(activeThreads > THREADS);
        activeThreads = THREADS;
    }

    char tmpFile[MAX_FILE_PATH];
    sprintf(tmpFile, "%s.tmp", myfile);
    FILE *my = fopen_chk(tmpFile, "w"); // always create a file even if it is zero bytes
    int64_t count = 0;
    if (myElement->size) {
        if (ourFileSize < READ_BLOCK_SIZE * THREADS) {
            // use the ReadBuffer
            int64_t bytesToRead = myElement->size;
            int64_t offset = myElement->offset;
            char * buf = malloc_chk(READ_BLOCK_SIZE);
            while (bytesToRead > 0) {
                int64_t readBufferPartition = (offset / READ_BLOCK_SIZE);
                int64_t readBufferThread = (readBufferPartition * THREADS + activeThreads - 1) / activeThreads;
                int64_t blockOffset = offset % READ_BLOCK_SIZE;
                int64_t blockRemaining = READ_BLOCK_SIZE - blockOffset;
                int64_t blockBytes = bytesToRead > blockRemaining ? blockRemaining : bytesToRead;
                LOGF("Reading ReadBuffer[%lld] offset=%lld bytesToRead=%lld blockOffset=%lld blockRemaining=%lld blockBytes=%lld readBufferPartition=%lld\n", (lld) readBufferThread, (lld) offset, (lld) bytesToRead, (lld) blockOffset, (lld) blockRemaining, (lld) blockBytes, (lld) readBufferPartition);
                upc_memget(buf, ((shared[] int8_t * ) &(readBuffer[readBufferThread].buf)) + blockOffset, blockBytes);
                int64_t bytes = fwrite_chk(buf, 1, blockBytes, my);
                if (bytes != blockBytes) DIE("Could not write %lld bytes, only %lld at offset=%lld. f=%s errno=%d error=%s\n", (lld) blockBytes, (lld) bytes, (lld) bytesToRead, tmpFile, errno, strerror(errno));
                bytesToRead -= blockBytes;
                offset += blockBytes;
            }
            free_chk(buf);
        } else {
            // Read directly from ourFile
            FILE *our = fopen_chk(ourFile, "r");
            if (fseek(our, myElement->offset, SEEK_SET) != 0) {
                DIE("Failed to fseek(%lld, SEEK_SET) on %s. errno=%d %s\n", (lld) myElement->offset, ourFile, errno, strerror(errno));
            }
            char *buf = malloc_chk(MINIMUM_WRITE_BLOCK);
            while (count < myElement->size) {
                int64_t block = myElement->size - count;
                block = (block > MINIMUM_WRITE_BLOCK) ? MINIMUM_WRITE_BLOCK : block;
                int64_t bytes = fread_chk(buf, 1, block, our);
                if (bytes != block) DIE("Could not read %lld bytes, got %lld at offset=%lld (started at %lld) f=%s errno=%d error=%s\n", (lld) block, (lld) bytes, (lld) myElement->offset + count, (lld) myElement->offset, ourFile, errno, strerror(errno));
                bytes = fwrite_chk(buf, 1, block, my);
                if (bytes != block) DIE("Could not write %lld bytes, only %lld at offset=%lld. f=%s errno=%d error=%s\n", (lld) block, (lld) bytes, (lld) count, tmpFile, errno, strerror(errno));
                count += bytes;
            }
            free_chk(buf);
            fclose_track(our);
        }
    } else if ( strcmp(myfile + strlen(myfile) - 3, ".gz") == 0 ) {
        LOGF("%s is empty but a gzip file, using the 20 byte gzip empty file instead\n", myfile);
        // 0000000: 1f8b 0800 3eda 445c 0003 0300 0000 0000  ....>.D\........
        // 0000010: 0000 0000                                ....

        static const unsigned char EMPTY_GZIP[] = {
           0x1f, 0x8b, 0x08, 0x00, 0x3e, 0xda, 0x44, 0x5c, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
           0x00, 0x00, 0x00, 0x00
        };

        int64_t bytes = fwrite_chk(EMPTY_GZIP, 1, 20, my);
        if (bytes != 20) DIE("Could not write %lld bytes, only %lld\n", (lld) 20, (lld) bytes);
        count += bytes;
    }
    int64_t mtime = 0;
    if (MYTHREAD == THREADS-1) {
        struct stat stat1;
        int s1 = stat(ourFile, &stat1);
        mtime = stat1.st_mtime;
    }
    fclose_track(my);
    if ( rename(tmpFile, myfile) ) {
        DIE("Could not rename restored aggregated file %s to %s! %s\n", tmpFile, myfile, strerror(errno));
    }
    if (ourFileSize < READ_BLOCK_SIZE * THREADS) {
        UPC_ALL_FREE_CHK(readBuffer);
    }
    double myTime =  now() - start;
    double myMB = myElement->size * PER_MB;
    clear_lustre_caches();
    UPC_LOGGED_BARRIER;
    mtime = broadcast_long(mtime, THREADS-1);
    struct timeval times[2];
    memset(&times, 0, sizeof(times));
    times[0].tv_sec = mtime-1;
    times[1].tv_sec = mtime-1;
    utimes(myfile, times);
    double totalMB = reduce_double(myMB, UPC_ADD, ALL_DEST);
    double avgTime = reduce_double(myTime, UPC_ADD, ALL_DEST) / THREADS;
    double globalTime = now() - start;
    LOGF("Restored %lld bytes to local file mtime=%lld in %0.3fs\n", (lld) myElement->size, globalTime, (lld) mtime);
    serial_printf("Restored %0.3f MB to %d threads in %0.3f s, %0.3f s avg, %0.3f MB/s globally, imbalance=%0.2f\n", totalMB, THREADS, globalTime, avgTime, totalMB / globalTime, avgTime / globalTime); 
    free_chk(myElement);
}


void lazyOpenAppendFile(AppendFile file, int flags)
{
    file->fd = open_chk(file->ourFileName, flags, 00666);

}

AppendFile _openAppendFile(const char *filename)
{
    AppendFile af = calloc_chk(1, sizeof(*af));
    strcpy(af->ourFileName, filename);
    af->buffering = initBuffer(MINIMUM_WRITE_BLOCK);
#ifndef HIPMER_NO_AIO
    af->inFlight = initBuffer(MINIMUM_WRITE_BLOCK);
#endif
    return af;
}

AppendFile openMyAppendFile(const char *myFileName)
{
    AppendFile af = _openAppendFile(myFileName);
    UPC_ALLOC_CHK(af->mergedOffset, sizeof(int64_t));
    af->mergedOffset = 0;
    lazyOpenAppendFile(af, O_CREAT|O_WRONLY|O_TRUNC);
    return af;
}

AppendFile openAppendFile(const char *ourFileName)
{
    AppendFile af = _openAppendFile(ourFileName);
    UPC_ALL_ALLOC_CHK(af->mergedOffset, 1, sizeof(int64_t));
    if (!MYTHREAD) {
        lazyOpenAppendFile(af, O_CREAT|O_WRONLY|O_TRUNC);
    } else {
        af->fd = -1;
    }
    UPC_LOGGED_BARRIER;
    return af;
}

void closeAppendFile(AppendFile *_file)
{
    assert(_file != NULL);
    AppendFile file = *_file;
    assert(file != NULL);

    // consolidate small remaining file->buffering Buffers
    shared [1] int64_t *ourSizes = NULL;
    UPC_ALL_ALLOC_CHK(ourSizes, THREADS, sizeof(long));
    consolidateSmallBuffers(file->buffering, ourSizes, MINIMUM_WRITE_BLOCK);
    UPC_ALL_FREE_CHK(ourSizes);

    // flush any remaining in buffering
    flushAppendFile(file);

    // flush any newly in flight
    flushInFlightAppendFile(file);

    UPC_LOGGED_BARRIER;
    if (file->fd >= 0) {
        close_track(file->fd);
    }
    if (!MYTHREAD) {
        int64_t finalSize = *file->mergedOffset;
        LOGF("Closed %s with final size %lld\n", file->ourFileName, (lld) finalSize);
    }
    UPC_ALL_FREE_CHK(file->mergedOffset);
#ifndef HIPMER_NO_AIO
    if (file->_aio == NULL) DIE("!\n");
#endif
    if (file->buffering) {
        assert(getReadLengthBuffer(file->buffering) == 0);
        freeBuffer(file->buffering);
    }
    if (file->inFlight) {
        assert(getReadLengthBuffer(file->inFlight) == 0);
        freeBuffer(file->inFlight);
    }
    *_file = NULL;
}

void flushInFlightAppendFile(AppendFile file) {
#ifndef HIPMER_NO_AIO
    if (file->_aio) {
        int err = 0;
        while(1) {
            err = aio_error(file->_aio);
            if (err == ECANCELED) { WARN("aio was cancelled! %s\n", file->ourFileName); break; }
            if (err > 0) { DIE("aio had an error %d (%s)! %s\n", err, strerror(err), file->ourFileName); break; }
            if (err == 0) { break; }
            assert(err == EINPROGRESS);
        }
        free_chk(file->_aio);
        file->_aio = NULL;
        resetBuffer(file->inFlight);
    }
#endif
}

void flushAppendFile(AppendFile file)
{
    flushInFlightAppendFile(file);
    if (getReadLengthBuffer(file->buffering)) {
        if (file->fd < 0) {
           lazyOpenAppendFile(file, O_WRONLY);
        }
        // write some
        int64_t mySize = getReadLengthBuffer(file->buffering);
        int64_t myOffset = 0;
        UPC_ATOMIC_FADD_I64(&myOffset, file->mergedOffset, mySize);
#ifndef HIPMER_NO_AIO
        file->_aio = calloc_chk(1, sizeof(*file->_aio));
        assert(isValidBuffer(file->inFlight));
        assert(getReadLengthBuffer(file->inFlight) == 0);
        swapBuffer(file->buffering, file->inFlight);
        assert(getReadLengthBuffer(file->buffering) == 0);
        assert(getReadLengthBuffer(file->inFlight) == mySize);

        file->_aio->aio_fildes = file->fd;
        file->_aio->aio_offset = myOffset;
        file->_aio->aio_buf = getCurBuffer(file->inFlight);
        file->_aio->aio_nbytes = mySize;
        file->_aio->aio_reqprio = 0;
        file->_aio->aio_sigevent.sigev_notify = SIGEV_NONE;
        file->_aio->aio_lio_opcode = 0;
        int err = aio_write(file->_aio);
        if (err != 0) {
            DIE("Could not call aio_write (return %d) on %lld bytes! %s %s\n", err, (lld)mySize, strerror(errno), file->ourFileName);
        }
#else
        writeFDBuffer(file->buffering, file->fd, myOffset);
#endif
    }
}

