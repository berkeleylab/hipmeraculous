#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <upc.h>

#include "upc_common.h"
#include "common.h"
#include "upc2obj.h"

uint8_t *upc2obj_upc_alloc(size_t n)
{
    assert(UPC_SHARED_PTR_SIZE == sizeof(SharedChar));
    uint8_t *localPtr = NULL;
    DBG3("upc2obj: attempting to upc_alloc(%lld)\n", (lld)n);
    SharedChar myPtr = (SharedChar)upc_alloc(n + sizeof(SharedChar));
    if (myPtr == NULL) {
        WARN("Out of memory!!!\n"); return localPtr;
    }
    localPtr = (uint8_t *)myPtr;
    memcpy(localPtr, &myPtr, sizeof(SharedChar));
    localPtr += sizeof(SharedChar);
    memset(localPtr, 0, n);
    DBG2("upc2obj: allocated %lld(+%lld) bytes: %p, %lld (%p).\n", (lld)n, (lld)sizeof(SharedChar), localPtr, (lld)upc_addrfield(myPtr), (uint8_t *)myPtr);
    return localPtr;
}

void upc2obj_upc_free(uint8_t *ptr)
{
    if (ptr != NULL) {
        SharedChar myPtr = NULL;
        memcpy(&myPtr, ptr - sizeof(SharedChar), sizeof(SharedChar));
        assert(upc_threadof(myPtr) == MYTHREAD);
        DBG2("upc2obj: freeing %p, %lld (%p).\n", ptr, (lld)upc_addrfield(myPtr), ptr - sizeof(SharedChar));
        upc_free(myPtr);
        DBG3("upc2obj: freed %p\n", ptr);
    }
}

upc2obj_tuple upc2obj_getTuple(uint8_t *ptr)
{
    upc2obj_tuple x;

    x._local_ptr = ptr;
    SharedChar myPtr = NULL;
    memcpy(&myPtr, x._local_ptr - sizeof(SharedChar), sizeof(SharedChar));
    assert(upc_threadof(myPtr) == MYTHREAD);
    x._shared_ptr._ptr = myPtr + sizeof(SharedChar);
    return x;
}

uint8_t *upc2obj_cast_local(upc2obj_tuple ptr)
{
    return ptr._local_ptr;
}

void upc2obj_startUPC()
{
    DBG1("upc2obj_startUPC(): starting barrier\n");
    /* Should put MPI_Barrier here, but running into compiler issues on edison.. moved to upc_allocator.hpp */
    DBG2("upc2obj_startUPC(): ready\n");
}

void upc2obj_endUPC()
{
    DBG1("upc2obj_endUPC(): waiting\n");
    upc_barrier; /* Actually wait */
    DBG2("upc2obj_endUPC(): done\n");
}
