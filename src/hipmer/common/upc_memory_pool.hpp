/*-
 * Copyright (c) 2013 Cosku Acay, http://www.coskuacay.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Taken from https://github.com/cacay/MemoryPool.git
 * Modified to use upc_allocator.hpp by Rob Egan
 */

#ifndef MEMORY_POOL_H
#define MEMORY_POOL_H

#include <memory>
#include <climits>
#include <cstddef>

#ifndef USE_PRIVATE_MEM_TOO
#define USE_PRIVATE_MEM_TOO 1
#endif

#if USE_PRIVATE_MEM_TOO
//#pragma message("Using private memory along with UPC")
#endif

#ifndef BROKEN_ALLOCATOR_REBIND
#ifdef __INTEL_COMPILER
#define BROKEN_ALLOCATOR_REBIND 1
#endif
#endif

#ifdef BROKEN_ALLOCATOR_REBIND
//#pragma message "Using alternate method for allocator rebind"
#endif

// BlockCount defaults to the number of objects that can safely fit within 8 MB
#ifndef DEFAULT_BLOCK_SIZE
#define DEFAULT_BLOCK_SIZE (8 * 1024 * 1024)
#endif

#include "upc_allocator.hpp"

namespace upc_allocator {
// BlockCount defaults to the number of objects that can safely fit within the block size
#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount = (DEFAULT_BLOCK_SIZE - 64) / sizeof(T)>
#else
template <typename T>
#endif
class MemoryPool : public upc_allocator::simple_allocator<T>
{
public:
/* Member types */
typedef T value_type;
typedef T *pointer;
typedef T&              reference;
typedef const T *const_pointer;
typedef const T&        const_reference;
typedef size_t size_type;
typedef ptrdiff_t difference_type;
typedef std::false_type propagate_on_container_copy_assignment;
typedef std::true_type propagate_on_container_move_assignment;
typedef std::true_type propagate_on_container_swap;
#ifndef BROKEN_ALLOCATOR_REBIND
#else
static const long BlockCount = (DEFAULT_BLOCK_SIZE - 64) / sizeof(T);
#endif
static const long BlockSize = BlockCount * sizeof(T);

template <typename U> struct rebind {
#ifndef BROKEN_ALLOCATOR_REBIND
    typedef MemoryPool<U, (BlockCount *sizeof(T) / sizeof(U) - 1)> other;
#else
    typedef MemoryPool<U> other;
#endif
};

typedef typename upc_allocator::simple_allocator<T> UPCAllocator;

/* Member functions */
MemoryPool();
MemoryPool(const MemoryPool& memoryPool);
MemoryPool(MemoryPool && memoryPool);
template <class U> MemoryPool(const MemoryPool<U>& memoryPool);

virtual ~MemoryPool();

MemoryPool& operator=(const MemoryPool& memoryPool) = delete;
MemoryPool& operator=(MemoryPool && memoryPool);

pointer address(reference x) const;
const_pointer address(const_reference x) const;

// Optimized to allocate one object at a time.
// if n>1 then a new stand-alone block is created and *must* be deallocated with the same (p,n) parameters.
// hints are ignored
pointer allocate(size_type n = 1, const_pointer hint = 0);
void deallocate(pointer p, size_type n = 1);

size_type max_size() const;

template <class U, class ... Args> void construct(U * p, Args && ... args);
template <class U> void destroy(U *p);

template <class ... Args> pointer newElement(Args && ... args);
void deleteElement(pointer p);

private:
union Slot_ {
    value_type element;
    Slot_ *    next;
};

typedef char *data_pointer_;
typedef Slot_ slot_type_;
typedef Slot_ *slot_pointer_;

slot_pointer_ currentBlock_;
slot_pointer_ currentSlot_;
slot_pointer_ lastSlot_;
slot_pointer_ freeSlots_;
#if USE_PRIVATE_MEM_TOO
int64_t numBlocks;
#endif

size_type padPointer(data_pointer_ p, size_type align) const;
void allocateBlock();

static_assert(BlockSize >= 2 * sizeof(slot_type_), "BlockSize too small.");
};

#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t TC, typename U, size_t UC>
inline bool operator ==(const MemoryPool<T, TC>&, const MemoryPool<U, UC>&)
{
    return sizeof(T) * TC == sizeof(U) * UC;
}
#else
template <typename T, typename U>
inline bool operator ==(const MemoryPool<T>&a, const MemoryPool<U>&b)
{
    return a.BlockSize == b.BlockSize;
}
#endif

#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t TC, typename U, size_t UC>
inline bool operator !=(const MemoryPool<T, TC>& a, const MemoryPool<U, UC>& b)
#else
template <typename T, typename U>
inline bool operator !=(const MemoryPool<T>& a, const MemoryPool<U>& b)
#endif
{
    return !(a == b);
}

#include "upc_memory_pool.tcc"


// SingletonMemoryPool - pseudo singleton pattern.
// One instance until a destrutor is called then a new one will be used at the next constructor
// This allows the final destructor for a set to deallocate the used memory
#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t BlockCount = (DEFAULT_BLOCK_SIZE - 24) / sizeof(T)>
class SingletonMemoryPool : public MemoryPool<T, BlockCount>
#else
template <typename T>
class SingletonMemoryPool : public MemoryPool<T>
#endif
{
public:
typedef T value_type;
typedef T *pointer;
typedef T&              reference;
typedef const T *const_pointer;
typedef const T&        const_reference;
typedef size_t size_type;
typedef ptrdiff_t difference_type;
typedef std::false_type propagate_on_container_copy_assignment;
typedef std::true_type propagate_on_container_move_assignment;
typedef std::true_type propagate_on_container_swap;
template <typename U> struct rebind {
#ifndef BROKEN_ALLOCATOR_REBIND
    typedef SingletonMemoryPool<U, (BlockCount *sizeof(T) / sizeof(U) - 1)> other;
#else
    typedef SingletonMemoryPool<U> other;
#endif
};
#ifndef BROKEN_ALLOCATOR_REBIND
typedef MemoryPool<T, BlockCount> MP;
#else
typedef MemoryPool<T> MP;
#endif

/* Member functions */
SingletonMemoryPool() : _myInstance(get())
{
    DBG2("SingletonMemoryPool(): %p %p\n", this, _myInstance.get());
}
SingletonMemoryPool(const SingletonMemoryPool& memoryPool) : _myInstance(memoryPool._myInstance)
{
    DBG2("SingletonMemoryPool(const SMP& %p): %p %p\n", &memoryPool, this, _myInstance.get());
}
SingletonMemoryPool(SingletonMemoryPool && memoryPool) : _myInstance(memoryPool._myInstance)
{
    DBG2("SingletonMemoryPool(cosnt SMP&& %p): %p %p\n", &memoryPool, this, _myInstance.get());  memoryPool._myInstance.reset();
}
template <class U> SingletonMemoryPool(const SingletonMemoryPool<U>& memoryPool) : _myInstance(get())
{
    DBG2("SingletonMemoryPool( const SMP<U>& %p): %p %p\n", &memoryPool, this, _myInstance.get());
}

// destructor call unsets static variable -- new get() calls will instantiate a new MP, last destructure will call ~MP
virtual ~SingletonMemoryPool()
{
    if (_myInstance) {
        DBG2("~SingletonMemoryPool(): %p %p (static %p)\n", this, _myInstance.get(), _get().get()); _get().reset(); _myInstance.reset();
    }
}

SingletonMemoryPool &operator=(const SingletonMemoryPool& memoryPool) = delete;
SingletonMemoryPool &operator=(SingletonMemoryPool && other)
{
    _myInstance = other._myInstance;
    other._myInstance.reset();
    return *this;
}

pointer allocate(size_type n = 1, const_pointer hint = 0)
{
    return _myInstance->allocate(n, hint);
}
void deallocate(pointer p, size_type n = 1)
{
    return _myInstance->deallocate(p, n);
}
template <class U, class ... Args> void construct(U *p, Args && ... args)
{
    return _myInstance->construct(p, std::forward<Args>(args) ...);
}
template <class U> void destroy(U *p)
{
    _myInstance->destroy(p);
}

template <class ... Args> pointer newElement(Args && ... args)
{
    return _myInstance->newElement(std::forward<Args>(args) ...);
}
void deleteElement(pointer p)
{
    _myInstance->deleteElement(p);
}

bool isEqual(const SingletonMemoryPool &other) const
{
    return _get() == other._get();
}
private:
static std::shared_ptr<MP> &_get()
{
    static shared_ptr<MP> _;
    return _;
}
static std::shared_ptr<MP> &get()
{
    std::shared_ptr<MP> &_ = _get();
    if (!_) {
        _.reset(new MP()); DBG("SingletonMemoryPool::get(): new %p\n", _.get());
    }
    return _;
}
std::shared_ptr<MP> _myInstance;
};

#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t TC, typename U, size_t UC>
inline bool operator ==(const SingletonMemoryPool<T, TC>&a, const SingletonMemoryPool<U, UC>&b)
#else
template <typename T, typename U>
inline bool operator ==(const SingletonMemoryPool<T>&a, const SingletonMemoryPool<U>&b)
#endif
{
    return a.isEqual(b);
}

#ifndef BROKEN_ALLOCATOR_REBIND
template <typename T, size_t TC, typename U, size_t UC>
inline bool operator !=(const SingletonMemoryPool<T, TC>& a, const SingletonMemoryPool<U, UC>& b)
#else
template <typename T, typename U>
inline bool operator !=(const SingletonMemoryPool<T>& a, const SingletonMemoryPool<U>& b)
#endif
{
    return !(a == b);
}
}; // end namespace upc_allocator

#endif // MEMORY_POOL_H
