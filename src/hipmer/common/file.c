#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>
#include <stdarg.h>
#include <zlib.h>

// INCLUDE other common header files too

#include "optlist.h"

#include "version.h"
#include "colors.h"
#include "common.h"
#include "common_rankpath.h"

#include "StaticVars.h"
#include "log.h"

#include "file.h"

/****************************************************************************************/

int fclose_track_at_line(FILE *f, const char *which_file, int which_line)
{
#ifdef TRACK_FILE_OPENS
    fflush(stderr);
    LOG("\n%p/%d CLOSE. %s:%d\n", f, MYTHREAD, which_file, which_line);
    fflush(stderr);
#endif
    //int64_t size = ftell(f);
    if (!f) {
        WARN("fclose called on NULL fh! (%s:%d)\n", which_file, which_line);
    }
    int ret = fclose(f);
    int isMyLog = _sv == NULL ? 0 : 1;
    if (isMyLog) {
        isMyLog = f == MYSV._my_log;
    }
    if (isMyLog) {
        MYSV._my_log = NULL;          // check to ensure _my_log is never used again even in this function
    }
    if (ret != 0) {
        DIE("Could not close file: %p! (%s:%d) errno=%d %s\n", f, which_file, which_line, errno, strerror(errno));
    }
    //if (!isMyLog) LOGF("fclose_track(%p) finished. %lld bytes. %s:%d\n", f, (lld) size, which_file, which_line);
    return ret;
}

FILE *fopen_track_at_line(const char *path, const char *mode, const char *which_file, int which_line)
{
    FILE *f = fopen(path, mode);

    if (!f) {
        WARN("Could not open %s with '%s' mode: %s. (%s:%d) %s\n", path, mode, strerror(errno), which_file, which_line, strerror(errno));
    }
#ifdef TRACK_FILE_OPENS
    fflush(stderr);
//    if (_sv != NULL && MYSV._my_log) LOGF("\n%p/%d OPEN %s (%s:%d)\n", f, MYTHREAD, path, which_file, which_line);
    fflush(stderr);
#else
//    if (_sv != NULL && MYSV._my_log) LOGF("Opening %s with mode '%s': %s  (%s:%d)\n", path, mode, f == NULL ? "FAILED!" : "success.", which_file, which_line);
#endif
    return f;
}

FILE *fopen_chk_at_line(const char *path, const char *mode, const char *which_file, int which_line)
{
    FILE *f = fopen_track_at_line(path, mode, which_file, which_line);

    if (!f) {
        DIE("(%s:%d-%s) Could not open file '%s': %s\n", which_file, which_line, HIPMER_VERSION, path, strerror(errno));
    }
    return f;
}

size_t fread_chk_at_line(void *ptr, size_t size, size_t nmemb, FILE *stream, const char *which_file, int which_line) 
{
    if (size == 0 || nmemb == 0) {
        WARN("(%s:%d) fread called on 0 bytes: size=%lld nmemb=%lld\n", which_file, which_line, (lld) size, (lld) nmemb);
        return 0;
    }
    size_t nRead = fread(ptr, size, nmemb, stream);
    if (nRead != nmemb) {
        int err;
        if (feof(stream)) {
            // okay, not expected though
            err = ferror(stream); // check error too
            WARN("(%s:%d) fread(size=%lld, nmemb=%lld) to end of stream returned %lld elements (%lld bytes not the requested %lld) ferror=%d ftell=%lld\n", which_file, which_line, (lld) size, (lld) nmemb, (lld) nRead, (lld) nRead * size, (lld) nmemb * size, err, (lld) ftell(stream));
        } else if ((err = ferror(stream))) {
            DIE("(%s:%d) fread(size=%lld, nmemb=%lld) returned an ERROR(ferror=%d) and only %lld elements (%lld bytes not the requested %lld) possibly because: %s\n", which_file, which_line, (lld) size, (lld) nmemb, err, (lld) nRead, (lld) nRead * size, (lld) nmemb * size, strerror(errno)); // errno may not be set but output it anyway
        }
    }
    return nRead;
}

size_t fwrite_chk_at_line(const void *ptr, size_t size, size_t nmemb, FILE *stream, const char *which_file, int which_line)
{
    if (size == 0 || nmemb == 0) {
        WARN("(%s:%d) fwrite called on 0 bytes: size=%lld nmemb=%lld\n", which_file, which_line, (lld) size, (lld) nmemb);
        return 0;
    }
    size_t nWritten = fwrite(ptr, size, nmemb, stream);
    if (nWritten != nmemb) {
        int err;
        if (feof(stream)) {
            // This is a disk error
            DIE("(%s:%d) fwrite(size=%lld, nmemb=%lld) to end of stream returned %lld elements!!! THIS SHOULD NOT HAPPEN!!!! (%lld bytes not the requested %lld) ftell=%lld\n", which_file, which_line, (lld) size, (lld) nmemb, (lld) nWritten, (lld) nWritten * size, (lld) nmemb * size, (lld) ftell(stream));
        } else if ((err = ferror(stream))) {
            DIE("(%s:%d) fwrite(size=%lld, nmemb=%lld) returned an ERROR(ferror=%d) and only %lld elements (%lld bytes not the requested %lld) ftell=%lld possibly because: %s\n", which_file, which_line, (lld) size, (lld) nmemb, err, (lld) nWritten, (lld) nWritten * size, (lld) nmemb * size, (lld) ftell(stream), strerror(errno)); // errno may not be set but output it anyway
        }
    }
    return nWritten;
}


int open_chk_at_line(const char *path, int flags, mode_t mode, const char *which_file, int which_line)
{
    int fd = open(path, flags, mode);

    if (fd < 0) {
        WARN("Could not open %s with '%s' mode: %d. (%s:%d) %s\n", path, flags, strerror(errno), which_file, which_line, strerror(errno));
    }
    return fd;
}

int close_track_at_line(int fd, const char *which_file, int which_line)
{
    int ret = close(fd);
    if (ret < 0) {
        DIE("Could not close file: %d! (%s:%d) errno=%d %s\n", fd, which_file, which_line, errno, strerror(errno));
    }
    return ret;
}


gzFile gzopen_track_at_line(const char *path, const char *_mode, const char *which_file, int which_line)
{
    char mode[5];

    // choose fastest compression mode if not chosen already
    if ((strcmp(_mode, "w") == 0 || strcmp(_mode, "wb") == 0 || strcmp(_mode, "a") == 0 || strcmp(_mode, "ab") == 0)) {
        sprintf(mode, "%s1", _mode);
    } else {
        strcpy(mode, _mode);
    }
//    LOGF("gzopening %s with %s mode (%s-%d)\n", path, mode, which_file, which_line);
    gzFile f = gzopen(path, mode);
#if ZLIB_VERNUM >= 0x1240
    gzbuffer(f, 128 * 1024);
#endif
    if (!f) {
        WARN("(%s:%d) Could not open %s with %s mode! %s\n", which_file, which_line, path, mode, strerror(errno));
    }
    return f;
}

gzFile gzopen_chk_at_line(const char *path, const char *mode, const char *which_file, int which_line)
{
    gzFile f = gzopen_track(path, mode);

    if (!f) {
        DIE("(%s:%d) Could not open gzfile: %s! %s\n", which_file, which_line, path, strerror(errno));
    }
    return f;
}

int gzclose_track_at_line(gzFile gz, const char *which_file, int which_line)
{
//    int64_t size = gztell(gz);
    if (!gz) {
        WARN("gzclose called on null gzFile! (%s:%d)\n", which_file, which_line);
    }
    int status = gzclose(gz);

    if (Z_OK != status) {
        WARN("Could not close a gzFile! (%s:%d) %s\n", which_file, which_line, strerror(errno));
    }
//    LOGF("Closed gzip file. %lld bytes\n", (lld) size);
    return status == Z_OK ? 0 : 1;
}

void gzclose_chk_at_line(gzFile gz, const char *which_file, int which_line)
{
    if (!gzclose_track_at_line(gz, which_file, which_line)) {
        DIE("Could not close gzfile! (%s:%d) %s\n", which_file, which_line, strerror(errno));
    }
}


FILE *fopen_rank_path_at_line(char *buf, const char *mode, int rank, const char *which_file, int which_line)
{
    char *rankpath = get_rank_path(buf, rank);
    FILE *f = fopen_chk_at_line(rankpath, mode, which_file, which_line);

    return f;
}


gzFile gzopen_rank_path_at_line(char *buf, const char *mode, int rank, const char *which_file, int which_line)
{
    char *rankpath = get_rank_path(buf, rank);
    gzFile f = gzopen_chk_at_line(rankpath, mode, which_file, which_line);

    return f;
}

int64_t gzread_chk_at_line(gzFile file, char * buf, uint64_t _len, const char *which_file, int which_line) {
    int64_t len = _len;
    if (len == 0) {
        WARN("(%s:%d) gzread called requesting 0 bytes!\n", which_file, which_line);
        return 0;
    }
    int64_t totalRead = 0;
    while (len > 0) {
        int lenToRead = len < 1L<<28 ? (int) len : 1<<28; // batch in less than max int bytes
        int nRead = gzread(file, buf, lenToRead);
        if (nRead != lenToRead) {
            const char *err = NULL;
            int errnum;
            if (nRead == 0) {
                if (gzeof(file)) {
                    // okay, may be expected, just log
                    LOGF("(%s:%d) gzread(len=%d) to end of stream returned %d bytes\n", which_file, which_line, lenToRead, nRead, (lld) nRead);
                    len = 0;
                } else {
                    err = gzerror(file, &errnum);
                    DIE("(%s:%d) gzread(len=%d) returned an ERROR(gzerror=%d - %s) and only %d bytes were read. Possibly because: %s\n", which_file, which_line, lenToRead, errnum, err, nRead, strerror(errno));
                }
            } else if (nRead < 0) {
                err = gzerror(file, &errnum);
                DIE("(%s:%d) gzread(len=%d) has an unknown error state. It returned less than 0 (%d) with error (%d - %s). Possibly because: %s\n", which_file, which_line, lenToRead, nRead, errnum, err, strerror(errno));
            } else { 
                // okay try to keep reading
                LOGF("(%s:%d) gzread(len=%d) returned fewer bytes (%d) but is not eof or in error. Attempting to continue reading.\n", which_file, which_line, len, nRead);
            }
        }
        totalRead += nRead;
        buf += nRead;
        len -= nRead;
    }
    if (len != 0) {
        // This is likely okay as it should be end of file
        LOGF("(%s:%d) gzread(len=%d) returned just %d bytes (eof=%d). Remaining in destination buffer (len=)%d\n", which_file, which_line, _len, totalRead, gzeof(file), len);
    }
    return totalRead; 
}

int64_t gzwrite_chk_at_line(gzFile file, const char * buf, uint64_t _len, const char *which_file, int which_line)
{
    uint64_t len = _len;
    if (len == 0) {
        WARN("(%s:%d) gzwrite called requesting 0 bytes!\n", which_file, which_line);
        return 0;
    }
    uint64_t totalWritten = 0;
    while (len > 0) {
        int lenToWrite = len < 1L<<28 ? (int) len : 1<<28; // batch in less than max int bytes
        int nWritten = gzwrite(file, buf, lenToWrite);
        if (nWritten != lenToWrite) {
            const char *err = NULL;
            int errnum;
            if (nWritten <= 0) {
                err = gzerror(file, &errnum);
                DIE("(%s:%d) gzwrite(len=%d) returned an ERROR (%d) (gzerror=%d - %s). Possibly because: %s\n", which_file, which_line, lenToWrite, nWritten, errnum, err, strerror(errno));
            } else { 
                // This is likely okay, but unclear from the spec... try to continue anyway
                LOGF("(%s:%d) gzwrite(len=%d) returned fewer bytes (%d) but is not eof or in error. Attempting to continue writing.\n", which_file, which_line, lenToWrite, nWritten);
            }
        }
        totalWritten += nWritten;
        buf += nWritten;
        len -= nWritten;
    }
    if (len != 0) DIE("(%s:%d) gzwrite(%d) Unexpected error state remaining byte to be written (len=%d), expecting 0\n", which_file, which_line, _len, len);
    return totalWritten;
}


void link_chk_at_line(const char *old_file, const char *new_file, const char *which_file, int which_line)
{
    int ret = link(old_file, new_file);

    if (ret != 0) {
        if (errno == EEXIST) {
            ret = unlink(new_file);
            if (ret == 0) {
                return link_chk(old_file, new_file);         // try again!
            }
        } else if (errno == EPERM) {
            ret = symlink(old_file, new_file); // try a symlink!
            if (ret != 0) {
                WARN("(%s:%d) Could not link %s to %s! %s\n", which_file, which_line, old_file, new_file, strerror(errno));
            }
            return; // well at least we tried!
        }
    }
    if (ret != 0) {
        DIE("Could not link %s to %s! (%s:%d) %s\n", old_file, new_file, which_file, which_line, strerror(errno));
    }
}

size_t copyFile(const char *source, const char *dest)
{
    size_t written = 0;
    FILE *input;
    FILE *output; 

    input = fopen_chk(source, "r");
    output = fopen_chk(dest, "w");   //create and write to file
   
    written = copyFile2(input, output);

    fclose_track(input);
    fclose_track(output);

    return written;
}
size_t copyFile2(FILE *input, FILE *output)
{
    char * buf = malloc_chk(ONE_MB);
    size_t written = 0;
    int bytes; 
    while ( (bytes = fread_chk(buf, ONE_MB, 1, input)) > 0 ) {
        size_t wrote = fwrite_chk(buf, bytes, 1, output);
        written += wrote;
        if (wrote != bytes) DIE("Could not write all %lld of %lld bytes\n", (lld) wrote, (lld) bytes);
    }
    if (!feof(input)) DIE("Could not read to the end of (got %lld bytes)\n", (lld) written);

    return written;
}

