#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifndef USE_MPI
#define USE_MPI 1
#endif

#if USE_MPI
#include <mpi.h>
#endif

#define ONE_KB (1024LL)

#define MAX_FILE_PATH 384
#define MAX_RANKS_PER_DIR 1000

#define DBG(...)
#define DBG2(...)
#define WARN(...)

typedef long long int lld;

int purgeDir(const char *dir, lld *bytes, int *files, int *dirs)
{
    uid_t uid = getuid(), euid = geteuid();

    DBG2("purgeDir(%s) as %d(%d)\n", dir, (int)uid, (int)euid);
    int count = 0, _files = 0, _dirs = 0, __files = 0, __dirs = 0;
    lld _bytes = 0, __bytes = 0;
    if (bytes == NULL) {
        bytes = &__bytes;
    }
    if (files == NULL) {
        files = &__files;
    }
    if (dirs == NULL) {
        dirs = &__dirs;
    }
    DIR *d = opendir(dir);
    if (!d && errno == ENOENT) {
        // okay dir does not exist
        return 0;
    }
    // ignore permission denied errors - means directory owned by someone else
    if (!d && errno != EACCES) {
        WARN("Could not opendir %s! %s\n", dir, strerror(errno));
        return count;
    }
    //fprintf(stderr, "Opened dir %s\n", dir);
    struct dirent *result;
    int path_length;
    char path[MAX_FILE_PATH];
    struct stat s;
    while ((result = readdir(d)) != NULL) {
        path_length = snprintf(path, MAX_FILE_PATH,
                               "%s/%s", dir, result->d_name);
        if (path_length >= MAX_FILE_PATH) {
            WARN("Path length has got too long.\n");
            continue;
        }
        stat(path, &s);
        if (s.st_uid != uid && s.st_uid != euid) {
            DBG2("Skipping %s/%s as I do not own it (%d)\n", dir, result->d_name, (int)s.st_uid);
            continue;
        }
        if (S_ISDIR(s.st_mode)) {
            _dirs++;
            if (result->d_name[0] != '.') { // skip '.', '..', and anything starting with '.'
                /* Recursively call purgeDir  */
                count += purgeDir(path, bytes, files, dirs);
            }
        } else if (S_ISREG(s.st_mode) || S_ISLNK(s.st_mode)) {
            DBG("Unlinking %s\n", path);
            _files++;
            if (unlink(path) == 0) {
                _bytes += s.st_size;
                count++;
            } else {
              WARN("Unlink of %s returned an error: %s (%d)\n", path, strerror(errno), errno);
            }
        }
    }
    if (closedir(d) == 0) {
        if (strcmp(dir, "/dev/shm") == 0 || strcmp(dir, "/dev/shm/") == 0 || strcmp(dir, "/dev/shm/.") == 0) {
            DBG("Finished purgeDir %s\n", dir);
        } else {
            DBG("rmdir %s\n", dir);
            if (rmdir(dir) != 0) {
                WARN("Could not rmdir(%s)\n", dir);
            }
        }
    }

    *bytes += _bytes;
    *files += _files;
    *dirs += _dirs;
    DBG("Exiting %s: purged %lld bytes, %d files, %d dirs (total %lld %d %d)\n", dir, (lld)_bytes, (int)_files, (int)_dirs, (lld) * bytes, (int)*files, (int)*dirs);
    return count;
}

// returns 1 when it created the directory, 0 otherwise, -1 if there is an error
int check_dir(const char *path)
{
    if (0 != access(path, F_OK)) {
        if (ENOENT == errno) {
            // does not exist
            // note: we make the directory to be world writable, so others can delete it later if we
            // crash to avoid cluttering up memory
            mode_t oldumask = umask(0000);
            if (0 != mkdir(path, 0777) && 0 != access(path, F_OK)) {
                fprintf(stderr, "Could not create the (missing) directory: %s (%s)", path, strerror(errno));
                umask(oldumask);
                return -1;
            }
            umask(oldumask);
        }
        if (ENOTDIR == errno) {
            // not a directory
            fprintf(stderr, "Expected %s was a directory!", path);
            return -1;
        }
    } else {
        return 0;
    }
    assert(access(path, F_OK) == 0);
    return 1;
}

// replaces the given path with a rank based path, inserting a rank-based directory
// example:  get_rank_path("path/to/file_output_data.txt", rank) -> "path/to/per_rank/<rankdir>/<rank>/file_output_data.txt"
// of if rank == -1, "path/to/per_rank/file_output_data.txt"
char *get_rank_path(char *buf, int rank)
{
    int pathlen = strlen(buf);
    char newPath[MAX_FILE_PATH];
    char *lastslash = strrchr(buf, '/');
    int checkDirs = 0;
    int thisDir;
    char *lastdir = NULL;

    if (pathlen + 25 >= MAX_FILE_PATH) {
        fprintf(stderr, "File path is too long (max: %d): %s\n", MAX_FILE_PATH, buf);
        return NULL;
    }
    if (lastslash) {
        *lastslash = '\0';
    }
    if (rank < 0) {
        if (lastslash) {
            snprintf(newPath, MAX_FILE_PATH, "%s/per_rank/%s", buf, lastslash + 1);
            checkDirs = 1;
        } else {
            snprintf(newPath, MAX_FILE_PATH, "per_rank/%s", buf);
            checkDirs = 1;
        }
    } else {
        if (lastslash) {
            snprintf(newPath, MAX_FILE_PATH, "%s/per_rank/%08d/%08d/%s", buf, rank / MAX_RANKS_PER_DIR, rank, lastslash + 1);
            checkDirs = 3;
        } else {
            snprintf(newPath, MAX_FILE_PATH, "per_rank/%08d/%08d/%s", rank / MAX_RANKS_PER_DIR, rank, buf);
            checkDirs = 3;
        }
    }
    strcpy(buf, newPath);
    while (checkDirs > 0) {
        strcpy(newPath, buf);
        thisDir = checkDirs;
        while (thisDir--) {
            lastdir = strrchr(newPath, '/');
            if (!lastdir) {
                fprintf(stderr, "What is happening here?!?! newPath=%s\n", newPath); return NULL;
            }
            *lastdir = '\0';
        }
        check_dir(newPath);
        checkDirs--;
    }
    return buf;
}

const char *USAGE = "stress_lustre <directory> [num_files_per_rank=1] [num_kb_per_file=1]";
int main(int argc, char **argv)
{
    int size = 1, rank = 0, i;

#if USE_MPI
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

    char host[64];
    gethostname(host, 63);

    if (!rank) printf("%d: Started on %s\n", rank, host);

    char dir[MAX_FILE_PATH];
    int num_files_per_rank = 1;
    int num_kb_per_file = 1;
    if (argc < 2) {
      if (!rank)
        fprintf(stderr, "\nUSAGE: %s\n\n", USAGE);
      exit(1);
    }
    strncpy(dir, argv[1], MAX_FILE_PATH);
    if (argc >= 3)
      num_files_per_rank = atoi(argv[2]);
    if (argc >= 4)
      num_kb_per_file = atoi(argv[3]);
    if (!rank) printf("Writing into %s: %d files per rank of %d KB\n", dir, num_files_per_rank, num_kb_per_file);

    char my_rank_path[MAX_FILE_PATH];
    // make the top level dirs
    if (!rank) {
        if (!rank) check_dir(dir);
        strcpy(my_rank_path, dir);
        get_rank_path(my_rank_path, -1);
        printf("Created %s and %s\n", dir, my_rank_path);
    }

#if USE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    // prepare output buffer
    char *spam = calloc(ONE_KB+1, 1);
    for(long i = 0; i < ONE_KB; i++)
      *(spam+i) = i%95 + 32;
    // add newlines
    for(long i = 127 ; i < ONE_KB ; i+= 128)
      *(spam+i) = '\n';

    // prepare and make per_rank  dirs
    strcpy(my_rank_path, dir);
    get_rank_path(my_rank_path, rank);
    if (!rank) printf("%d: my path is %s\n", rank, my_rank_path);

#if USE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    if (!rank) printf("all per_rank dirs created\n");
    
    int status = 0;
    for (int file_i = 0; file_i < num_files_per_rank; file_i++) {
      char filename[MAX_FILE_PATH*2+1];
      sprintf(filename, "%s/file-%d.spam", dir, file_i);
      get_rank_path(filename, rank);
      if (!rank) printf("%d: Writing %d KB into %s\n", rank, num_kb_per_file, filename);
      FILE *f = fopen(filename, "w");
      if (!f) abort();
      for(int j = 0; j < num_kb_per_file; j++) {
        status = fwrite(spam, 1, ONE_KB, f);
        if (status != ONE_KB) abort();
      }
      status = fclose(f);
      if (status != 0) abort();
    }

    free(spam);

#if USE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    if (!rank) printf("%d: Removing per_rank directories like %s\n", rank, my_rank_path);
    lld bytes = 0;
    int files = 0, dirs = 0;
#ifndef NO_PURGE
    purgeDir(my_rank_path, &bytes, &files, &dirs);
    if (!rank) printf("%d: Removed %lld bytes, %d files and %d dirs\n", rank, bytes, files, dirs);
#endif


#if USE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    if (!rank) {
        printf("%d: Removing all empty directories under %s\n", rank, dir);
#ifndef NO_PURGE
        purgeDir(dir, &bytes, &files, &dirs);
        printf("%d: Removed %lld bytes, %d files and %d dirs\n", rank, bytes, files, dirs);
#endif
    }

#if USE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    if (!rank) printf("Completed purges.\n");

#if USE_MPI
    MPI_Finalize();
#endif
    
    if (!rank) printf("Success!\n");
    return 0;
}
