#!/bin/bash

# first parse any ENV=val command line arguments
job_env_args=()
for arg in "$@"
do
  if [ "${arg/=}" != "${arg}" ]
  then
    job_env_args+=("export ${arg};")
    setenvlog="$setenvlog
Setenv $arg"
    eval "export ${arg%=*}='${arg##*=}'"
    shift
  else
    break
  fi  
done

if [ -z "$SLURM_JOB_ID" ]
then
  echo "Not a SLURM job... not doing anything here" 1>&2
  exit 0
fi

if [ ! -f /proc/buddyinfo ]
then
  echo "/proc/buddyinfo is not present... can not check huge pages available" 1>&2
  exit 0
fi

NUM_NODES=${SLURM_NNODES}
[ -n "${NUM_NODES}" ] || NUM_NODES=${SLURM_JOB_NUM_NODES}
if [ -z "${NUM_NODES}" ]
then
  echo "Could not find the number of nodes in the job!" 1>&2
  exit 1
fi

TMPDIR=${TMPDIR:=/tmp}
MIN_NODES=${MIN_NODES:=${NUM_NODES}}
MAX_NODES=${MAX_NODES:=${NUM_NODES}}
CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_NTASKS_PER_NODE}}
[ -n "${CORES_PER_NODE}" ] || CORES_PER_NODE=${SLURM_CPUS_ON_NODE}

CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
CHECK_FREE_HUGEPAGES_SIZE_BYTES=${CHECK_FREE_HUGEPAGES_SIZE_BYTES:=$((2*1024*1024))}
CHECK_FREE_HUGEPAGES_ALLOCATE_MB=${CHECK_FREE_HUGEPAGES_ALLOCATE_MB:=$((CHECK_FREE_HUGEPAGES_MB/CORES_PER_NODE))}

check="$HIPMER_INSTALL/bin/check_hugepages"
if [ ! -x "$check" ]
then
  check=$(which check_hugepages)
fi
if [ ! -x "$check" ]
then
  echo "Could not find check_hugepages executable in the PATH" 1>&2
  exit 1
fi

if [ ${NUM_NODES} == 1 ] || ( [ -n "${MAX_NODES}" ] && [ ${MAX_NODES} == 1 ] )
then

  echo "NUM_NODES=$NUM_NODES MAX_NODES=$MAX_NODES so the head node must be the only remaining one"
  # just fake it:
  echo "110368 to 117152 MB on 0of8 $(echo $SLURM_NODELIST | sed 's/\[//; s/\-.*//;') Total" > $TMPDIR/host_mem.txt.$SLURM_JOB_ID

else

  echo "Executing check_hugepages at $(date)"
  set -x
  SLURM_BCAST=/tmp/check_hugepages-${SLURM_JOB_ID} srun -N ${NUM_NODES} -n ${NUM_NODES} --ntasks-per-node=1 --cpus-per-task=${CORES_PER_NODE} \
            ${check} ${CHECK_FREE_HUGEPAGES_SIZE_BYTES} "${CHECK_HUGEPAGES_PATTERN}" ${CORES_PER_NODE} ${CHECK_FREE_HUGEPAGES_ALLOCATE_MB} \
            > $TMPDIR/host_mem.txt.$SLURM_JOB_ID
  set +x

fi

# host_mem.txt should look like this -- 4 lines for each host -- just use the lower range of the Total lines for now:
#  60756 MB on 0of4 nid00040 Node 0, zone   Normal 
#  63546 MB on 0of4 nid00040 Node 1, zone   Normal 
#  121512 to 124302 MB on 0of4 nid00040 Total
#  8.163429 8.398313 8.280980 on 0of4 nid00040 Alloc 80000 MB over 32 threads min/max/avg hugetbl


# sort nodes by memory, exclude any low memory
grep Total $TMPDIR/host_mem.txt.$SLURM_JOB_ID | sort -rnk1 | awk '/Total/ && $1 >= '${CHECK_FREE_HUGEPAGES_MB}' {print $7}' > $TMPDIR/host_pass.txt.$SLURM_JOB_ID
passcount=$(cat $TMPDIR/host_pass.txt.$SLURM_JOB_ID | wc -l)

grep Total $TMPDIR/host_mem.txt.$SLURM_JOB_ID | sort -rnk1 | awk '/Total/ && $1 < '${CHECK_FREE_HUGEPAGES_MB}' {print}' > $TMPDIR/host_fail.txt.$SLURM_JOB_ID

if [ -s $TMPDIR/host_fail.txt.$SLURM_JOB_ID ]
then
  echo "Warning: The following nodes have significant Hugetable Memory Fragmentation.  Please contact your systems administrator to fix them:" 1>&2
  cat $TMPDIR/host_fail.txt.$SLURM_JOB_ID 1>&2
fi

keepcount=${MAX_NODES}
if [ $passcount -lt ${MIN_NODES} ]
then
  echo "WARNING: There are not enough nodes with sufficient memory. Have: $passcount Need: $MIN_NODES"
  echo "Using the best nodes then..."
  passcount=${MIN_NODES}
  grep Total $TMPDIR/host_mem.txt.$SLURM_JOB_ID | sort -rnk1 | awk '{print $7}' > $TMPDIR/host_pass.txt.$SLURM_JOB_ID
fi
 
if [ $passcount -gt ${MAX_NODES} ]
then
   echo "More nodes passed than MAX_NODES, dropping the remaining: passcount=$passcount MAX_NODES=$MAX_NODES"
   passcount=${MAX_NODES}
fi

if [ -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID.SOURCED ]
then
   echo "Already sourced $TMPDIR/new_slurm_env.$SLURM_JOB_ID . You ought not to resize the job a second time"
   exit 1
elif [ $NUM_NODES -gt $passcount ]
then
   echo "Excluding $((NUM_NODES-passcount)) nodes from the job (nodes=$NUM_NODES -> list=$SLURM_NODELIST)"
   head --lines=${passcount} $TMPDIR/host_pass.txt.$SLURM_JOB_ID | sort > $TMPDIR/host_keep.txt.$SLURM_JOB_ID
   >$TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID
   # ensure head node is still part of the job!
   host=${SLURMD_NODENAME}
   [ -n "${host}" ] || host=$(awk '{print $5}' $TMPDIR/host_mem.txt.$SLURM_JOB_ID | head -1)
   if [ -n "${host}" ] && ! grep -q ${host} $TMPDIR/host_keep.txt.$SLURM_JOB_ID 
   then
      keepcount=${passcount}
      if ! grep ${host} $TMPDIR/host_pass.txt.$SLURM_JOB_ID
      then
         # head node did not pass, append it anyway, but attempt to exclude it from execution of UPC
         keepcount=$((keepcount+1))
         echo "Keeping head node too"
         if [ $passcount -gt 1 ]
         then
             echo "Will try not to use the head node for execution"
             echo ${host} > $TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID
         fi
         echo ${host} >> $TMPDIR/host_keep.txt.$SLURM_JOB_ID
      else
        # head node did in fact pass requirements, but was excluded, put it back at the top
        (
         echo ${host}
         cat $TMPDIR/host_keep.txt.$SLURM_JOB_ID
        ) | head --lines=${keepcount} > $TMPDIR/host_keep.txt.tmp
        mv $TMPDIR/host_keep.txt.tmp $TMPDIR/host_keep.txt.$SLURM_JOB_ID
      fi
   fi
   newlist=$(cat $TMPDIR/host_keep.txt.$SLURM_JOB_ID | tr '\n' ',')
   newlist=${newlist%,}
   echo "Keeping $newlist"
   tpn=${SLURM_NTASKS_PER_NODE}
   [ -n "${tpn}" ] || tpn=$SLURM_CPUS_ON_NODE
   tasks=${SLURM_NTASKS}
   [ -n "$tasks" ] || tasks=$((tpn*NUM_NODES))
   newtasks=$((tasks*passcount/NUM_NODES))
   
   (
     echo "if [ ! -f '$TMPDIR/new_slurm_env.$SLURM_JOB_ID.SOURCED' ]; then"
     echo "    scontrol update JobId=${SLURM_JOB_ID} NodeList=${newlist} || /bin/true"
     echo "    ln -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID $TMPDIR/new_slurm_env.$SLURM_JOB_ID.SOURCED"
     echo "fi"
     echo "export SLURM_NODELIST=$newlist"
     echo "export SLURM_JOB_NODELIST=$newlist"
     echo "export SLURM_NNODES=$passcount"
     echo "export SLURM_JOB_NUM_NODES=$passcount"
     echo "export SLURM_CPUS_PER_NODE='$SLURM_CPUS_ON_NODE(x$passcount)'"
     echo "export SLURM_TASKS_PER_NODE='$tpn(x$passcount)'"
     echo "export SLURM_JOB_CPUS_PER_NODE='$SLURM_CPUS_ON_NODE(x$passcount)'"
     [ -z "$SLURM_NTASKS" ] || echo "export SLURM_NTASKS=${newtasks}"
     [ -z "$SLURM_NPROCS" ] || echo "export SLURM_NPROCS=${newtasks}"
     [ -z "$THREADS" ] || echo "export THREADS=$newtasks"
     if [ -s "$TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID" ]
     then
        echo "export UPC_SRUN_FLAGS='--exclude=$TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID'"
     fi
   ) > $TMPDIR/new_slurm_env.$SLURM_JOB_ID

   if [ -s "$TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID" ]
   then
     echo "Excluding the following nodes with low memory:"
     grep -f $TMPDIR/exclude_nodes.txt.$SLURM_JOB_ID $TMPDIR/host_mem.txt.$SLURM_JOB_ID
   fi
   echo "Expunging the following nodes with low memory from job $SLURM_JOB_ID"
   grep -v -f $TMPDIR/host_keep.txt.$SLURM_JOB_ID $TMPDIR/host_mem.txt.$SLURM_JOB_ID
   
   echo "You will need to source $TMPDIR/new_slurm_env.$SLURM_JOB_ID:"
   cat $TMPDIR/new_slurm_env.$SLURM_JOB_ID
else
   echo "All nodes will be kept in job $SLURM_JOB_ID"
fi

