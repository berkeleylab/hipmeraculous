#ifndef MAIN_MODULES_H_
#define MAIN_MODULES_H_

extern int meraculous_main(int argc, char **argv);
extern int contigMerDepth_main(int argc, char **argv);
extern int contigEndAnalyzer_main(int argc, char **argv);
extern int bubbleFinder_main(int argc, char **argv);
extern int findNewMers_main(int argc, char **argv);
extern int progressiveRelativeDepth_main(int argc, char **argv);
extern int localassm_main(int argc, char **argv);
extern int prefixMerDepthExtended_main(int argc, char **argv);
extern int merAligner_main(int argc, char **argv);
extern int merAlignerAnalyzer_main(int argc, char **argv);
extern int splinter_main(int argc, char **argv);
extern int spanner_main(int argc, char **argv);
extern int bmaToLinks_main(int argc, char **argv);
extern int oNo_main(int argc, char **argv);
extern int oNo_meta_main(int argc, char **argv);
extern int oNo_2D_main(int argc, char **argv);
extern int parCC_main(int argc, char **argv);
extern int merauder_main(int argc, char **argv);
extern int canonical_assembly_main(int argc, char **argv);
extern int loadfq_main(int argc, char **argv);
extern int read_shuffler_main(int argc, char **argv);

extern void createMeralignerInterModuleState();
extern void destroyMeralignerInterModuleState();

#endif
