#ifndef __EXEC_UTILS_H
#define __EXEC_UTILS_H

#include "config.h"
#include "StaticVars.h"
#include "timers.h"
#include "Buffer.h"
#include "upc_output.h"

#ifndef DBG
#if 1
#define DBG(...)                                \
    do {                                        \
        fprintf(stderr, __VA_ARGS__);           \
        fflush(stderr);                         \
    } while (0)
#else
#define DBG(...)
#endif
#endif

#define NOT_SCAFF 0
#define IS_SCAFF 1
#define MERGE_FLAG 0

#define BASE_DIR(b, local_dir) ((b) ? (_sv && MYSV.cached_io_path ? MYSV.cached_io_path : local_dir) : ".")

#define HASH_BAR "########################################################################"

char *name_i(const char *name, int k);
char *name_lib(const char *name, char *lib);
void add_to_list(Buffer list, const char *fmt, ...);
int exec_stage(char *stages, int is_scaff, int (*stage_main)(int, char **), const char *stage_name, ...);
void print_timings(cfg_t *cfg);
int64_t get_num_from_file(const char *prepend, const char *fname);
int64_t get_num_from_file_and_broadcast(const char *prepend, const char *fname);
char *get_str_from_file(const char *fname);
int64_t get_long_from_file_and_broadcast(const char *fname);
void put_str_in_file(const char *fname, char *s);
char *get_num_file_name(const char *prepend, const char *fname);
void put_num_in_file(const char *prepend, const char *fname, int64_t num);
int dummy_exec(char *stage_name);

#endif
