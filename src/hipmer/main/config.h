/*
 * The idea here is to get as many parameters as possible from the config file
 */

#ifndef __EXEC_CONFIG_H
#define __EXEC_CONFIG_H

#include "defines.h"

typedef struct {
    char *files_glob;
    char *name;
    int   ins_avg;
    int   std_dev;
    int   read_len;
    int   num;
    char   innie;
    char   rev_comp;
    char   for_contigging;
    char   ono_setid;
    char   for_gapclosing;
    char   five_p_wiggle;
    char   three_p_wiggle;
    char   files_per_pair;
    char   for_splinting;
    char   is_merged;
    char   estimate_from_merged;
} lib_t;

typedef struct {
    char   config_fname[MAX_FILE_PATH];
    char   local_tmp_dir[MAX_FILE_PATH];
    int    num_libs;
    lib_t  libs[MAX_LIBRARIES];
    int    is_metagenome;
    int    is_diploid;
    int    high_heterozygosity;
    int    mer_size; // deprecated!
    int    num_mer_sizes;
    int    mer_sizes[MAX_MER_SIZES];
    int    scaff_mer_size;
    int    cores_per_node;
    int    min_contig_len;
    int    min_depth_cutoff;
    int    extension_cutoff;
    double dynamic_min_depth;
    double alpha;
    double beta;
    double tau;
    double error_rate;
    int    qual_offset;
    int    bubble_min_depth_cutoff;
    int    cached_io;
    int    cached_reads;
    int    sw_cache_mb;
    int    kmer_cache_mb;
    int    nodes;
    int    max_ono_setid;
    char   ono_pair_thresholds[100];
    int    ono_w_long_reads;
    int    min_link_support; //deprecated
    int    use_cc_ono;
    double gap_close_rpt_depth_ratio;
    int    colorize_output;
    int    skip_la;
    double la_depth_dist_thres;
    double la_nvote_diff_thres;
    int    la_mer_size_min;
    int    la_mer_size_max;
    int    la_hi_qual_thres;
    int    la_qual_thres;
    double la_min_viable_depth;
    double la_min_expected_depth;
    int    la_rating_thres;
    char * stages;
    int    assm_scaff_len_cutoff;
    int    canonicalize;
    int    num_scaff_loops;
    int    illumina_version;
    int    save_intermediates;
    int    no_cleanup_after_success;
    int    save_mergraph;
    char * pePath;
    int    runHLL;
    int    noHH;
    char   hmm_model_file[MAX_FILE_PATH];
    int    localize_reads;
    int    min_gap_size;
    int    cgraph_scaffolding;
    int    skip_splints;
    int    merge_reads; // 0 - no, 1 - merge before the first ufx, 2 - merge just for cgraph only
    char*  cgraph_quality;
    int    use_bloom_filter;
} cfg_t;

void get_list_lib_params(cfg_t *cfg, int *max_readlen, char *libs, char *list_insert_sizes, char *list_insert_sigmas, char *list_revcomps, char *list_5p_wiggles, char *list_3p_wiggles, int forContigOrGapclosing1or2);
void get_all_lib_names(cfg_t *cfg, char *all_libs);
void get_contigging_lib_names(cfg_t *cfg, char *all_libs);
void get_noncontigging_lib_names(cfg_t *cfg, char *all_libs);
void get_config(int argc, char **argv, cfg_t *cfg, const char *_use_col);
void print_lib(lib_t *lib);

#endif
