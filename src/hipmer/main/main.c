#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <libgen.h>
#include <upc.h>
#include <upc_tick.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <glob.h>

#include "version.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "hash_funcs.h"
#include "Buffer.h"

//#include "../kmercount/ufx.h"
#include "exec_utils.h"
#include "config.h"
#include "run_meraligner.h"
#include "cpu_affinity.h"
#include "utils.h"
#include "upc_output.h"
#include "main_modules.h"
#include "../fqreader/fq_reader.h"
#include "fasta_partition.h"
#include "../loadfq/loadfq.h"
#include "../cgraph/cgraph.h"
#include "../kcount/kcount.h"
#include "upcxx_utils/log.h"

//#define FIND_NEW_MERS

char *_use_col;

// rebuilds the all_inputs fofn by using all libraries to be used for contigging
void rebuild_all_inputs_fofn(const char *all_inputs_fofn, cfg_t *cfg, const char * stage_suffix, int force)
{
    if (MYTHREAD != 0) return; // only THREAD 0 
    char stage_fofn[MAX_FILE_PATH];
    sprintf(stage_fofn, "%s-%s", all_inputs_fofn, stage_suffix);
    if ( does_file_exist(stage_fofn) ) {
        if ( !force ) {
            serial_printf("Skipping rebuild of all_inputs as %s already exists.. using that\n", stage_fofn);
            unlink(all_inputs_fofn);
            link_chk(stage_fofn, all_inputs_fofn);
            return;
        } else {
            WARN("Replacing existing stage fofn: %s\n", stage_fofn);
        }
    }
    unlink(all_inputs_fofn); // okay to fail, but breaks hard links
    LOGF("Creating new all_inputs_fofn %s:\n", all_inputs_fofn);
    FILE *all_inputs_f = fopen_chk(all_inputs_fofn, "w");
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &(cfg->libs[i]);
        if ( ! lib->for_contigging ) continue;
        char libname[MAX_FILE_PATH];
        snprintf(libname, MAX_FILE_PATH, "%s.fofn", lib->name);
        FILE *f = fopen_chk(libname, "r");
        char line[MAX_FILE_PATH];
        while ( fgets(line, MAX_FILE_PATH, f) ) {
            LOGFN("%s", line); // no newline
            fwrite_chk(line, 1, strlen(line), all_inputs_f);
        }
        fclose_track(f);
    }
    fclose_track(all_inputs_f);
    link_chk(all_inputs_fofn, stage_fofn);
    LOGF("Linked to %s\n", stage_fofn);
}


// creates fofn files for all_inputs_fofn and per-library ones
// replaces the glob with the fully qualified files path within the file-of-filenames
// if there is *no* '/' in the file-of-filenames, then the file is split in the per_rank dirs as a checkpoint name
static void setup_fofns(const char *all_inputs_fofn, cfg_t *cfg)
{
    UPC_LOGGED_BARRIER;
    serial_printf("Setting up libary file-of-file-names (fofn)\n");

    if (MYTHREAD != 0) {
        return;
    }

    // only thread 0 proceed
    assert(!MYTHREAD);

    for (int i = 0; i < cfg->num_libs; i++) {
        glob_t globbuf;
        memset(&globbuf, 0, sizeof(glob_t));
        lib_t *lib = &(cfg->libs[i]);
        char libname[MAX_FILE_PATH];
        snprintf(libname, MAX_FILE_PATH, "%s.fofn", lib->name);
        LOGF("Preparing %s.  filesPerPair=%d files=%s\n", libname, lib->files_per_pair, lib->files_glob);
        if (lib->files_glob == NULL) {
            serial_printf("Skipping re-expansion of files in %s\n", lib->name);
            continue;
        }
        if (does_file_exist(libname)) {
            WARN("setup_fofn is replacing an existing fofn.  renaming to .%s.OLD\n", libname);
            char new[MAX_FILE_PATH+10];
            sprintf(new, ".%s.OLD", libname);
            rename(libname, new);
        }
        FILE *lib_f = fopen_chk(libname, "w");
        char *filename = strdup(lib->files_glob), *filename2 = NULL;
        if (lib->files_per_pair == 2) {
            glob_t globbuf1, globbuf2;
            memset(&globbuf1, 0, sizeof(glob_t));
            memset(&globbuf2, 0, sizeof(glob_t));
            char *splitpt = strchr(filename, ',');
            if (!splitpt) {
                SDIE("Expected a comma in files field for %s: '%s'\n", lib->name, filename);
            }
            *splitpt = '\0'; // split the string
            filename2 = splitpt + 1;
            {
                char *new_files1 = NULL, *new_files2 = NULL;
                int gret1 = glob(filename, GLOB_ERR, NULL, &globbuf1);
                int gret2 = glob(filename2, GLOB_ERR, NULL, &globbuf2);
                if (gret1 != 0 || gret2 != 0) {
                    DIE("Could not glob-expand files for lib %s: '%s' or '%s'! (%d and %d)\n", lib->name, lib->files_glob, splitpt + 1, gret1, gret2);
                }
                if (globbuf1.gl_pathc != globbuf2.gl_pathc) {
                    DIE("Got different results for lib %s: %d vs %d for '%s' and '%s'\n", lib->name, globbuf1.gl_pathc, globbuf2.gl_pathc, lib->files_glob, splitpt + 1);
                }
                for (int j = 0; j < globbuf1.gl_pathc; j++) {
                    char fullpath1[PATH_MAX+1], fullpath2[PATH_MAX+1];
                    char *tmp1 = realpath(globbuf1.gl_pathv[j], fullpath1);
                    char *tmp2 = realpath(globbuf2.gl_pathv[j], fullpath2);
                    if (tmp1 == NULL || tmp2 == NULL) DIE("Coud not resolve paths for %s and/or %s ('%s' and/or '%s')\n", globbuf1.gl_pathv[j], globbuf2.gl_pathv[j], filename, filename2);
                    fprintf(lib_f, "%s\n%s\n", fullpath1, fullpath2);
                }
                globfree(&globbuf1);
                globfree(&globbuf2);
            }
        } else {
            glob_t globbuf;
            memset(&globbuf, 0, sizeof(glob_t));
            {
                char *newfiles = NULL;
                int gret = glob(lib->files_glob, GLOB_ERR, NULL, &globbuf);
                if (gret != 0) {
                    DIE("Could not glob-expand files for lib %s: '%s'! (%d)\n", lib->name, lib->files_glob, gret);
                }
                for (int j = 0; j < globbuf.gl_pathc; j++) {
                    char fullpath[PATH_MAX+1];
                    char *tmp = realpath(globbuf.gl_pathv[j], fullpath);
                    if (tmp == NULL) DIE("Could not get the real path for %s ('%s')\n", globbuf.gl_pathv[j], lib->files_glob);
                    fprintf(lib_f, "%s\n", fullpath);
                }
                globfree(&globbuf);
            }
        }
        free(filename);
        fclose(lib_f);
    }
    rebuild_all_inputs_fofn(all_inputs_fofn, cfg, "INITIAL", 1);
}

static int find_dmin(char *histo_fname)
{
    int64_t prev_cnt = -1;
    int64_t peak_cnt = -1;
    int64_t peak_m = -1;
    int64_t min_peak_m = 5;
    int64_t b_minima_found = 0;
    int64_t prev_m = -1;
    char buf[4096];

    const int NUM_ENTRIES = 6000001;

    int64_t *m_array = malloc_chk(sizeof(int64_t) * NUM_ENTRIES);
    int64_t *cnt_array = malloc_chk(sizeof(int64_t) * NUM_ENTRIES);
    FILE *f = fopen_track(histo_fname, "r");
    if (f) {
        int line = 0;

        while (fgets(buf, 4095, f)) {
            char *m_str = buf;
            char *cnt_str = strchr(buf, '\t');
            if (!cnt_str) {
                WARN("Could not parse line %s: incorrect format, expected number<tab>count\n", buf);
                return 0;
            }
            cnt_str[0] = 0;
            m_array[line] = atol(m_str);
            cnt_array[line] = atol(cnt_str + 1);
            if (b_minima_found && (cnt_array[line] > peak_cnt && m_array[line] > min_peak_m)) {
                peak_cnt = cnt_array[line];
                peak_m = m_array[line];
            }
            if (!b_minima_found && cnt_array[line] > prev_cnt && prev_cnt != -1) {
                b_minima_found = 1;
                min_peak_m = prev_m;
            }
            prev_cnt = cnt_array[line];
            prev_m = m_array[line];
            line++;
            if (line == NUM_ENTRIES) {
                WARN("reached max entries\n");
                break;
            }
            if (line > 200) {
                break;
            }
        }
        fclose_track(f);
    }
    if (!b_minima_found || peak_m > 1000) {
        WARN("No minima found / peakM suspiciously high at %lld\n", (lld) peak_m);
        return 0;
    }

    serial_printf("\n%sChecking histogram: peak %lld, min %lld" KNORM "\n",
                  _use_col, (lld) peak_m, (lld) min_peak_m);
    ADD_DIAG("%lld", "histogram_peak", (lld) peak_m);
    ADD_DIAG("%lld", "histogram_min_peak", (lld) min_peak_m);

    int dmin = 0;
    for (int i = 0; i < NUM_ENTRIES; i++) {
        if (m_array[i] > min_peak_m) {
            serial_printf("Couldn't find suitable d-min\n");
            dmin = 0;
            break;
        }
        if (cnt_array[i] < peak_cnt) {
            dmin = m_array[i];
            break;
        }
    }
    free_chk(cnt_array);
    free_chk(m_array);
    if (dmin == 0) {
        serial_printf("D-Min could not be determined\n");
    }
    return dmin;
}

#define T_MERGE_READS 1

const double Q2Perror[] = { 1.0,
                            0.7943,	0.6309,	0.5012,	0.3981,	0.3162,
                            0.2512,	0.1995,	0.1585,	0.1259,	0.1,
                            0.07943,	0.06310,	0.05012,	0.03981,	0.03162,
                            0.02512,	0.01995,	0.01585,	0.01259,	0.01,
                            0.007943,	0.006310,	0.005012,	0.003981,	0.003162,
                            0.002512,	0.001995,	0.001585,	0.001259,	0.001,
                            0.0007943,	0.0006310,	0.0005012,	0.0003981,	0.0003162,
                            0.0002512,	0.0001995,	0.0001585,	0.0001259,	0.0001,
                            7.943e-05,	6.310e-05,	5.012e-05,	3.981e-05,	3.162e-05,
                            2.512e-05,	1.995e-05,	1.585e-05,	1.259e-05,	1e-05,
                            7.943e-06,	6.310e-06,	5.012e-06,	3.981e-06,	3.162e-06,
                            2.512e-06,	1.995e-06,	1.585e-06,	1.259e-06,	1e-06,
                            7.943e-07,	6.310e-07,	5.012e-07,	3.981e-07,	3.1622e-07,
                            2.512e-07,	1.995e-07,	1.585e-07,	1.259e-07,	1e-07,
                            7.943e-08,	6.310e-08,	5.012e-08,	3.981e-08,	3.1622e-08,
                            2.512e-08,	1.995e-08,	1.585e-08,	1.259e-08,	1e-08};

// This is a very simple algorithm (eg compared to bbmerge). We just take the longest perfect overlap
// between the two reads. If there is no perfect overlap, then there is no merge.
// the reads_fname will be replaced with the new name of the checkpoint
// modified to allow some % errors in the overlap based on quality score differences
// and pick the base from the read with the higher quality score
static void merge_reads_file(fq_reader_t fqr, fq_reader_t fqr2, char *merged_reads_checkpoint, char * libName, int files_per_pair, int hexify_libi, int qual_offset) {
    if (files_per_pair == 0) DIE("Invalid program logic, can not merge reads that are not paired!\n");
    START_TIMER(T_MERGE_READS);
    Buffer id_buf = initBuffer(MAX_READ_NAME_LEN), seq_buf = initBuffer(1024), quals_buf = initBuffer(1024);
    Buffer prev_id_buf = initBuffer(MAX_READ_NAME_LEN), prev_seq_buf = initBuffer(1024), prev_quals_buf = initBuffer(1024);
    Buffer tmpseq_buf = initBuffer(1024), tmpquals_buf = initBuffer(1024);
    int64_t num_read_pairs = 0, num_reads = 0, num_merged = 0, num_ambiguous = 0;
    uint64_t read_id_1 = MYTHREAD, read_id_2 = MYTHREAD;
    int64_t bytes_written = 0;
    int64_t mergedLen = 0, overlapLen = 0, maxReadLen = 0;
    const int16_t MIN_OVERLAP = 12;
    const int16_t EXTRA_TEST_OVERLAP = 2;
    const int16_t MAX_MISMATCHES = 3; // allow up to 3 mismatches, with MAX_PERROR
    const int Q2PerrorSize = sizeof(Q2Perror) / sizeof(*Q2Perror);
    assert(qual_offset == 33 || qual_offset == 64);

    // illumina reads generally accumulate errors at the end, so allow more mismatches in the overlap as long as differential quality indicates a clear winner
    const double MAX_PERROR = 0.025; // allow up to 2.5% accumulated mismatch probablity of error within the overlap by differential quality score
    const int16_t EXTRA_MISMATCHES_PER_1000 = (int) 150; // allow additional mismatches per 1000 bases of overlap before aborting an overlap test
    const uint8_t MAX_MATCH_QUAL = 41 + qual_offset;
    GZIP_FILE f = openCheckpoint(merged_reads_checkpoint, "w");
    char pair = '0', prev_pair = '0';
    while (get_next_fq_record(fqr, id_buf, seq_buf, quals_buf)) {
        pair = num_reads % 2 == 0 ? '1' : '2';
        if (hexify_libi >= 0) {
            hexifyId(getStartBuffer(id_buf), hexify_libi, (pair == '1' ? &read_id_1 : NULL), (pair == '2' ? &read_id_2 : NULL), THREADS);
        }
        DBG2("Merging %s\n", getStartBuffer(id_buf));
        num_reads++;
        if (files_per_pair == 2) {
            // read read2
            swapBuffer(prev_id_buf, id_buf);
            swapBuffer(prev_seq_buf, seq_buf);
            swapBuffer(prev_quals_buf, quals_buf);
            int readLen = getLengthBuffer(prev_seq_buf);
            if (maxReadLen < readLen) maxReadLen = readLen;
    
            if (!get_next_fq_record(fqr2, id_buf, seq_buf, quals_buf)) {
                DIE("Did not find matching paired read for lib=%s, num_reads=%lld hexid=%s\n", libName, (lld) num_reads, getStartBuffer(id_buf));
            }
            DBG2("with %s\n", getStartBuffer(id_buf));
            prev_pair = pair;
            pair = '2';
            if (hexify_libi >= 0) {
                hexifyId(getStartBuffer(id_buf), hexify_libi, NULL, &read_id_2, THREADS);
            }
            num_reads++;
        }
        char *id = getStartBuffer(id_buf);
        char *seq = getStartBuffer(seq_buf);
        char *quals = getStartBuffer(quals_buf);

        // ensure prev buffers are large enough for a merged read
        growBuffer(prev_seq_buf, getLengthBuffer(seq_buf));
        growBuffer(prev_quals_buf, getLengthBuffer(quals_buf));

        char *prev_id = getStartBuffer(prev_id_buf);
        char *prev_seq = getStartBuffer(prev_seq_buf);
        char *prev_quals = getStartBuffer(prev_quals_buf);
        int needs_pair_suffix = 1;
        if (strlen(prev_id)) {
            if (files_per_pair == 2 || (prev_pair == '1' && pair == '2')) {
                uint8_t namelen = strlen(id);
                uint8_t prev_namelen = strlen(prev_id);
                if (namelen != prev_namelen || strncmp(prev_id, id, namelen - 1) != 0) {
                    DIE("Error in paired reads file %s line %lld: names don't match, %s != %s\n",
                        libName, (lld) num_reads * 4, prev_id, id);
                }
                if (namelen >= 3 && id[namelen-2] == '/') needs_pair_suffix = 0;

                int8_t is_merged = 0;
                int8_t abortMerge = 0;

                // now make a copy and revcomp the second mate pair and reverse the second quals
                copyBuffer(tmpseq_buf, seq_buf);
                char *revcomp_seq = getStartBuffer(tmpseq_buf);
                switch_code(reverse(revcomp_seq));

                copyBuffer(tmpquals_buf, quals_buf);
                char *rev_quals = getStartBuffer(tmpquals_buf);
                reverse(rev_quals);

                // calcualte the sequenc length
                int prev_len = strlen(prev_seq), revcomp_len = strlen(revcomp_seq);
                if (prev_len > 32767 || revcomp_len > 37676) DIE("Can not merge ultra-long reads %lld or %lld size\n", prev_len, revcomp_len);

                // use start_i to offset inequal lengths which can be very different but still overlap near the end.  250 vs 178..
                int16_t len = (revcomp_len < prev_len) ? revcomp_len : prev_len;
                int16_t start_i = ((len == prev_len) ? 0 : prev_len - len);
                int16_t found_i = -1;
                int16_t best_i = -1;
                int16_t best_mm = len;
                double best_perror = -1.0;

                // slide along prev_seq
                for (int16_t i = 0; i < len - MIN_OVERLAP + EXTRA_TEST_OVERLAP; i++) { // test less overlap than MIN_OVERLAP
                    if (abortMerge) break;
                    int16_t overlap = len-i;
                    int16_t this_max_mismatch = MAX_MISMATCHES + (EXTRA_MISMATCHES_PER_1000 * overlap / 1000);
                    int16_t error_max_mismatch = this_max_mismatch * 4 / 3 + 1; // 33% higher
                    int fastMismatch = fastCountMismatches(prev_seq + start_i + i, revcomp_seq, overlap, error_max_mismatch);
                    if (fastMismatch > error_max_mismatch) {
                        continue;
                    }
                    int16_t matches = 0, mismatches = 0, bothNs = 0, Ncount = 0;
                    int16_t overlapChecked = 0;
                    double perror = 0.0;
                    for (int16_t j = 0; j < overlap; j++) {
                        overlapChecked++;
                        char ps = prev_seq[start_i + i + j];
                        char rs = revcomp_seq[j];
                        if (ps == rs) {
                            matches++;
                            if (ps == 'N') {
                                Ncount += 2;
                                if (bothNs++) {
                                    abortMerge++;
                                    num_ambiguous++;
                                    DBG2("Both have Ns at the same spot\n");
                                    break; // do not match multiple Ns in the same position -- 1 is okay
                                }
                            }
                        } else {
                            mismatches++;
                            if (ps == 'N') {
                                mismatches++; // N still counts as a mismatch
                                Ncount++;
                                prev_quals[start_i + i + j] = qual_offset;
                                assert(rev_quals[j] - qual_offset < Q2PerrorSize); 
                                assert(rev_quals[j] - qual_offset >= 0); 
                                perror += Q2Perror[rev_quals[j] - qual_offset];
                            } else if (rs == 'N') {
                                Ncount++;
                                mismatches++; // N still counts as a mismatch
                                rev_quals[j] = qual_offset;
                                assert(prev_quals[start_i + i + j] - qual_offset < Q2PerrorSize); 
                                assert(prev_quals[start_i + i + j] - qual_offset >= 0);
                                perror += Q2Perror[prev_quals[start_i + i + j] - qual_offset];
                            }
                            if (MAX_PERROR > 0.0) {

                                assert(prev_quals[start_i + i + j] >= qual_offset);
                                assert(rev_quals[j] >= qual_offset);
                                uint8_t q1 = prev_quals[start_i + i + j] - qual_offset;
                                uint8_t q2 = rev_quals[j] - qual_offset;
                                if (q1 < 0 || q2 < 0 || q1 >= Q2PerrorSize || q2 >= Q2PerrorSize) {
                                    DIE("Invalid quality score for read %s '%c' or %s '%c' assuming common qual_offset of %d. Please check your data and make sure it follows a single consistent quality scoring model (phred+64 vs. phred+33)\n", prev_id,  prev_quals[start_i + i + j], id, rev_quals[j], qual_offset);
                                }
                                // sum perror as the difference in q score perrors
                                uint8_t diffq = (q1 > q2) ? q1-q2 : q2-q1;
                                if (diffq <= 2) {
                                    perror += 0.5; // cap at flipping a coin when both quality scores are close
                                } else {
                                    assert(diffq < Q2PerrorSize);
                                    perror += Q2Perror[diffq];
                                }
                            }
                        }
                        if (Ncount > 3) {
                            abortMerge++;
                            num_ambiguous++;
                            break; // do not match reads with many Ns
                        }
                        if (mismatches > error_max_mismatch) break;
                    }
                    int16_t match_thres = overlap - this_max_mismatch;
                    if (match_thres < MIN_OVERLAP) match_thres = MIN_OVERLAP;
                    if (matches >= match_thres && overlapChecked == overlap && mismatches <= this_max_mismatch && perror/overlap <= MAX_PERROR) {
                        if (best_i < 0 && found_i < 0) {
                            DBG2("Found potential match at i=%d MM=%d PE=%f\n", i, mismatches, perror);
                            best_i = i;
                            best_mm = mismatches;
                            best_perror = perror;
                        } else {
                            // another good or ambiguous overlap detected
                            DBG2("Found ambiguous match at i=%d MM=%d PE=%f\n", i, mismatches, perror);
                            num_ambiguous++;
                            best_i = -1;
                            best_mm = len;
                            best_perror = -1.0;
                            break;
                        }
                    } else if (overlapChecked == overlap && mismatches <= error_max_mismatch && perror/overlap <= MAX_PERROR * 4 / 3) {
                        // lower threshold for detection of an ambigious overlap
                        found_i = i;
                        if (best_i >= 0) {
                            // ambiguous mapping found after a good one was
                            DBG2("Found ambiguous (poor) match at i=%d MM=%d PE=%f\n", i, mismatches, perror);
                            num_ambiguous++;
                            best_i = -1;
                            best_mm = len;
                            best_perror = -1.0;
                            break;
                        }
                    }
                }

                if (best_i >= 0 && abortMerge == 0) {
                    int16_t i = best_i;
                    int16_t overlap = len-i;
                    // pick the base with the highest quality score for the overlapped region
                    for(int16_t j = 0 ; j < overlap ; j++) {
                        if (prev_seq[start_i+i+j] == revcomp_seq[j]) {
                            // match boost quality up to the limit
                            uint16_t newQual = prev_quals[start_i+i+j] + rev_quals[j] - qual_offset;
                            prev_quals[start_i+i+j] = ((newQual > MAX_MATCH_QUAL) ? MAX_MATCH_QUAL : newQual);
                            assert( prev_quals[start_i+i+j] >= prev_quals[start_i+i+j] );
                            assert( prev_quals[start_i+i+j] >= rev_quals[j] );
                        } else {
                            uint8_t newQual;
                            if (prev_quals[start_i+i+j] < rev_quals[j]) {
                                // use rev base and discount quality
                                newQual = rev_quals[j] - prev_quals[start_i+i+j] + qual_offset;
                                prev_seq[start_i+i+j] = revcomp_seq[j];
                            } else {
                                // keep prev base, but still discount quality
                                newQual = prev_quals[start_i+i+j] - rev_quals[j] + qual_offset;
                            }
                            prev_quals[start_i+i+j] = ((newQual > (2+qual_offset)) ? newQual : (2+qual_offset)); // a bit better than random chance here
                        }
                        assert(prev_quals[start_i+i+j] >= qual_offset);
                    }

                    // include the remainder of the revcomp_seq and quals
                    strcpy(prev_seq + start_i + i + overlap, revcomp_seq + overlap);
                    strcpy(prev_quals + start_i + i + overlap, rev_quals + overlap);
                       
                    is_merged = 1;
                    num_merged++;
                    DBG2("Merged: %s OL=%d MM=%d PE=%0.2f\n", prev_id, overlap, best_mm, best_perror);
                    bytes_written += GZIP_PRINTF(f, "%s%s\n%s\n+\n%s\n", prev_id, (needs_pair_suffix ? "/1" : ""), prev_seq, prev_quals);
                    bytes_written += GZIP_PRINTF(f, "%s%s\nN\n+\n%c\n", id, (needs_pair_suffix ? "/2" : ""), qual_offset); // output an empty record, keeping a dummy entry for Read2
                    int readLen = strlen(prev_seq); // cacluate new merged length
                    if (maxReadLen < readLen) maxReadLen = readLen;
                    mergedLen += readLen;
                    overlapLen += overlap;
                }
                if (!is_merged) {
                    DBG2("NOT MERGED: %s abortMerge=%d\n", prev_id, abortMerge);
                    bytes_written += GZIP_PRINTF(f, "%s%s\n%s\n+\n%s\n", prev_id, (needs_pair_suffix ? "/1" : ""), prev_seq, prev_quals);
                    bytes_written += GZIP_PRINTF(f, "%s%s\n%s\n+\n%s\n", id, (needs_pair_suffix ? "/2" : ""), seq, quals);
                }
                num_read_pairs++;
                // ensure this read pair will not be used again
                resetBuffer(id_buf);
                resetBuffer(prev_id_buf);
                pair = '0';
                prev_pair = '0';
            } else {
                WARN("Mismatched pairs! '%s' '%s')\n", id, prev_id);
            }
        }
        if (maxReadLen < getLengthBuffer(seq_buf)) maxReadLen = getLengthBuffer(seq_buf);
        if (files_per_pair == 1) {
            swapBuffer(prev_id_buf, id_buf);
            swapBuffer(prev_seq_buf, seq_buf);
            swapBuffer(prev_quals_buf, quals_buf);
            prev_pair = pair;
        }
    }
    if (num_reads != num_read_pairs * 2) {
        DIE("Mismatch in number of reads (%lld) and num read pairs (%lld)\n", (lld) num_reads, (lld) num_read_pairs);
    }
    closeCheckpoint(f);
    // store the uncompressed size in a secondary file
    char uncompressed_fname[MAX_FILE_PATH+30];
    sprintf(uncompressed_fname, "%s.uncompressedSize", merged_reads_checkpoint);
    FILE *fsz = openCheckpoint0(uncompressed_fname, "w");
    fwrite_chk(&bytes_written, sizeof(int64_t), 1, fsz);
    closeCheckpoint0(fsz);

    // cleanup
    freeBuffer(id_buf);
    freeBuffer(seq_buf);
    freeBuffer(quals_buf);
    freeBuffer(prev_id_buf);
    freeBuffer(prev_seq_buf);
    freeBuffer(prev_quals_buf);
    freeBuffer(tmpseq_buf);
    freeBuffer(tmpquals_buf);

    // update the maximum read length
    int max_read_len = reduce_long(maxReadLen, UPC_MAX, ALL_DEST);

    if (!MYTHREAD) {
        serial_printf("Writing maxReadLen for %s: %d\n", merged_reads_checkpoint, max_read_len);
        char buf[MAX_FILE_PATH+30];
        snprintf(buf, MAX_FILE_PATH+30, "%s.maxReadLen.txt", merged_reads_checkpoint);
        put_num_in_file(NULL, buf, max_read_len);
        snprintf(buf, MAX_FILE_PATH+30, "%s-readlen.txt", libName);
        FILE * f = fopen_chk(buf, "a");
        fprintf(f, "%d", max_read_len);
        fclose_track(f);
    }

    UPC_TIMED_BARRIER;
    long all_num_read_pairs = reduce_long(num_read_pairs, UPC_ADD, SINGLE_DEST);
    long all_num_merged = reduce_long(num_merged, UPC_ADD, SINGLE_DEST);
    long all_num_ambiguous = reduce_long(num_ambiguous, UPC_ADD, SINGLE_DEST);
    double avg_merged_len = reduce_long(mergedLen, UPC_ADD, SINGLE_DEST) / (double) ((all_num_merged > 0) ? all_num_merged : 1);
    double avg_overlap_len = reduce_long(overlapLen, UPC_ADD, SINGLE_DEST) / (double) ((all_num_merged > 0) ? all_num_merged : 1);
    stop_timer(T_MERGE_READS);
    serial_printf("Merged %lld out of %lld read pairs (%.2f%%, %0.2f%% ambiguous) with %0.1f avgLength (%d max), %0.1f avgOverlap in %.2f s\n", (lld) all_num_merged, (lld) all_num_read_pairs,
                  100.0 * all_num_merged / all_num_read_pairs, 100.0* all_num_ambiguous / all_num_read_pairs, avg_merged_len, max_read_len, avg_overlap_len,
                  get_elapsed_time(T_MERGE_READS));
}

static void merge_reads_lib(char * libName, int files_per_pair, int hexify_libi, char * base_dir, int cached_reads, int qual_offset)
{
    if (files_per_pair == 0) {
        DIE("merge_reads is called on a single library!\n");
        return;
    }

    serial_printf("Merging reads from %s\n", libName);
    UPC_LOGGED_BARRIER;

    // read the fofn, merge each, then replace fofn
    Buffer newFofnBuffer = initBuffer(MAX_FILE_PATH);
    char fofn[MAX_FILE_PATH+10];
    sprintf(fofn, "%s.fofn", libName);
    Buffer fofnBuffer = broadcast_file(fofn);
    char file1[MAX_FILE_PATH], file2[MAX_FILE_PATH];
    while ( getsBuffer(fofnBuffer, file1, MAX_FILE_PATH) ) {
        file1[strlen(file1)-1] = '\0';
        if (files_per_pair == 2) {
            if ( !getsBuffer(fofnBuffer, file2, MAX_FILE_PATH) ) { DIE("Missing second file for paired libs: %s\n", file1); }
            file2[strlen(file2)-1] = '\0';
        }

        char *base = strdup(file1);
        char *reads_fname_base = basename(base);
        char *isgz = strstr(reads_fname_base, ".gz");
        if (isgz) isgz[0] = '\0';
        char merged_reads_checkpoint[MAX_FILE_PATH];
        sprintf(merged_reads_checkpoint, "%s-merged.fastq" GZIP_EXT, reads_fname_base);
        free(base);

        printfBuffer(newFofnBuffer, "%s\n", merged_reads_checkpoint);

        if (doesCheckpointExist(merged_reads_checkpoint)) {
            serial_printf("Skipping merge of %s as they already exist\n", merged_reads_checkpoint);
            if (!doesLocalCheckpointExist(merged_reads_checkpoint)) {
                restoreLocalCheckpoint(merged_reads_checkpoint);
                strcat(merged_reads_checkpoint, ".uncompressedSize");
                if ( !doesLocalCheckpointExist(merged_reads_checkpoint) && doesGlobalCheckpointExist(merged_reads_checkpoint) ) {
                    restoreCheckpoint(merged_reads_checkpoint);
                }
            }
            continue;
        }
        serial_printf("Merging reads from %s %s into %s\n", file1, (files_per_pair==2 ? file2 : ""), merged_reads_checkpoint);
    
        fq_reader_t fqr = create_fq_reader(), fqr2;
        open_fq(fqr, file1, cached_reads, base_dir, cached_reads ? -1 : broadcast_file_size(file1));
        if (files_per_pair == 2) {
            fqr2 = create_fq_reader();
            open_fq(fqr2, file2, cached_reads, base_dir, cached_reads ? -1 : broadcast_file_size(file2));
        }

        merge_reads_file(fqr, fqr2, merged_reads_checkpoint, libName, files_per_pair, hexify_libi, qual_offset);
     
        destroy_fq_reader(fqr);
        if (files_per_pair == 2) {
            destroy_fq_reader(fqr2);
        }
    }
    UPC_LOGGED_BARRIER;
    if (!MYTHREAD) {
        serial_printf("Replacing fofn: %s\n", fofn);
        char new[MAX_FILE_PATH+30];
        sprintf(new, "%s-PREMERGE", fofn);
        rename(fofn, new);
        FILE * f = fopen_chk(fofn, "w");
        fwrite_chk(getStartBuffer(newFofnBuffer), 1, getLengthBuffer(newFofnBuffer), f);
        fclose_track(f);
    }
    UPC_LOGGED_BARRIER;
}

// the stage by lib
int merge_reads_by_lib(int argc, char **argv) {
    if (argc != 13) DIE("Invalid call: %d\n", argc);
    if (strcmp(argv[1], "-l") != 0) DIE("expected -l\n");
    char * libName = argv[2];
    if (strcmp(argv[3], "-p") != 0) DIE("expected -p\n");
    int files_per_pair = atoi(argv[4]);
    if (strcmp(argv[5], "-h") != 0) DIE("expected -h\n");
    int hexify_libi = atoi(argv[6]);
    if (strcmp(argv[7], "-B") != 0) DIE("expected -c\n");
    char * base_dir = argv[8];
    if (strcmp(argv[9], "-X") != 0) DIE("expected -X\n");
    int cached_reads = atoi(argv[10]);
    if (strcmp(argv[11], "-q") != 0) DIE("expected -q\n");
    int qual_offset = atoi(argv[12]);

    merge_reads_lib(libName, files_per_pair, hexify_libi, base_dir, cached_reads, qual_offset);
    return 0;
}

// merges all eligible libs, and updates lib values
void merge_reads(cfg_t *cfg, const char * all_inputs_fofn_name)
{
    if (cfg->merge_reads == 0) DIE("merge_reads should not be called when the merge_reads option == 0\n");
    serial_printf("Starting to merge overlapping reads\n");
    int didRun = 0, numToRun = 0;
    for (int libi = 0; libi < cfg->num_libs; libi++) {
        lib_t *lib = cfg->libs + libi;

        int canMergeLib = (lib->files_per_pair > 0  // paired or interleaved libs
                           && (lib->ins_avg == 0 || lib->ins_avg < ((lib->read_len?lib->read_len:250) - 12) * 2 + 3*lib->std_dev || lib->innie));
        
        if (canMergeLib) {
            serial_printf("Expecting many reads can be merged\n");
        }
        // but always merge a paired library so it becomes interleaved
        if ( canMergeLib || lib->files_per_pair == 2 ) {
            // merge reads into per_rank files
            numToRun++;
            char stageLabel[MAX_FILE_PATH+20];
            sprintf(stageLabel, "merge_reads-%s", lib->name);
            didRun += exec_stage(cfg->stages, NOT_SCAFF, merge_reads_by_lib, stageLabel,
                                 "-l %s", lib->name,
                                 "-p %d", lib->files_per_pair,
                                 "-h %d", libi,
                                 "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                                 "-X %d", (cfg->merge_reads == 1 ? 0 : cfg->cached_reads),
                                 "-q %d", cfg->qual_offset,
                                 NULL);

            lib->files_per_pair = 1; // paired libs are now interleaved
            lib->is_merged = 1;
            // get the new maxReadLen
            if (!MYTHREAD) {
                char buf[MAX_FILE_PATH+20];
                sprintf(buf, "%s.fofn", lib->name);
                FILE *f = fopen_chk(buf, "r");
                while( fgets(buf, MAX_FILE_PATH, f) ) {
                    buf[strlen(buf)-1] = '\0';
                    if (strchr(buf, '/')) DIE("Invalid entry in %s.fofn, which should not contain a slash after merging! %s\n", lib->name, buf);
                    char buf2[MAX_FILE_PATH+40];
                    sprintf(buf2, "%s.maxReadLen.txt", buf);
                    int maxlen = get_num_from_file(NULL, buf2); 
                    if (lib->read_len < maxlen) lib->read_len = maxlen;
                }
                fclose_track(f);
            }
            lib->read_len = broadcast_int(lib->read_len, 0);
            serial_printf("Updated max_read len for %s: %d\n", lib->name, lib->read_len);
            if (!lib->ins_avg && !lib->innie) {
                lib->ins_avg = lib->read_len * 1.10; // wild estimate of the insert size if none was provided
                lib->estimate_from_merged = 1;
                serial_printf("Updated estimated insert size for %s: %d\n", lib->name, lib->ins_avg);
            }
            if (!lib->std_dev && lib->ins_avg && !lib->innie) {
                lib->std_dev = lib->ins_avg / 10;
                serial_printf("Updated estimated standard dev for %s: %d\n", lib->name, lib->std_dev);
            }
        } else {
            serial_printf("Skipping library %s fpp=%d insert=%d stddev=%d read_len=%d that will not have any overlaps\n", lib->name, lib->files_per_pair, lib->ins_avg, lib->std_dev, lib->read_len);
            serial_printf("Loading %s to local checkpoints\n", lib->name);
            loadfq_library(lib->name, BASE_DIR(cfg->cached_io, cfg->local_tmp_dir));
        }
    }
    rebuild_all_inputs_fofn(all_inputs_fofn_name, cfg, "MERGED_READS", 0);
    cfg->cached_reads = 1; // now all input files are in cached io or are checkpoints
}

void run_ono_scaffolding(cfg_t *cfg, int min_contig_len, int max_kmer_len, int64_t n_contigs, int scaff_mer_size_max,
                         int dryrun, char *tigs_out, char *mer_depth_prefix)
{
    // if the number of scaffold loops > 0, then set up the libraries accordingly
    if (cfg->num_scaff_loops) {
        // replicate each lib in order, and for each replication set to 0 all of
        // use_for_contigging, use_for_gapclosing, use_for_splinting, and increment ono set ids with each one
        lib_t *new_libs = calloc_chk(MAX_LIBRARIES, sizeof(lib_t));
        int num_libs = 0;
        // need to expand by ono id order, since multiple libs may have the same ono id
        for (int ono_i = 1; ono_i <= cfg->max_ono_setid; ono_i++) {
            for (int lib_i = 0; lib_i < cfg->num_libs; lib_i++) {
                lib_t *lib = &cfg->libs[lib_i];
                if (lib->ono_setid == ono_i) {
                    char oldfofn[MAX_FILE_PATH+20];
                    sprintf(oldfofn, "%s.fofn", lib->name);
                    for (int scaff_i = 1; scaff_i <= cfg->num_scaff_loops; scaff_i++) {
                        new_libs[num_libs] = *lib;
                        new_libs[num_libs].name = malloc(strlen(lib->name) + 10);
                        sprintf(new_libs[num_libs].name, "%s-%d", lib->name, scaff_i);
                        char newfofn[MAX_FILE_PATH+20];
                        sprintf(newfofn, "%s.fofn", new_libs[num_libs].name);
                        if (!MYTHREAD) {
                            unlink(newfofn); // okay to fail
                            link_chk(oldfofn, newfofn);
                        }
                        new_libs[num_libs].ono_setid = (lib->ono_setid - 1) * cfg->num_scaff_loops + scaff_i;
                        if (scaff_i > 1) {
                            new_libs[num_libs].for_contigging = 0;
                            new_libs[num_libs].for_splinting = 0;
                            new_libs[num_libs].for_gapclosing = 0;
                        }
                        num_libs++;
                    }
                }
            }
        }
        for (int i = 0; i < num_libs; i++) {
            if (cfg->libs[i].name) {
                free(cfg->libs[i].name);
            }
            cfg->libs[i] = new_libs[i];
            if (cfg->max_ono_setid < cfg->libs[i].ono_setid) {
                cfg->max_ono_setid = cfg->libs[i].ono_setid;
            }
        }
        cfg->num_libs = num_libs;

        UPC_LOGGED_BARRIER;
        free_chk(new_libs);
    }
    //if (!MYTHREAD) {
    //    for (int i = 0; i < cfg->num_libs; i++)
    //        print_lib(&cfg->libs[i]);
    //}
    UPC_LOGGED_BARRIER;

    Buffer bma_meta_libs_list = initBuffer(128);
    assert(getLengthBuffer(bma_meta_libs_list) == 0);
    int64_t n_tot_alns = 0;
    //int rerun_coverage;

    char merAlignerOutput[100];
    sprintf(merAlignerOutput, "merAlignerOutput-%d-scaff", min_contig_len);

    char output_ext[100];
    sprintf(output_ext, "%d-scaff", min_contig_len);
    // run meraligner and splinter on each lib in turn
    // we're rerunning merAligner with a potentially different kmer length than before
    int max_lib_read_len = 0;
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        if (lib->read_len == 0) {
            max_lib_read_len = 999999999; // assume the worst case here
            serial_printf("Assuming readlengths are very long in %s\n", lib->name);
        }
        if (max_lib_read_len < lib->read_len) {
            max_lib_read_len = lib->read_len * 2; // *2 for scaffold run
        }
    }
    LOGF("Found max_lib_read_len=%d\n", max_lib_read_len);

    // run meraligner
    createMeralignerInterModuleState(max_lib_read_len);
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        int seed_space = cfg->scaff_mer_size < max_kmer_len ? 8 : 1;
        int is_merged = lib->files_per_pair == 1 && (cfg->merge_reads == 1 || cfg->merge_reads == 2);
        int allow_repetitive_seeds = 1;
        run_meraligner_lib(cfg, lib, cfg->scaff_mer_size, tigs_out, min_contig_len, IS_SCAFF, 0, allow_repetitive_seeds,
                           merAlignerOutput, seed_space, _use_col);
        n_tot_alns += get_num_from_file_and_broadcast(lib->name, "TotalAlignments");
    }
    destroyMeralignerInterModuleState();

    // possibly run splinter and add to bma list
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        if (cfg->skip_splints == 0 && lib->for_splinting) {
            exec_stage(cfg->stages, IS_SCAFF, splinter_main, name_lib("splinter", lib->name),
                       "-l %s", lib->name,
                       "-m %d", cfg->scaff_mer_size,
                       "-r %d", lib->read_len,
                       "-T %d", lib->three_p_wiggle,
                       "-F %d", lib->five_p_wiggle,
                       "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                       "-a %s", merAlignerOutput,
                       "-P %d", lib->files_per_pair,
                       "-L %d", 0,
                       NULL);
            add_to_list(bma_meta_libs_list, "%s-bmaMeta-splints-%d", lib->name, 0);
        }
        // Re-evaluate contig depths 
        // if (!cfg->old_ono) {
        //  contigCoverage(n_contigs, lib->name, lib->files_per_pair, BASE_DIR(cfg->cached_io, cfg->local_tmp_dir), tigs_out,
        //                 scaff_mer_size_max, merAlignerOutput);
        // }
    }

    char prev_srf[100] = "";
    char this_srf[100] = "";
    int64_t n_scaffs = 0;
    int prev_round_max_ins = 0;


    // loop through ono ids, running histogrammer, spanner, bmaToLinks and ono
    serial_printf("Preparing %d rounds of scaffolding.\n", cfg->max_ono_setid);
    for (int ono_i = 1; ono_i <= cfg->max_ono_setid; ono_i++) {
        int max_ins_size = 0;
        for (int lib_i = 0; lib_i < cfg->num_libs; lib_i++) {
            lib_t *lib = &cfg->libs[lib_i];
            if (lib->ono_setid == ono_i && lib->files_per_pair > 0) {
                if (lib->ins_avg > max_ins_size) {
                    max_ins_size = lib->ins_avg;
                }
                run_histogrammer_lib(cfg, lib, cfg->scaff_mer_size, min_contig_len, prev_srf,
                                     n_contigs, n_scaffs, IS_SCAFF, merAlignerOutput, output_ext, 1, _use_col);
                if (strlen(prev_srf) == 0) {
                    exec_stage(cfg->stages, IS_SCAFF, spanner_main, name_lib("spanner", lib->name),
                               "-l %s", lib->name,
                               "-R %B", lib->rev_comp,
                               "-A %B", lib->innie,
                               "-T %d", lib->three_p_wiggle,
                               "-F %d", lib->five_p_wiggle,
                               "-i %d", lib->ins_avg,
                               "-s %d", lib->std_dev,
                               "-r %d", lib->read_len,
                               "-L %d", ono_i,
                               "-m %d", scaff_mer_size_max,
                               "-C %lld", (lld)n_contigs,
                               "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                               "-a %s", merAlignerOutput,
                               NULL);
			
                    add_to_list(bma_meta_libs_list, "%s-bmaMeta-spans-%d", lib->name, ono_i);
                    // Run code to estimate density of mapped reads per contig 
                    if (!dryrun) {
                        // FIXME This crashes for some reason
                        // int density = calculateReadDensity(n_contigs, lib->name, BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                        // merAlignerOutput);
                    }
                } else {  //multiple rounds - expect previous round's srf as input
                    if (strlen(prev_srf) == 0) DIE("Invalid workflow. ono_i=%d\n", ono_i);
                    char excludeListFname[MAX_FILE_PATH*2+20];
                    char excludeListPrefix[] = "contigs_mask_cumul";
                    if (cfg->is_diploid && cfg->high_heterozygosity) {
                        snprintf(excludeListFname, MAX_FILE_PATH*2+20, "%s/%s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir), excludeListPrefix);
                        get_rank_path(excludeListFname, MYTHREAD);
                        FILE *curMask_f = fopen_chk(excludeListFname, "w");
                        // collect all previous rounds' exclude lists
                        for (int i = 1; i < ono_i; i++) {
                            char prevExclList[MAX_FILE_PATH+50];
                            snprintf(prevExclList, MAX_FILE_PATH+50, "%s/parCC-out_SsrfFile-%d_%d.contigs_mask",
                                     BASE_DIR(cfg->cached_io, cfg->local_tmp_dir), i, MYTHREAD); // this file got created by ono_2D in the previous round
                            get_rank_path(prevExclList, MYTHREAD);
                            FILE *prevMask_f = fopen(prevExclList, "r");
                            if (prevMask_f != NULL) {
                                char line [256];
                                while ((fgets(line, sizeof line, prevMask_f)) != NULL) {
                                    fprintf(curMask_f, "%s", line);
                                }
                                fclose(prevMask_f);
                            }
                        }
                        fclose(curMask_f);
                        UPC_LOGGED_BARRIER;
                    }

                    exec_stage(cfg->stages, IS_SCAFF, spanner_main, name_lib("spanner", lib->name),
                               "-l %s", lib->name,
                               "-R %B", lib->rev_comp,
                               "-A %B", lib->innie,
                               "-T %d", lib->three_p_wiggle,
                               "-F %d", lib->five_p_wiggle,
                               "-i %d", lib->ins_avg,
                               "-s %d", lib->std_dev,
                               "-r %d", lib->read_len,
                               "-L %d", ono_i,
                               "-m %d", scaff_mer_size_max,
                               "-C %lld", (lld)n_contigs,
                               "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                               "-a %s", merAlignerOutput,
                               "-S %s", prev_srf,
                               "-Z %lld", (lld)n_scaffs,
                               (cfg->high_heterozygosity && does_file_exist(excludeListFname) ? "-X %s" : "--"), excludeListPrefix,
                               NULL);
                    add_to_list(bma_meta_libs_list, "%s-bmaMeta-spans-%d", lib->name, ono_i);
                }
                /* Run code to estimate density of mapped reads per contig */
                if (!dryrun) {
                    /* FIXME This crashes for some reason
                     * int density = calculateReadDensity(n_contigs, lib->name, BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                     * merAlignerOutput);
                     */
                }
            }
        }

        if (getLengthBuffer(bma_meta_libs_list) == 0) {
            serial_printf("Skipping round %d as there are no links to process\n", ono_i);
            continue;
        }
        exec_stage(cfg->stages, IS_SCAFF, bmaToLinks_main, name_i("bmaToLinks", ono_i),
                   "-b %s", getStartBuffer(bma_meta_libs_list),
                   "-m %d", scaff_mer_size_max,
                   "-C %lld", (lld)n_contigs,
                   "-L %d", ono_i,
                   "-n %lld", (lld) n_tot_alns,
                   "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                   "-P %d", prev_round_max_ins,
                   (cfg->is_metagenome ? "-M" : ""),
                   NULL);
        resetBuffer(bma_meta_libs_list);
        sprintf(this_srf, "SsrfFile-%d", ono_i);

        char links_meta[255];
        sprintf(links_meta, "linksMeta-%d", ono_i);
        int oNoBubbleDepthThreshold;
        int diploidMode;
        if (cfg->is_diploid) {
            diploidMode = (cfg->high_heterozygosity) ? 2 : 1;
            // if restrating at an ono round, user may have changed the cutoff in the config file, so we check every time
            char autocalBDFileName[255];
            sprintf(autocalBDFileName, "bubble_depth_threshold.txt");
            get_rank_path(autocalBDFileName, -1);
            oNoBubbleDepthThreshold = (cfg->bubble_min_depth_cutoff) ? cfg->bubble_min_depth_cutoff :
                get_num_from_file_and_broadcast("", autocalBDFileName);
            assert(oNoBubbleDepthThreshold > 0);
        }
        char contigs_fname[MAX_FILE_PATH];
        snprintf(contigs_fname, MAX_FILE_PATH, "%s", tigs_out);

        exec_stage(cfg->stages, IS_SCAFF, parCC_main, name_i("parCC", ono_i),
                   "-l %s", links_meta,
                   "-m %d", scaff_mer_size_max,
                   (strlen(prev_srf)==0 ? "-c %s" : "--"), mer_depth_prefix,
                   (strlen(prev_srf)>0 ? "-s %s" : "--"), prev_srf,
                   "-n %lld", (strlen(prev_srf)==0 ? n_contigs : n_scaffs),
                   "-o %s", this_srf,
                   "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                   "-P %s", cfg->ono_pair_thresholds,
                   "-C %s", contigs_fname,
                   (cfg->ono_w_long_reads ? "-R" : ""),
                   (cfg->is_diploid ? "-D %d" : "--"), diploidMode,
                   (cfg->is_diploid ? "-d %d" : "--"), oNoBubbleDepthThreshold,
                   (cfg->is_metagenome ? "-M" : ""),
                   (cfg->is_metagenome && strlen(prev_srf)==0 && cfg->hmm_model_file[0] ? "-H %s" : "--"), cfg->hmm_model_file,
                   /* hmm is only supported in first round currently */
                   NULL);
	

        strcpy(prev_srf, this_srf);
        char scaff_fname[255] = "Scaffolds_";
        strcat(scaff_fname, prev_srf);
        n_scaffs = get_num_from_file_and_broadcast("", scaff_fname);
        prev_round_max_ins = max_ins_size;
        UPC_LOGGED_BARRIER;	    
    }
	
    freeBuffer(bma_meta_libs_list);

    // run gap closing
    // get all lib params for gap closing and local assembly
    int max_readlen = 0;
    char gapclosing_libs[4096] = "";
    char list_insert_sizes[1024] = "", list_insert_sigmas[1024] = "", list_revcomps[1024] = "";
    char list_5p_wiggles[1024] = "", list_3p_wiggles[1024] = "";
    get_list_lib_params(cfg, &max_readlen, gapclosing_libs, list_insert_sizes, list_insert_sigmas,
                        list_revcomps, list_5p_wiggles, list_3p_wiggles, 2);
    if (max_readlen) {
        exec_stage(cfg->stages, IS_SCAFF, merauder_main, "gapclosing",
                   "-Q %d", cfg->qual_offset,
                   "-s %s", prev_srf,
                   "-c %s", tigs_out,
                   "-a %s", merAlignerOutput,
                   "-b %s", gapclosing_libs,
                   "-i %s", list_insert_sizes,
                   "-I %s", list_insert_sigmas,
                   "-m %d", scaff_mer_size_max,
                   "-D %d", cfg->min_depth_cutoff,
                   "-R %f", cfg->gap_close_rpt_depth_ratio,
                   "-P %B", cfg->is_metagenome || cfg->is_diploid,
                   "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                   "-X %B", cfg->cached_reads,
                   "-r %s", list_revcomps,
                   "-F %s", list_3p_wiggles,
                   "-T %s", list_5p_wiggles,
                   "-l %d", max_readlen,
                   "-L %d", cfg->assm_scaff_len_cutoff,
                   "-G %d", cfg->min_gap_size,
                   NULL);
    }

    {
        exec_stage(cfg->stages, IS_SCAFF, canonical_assembly_main, "canonical_assembly",
                   "-o %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                   "-n %d", cfg->cores_per_node,
                   "-O %s", "final_assembly.fa",
                   "-l %d", cfg->assm_scaff_len_cutoff,  // min length 
                   "-d %B", !cfg->canonicalize, NULL);
        if (cfg->assm_scaff_len_cutoff) {
            char assm[100];
            sprintf(assm, "assembly-%d", cfg->assm_scaff_len_cutoff);
            char can_assm_name[MAX_FILE_PATH] = "canonical_";
            strcat(can_assm_name, assm);
            char final_assm_file[MAX_FILE_PATH] = "final_";
            sprintf(final_assm_file, "final_%s.fa", assm);
            char assm_prefix[MAX_FILE_PATH];
            sprintf(assm_prefix, "%s", assm);
            exec_stage(cfg->stages, IS_SCAFF, canonical_assembly_main, can_assm_name,
                       "-o %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir), "-n %d", cfg->cores_per_node,
                       "-f %s", assm_prefix, "-F %s", ".fa", "-O %s", final_assm_file,
                       "-L %d", cfg->assm_scaff_len_cutoff,  // max length
                       "-d %B", !cfg->canonicalize, NULL);
        }
    }
}

int prep_cgraph_contigs_and_links(char *contigs, int scaffoldRound, int num_paired)
{
    // load any missing checkpoints into LOCAL_DIR (cfg.local_tmp_dir)
    // expects uutigs, merDepth and LINKS_OUTPUT ... merAlignerOutput and reads will be loaded if this module is executed
    char ckptName[MAX_FILE_PATH+30];
    sprintf(ckptName, "%s.fasta" GZIP_EXT, contigs);
    if (!doesLocalCheckpointExist(ckptName)) {
        restoreCheckpoint(ckptName);
    }
    sprintf(ckptName, "merDepth_%s.txt" GZIP_EXT, contigs);
    if (!doesLocalCheckpointExist(ckptName)) {
        restoreCheckpoint(ckptName);
    }
    if (num_paired) {
        sprintf(ckptName, "LINKS_OUTPUT_%d" GZIP_EXT, scaffoldRound);
        if (!doesLocalCheckpointExist(ckptName)) {
            restoreCheckpoint(ckptName);
        }
    }
    return 0;
}

void run_cgraph_scaffolding(cfg_t *cfg, int kmer_len, int max_kmer_len, char *tigs_out, int final)
{
    int64_t n_contigs = get_num_from_file_and_broadcast("", tigs_out);
    if (n_contigs == 0) SDIE("There were no contigs generated at this stage\n");
    serial_printf("WITH %lld contigs,extracted from %s\n", (lld) n_contigs, tigs_out);
    int scaffoldRound = final ? 1 : 0;
    
    int max_lib_read_len = 0;
    int max_ins_size = 0;
    int64_t n_scaffs = 0;
    char prev_srf[255] = "";
    Buffer bma_meta_libs_list = initBuffer(128);
    assert(getLengthBuffer(bma_meta_libs_list) == 0);
    int64_t n_tot_alns = 0;
    for (int libi = 0; libi < cfg->num_libs; libi++) {
        if (max_lib_read_len < cfg->libs[libi].read_len) max_lib_read_len = cfg->libs[libi].read_len;
        if (cfg->libs[libi].read_len == 0) {
            max_lib_read_len = 999999999; // assume the worst case here
            serial_printf("Assuming readlengths are very long in %s\n", cfg->libs[libi].name);
        }
    }
    LOGF("Found max_lib_read_len=%d\n", max_lib_read_len);

    char merAlignerOutput[255];
    sprintf(merAlignerOutput, "merAlignerOutput-cgraph%d-%i", scaffoldRound, kmer_len);

    char output_ext[100];
    sprintf(output_ext, "%d-scaff", kmer_len);
    // run meraligner and spanner (not splinter) on each lib in turn
    // we're rerunning merAligner with a potentially different kmer length than before

    int num_paired = 0;
    createMeralignerInterModuleState(max_lib_read_len * 2); // * 2 for scaffolds
    for (int libi = 0; libi < cfg->num_libs; libi++) {
        lib_t *lib = cfg->libs + libi;
        int seed_space = kmer_len < max_kmer_len ? 8 : 1;
        int allow_repetitive_seeds = 0;
        int is_merged = lib->files_per_pair == 1 && (cfg->merge_reads == 1 || (cfg->merge_reads == 2));
        run_meraligner_lib(cfg, &cfg->libs[libi], kmer_len, tigs_out, max_kmer_len, final ? IS_SCAFF : NOT_SCAFF,
                           cfg->cgraph_scaffolding, allow_repetitive_seeds,
                           merAlignerOutput, seed_space, _use_col);
        n_tot_alns += get_num_from_file_and_broadcast(lib->name, "TotalAlignments");
        if (lib->files_per_pair != 0) num_paired++;
    }
    destroyMeralignerInterModuleState();
    UPC_LOGGED_BARRIER;
        
    // run spanner and bmatolinks to get span links
    for (int libi = 0; libi < cfg->num_libs; libi++) {
        lib_t *lib = cfg->libs + libi;
        if (lib->files_per_pair == 0) continue; // no spans for single libs
        if (lib->ins_avg > max_ins_size) max_ins_size = lib->ins_avg;
        run_histogrammer_lib(cfg, lib, kmer_len, max_kmer_len, prev_srf,
                             n_contigs, n_scaffs, final ? IS_SCAFF : NOT_SCAFF, merAlignerOutput, output_ext, 1, _use_col);
        exec_stage(cfg->stages, IS_SCAFF, spanner_main, name_i(name_lib("spanner", lib->name), scaffoldRound),
                   "-l %s", lib->name,
                   "-R %B", lib->rev_comp,
                   "-A %B", lib->innie,
                   "-T %d", lib->three_p_wiggle,
                   "-F %d", lib->five_p_wiggle,
                   "-i %d", lib->ins_avg,
                   "-s %d", lib->std_dev,
                   "-r %d", lib->read_len,
                   "-L %d", scaffoldRound,
                   "-C %lld", (lld)n_contigs,
                   "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                   "-a %s", merAlignerOutput,
                   NULL);
        add_to_list(bma_meta_libs_list, "%s-bmaMeta-spans-%d", lib->name, scaffoldRound);
    }
    if (num_paired) {
        exec_stage(cfg->stages, IS_SCAFF, bmaToLinks_main, name_i("bmaToLinks", scaffoldRound),
               "-b %s", getStartBuffer(bma_meta_libs_list),
               "-m %d", kmer_len,
               "-C %lld", (lld)n_contigs,
               "-L %d", scaffoldRound,
               "-n %lld", (lld)n_tot_alns,
               "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
               "-P %d", max_ins_size,
               (cfg->is_metagenome ? "-M" : ""),
               NULL);
    }
    UPC_LOGGED_BARRIER;

    // prepare files for cgraph in the case of a checkpoint restore
    {
        Buffer merAlignerOutputList = initBuffer(MAX_FILE_PATH);
        Buffer readFileList = initBuffer(MAX_FILE_PATH);
        serial_printf("Preparing alignment files for cgraph\n");
        // contigs are loaded to local (per_rank) checkpoints, now ensure merged reads and alignments are too
        for (int libi = 0; libi < cfg->num_libs; libi++) {
            lib_t *lib = cfg->libs + libi;
            char ckpt[MAX_FILE_PATH];
            serial_printf("Preparing library %s for cgraph\n", lib->name);

            // restore read files in fofn
            char fofn[MAX_FILE_PATH+20];
            sprintf(fofn, "%s.fofn", lib->name);    
            Buffer fofnBuffer = broadcast_file(fofn);
            while (getsBuffer(fofnBuffer, ckpt, MAX_FILE_PATH) != NULL) {
                ckpt[strlen(ckpt) - 1] = '\0'; // remove trailing newline
                if (ckpt[0] == '\0') {
                    break;
                }
                if (!doesLocalCheckpointExist(ckpt)) {
                    restoreCheckpoint(ckpt);
                }
                if (getLengthBuffer(readFileList)) strcatBuffer(readFileList, ",");
                strcatBuffer(readFileList, ckpt);
            }
            freeBuffer(fofnBuffer);
            sprintf(ckpt, "%s-merAlignerOutput-cgraph%d-%d_Read1" GZIP_EXT, lib->name, scaffoldRound, kmer_len);
            if (getLengthBuffer(merAlignerOutputList)) strcatBuffer(merAlignerOutputList, ",");
            strcatBuffer(merAlignerOutputList, ckpt);
            if (!doesLocalCheckpointExist(ckpt)) {
                restoreCheckpoint(ckpt);
            }
            if (lib->files_per_pair > 0) {
                sprintf(ckpt, "%s-merAlignerOutput-cgraph%d-%d_Read2" GZIP_EXT, lib->name, scaffoldRound, kmer_len);
                if (getLengthBuffer(merAlignerOutputList)) strcatBuffer(merAlignerOutputList, ",");
                strcatBuffer(merAlignerOutputList, ckpt);
                if (!doesLocalCheckpointExist(ckpt)) {
                    restoreCheckpoint(ckpt);
                }
            }
        }
        cfg->cached_reads = 1; // currently cgraph requires reads to be in the per_rank dir and it should already be done

        UPC_LOGGED_BARRIER;
            
        prep_cgraph_contigs_and_links(tigs_out, scaffoldRound, num_paired);

        char merDepthTigs[100];
        sprintf(merDepthTigs, "merDepth_%s.txt", tigs_out);
        char links_output[64];
        sprintf(links_output, "LINKS_OUTPUT_%d", scaffoldRound);
        if (!final) {
            int did_run = exec_stage(cfg->stages, IS_SCAFF, cgraph_main, name_i("cgraph", scaffoldRound),
                       "-r %s", getStartBuffer(readFileList),
                       "-k %d", kmer_len,
                       "-K %d", max_kmer_len,
                       num_paired ? "-s %s" : "--", links_output, // only include links_output if there is a paired library that generated it
                       "-a %s", getStartBuffer(merAlignerOutputList),
                       "-d %s", merDepthTigs,
                       "-c %s", tigs_out,
                       "-B %B", cfg->cached_io,
                       "-l %s", cfg->local_tmp_dir,
                       "-x %d", 1,
                       "-q %s", (!cfg->cgraph_quality ? "normal" : cfg->cgraph_quality),
                       "-M %d", (get_shared_heap_mb() + 9) / 10,
                       NULL);
             
             // ensure any files cgraph writes are checkpointed
             char ckpt[MAX_FILE_PATH];
             sprintf(ckpt, "merDepth_cgraph-%d.txt" GZIP_EXT, kmer_len);
             restoreCheckpoint(ckpt);
             sprintf(ckpt, "cgraph-%d.fasta" GZIP_EXT, kmer_len);
             restoreCheckpoint(ckpt);
             if (did_run) {
                 // save the cgraph cgraph-%d.fasta.gz to results
                 char resultsPath[MAX_FILE_PATH+20];
                 sprintf(resultsPath, "results/%s", ckpt);
                 serial_printf("Generating conservative contigs: %s\n", resultsPath);
                 if (cfg->save_intermediates && MYSV.checkpoint_path) {
                     // the file should be generating.  force a sync and hard link to results
                     syncCheckpoint(ckpt);
                     UPC_LOGGED_BARRIER;
                     if (!MYTHREAD) {
                         char globalPath[MAX_FILE_PATH+20];
                         checkpointToGlobal(ckpt, globalPath);
                         if (does_file_exist(resultsPath)) {
                             WARN("Overwriting existing %s\n", resultsPath);
                             unlink(resultsPath);
                         }
                         link_chk(globalPath, resultsPath);
                     }
                 } else {
                     // manually save the aggregated per_rank files
                     char fastaPath[MAX_FILE_PATH+40];
                     sprintf(fastaPath, "%s%s", cfg->cached_io ? cfg->local_tmp_dir : "", ckpt);
                     get_rank_path(fastaPath, MYTHREAD);
                     saveAggregatedFileSync(fastaPath, resultsPath, NULL);
                 }
             }
             // force any checkpoints to be on disk
             cleanupCheckpoints(1);
        } else {
            // final (will output)
            exec_stage(cfg->stages, IS_SCAFF, cgraph_main, "cgraph-final",
                       "-r %s", getStartBuffer(readFileList),
                       "-o %s", "results", 
                       "-k %d", kmer_len,
                       "-K %d", max_kmer_len,
                       "-L %d", cfg->assm_scaff_len_cutoff,
                       num_paired ? "-s %s" : "--", links_output, // only include links_output if there is a paired library that generated it 
                       "-a %s", getStartBuffer(merAlignerOutputList),
                       "-d %s", merDepthTigs,
                       "-c %s", tigs_out,
                       "-B %B", cfg->cached_io,
                       "-T %s", cfg->local_tmp_dir,
                       "-x %d", 0,
                       "-q %s", (!cfg->cgraph_quality ? "normal" : cfg->cgraph_quality),
                       "-M %d", (get_shared_heap_mb() + 9) / 10,
                       NULL);
        }
        freeBuffer(merAlignerOutputList);
        freeBuffer(readFileList);
    }
}

int main_main(int argc, char **argv)
{
    double start_mem_free = get_free_mem_gb();
    double start_mem_used = get_used_mem_gb();

    char *use_col_str = getenv("USECOLOR");

    if (use_col_str && atoi(use_col_str) == 1) {
        _use_col = strdup(KLBLUE);
    } else {
        _use_col = strdup(""); //strdup(KNORM);
    }

    // this call sets up the per_rank directories
    OPEN_MY_LOG("HipMer");
    LOG_SET_MEMCHECK( (LOG_ALL_UPC_ALLOC | LOG_ALL_MEM) );
    log_mem();

    init_diags();
    log_type_sizes();

    serial_printf("\n%s%s" KNORM "\n", _use_col, HASH_BAR);
    serial_printf("%s# Starting HipMer version %s (single exec %s) on %d threads" KNORM "\n",
                  _use_col, HIPMER_VERSION, HIPMER_BUILD_DATE, THREADS);
#ifdef DEBUG
    serial_printf("Debug build with log level %d\n", HIPMER_VERBOSITY);
#endif

    char cwd[MAX_FILE_PATH];
    serial_printf("%s# Current directory is %s" KNORM "\n", _use_col, getcwd(cwd, MAX_FILE_PATH));
    char *dryrun_str = getenv("DRYRUN");
    int dryrun = 0;
    if (dryrun_str && atoi(dryrun_str) == 1) {
        serial_printf("%s# DRYRUN (not executing stages)" KNORM "\n", _use_col);
        dryrun = 1;
    }
    serial_printf("%s# UPC shared heap is %d MB per thread" KNORM "\n", _use_col, get_shared_heap_mb());

    char *autorestart_ufx_str = getenv("AUTORESTART_UFX");
    int autorestart_ufx = 0;
    if (autorestart_ufx_str && atoi(autorestart_ufx_str) == 1) {
        serial_printf("%s# AUTORESTART_UFX (restart after UFX to free memory" KNORM "\n", _use_col);
        autorestart_ufx = 1;
    }

    upc_tick_t start_time = upc_ticks_now();
    cfg_t cfg;
    get_config(argc, argv, &cfg, _use_col);

    if (MYTHREAD % cfg.cores_per_node == 0) {
        char host[256];
        gethostname(host, 255);
        LOGF("host %s\n", host);
    }
    {
        char buf[256];
        get_cpu_affinity(buf);
        LOGF("cpu_affinity: %s\n", buf);
    }
    const char *all_inputs_fofn = "ALL_INPUTS.fofn";

    const int chunk_size = 100;
    const int load_factor = 1;
    char mer_depth_prefix[MAX_FILE_PATH] = "";
    char tigs_out[100] = "";
    char last_contigs_out[100] = "";
    char last_alignment_out[MAX_FILE_PATH] = "";
    char last_alignment_tigs_out[MAX_FILE_PATH] = "";

    int ufx_err_count = cfg.min_depth_cutoff;
    if (ufx_err_count <= 1) {
        ufx_err_count = 2;
    }

    // FIXME Issue #217 - instead of copying the file to per_rank dirs, extend kcount to be able to read the single fastq files directly in parallel
    if ((!cfg.cached_io) && (!cfg.cached_reads)) {
        serial_printf("Setting cached_reads so that kcount can read the input files by thread\n");
        cfg.cached_reads = 1;
    }

    int kmer_len_last = 0;
    int min_contig_len = cfg.min_contig_len;

    
    LOG_VERSION;

    // now set up the input files
    if (cfg.stages == NULL || !strstr(cfg.stages, "-end")) {
        setup_fofns(all_inputs_fofn, &cfg);
        if (cfg.cached_io && (MYTHREAD % cfg.cores_per_node) == 0 && strcmp(BASE_DIR(cfg.cached_io, cfg.local_tmp_dir), cfg.local_tmp_dir) == 0) {
            upc_tick_t t = upc_ticks_now();
            serial_printf("As this is not a restart, purging %s/per_rank intermediate files\n", cfg.local_tmp_dir);
            lld bytes = 0;
            int files = 0, dirs = 0;
            char p[MAX_FILE_PATH];
            strcpy(p, cfg.local_tmp_dir);
            strcat(p, "/per_rank");
            int purged = purgeDir(p, &bytes, &files, &dirs);
            if (purged) {
                LOGF("purged %d files from %s in %0.2f s (%0.3lf GB)\n",
                      purged, p, ((double)upc_ticks_to_ns(upc_ticks_now() - t) / 1000000000.0),
                      ((double)bytes) / ONE_GB);
                if (purged != files) {
                    WARN("Could not purge %d files in %s!\n", files - purged, p);
                }
            }
        }
        UPC_LOGGED_BARRIER;
    } else {
        serial_printf("Skipping fofn setup as this is a restart\n");
    }

    if (cfg.merge_reads == 1) {
        // merge all reads up front as first stage
        merge_reads(&cfg, all_inputs_fofn);
        UPC_LOGGED_BARRIER;
        clear_lustre_caches();
    }

    if (cfg.merge_reads != 1 && (cfg.cached_io || cfg.cached_reads)) {
        char all_libs[4096] = "";
        get_all_lib_names(&cfg, all_libs);

        int allHaveLoadFQ = 0;
        char check[MAX_FILE_PATH];
        if (cfg.cached_io) {
            // in support of checkpointing, loadfq is special, so ensure it gets re-run if some threads are missing the check file
            sprintf(check, "%s/.LOAD_FQ", cfg.local_tmp_dir);
            get_rank_path(check, MYTHREAD);
            allHaveLoadFQ = reduce_int(does_file_exist(check), UPC_ADD, ALL_DEST);
        } else {
            sprintf(check, ".LOAD_FQ");
            get_rank_path(check, -1);
            if (MYTHREAD == 0) {
                allHaveLoadFQ = does_file_exist(check);
            }
            allHaveLoadFQ = broadcast_int(allHaveLoadFQ, 0);
        }

        LOGF("load_fq is %s\n", allHaveLoadFQ == THREADS ? "NOT needed" : "NEEDED");

        int didRun = exec_stage(allHaveLoadFQ == THREADS ? cfg.stages : "all", NOT_SCAFF, loadfq_main, "loadfq",
                                "-N %d", cfg.cores_per_node,
                                "-f %s", all_libs,
                                "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                                "-p %B", 1, /* Purge cfg.local_tmp_dir if this stage runs */
                                NULL);

        rebuild_all_inputs_fofn(all_inputs_fofn, &cfg, "LOADED_FQ", 0); // do not overwrite an existing all_inputs_fofn, as loadfq may be specially run at every restart

        // just create the check file
        if (MYTHREAD % cfg.cores_per_node == 0) {
            if (!MYTHREAD || cfg.cached_io) {
                fclose_track(fopen_chk(check, "w"));
            }
        }
        cfg.cached_reads = 1;
    }
    clear_lustre_caches();

    if (cfg.cached_io && MYSV.checkpoint_path && cfg.stages && strstr(cfg.stages, "-end")) {
        // This is a restart, with checkpoints so ensure all per_rank files are synced to global
        syncMissingGlobalCheckpoints();
    }

    // initialize upcxx
    UPC_LOGGED_BARRIER;
    init_upcxx();
    if (local_rank_me() == 0 || MYTHREAD == THREADS-1 || MYTHREAD == THREADS/2) {
        init_logger_cxx("HipMer-CXX.log", 1, 1);
    } 
#ifdef DEBUG
    else {
        init_logger_cxx("HipMer-CXX.log", 1, 1);
    }
    open_dbg_cxx("HipMer-CXX");
#endif
    UPC_LOGGED_BARRIER;
    
    char inufx[255], all_inputs_fofn_kmer[255];
    int max_kmer_len = 0;
    inufx[0] = '\0';
    // kmer length loop for metagenomes
    for (int kmer_i = 0; kmer_i < cfg.num_mer_sizes; kmer_i++) {
        int kmer_len = cfg.mer_sizes[kmer_i];
        if (kmer_len > max_kmer_len) {
            max_kmer_len = kmer_len;
        }
        // separate copies of ufx output to restart at any stage
        sprintf(all_inputs_fofn_kmer, "%s-%d", all_inputs_fofn, kmer_len);
        if (!MYTHREAD) {
            unlink(all_inputs_fofn_kmer); // okay to fail
            if (link(all_inputs_fofn, all_inputs_fofn_kmer) != 0) {
                if (errno != 17) {
                    DIE("Could not link %s to %s: %s\n", all_inputs_fofn, all_inputs_fofn_kmer,
                        strerror(errno));
                }
            }
        }
        sprintf(inufx, "%s-%d.ufx.bin", all_inputs_fofn, kmer_len);

        // before UFX starts, sync all checkpoints 
        // UFX, being MPI based, does not participate directly with checkpoints 
        // so make sure they are flushed before starting to support a restart after an error
        cleanupCheckpoints(1);
        UPC_LOGGED_BARRIER;

        if (cfg.cached_reads) {
            // restore checkpoints for all reads in all_inputs_fofn_kmer
            Buffer fofnBuffer = broadcast_file(all_inputs_fofn_kmer);
            char file[MAX_FILE_PATH];
            while ( getsBuffer(fofnBuffer, file, MAX_FILE_PATH) != NULL ) {
                file[strlen(file)-1] = '\0'; // remove newline
                if (! doesLocalCheckpointExist(file) ) {
                    restoreCheckpoint(file);
                    strcat(file, ".uncompressedSize");
                    if ( !doesLocalCheckpointExist(file) && doesGlobalCheckpointExist(file) ) {
                        restoreCheckpoint(file);
                    }
                } else {
                    LOGF("Verified local checkpoint for %s is ready\n", file);
                }
            }
            freeBuffer(fofnBuffer);
        }

        // FIXME: unpack fofn into a comma separated list to pass as -r
        Buffer read_file_list = initBuffer(MAX_FILE_PATH*10);
        char line_buf[MAX_FILE_PATH];
        size_t sz = 0;
        Buffer fofnBuffer = broadcast_file(all_inputs_fofn_kmer);
        while ( getsBuffer(fofnBuffer, line_buf, MAX_FILE_PATH) != NULL ) {
            line_buf[strlen(line_buf) - 1] = '\0'; // remove newline
            if (getLengthBuffer(read_file_list)) strcatBuffer(read_file_list, ",");
            strcatBuffer(read_file_list, line_buf);
        }
        freeBuffer(fofnBuffer);
        int ufx_ran = 0;
        // adjust buffering to suit the number of threads, i.e. to always have a constant value per rank
        // use 10% of the shared memory
        int max_kmer_store = 12 * ONE_MB * get_shared_heap_mb() / 100; // 12% of shared memory
#ifndef FIND_NEW_MERS        
        if (kmer_i == 0) {
#endif
            ufx_ran = exec_stage(cfg.stages, NOT_SCAFF, kcount_main, name_i("kcount", kmer_len),
                                 "-k %d", kmer_len,
                                 "-Q %d", cfg.qual_offset,
                                 "-r %s", getStartBuffer(read_file_list),
                                 "-d %d", ufx_err_count,
                                 "-D %f", cfg.dynamic_min_depth,
                                 "-b %B", cfg.use_bloom_filter,
                                 "-B %B", cfg.cached_io,
                                 "-l %s", cfg.local_tmp_dir,
                                 "-m %d", max_kmer_store,
                                 NULL);
#ifndef FIND_NEW_MERS        
        } else {
            char ctgs_fname[MAX_FILE_PATH];
            sprintf(ctgs_fname, "%s.fasta.gz", (char*)tigs_out);
            restoreCheckpoint(ctgs_fname);
            char mer_depth_fname[MAX_FILE_PATH];
            sprintf(mer_depth_fname, "merDepth_%s.bin.gz", (char*)tigs_out);
            restoreCheckpoint(mer_depth_fname);
            ufx_ran = exec_stage(cfg.stages, NOT_SCAFF, kcount_main, name_i("kcount", kmer_len),
                                 "-k %d", kmer_len,
                                 "-p %d", kmer_len_last,
                                 "-Q %d", cfg.qual_offset,
                                 "-r %s", getStartBuffer(read_file_list),
                                 "-d %d", ufx_err_count,
                                 "-D %f", cfg.dynamic_min_depth,
                                 "-c %s", ctgs_fname,
                                 "-f %s", mer_depth_fname,
                                 "-b %B", cfg.use_bloom_filter,
                                 "-B %B", cfg.cached_io,
                                 "-m %d", max_kmer_store,
                                 NULL);
        }
#endif
        freeBuffer(read_file_list);
        UPC_LOGGED_BARRIER;
        if ((cfg.cached_io | cfg.save_intermediates) && MYSV.checkpoint_path) {
            // force a global checkpoint to be created
            serial_printf("Saving ufx checkpoint\n");
            char ckpt[MAX_FILE_PATH];
            sprintf(ckpt, "%s%s", inufx, GZIP_EXT);
            if (ufx_ran || (doesLocalCheckpointExist(ckpt) && !doesGlobalCheckpointExist(ckpt)) ) {
                restoreCheckpoint( ckpt );
                sprintf(ckpt, "%s%s.entries", inufx, GZIP_EXT);
                restoreCheckpoint( ckpt );
            }
        }

        int min_depth_cutoff = 2; // default if not config entry is present.
        if (!MYTHREAD && !dryrun) {
            if (cfg.is_metagenome) {
                serial_printf("Using config file value of min_depth_cutoff=%d\n", cfg.min_depth_cutoff);
                min_depth_cutoff = cfg.min_depth_cutoff;
            } else {
                char histo_fname[MAX_FILE_PATH];
                snprintf(histo_fname, MAX_FILE_PATH, "histogram_k%d.txt", kmer_len);
                get_rank_path(histo_fname, -1);
                min_depth_cutoff = find_dmin(histo_fname);
                serial_printf("Calculated min_depth_cutoff from histogram as %d. Config file setting is %d.\n",
                              min_depth_cutoff, cfg.min_depth_cutoff);
                if (!cfg.min_depth_cutoff) {
                    if (!(min_depth_cutoff)) {
                        if (cfg.is_metagenome) {
                            min_depth_cutoff = 2;
                            serial_printf("Using conservative min_depth_cutoff = 2 for this metagenome.\n");
                        } else {
                            SDIE("Cannot calculate min_depth_cutoff from histogram; please set in config file.\n");
                        }
                    }
                } else {
                    serial_printf("Using config file value of min_depth_cutoff=%d\n", cfg.min_depth_cutoff);
                    min_depth_cutoff = cfg.min_depth_cutoff;
                }
            }
        }
        // Ensure all threads use the same value
        cfg.min_depth_cutoff = broadcast_long(min_depth_cutoff, 0);
        serial_printf("Using min_depth_cutoff=%d and dynamic_min_depth=%0.3f\n", cfg.min_depth_cutoff, cfg.dynamic_min_depth);
        UPC_LOGGED_BARRIER;
        if (kmer_i == 0) {
            // make sure we have all the read lengths - calculated by ufx for variable length reads
            int overallmax = 0;
            for (int lib_i = 0; lib_i < cfg.num_libs; lib_i++) {
                lib_t *lib = &cfg.libs[lib_i];
                int maxReadLen = lib->read_len;
                if (!MYTHREAD) {
                    char buf[MAX_FILE_PATH];
                    sprintf(buf, "%s.fofn", lib->name);
                    FILE *f = fopen_chk(buf, "r");
                    Buffer fofn = initBuffer(MAX_FILE_PATH);
                    char * line = NULL;
                    while ( (line = fgetsBuffer(fofn, MAX_FILE_PATH, f)) != NULL) {
                        line[strlen(line)-1] = '\0'; // chop new line
                        sprintf(buf, "%s.maxReadLen.txt", basename(line));
                        get_rank_path(buf, -1);
                        if (does_file_exist(buf)) {
                            sprintf(buf, "%s.maxReadLen.txt", basename(line));
                            int maxlen = get_num_from_file(NULL, buf); 
                            if (!maxlen) {
                                SDIE("read length is not set in config file for library %s and could"
                                     " not find it in %s\n", lib->name, line);
                            }
                            if (maxReadLen < maxlen) maxReadLen = maxlen;
                        }
                    }
                    fclose_track(f);
                    snprintf(buf, MAX_FILE_PATH, "%s-readlen.txt", lib->name);
                    get_rank_path(buf, -1);
                    if (does_file_exist(buf)) {
                        f = fopen_chk(buf, "r");
                        while ( (line = fgets(buf, MAX_FILE_PATH, f)) != NULL) {
                            line[strlen(line)-1] = '\0';
                            int maxlen = atoi(line);
                            if (maxReadLen < maxlen) maxReadLen = maxlen;
                        }
                        fclose_track(f);
                    }
                    freeBuffer(fofn);
                    if (maxReadLen == 0) {
                        SWARN("lib=%s file=%s has no recorded maximum read length, so the maximum read length is assumed to be in the other libraries.\n", lib->name, buf);
                    }
                }
                // broadcast just the max of all files in the library
                lib->read_len = broadcast_int(maxReadLen, 0);
                serial_printf("%sFor lib %s max read len is %d" KNORM "\n", _use_col, lib->name, lib->read_len);
                
                if (overallmax < lib->read_len) {
                    overallmax = lib->read_len;
                }
            }
        }

#ifdef FIND_NEW_MERS
        if (kmer_i > 0) {
            exec_stage(cfg.stages, NOT_SCAFF, findNewMers_main, name_i("findNewMers", kmer_len),
                       "-k %d", kmer_len,
                       "-N %d", cfg.cores_per_node,
                       "-i %s", inufx,
                       "-C %d", chunk_size,
                       "-l %d", load_factor,
                       "-d %d", cfg.min_depth_cutoff,
                       "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-X %B", cfg.cached_io,
                       "-p %d", kmer_len_last,
                       "-D %f", cfg.dynamic_min_depth,
                       "-c %s", tigs_out, NULL);
        }
#endif

        if (cfg.is_metagenome) {
            min_contig_len = kmer_len;
        }
        sprintf(tigs_out, "UUtigs_contigs-%d", kmer_len);

        if (autorestart_ufx && dummy_exec(name_i("meraculous", kmer_len))) {
            serial_printf("Planned restart executing to clear UFX memory...\n");
            upc_global_exit(1);
        }

        // clear out any unnecessary files from the previous round located in cfg.local_tmp_dir and optionally intermediates
        if (cfg.cached_io && kmer_len_last > 0) {
            serial_printf("Purging old checkpoints from kmer_len=%d %s %s\n", kmer_len_last, cfg.local_tmp_dir, cfg.save_intermediates ? "" : "and intermediates");
            char ckptName[MAX_FILE_PATH];
            for (int li = 0; li < cfg.num_libs; li++) {
                lib_t *lib = &cfg.libs[li];
                for (int readNum = 1 ; readNum <= (lib->files_per_pair == 0 ? 1 : 2); readNum++) {
                    sprintf(ckptName, "%s-merAlignerOutput-%d_Read%d" GZIP_EXT, lib->name, kmer_len_last, readNum);
                    if (cfg.save_intermediates) {
                        unlinkLocalCheckpoint(ckptName);
                        serial_printf("Unlinked local cache of %s\n", ckptName);
                    } else {
                        unlinkCheckpoint(ckptName);
                        serial_printf("Unlinked all checkpoints of %s\n", ckptName);
                    }
                }
            }
            const char *patterns[] = { "UUtigs_contigs-%d.fasta%s", 
                                       "merDepth_UUtigs_contigs-%d.txt%s", 
                                       "UUtigs_contigs-%d.cea%s", 
                                       "Bubbletigs_diplotigs-%d.fasta%s",
                                       "merDepth_Bubbletigs_diplotigs-%d.txt%s",
                                       "pruned_Bubbletigs_diplotigs-%d.fasta%s",
                                       "merDepth_pruned_Bubbletigs_diplotigs-%d.txt%s",
                                       "walks_pruned_Bubbletigs_diplotigs-%d.txt%s",
                                       "walks_pruned_Bubbletigs_diplotigs-%d.fasta%s",
            };
            for(int i = 0 ; i < sizeof(patterns)/sizeof(char*); i++) {
                sprintf(ckptName, patterns[i], kmer_len_last, GZIP_EXT);
                if (cfg.save_intermediates) {
                    unlinkLocalCheckpoint(ckptName);
                    serial_printf("Unlinked local cache of %s\n", ckptName);
                } else {
                    unlinkCheckpoint(ckptName);
                    serial_printf("Unlinked all checkpoints of %s\n", ckptName);
                }
            }
        }

        exec_stage(cfg.stages, NOT_SCAFF, meraculous_main, name_i("meraculous", kmer_len),
                   "-k %d", kmer_len,
                   "-N %d", cfg.cores_per_node,
                   "-i %s", inufx,
                   "-m %d", min_contig_len,
                   "-o %s", tigs_out,
                   "-c %d", chunk_size,
                   "-l %d", load_factor,
                   "-d %d", cfg.min_depth_cutoff,
                   "-D %f", cfg.dynamic_min_depth,
                   "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                   "-X %B", cfg.cached_io,
                   NULL);

        strcpy(last_contigs_out, tigs_out);
        sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);
        exec_stage(cfg.stages, NOT_SCAFF, contigMerDepth_main, name_i("contigMerDepth", kmer_len),
                   "-k %d", kmer_len, "-i %s", inufx, "-c %s", tigs_out, "-d %d", cfg.min_depth_cutoff,
                   "-D %f", cfg.dynamic_min_depth, "-s %d", chunk_size, "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir), 
                   "-X %B", cfg.cached_io, NULL);
        int64_t n_contigs = get_num_from_file_and_broadcast("", tigs_out);
        if (n_contigs <= 0) {
            SDIE("There were no contigs generated at this stage\n");
        }
        if (cfg.is_diploid || cfg.is_metagenome) {
            exec_stage(cfg.stages, NOT_SCAFF, contigEndAnalyzer_main, name_i("contigEndAnalyzer", kmer_len),
                       "-i %s", inufx,
                       "-k %d", kmer_len,
                       "-c %s", tigs_out,
                       "-d %d", cfg.min_depth_cutoff,
                       "-D %f", cfg.dynamic_min_depth,
                       "-s %d", chunk_size,
                       "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-X %B", cfg.cached_io,
                       NULL);

	    Buffer merAlignerBubbleOutputList = initBuffer(MAX_FILE_PATH);
	    
            if (cfg.high_heterozygosity && !cfg.is_metagenome) {

	        // align reads to contigs for haplotype phasing   (for how, restricted to high_het diploids only)
	        char merAlignerBubbleOutput[255];
		sprintf(merAlignerBubbleOutput, "merAlignerOutput-%d-bubble",kmer_len);

                int max_lib_read_len = 0;
                for (int i = 0; i < cfg.num_libs; i++) {
                    lib_t *lib = &cfg.libs[i];
                    if (lib->read_len == 0) {
                        max_lib_read_len = 999999999; // assume the worst case here
                        serial_printf("Assuming readlengths are very long in %s\n", lib->name);
                    }
                    if (max_lib_read_len < lib->read_len) {
                        max_lib_read_len = lib->read_len;
                    }
                }
                LOGF("Found max_lib_read_len=%d\n", max_lib_read_len);
                createMeralignerInterModuleState(max_lib_read_len);
                for (int i = 0; i < cfg.num_libs; i++) {
                    lib_t *lib = &cfg.libs[i];
                    int seed_space = 8;
                    int is_merged = lib->files_per_pair == 1 && (cfg.merge_reads == 1 || cfg.merge_reads == 2);
                    run_meraligner_lib(&cfg, lib, kmer_len, tigs_out, min_contig_len, NOT_SCAFF, 0, 0, merAlignerBubbleOutput, seed_space, _use_col);
		    add_to_list(merAlignerBubbleOutputList, "%s-%s", lib->name, merAlignerBubbleOutput);  
                }
                destroyMeralignerInterModuleState();
            }

            char full_name[MAX_FILE_PATH];
            char *bubble_input = strdup_chk(tigs_out);
            sprintf(tigs_out, "Bubbletigs_diplotigs-%d", kmer_len);
            int only_short = 0;
            if (cfg.is_metagenome) {
                only_short = 1;
            }

            if (getLengthBuffer(merAlignerBubbleOutputList) != 0) {
                serial_printf("%sRunning bubble finding with read alignment support to facilitate dual-haplotype diplotig formation... " KNORM "\n", _use_col);
            }


            exec_stage(cfg.stages, NOT_SCAFF, bubbleFinder_main, name_i("bubbleFinder", kmer_len),
                       "-k %d", kmer_len,
                       "-N %lld", (lld)n_contigs,
                       "-d %s", bubble_input,
                       "-c %s", bubble_input,
                       "-o %s", tigs_out,
                       "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-D %d", cfg.bubble_min_depth_cutoff,
                       "-s %d", only_short,
                       "-K %B", cfg.high_heterozygosity,
                       (cfg.high_heterozygosity ? "-a %s" : "--"), getStartBuffer(merAlignerBubbleOutputList),
                       NULL);
	    resetBuffer(merAlignerBubbleOutputList);
            strcpy(last_contigs_out, tigs_out);


	    freeBuffer(merAlignerBubbleOutputList); 
            free_chk(bubble_input);
        } else {
            serial_printf("%sSkipping bubble finding as this is not diploid or metagenome" KNORM "\n", _use_col);
        }

        if (cfg.is_metagenome) {
            n_contigs = get_num_from_file_and_broadcast("", tigs_out);
            if (n_contigs == 0) {
                SDIE("There were no contigs generated at this stage\n");
            }
            sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);
            exec_stage(cfg.stages, NOT_SCAFF, progressiveRelativeDepth_main,
                       name_i("progressiveRelativeDepth", kmer_len),
                       "-k %d", kmer_len,
                       "-a %f", cfg.alpha,
                       "-t %f", cfg.tau,
                       "-b %f", cfg.beta,
                       "-i %s", inufx,
                       "-c %s", tigs_out,
                       "-s %d", chunk_size,
                       "-d %d", cfg.min_depth_cutoff,
                       "-D %f", cfg.dynamic_min_depth,
                       "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-X %B", cfg.cached_io,
                       "-N %lld", (lld)n_contigs,
                       "-C %s", mer_depth_prefix,
                       "-P %B", cfg.skip_la,
                       "-M %d", MERGE_FLAG,
                       NULL);
            prepend_str(tigs_out, "pruned");
            strcpy(last_contigs_out, tigs_out);
            sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);
        }

        if (kmer_i == cfg.num_mer_sizes - 1) {
            sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);
            UPC_LOGGED_BARRIER;
            break; // last kmer iteration

            // remove previous version of UFX file if we are using cached_io or save_intermediates
            if (cfg.cached_io) {
                char prevCheckpoint[MAX_FILE_PATH];
                snprintf(prevCheckpoint, MAX_FILE_PATH, "%s%s", inufx, GZIP_EXT);
                if (cfg.save_intermediates) {
                    // just unlink the local copy (cached_io)
                    unlinkLocalCheckpoint(prevCheckpoint);
                } else {
                    unlinkCheckpoint(prevCheckpoint);
                }
            } else if (!cfg.cgraph_scaffolding && !cfg.save_intermediates) {
#ifdef UFX_WRITE_SINGLE_FILE
                if (!MYTHREAD) {
                    if (unlink(inufx) != 0) LOG("Strange... Could not unlink old ufx file: %s %s!\n", inufx, strerror(errno));
                }
#endif
            }
        }

        if (!cfg.skip_la) {
            char merAlignerOutput[255];
            sprintf(merAlignerOutput, "merAlignerOutput-%d", min_contig_len);
            char output_ext[100];
            sprintf(output_ext, "%d", min_contig_len);

            int max_lib_read_len = 0;
            for (int i = 0; i < cfg.num_libs; i++) {
                lib_t *lib = &cfg.libs[i];
                if (max_lib_read_len < lib->read_len) {
                    if (lib->read_len == 0) {
                        max_lib_read_len = 999999999; // assume the worst case here
                        serial_printf("Assuming readlengths are very long in %s\n", lib->name);
                    }
                    max_lib_read_len = lib->read_len;
                }
            }
            LOGF("Found max_lib_read_len=%d\n", max_lib_read_len);
            createMeralignerInterModuleState(max_lib_read_len);
            for (int i = 0; i < cfg.num_libs; i++) {
                lib_t *lib = &cfg.libs[i];
                if (lib->for_contigging) {
                    int seed_space = 8;
                    int is_merged = lib->files_per_pair == 1 && (cfg.merge_reads == 1 || cfg.merge_reads == 2);
                    run_meraligner_lib(&cfg, lib, kmer_len, tigs_out, min_contig_len, NOT_SCAFF, 0, 0,
                                       merAlignerOutput, seed_space, _use_col);
                    run_histogrammer_lib(&cfg, lib, kmer_len, min_contig_len, "", 0, 0, NOT_SCAFF,
                                         merAlignerOutput, output_ext, 0, _use_col);
                }
            }
            destroyMeralignerInterModuleState();
            strcpy(last_alignment_out, merAlignerOutput);
            strcpy(last_alignment_tigs_out, tigs_out);

            // get all lib params for gap closing and local assembly
            int max_readlen = 0;
            char localassm_libs[4096] = "";
            char list_insert_sizes[1024] = "", list_insert_sigmas[1024] = "", list_revcomps[1024] = "";
            char list_5p_wiggles[1024] = "", list_3p_wiggles[1024] = "";
            get_list_lib_params(&cfg, &max_readlen, localassm_libs, list_insert_sizes, list_insert_sigmas,
                                list_revcomps, list_5p_wiggles, list_3p_wiggles, 1);
            if (max_readlen) {
                int64_t n_filtered_contigs = get_num_from_file_and_broadcast("", tigs_out);
                exec_stage(cfg.stages, NOT_SCAFF, localassm_main, name_i("localassm", kmer_len),
                           "-M %d", cfg.la_mer_size_max,
                           "-Q %d", cfg.qual_offset,
                           "-c %s", tigs_out,
                           "-a %s", merAlignerOutput,
                           "-b %s", localassm_libs,
                           "-i %s", list_insert_sizes,
                           "-I %s", list_insert_sigmas,
                           "-m %d", kmer_len,
                           "-P",
                           "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                           "-X %B", cfg.cached_reads,
                           "-r %s", list_revcomps,
                           "-F %s", list_3p_wiggles,
                           "-T %s", list_5p_wiggles,
                           "-l %d", max_readlen,
                           "-N %lld", (lld)n_filtered_contigs,
                           "-C %s", mer_depth_prefix,
                           "-z %d", cfg.la_mer_size_min,
                           "-t %f", cfg.la_depth_dist_thres,
                           "-v %f", cfg.la_nvote_diff_thres,
                           "-H %d", cfg.la_hi_qual_thres,
                           "-q %d", cfg.la_qual_thres,
                           "-d %f", cfg.la_min_viable_depth,
                           "-D %f", cfg.la_min_expected_depth, 
                           "-R %d", cfg.la_rating_thres,
                           NULL);
                prepend_str(tigs_out, "walks");
            }

            if (cfg.localize_reads == kmer_i + 1) {
                int64_t n_filtered_contigs = get_num_from_file_and_broadcast("", last_alignment_tigs_out);
                serial_printf("Sorting reads by how they align to contigs (%lld) for better localization\n", (lld)n_filtered_contigs);
                int didRun = 0, numToRun = 0;
                for (int i = 0; i < cfg.num_libs; i++) {
                    lib_t *lib = &cfg.libs[i];
                    if (!lib->for_contigging) {
                        continue;                       // can not localize at the moment
                    }
                    numToRun++;
                    char libname_fofn[MAX_FILE_PATH], readFileName[2][MAX_FILE_PATH], alignmentFilePrefix[MAX_FILE_PATH];
                    readFileName[0][0] = readFileName[1][0] = '\0';
                    snprintf(alignmentFilePrefix, MAX_FILE_PATH, "%s", last_alignment_out);

                    char stage_name[80];
                    snprintf(stage_name, 80, "localize_reads-%s", lib->name);
                    didRun += exec_stage(cfg.stages, NOT_SCAFF, read_shuffler_main, stage_name,
                                         "-L %s", lib->name,
                                         "-l %d", i,
                                         "-A %s", alignmentFilePrefix,
                                         "-f %d", lib->files_per_pair,
                                         "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                                         "-X %B", cfg.cached_reads,
                                         "-n %lld", (lld)n_filtered_contigs,
                                         NULL
                        );

                    clear_lustre_caches();

                }
                // signal to use "per_rank" for reads -- will be in cfg.local_tmp_dir if cfg.cached_io, ./per_rank if ! cfg.cached_io
                rebuild_all_inputs_fofn(all_inputs_fofn, &cfg, "LOCALIZED", 0);
                cfg.cached_reads = 1;
            }

            exec_stage(cfg.stages, NOT_SCAFF, prefixMerDepthExtended_main,
                       name_i("prefixMerDepthExtended", kmer_len),
                       "-k %d", kmer_len,
                       "-i %s", inufx,
                       "-c %s", tigs_out,
                       "-d %d", cfg.min_depth_cutoff,
                       "-e %f", cfg.error_rate,
                       "-s %d", chunk_size,
                       "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-X %B", cfg.cached_io,
                       NULL);
            prepend_str(tigs_out, "extended");
            strcpy(last_contigs_out, tigs_out);

            sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);

            // remove previous version of UFX file if we are using cached_io or not save_intermediates
            if (cfg.cached_io) {
                char prevCheckpoint[MAX_FILE_PATH];
                snprintf(prevCheckpoint, MAX_FILE_PATH, "%s%s", inufx, GZIP_EXT);
                if (cfg.save_intermediates) {
                    // just unlink the local copy (cached_io)
                    unlinkLocalCheckpoint(prevCheckpoint);
                } else {
                    unlinkCheckpoint(prevCheckpoint);
                }
            }
#ifdef UFX_WRITE_SINGLE_FILE
            else if (!cfg.save_intermediates) {
                if (!MYTHREAD) {
                    if (unlink(inufx) != 0) LOG("Strange... Could not unlink old ufx file: %s %s!\n", inufx, strerror(errno));
                }
            }
#endif
        } // end of  if (!cfg.skip_la) 

        UPC_LOGGED_BARRIER;

        kmer_len_last = kmer_len;

        if (cfg.canonicalize) {
            // write out the contigs so far
            char contigs_fname[MAX_FILE_PATH];
//        snprintf(contigs_fname, MAX_FILE_PATH, "%s_", tigs_out);
            // This should print out the contigs we'd see if we stopped the loop here, i.e. before walks
            // prefix extension
            snprintf(contigs_fname, MAX_FILE_PATH, "%s", last_contigs_out);
            char contigs_out_name[MAX_FILE_PATH];
            snprintf(contigs_out_name, MAX_FILE_PATH, "contigs-%d.fa", kmer_len);
            exec_stage(cfg.stages, NOT_SCAFF, canonical_assembly_main, name_i("canonical_assembly-contigs", kmer_len),
                       "-o %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                       "-n %d", cfg.cores_per_node,
                       "-f %s", contigs_fname,
                       "-F %s", ".fasta",
                       "-O %s", contigs_out_name,
                       "-d %B", 1,
                       NULL);
        }
    }

    UPC_LOGGED_BARRIER;
    int64_t n_contigs = get_num_from_file_and_broadcast("", tigs_out);
    if (n_contigs == 0) {
        SDIE("There were no contigs generated at this stage\n");
    }
    serial_printf("WITH %lld contigs,extracted from %s\n", (lld) n_contigs, tigs_out);

    int contig_mer_size_max = cfg.mer_sizes[cfg.num_mer_sizes - 1];
    int scaff_mer_size_max = cfg.scaff_mer_size;

#ifdef ITERATIVE_CONTIG_EXTENSION
    /* Iterative assembly module */

    int iter_k = 37;
    int n_iters = 4;

    int iter_assembly = iterative_contig_extension(cfg, tigs_out, iter_k, scaff_mer_size_max, n_iters,
                                                   merAlignerOutput, output_ext, _use_col);
#endif

    // output contigs, do canonical ordering of assembly of contigs for smaller datasets or if requested
    if (cfg.canonicalize) {
        char contigs_fname[MAX_FILE_PATH];
        snprintf(contigs_fname, MAX_FILE_PATH, "%s", last_contigs_out);
        exec_stage(cfg.stages, IS_SCAFF, canonical_assembly_main, "canonical_assembly-contigs",
                   "-o %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                   "-n %d", cfg.cores_per_node,
                   "-f %s", contigs_fname,
                   "-F %s", ".fasta",
                   "-O %s", "canonical_contigs.fa",
                   "-d %B", !cfg.canonicalize,
                   NULL);
    }


    if (!cfg.max_ono_setid) {
      SDIE("Scaffolding rounds are disabled or not properly set.  Exiting prematurely!\n");
    }
    
    if (cfg.is_metagenome) {
        min_contig_len = cfg.scaff_mer_size;
    }

    serial_printf("\n%s%s" KNORM "\n", _use_col, HASH_BAR);

    if (cfg.cgraph_scaffolding) {
        serial_printf("%s# Scaffolding" KNORM "\n", _use_col);
        serial_printf("%s%s" KNORM "\n", _use_col, HASH_BAR);
        if (cfg.merge_reads == 2) {
            merge_reads(&cfg, all_inputs_fofn);
            // now set up the input files again
            UPC_LOGGED_BARRIER;
        } else if (cfg.merge_reads == 0 || !cfg.cached_reads) {
            for (int libi = 0; libi < cfg.num_libs; libi++) {
                lib_t *lib = cfg.libs + libi;
                if (!cfg.cached_reads) {
                    serial_printf("Loading %s to local checkpoints\n", lib->name);
                    loadfq_library(lib->name, BASE_DIR(cfg.cached_io, cfg.local_tmp_dir));
                }
            }
            cfg.cached_reads = 1;
        }

        UPC_LOGGED_BARRIER;
        int two_rounds = (cfg.scaff_mer_size != contig_mer_size_max);
        serial_printf("Performing cgraph round 1 of %d (k=%d)\n", two_rounds ? 2 : 1, contig_mer_size_max);
        run_cgraph_scaffolding(&cfg, contig_mer_size_max, contig_mer_size_max, tigs_out, !two_rounds);
        if (two_rounds) {
            serial_printf("Performing cgraph round 2 of %d (k=%d)\n", two_rounds ? 2 : 1, cfg.scaff_mer_size);
            sprintf(tigs_out, "cgraph-%d", contig_mer_size_max);
            run_cgraph_scaffolding(&cfg, cfg.scaff_mer_size, contig_mer_size_max, tigs_out, 1);
        }
        UPC_LOGGED_BARRIER;
    } else { // !cfg.cgraph_scaffolding
        // Each scaffolding loop is a round of scaffolding over all the ono_setid
        // so replicate the involved libraries for each scaffolding loop and generate higher ono_setids for these duplicates
        serial_printf("%s# Starting %d scaffolding loop%s" KNORM "\n", _use_col, cfg.num_scaff_loops,
                      cfg.num_scaff_loops == 1 ? "" : "s");
        serial_printf("%s%s" KNORM "\n", _use_col, HASH_BAR);
        run_ono_scaffolding(&cfg, min_contig_len, max_kmer_len, n_contigs, scaff_mer_size_max, dryrun, tigs_out, mer_depth_prefix);
    }
    
    serial_printf("Syncing all files to disk\n");
    cleanupCheckpoints(1);
    fflush(stdout);

    UPC_LOGGED_BARRIER;
    print_timings(&cfg);
    UPC_LOGGED_BARRIER;
    if (EXIT_SUCCESS == 0 && cfg.cached_io && (MYTHREAD % cfg.cores_per_node) == 0 && strcmp(BASE_DIR(cfg.cached_io, cfg.local_tmp_dir), cfg.local_tmp_dir) == 0) {
        upc_tick_t t = upc_ticks_now();
        serial_printf("Purging %s/per_rank intermediate files\n", cfg.local_tmp_dir);
        lld bytes = 0;
        int files = 0, dirs = 0;
        char p[MAX_FILE_PATH];
        strcpy(p, cfg.local_tmp_dir);
        strcat(p, "/per_rank");
        int purged = purgeDir(p, &bytes, &files, &dirs);
        if (purged) {
            LOGF("purged %d files from %s in %0.2f s (%0.3lf GB)\n",
                 purged, p, ((double)upc_ticks_to_ns(upc_ticks_now() - t) / 1000000000.0),
                 ((double)bytes) / ONE_GB);
            if (purged != files) {
                WARN("Could not purge %d files in %s!\n", files - purged, p);
            }
        }
    }
    if (EXIT_SUCCESS == 0 && !cfg.no_cleanup_after_success) {
        UPC_LOGGED_BARRIER;
        upc_tick_t t = upc_ticks_now();
        serial_printf("Cleaning up all intermediate files\n");
        char path[MAX_FILE_PATH] = "";
        get_rank_path(path, MYTHREAD);
        //printf("thread %d is cleaning %s\n", MYTHREAD, path);
        lld bytes = 0;
        int files = 0, dirs = 0;
        int purged = purgeDir(path, &bytes, &files, &dirs);
        LOGF("Purged %d files from %s\n", purged, path);
        if (!MYTHREAD) {
            purged = purgeDir("intermediates", &bytes, &files, &dirs);
            LOGF("Purged %d files from intermediates\n", purged);
        }
        UPC_LOGGED_BARRIER;
        if (!MYTHREAD) {
            purged = purgeDir("per_rank", &bytes, &files, &dirs);
            LOGF("Purged %d files from per_rank\n", purged);
            // clean up files in main directory
            rmFiles("*fofn*");
            rmFiles("*readlen.txt");
        }
        serial_printf("Cleaning took %0.2f s\n", ELAPSED_TIME(t));
    }
    for (int lib_i = 0; lib_i < cfg.num_libs; lib_i++) {
        if (cfg.libs[lib_i].files_glob) {
            free(cfg.libs[lib_i].files_glob);
        }
        if (cfg.libs[lib_i].name) {
            free(cfg.libs[lib_i].name);
        }
    }

    // Done with all cleanup now. calculate and report timings and memory leaks

    serial_printf("%s# Overall time for HipMer, version %s (%s) is %.2f s" KNORM "\n", _use_col,
                  HIPMER_VERSION, basename(argv[0]),
                  ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));


    double mem_leaked_used = get_used_mem_gb() - start_mem_used;
    LOGF("mem_leakded=%0.3f GB, used=%0.3f GB, used start=%0.3f GB\n", mem_leaked_used, get_used_mem_gb(), start_mem_used);
    double tot_mem_leaked_used = reduce_double(mem_leaked_used, UPC_ADD, SINGLE_DEST);

    double mem_leaked_free = get_free_mem_gb() - start_mem_free;
    LOGF("mem_leakded2=%0.3f GB, used=%0.3f GB, free start=%0.3f GB\n", mem_leaked_free, get_free_mem_gb(), start_mem_free);
    double tot_mem_leaked_free = reduce_double(mem_leaked_free, UPC_ADD, SINGLE_DEST);

    {
        char buf[256];
        report_memory_allocations(buf);
        serial_printf("%s# %s" KNORM "\n", _use_col, buf);
    }


    if (!MYTHREAD) {
        double end_mem_free = get_free_mem_gb();
        serial_printf("%s# Overall memory: full system delta free %.3f GB ( %0.3f GB total ), "
                      "application delta used %.3f GB (%0.3f GB total)" KNORM "\n", _use_col,
                      mem_leaked_free, tot_mem_leaked_free / MYSV.cores_per_node, mem_leaked_used, tot_mem_leaked_used / MYSV.cores_per_node);
        serial_printf("%s# Remaining on node: %.3f GB" KNORM "\n", _use_col, end_mem_free);
    }
    
    serial_printf("%s%s" KNORM "\n\n", _use_col, HASH_BAR);
    serial_printf("%s# Final assembly should be here: %s/results/final_assembly.fa" KNORM "\n\n",
                  _use_col, getcwd(cwd, MAX_FILE_PATH));

    int tot_num_warnings = reduce_int(_sv[MYTHREAD]._num_warnings, UPC_ADD, SINGLE_DEST);

    if (tot_num_warnings) {
        serial_printf("\n" KLRED "There %s %d warning%s. Please check stderr." KNORM "\n\n",
                      tot_num_warnings == 1 ? "was" : "were", tot_num_warnings,
                      tot_num_warnings == 1 ? "" : "s");
    }

    fini_diags();

    upc_barrier;
    double elapsed_time = upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0;
    serial_printf("# HipMer has completed successfully. Executed using version %s (%s) in %.2f s\n", 
                  HIPMER_VERSION, basename(argv[0]), 
                  elapsed_time);

    UPC_LOGGED_BARRIER;
    close_logger_cxx();
#ifdef DEBUG
    close_dbg_cxx();
#endif
    finalize_upcxx();
    UPC_LOGGED_BARRIER;
    CLOSE_MY_LOG;
    
    if (!MYTHREAD) {
      time_t end;
      time(&end);
      printf("# Success, exit %d (%.2f s teardown) at %s\n", EXIT_SUCCESS, (upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0) - elapsed_time, ctime(&end));
      fflush(stdout);
    }
    return EXIT_SUCCESS;
}
