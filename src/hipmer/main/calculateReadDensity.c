#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "upc_output.h"

#define REQUIRE_MAPPED_PAIRS

typedef struct align_info {
    int     contigs_matched;
    int64_t aligned_contig_ids[MAX_ALIGN];
    int     aligned_contig_lengths[MAX_ALIGN];
} align_info;

int short_splitAlignment(char *input_map, char *read_id, int64_t *subject, int *sLength)
{
    char *token;
    char *aux;

    (*sLength) = 0;
    token = strtok_r(input_map, "\t", &aux);
    if (strcmp(token, "MERALIGNER-F") == 0) {
        token = strtok_r(NULL, "\t", &aux);
        strcpy(read_id, token);
        return 0;
    }

    token = strtok_r(NULL, "\t", &aux);
    strcpy(read_id, token);
    token = strtok_r(NULL, "\t", &aux);
    token = strtok_r(NULL, "\t", &aux);
    token = strtok_r(NULL, "\t", &aux);
    token = strtok_r(NULL, "\t", &aux);
    (*subject) = atol(token + 6);
    token = strtok_r(NULL, "\t", &aux);
    token = strtok_r(NULL, "\t", &aux);
    token = strtok_r(NULL, "\t", &aux);
    (*sLength) = atoi(token);

    return 1;
}

int calculateReadDensity(int64_t totalContigs, const char *libname, const char *base_dir, const char *merAlignerOutput)
{
    char *outputMerAligner = malloc_chk(1024);
    char *firstAlignmentBuffer1 = malloc_chk(1024);
    char *alignmentBuffer1 = malloc_chk(1024);
    char *copyAlignmentBuffer1 = malloc_chk(1024);
    char *firstAlignmentBuffer2 = malloc_chk(1024);
    char *alignmentBuffer2 = malloc_chk(1024);
    char *copyAlignmentBuffer2 = malloc_chk(1024);
    char *readIdLane1 = malloc_chk(1024);
    char *newReadIdLane1 = malloc_chk(1024);
    char *readIdLane2 = malloc_chk(1024);
    char *newReadIdLane2 = malloc_chk(1024);
    GZIP_FILE laneFD1;
    GZIP_FILE laneFD2;
    char *resRead1, *resRead2;
    int validAlign1, validAlign2;
    align_info result1, result2;
    int64_t curContig, subject1, subject2;
    int curLength;

    /* Auxiliary data structures to find density of mapped reads per contig */
    int64_t *arrayOfMappedPairCounts = NULL;
    int64_t *arrayOfContigLengths = NULL;

    LOGF("calculateReadDensity(%lld, %s, %s, %s): %0.3f MB global memory %0.3f MB local\n", (lld)totalContigs, libname, base_dir, merAlignerOutput, 2 * 1.0 / ONE_MB * totalContigs * sizeof(int64_t), 2 * 1.0 / ONE_MB * totalContigs * sizeof(int64_t) / THREADS);
    arrayOfMappedPairCounts = (int64_t *)calloc_chk(totalContigs, sizeof(int64_t));
    arrayOfContigLengths = (int64_t *)calloc_chk(totalContigs, sizeof(int64_t));

    shared[1] int64_t * sharedArrayOfMappedPairCounts = NULL;
    UPC_ALL_ALLOC_CHK(sharedArrayOfMappedPairCounts, totalContigs, sizeof(int64_t));

    shared[1] int64_t * sharedArrayOfContigLengths = NULL;
    UPC_ALL_ALLOC_CHK(sharedArrayOfContigLengths, totalContigs, sizeof(int64_t));

    for (int z = MYTHREAD; z < totalContigs; z += THREADS) {
        sharedArrayOfMappedPairCounts[z] = 0;
        sharedArrayOfContigLengths[z] = 0;
    }

    if (libname == NULL) {
        SDIE("You must specify a -l libname!\n");
    }

    sprintf(outputMerAligner, "%s-%s_Read1" GZIP_EXT, libname, merAlignerOutput);
    laneFD1 = openCheckpoint(outputMerAligner, "r");

    sprintf(outputMerAligner, "%s-%s_Read2" GZIP_EXT, libname, merAlignerOutput);
    laneFD2 = openCheckpoint(outputMerAligner, "r");

    UPC_LOGGED_BARRIER;

    /***************************/
    /* Read merAligner outputs */
    /***************************/

    resRead1 = GZIP_GETS(firstAlignmentBuffer1, 1024, laneFD1);
    resRead2 = GZIP_GETS(firstAlignmentBuffer2, 1024, laneFD2);

    int moreAlignmentsExist = 1;

    while (moreAlignmentsExist) {
        if ((resRead1 == NULL) && (resRead2 == NULL)) {
            break;
        }

        /* Parse the first alignment of read (lane 1) */
        if (resRead1 != NULL) {
            validAlign1 = short_splitAlignment(firstAlignmentBuffer1, readIdLane1, &curContig, &curLength);
            result1.aligned_contig_ids[0] = curContig;
            result1.aligned_contig_lengths[0] = curLength;
            if (curLength) {
                arrayOfContigLengths[curContig] = curLength;
            }

            if (validAlign1 == 0) {
                result1.contigs_matched = 0;
            } else {
                result1.contigs_matched = 1;
            }
        }

        //  Read the the rest alignments of read (lane 1)
        if (resRead1 != NULL) {
            resRead1 = GZIP_GETS(alignmentBuffer1, 1024, laneFD1);
            memcpy(copyAlignmentBuffer1, alignmentBuffer1, 1024 * sizeof(char));
            if (resRead1 != NULL) {
                validAlign1 = short_splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &curContig, &curLength);
                result1.aligned_contig_ids[result1.contigs_matched] = curContig;
                result1.aligned_contig_lengths[result1.contigs_matched] = curLength;
                if (curLength) {
                    arrayOfContigLengths[curContig] = curLength;
                }

                while (strcmp(readIdLane1, newReadIdLane1) == 0) {
                    result1.contigs_matched++;
                    resRead1 = GZIP_GETS(alignmentBuffer1, 1024, laneFD1);
                    memcpy(copyAlignmentBuffer1, alignmentBuffer1, 1024 * sizeof(char));
                    if (resRead1 == NULL) {
                        break;
                    }
                    validAlign1 = short_splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &curContig, &curLength);
                    result1.aligned_contig_ids[result1.contigs_matched] = curContig;
                    result1.aligned_contig_lengths[result1.contigs_matched] = curLength;
                    if (curLength) {
                        arrayOfContigLengths[curContig] = curLength;
                    }
                }

                if (resRead1 != NULL) {
                    memcpy(firstAlignmentBuffer1, alignmentBuffer1, 1024 * sizeof(char));
                    strcpy(readIdLane1, newReadIdLane1);
                }
            }
        }

        /* Parse the first alignment of read (lane 2) */
        if (resRead2 != NULL) {
            validAlign2 = short_splitAlignment(firstAlignmentBuffer2, readIdLane2, &curContig, &curLength);
            result2.aligned_contig_ids[0] = curContig;
            result2.aligned_contig_lengths[0] = curLength;
            if (curLength) {
                arrayOfContigLengths[curContig] = curLength;
            }

            if (validAlign2 == 0) {
                result2.contigs_matched = 0;
            } else {
                result2.contigs_matched = 1;
            }
        }

        //  Read the the rest alignments of read (lane 2)
        if (resRead2 != NULL) {
            resRead2 = GZIP_GETS(alignmentBuffer2, 1024, laneFD2);
            memcpy(copyAlignmentBuffer2, alignmentBuffer2, 1024 * sizeof(char));
            if (resRead2 != NULL) {
                validAlign2 = short_splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &curContig, &curLength);
                result2.aligned_contig_ids[result2.contigs_matched] = curContig;
                result2.aligned_contig_lengths[result2.contigs_matched] = curLength;
                if (curLength) {
                    arrayOfContigLengths[curContig] = curLength;
                }

                while (strcmp(readIdLane2, newReadIdLane2) == 0) {
                    result2.contigs_matched++;
                    resRead2 = GZIP_GETS(alignmentBuffer2, 1024, laneFD2);
                    memcpy(copyAlignmentBuffer2, alignmentBuffer2, 1024 * sizeof(char));
                    if (resRead2 == NULL) {
                        break;
                    }
                    validAlign2 = short_splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &curContig, &curLength);
                    result2.aligned_contig_ids[result2.contigs_matched] = curContig;
                    result2.aligned_contig_lengths[result2.contigs_matched] = curLength;
                    if (curLength) {
                        arrayOfContigLengths[curContig] = curLength;
                    }
                }

                if (resRead2 != NULL) {
                    memcpy(firstAlignmentBuffer2, alignmentBuffer2, 1024 * sizeof(char));
                    strcpy(readIdLane2, newReadIdLane2);
                }
            }
        }

#ifdef REQUIRE_MAPPED_PAIRS
        /* This may be a bit too strict... Would be better to look if there is at least one common contig in the alignments */
        if ((result1.contigs_matched == 1) && (result2.contigs_matched == 1)) {
            subject1 = result1.aligned_contig_ids[0];
            subject2 = result2.aligned_contig_ids[0];
            if (subject1 == subject2) {
                arrayOfMappedPairCounts[subject1]++;
            }
        }
#endif

#ifndef REQUIRE_MAPPED_PAIRS
        int nContigs1 = result1.contigs_matched;
        int nContigs2 = result2.contigs_matched;
        int commonMapped = 0;
        int found = 0;
        int ii, jj;

        for (ii = 0; ii < nContigs1; ii++) {
            subject1 = result1.aligned_contig_ids[ii];
            arrayOfMappedPairCounts[subject1]++;
        }

        for (ii = 0; ii < nContigs2; ii++) {
            subject2 = result2.aligned_contig_ids[ii];
            arrayOfMappedPairCounts[subject2]++;
        }

        for (ii = 0; ii < nContigs1; ii++) {
            subject1 = result1.aligned_contig_ids[ii];
            for (jj = 0; jj < nContigs2; jj++) {
                subject2 = result2.aligned_contig_ids[jj];
                if (subject1 == subject2) {
                    arrayOfMappedPairCounts[subject1]--;
                }
            }
        }
#endif
    }

    closeCheckpoint(laneFD1);
    closeCheckpoint(laneFD2);

    /* Perform aggregation for the partial counts */
    for (int z = 0; z < totalContigs; z++) {
        if (arrayOfMappedPairCounts[z] != 0) {
            /* Perform a fetch and add */
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &sharedArrayOfMappedPairCounts[z], arrayOfMappedPairCounts[z]);
        }
        if (arrayOfContigLengths[z] != 0) {
            UPC_ATOMIC_CSWAP_I64_RELAXED(NULL, &sharedArrayOfContigLengths[z], 0, arrayOfContigLengths[z]);
        }
    }

    UPC_LOGGED_BARRIER;

    /* Write results for mapped read-pair density on a file */
    char nMappedFilename[1024];
    sprintf(nMappedFilename, "nMappedReadsPerContig");
    FILE *myCountFd = openCheckpoint0(nMappedFilename, "w+");
    for (int z = MYTHREAD; z < totalContigs; z += THREADS) {
        fprintf(myCountFd, "%d\t%lld\t%lld\n", z, (lld)sharedArrayOfMappedPairCounts[z], (lld)sharedArrayOfContigLengths[z]);
    }

    closeCheckpoint0(myCountFd);
    UPC_LOGGED_BARRIER;

    if (arrayOfMappedPairCounts != NULL) {
        free_chk(arrayOfMappedPairCounts);
    }
    if (sharedArrayOfMappedPairCounts != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfMappedPairCounts);
    }
    if (arrayOfContigLengths != NULL) {
        free_chk(arrayOfContigLengths);
    }
    if (sharedArrayOfContigLengths != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfContigLengths);
    }

    free_chk(outputMerAligner);
    free_chk(firstAlignmentBuffer1);
    free_chk(alignmentBuffer1);
    free_chk(copyAlignmentBuffer1);
    free_chk(firstAlignmentBuffer2);
    free_chk(alignmentBuffer2);
    free_chk(copyAlignmentBuffer2);
    free_chk(readIdLane1);
    free_chk(newReadIdLane1);
    free_chk(readIdLane2);
    free_chk(newReadIdLane2);

    return 0;
}
