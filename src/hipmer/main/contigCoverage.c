#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "../scaffolding/histogrammer/analyzerUtils.h"
#include "upc_output.h"
#include "utils.h"

#define MAX_LINE_SIZE 1000

int contigCoverage(int64_t totalContigs, char *libname, int filesPerPair, const char *base_dir, char *contig_file_name,
                   int k_length, char *merAlignerOutput)
{
    serial_printf("Computing contig coverage for %s, with a total of %lld contigs\n", libname, (lld) totalContigs);

    //int64_t *tmp_coverage = (int64_t*) malloc_chk(totalContigs*sizeof(int64_t));
    //int64_t *tmp_lengths = (int64_t*) malloc_chk(totalContigs*sizeof(int64_t));
    int64_t contigId;
    char outputMerAligner[MAX_FILE_PATH];
    char my_output_file_name[MAX_FILE_PATH];
    GZIP_FILE laneFD;
    char *lineBuffers = (char *)calloc_chk(MAX_LINE_SIZE * 2, 1);
    char *alignmentBuffer = lineBuffers;
    char *readIdLane = alignmentBuffer + MAX_LINE_SIZE;
    char *resRead;
    int qStart, qStop, qLength, sStart, sStop, sLength, strand, validAlign;
    int64_t subject;
    GZIP_FILE myOutputFile;
    int histogram_size = 2000;
    int64_t *local_histo = (int64_t *)calloc_chk(histogram_size, sizeof(int64_t));
    int bin_size = 2;


    //for (contigId=0; contigId<totalContigs; contigId++) {
    //  tmp_lengths[contigId] = 0;
    //  tmp_coverage[contigId] = 0;
    //}

    shared[1] int64_t * contig_coverage = NULL;
    shared[1] int64_t * contig_lengths = NULL;

    UPC_ALL_ALLOC_CHK(contig_coverage, totalContigs, sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(contig_lengths, totalContigs, sizeof(int64_t));

    for (contigId = MYTHREAD; contigId < totalContigs; contigId += THREADS) {
        contig_coverage[contigId] = 0;
        contig_lengths[contigId] = 0;
    }

    upc_barrier;

    /* Read the merAligner outputs and store coverage info to local arrays */
    int lane_to_read;
    int covered_bases;
    int64_t num_alns_found = 0;

    for (lane_to_read = 1; lane_to_read <= 2; lane_to_read++) {
        if (filesPerPair == 0 && lane_to_read == 2) {
            break;                                      // There is no pair
        }
        if (filesPerPair == 0) sprintf(outputMerAligner, "%s-%s" GZIP_EXT, libname, merAlignerOutput);
        else sprintf(outputMerAligner, "%s-%s_Read%d" GZIP_EXT, libname, merAlignerOutput, lane_to_read);
        laneFD = openCheckpoint(outputMerAligner, "r");
        serial_printf("Reading meraligner file: %s\n", outputMerAligner);
        while ((resRead = GZIP_GETS(alignmentBuffer, MAX_LINE_SIZE, laneFD)) != NULL) {
            validAlign = splitAlignment(alignmentBuffer, readIdLane, &qStart, &qStop, &qLength,
                                        &subject, &sStart, &sStop, &sLength, &strand);
            if (validAlign) {
                num_alns_found++;
                covered_bases = sStop - sStart + 1;
                if (subject >= totalContigs) {
                    DIE("subject %d is out of bounds for access, max %lld\n", subject, (lld) totalContigs);
                }
                UPC_ATOMIC_FADD_I64_RELAXED(NULL, &contig_coverage[subject], covered_bases);
                //tmp_coverage[subject] += covered_bases;
                if (sLength == 0) WARN("found zero length contig at id %d\n", subject);
                //if (!MYTHREAD) fprintf(stderr, "contig %lld, length %d, coverage %d\n", (lld) contigId, sLength, covered_bases);
                int64_t dummy = 0;
                UPC_ATOMIC_CSWAP_I64_RELAXED(&dummy, &contig_lengths[subject], 0, sLength);
                //if (tmp_lengths[subject] == 0) {
                //   tmp_lengths[subject] = sLength;
                //}
            }
        }
        closeCheckpoint(laneFD);
    }

    int64_t tot_alns_found = reduce_long(num_alns_found, UPC_ADD, SINGLE_DEST);
    serial_printf("Done reading meraligner files, found %lld alns\n", (lld) tot_alns_found);

    /* Propagate coverage info to global depth array */
    //for (contigId=0; contigId<totalContigs; contigId++) {
    //   if (tmp_coverage[contigId] != 0) UPC_ATOMIC_FADD_I64(NULL, &contig_coverage[contigId], tmp_coverage[contigId]);
    //   int64_t dummy;
    //   if (tmp_lengths[contigId] != 0) UPC_ATOMIC_CSWAP_I64(&dummy, &contig_lengths[contigId], 0, tmp_lengths[contigId]);
    //}

    upc_barrier;
    /* Write merDepth file */
    sprintf(my_output_file_name, "merDepth_%s-cc.txt" GZIP_EXT, contig_file_name);
    myOutputFile = openCheckpoint(my_output_file_name, "w");
    serial_printf("Writing depth file to %s\n", my_output_file_name);
    double depth;
    int nMers;
    int con_length;
    int con_coverage;

    int int_part;

    int64_t num_contigs_written = 0;
    for (contigId = MYTHREAD; contigId < totalContigs; contigId += THREADS) {
        con_length = contig_lengths[contigId];
        con_coverage = contig_coverage[contigId];
        nMers = con_length - k_length + 1;
        if (con_length <= 0) {
            if (con_length < 0) WARN("Contig%lld length < 0: %d\n", (lld)contigId, con_length);
            continue;
        }
        depth = (1.0 * con_coverage) / (1.0 * con_length);
        GZIP_PRINTF(myOutputFile, "Contig%lld\t%d\t%f\n", (lld)contigId, nMers, depth);

        int_part = (int)depth;
        int_part = int_part / bin_size;

        if (int_part < histogram_size) {
            local_histo[int_part]++;
        } else {
            local_histo[histogram_size - 1]++;
        }
        num_contigs_written++;
    }

    int64_t tot_contigs_written = reduce_long(num_contigs_written, UPC_ADD, SINGLE_DEST);
    serial_printf("Wrote %lld contig depths\n", (lld) tot_contigs_written);

    /* Now all reduce the local histograms */
    int64_t padded_size = ((histogram_size + THREADS - 1) / THREADS) * ((int64_t)THREADS);
    shared[1] int64_t * dist_histograms = NULL;
    UPC_ALL_ALLOC_CHK(dist_histograms, THREADS * padded_size, sizeof(int64_t));
    shared[1] int64_t * shared_result = NULL;
    UPC_ALL_ALLOC_CHK(shared_result, histogram_size, sizeof(int64_t));
    int j;

    for (j = 0; j < histogram_size; j++) {
        dist_histograms[MYTHREAD * padded_size + j] = local_histo[j];
    }

    upc_barrier;
    int64_t cur_sum = 0;
    int i;

    for (j = MYTHREAD; j < histogram_size; j += THREADS) {
        cur_sum = 0;
        for (i = 0; i < THREADS; i++) {
            cur_sum += dist_histograms[i * padded_size + j];
        }
        shared_result[j] = cur_sum;
    }

    upc_barrier;
    FILE *fd_histo;

    if (MYTHREAD == 0) {
        for (j = 0; j < histogram_size; j++) {
            local_histo[j] = shared_result[j];
        }

        char path[MAX_FILE_PATH];
        sprintf(path, "MAPPING_COVERAGE_histogram-%d.txt", k_length);
        fd_histo = fopen_rank_path(path, "w", -1);

        for (j = 0; j < histogram_size; j++) {
            fprintf(fd_histo, "%d\t%lld\n", j * bin_size, (long long int)local_histo[j]);
        }

        fclose_track(fd_histo);
    }

    closeCheckpoint(myOutputFile);
    upc_barrier;

    UPC_ALL_FREE_CHK(shared_result);
    UPC_ALL_FREE_CHK(dist_histograms);
    free_chk(lineBuffers);
    free_chk(local_histo);

    /* Free the allocated memory */

    //free_chk(tmp_coverage);
    //free_chk(tmp_lengths);
    if (contig_coverage != NULL) {
        UPC_ALL_FREE_CHK(contig_coverage);
    }
    if (contig_lengths != NULL) {
        UPC_ALL_FREE_CHK(contig_lengths);
    }

    return 0;
}
