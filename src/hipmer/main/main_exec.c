#include <stddef.h>
#include <stdio.h>
#include <time.h>

#include <upc.h>

extern int main_main(int argc, char **argv);

int main(int argc, char **argv)
{
    time_t start, end;
    time(&start);

    int retVal = 0;

    retVal = main_main(argc, argv);
#ifdef DEBUG
    time(&end);
    if (MYTHREAD == 0) {
      fprintf(stdout, "main_main returned: %d at %s", retVal, ctime(&end)); 
      fflush(stdout);
    }
#endif

    return retVal;
}
