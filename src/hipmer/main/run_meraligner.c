#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <upc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#include "run_meraligner.h"
#include "contigCoverage.h"
#include "exec_utils.h"
#include "main_modules.h"
#include "upc_common.h"
#include "common.h"
#include "utils.h"
#include "upc_output.h"

/*
 *
 * Protocol to implement:
 *
 * 1) Align reads to contigs
 * 2) Run coverage calculator to get more acculate depth calculations
 * 3) Run local assembly module
 *
 */
int iterative_contig_extension(cfg_t cfg, char *tigs_out, int align_k, int last_k, int n_iters,
                               char *merAlignerOutput, char *output_ext, const char *_use_col)
{
    int min_contig_len = align_k;
    int64_t n_contigs = get_num_from_file("", tigs_out);
    char mer_depth_prefix[1024] = "";
    int its;

    for (its = 0; its < n_iters; its++) {
        /* Align reads to contigs */
        for (int li = 0; li < cfg.num_libs; li++) {
            lib_t *lib = &cfg.libs[li];
            if (lib->for_contigging) {
                int is_merged = lib->files_per_pair == 1 && cfg.merge_reads == 1;
                run_meraligner_lib(&cfg, lib, align_k, tigs_out, min_contig_len, NOT_SCAFF, 0, 0, merAlignerOutput, 1, _use_col);
                run_histogrammer_lib(&cfg, lib, align_k, min_contig_len, "", 0, 0, NOT_SCAFF, 
                                     merAlignerOutput, output_ext, 0, _use_col);
            }
            /* Run coverage calculator to get more accurate depth calculations */
            int rerun_coverage = contigCoverage(n_contigs, lib->name, lib->files_per_pair, BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                                                tigs_out, last_k, merAlignerOutput);
        }

        sprintf(mer_depth_prefix, "merDepth_%s", tigs_out);


        /* Run local assembly */
        int max_readlen = 0;
        char localassm_libs[4096] = "";
        char list_insert_sizes[1024] = "", list_insert_sigmas[1024] = "", list_revcomps[1024] = "";
        char list_5p_wiggles[1024] = "", list_3p_wiggles[1024] = "";
        get_list_lib_params(&cfg, &max_readlen, localassm_libs, list_insert_sizes, list_insert_sigmas,
                            list_revcomps, list_5p_wiggles, list_3p_wiggles, 1);

        n_contigs = get_num_from_file("", tigs_out);
        exec_stage(cfg.stages, NOT_SCAFF, localassm_main, name_i("localassm", last_k),
                   "-M %d", cfg.la_mer_size_max,
                   "-Q %d", cfg.qual_offset,
                   "-c %s", tigs_out,
                   "-a %s", merAlignerOutput,
                   "-b %s", localassm_libs,
                   "-i %s", list_insert_sizes,
                   "-I %s", list_insert_sigmas,
                   "-m %d", last_k,
                   "-P",
                   "-B %s", BASE_DIR(cfg.cached_io, cfg.local_tmp_dir),
                   "-X %B", cfg.cached_reads,
                   "-r %s", list_revcomps,
                   "-F %s", list_3p_wiggles,
                   "-T %s", list_5p_wiggles,
                   "-l %d", max_readlen,
                   "-N %lld", (lld)n_contigs,
                   "-C %s", mer_depth_prefix,
                   "-z %d", cfg.la_mer_size_min,
                   "-t %f", cfg.la_depth_dist_thres,
                   "-v %f", cfg.la_nvote_diff_thres,
                   "-H %d", cfg.la_hi_qual_thres,
                   "-q %d", cfg.la_qual_thres,
                   "-d %f", cfg.la_min_viable_depth,
                   "-D %f", cfg.la_min_expected_depth,
                   NULL);

//      prepend_str(tigs_out, "walks");
    }

    prepend_str(tigs_out, "walks");

    return 1;
}

char *prepend_str(char *s, const char *s_pre)
{
    char *old_s = strdup(s);

    sprintf(s, "%s_%s", s_pre, old_s);
    free(old_s);
    return s;
}

void run_meraligner_lib(cfg_t *cfg, lib_t *lib, int kmer_len, char *tigs_out, int min_contig_len, int is_scaff, int use_cgraph, int allow_repetitive_seeds,
                        char *merAlignerOutput, int seed_space, const char *_use_col)
{
    int rlen = (is_scaff ? lib->read_len * 2 : lib->read_len);
    int cache_contig_len = 4032; // 4kb minus some overhead -- supports scaffolding readlen up to about 330 bases
    int min_cache_contig_len = 3 * 2 * (rlen + 20) - kmer_len;

    if (cache_contig_len < min_cache_contig_len) {
        cache_contig_len = min_cache_contig_len;
    }
    if (rlen == 0) {
        cache_contig_len = 0;
    }
    char libname_fofn[MAX_FILE_PATH];
    snprintf(libname_fofn, MAX_FILE_PATH, "%s.fofn", lib->name);
    char stage_name[MAX_FILE_PATH];
    if (is_scaff) {
        sprintf(stage_name, "merAligner-%s-%d-%s", lib->name, kmer_len, use_cgraph ? "cgraph" : "scaff");
    } else {
        sprintf(stage_name, "merAligner-%s-%d", lib->name, kmer_len);
    }
    // check to see if we already have results for this file at this value of k
    int must_run = 1;
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *other_lib = &cfg->libs[i];
        if (strcmp(other_lib->name, lib->name) != 0 && strcmp(other_lib->files_glob, lib->files_glob) == 0) {
            // check to see if the output exists and if it used the same k value
            if (link_to_prev_meraligner_output(cfg, lib, other_lib, merAlignerOutput, kmer_len, 1) &&
                (lib->files_per_pair == 0 || link_to_prev_meraligner_output(cfg, lib, other_lib, merAlignerOutput, kmer_len, 2))
               ) {
                serial_printf("%s# Skipping run of %s" KNORM "\n", _use_col, stage_name);
                char other_output[MAX_FILE_PATH];
                sprintf(other_output, "%s-%s", other_lib->name, merAlignerOutput);
                char my_output[MAX_FILE_PATH];
                sprintf(my_output, "%s-%s", lib->name, merAlignerOutput);
                serial_printf("%s# Found merAligner output for same input file/s (%s) and k (%d) at %s\n"
                              "#  -> creating link to %s instead of rerunning merAligner" KNORM "\n", _use_col,
                              lib->files_glob, kmer_len, other_output, my_output);
                must_run = 0;
                break;
            }
        }
    }
    UPC_LOGGED_BARRIER;
    if (must_run) {
        char avoidMaskFlag[3];
        memset(avoidMaskFlag, '\0', sizeof(avoidMaskFlag));
        if (cfg->high_heterozygosity) {
            strcpy(avoidMaskFlag, "-M"); //maskMode
        }
        char new_libname_fofn[MAX_FILE_PATH];
        strcpy(new_libname_fofn, libname_fofn);
        int lib_num = lib->num;
        int lib_files_per_pair = lib->files_per_pair;
        exec_stage(cfg->stages, is_scaff, merAligner_main, stage_name,
                   "-k %d", kmer_len, "-d %d", seed_space, "-N %d", cfg->cores_per_node, "-j %f", 1.15,
                   "-r %s", new_libname_fofn, "-l %s", lib->name, "-u %d", lib_num, "-P %d", lib_files_per_pair,
                   "-c %s", tigs_out, "-C %d", cfg->sw_cache_mb, "-K %d", cfg->kmer_cache_mb,
                   "-x %d", 200000, "-m %d", min_contig_len, "-e %d", cache_contig_len,
                   "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir), "-X %B", cfg->cached_reads,
                   "-L %d", (rlen == 0) ? -1 : rlen, "-o %s", merAlignerOutput, avoidMaskFlag,
                   "-z %B", use_cgraph,
                   "-R %B", !allow_repetitive_seeds,
                    NULL);
        if (!MYTHREAD) {
            // update the lib->read_len with the maximum found while actually reading the files
            char buf[MAX_FILE_PATH];
            sprintf(buf, "%s-readlen.txt", lib->name);
            get_rank_path(buf, -1);
            FILE * lens = fopen_chk(buf, "r");
            while (fgets(buf, MAX_FILE_PATH, lens)) {
               int len = atoi(buf);
               if (lib->read_len < len) lib->read_len = len;
            }
            fclose_track(lens);
        }
        lib->read_len = broadcast_long(lib->read_len, 0);
        serial_printf("Found maxium read for library %s to be %d\n", lib->name, lib->read_len);
    }
}

void run_histogrammer_lib(cfg_t *cfg, lib_t *lib, int kmer_len, int min_contig_len, const char *srf,
                          int64_t n_contigs, int64_t n_scaffs, int is_scaff, const char *merAlignerOutput, const char *output_ext, int require_estimate, const char *_use_col)
{
    if (lib->files_per_pair) {
        int min_threads = cfg->cores_per_node;
        if (cfg->nodes > 1) {
            min_threads *= 2;
        }
        int threads_to_use_per_node = min_threads / cfg->nodes;
        if (threads_to_use_per_node < 1) {
            threads_to_use_per_node = 1;
        }
        //threads_to_use_per_node = cfg->cores_per_node;
        int tot_threads_to_use = threads_to_use_per_node * cfg->nodes;
        int num_alns = 192000 / tot_threads_to_use;
        //num_alns = 192000;
        char stage_name[MAX_FILE_PATH];
        sprintf(stage_name, "merAlignerAnalyzer-%s-%d%s", lib->name, kmer_len, is_scaff ? "-scaff" : "");
        if (!srf[0]) {
            exec_stage(cfg->stages, is_scaff, merAlignerAnalyzer_main, stage_name,
                       "-l %s", lib->name,
                       "-m %d", kmer_len,
                       "-i %d", lib->ins_avg,
                       "-s %d", lib->std_dev,
                       "-e %B", lib->estimate_from_merged,
                       "-c %d", num_alns,
                       "-T %d", lib->three_p_wiggle,
                       "-F %d", lib->five_p_wiggle,
                       "-R %B", lib->rev_comp,
                       "-A %B", lib->innie,
                       "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                       "-N %d", cfg->cores_per_node,
                       "-p %d", threads_to_use_per_node,
                       "-a %s", merAlignerOutput,
                       "-o %s", output_ext,
                       NULL);
        } else {
            exec_stage(cfg->stages, is_scaff, merAlignerAnalyzer_main, stage_name,
                       "-l %s", lib->name,
                       "-m %d", kmer_len,
                       "-i %d", lib->ins_avg,
                       "-s %d", lib->std_dev,
                       "-e %B", lib->estimate_from_merged,
                       "-c %d", num_alns,
                       "-T %d", lib->three_p_wiggle,
                       "-F %d", lib->five_p_wiggle,
                       "-R %B", lib->rev_comp,
                       "-A %B", lib->innie,
                       "-B %s", BASE_DIR(cfg->cached_io, cfg->local_tmp_dir),
                       "-N %d", cfg->cores_per_node,
                       "-p %d", threads_to_use_per_node,
                       "-Z %s", srf,
                       "-C %lld", (lld)n_contigs,
                       "-G %lld", (lld)n_scaffs,
                       "-a %s", merAlignerOutput,
                       "-o %s", output_ext, NULL);
        }
        // now set the insert average and std dev according to the values calculated
        int std_dev = get_ins_val("std", lib->name, output_ext);
        int ins_avg = get_ins_val("insert", lib->name, output_ext);
        if (require_estimate && !ins_avg && !lib->ins_avg) {
            SDIE("Could not determine insert size for lib %s...  Please restart with --rebuild-config and include an estimate of the insert size distribution.\n", lib->name);
        }

        if (std_dev) lib->std_dev = std_dev;
        if (ins_avg) {
            lib->ins_avg = ins_avg;
            lib->estimate_from_merged = 0;
            serial_printf("%sUsing calculated insert size for library %s: %d +/- %d" KNORM "\n",
                          _use_col, lib->name, lib->ins_avg, lib->std_dev);
            char inslib[255];
            sprintf(inslib, "%s_insert", lib->name);
            ADD_DIAG("%d", inslib, lib->ins_avg);
            sprintf(inslib, "%s_std_dev", lib->name);
            ADD_DIAG("%d", inslib, lib->std_dev);
        }
    }
}

int link_to_prev_meraligner_output(cfg_t *cfg, lib_t *lib, lib_t *other_lib,
                                   char *merAlignerOutput, int kmer_len, int read_num)
{
    char other_output[MAX_FILE_PATH];
    char my_output[MAX_FILE_PATH];

    sprintf(other_output, "%s-%s_Read%d" GZIP_EXT, other_lib->name, merAlignerOutput, read_num);
    if (!doesCheckpointExist(other_output)) {
        LOGF("Skipping link_to_prev_meraligner as there is no checkpoint for %s.\n", other_output);
        return 0;
    }

    sprintf(my_output, "%s-%s_Read%d" GZIP_EXT, lib->name, merAlignerOutput, read_num);
    if (doesCheckpointExist(my_output)) {
        // if the file already exists, that means we could be reexecuting a previous run, so we just
        deleteCheckpoint(my_output);
    }
    if (!linkCheckpoints(other_output, my_output)) {
        WARN("Could not linkCheckpoints %s to %s\n", other_output, my_output);
        return 0;
    }

    if (!MYTHREAD) {
        // now also update the total alignments file
        struct stat st;
        sprintf(other_output, "%s-nTotalAlignments.txt", other_lib->name);
        get_rank_path(other_output, -1);
        if (stat(other_output, &st) == -1) {
            DIE("Could not stat %s for linking to\n", other_output);
        }
        sprintf(my_output, "%s-nTotalAlignments.txt", lib->name);
        get_rank_path(my_output, -1);
        // if the file already exists, that means we could be reexecuting a previous run, so we just
        // delete the file/link
        if (stat(my_output, &st) == 0) {
            unlink(my_output);
        }
        if (link(other_output, my_output) != 0) {
            DIE("Could not link previous merAligner output at %s to %s: %s\n",
                other_output, my_output, strerror(errno));
        }
    }
    return 1;
}

int get_ins_val(const char *ins_type, const char *libname, const char *output_ext)
{
    char buf[256];

    sprintf(buf, "%s-%s-%s.txt", libname, ins_type, output_ext);
    get_rank_path(buf, -1);
    int test = 0;
    if (!MYTHREAD) {
        test = broadcast_long(does_file_exist(buf), 0);
    } else {
        test = broadcast_long(0, 0);
    }
    return test ? get_long_from_file_and_broadcast(buf) : 0;
}
