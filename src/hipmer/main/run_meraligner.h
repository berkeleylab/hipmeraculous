#ifndef RUN_MERALIGNER_H_
#define RUN_MERALIGNER_H_

#include "config.h"
#include "common.h"

int iterative_contig_extension(cfg_t cfg, char *tigs_out, int align_k, int last_k, int n_iters, char *merAlignerOutput, char *output_ext, const char *_use_col);

char *prepend_str(char *s, const char *s_pre);

void run_meraligner_lib(cfg_t *cfg, lib_t *lib, int kmer_len, char *tigs_out, int min_contig_len, int is_scaff, int use_cgraph, int allow_repetitive_seeds,
                        char *merAlignerOutput, int seed_space, const char *_use_col);

void run_histogrammer_lib(cfg_t *cfg, lib_t *lib, int kmer_len, int min_contig_len, const char *srf, int64_t n_contigs, int64_t n_scaffs, int is_scaff, const char *merAlignerOutput, const char *output_ext, int require_estimate, const char *_use_col);

int link_to_prev_meraligner_output(cfg_t *cfg, lib_t *lib, lib_t *other_lib, char *merAlignerOutput, int kmer_len, int read_num);

int get_ins_val(const char *ins_type, const char *libname, const char *output_ext);
#endif
