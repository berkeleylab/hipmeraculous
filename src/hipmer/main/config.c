#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <libgen.h>
#include <ctype.h>
#include <glob.h>

#include <upc.h>
#include <upc_tick.h>

#include "optlist.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "config.h"
#include "exec_utils.h"
#include "Buffer.h"
#include "StaticVars.h"

#define MAX_CFG_LINE_LEN 4096

#define CFG_ERR(fmt, ...)                                        \
    do {                                                        \
        if (!MYTHREAD) {                                          \
            fprintf(stderr, KLRED "ERROR: " fmt KNORM, ## __VA_ARGS__); }    \
        fflush(stderr);                                         \
        EXIT_FUNC(100);                                         \
    } while (0)


static const char *_usage = "-f config_file -N cores_per_node -B (cached_io) -b (no_cached_reads) -S (save_intermediates) -l local_tmp_dir "
    "-x (no_save_intermediates) -c (no_cleanup_after_success) -I illumina_version -X sw_cache_mb -Y kmer_cache_mb -s stages -P firstTimeKmerDir -H (runHLL) "
    "-n (noHH) -C (canonicalizeOutput) -T (testCheckpointing) -z (cgraph_scaffolding) -q cgraph_quality -F (use_bloom_filter) -h (help)";

shared int _qual_offset = 0;
shared int _config_err = 0;

static int get_qual_offset(int illumina_version, lib_t *lib, const char *_use_col)
{
    if (!MYTHREAD) {
        _qual_offset = 33;
        if (!illumina_version) {
            char *libname = lib->files_glob;
            if (lib->files_per_pair == 2) {
                libname = strchr(lib->files_glob, ',');
                if (!libname) { // improper config for FilesPerPair == 2
                    _config_err |= 0x01;
                    SWARN("Detected an improper files field for %s\n", lib->name);
                    libname = lib->files_glob; /* use the original name so nothing downstream breaks as null pointer */
                } else {
                    libname++;            // chose the file after the ','
                }
            }
            double startEstimate = now();
            glob_t globbuf; memset(&globbuf, 0, sizeof(glob_t));
            if (glob(libname, GLOB_ERR, NULL, &globbuf) != 0) {
                DIE("Could not glob-expand '%s'!\n", libname);
            }
            if (globbuf.gl_pathc == 0) {
                DIE("Found no match to '%s'!\n", libname);
            }
            libname = globbuf.gl_pathv[0];
            serial_printf("%s# Estimating quality offset from %s as ", _use_col, libname);
            FILE *f = fopen_chk(libname, "r");
            globfree(&globbuf);
            Buffer buf = initBuffer(4096);
            int i = 0;
            while (fgetsBuffer(buf, 4096, f)) {
                if (i%4 == 3) { // the quality line
                    if (strpbrk(getStartBuffer(buf), "RSTUVWXYZ[\\]^_`abcdefghi")) {
                        _qual_offset = 64;
                        //serial_printf("\nDEBUG: got q-offset from line %s\n", getStartBuffer(buf));
                        break;
                    }
                    if (strpbrk(getStartBuffer(buf), "\"#$%&'()*,./0123456789:;<=>?")) {
                        _qual_offset = 33;
                        //serial_printf("\nDEBUG: got q-offset from line %s\n", getStartBuffer(buf));
                        break;
                    }
                }
                resetBuffer(buf);
                i++;
                if (i > 50000) {
                    SWARN("Could not determine the quality off set in 50000 lines all qualitys are ambiguous with both 33 and 64 encoding.  Consider setting the illumina_version option.  Guessing %d encoding\n", _qual_offset);
                    break;
                }
            }
            fclose_track(f);
            serial_printf("%d in %0.3f s" KNORM "\n", _qual_offset, now() - startEstimate);
        } else if (illumina_version == 13 || illumina_version == 15) {
            _qual_offset = 64;
        } else if (illumina_version == 18) {
            _qual_offset = 33;
        } else {
            _qual_offset = 0;
        }
    }
    serial_printf("Detected quality offset %d\n", _qual_offset);
    UPC_LOGGED_BARRIER;
    if (_config_err) {
        if ((_config_err & 0x01)) {
            CFG_ERR("Your config file is invalid, could not find a second file listed for library %s in the files field '%s' (no commma), when FilesPerPair==2... Aborting\n", lib->name, lib->files_glob);
        }
        CFG_ERR("An unknown error happened while finding the quality offset for library %s\n", lib->name);
        assert(0); /* we should never get here as CFG_ERR exits */
    }
    return _qual_offset;
}

static int get_kmer_cache_mb(int cores_per_node, const char *_use_col)
{
    int shared_heap_mb = get_shared_heap_mb();

    if (!shared_heap_mb) {
        SWARN("%s# Cannot determine UPC shared heap size and hence cannot determine KMER cache size" KNORM "\n",
              _use_col);
        return 0;
    }
    // set kmer cache as up to 20% of the shared memory of a node
    int cache_size = shared_heap_mb * cores_per_node * 0.2;
    int min_cache = 128;
    if (cache_size < min_cache) {
        cache_size = min_cache;
    }
    return cache_size;
}

static int get_sw_cache_mb(int cores_per_node, int kmer_cache_mb, const char *_use_col)
{
    int shared_heap_mb = get_shared_heap_mb();

    if (!shared_heap_mb) {
        SWARN("%s# Cannot determine UPC shared heap size and hence cannot determine SW cache size" KNORM "\n",
              _use_col);
        return 0;
    }
    // set the SW cache to up to 40% of the remaining shared memory
    int cache_size = (shared_heap_mb * cores_per_node - kmer_cache_mb) * 0.4;
    // make sure it's no more than 40% of the kmer cache
    int max_cache = kmer_cache_mb * 0.4;
    if (cache_size > max_cache) {
        cache_size = max_cache;
    }
    return cache_size;
}

static int get_bool(const char *val)
{
    if (strlen(val) >= 1) {
        if (val[0] == '0') {
            return 0;
        }
        if (val[0] == '1') {
            return 1;
        }
    }
    return -1;
}

static char *get_next_lib_tok(char **saveptr, int line)
{
    char *tok = strtok_r(NULL, " \t", saveptr);

    if (!tok) {
        CFG_ERR("Invalid or missing token at line %d\n", line);
    }
    return tok;
}

static int get_bool_lib(const char *key, char **saveptr, int line)
{
    char *val = get_next_lib_tok(saveptr, line);
    int b = get_bool(val);

    if (b == -1) {
        CFG_ERR("Error on line %d: %s must be either 1 or 0, not '%s'\n", line, key, val);
    }
    return b;
}

static int get_str_param(const char *key, char *token, char *val, char *s)
{
    if (strcmp(key, token) == 0) {
        strcpy(s, val);
        if (s[strlen(s) - 1] == '\n') {
            s[strlen(s) - 1] = 0;
        }
        return 1;
    }
    return 0;
}

static int get_int_param(const char *key, char *token, char *val, int *i)
{
    if (strcmp(key, token) == 0) {
        *i = atoi(val);
        return 1;
    }
    return 0;
}

static int get_double_param(const char *key, char *token, char *val, double *d)
{
    if (strcmp(key, token) == 0) {
        *d = atof(val);
        return 1;
    }
    return 0;
}

static int get_bool_param(const char *key, char *token, char *val, int *b)
{
    if (strcmp(key, token) == 0) {
        *b = get_bool(val);
        if (*b == -1) {
            CFG_ERR("Error: %s must be either 1 or 0, not '%s'\n", key, val);
        }
        return 1;
    }
    return 0;
}

static int get_int_array_param(const char *key, char *token, char *val, int *arr, int *num)
{
    if (strcmp(key, token) != 0) {
        return 0;
    }

    char *saveptr;
    char *tok = strtok_r(val, ",", &saveptr);
    *num = 0;
    while (tok) {
        arr[*num] = atoi(tok);
        (*num)++;
        tok = strtok_r(NULL, ",", &saveptr);
    }
    if (!(*num)) {
        return 0;
    }
    return 1;
}

static int skip_line(const char *line)
{
    while (*line) {
        if (!isspace(*line)) {
            if (*line == '#') {
                return 1;
            }
            return 0;
        }
        line++;
    }
    return 1;
}

static void read_config_file(cfg_t *cfg, const char *_use_col)
{
    FILE *f = fopen_chk(cfg->config_fname, "r");
    char buf[MAX_CFG_LINE_LEN];
    char *saveptr;
    int line = -1;

    cfg->max_ono_setid = 0;
    //    cfg->min_link_support = 2;
    cfg->localize_reads = -1; // -1 disabled, 0 sort / clumpify before/during ufx (not supported yet), >0 sort after this contig round
    cfg->merge_reads = 2; // only for cgraph
    while (fgets(buf, MAX_CFG_LINE_LEN, f)) {
        line++;
        if (skip_line(buf)) {
            continue;
        }
        char *key = strtok_r(buf, " \t", &saveptr);
        if (!key) {
            CFG_ERR("Error parsing config file at line %d: expected a key string\n", line);
        }
        if (strcmp(key, "lib_seq") == 0) {
            lib_t *lib = &cfg->libs[cfg->num_libs];
            lib->files_glob = strdup(get_next_lib_tok(&saveptr, line));
            lib->name = strdup(get_next_lib_tok(&saveptr, line));
            if (strlen(lib->name) >= MAX_USER_LIB_NAME_LEN - 3) {
                CFG_ERR("Invalid config file, library names must be strictly shorter than %d letters: '%s' is too long\n", MAX_USER_LIB_NAME_LEN, lib->name);
            }
            lib->ins_avg = atoi(get_next_lib_tok(&saveptr, line));
            lib->std_dev = atoi(get_next_lib_tok(&saveptr, line));
            lib->read_len = atoi(get_next_lib_tok(&saveptr, line));
            lib->innie = get_bool_lib("innie", &saveptr, line);
            lib->rev_comp = get_bool_lib("revcomp", &saveptr, line);
            lib->for_contigging = get_bool_lib("contigging", &saveptr, line);
            lib->ono_setid = atoi(get_next_lib_tok(&saveptr, line));
            lib->for_gapclosing = get_bool_lib("gapclosing", &saveptr, line);
            lib->five_p_wiggle = atoi(get_next_lib_tok(&saveptr, line));
            lib->three_p_wiggle = atoi(get_next_lib_tok(&saveptr, line));
            lib->files_per_pair = atoi(get_next_lib_tok(&saveptr, line));
            lib->for_splinting = get_bool_lib("splinting", &saveptr, line);

            if (lib->ono_setid > cfg->max_ono_setid) {
                cfg->max_ono_setid = lib->ono_setid;
            }

	    
            if (strchr(lib->files_glob, ',')) {
                if (lib->files_per_pair != 2) {
                    CFG_ERR("Invalid config file at line %d: %s has a comma in the filename field but %d"
                            " in FilesPerPair\n",
                            line, lib->files_glob, lib->files_per_pair);
                }
                if (!lib->read_len) {
                    CFG_ERR("Invalid fastqs for %s on line %d.  Currently variable length paired read file in"
                            " two separate files is NOT supported.  Please convert them to interleaved files"
                            " prior to executing HipMer.  You may want to use the "
                            "${HIPMER_INSTALL}/bin/interleave_fastq executable\n",
                            lib->files_glob, line);
                }
            }
            if (lib->files_per_pair == 2) {
                // two files one for each read of a pair
                if (lib->read_len == 0) {
                    CFG_ERR("You must specify a readlength, and all reads must be that exact length when files_per_pair == 2: %s\n", lib->name);
                }
            } else {
                // single or interleaved files
                // Ignore any user supplied read_len and assume it is variable length
                lib->read_len = 0;
            }
            if (!lib->five_p_wiggle) {
                lib->five_p_wiggle = 5;
                serial_printf("%s# In config line %d: Setting 5' wiggle room in %s to the default of 5" KNORM "\n",
                              _use_col, line, lib->name);
            }
            if (!lib->three_p_wiggle) {
                lib->three_p_wiggle = 5;
                serial_printf("%s# In config line %d: Setting 3' wiggle room in %s to the default of 5" KNORM "\n",
                              _use_col, line, lib->name);
            }
            lib->num = cfg->num_libs++;
        } else {
            char *val = strtok_r(NULL, " \t", &saveptr);
            if (!val) {
                CFG_ERR("Error parsing config file at line %d: expected a value\n", line);
            }
            if (get_int_param("min_contig_len", key, val, &cfg->min_contig_len)) {
                continue;
            }
            if (get_int_param("min_depth_cutoff", key, val, &cfg->min_depth_cutoff)) {
                continue;
            }
            if (get_int_param("extension_cutoff", key, val, &cfg->extension_cutoff)) {
                continue;
            }
            if (get_int_param("mer_size", key, val, &cfg->mer_size)) {
                continue;
            }
            if (get_int_array_param("mer_sizes", key, val, cfg->mer_sizes, &cfg->num_mer_sizes)) {
                continue;
            }
            if (get_int_param("scaff_mer_size", key, val, &cfg->scaff_mer_size)) {
                continue;
            }
            if (get_double_param("dynamic_min_depth", key, val, &cfg->dynamic_min_depth)) {
                continue;
            }
            if (get_double_param("alpha", key, val, &cfg->alpha)) {
                continue;
            }
            if (get_double_param("beta", key, val, &cfg->beta)) {
                continue;
            }
            if (get_double_param("tau", key, val, &cfg->tau)) {
                continue;
            }
            if (get_double_param("error_rate", key, val, &cfg->error_rate)) {
                continue;
            }
            if (get_int_param("bubble_min_depth_cutoff", key, val, &cfg->bubble_min_depth_cutoff)) {
                continue;
            }
            if (get_bool_param("ono_w_long_reads", key, val, &cfg->ono_w_long_reads)) {
                continue;
            }
            if (get_bool_param("is_metagenome", key, val, &cfg->is_metagenome)) {
                continue;
            }
            if (get_bool_param("is_diploid", key, val, &cfg->is_diploid)) {
                continue;
            }
            if (get_bool_param("high_heterozygosity", key, val, &cfg->high_heterozygosity)) {
                continue;
            }
            if (get_str_param("ono_pair_thresholds", key, val, cfg->ono_pair_thresholds)) {
                continue;
            }
            /* if (get_int_param("min_link_support", key, val, &cfg->min_link_support)) { */
            /*     continue; */
            /* } */
            if (get_str_param("hmm_model_file", key, val, cfg->hmm_model_file)) {
                continue;
            }
            if (get_double_param("gap_close_rpt_depth_ratio", key, val, &cfg->gap_close_rpt_depth_ratio)) {
                continue;
            }
            if (get_int_param("assm_scaff_len_cutoff", key, val, &cfg->assm_scaff_len_cutoff)) {
                continue;
            }
            if (get_int_param("num_scaff_loops", key, val, &cfg->num_scaff_loops)) {
                continue;
            }
            if (get_int_param("canonicalize_output", key, val, &cfg->canonicalize)) {
                continue;
            }
            // all the local assembly params
            if (get_int_param("skip_la", key, val, &cfg->skip_la)) {
                continue;
            }
            if (get_double_param("la_depth_dist_thres", key, val, &cfg->la_depth_dist_thres)) {
                continue;
            }
            if (get_double_param("la_nvote_diff_thres", key, val, &cfg->la_nvote_diff_thres)) {
                continue;
            }
            if (get_int_param("la_mer_size_min", key, val, &cfg->la_mer_size_min)) {
                continue;
            }
            if (get_int_param("la_mer_size_max", key, val, &cfg->la_mer_size_max)) {
                continue;
            }
            if (get_int_param("la_qual_thres", key, val, &cfg->la_qual_thres)) {
                continue;
            }
            if (get_int_param("la_hi_qual_thres", key, val, &cfg->la_hi_qual_thres)) {
                continue;
            }
            if (get_double_param("la_min_viable_depth", key, val, &cfg->la_min_viable_depth)) {
                continue;
            }
            if (get_double_param("la_min_expected_depth", key, val, &cfg->la_min_expected_depth)) {
                continue;
            }
            if (get_int_param("la_rating_thres", key, val, &cfg->la_rating_thres)) {
                continue;
            }
            if (get_int_param("illumina_version", key, val, &cfg->illumina_version)) {
                continue;
            }
            if (get_bool_param("save_intermediates", key, val, &cfg->save_intermediates)) {
                continue;
            }
            if (get_bool_param("save_mergraph", key, val, &cfg->save_mergraph)) {
                continue;
            }
            if (get_int_param("localize_reads", key, val, &cfg->localize_reads)) {
                continue;
            }
            if (get_int_param("min_gap_size", key, val, &cfg->min_gap_size)) {
                continue;
            }
            if (get_int_param("cgraph_scaffolding", key, val, &cfg->cgraph_scaffolding)) {
                continue;
            }
            if (get_int_param("merge_reads", key, val, &cfg->merge_reads)) {
                continue;
            }
            if (!MYTHREAD) {
                WARN("Ignoring unrecognized key in config file at line %d: %s\n", line + 1, key);
            }
        }
    }
    fclose_track(f);
}

static char *get_option_list(void)
{
    char *usage = strdup(_usage);
    // get option characters from usage
    char *options = malloc_chk(1000);
    int pos = 0;
    char *aux = NULL;
    char *token = strtok_r(usage, " ", &aux);

    while (token) {
        if (token[0] == '-') {
            options[pos++] = token[1];
        } else if (token[0] != '(') {
            options[pos++] = ':';
        }
        token = strtok_r(NULL, " ", &aux);
    }
    options[pos] = 0;
    return options;
}


static void clean_end(char *list)
{
    int len = strlen(list);

    if (list[len - 1] == ',') {
        list[len - 1] = 0;
    }
}

void add_list_elem(char *list, char *elem, int i, int n)
{
    strcat(list, elem);
    if (i < n - 1) {
        strcat(list, ",");
    }
}

void get_list_lib_params(cfg_t *cfg, int *max_readlen, char *libs, char *list_insert_sizes,
                         char *list_insert_sigmas, char *list_revcomps, char *list_5p_wiggle,
                         char *list_3p_wiggle, int forContigOrGapclosing1or2)
{
    char buf[256];

    *max_readlen = 0;
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        if (forContigOrGapclosing1or2 == 1 && !lib->for_contigging) {
            continue;
        }
        if (forContigOrGapclosing1or2 == 2 && !lib->for_gapclosing) {
            continue;
        }
        if (lib->read_len > *max_readlen) {
            *max_readlen = lib->read_len;
        }
        add_list_elem(libs, lib->name, i, cfg->num_libs);

        sprintf(buf, "%d", lib->ins_avg);
        add_list_elem(list_insert_sizes, buf, i, cfg->num_libs);
        sprintf(buf, "%d", lib->std_dev);
        add_list_elem(list_insert_sigmas, buf, i, cfg->num_libs);

        sprintf(buf, "%d", lib->rev_comp);
        add_list_elem(list_revcomps, buf, i, cfg->num_libs);
        sprintf(buf, "%d", lib->five_p_wiggle);
        add_list_elem(list_5p_wiggle, buf, i, cfg->num_libs);
        sprintf(buf, "%d", lib->three_p_wiggle);
        add_list_elem(list_3p_wiggle, buf, i, cfg->num_libs);
    }
    clean_end(libs);
    clean_end(list_insert_sizes);
    clean_end(list_insert_sigmas);
    clean_end(list_revcomps);
    clean_end(list_5p_wiggle);
    clean_end(list_3p_wiggle);
}

void get_all_lib_names(cfg_t *cfg, char *all_libs)
{
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        add_list_elem(all_libs, lib->name, i, cfg->num_libs);
    }
    clean_end(all_libs);
}

void get_contigging_lib_names(cfg_t *cfg, char *all_libs)
{
    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        if (lib->for_contigging) {
            add_list_elem(all_libs, lib->name, i, cfg->num_libs);
        }
    }
    clean_end(all_libs);
}

void get_noncontigging_lib_names(cfg_t *cfg, char *all_libs)
{

    for (int i = 0; i < cfg->num_libs; i++) {
        lib_t *lib = &cfg->libs[i];
        if (!lib->for_contigging) {
            add_list_elem(all_libs, lib->name, i, cfg->num_libs);
        }
    }
    clean_end(all_libs);
}

void get_config(int argc, char **argv, cfg_t *cfg, const char *_use_col)
{
    if (argc == 1) {
        CFG_ERR("Usage: %s %s\n", basename(argv[0]), _usage);
    }
    // defaults are all 0
    memset(cfg, 0, sizeof(cfg_t));

    // by default only loop once through scaffolding
    cfg->num_scaff_loops = 1;

    // defaults for progressiveRelativeDepth
    cfg->alpha = 0.1;
    cfg->beta = 0.2;
    cfg->tau = 2.0;

    // except defaults for localassm
    cfg->skip_la = 0;
    cfg->la_depth_dist_thres = 0.5;
    cfg->la_nvote_diff_thres = 1.5;
    cfg->la_mer_size_min = 13;
    cfg->la_mer_size_max = -1;
    cfg->la_qual_thres = 10;
    cfg->la_hi_qual_thres = 20;
    cfg->la_min_viable_depth = 0.2;
    cfg->la_min_expected_depth = 0.5;
    cfg->la_rating_thres = 2;
    cfg->pePath = NULL;
    cfg->kmer_cache_mb = -1;
    cfg->sw_cache_mb = -1;
    cfg->assm_scaff_len_cutoff = 200;
    cfg->extension_cutoff = 20;
    cfg->min_gap_size = 3;
    cfg->cgraph_scaffolding = 0;
    cfg->canonicalize = 0;
    cfg->cached_io = 0;
    strcpy(cfg->local_tmp_dir,"/dev/shm");
    cfg->cgraph_quality = NULL;

    // get command line params
    option_t *this_opt;
    char *myoptions = get_option_list();
    option_t *opt_list = GetOptList(argc, argv, myoptions);
    char fail[10000] = "";
    char buf[100];
    while (opt_list) {
        this_opt = opt_list;
        opt_list = opt_list->next;
        switch (this_opt->option) {
        case 'f': strcpy(cfg->config_fname, this_opt->argument); break;
        case 'N': cfg->cores_per_node = atoi(this_opt->argument); SET_CORES_PER_NODE(cfg->cores_per_node); break;
        case 'B': cfg->cached_io = 1; cfg->cached_reads = 1; break;
        case 'l': strcpy(cfg->local_tmp_dir, this_opt->argument); break;
        case 'b': cfg->cached_reads = 0; break;
        case 'S': cfg->save_intermediates = 1; break;
        case 'x': cfg->save_intermediates = 0; break;
        case 'c': cfg->no_cleanup_after_success = 1; break;
        case 'I': cfg->illumina_version = atoi(this_opt->argument); break;
        case 'X': cfg->sw_cache_mb = atoi(this_opt->argument); break;
        case 'Y': cfg->kmer_cache_mb = atoi(this_opt->argument); break;
        case 's': cfg->stages = strdup(this_opt->argument); break;
        case 'P': cfg->pePath = strdup(this_opt->argument); break;
        case 'H': cfg->runHLL = 1; break;
        case 'n': cfg->noHH = 1; break;
        case 'C': cfg->canonicalize = 1; break;
        case 'z': cfg->cgraph_scaffolding = 1; break;
        case 'T': MYSV.testCheckpointing = 1; break;
        case 'q': cfg->cgraph_quality = strdup(this_opt->argument); break;
        case 'F': cfg->use_bloom_filter = 1; break;
        case 'h': CFG_ERR("Usage: %s %s\n", basename(argv[0]), _usage);
        default:
            sprintf(buf, "\tInvalid Option: %c\n", this_opt->option);
            strcat(fail, buf);
            break;
        }
        if (cfg->cached_io) {
             MYSV.cached_io_path = strdup(cfg->local_tmp_dir); 
             MYSV.checkpoint_path = MYSV.checkpoint_path ? MYSV.checkpoint_path : strdup("intermediates");
        }
        if (cfg->save_intermediates) {
            MYSV.checkpoint_path = MYSV.checkpoint_path ? MYSV.checkpoint_path : strdup("intermediates");
        } else {
            if (MYSV.checkpoint_path) { free(MYSV.checkpoint_path); } MYSV.checkpoint_path = NULL; 
        }
    }

    // check command line params
    if (!cfg->config_fname[0]) {
        strcat(fail, "\tconfig_file required (-f)\n");
    }
    if (!cfg->cores_per_node) {
        // estimate cores per node from procfs
        cfg->cores_per_node = get_cores_per_node();
        if (cfg->cores_per_node <= 0) {
            strcat(fail, "Cannot determine cores per node from /proc/cpuinfo. Please specify with -N\n");
        } else {
            serial_printf("%s# Determined cores per node as %d" KNORM "\n", _use_col, cfg->cores_per_node);
        }
    }
    if (cfg->cores_per_node > THREADS) {
        serial_printf("%s# Cores per node (%d) is greater than threads (%d), restricting to thread count" KNORM "\n",
                      _use_col, cfg->cores_per_node, THREADS);
        cfg->cores_per_node = THREADS;
    }
    if (THREADS > cfg->cores_per_node) {
      if (cfg->kmer_cache_mb < 0) {
          cfg->kmer_cache_mb = get_kmer_cache_mb(cfg->cores_per_node, _use_col);
          if (cfg->kmer_cache_mb <= 0) {
              strcat(fail, "Cannot determine KMER cache size. Please specify with -Y\n");
          } else {
              serial_printf("%s# Calculated KMER cache size as %d MB" KNORM "\n", _use_col, cfg->kmer_cache_mb);
          }
      }
      if (cfg->sw_cache_mb < 0) {
          cfg->sw_cache_mb = get_sw_cache_mb(cfg->cores_per_node, cfg->kmer_cache_mb, _use_col);
          if (cfg->sw_cache_mb <= 0) {
              strcat(fail, "Cannot determine software cache size. Please specify with -X\n");
          } else {
              serial_printf("%s# Calculated software cache size as %d MB" KNORM "\n", _use_col, cfg->sw_cache_mb);
          }
      }
    } else {
      serial_printf("%s# Using no kmer or software cache size as this is a single node" KNORM "\n", _use_col);
      cfg->kmer_cache_mb = 0;
      cfg->sw_cache_mb = 0;
    }
    if (fail[0]) {
        CFG_ERR("Error in command line parameters:\n%s\nUsage: %s %s\n",
                fail, basename(argv[0]), _usage);
    }

    // some defaults
    strcpy(cfg->ono_pair_thresholds, "1,10");
    cfg->gap_close_rpt_depth_ratio = 0;

    // now read in config file
    read_config_file(cfg, _use_col);

    // check config file params
    if (!cfg->num_mer_sizes) {
        if (!cfg->mer_size) { // legacy parameter
            strcat(fail, "\tneed to set at least one entry in mer_sizes\n");
        } else {
            SWARN("the config option mer_size is deprecated, please use the mer_sizes parameter in the future\n");
            cfg->num_mer_sizes = 1;
            cfg->mer_sizes[0] = cfg->mer_size;
            cfg->mer_size = 0; // to detect if they are both set!
        }
    }
    if (cfg->num_mer_sizes > MAX_MER_SIZES) {
        strcat(fail, "\ttoo many mer sizes were specified, please reduce or recompile with a larger MAX_MER_SIZES configuration parameter\n");
    }
    if (cfg->mer_size) {
        strcat(fail, "\tthe config option mer_size is deprecated, and mer_sizes is also defined.  Please just specify mer_sizes\n");
    }
    for (int i = 0; i < cfg->num_mer_sizes - 1; i++) {
        if (cfg->mer_sizes[i] >= cfg->mer_sizes[i + 1]) {
            strcat(fail, "\tmer sizes need to be increasing\n");
            break;
        }
    }
    if (!cfg->num_libs) {
        strcat(fail, "\tat least one lib_seq required\n");
    }
    if (!cfg->min_depth_cutoff) {
        SWARN("min_depth_cutoff is not specified, the default will be 2 if one can not be determimed.  Please consider setting this important parameter considering your expected coverage of your data set.\n");
    }
    if (!cfg->dynamic_min_depth) {
        if (cfg->is_metagenome) {
            cfg->dynamic_min_depth = 0.9;
        } else {
            cfg->dynamic_min_depth = 1.0;
        }
        SWARN("dynamic_min_depth is not specified, choosing %0.1f %s. Please consider setting this important parameter considering your data set kmer-error rates.\n", cfg->dynamic_min_depth, cfg->dynamic_min_depth == 1.0 ? "(i.e disabled)" : "for a metagenome");
    }

    if (cfg->is_diploid && cfg->is_metagenome) {
        strcat(fail, "\tcannot have both is_diploid and is_metagenome parameters;  use one or the other!\n");
    }

    if (cfg->is_metagenome) {
        if (!cfg->alpha) {
            strcat(fail, "\talpha is required with is_metagenome\n");
        }
        if (!cfg->beta) {
            strcat(fail, "\tbeta is required with is_metagenome\n");
        }
        if (!cfg->tau) {
            strcat(fail, "\ttau is required with is_metagenome\n");
        }
        if (!cfg->error_rate) {
            if (cfg->dynamic_min_depth < 1.0) {
                cfg->error_rate = cfg->dynamic_min_depth;
            } else {
                cfg->error_rate = 0.9;
                SWARN("Setting error_rate to %f, Please consider setting this important metagenome parameter and/or dynamic_min_depth.\n", cfg->error_rate);
            }
        }
    }

    if (!cfg->skip_la) {
        if (cfg->num_mer_sizes && cfg->la_mer_size_max <= 0) {
            // using the default, so set it to 2+ the max
            cfg->la_mer_size_max = 2 + cfg->mer_sizes[cfg->num_mer_sizes - 1];
        }
        if (cfg->la_mer_size_min >= cfg->la_mer_size_max) {
            strcat(fail, "\tla_mer_size_max must be larger than la_mer_size_min\n");
        }
        if (cfg->num_mer_sizes && cfg->mer_sizes[cfg->num_mer_sizes - 1] > cfg->la_mer_size_max) {
            SWARN("largest mer_size must be larger than la_mer_size_max (it was %d, the max is %d)\n", cfg->la_mer_size_max, cfg->mer_sizes[cfg->num_mer_sizes - 1]);
            cfg->la_mer_size_max = 2 + cfg->mer_sizes[cfg->num_mer_sizes - 1];
        }
    }
    if (fail[0]) {
        CFG_ERR("Error in config file:\n%s\nUsage: %s %s\n", fail, basename(argv[0]), _usage);
    }
    if (!cfg->scaff_mer_size) {
        cfg->scaff_mer_size = cfg->mer_sizes[cfg->num_mer_sizes - 1];
    }
//    if (cfg->scaff_mer_size < cfg->mer_sizes[cfg->num_mer_sizes - 1]) {
//        SWARN("scaff_mer_size (%d) is less than the maximum mer_sizes in the contig stage (%d).  You can expect some additional errors in assembly.\n", cfg->scaff_mer_size, cfg->mer_sizes[cfg->num_mer_sizes - 1]);
//    }
    int max_mer_size = cfg->mer_sizes[cfg->num_mer_sizes - 1];
    if (cfg->scaff_mer_size > max_mer_size) {
        SWARN("scaff_mer_size (%d) is greater than the maximum mer_sizes in the contig stage (%d)\n", cfg->scaff_mer_size, max_mer_size);
//        SWARN("scaff_mer_size (%d) is greater than the maximum mer_sizes in the contig stage (%d). Adjusted to %d\n", 
//              cfg->scaff_mer_size, max_mer_size, max_mer_size);
//        cfg->scaff_mer_size = max_mer_size;
    }
    if (!cfg->min_contig_len) {
        cfg->min_contig_len = cfg->mer_sizes[0];
    }
    if (!cfg->is_diploid && !cfg->is_metagenome && cfg->min_contig_len < cfg->mer_sizes[0] * 2) {
        serial_printf("%s# Setting min_contig_len to %d because this is not a diploid or metagenome" KNORM "\n",
                      _use_col, cfg->mer_sizes[0] * 2);
        cfg->min_contig_len = cfg->mer_sizes[0] * 2;
    }

    cfg->nodes = THREADS / cfg->cores_per_node;

    cfg->qual_offset = get_qual_offset(cfg->illumina_version, &cfg->libs[0], _use_col);
    if (cfg->qual_offset <= 0) {
        CFG_ERR("Error in command line parameters:\n%s\nUsage: %s %s\n",
                "Could not determine quality offset. Please specify with -I\n", basename(argv[0]), _usage);
    }

    // print params
    if (!MYTHREAD) {
        serial_printf("\n%s%s" KNORM "\n", _use_col, HASH_BAR);
        serial_printf("%s# Parameters for %s:" KNORM "\n", _use_col, argv[0]);
        serial_printf("%s#" KNORM "\n", _use_col);
        serial_printf("%s#  config_file:               %s" KNORM "\n", _use_col, cfg->config_fname);
        serial_printf("%s#  Libs:                      ", _use_col);
        for (int i = 0; i < cfg->num_libs; i++) {
            serial_printf("%s ", cfg->libs[i].files_glob);
        }
        serial_printf("" KNORM "\n");
        serial_printf("%s#  cores_per_node:            %d" KNORM "\n", _use_col, cfg->cores_per_node);
        serial_printf("%s#  nodes:                     %d" KNORM "\n", _use_col, cfg->nodes);
        serial_printf("%s#  min_depth_cutoff:          %d" KNORM "\n", _use_col, cfg->min_depth_cutoff);
        serial_printf("%s#  extension_cutoff:          %d" KNORM "\n", _use_col, cfg->extension_cutoff);
        serial_printf("%s#  dynamic_min_depth:         %.3f" KNORM "\n", _use_col, cfg->dynamic_min_depth);
        serial_printf("%s#  illumina_version::         %d" KNORM "\n", _use_col, cfg->illumina_version);
        serial_printf("%s#  qual_offset:               %d" KNORM "\n", _use_col, cfg->qual_offset);
        serial_printf("%s#  mer_sizes:                 ", _use_col);
        for (int i = 0; i < cfg->num_mer_sizes; i++) {
            serial_printf("%d ", cfg->mer_sizes[i]);
        }
        serial_printf("" KNORM "\n");
        if (cfg->is_metagenome) {
            serial_printf("%s#  alpha:                     %.3f" KNORM "\n", _use_col, cfg->alpha);
            serial_printf("%s#  beta:                      %.3f" KNORM "\n", _use_col, cfg->beta);
            serial_printf("%s#  tau:                       %.3f" KNORM "\n", _use_col, cfg->tau);
            serial_printf("%s#  error_rate:                %.3f" KNORM "\n", _use_col, cfg->error_rate);
            serial_printf("%s#  skip_la:                   %d" KNORM "\n", _use_col, cfg->skip_la);
            serial_printf("%s#  la_depth_dist_thres:       %.2f" KNORM "\n", _use_col, cfg->la_depth_dist_thres);
            serial_printf("%s#  la_nvote_diff_thres:       %.2f" KNORM "\n", _use_col, cfg->la_nvote_diff_thres);
            serial_printf("%s#  la_mer_size_min:           %d" KNORM "\n", _use_col, cfg->la_mer_size_min);
            serial_printf("%s#  la_mer_size_max:           %d" KNORM "\n", _use_col, cfg->la_mer_size_max);
            serial_printf("%s#  la_qual_thres:             %d" KNORM "\n", _use_col, cfg->la_qual_thres);
            serial_printf("%s#  la_hi_qual_thres:          %d" KNORM "\n", _use_col, cfg->la_hi_qual_thres);
            serial_printf("%s#  la_min_viable_depth:       %.2f" KNORM "\n", _use_col, cfg->la_min_viable_depth);
            serial_printf("%s#  la_min_expected_depth:     %.2f" KNORM "\n", _use_col, cfg->la_min_expected_depth);
            serial_printf("%s#  la_rating_thres:           %d" KNORM "\n", _use_col, cfg->la_rating_thres);
        } else {
            serial_printf("%s#  min_contig_len:            %d" KNORM "\n", _use_col, cfg->min_contig_len);
        }
        serial_printf("%s#  scaff_mer_size:            %d" KNORM "\n", _use_col, cfg->scaff_mer_size);

        if (cfg->is_metagenome || cfg->is_diploid) {
            serial_printf("%s#  bubble_min_depth_cutoff:   %d" KNORM "\n", _use_col, cfg->bubble_min_depth_cutoff);
        }
        serial_printf("%s#  is_diploid:                %s" KNORM "\n", _use_col, cfg->is_diploid ? "true" : "false");
        serial_printf("%s#  high_heterozygosity:       %s" KNORM "\n", _use_col, cfg->high_heterozygosity ? "true" : "false");
        serial_printf("%s#  is_metagenome:             %s" KNORM "\n", _use_col, cfg->is_metagenome ? "true" : "false");
        serial_printf("%s#  kmer_cache_mb:             %d" KNORM "\n", _use_col, cfg->kmer_cache_mb);
        serial_printf("%s#  sw_cache_mb:               %d" KNORM "\n", _use_col, cfg->sw_cache_mb);
        serial_printf("%s#  ono_pair_thresholds:       %s" KNORM "\n", _use_col, cfg->ono_pair_thresholds);
        serial_printf("%s#  ono_w_long_reads:          %s" KNORM "\n", _use_col, cfg->ono_w_long_reads ?  "true" : "false");	
	//        serial_printf("%s#  min_link_support           %d" KNORM "\n", _use_col, cfg->min_link_support);
        serial_printf("%s#  gap_close_rpt_depth_ratio: %.3f" KNORM "\n", _use_col, cfg->gap_close_rpt_depth_ratio);
        serial_printf("%s#  use cached IO:             %s" KNORM "\n", _use_col, cfg->cached_io ? "true" : "false");
        serial_printf("%s#  use cached reads:          %s" KNORM "\n", _use_col, cfg->cached_reads ? "true" : "false");
        serial_printf("%s#  save_intermediates:        %s" KNORM "\n", _use_col, cfg->save_intermediates ? "true" : "false");
        serial_printf("%s#  no_cleanup_on_success:     %s" KNORM "\n", _use_col, cfg->no_cleanup_after_success ? "true" : "false");
        serial_printf("%s#  checkpoint_path:           %s" KNORM "\n", _use_col, MYSV.checkpoint_path ? MYSV.checkpoint_path : "");
        serial_printf("%s#  save_mergraph:             %s" KNORM "\n", _use_col, cfg->save_mergraph ? "true" : "false");
        serial_printf("%s#  assm_scaff_len_cutoff:     %d" KNORM "\n", _use_col, cfg->assm_scaff_len_cutoff);
        serial_printf("%s#  num_scaff_loops:           %d" KNORM "\n", _use_col, cfg->num_scaff_loops);
        serial_printf("%s#  canonicalize_output:       %s" KNORM "\n", _use_col, cfg->canonicalize ? "true" : "false");
        serial_printf("%s#  run_HLL:                   %s" KNORM "\n", _use_col, cfg->runHLL ? "true" : "false");
        serial_printf("%s#  no_HH:                     %s" KNORM "\n", _use_col, cfg->noHH ? "true" : "false");
        serial_printf("%s#  hmm_model_file:            %s" KNORM "\n", _use_col, cfg->hmm_model_file[0] ? cfg->hmm_model_file : "(none)");
        serial_printf("%s#  localize_reads:            %s" KNORM "\n", _use_col, cfg->localize_reads ? "true" : "false");
        serial_printf("%s#  merge_reads:               %d" KNORM "\n", _use_col, cfg->merge_reads);
        serial_printf("%s#  min_gap_size:              %d" KNORM "\n", _use_col, cfg->min_gap_size);
        if (cfg->cgraph_scaffolding) {
            serial_printf("%s#  cgraph scaffolding:        true" KNORM "\n", _use_col);
            serial_printf("%s#  cgraph quality:            %s" KNORM "\n", _use_col, (!cfg->cgraph_quality ? "normal" : cfg->cgraph_quality));
        }
        serial_printf("%s#  use bloom filter:          %s" KNORM "\n", _use_col, cfg->use_bloom_filter ? "true" : "false");
        serial_printf("%s#  min_gap_size:              %d" KNORM "\n", _use_col, cfg->min_gap_size);
        if (cfg->pePath) {
            serial_printf("%s#  first_time_kmer_dir:       %s" KNORM "\n", _use_col, cfg->pePath);
        }
        if (cfg->stages) {
            serial_printf("%s#  running stages:            %s" KNORM "\n", _use_col, cfg->stages);
        }
        serial_printf("%s%s" KNORM "\n", _use_col, HASH_BAR);
        if (cfg->hmm_model_file[0] && get_file_size(cfg->hmm_model_file) == 0) {
            DIE("Could not find the requested hmm model file: %s\n", cfg->hmm_model_file);
        }
    }
    free_chk(myoptions);
    UPC_LOGGED_BARRIER;
}

void print_lib(lib_t *lib)
{
#ifdef CONFIG_USE_COLORS
    printf(KLGREEN);
#endif
    printf("LLLLib %s\n", lib->name);
    printf("LLL\tfiles          %s\n", lib->files_glob);
    printf("LLL\tins_avg        %d\n", lib->ins_avg);
    printf("LLL\tstd_dev        %d\n", lib->std_dev);
    printf("LLL\tread_len       %d\n", lib->read_len);
    printf("LLL\tinnie          %d\n", lib->innie);
    printf("LLL\trev_comp       %d\n", lib->rev_comp);
    printf("LLL\tfive_p_wiggle  %d\n", lib->five_p_wiggle);
    printf("LLL\tthree_p_wiggle %d\n", lib->three_p_wiggle);
    printf("LLL\tfiles_per_pair %d\n", lib->files_per_pair);
    printf("LLL\tfor_contigging %d\n", lib->for_contigging);
    printf("LLL\tfor_splinting  %d\n", lib->for_splinting);
    printf("LLL\tfor_gapclosing %d\n", lib->for_gapclosing);
    printf("LLL\tono_setid      %d\n", lib->ono_setid);
#ifdef CONFIG_USE_COLORS
    printf(KNORM);
#endif
}
