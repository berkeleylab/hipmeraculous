#ifndef CONTIG_COVERAGE_H_
#define CONTIG_COVERAGE_H_

#include <stdint.h>

int contigCoverage(int64_t totalContigs, char *libname, int filesPerPair, const char *base_dir, char *contig_file_name, int k_length, char *merAlignerOutput);

#endif
