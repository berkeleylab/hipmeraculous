#ifndef _PACK_H_
#define _PACK_H_

#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <algorithm>
#include <cstring>
#include <numeric>
#include <vector>
#include <sstream>
#include <limits>
#include <array>
#include <map>
#include <tuple>
#include <locale>

#include "common_base.h"
#include "Kmer.hpp"
#include "Friends.h"
#include "FriendsMPI.h"
#include "log.h"

using namespace std;

typedef int8_t QualType;
typedef char SeqType;

typedef array<QualType, 2> TwoQual;
typedef array<SeqType, 2> TwoSeq;
typedef vector< TwoQual > TwoQualVector;
typedef vector< TwoSeq > TwoSeqVector;

extern int nprocs;
extern int myrank;

extern QualType phred_encoding;
extern QualType ignored_ext;
extern QualType extensionCutoff;

#ifdef HEAVYHITTERS
#ifndef MAXHITTERS
#define MAXHITTERS 200000
#endif
extern SimpleCount<Kmer, KmerHash> *heavyhitters;
extern UFX2ReduceObj *Frequents;
#endif

// The bloom filter pass; extensions are ignored
inline size_t FinishPackPass1(vector< vector<Kmer> > & outgoing, Kmer & kmerreal)
{
    uint64_t myhash = kmerreal.hash();  // whichever one is the representative
    double range = static_cast<double>(myhash) * static_cast<double>(nprocs);
    size_t owner = range / static_cast<double>(numeric_limits<uint64_t>::max());

    DBG2("kmer=%s myhash=%llx range=%f owner=%lld twin=%s\n", kmerreal.toString().c_str(), (lld)myhash, range, (lld)owner, kmerreal.twin().toString().c_str());

#ifdef HEAVYHITTERS
    if (heavyhitters && heavyhitters->IsMember(kmerreal)) {
        assert(heavyhitters->FindIndex(kmerreal) < heavyhitters->maxsize);
        // no-op here
        // cout << kmerreal.toString() << " is high freq" << endl;
    } else {
        assert(!heavyhitters || heavyhitters->FindIndex(kmerreal) >= heavyhitters->maxsize);
#endif
    outgoing[owner].push_back(kmerreal);
#ifdef HEAVYHITTERS
}
#endif
    return outgoing[owner].size();
}

inline void IncrementBasedOnQual(array<int, 4> & mergraphentry, QualType qual, SeqType seq)
{
    assert(qual >= phred_encoding);
    assert((int)seq == toupper(seq));
    if (qual >= phred_encoding + extensionCutoff) {
        switch (seq) {
        case 'A':
            mergraphentry[0]++;
            break;
        case 'C':
            mergraphentry[1]++;
            break;
        case 'G':
            mergraphentry[2]++;
            break;
        case 'T':
            mergraphentry[3]++;
            break;
        default:
            WARN("Invalid base in IncrementBasedOnQual: %c qual %d\n", seq, qual);
            break;
        }
    }
}

// The hash table pass; extensions are important
inline size_t FinishPackPass2(vector< vector<Kmer> > & outgoing, vector< TwoQualVector > & extquals,
                              vector< TwoSeqVector > & extseqs, Kmer & kmerreal, TwoQual & extqual, TwoSeq & extseq)
{
    assert(kmerreal == kmerreal.rep());
    uint64_t myhash = kmerreal.hash();  // whichever one is the representative
    double range = static_cast<double>(myhash) * static_cast<double>(nprocs);
    size_t owner = range / static_cast<double>(numeric_limits<uint64_t>::max());

    size_t location = 0, maxsize = 0;
#ifdef HEAVYHITTERS
    if (heavyhitters) {
        maxsize = heavyhitters->maxsize;
        location = heavyhitters->FindIndex(kmerreal);    // maxsie if not found
    }
#endif
    if (location < maxsize) {
#ifdef HEAVYHITTERS
        assert(maxsize > 0);
        assert(heavyhitters);
        assert(heavyhitters->IsMember(kmerreal));
        bool wasCounted = false;
        // Same logic as in KmerInfo only count if both quality values are >= 0
        if (extqual[0] >= 0 && extqual[1] >= 0) {
            Frequents[location].count += 1; // add one more count
            wasCounted = true;
        } else {
            // make quals positive
            assert(extqual[0] <= 0);
            assert(extqual[1] <= 0);
            extqual[0] = 0 - extqual[0];
            extqual[1] = 0 - extqual[1];
        }
        assert(extqual[0] >= phred_encoding);
        assert(extqual[1] >= phred_encoding);
        IncrementBasedOnQual(Frequents[location].ACGTleft, extqual[0], extseq[0]);
        IncrementBasedOnQual(Frequents[location].ACGTrigh, extqual[1], extseq[1]);
        //fprintf(stderr, "HeavyHiter: %s %d: [ %c %d, %c %d ]\n", kmerreal.toString().c_str(), wasCounted, extseq[0], extqual[0] - phred_encoding, extseq[1], extqual[1] - phred_encoding);
#endif
    } else {
        // Count here
#ifdef HEAVYHITTERS
#ifdef DEBUG
        if (heavyhitters && heavyhitters->IsMember(kmerreal)) {
            WARN("About to send a heavy hitter kmer! %s\n", kmerreal.toString().c_str());
        }
#endif
        assert(!heavyhitters || !heavyhitters->IsMember(kmerreal));
#endif
        //cout << myrank << ": " << kmerreal.toString() << " is NOT a heavy hitter " << endl;
        outgoing[owner].push_back(kmerreal);
        extquals[owner].push_back(extqual);
        extseqs[owner].push_back(extseq);
    }
    return outgoing[owner].size();
}

size_t PackEndsKmer(string & seq, string & qual, int j, Kmer &kmerreal, vector< vector<Kmer> > & outgoing, vector<TwoQualVector> & extquals,
                    vector<TwoSeqVector> & extseqs, int pass, int lastCountedBase, int kmer_length)
{
    bool isCounted = lastCountedBase >= j + kmer_length;
    size_t procSendCount;

    assert(seq.size() >= j + kmer_length);
    assert(seq.substr(j, kmer_length).find('N') == std::string::npos);
    assert(kmerreal == Kmer(seq.c_str() + j));
    if (pass == 1) {
        if (!isCounted) {
            return 0;
        }
        kmerreal = kmerreal.rep();
        procSendCount = FinishPackPass1(outgoing, kmerreal);
    } else if (pass == 2) { // otherwise we don't care about the extensions
        bool isLeft = j > 0;
        bool isRight = j < seq.length() - kmer_length;
        TwoSeq extseq = { isLeft ? seq[j - 1] : '-', isRight ? seq[j + kmer_length] : '-' };
        TwoQual extqual = { (int8_t)(isLeft ? (int8_t)qual[j - 1] : (int8_t)ignored_ext), (int8_t)(isRight ? (int8_t)qual[j + kmer_length] : (int8_t)ignored_ext) };
        if (!isACGT(extseq[0])) {
            extseq[0] = '-'; extqual[0] = ignored_ext;
        }
        if (!isACGT(extseq[1])) {
            extseq[1] = '-'; extqual[1] = ignored_ext;
        }
        if (!isCounted) {
            // Do not count this kmer, but extensions may be counted...
            extqual[0] = 0 - extqual[0];
            extqual[1] = 0 - extqual[1];
        }
        Kmer kmertwin = kmerreal.twin();
        if (kmertwin < kmerreal) { // the real k-mer is not lexicographically smaller
            kmerreal = kmertwin;
            RevCompSwitch(extseq[0]);
            RevCompSwitch(extseq[1]);
            std::swap(extseq[0], extseq[1]);    // swap the sequence ends
            std::swap(extqual[0], extqual[1]);  // swap the quality ends
        }
        procSendCount = FinishPackPass2(outgoing, extquals, extseqs, kmerreal, extqual, extseq);
    }
    return procSendCount;
}

size_t PackEnds(string & seq, string & qual, int j, vector< vector<Kmer> > & outgoing, vector<TwoQualVector> & extquals,
                vector<TwoSeqVector> & extseqs, int pass, int lastCountedBase, int kmer_length)
{
    bool isCounted = lastCountedBase >= j + kmer_length;

    if (pass == 1 && !isCounted) {
        return 0;
    }

    string kmerextstr;
    try {
        assert(seq.size() >= j + kmer_length);
        kmerextstr = seq.substr(j, kmer_length);
    } catch(std::exception const & exc) {
        std::cerr << "Exception caught in file " << __FILE__ << " at " << __LINE__ << ": " << exc.what() << "\n";
        std::cerr << "j = " << j << ", kmer_length = " << kmer_length << ", len seq = " << seq.length() << "\n";
        std::cerr << "seq = " << seq << "\n";
        common_exit(1);
    }
    for (auto & c : kmerextstr) {
        c = toupper(c);                         // convert all to uppercase
    }
    size_t found = kmerextstr.find('N');
    if (found != std::string::npos) {
        return 0;                           // if there is an 'N', toss it
    }
    Kmer kmerreal(kmerextstr.c_str());
    return PackEndsKmer(seq, qual, j, kmerreal, outgoing, extquals, extseqs, pass, lastCountedBase, kmer_length);
}

#endif
