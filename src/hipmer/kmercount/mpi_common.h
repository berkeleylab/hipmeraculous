#ifndef _MPI_COMMON_H
#define _MPI_COMMON_H

#include <stdio.h>
#include <unistd.h>

#include <mpi.h>

#include "version.h"
#include "defines.h"

#define mpi_common_exit(x, file, line) do { \
    if (x) { \
        fprintf(stderr, "Th%d [%s:%d], calling MPI_Abort(%d)\n", get_rank(), file, line, x); \
    } \
    fflush(stdout); \
    fflush(stderr); \
    MPI_Abort(MPI_COMM_WORLD, x); \
    _exit(x); \
} while(0)

// override EXIT_FUNC
#undef EXIT_FUNC
#define EXIT_FUNC(x) mpi_common_exit(x, __FILENAME__, __LINE__)

#include "StaticVars.h"
#include "common_base.h"
#include "common.h"

#if defined (__cplusplus)
extern "C" {
#endif

void print_args(option_t *optList, const char *stage);

#if defined (__cplusplus)
}
#endif

#endif
