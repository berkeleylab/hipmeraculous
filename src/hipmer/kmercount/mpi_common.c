#include <stdio.h>
#include <unistd.h>

#include <mpi.h>

#include "mpi_common.h"

static int *__get_rank()
{
    static int _ = -1;

    return &_;
}

static int *__get_num_ranks()
{
    static int _ = 0;

    return &_;
}

void _set_rank_and_size(int myRank, int ourSize, const char *file, int line)
{
    int rank = *__get_rank();
    int num_ranks = *__get_num_ranks();
    int firstRun = rank < 0;

    if (firstRun || myRank < 0 || ourSize <= 0) {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);
        if (myRank < 0 || ourSize <= 0) {
            myRank = rank;
            ourSize = num_ranks;
        }
    }
    if (myRank != rank || ourSize != num_ranks) {
        fprintf(stderr, "ERROR: set_rank_and_size called with incorrect values(%d,%d) vs (%d, %d)\n", myRank, ourSize, rank, num_ranks);
        _common_exit(1, file, line);
    }
    *__get_rank() = rank;
    *__get_num_ranks() = num_ranks;
}
int _get_rank(const char * file, int line)
{
    int rank = *__get_rank();

    if (rank < 0) {
        _set_rank_and_size(-1, -1, file, line);
        rank = *__get_rank();
    }
    return rank;
}
int _get_num_ranks(const char * file, int line)
{
    int num_ranks = *__get_num_ranks();

    if (num_ranks <= 0) {
        _set_rank_and_size(-1, -1, file, line);
        num_ranks = *__get_num_ranks();
    }
    return num_ranks;
}

void _common_exit(int x, const char * file, int line)
{
   mpi_common_exit(x,file,line); // macro defined in mpi_common.h
}

void print_args(option_t *optList, const char *stage)
{
    if (!get_rank()) {
        option_t *olist = optList;
        option_t *opt = NULL;
        //serial_printf(KLWHITE "STAGE %s ", stage);
        serial_printf("STAGE %s ", stage);
        while (olist) {
            opt = olist;
            olist = olist->next;
            serial_printf("-%c ", opt->option);
            if (opt->argument) {
                serial_printf("%s ", opt->argument);
            }
        }
        //serial_printf(KNORM "\n");
        serial_printf("\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
}

#include "StaticVars.h"
void initAtomicMetadata(_StaticVars *mySV)
{
}
void destroyAtomicMetadata(_StaticVars *mySV)
{
}
void delete_all_checkpoints(_StaticVars *mySV)
{
}


int isOnSameNode(int threada, int threadb)
{
    if (_sv) {
        return threada / MYSV.cores_per_node == threadb / MYSV.cores_per_node;
    } else { DIE("Please populate the StaticVars with a valid cores_per_node to access this function\n"); }
}

int isOnMyNode(int threada)
{
    return isOnSameNode(MYTHREAD, threada);
}
