#ifndef _UFX_H_
#define _UFX_H_

#ifdef __cplusplus
extern "C" {
#endif

int ufx_main(int argc, char **argv);

#ifdef __cplusplus
}
#endif
#endif
