#include <cassert>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include <algorithm>
#include <cstring>
#include <numeric>
#include <vector>
#include <sstream>
#include <limits>
#include <array>
#include <cstdint>
#include <functional>
#include <tuple>

#include <mpi.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>

extern "C" {
#include "optlist.h"
}

#include "mpi_common.h"
#include "common.h"
#include "file.h"
#include "stdlib_compatibility.hpp"

#include "StaticVars.h"

#include "MPIUtils.h"
#include "DataStructures/hyperloglog.hpp"
extern "C" {
#ifdef HIPMER_BLOOM64
#include "DataStructures/libbloom/bloom64.h"
#else
#include "DataStructures/libbloom/bloom.h"
#endif
}

#ifdef HEAVYHITTERS

#ifndef MAXHITTERS
#define MAXHITTERS 32000
#endif

#endif

#include "Kmer.hpp"
#include "KmerIterator.hpp"
#include "Deleter.h"
#include "ParallelFASTQ.h"
#include "Friends.h"
#include "MPIType.h"
#include "SimpleCount.h"
#include "FriendsMPI.h"
#include "Pack.h"
#include "Buffer.h"
#include "hash_funcs.h"

#ifdef KHASH
#include "khash.hh"
#elif defined(BYTELL_HASH)
#include "bytell_hash_map.hpp"
#elif defined(STD_HASH)
#include "unordered_map.hpp"
#else
#define VECTOR_MAP
#include "VectorMap.hpp"
#endif


#include "ufx.h"

// Always trim the read at qual==2
#define TRIMSPECIAL // http://en.wikipedia.org/wiki/FASTQ_format#Encoding

#ifdef HEAVYHITTERS
typedef SimpleCount<Kmer, KmerHash> HeavyHitters;
HeavyHitters *heavyhitters = NULL;
UFX2ReduceObj *Frequents;
size_t nfreqs;
#endif

using namespace std;

//#define BENCHMARKONLY // does not output a ufx file when defined
#define MEGA 1000000.0
#define MILLION 1000000
#define COUNT_THRESHOLD 300000
#define COUNT_THRESHOLD_HIGH 30300000
#define HIGH_BIN 100
#define HIGH_NUM_BINS ((COUNT_THRESHOLD_HIGH - COUNT_THRESHOLD) / HIGH_BIN)

#ifndef MAX_ALLTOALL_MEM_PER_NODE
#define MAX_ALLTOALL_MEM_PER_NODE (4096LL * 1024LL * 1024LL) /* 4096 MB per node */
#endif
#define MAX_ALLTOALL_MEM (MAX_ALLTOALL_MEM_PER_NODE / MYSV.cores_per_node)
//#define MAX_ALLTOALL_MEM (128 * 1024 * 1024) /* 128 MB */

int ERR_THRESHOLD;
typedef array<int, 4> ACGT;
int nprocs;
int myrank;
int64_t nonerrorkmers;
int64_t kmersprocessed;
int64_t readsprocessed;
int totaltime;

QualType phred_encoding = 33;
QualType ignored_ext = phred_encoding + 1; // too low to be a good extension
QualType extensionCutoff = 20;

static int KMER_LENGTH = MAX_KMER_SIZE - 1;
static int KMER_PACKED_LENGTH = (MAX_KMER_SIZE - 1 + 3) / 4;

typedef tuple<ACGT, ACGT, int> KmerCountType;

typedef std::pair<Kmer::MERARR, KmerCountType> KmerValue;

#ifndef UPC_ALLOCATOR
#define UPC_ALLOCATOR 1
#endif

#if UPC_ALLOCATOR
#include "upc_allocator.hpp"
#define USE_UPC_ALLOCATOR_IN_MPI

#ifdef NO_UPC_MEMORY_POOL
typedef upc_allocator::simple_allocator< KmerValue > KmerAllocator;
#define ALLOCATOR_MESSAGE "simple UPC allocator"
#else
#include "upc_memory_pool.hpp"
typedef upc_allocator::SingletonMemoryPool< KmerValue > KmerAllocator;
#define ALLOCATOR_MESSAGE "singelton memory pool UPC allocator"
#endif

#else   // do NOT use UPC_ALLOCATOR

typedef std::allocator< KmerValue > KmerAllocator;
#define ALLOCATOR_MESSAGE "standard allocator"

#endif

#ifdef KHASH
// khash does not support a custom allocator
typedef khmap_t<Kmer::MERARR, KmerCountType > KmerCountsType;
#define HASHMAP_MESSAGE "khash hash map (normal allocator)"

#elif defined(BYTELL_HASH)
typedef ska::bytell_hash_map<Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR>, std::equal_to<Kmer::MERARR>, KmerAllocator > KmerCountsType;
//typedef ska::bytell_hash_map<Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR>, std::equal_to<Kmer::MERARR>, std::allocator< KmerValue > > KmerCountsType;
#define HASHMAP_MESSAGE "bytell hash map"

#elif defined(STD_HASH)
typedef std::unordered_map<Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR>, std::equal_to<Kmer::MERARR>, KmerAllocator > KmerCountsType;
#define HASHMAP_MESSAGE "unordered_map hash map"

#else
typedef VectorMap< Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR>, std::less<Kmer::MERARR>, std::equal_to<Kmer::MERARR>, KmerAllocator > KmerCountsType;
#define HASHMAP_MESSAGE "VectorMap hash map"

#endif

//#else   // not UPC_ALLOCATOR
//  typedef U_MAP< Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR> > KmerCountsType;
//typedef VectorMap< Kmer::MERARR, KmerCountType, std::hash<Kmer::MERARR>, std::less<Kmer::MERARR>, std::equal_to<Kmer::MERARR> > KmerCountsType;
//  typedef std::map< Kmer::MERARR, KmerCountType, std::less<Kmer::MERARR> > KmerCountsType;


KmerCountsType *kmercounts = NULL;

void countTotalKmersAndCleanHash();

void writeMaxReadLengths(vector<int> &maxReadLengths, const vector<filedata> &allfiles)
{
    assert(maxReadLengths.size() == allfiles.size());
    int *globalMaxReadLength = new int[allfiles.size()];
    CHECK_MPI(MPI_Reduce(&maxReadLengths[0], globalMaxReadLength, allfiles.size(), MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD));
    if (!myrank) {
        for (int i = 0; i < allfiles.size(); i++) {
            char file[MAX_FILE_PATH];
            char *name = get_basename(file, allfiles[i].filename);
            strcat(name, ".maxReadLen.txt");
            FILE *f = fopen_rank_path(name, "w", -1);
            fprintf(f, "%d", globalMaxReadLength[i]);
            if (fclose_track(f) != 0) {
                DIE("Could not write %s! %s\n", name, strerror(errno));
            }
        }
    }
    delete [] globalMaxReadLength;
}
// Kmer is of length k
// HyperLogLog counting, bloom filtering, and std::maps use Kmer as their key
size_t ParseNPack(vector<string> & seqs, vector<string> & quals, vector< vector<Kmer> > & outgoing,
                  vector< TwoQualVector > & extquals, vector< TwoSeqVector > & extseqs, int pass, size_t offset)
{
    MPI_Pcontrol(1, "ParseNPack");
    QualType special = phred_encoding + 2;
    size_t nreads = seqs.size();
    size_t maxsending = 0, kmersthisbatch = 0;
    size_t bytesperkmer = Kmer::numBytes();
    size_t bytesperentry = bytesperkmer + 4;
    size_t memoryThreshold = (MAX_ALLTOALL_MEM / nprocs) * 2; // 2x any single rank
    DBG("ParseNPack(seqs %lld, qals %lld, out %lld, extq %lld, exts %lld, pass %d, offset %lld)\n", (lld)seqs.size(), (lld)quals.size(), (lld)outgoing.size(), (lld)extquals.size(), (lld)extseqs.size(), pass, (lld)offset);

    for (size_t i = offset; i < nreads; ++i) {
        size_t found;
#ifdef TRIMSPECIAL
        found = quals[i].find_first_of(special);
        if (found == string::npos) {
            found = seqs[i].length();   // remember that the last valid position is length()-1
        }
#else
        found = seqs[i].length();
#endif
        // skip this sequence if the length is too short
        if (seqs[i].length() <= KMER_LENGTH) {
            //cerr << "seq is too short (" << seqs[i].length() << " < " << KMER_LENGTH << " : " << seqs[i] << endl;
            continue;
        }
        int nkmers = (seqs[i].length() - KMER_LENGTH + 1);
        kmersprocessed += nkmers;
        kmersthisbatch += nkmers;

        std::vector<Kmer> kmers = Kmer::getKmers(seqs[i]); // calculate all the kmers
        assert(kmers.size() == nkmers);
        size_t Nfound = seqs[i].find('N');

        for (size_t j = 0; j < nkmers; ++j) {
            while (Nfound != std::string::npos && Nfound < j) {
                Nfound = seqs[i].find('N', Nfound + 1);
            }
            if (Nfound != std::string::npos && Nfound < j + KMER_LENGTH) {
                continue;                                                       // if there is an 'N', toss it
            }
            assert(kmers[j] == Kmer(seqs[i].c_str() + j));

            size_t sending = PackEndsKmer(seqs[i], quals[i], j, kmers[j], outgoing, extquals, extseqs, pass, found, KMER_LENGTH);
            if (sending > maxsending) {
                maxsending = sending;
            }
        }
        if (maxsending * bytesperentry >= memoryThreshold || (kmersthisbatch + seqs[i].length()) * bytesperentry >= MAX_ALLTOALL_MEM) {
            nreads = i + 1; // start with next read
            break;
        }
    }
    //LOGF("ParseNPack got through %lld reads (of %lld), %lld kmers\n", (lld) nreads, (lld) seqs.size(), (lld) kmersthisbatch);
    MPI_Pcontrol(-1, "ParseNPack");
    return nreads;
}

static int exchange_iter = 0;

void Exchange(vector< vector<Kmer> > & outgoing, vector<TwoQualVector> & extquals, vector<TwoSeqVector> & extseqs,
              vector<Kmer> & mykmers, TwoQualVector& myquals, TwoSeqVector& myseqs, int pass, Buffer scratch1, Buffer scratch2)
{
    MPI_Pcontrol(1, "Exchange");
    // pass 1 is just kmers, pass 2 is kmers + seq_extensions & quals
    // serialize k-mer
    size_t bytesperkmer = Kmer::numBytes();
    size_t bytesperentry = bytesperkmer + (pass == 2 ? 4 : 0);
    size_t totalEntries = 0;
    int *sendcnt = new int[nprocs];
    for (int i = 0; i < nprocs; ++i) {
        totalEntries += outgoing[i].size();
        sendcnt[i] = (int)outgoing[i].size() * bytesperentry;
        if (pass == 2) {
            assert(outgoing[i].size() == extquals[i].size());
            assert(outgoing[i].size() == extseqs[i].size());
        } else {
            assert(extquals[i].size() == 0);
            assert(extseqs[i].size() == 0);
        }
    }
    LOGF("Sending %lld entries %0.3f MB - %0.3f MB/thread\n", (lld) totalEntries, 1.0/ONE_MB * totalEntries * bytesperentry, 1.0/ONE_MB * totalEntries * bytesperentry / nprocs);
    int *sdispls = new int[nprocs];
    int *rdispls = new int[nprocs];
    int *recvcnt = new int[nprocs];
    CHECK_MPI(MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD));    // share the request counts
    sdispls[0] = 0;
    rdispls[0] = 0;
    for (int i = 0; i < nprocs - 1; ++i) {
        if (sendcnt[i] < 0 || recvcnt[i] < 0) {
            cerr << myrank << " detected overflow in Alltoall" << endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        sdispls[i + 1] = sdispls[i] + sendcnt[i];
        rdispls[i + 1] = rdispls[i] + recvcnt[i];
        if (sdispls[i + 1] < 0 || rdispls[i + 1] < 0) {
            cerr << myrank << " detected overflow in Alltoall" << endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }
    uint64_t totsend = accumulate(sendcnt, sendcnt + nprocs, static_cast<uint64_t>(0));
    uint64_t totrecv = accumulate(recvcnt, recvcnt + nprocs, static_cast<uint64_t>(0));
    DBG("totsend=%lld totrecv=%lld\n", (lld)totsend, (lld)totrecv);

#ifdef DEBUG
    uint64_t sendmin, sendmax;
    CHECK_MPI(MPI_Reduce(&totsend, &sendmin, 1, MPI_UNSIGNED_LONG_LONG, MPI_MIN, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(&totsend, &sendmax, 1, MPI_UNSIGNED_LONG_LONG, MPI_MAX, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        cout << "Max send: " << sendmax << ", Min send: " << sendmin << " totsend: " << totsend << endl;
    }
#endif // DEBUG

    growBuffer(scratch1, sizeof(uint8_t) * totsend);
    uint8_t *sendbuf = (uint8_t *)getStartBuffer(scratch1);
    for (int i = 0; i < nprocs; ++i) {
        size_t nkmers2send = outgoing[i].size();
        uint8_t *addrs2fill = sendbuf + sdispls[i];
        for (size_t j = 0; j < nkmers2send; ++j) {
            assert(addrs2fill == sendbuf + sdispls[i] + j * bytesperentry);
            (outgoing[i][j]).copyDataInto(addrs2fill);
            if (pass == 2) {
                char *ptr = ((char *)addrs2fill) + bytesperkmer;
                ptr[0] = extquals[i][j][0];
                ptr[1] = extquals[i][j][1];
                ptr[2] = extseqs[i][j][0];
                ptr[3] = extseqs[i][j][1];
            }
            addrs2fill += bytesperentry;
        }
        outgoing[i].clear();
        extquals[i].clear();
        extseqs[i].clear();
    }

    growBuffer(scratch2, sizeof(uint8_t) * totrecv);
    uint8_t *recvbuf = (uint8_t *)getStartBuffer(scratch2);

#ifdef DEBUG
    uint64_t globalmin, globalmax;
    CHECK_MPI(MPI_Reduce(&totrecv, &globalmin, 1, MPI_UNSIGNED_LONG_LONG, MPI_MIN, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(&totrecv, &globalmax, 1, MPI_UNSIGNED_LONG_LONG, MPI_MAX, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        cout << "Max recv: " << globalmax << ", Min recv: " << globalmin << " totrecv: " << totrecv << endl;
    }
    CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
#endif // DEBUG
    DBG("all2all()\n");
    CHECK_MPI(MPI_Alltoallv(sendbuf, sendcnt, sdispls, MPI_BYTE, recvbuf, recvcnt, rdispls, MPI_BYTE, MPI_COMM_WORLD));

    uint64_t nkmersrecvd = totrecv / bytesperentry;
    for (uint64_t i = 0; i < nkmersrecvd; ++i) {
        Kmer kk;
        kk.copyDataFrom(recvbuf + (i * bytesperentry));
        mykmers.push_back(kk);
        char *ptr = ((char *)recvbuf) + (i * bytesperentry) + bytesperkmer;
        if (pass == 2) {
            TwoQual qualexts = { (int8_t)ptr[0], (int8_t)ptr[1] };
            TwoSeq seqexts = { ptr[2], ptr[3] };
            myquals.push_back(qualexts);
            myseqs.push_back(seqexts);
        }
    }

    DBG("DeleteAll: recvcount=%lld, sendct=%lld\n", (lld)recvcnt, (lld)sendcnt);
    DeleteAll(rdispls, sdispls, recvcnt, sendcnt);

    //serial_printf("exchanged totsend=%lld, totrecv=%lld, pass=%d\n", (lld) totsend, (lld) totrecv, pass);
    exchange_iter++;
    MPI_Pcontrol(-1, "Exchange");
}


typedef struct {
    double duration, parsingTime, getKmerTime, lexKmerTime, hllTime, hhTime;
    double last;
} MoreHLLTimers;
inline double getDuration(MoreHLLTimers &t)
{
    double delta = t.last;

    t.last = MPI_Wtime();
    return t.last - delta;
}
MoreHLLTimers InsertIntoHLL(vector<string> & seqs, vector<string> & quals, HyperLogLog & hll, bool extraTimers = false)
{
    QualType special = phred_encoding + 2;
    size_t locreads = seqs.size();
    MoreHLLTimers t;

    if (extraTimers) {
        memset(&t, 0, sizeof(MoreHLLTimers));
        getDuration(t);
        t.duration = t.last;
    }

    readsprocessed += locreads;

    MPI_Pcontrol(1, "HLL_Parse");
    for (size_t i = 0; i < locreads; ++i) {
#ifdef TRIMSPECIAL
        size_t found = quals[i].find_first_of(special);
        if (found == string::npos) {
            found = seqs[i].length();   // remember that the last valid position is length()-1
        }
#else
        size_t found = seqs[i].length();
#endif

        if (found >= KMER_LENGTH) { // otherwise size_t being unsigned will underflow
            if (extraTimers) {
                t.parsingTime += getDuration(t);
            }
            std::vector<Kmer> kmers = Kmer::getKmers(seqs[i]); // calculate all the kmers
            if (extraTimers) {
                t.getKmerTime += getDuration(t);
            }

            assert(kmers.size() >= found - KMER_LENGTH + 1);
            size_t Nfound = seqs[i].find('N');
            for (size_t j = 0; j < found - KMER_LENGTH + 1; ++j) {
                assert(kmers[j] == Kmer(seqs[i].c_str() + j));
                while (Nfound != std::string::npos && Nfound < j) {
                    Nfound = seqs[i].find('N', Nfound + 1);
                }
                if (Nfound != std::string::npos && Nfound < j + KMER_LENGTH) {
                    continue;                                                       // if there is an 'N', toss it
                }
                Kmer &mykmer = kmers[j];

                if (extraTimers) {
                    t.parsingTime += getDuration(t);
                }
                Kmer lexsmall = mykmer.rep();
                if (extraTimers) {
                    t.lexKmerTime += getDuration(t);
                }
                hll.add((const char *)lexsmall.getBytes(), lexsmall.getNumBytes());
                if (extraTimers) {
                    t.hllTime += getDuration(t);
                }
#ifdef HEAVYHITTERS
                if (heavyhitters) {
                    heavyhitters->Push(lexsmall);
                }
                if (extraTimers) {
                    t.hhTime += getDuration(t);
                }
#endif
            }
        }
        DBG2("HLL Processed %lld\n", (lld)i);
    }
    MPI_Pcontrol(-1, "HLL_Parse");
    seqs.clear();
    quals.clear();
    if (extraTimers) {
        t.last = t.duration;
        t.duration = getDuration(t);
    }
    return t;
}

void ProudlyParallelCardinalityEstimate(const vector<filedata> & allfiles, double & cardinality, bool cached_io_reads, const char *base_dir)
{
    HyperLogLog hll(12);
    int numFiles = allfiles.size();
    double *_times = (double *)calloc(numFiles * 16, sizeof(double));
    double *pfq_times = _times;
    double *read_times = pfq_times + numFiles;
    double *HH_reduce_times = read_times + numFiles;
    double *HH_max_times = HH_reduce_times + numFiles;
    double *tot_times = HH_max_times + numFiles;
    double *max_times = tot_times + numFiles;
    double *min_times = max_times + numFiles;
    double *HLL_times = min_times + numFiles;
    double *HLL_avg_times = HLL_times + numFiles;
    double *HLL_max_times = HLL_avg_times + numFiles;
    double *avg_per_file_times = HLL_max_times + numFiles;
    double *max_per_file_times = avg_per_file_times + numFiles;
    double *tot_HH_times = max_per_file_times + numFiles;
    double *max_HH_times = tot_HH_times + numFiles;

    for (auto itr = allfiles.begin(); itr != allfiles.end(); itr++) {
        double start_t = MPI_Wtime();
        double hll_time = 0.0;
        int idx = itr - allfiles.begin();
        ParallelFASTQ pfq;
        pfq.open(itr->filename, cached_io_reads, base_dir, itr->filesize);
        // The fastq file is read line by line, so the number of records processed in a block
        // shouldn't make any difference, so we can just set this to some arbitrary value.
        // The value 262144 is for records with read lengths of about 100.
        size_t upperlimit = MAX_ALLTOALL_MEM / 16;
        size_t totalsofar = 0;
        vector<string> seqs;
        vector<string> quals;
        vector<Kmer> mykmers;
        int iterations = 0;

        while (1) {
            MPI_Pcontrol(1, "FastqIO");
            iterations++;
            size_t fill_status = pfq.fill_block(seqs, quals, upperlimit);
            DBG("fill_status=%lld, seqs.size()=%lld\n", (lld)fill_status, (lld)seqs.size());
            // Sanity checks
            assert(seqs.size() == quals.size());
            for (int i = 0; i < seqs.size(); i++) {
                if (seqs[i].length() != quals[i].length()) {
                    DIE("sequence length %lld != quals length %lld\n%s\n",
                        (lld)seqs[i].length(), (lld)quals[i].length(), seqs[i].c_str());
                }
            }
            MPI_Pcontrol(-1, "FastqIO");
            if (!fill_status) {
                break;
            }
            double start_hll = MPI_Wtime();
            int64_t numSeqs = seqs.size();
            // this clears the vectors
            MoreHLLTimers mt = InsertIntoHLL(seqs, quals, hll, true);
            hll_time += MPI_Wtime() - start_hll;
            DBG("HLL timings: iteration %d, seqs %lld, duration %0.4f, parsing %0.4f, getKmer %0.4f, lexKmer %0.4f, hllTime %0.4f, hhTime %0.4f\n", iterations, (lld) numSeqs, mt.duration, mt.parsingTime, mt.getKmerTime, mt.lexKmerTime, mt.hllTime, mt.hhTime);
        }

        HLL_times[idx] = hll_time;
        pfq_times[idx] = pfq.get_elapsed_time();

#ifdef HEAVYHITTERS
        if (heavyhitters) {
            HH_reduce_times[idx] = heavyhitters->GetReduceTime() - (idx == 0 ? 0.0 : HH_reduce_times[idx - 1]);
        } else {
            HH_reduce_times[idx] = 0.0;
        }

        HH_max_times[idx] = HH_reduce_times[idx];
#endif  // HEAVYHITTERS

#ifdef DEBUG
        CHECK_MPI(MPI_Reduce(read_times + idx, tot_times + idx, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
        if (myrank == 0) {
            int num_ranks;
            double t2 = pfq.get_elapsed_time();
            CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &num_ranks));
            cout << "Total time taken for FASTQ reads is " << (tot_times[idx] / num_ranks) << ", elapsed " << t2 << endl;
        }
#endif  // DEBUG
        read_times[idx] = MPI_Wtime() - start_t;
    }

#ifdef HEAVYHITTERS
    if (heavyhitters && myrank == 0) {
        cout << "Thread " << myrank << " HeavyHitters performance: " << heavyhitters->getPerformanceStats() << endl;
    }
#endif // HEAVYHITTERS

#ifndef DEBUG
    CHECK_MPI(MPI_Reduce(pfq_times, tot_times, allfiles.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(pfq_times, max_times, allfiles.size(), MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(pfq_times, min_times, allfiles.size(), MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(HH_reduce_times, tot_HH_times, allfiles.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(HH_max_times, max_HH_times, allfiles.size(), MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(HLL_times, HLL_avg_times, allfiles.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(HLL_times, HLL_max_times, allfiles.size(), MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD));

    // maximum elapsed time per file
    CHECK_MPI(MPI_Reduce(read_times, avg_per_file_times, allfiles.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(read_times, max_per_file_times, allfiles.size(), MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        int num_ranks;
        CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &num_ranks));
        for (int i = 0; i < allfiles.size(); i++) {
            cout << "Total time taken for FASTQ " << allfiles[i].filename << " size: " << allfiles[i].filesize << " avg: " << tot_times[i] / num_ranks << " max: " << max_times[i] << " min: " << min_times[i] << " HLL avg: " << HLL_avg_times[i] / num_ranks << " HLL max: " << HLL_max_times[i] << " HH avg: " << HH_reduce_times[i] / num_ranks << " HH max: " << HH_max_times[i] << " elapsed_avg: " << avg_per_file_times[i] / num_ranks << " elapsed_max: " << max_per_file_times[i] << " seconds" << endl;
        }
    }
#endif // ndef DEBUG
    LOGF("My cardinality before reduction: %f\n", hll.estimate());

    // using MPI_UNSIGNED_CHAR because MPI_MAX is not allowed on MPI_BYTE
    int count = hll.M.size();
    CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, hll.M.data(), count, MPI_UNSIGNED_CHAR, MPI_MAX, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, &readsprocessed, 1, MPI_INT64_T, MPI_SUM, MPI_COMM_WORLD));

    cardinality = hll.estimate();
    if (myrank == 0) {
        cout << "Embarrassingly parallel k-mer count estimate is " << cardinality << endl;
        cout << "Total reads processed over all processors is " << readsprocessed << endl;
        ADD_DIAG("%f", "cardinality", cardinality);
        ADD_DIAG("%lld", "total_reads", (lld)readsprocessed);
    }
    SLOG("total cardinality %f\n", cardinality);
#ifdef HEAVYHITTERS
    if (heavyhitters) {
        double hh_merge_start = MPI_Wtime();
        MPI_Pcontrol(1, "HeavyHitters");
        ParallelAllReduce(*heavyhitters);
        nfreqs = heavyhitters->Size();
        heavyhitters->CreateIndex();     // make it indexible
        double myMergeTime, avgMergeTime, maxMergeTime;
        myMergeTime = heavyhitters->GetMergeTime();
        CHECK_MPI(MPI_Reduce(&myMergeTime, &maxMergeTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(&myMergeTime, &avgMergeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
        if (myrank == 0) {
            int num_ranks;
            CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &num_ranks));
            cout << "Total time taken for HH Merge " << (MPI_Wtime() - hh_merge_start) << " avg HH merge: " << avgMergeTime / num_ranks << " max: " << maxMergeTime << endl;
        }
        Frequents = new UFX2ReduceObj[nfreqs]();        // default initialize
        MPI_Pcontrol(-1, "HeavyHitters");
    } else {
        SLOG("Skipping heavy hitter merge\n");
    }
#endif

    MPI_Barrier(MPI_COMM_WORLD);
    cardinality /= static_cast<double>(nprocs); // assume a balanced distribution
    cardinality *= 1.1;                         // 10% benefit of doubt
    LOGF("Adjusted per-process cardinality: %f\n", cardinality);
}

class KmerInfo {
public:
private:
Kmer kmer;
TwoSeq seqs;
TwoQual quals;
public:
KmerInfo()
{
}
KmerInfo(Kmer k) : kmer(k), quals(), seqs()
{
}
KmerInfo(Kmer k, TwoQual q, TwoSeq s) : kmer(k), quals(q), seqs(s)
{
}
KmerInfo(const KmerInfo &copy)
{
    kmer = copy.kmer;
    quals = copy.quals;
    seqs = copy.seqs;
}
const Kmer& getKmer() const
{
    return kmer;
}
int write(GZIP_FILE f)
{
    int count = GZIP_FWRITE(this, sizeof(*this), 1, f);

    if (count != sizeof(*this) * 1) {
        DIE("There was a problem writing the kmerInfo file! %s\n", strerror(errno));
    }
    return count;
}
int read(GZIP_FILE f)
{
    int count = GZIP_FREAD(this, sizeof(*this), 1, f);

    if (count != sizeof(*this) * 1 && !GZIP_EOF(f)) {
        DIE("There was a problem reading the kmerInfo file! %s\n", strerror(errno));
    }
    return count;
}
// returns true if in bloom, does not modify
bool checkBloom(struct bloom *bm)
{
    MPI_Pcontrol(1, "BloomFilter");
    bool inBloom = bloom_check(bm, kmer.getBytes(), kmer.getNumBytes()) == 1;
    MPI_Pcontrol(-1, "BloomFilter");
    return inBloom;
}
// returns true if in bloom, inserts if not
bool checkBloomAndRemember(struct bloom *bm)
{
    bool inBloom = checkBloom(bm);

    if (!inBloom) {
        MPI_Pcontrol(1, "BloomFilter");
        bloom_add(bm, kmer.getBytes(), kmer.getNumBytes());
        MPI_Pcontrol(-1, "BloomFilter");
    }
    return inBloom;
}
// returns true when kmer is already in bloom, if in bloom, inserts into map, if andCount, increments map count
bool checkBloomAndInsert(struct bloom *bm, bool andCount)
{
    bool inBloom = checkBloomAndRemember(bm);

    if (inBloom) {
        MPI_Pcontrol(1, "InsertOrUpdate");
        auto got = kmercounts->find(kmer.getArray());      // kmercounts is a global variable
        if (got == kmercounts->end()) {
            ACGT nulltuple = { 0, 0, 0, 0 };
#ifdef KHASH
            kmercounts->insert(kmer.getArray(), make_tuple(nulltuple, nulltuple, 0));
#else
            kmercounts->insert(make_pair(kmer.getArray(), make_tuple(nulltuple, nulltuple, 0)));
#endif
            if (andCount) {
                includeCount();
            }
        } else {
            if (andCount) {
                includeCount(got);
            }
        }
        MPI_Pcontrol(-1, "InsertOrUpdate");
    }
    return inBloom;
}

bool includeCount()
{
    auto got = kmercounts->find(kmer.getArray());      // kmercounts is a global variable

    return includeCount(got);
}

bool includeCount(KmerCountsType::iterator got)
{
    MPI_Pcontrol(1, "HashTable");
    bool inserted = false;
    assert(quals.size() == 2);
    assert(seqs.size() == 2);
    if (got != kmercounts->end()) {  // don't count anything else
        if (quals[0] >= 0 && quals[1] >= 0) {
            // count the kmer in mercount
#ifdef KHASH
            ++(get<2>(*got));        // ::value returns (const valtype_t &) but ::* returns (valtype_t &), which can be changed
#else
            ++(get<2>(got->second)); // increment the counter regardless of quality extensions
#endif
        } else {
            // ignore the mercount, but still use the extensions, if good enough
            assert(quals[0] <= 0);
            assert(quals[1] <= 0);
            quals[0] = 0 - quals[0];
            quals[1] = 0 - quals[1];
        }
        if (quals[0] < phred_encoding || quals[1] < phred_encoding) {
            WARN("Detected incorrect quals %d or %d is < phred_encoding=%d kmer=%s\n", quals[0], quals[1], phred_encoding, Kmer(got->first).toString().c_str());
        }
        assert(quals[0] >= phred_encoding);
        assert(quals[1] >= phred_encoding);
        if (quals[0] >= phred_encoding + extensionCutoff) {
#ifdef KHASH
            Increment1stBasedOnExt(*got, seqs[0]);
#else
            Increment1stBasedOnExt(got->second, seqs[0]);
#endif
        }
        if (quals[1] >= phred_encoding + extensionCutoff) {
#ifdef KHASH
            Increment2ndBasedOnExt(*got, seqs[1]);
#else
            Increment2ndBasedOnExt(got->second, seqs[1]);
#endif
        }
        inserted = true;
    }
    MPI_Pcontrol(-1, "HashTable");
    return inserted;
}
};

// at this point, no kmers include anything other than uppercase 'A/C/G/T'
void DealWithInMemoryData(vector<Kmer> & mykmers, int pass, struct bloom *bm, TwoQualVector& myquals, TwoSeqVector & myseqs, GZIP_FILE firstTimeKmers)
{
    // store kmer & extensions with confirmaton of bloom
    // store to disk first time kmers with (first) insert into bloom
    // pass 1 - just store kmers in bloom and insert into count, pass 2 - count (if firstTimeKmers also store bloom & insert & store firstTimeKmers for later)
    //LOGF("Dealing with memory pass %d: mykmers=%lld, myseqs=%lld\n", pass, (lld) mykmers.size(), (lld) myseqs.size());
    if (firstTimeKmers) {
        assert(pass == 2);
    }
    if (pass == 2) {
        assert(mykmers.size() == myquals.size());
        assert(mykmers.size() == myseqs.size());
    }
    if (pass == 1) {
        assert(myquals.size() == 0);
        assert(myseqs.size() == 0);
    }
#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::startUPC();
#endif

    if (pass == 1) {
        assert(!firstTimeKmers);
        assert(bm);
        MPI_Pcontrol(1, "BloomFilter");
        size_t count = mykmers.size();
        for (size_t i = 0; i < count; ++i) {
            KmerInfo ki(mykmers[i]);
            // there will be a second pass, just insert into bloom, and map, but do not count
            ki.checkBloomAndInsert(bm, false);
        }
        MPI_Pcontrol(-1, "BloomFilter");
    } else {
        assert(pass == 2);
        assert(firstTimeKmers || bm == NULL);
        MPI_Pcontrol(1, "CountKmers");
        size_t count = mykmers.size();
        for (size_t i = 0; i < count; ++i) {
            KmerInfo ki(mykmers[i], myquals[i], myseqs[i]);
            DBG2("i=%lld kmer=%s quals=%d %d seqs=%c %c\n", (lld) i, mykmers[i].toString().c_str(), myquals[i][0], myquals[i][1], myseqs[i][0], myseqs[i][1]);
#ifdef HEAVYHITTERS
#ifdef DEBUG
            if (heavyhitters && heavyhitters->IsMember(mykmers[i])) {
                WARN("Detected heavy hitter was exchanged: %s\n", mykmers[i].toString().c_str());
            }
#endif
            assert(!heavyhitters || !heavyhitters->IsMember(mykmers[i]));
#endif
            if (firstTimeKmers) {
                // This is the first and only pass
                assert(bm);
                if (ki.checkBloomAndInsert(bm, true)) {
                    // found and included this kmer
                    DBG3("Remembering kmer: %s\n", mykmers[i].toString().c_str());
                } else {
                    // store first time kmers in case it turns out to be good
                    ki.write(firstTimeKmers);
                }
            } else {
                assert(!bm);
                ki.includeCount();
            }
        }
        MPI_Pcontrol(-1, "CountKmers");
    }

#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::endUPC();
#endif
}

void ProcessFiles(const vector<filedata> & allfiles, int pass, double & cardinality, bool cached_io_reads, const char *base_dir, const char *firstTimeKmerPath)
{
    struct bloom *bm = NULL;
    GZIP_FILE firstTimeKmers = NULL;

    char firstTimeKmersFile[MAX_FILE_PATH];

    firstTimeKmersFile[0] = '\0';
    char *ft_filename = firstTimeKmersFile;
    int exchangeAndCountPass = pass;
    if (firstTimeKmerPath) {
        assert(pass == 1);
        // break streaming, and skip one pass, building bloom and storing first time kmers in a temporary file
        exchangeAndCountPass = 2;
        snprintf(firstTimeKmersFile, MAX_FILE_PATH, "%s/firstTimeKmers_%d" GZIP_EXT, firstTimeKmerPath, myrank);
        ft_filename = get_rank_path(firstTimeKmersFile, myrank);
        MPI_Pcontrol(1, "WritePossibleErrorKmer");
        firstTimeKmers = GZIP_OPEN(ft_filename, "w");
        MPI_Pcontrol(-1, "WritePossibleErrorKmer");
        LOGF("Opened firstTimeKmers error file: %s\n", ft_filename);
    }

    Buffer scratch1 = initBuffer(MAX_ALLTOALL_MEM);
    Buffer scratch2 = initBuffer(MAX_ALLTOALL_MEM);

    if (pass == 1) {
        // Only require bloom in pass 1!!
        unsigned int random_seed = 0xA57EC3B2;
        const double desired_probability_of_false_positive = 0.05;
        bm = (struct bloom *)malloc(sizeof(struct bloom));
#ifdef HIPMER_BLOOM64
        bloom_init64(bm, cardinality, desired_probability_of_false_positive);
#else
        assert(cardinality < 1L << 32);
        bloom_init(bm, cardinality, desired_probability_of_false_positive);
#endif

        if (myrank == 0) {
            cout << "Table size is: " << bm->bits << " bits, " << ((double)bm->bits) / 8 / 1024 / 1024 << " MB" << endl;
            cout << "Optimal number of hash functions is : " << bm->hashes << endl;
        }

        LOGF("Initialized bloom filter with %lld bits and %d hash functions\n", (lld)bm->bits, (int)bm->hashes);
    }
    std::vector<int> maxReadLengths;

    double pfqTime = 0.0;
    auto files_itr = allfiles.begin();
    int trunc_ret;
    double t01 = MPI_Wtime(), t02;
    double tot_pack = 0.0, tot_exch = 0.0, tot_process = 0.0;
    while (files_itr != allfiles.end()) {
        ParallelFASTQ *pfq = new ParallelFASTQ();
        pfq->open(files_itr->filename, cached_io_reads, base_dir, files_itr->filesize);
        LOGF("Opened %s of %lld size\n", files_itr->filename, (lld)files_itr->filesize);
        files_itr++;
        // once again, arbitrarily chosen - see ProudlyParallelCardinalityEstimate
        size_t upperlimit = MAX_ALLTOALL_MEM / 16;

        vector<string> seqs;
        vector<string> quals;
        vector< vector<Kmer> > outgoing(nprocs);
        vector< TwoQualVector > extquals(nprocs);
        vector< TwoSeqVector > extseqs(nprocs);

        vector<Kmer> mykmers;
        TwoQualVector myquals;
        TwoSeqVector myseqs;

        int moreflags[3], allmore2go[3], anymore2go;
        int &moreSeqs = moreflags[0], &moreToExchange = moreflags[1], &moreFiles = moreflags[2];
        int &allmoreSeqs = allmore2go[0], &allmoreToExchange = allmore2go[1], &allmoreFiles = allmore2go[2];
        moreSeqs = 1;       // assume more as file is just open
        moreFiles = (files_itr != allfiles.end());
        moreToExchange = 0; // no data yet
        size_t fill_status;
        int exchanges = 0;
        do {
            DBG("Starting new round: moreSeqs=%d, moreFiles=%d\n", moreSeqs, moreFiles);
            MPI_Pcontrol(1, "FastqIO");
            do {
                DBG2("Filling a block: moreSeqs=%d, moreFiles=%d\n", moreSeqs, moreFiles);
                // fill a block, from this or the next file
                if (pfq && !moreSeqs) {
                    assert(pfq != NULL);
                    double t = pfq->get_elapsed_time();
                    pfqTime += t;
                    DBG2("Closed last file %.3f sec\n", t);
                    maxReadLengths.push_back(pfq->get_max_read_len());
                    delete pfq;
                    pfq = NULL;
                    if (files_itr != allfiles.end()) {
                        pfq = new ParallelFASTQ();
                        pfq->open(files_itr->filename, cached_io_reads, base_dir, files_itr->filesize);
                        DBG2("Opened new file %s of %lld size\n", files_itr->filename, (lld)files_itr->filesize);
                        files_itr++;
                        moreSeqs = 1;
                    }
                }
                moreFiles = (files_itr != allfiles.end());
                fill_status = 0;
                if (moreSeqs) {
                    fill_status = pfq->fill_block(seqs, quals, upperlimit);  // file_status is 0 if fpos >= end_fpos
                    long long llf = fill_status;
                    DBG2("Filled block to %lld\n", (lld)llf);
                    assert(fill_status == seqs.size());
                    assert(fill_status == quals.size());
                }
                moreSeqs = (fill_status > 0);
            } while (moreFiles && !moreSeqs);
            MPI_Pcontrol(-1, "FastqIO");

            size_t offset = 0;
            do {
                DBG("Starting Exchange - ParseNPack %lld (%lld)\n", (lld)offset, (lld)seqs.size());

                double exch_start_t = MPI_Wtime();
                offset = ParseNPack(seqs, quals, outgoing, extquals, extseqs, exchangeAndCountPass, offset);    // no-op if seqs.size() == 0
                double pack_t = MPI_Wtime() - exch_start_t;
                tot_pack += pack_t;

                DBG("Packed up to  %lld.  Starting Exchange outgoing %lld %0.3f sec\n", (lld)offset, (lld)outgoing.size(), pack_t);
                if (offset == seqs.size()) {
                    seqs.clear();   // no need to do the swap trick as we will reuse these buffers in the next iteration
                    quals.clear();
                    offset = 0;
                }
                Exchange(outgoing, extquals, extseqs, mykmers, myquals, myseqs, exchangeAndCountPass, scratch1, scratch2); // outgoing arrays will be all empty, shouldn't crush
                double exch_t = MPI_Wtime() - exch_start_t - pack_t;
                tot_exch += exch_t;
                DBG("Exchanged. Received %lld %0.3f sec\n", (lld)mykmers.size(), exch_t);

                if (exchangeAndCountPass == 2) {
                    assert(mykmers.size() == myquals.size());
                    assert(mykmers.size() == myseqs.size());
                } else {
                    assert(0 == myquals.size());
                    assert(0 == myseqs.size());
                }

                DealWithInMemoryData(mykmers, exchangeAndCountPass, bm, myquals, myseqs, firstTimeKmers);   // we might still receive data even if we didn't send any
                double process_t = MPI_Wtime() - exch_start_t - pack_t - exch_t;
                tot_process += process_t;
                moreToExchange = offset < seqs.size();
                DBG("Processed (%lld).  remainingToExchange=%lld %0.3f sec\n", (lld)mykmers.size(), (lld)seqs.size() - offset, process_t);
                mykmers.clear();
                myquals.clear();
                myseqs.clear();

                DBG("Checking global state: moreSeqs=%d moreToExchange=%d moreFiles=%d\n", moreSeqs, moreToExchange, moreFiles);
                CHECK_MPI(MPI_Allreduce(moreflags, allmore2go, 3, MPI_INT, MPI_SUM, MPI_COMM_WORLD));
                DBG("Got global state: allmoreSeqs=%d allmoreToExchange=%d allmoreFiles=%d\n", allmoreSeqs, allmoreToExchange, allmoreFiles);
                double now = MPI_Wtime();
                if (myrank == 0 && !(exchanges % 30)) {
                    cout << "active ranks moreSeqs: " << allmoreSeqs
                         << " moreToExchange: " << allmoreToExchange
                         << " moreFiles: " << allmoreFiles
                         << ", rank " << myrank
                         << " moreSeqs: " << moreSeqs
                         << " moreToExchange: " << moreToExchange
                         << " moreFiles: " << moreFiles;
                    cout << " pack_time: " << std::fixed << std::setprecision(3) << pack_t
                         << " exchange_time: " << std::fixed << std::setprecision(3) << exch_t
                         << " process_time: " << std::fixed << std::setprecision(3) << process_t
                         << " elapsed: " << std::fixed << std::setprecision(3) << now - t01 << endl;
                }
                LOGF("Exchange timings pack: %0.3f exch: %0.3f process: %0.3f elapsed: %0.3f\n", pack_t, exch_t, process_t, now - t01);
            } while (moreToExchange);
            anymore2go = allmoreSeqs + allmoreToExchange + allmoreFiles;
            exchanges++;
        } while (anymore2go);

        if (myrank == 0) {
            serial_printf("Excanged %lld rounds\n", (lld)exchanges);
        }

        if (pfq) {
            double t = pfq->get_elapsed_time();
            pfqTime += t;
            DBG("Closing last file: %.3f sec\n", t);
            maxReadLengths.push_back(pfq->get_max_read_len());
            delete pfq;
            pfq = NULL;
        }

        // Calculate the max_read_len for each of the input files and write to marker files
        writeMaxReadLengths(maxReadLengths, allfiles);
    }   // end_of_loop_over_all_files

    t02 = MPI_Wtime();
    double tots[5], gtots[5] = { 0.0, 0.0, 0.0, 0.0, 0.0 };
    tots[0] = pfqTime;
    tots[1] = tot_pack;
    tots[2] = tot_exch;
    tots[3] = tot_process;
    tots[4] = t02 - t01;
    LOGF("Process Total times: fastq: %0.3f pack: %0.3f exch: %0.3f process: %0.3f elapsed: %0.3f\n", pfqTime, tot_pack, tot_exch, tot_process, tots[4]);

    CHECK_MPI(MPI_Reduce(&tots, &gtots, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        int num_ranks;
        CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &num_ranks));
        cout << "Average time taken for FASTQ reads is " << (gtots[0] / num_ranks) << ", myelapsed " << tots[0] << endl;
        cout << "Average time taken for pack  reads is " << (gtots[1] / num_ranks) << ", myelapsed " << tots[1] << endl;
        cout << "Average time taken for exch  reads is " << (gtots[2] / num_ranks) << ", myelapsed " << tots[2] << endl;
        cout << "Average time taken for proc  reads is " << (gtots[3] / num_ranks) << ", myelapsed " << tots[3] << endl;
        cout << "Average time taken for elapsed     is " << (gtots[4] / num_ranks) << ", myelapsed " << tots[4] << endl;
    }

    if (myrank == 0) {
        cout << "PASS " << pass << endl;
        cout << "Read/distributed/processed reads of " << (files_itr == allfiles.end() ? " ALL files " : files_itr->filename) << " in " << t02 - t01 << " seconds" << endl;
    }

    LOGF("Finished Pass %d, Freeing bloom and other memory. kmercounts: %lld entries\n", pass, (lld)kmercounts->size());
    t02 = MPI_Wtime(); // redefine after prints

    if (bm) {
        LOGF("Freeing Bloom filter\n");
        bloom_free(bm);
        free(bm);
        bm = NULL;
    }

    freeBuffer(scratch1);
    freeBuffer(scratch2);

    if (firstTimeKmers) {
        MPI_Pcontrol(1, "ReprocessFirstObservations");
        assert(pass == 1);
        assert(exchangeAndCountPass == 2);
        KmerInfo ki;

        // close firstTimeKmers
        size_t ftFileSize = GZIP_FTELL(firstTimeKmers);
        SLOG("First time kmer file size: %0.3f MB %lld kmers\n", 1.0 * ftFileSize / (1024 * 1024), (lld)ftFileSize / sizeof(KmerInfo));
        GZIP_CLOSE(firstTimeKmers);

        LOGF("Rescanning firstTimeKmers: %lld bytes %lld kmers\n", (lld)ftFileSize, (lld)ftFileSize / sizeof(KmerInfo));
        // reopen for reading
        firstTimeKmers = GZIP_OPEN(ft_filename, "r");
        int64_t recovered = 0, rescanned = 0;
        while (ki.read(firstTimeKmers)) {
            rescanned++;
            if (ki.includeCount()) {
                recovered++;
            }
        }
        double rescan_t = MPI_Wtime() - t02;


        LOGF("Finished first time kmers: %lld rescanned, %lld recovered (%d %%), %lld total kmers in %0.3f seconds\n", (lld)rescanned, (lld)recovered, (int)((rescanned > 0) ? (100 * recovered / rescanned) : 0), (lld)ftFileSize / sizeof(KmerInfo), rescan_t);
        LOGF("kmercounts: %lld entries\n", (lld)kmercounts->size());

        GZIP_CLOSE(firstTimeKmers);
        unlink(ft_filename);
        firstTimeKmers = NULL;
        DBG("Purged firstTimeKmers: %s\n", ft_filename);
        MPI_Pcontrol(-1, "ReprocessFirstObservations");
    }

    if (exchangeAndCountPass == 2) {
        countTotalKmersAndCleanHash();
    }
}

void countTotalKmersAndCleanHash()
{
    MPI_Pcontrol(1, "HashClean");

    int64_t maxcount = 0;
    int64_t globalmaxcount = 0;
    int64_t hashsize = 0;
    for (auto itr = kmercounts->begin(); itr != kmercounts->end(); ++itr) {
#ifdef KHASH
        if (!itr.isfilled()) {
            continue;                   // not all entries are full in khash
        }
        int allcount = get<2>(itr.value());
#else
        int allcount = get<2>(itr->second);
#endif
        if (allcount > maxcount) {
            maxcount = allcount;
        }
        nonerrorkmers += allcount;
        ++hashsize;
    }
    LOGF("my hashsize=%lld, nonerrorkmers=%lld, maxcount=%lld\n", (lld)hashsize, (lld)nonerrorkmers, (lld)maxcount);


    int64_t totalnonerror;
    int64_t distinctnonerror;
    CHECK_MPI(MPI_Reduce(&nonerrorkmers, &totalnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(&hashsize, &distinctnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Allreduce(&maxcount, &globalmaxcount, 1, MPI_LONG_LONG, MPI_MAX, MPI_COMM_WORLD));
    if (myrank == 0) {
        cout << "Counting finished " << endl;
        cout << "Kmerscount hash includes " << distinctnonerror << " distinct elements" << endl;
        cout << "Kmerscount non error kmers count is " << totalnonerror << endl;
        cout << "Global max count is " << globalmaxcount << endl;
        cout << "Large count histogram is of size " << HIGH_NUM_BINS << endl;
        ADD_DIAG("%lld", "distinct_non_error_kmers", (lld)distinctnonerror);
        ADD_DIAG("%lld", "total_non_error_kmers", (lld)totalnonerror);
        ADD_DIAG("%lld", "global_max_count", (lld)globalmaxcount);
    }
#ifdef HISTOGRAM
    vector<int64_t> hist(COUNT_THRESHOLD, 0);    // zero initialize
    vector<int64_t> hist_high(HIGH_NUM_BINS, 0);
#endif

#ifdef HEAVYHITTERS
    if (heavyhitters) {
        double shh = MPI_Wtime();
        MPI_Op ufxreducempiop;
        if ((_sv != NULL) && MYSV._my_log && HIPMER_VERBOSITY >= LL_DBG2) {
            for (size_t i = 0; i < nfreqs; ++i) {
                Kmer kmer = heavyhitters->Get(i);
                DBG2("myHeavyHitter: %s %d [%d,%d,%d,%d] [%d,%d,%d,%d]\n", kmer.toString().c_str(), Frequents[i].count, Frequents[i].ACGTleft[0], Frequents[i].ACGTleft[1], Frequents[i].ACGTleft[2], Frequents[i].ACGTleft[3], Frequents[i].ACGTrigh[0], Frequents[i].ACGTrigh[1], Frequents[i].ACGTrigh[2], Frequents[i].ACGTrigh[3]);
            }
        }
        CHECK_MPI(MPI_Op_create(MPI_UFXReduce, true, &ufxreducempiop));      // create commutative mpi-reducer
        CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, Frequents, nfreqs, MPIType<UFX2ReduceObj>(), ufxreducempiop, MPI_COMM_WORLD));
        int heavyitems = 0;
        int millioncaught = 0;
        int64_t heavycounts = 0;
#ifdef USE_UPC_ALLOCATOR_IN_MPI
        KmerAllocator::startUPC();
#endif
        int missingHH = 0;
        for (size_t i = 0; i < nfreqs; ++i) {
            Kmer kmer = heavyhitters->Get(i);
            assert(heavyhitters->IsMember(kmer));
            assert(heavyhitters->FindIndex(kmer) == i);
            uint64_t myhash = kmer.hash();
            double range = static_cast<double>(myhash) * static_cast<double>(nprocs);
            size_t owner = range / static_cast<double>(numeric_limits<uint64_t>::max());
            if (owner == myrank) {
                bool inserted = false, merged = false;
                auto iter = kmercounts->find(kmer.getArray());
                if (iter != kmercounts->end()) {
#ifdef KHASH
                    KmerCountType &kct = *iter;
#else
                    KmerCountType &kct = iter->second;
#endif
                    DBG("HH kmer     count for i=%lld %s exists %d (HH: %d)\n", (lld) i, kmer.toString().c_str(), get<2>(kct), Frequents[i].count);
                } else {
                    missingHH++;
                    DBG2("HH kmer missing for i=%lld\n", (lld) i);
                }
                if (UFX2ReduceObjHasCount(Frequents[i])) {
                    if (iter == kmercounts->end()) {
                        inserted = true;
#ifdef KHASH
                        kmercounts->insert(kmer.getArray(), make_tuple(Frequents[i].ACGTleft, Frequents[i].ACGTrigh, Frequents[i].count));
#else
                        kmercounts->insert(make_pair(kmer.getArray(), make_tuple(Frequents[i].ACGTleft, Frequents[i].ACGTrigh, Frequents[i].count)));
#endif
                    } else {
                        assert(false);   // This should not happen
                        merged = true;
#ifdef KHASH
                        KmerCountType &kct = *iter;
#else
                        KmerCountType &kct = iter->second;
#endif
                        DBG("before %s %d [%d,%d,%d,%d] [%d,%d,%d,%d]\n", kmer.toString().c_str(), get<2>(kct), get<0>(kct)[0], get<0>(kct)[1], get<0>(kct)[2], get<0>(kct)[3], get<1>(kct)[0], get<1>(kct)[1], get<1>(kct)[2], get<1>(kct)[3]);

                        get<2>(kct) += Frequents[i].count;
                        for (int b = 0; b < 4; b++) {
                            get<0>(kct)[b] += Frequents[i].ACGTleft[b];
                            get<1>(kct)[b] += Frequents[i].ACGTrigh[b];
                        }
                    }

                    nonerrorkmers += Frequents[i].count;
                    heavycounts += Frequents[i].count;
                    if (Frequents[i].count > maxcount) {
                        maxcount = Frequents[i].count;
                    }
                    if (Frequents[i].count > MILLION) {
                        ++millioncaught;
                    }
                }
                DBG2("HeavyHitter (%s): %s %d [%d,%d,%d,%d] [%d,%d,%d,%d]\n", (inserted | merged) ? (inserted ? "insert" : "merged") : "notused", kmer.toString().c_str(), Frequents[i].count, Frequents[i].ACGTleft[0], Frequents[i].ACGTleft[1], Frequents[i].ACGTleft[2], Frequents[i].ACGTleft[3], Frequents[i].ACGTrigh[0], Frequents[i].ACGTrigh[1], Frequents[i].ACGTrigh[2], Frequents[i].ACGTrigh[3]);
                ++hashsize;
                ++heavyitems;
            } else { // not the owner
                assert(kmercounts->find(kmer.getArray()) == kmercounts->end());
            }
        }
        LOGF("There were %d kmers missing from HH\n", missingHH)
        delete [] Frequents;    // free memory

#ifdef USE_UPC_ALLOCATOR_IN_MPI
        KmerAllocator::endUPC();
#endif
        if (heavyhitters) {
            heavyhitters->Clear();
            delete heavyhitters;
        }
        heavyhitters = NULL;
        int totalheavyitems, totalmillioncaught;
        int64_t totalheavycounts;
        CHECK_MPI(MPI_Reduce(&millioncaught, &totalmillioncaught, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(&heavyitems, &totalheavyitems, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(&heavycounts, &totalheavycounts, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(&nonerrorkmers, &totalnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(&hashsize, &distinctnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Allreduce(&maxcount, &globalmaxcount, 1, MPI_LONG_LONG, MPI_MAX, MPI_COMM_WORLD));
        double ehh = MPI_Wtime();
        if (myrank == 0) {
            cout << "****** After high freq k-mers are communicated (" << (ehh - shh) << " s)*******" << endl;
            cout << "Total heavy hitters " << totalheavyitems << " for a total of " << totalheavycounts << " counts" << endl;
            cout << "Averaging " << static_cast<double>(totalheavycounts) / static_cast<double>(totalheavyitems) << " count per item" << endl;
            cout << "which caught " << totalmillioncaught << " entries of over million counts" << endl;
            cout << "Kmerscount hash includes " << distinctnonerror << " distinct elements" << endl;
            cout << "Kmerscount non error kmers count is " << totalnonerror << endl;
            cout << "Global max count is " << globalmaxcount << endl;
            ADD_DIAG("%d", "total_heavy_hitters", totalheavyitems);
            ADD_DIAG("%lld", "total_heavy_counts", (lld)totalheavycounts);
            ADD_DIAG("%lld", "distinct_non_error_kmers", (lld)distinctnonerror);
            ADD_DIAG("%lld", "total_non_error_kmers", (lld)totalnonerror);
            ADD_DIAG("%lld", "global_max_count", (lld)globalmaxcount);
        }
    } else {
        SLOG("Skipping heavy hitter counts\n");
    }
#endif // HEAVYHITTERS

    if (globalmaxcount == 0) {
        DIE("There were no kmers found, perhaps your KMER_LENGTH (%d) is longer than your reads?\n", KMER_LENGTH);
    }

    nonerrorkmers = 0;  // reset
    distinctnonerror = 0;
    int64_t overonecount = 0;
    auto itr = kmercounts->begin();
    while (itr != kmercounts->end()) {
#ifdef KHASH
        if (!itr.isfilled()) {
            ++itr; continue;
        }
        int allcount = get<2>(itr.value());
#else
        int allcount = get<2>(itr->second);
#endif
#ifdef HISTOGRAM
        if (allcount <= 0) {
            ++(hist[0]);
        } else if (allcount <= COUNT_THRESHOLD) {
            ++(hist[allcount - 1]);
        } else if (allcount <= COUNT_THRESHOLD_HIGH) {
            ++(hist_high[(allcount - COUNT_THRESHOLD - 1) / HIGH_BIN]);
        } else {
            ++(hist_high[HIGH_NUM_BINS - 1]);
        }
#endif  // HISTOGRAM
        if (allcount < ERR_THRESHOLD) {
            --hashsize;
#ifdef KHASH
            auto newitr = itr;
            ++newitr;
            kmercounts->erase(itr); // amortized constant
            // Iterators, pointers and references referring to elements removed by the function are invalidated.
            // All other iterators, pointers and references keep their validity.
#else
            // C++11 style erase returns next iterator after erased entry
            itr = kmercounts->erase(itr);   // amortized constant
#endif
        } else {
            nonerrorkmers += allcount;
            distinctnonerror++;
            ++itr;
        }
        if (allcount > 1) {
            overonecount += allcount;
        }
    }
#ifdef HISTOGRAM
    hist[0] = kmersprocessed - overonecount;    // over-ride the value for 1, which wasn't correct anyway
#endif

    CHECK_MPI(MPI_Reduce(&nonerrorkmers, &totalnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Reduce(&hashsize, &distinctnonerror, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        cout << "Erroneous count < " << ERR_THRESHOLD << " cases removed " << endl;
        cout << "Kmerscount hash includes " << distinctnonerror << " distinct elements" << endl;
        cout << "Kmerscount non error kmers count is " << totalnonerror << endl;
        ADD_DIAG("%lld", "distinct_non_error_kmers", (lld)distinctnonerror);
        ADD_DIAG("%lld", "total_non_error_kmers", (lld)totalnonerror);
        ADD_DIAG("%lld", "global_max_count", (lld)globalmaxcount);
    }
#ifdef HISTOGRAM
    assert(hist.size() == COUNT_THRESHOLD);
    assert(((int64_t *)hist.data()) + COUNT_THRESHOLD - 1 == &(hist.back()));
    assert(hist_high.size() == HIGH_NUM_BINS);
    assert(((int64_t *)hist_high.data()) + HIGH_NUM_BINS - 1 == &(hist_high.back()));
    if (myrank == 0) {
        CHECK_MPI(MPI_Reduce(MPI_IN_PLACE, hist.data(), COUNT_THRESHOLD, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD));
        CHECK_MPI(MPI_Reduce(MPI_IN_PLACE, hist_high.data(), HIGH_NUM_BINS, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD));

        stringstream ss, ss1, ss2;
        ss << KMER_LENGTH;
        string hname = "histogram_k";
        hname += ss.str();
        string hnamehigh = hname + "_beyond";
        ss1 << COUNT_THRESHOLD;
        hnamehigh += ss1.str();
        hnamehigh += "_binsize";
        ss2 << HIGH_BIN;
        hnamehigh += ss2.str();
        hname += ".txt";
        hnamehigh += ".txt";
        hname = getRankPath(hname.c_str(), -1);
        hnamehigh = getRankPath(hnamehigh.c_str(), -1);
        ofstream hout(hname.c_str());
        int bin = 0;
        for (auto it = hist.begin(); it != hist.end(); it++) {
            hout << ++bin << "\t" << *it << "\n";
        }

        ofstream hhigh(hnamehigh.c_str());
        bin = COUNT_THRESHOLD;
        for (auto it = hist_high.begin(); it != hist_high.end(); it++) {
            hout << bin << "\t" << *it << "\n";
            bin += HIGH_BIN;
        }
    } else {
        CHECK_MPI(MPI_Reduce(hist.data(), NULL, COUNT_THRESHOLD, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD));     // receive buffer not significant at non-root
        CHECK_MPI(MPI_Reduce(hist_high.data(), NULL, HIGH_NUM_BINS, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD));
    }
    if (myrank == 0) {
        cout << "Generated histograms" << endl;
    }
#endif
    MPI_Pcontrol(-1, "HashClean");
}


int ufx_main(int argc, char **argv)
{
    CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &nprocs));
    CHECK_MPI(MPI_Comm_rank(MPI_COMM_WORLD, &myrank));

    double start_time = MPI_Wtime();

    KMER_LENGTH = MAX_KMER_SIZE - 1;
    KMER_PACKED_LENGTH = (MAX_KMER_SIZE - 1 + 3) / 4;

    nonerrorkmers = 0;
    totaltime = 0;
    kmersprocessed = 0;
    readsprocessed = 0;

    bool opt_err = false;
    char *input_fofn = NULL;
    bool cached_io = false;
    bool cached_io_reads = false;
    string base_dir = "/dev/shm";
    string prefix = "";
    bool use_hll = false;
    bool use_hh = true; // use_hll must be true too for HEAVYHITTERS to activate
    bool save_ufx = false;
    bool save_mergraph = false;
    char *firstTimeKmersPath = NULL;

    option_t *this_opt;
    option_t *opt_list = GetOptList(argc, argv, "Q:f:e:p:sB:ScHhE:k:P:X");
    print_args(opt_list, __func__);

    while (opt_list) {
        this_opt = opt_list;
        opt_list = opt_list->next;
        switch (this_opt->option) {
        case 'Q':
            phred_encoding = atoi(this_opt->argument);
            if (phred_encoding != 33 && phred_encoding != 64) {
                SDIE("Please enter a valid phred_encoding (64 or 33) not %d", phred_encoding);
            }
            ignored_ext = phred_encoding + 1; // too low to be a good extension break;
        case 'f':
            input_fofn = strdup(this_opt->argument);
            break;
        case 'e':
            ERR_THRESHOLD = atoi(this_opt->argument);
            if (ERR_THRESHOLD < 0) {
                ERR_THRESHOLD = 0;
            }
            break;
        case 'E':
            extensionCutoff = atoi(this_opt->argument);
            if (extensionCutoff < 0) {
                extensionCutoff = 0;
            }
            break;
        case 'k':
            KMER_LENGTH = atoi(this_opt->argument);
            if (KMER_LENGTH >= MAX_KMER_SIZE) {
                SDIE("Kmer length (%d) must be < %d for this binary.  Please recompile with a larger MAX_KMER_SIZE", KMER_LENGTH, MAX_KMER_SIZE);
            }
            break;
        case 'p':
            prefix = this_opt->argument;
            break;
        case 's':
            cached_io = true;
            break;
        case 'B':
            base_dir = this_opt->argument;
            break;
        case 'X':
            cached_io_reads = true;
            break;
        case 'H':
            use_hll = true;
            break;
        case 'h':
            use_hh = false;
            break;
        case 'S':
            save_ufx = true;
            break;
        case 'c':
            save_mergraph = true;
            break;
        case 'P':
            firstTimeKmersPath = this_opt->argument;
            break;
        default:
            opt_err = true;
        }
    }

    if (opt_err || !input_fofn) {
        if (myrank == 0) {
            cout << "Usage: ./ufx -Q phred_encoding -f filename -e error_threshold <-p prefix> <-s> <-H>" << endl;
            cout << "'phred_encoding' is the offset for the quality scores in the fastq file (33 or 64)" << endl;
            cout << "'filename' is a text file containing the paths to each file to be counted - one on each line -" << endl;
            cout << "'error_threshold' is the lowest number of occurrences of a k-mer for it to be considered a real (non-error) k-mer" << endl;
            cout << "'prefix' is optional and if set, the code prints all k-mers with the prefix" << endl;
            cout << "-s is optional and if set, shared memory will be used to cache all files" << endl;
            cout << "-H is optional and if set, then HyperLogLog will be used to estimate the required bloomfilter" << endl;
            cout << "-P firstTimeKmersPath: the optional path (default $TMPDIR) to store a very large firstTimeKmersr file -- reduces exchange by 1" << endl;
        }
        return 0;
    }

    if (myrank == 0) {
        cout << "HipMER k-mer characterization (ufx generation) step" << endl;
        cout << "You are running with the following settings:" << endl;
        cout << "K-mer length = " << KMER_LENGTH << endl;
        cout << "Max k-mer length (internal) = " << MAX_KMER_SIZE << endl;
        cout << "Using " << HASHMAP_MESSAGE << " with " << ALLOCATOR_MESSAGE << " for hash tables" << endl;
        cout << "Phred encoding = " << (int) phred_encoding << endl;
    }
    if (cached_io && save_ufx) {
       if (myrank == 0) {
           cout << "Since cached_io is true, not saving the aggregated file into intermediates.\n\tThat checkpoint will now happen in the next stage in the background" << endl;
        }
        save_ufx = false;
    }

    double t01 = MPI_Wtime();

    vector<filedata> allfiles = GetFiles(input_fofn);
    Kmer::set_k(KMER_LENGTH);
    double cardinality;
    if (use_hll) {
#ifdef HEAVYHITTERS
        if (use_hh) {
            heavyhitters = new HeavyHitters(MAXHITTERS);
        }
#endif
        ProudlyParallelCardinalityEstimate(allfiles, cardinality, cached_io_reads, base_dir.c_str());   // doesn't update kmersprocessed yet (but updates readsprocessed)
    } else {
        // just estimate the cardinality using fastq size and sampling.
        int64_t mySums[3] = { 0, 0, 0 };
        int64_t &myCardinality = mySums[0];
        int64_t &myTotalReads = mySums[1];
        int64_t &myTotalBases = mySums[2];
        for (int i = 0; i < allfiles.size(); i++) {
            if (MurmurHash3_x64_64((char *)&i, sizeof(int)) % nprocs == myrank) {
                int64_t totalReads, totalBases;
                int64_t fileSize = estimate_fq(allfiles[i].filename, cached_io_reads, base_dir.c_str(), 10000, &totalReads, &totalBases);
                myTotalReads += totalReads;
                myTotalBases += totalBases;
                if (totalReads > 0) {
                    int64_t kmersPerRead = ((totalBases + totalReads - 1) / totalReads) - KMER_LENGTH + 1;
                    myCardinality += kmersPerRead * totalReads;
                    cout << "Cardinality for " << allfiles[i].filename << ": " << myCardinality << endl;
                }
            }
        }
        MPI_Allreduce(MPI_IN_PLACE, mySums, 3, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
        if (myrank == 0) {
            cout << "Estimated our cardinality: " << myCardinality << " totalReads: " << myTotalReads << " totalBases: " << myTotalBases << endl;
        }
        // baseline of 10M kmers
        if (myCardinality < 10000000) {
            myCardinality = 10000000;
        }
        // assume maximum of 90% of the kmers are unique, because at least some have to repeat...
        cardinality = 0.9 * myCardinality / (double)nprocs;
    }
#ifndef HIPMER_BLOOM64
    if (cardinality > 1L << 31) {
        cout << "Reduced cardinality to fit within int32_t" << endl;
        cardinality = 1L << 31 - 1L;
    }
#endif
    double t02 = MPI_Wtime();
    serial_printf("Stage 1 - Cardinality: %0.3f s\n", t02 - t01);

    // initialize and reserve kmer counts
#if UPC_ALLOCATOR
    SLOG("Using upc-shared memory for kmercounts and bloom\n");
#endif
#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::startUPC();
#endif
    assert(kmercounts == NULL);
    kmercounts = new KmerCountsType();
    uint64_t myReserve = cardinality * 0.1 + 64000;     // assume we have at least 10x depth of kmers to store and a miniuum 64k
    if (myReserve >= 4294967291ul) {
        myReserve = 4294967290ul;                       // maximum supported by khash
    }
#ifdef KHASH
    LOGF("Reserving %lld entries in KHASH for cardinality %lld\n", (lld)myReserve, (lld)cardinality);
    kmercounts->reserve(myReserve);
#else
    //LOGF("NOT Reserving %lld entries in std::map for cardinality %lld\n", (lld) myReserve, (lld) cardinality);
    LOGF("Reserving %lld entries in VectorMap for cardinality %lld\n", (lld)myReserve, (lld)cardinality);
    kmercounts->reserve(myReserve);
#endif
    DBG("Reserved kmercounts\n");

#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::endUPC();
#endif


    // only one pass is necessary if firstTimeKmers are stored
    ProcessFiles(allfiles, 1, cardinality, cached_io_reads, base_dir.c_str(), firstTimeKmersPath);  // determine final hash-table entries using bloom filter
    double t03 = MPI_Wtime();
    serial_printf("Stage 2 - Exchange and Insert%s: %0.3f\n", ((firstTimeKmersPath == NULL) ? "" : " and Count"), t03 - t02);

    CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
    double t04;
    if (firstTimeKmersPath == NULL) {
        // perform pass 2
        ProcessFiles(allfiles, 2, cardinality, cached_io_reads, base_dir.c_str(), NULL);
        t04 = MPI_Wtime();
        serial_printf("Stage 3 Count %0.3f s\n", t04 - t03);
    } else {
        t04 = MPI_Wtime();
        serial_printf("Stage 3 (noop): %0.3f s\n", t04 - t03);
    }

    int64_t sendbuf = kmercounts->size();
    int64_t recvbuf, totcount, maxcount;
    CHECK_MPI(MPI_Exscan(&sendbuf, &recvbuf, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Allreduce(&sendbuf, &totcount, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD));
    CHECK_MPI(MPI_Allreduce(&sendbuf, &maxcount, 1, MPI_LONG_LONG, MPI_MAX, MPI_COMM_WORLD));
    int64_t totkmersprocessed;
    CHECK_MPI(MPI_Reduce(&kmersprocessed, &totkmersprocessed, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD));
    if (myrank == 0) {
        double imbalance = static_cast<double>(nprocs * maxcount) / static_cast<double>(totcount);
        cout << "Load imbalance for final k-mer counts is " << imbalance << endl;
        cout << "CardinalityEstimate " << static_cast<double>(totkmersprocessed) / (MEGA * max((t02 - t01), 0.001) * nprocs) << " MEGA k-mers per sec/proc in " << (t02 - t01) << " seconds" << endl;
        cout << "Bloom filter + hash pre-allocation  " << static_cast<double>(totkmersprocessed) / (MEGA * max((t03 - t02), 0.001) * nprocs) << " MEGA k-mers per sec/proc in " << (t03 - t02) << " seconds" << endl;
        cout << "Actual hash-based counting  " << static_cast<double>(totkmersprocessed) / (MEGA * max((t04 - t03), 0.001) * nprocs) << " MEGA k-mers per sec/proc in " << (t04 - t03) << " seconds" << endl;
    }
    CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
    double t05 = MPI_Wtime();
    serial_printf("Stage 4 - loadImbalance: %0.3f s\n", t05 - t04);
    vector<filedata>().swap(allfiles);
    string countfile = string(input_fofn) + ".ufx.bin";
#ifdef UFX_WRITE_SINGLE_FILE
    string intermediate_countfile = string("intermediates/") + countfile;
#endif


#ifndef BENCHMARKONLY
    if (myrank == 0) {
        cout << "Writing to binary via MPI-IO" << endl;
    }

    MPI_File thefile;
    int64_t lengthuntil;


#ifdef UFX_WRITE_SINGLE_FILE
    MPI_Datatype datatype;
    CHECK_MPI(MPI_Type_contiguous(sizeof(ufxpack), MPI_UNSIGNED_CHAR, &datatype));
    CHECK_MPI(MPI_Type_commit(&datatype));
    int dsize;
    CHECK_MPI(MPI_Type_size(datatype, &dsize));
    assert(dsize == sizeof(ufxpack));
#endif

    uint64_t ufxcount = kmercounts->size();
    uint64_t ufxcountBatch = (1L<<29) / MYSV.cores_per_node / sizeof(ufxpack); // 512 MB of node memory per batch
    uint64_t numBatches = ((totcount + ufxcountBatch - 1)  / ufxcountBatch + nprocs - 1) / nprocs;
    assert(ufxcount >= numBatches);
    ufxcountBatch = ufxcount / numBatches - 1; // remainder forced here so all threads have a final write
    assert(ufxcountBatch > 0);
#ifdef UFX_WRITE_SINGLE_FILE
    if (save_ufx || !cached_io) {
        if (myrank == 0) {
            // Unlink first (if it exists do not check error status)
            MPI_File_delete((char *)intermediate_countfile.c_str(), MPI_INFO_NULL);
            // now try to make the intermediates directory, in case it doesn't exist
            mkdir("intermediates", S_IRWXU);
        }
        SLOG("Creating the ufx file in %lld + 1 batches: %s\n", numBatches, intermediate_countfile.c_str());
        CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
        CHECK_MPI(MPI_File_open(MPI_COMM_WORLD, (char *)intermediate_countfile.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &thefile));
        if (myrank == 0) {
            lengthuntil = 0;
        } else {
            lengthuntil = recvbuf;
        }

        SLOG("Eventual total file size: %0.3f MB\n", 1.0 * totcount * sizeof(ufxpack) / ONE_MB);
        CHECK_MPI(MPI_File_set_size(thefile, totcount * sizeof(ufxpack)));
        SLOG("Setting file views\n");
        // write a single global file
        int mpi_err = MPI_File_set_view(thefile, lengthuntil * dsize, datatype, datatype, (char *)"external32", MPI_INFO_NULL);
        if (mpi_err == 51) {
            // external32 datarep is not supported, use native instead
            CHECK_MPI(MPI_File_set_view(thefile, lengthuntil * dsize, datatype, datatype, (char *)"native", MPI_INFO_NULL));
        } else {
            CHECK_MPI(mpi_err);
        }
        SLOG("Set file views\n");
    }
#endif
    GZIP_FILE UFX_f = NULL;
    int64_t ufxcount64bit = static_cast<int64_t>(ufxcount);
    char fname[MAX_FILE_PATH];
    fname[0] = '\0';
#ifdef UFX_WRITE_SINGLE_FILE
    if (cached_io) {
#endif
    {
        stringstream ss;
        string rank;
        ss << myrank;
        ss >> rank;
        unsigned found = countfile.find_last_of("/");
        countfile = countfile.substr(found + 1);

        sprintf(fname, "%s/%s" GZIP_EXT, base_dir.c_str(), countfile.c_str());
        get_rank_path(fname, myrank);

        LOGF("Creating ufx file: %s\n", fname);
        UFX_f = GZIP_OPEN(fname, "wb");
    }

    ofstream *mergraph = NULL;
    char mergraphfilepath[MAX_FILE_PATH];
    snprintf(mergraphfilepath, MAX_FILE_PATH, "%s.mergraph.%d.txt", countfile.c_str(), myrank);
    char *mergraphfile = get_rank_path(mergraphfilepath, myrank);
#ifdef DEBUG
    mergraph = new ofstream(mergraphfile);
#endif
    if (!mergraph && save_mergraph) {
        // output the mergraph text file too
        mergraph = new ofstream(mergraphfile);
    }

    SLOG("Packed UFX count and size: %lld %0.3f MB (%0.3f MB per batch)\n", (lld) ufxcount, 1.0 / ONE_MB * ufxcount * sizeof(ufxpack), 1.0 / ONE_MB * ufxcountBatch * sizeof(ufxpack));
    ufxpack *packed = new ufxpack[ufxcountBatch];
    memset(packed, 0, ufxcountBatch * sizeof(ufxpack));

    // pack in batches, not the whole thing at nearly double the memory!
    SLOG("Packing UFX data\n");
    uint64_t packedIdx = 0;
    uint64_t fileIdx = 0;
    for (auto itr = kmercounts->begin(); itr != kmercounts->end(); ++itr) {
#ifdef KHASH
        if (!itr.isfilled()) {
            continue;
        }
        auto key = itr.key();
        auto val = itr.value();
#else
        auto key = itr->first;
        auto val = itr->second;;
#endif

        Kmer kmer(key);
        ufxpack upack;
        upack.arr = kmer.getArray();
        PackIntoUFX(get<0>(val), get<1>(val), get<2>(val), upack);
        CHECK_BOUNDS(packedIdx, ufxcount);
        packed[packedIdx++] = upack;
        if (mergraph) {
            *mergraph << kmer.toString() << "\t" << get<0>(val)[0] << "\t" << get<0>(val)[1] << "\t" << get<0>(val)[2] << "\t" << get<0>(val)[3] << "\t0\t" << (get<2>(val) - get<0>(val)[0] - get<0>(val)[1] - get<0>(val)[2] - get<0>(val)[3]) << "\t" << get<1>(val)[0] << "\t" << get<1>(val)[1] << "\t" << get<1>(val)[2] << "\t" << get<1>(val)[3] << "\t0\t" << (get<2>(val) - get<1>(val)[0] - get<1>(val)[1] - get<1>(val)[2] - get<1>(val)[3]) << "\t0" << endl;
        }
        if (packedIdx == ufxcountBatch) {
            // write a batch
#ifdef UFX_WRITE_SINGLE_FILE
            if (save_ufx || !cached_io) {
                int64_t size = packedIdx;
                MPI_Status status;
                LOGF("Writing %lld of %lld from idx %lld\n", (lld)size, (lld)ufxcount, (lld)fileIdx);
                CHECK_MPI(MPI_File_write(thefile, packed, size, datatype, &status));
                int count;
                CHECK_MPI(MPI_Get_count(&status, datatype, &count));
                if (size != count) DIE("Could not write the entire %lld bytes (%lld returned)\n", (lld) size, (lld) count);
                fileIdx += size;
            }
            if (cached_io)
#endif
            {
                int64_t bytes2write = packedIdx * sizeof(ufxpack);
                int64_t bytesWritten = 0;
                int64_t maxWrite = 16777216;
                // write in batches smaller than 16MB to prevent zlib overflows and too much memory consumed
                while (bytes2write > bytesWritten) {
                    int64_t remaining = bytes2write - bytesWritten;
                    assert(remaining > 0);
                    if (remaining > maxWrite) {
                        remaining = maxWrite;
                    }
                    int64_t wroteBytes = GZIP_FWRITE(((char *)packed) + bytesWritten, 1, remaining, UFX_f);
                    if (wroteBytes != remaining) {
                        DIE("Could not write %lld bytes (out of %lld) to UFX file %s... Write %lld! %s\n", (lld)remaining, (lld)bytes2write, fname, (lld)wroteBytes, strerror(errno));
                    }
                    bytesWritten += wroteBytes;
                }
                if (bytesWritten != bytes2write) {
                    DIE("Could not write the full number of bytes to file %s: expected %lld, got %lld! %s\n", fname, (lld)bytes2write, (lld)bytesWritten, strerror(errno));
                }
            }
            packedIdx = 0;
        } 
    }
    // write final batch
#ifdef UFX_WRITE_SINGLE_FILE
    if (save_ufx || !cached_io) {
        if (true) { // always write collectively
            int64_t size = packedIdx;
            MPI_Status status;
            LOGF("Writing %lld of %lld from idx %lld\n", (lld)size, (lld)ufxcount, (lld)fileIdx);
            CHECK_MPI(MPI_File_write_all(thefile, packed, size, datatype, &status));
            int count;
            CHECK_MPI(MPI_Get_count(&status, datatype, &count));
            if (size != count) DIE("Could not write the entire %lld bytes (%lld returned)\n", (lld) size, (lld) count);
            fileIdx += size;
        }
    }
    if (cached_io)
#endif
    {
         if (packedIdx > 0) { 
            int64_t bytes2write = packedIdx * sizeof(ufxpack);
            int64_t bytesWritten = 0;
            int64_t maxWrite = 16777216;
            // write in batches smaller than 16MB to prevent zlib overflows and too much memory consumed
            while (bytes2write > bytesWritten) {
                int64_t remaining = bytes2write - bytesWritten;
                assert(remaining > 0);
                if (remaining > maxWrite) {
                    remaining = maxWrite;
                }
                int64_t wroteBytes = GZIP_FWRITE(((char *)packed) + bytesWritten, 1, remaining, UFX_f);
                if (wroteBytes != remaining) {
                    DIE("Could not write %lld bytes (out of %lld) to UFX file %s... Write %lld! %s\n", (lld)remaining, (lld)bytes2write, fname, (lld)wroteBytes, strerror(errno));
                }
                bytesWritten += wroteBytes;
            }
            if (bytesWritten != bytes2write) {
                DIE("Could not write the full number of bytes to file %s: expected %lld, got %lld! %s\n", fname, (lld)bytes2write, (lld)bytesWritten, strerror(errno));
            }
        }
        packedIdx = 0;
    }

    if (mergraph) {
        delete mergraph;           // close the file
    }
    // explicitly free / destruct kmercounts memory now
//    SLOG("Deallocating kmercount memory: %lld\n", (lld) kmercounts->size());
//    long totMem = sizeof(kmercounts) + kmercounts->size() * (sizeof(MERARR) + sizeof(KmerCountType));
//    WARN("Deallocating kmercount memory: %lld (%ld %.2f M)\n", (lld) kmercounts->size(),
//         totMem, (double)totMem / 1024 / 1024);
    LOGF("Deallocating kmercount memory (%lld entries)\n", (lld)kmercounts->size());

    // Free kmercounts - protect upc allocations if necessary
#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::startUPC();
#endif

#ifdef KHASH
    kmercounts->free();
#else
    kmercounts->clear();
    delete kmercounts;
    kmercounts = NULL;
#endif

#ifdef USE_UPC_ALLOCATOR_IN_MPI
    KmerAllocator::endUPC();
#endif

    DBG2("Deallocated kmercount memory\n");

#ifdef UFX_WRITE_SINGLE_FILE
    if (save_ufx || !cached_io) {
        // write in batches smaller than 2GB per node, assuming maximum of 256 cores in a node
        CHECK_MPI(MPI_File_close(&thefile));
        if (myrank == 0) {
            link_chk(intermediate_countfile.c_str(), countfile.c_str());
        }
    }
    CHECK_MPI(MPI_Type_free(&datatype));
    if (cached_io)
#endif
    {
        GZIP_CLOSE(UFX_f);

        // now write the total size to file - because we cannot use the mpi functions in
        // readufx.cpp which is used by numerous upc stages to read in the ufx binary data
        strcat(fname, ".entries");
        FILE *f = fopen_chk(fname, "w");
        fprintf(f, "%lld\n", (lld)ufxcount64bit);
        fclose(f);
    }

    double t06 = MPI_Wtime();
    serial_printf("Stage 6 - FileIO: %0.3f s\n", t06 - t05);

    if (myrank == 0) {
        cout << "File write completed\n";
    }
    {
        if (prefix != "") {
            string sanity = "ufx_";
            stringstream ss;
            ss << myrank;
            sanity += ss.str();
            sanity += string(".");
            sanity += prefix;
            ofstream outsanity(sanity.c_str());

            int prelen = prefix.length();
            for (int i = 0; i < ufxcount; ++i) {
                Kmer kmer(packed[i].arr);
                string kstr = kmer.toString();
                if (kstr.substr(0, prelen) == prefix) {
                    outsanity << kstr << "\t" << packed[i].count << "\t" << packed[i].left << "[" << packed[i].leftmin << "," << packed[i].leftmax;
                    outsanity << "]" << packed[i].right << "[" << packed[i].rightmin << "," << packed[i].rightmax << "]" << endl;
                }
            }
            outsanity.close();
        }
        if (myrank == 0) {
            cout << "Finished writing, here is the top of processor 0's data" << endl;
            for (int i = 0; i < 10 && i < ufxcount; ++i) {
                Kmer kmer(packed[i].arr);
                cout << kmer.toString() << "\t" << packed[i].count << "\t" << packed[i].left << "[" << packed[i].leftmin << "," << packed[i].leftmax;
                cout << "]" << packed[i].right << "[" << packed[i].rightmin << "," << packed[i].rightmax << "]" << endl;
            }
        }
    }
    delete [] packed;
#endif // #ifndef BENCHMARKONLY
    double t07 = MPI_Wtime();
    serial_printf("Stage 7 - shmem storage: %0.3f s\n", t07 - t06);

    double elapsed_time = MPI_Wtime() - start_time;
    double slowest_time;
    CHECK_MPI(MPI_Allreduce(&elapsed_time, &slowest_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD));

    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]), slowest_time);

    // barrier to ensure everything is done before transitioning to upc
    CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));

    return 0;
}


#ifndef SINGLE_EXEC

int main(int argc, char **argv)
{
    CHECK_MPI(MPI_Init(&argc, &argv));
    OPEN_MY_LOG("ufx");
    int ret = ufx_main(argc, argv);
    MPI_Finalize();
    return ret;
}


#endif
