#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <array>
#include <fstream>
#include <chrono>
#include <iomanip>

#include "zstr.hpp"
#include "readufx.h"
#include "common_rankpath.h"

using std::string;
using std::istringstream;
using std::ostringstream;
using std::array;
using std::cout;
using std::cerr;
using std::endl;
using std::flush;
using std::pair;
using std::make_pair;

struct ufx_file_t {
    zstr::ifstream *zf;
};

ufx_file_t *UFXInitOpen(char *filename, int64_t *myshare, int my_rank, const char * base_dir, int kmer_length)
{
    char full_fname[MAX_FILE_PATH*2+10];
    sprintf(full_fname, "%s/%s.gz", base_dir, filename);
    get_rank_path(full_fname, my_rank);
    ufx_file_t *UFX_f = new ufx_file_t;
    UFX_f->zf = new zstr::ifstream(full_fname);
    if (!UFX_f->zf) return NULL;
    // need to get the number of entries from file
    *myshare = 0;
    strcat(full_fname, ".entries");
    std::ifstream entries_f(full_fname);
    int64_t num = 0;
    while (entries_f >> num) {
        //if (!my_rank) cerr << num << endl << flush;
        *myshare += num;
    }
    return UFX_f;
}

void UFXClose(ufx_file_t *UFX_f)
{
    delete UFX_f->zf;
    delete UFX_f;
}

// inputs: f, dsize, requestedkmers, dmin
// outputs: kmersarr, counts, lefts, rights
// returns: number of k-mers read (can be less than requestedkmers if end of file) (-1 for error)
int64_t UFXRead(char ***kmersarr, int **counts, char **lefts, char **rights, int64_t requestedkmers, int dmin,
                double errorRate, int reuse, int my_rank, int kmer_length, ufx_file_t *UFX_f)
{
    if (!UFX_f) {
        cerr << "Thread " << my_rank << ": Problem reading ufx input file - no struct" << endl;
        return -1;
    }
    if (requestedkmers == 0) return 0;
    auto t = std::chrono::high_resolution_clock::now();
    // allocate the data structures
    if (!reuse) { // OK in the last iteration too because the invariant (totread <= requestedkmers) holds
        // (*kmersarr) is of type char**
        (*kmersarr) = (char **)malloc(requestedkmers * sizeof(char*));
        if (!(*kmersarr)) {
            cerr << "Thread " << my_rank << ": Could not allocate memory for " << requestedkmers << " kmers" << endl;
            return -1;
        }
        for (int64_t i = 0; i < requestedkmers; i++) {
            (*kmersarr)[i] = (char *)malloc(kmer_length + 1); // extra character for NULL termination
            if (!(*kmersarr)[i]) {
                cerr << "Thread " << my_rank << ": Could not allocate memory for kmer " << i << endl;
                return -1;
            }
        }
        *counts = (int *)malloc(sizeof(int) * requestedkmers);
        *lefts = (char *)malloc(requestedkmers);
        *rights = (char *)malloc(requestedkmers);
        if (!(*counts) || !(*lefts) || !(*rights)) {
            cerr << "thread " << my_rank << ": Could not allocate memory for 3 * " << requestedkmers << " kmer metadata" << endl;
            return -1;
        }
    }
    // fill the data structures
    string line;
    int64_t kmer_i = 0;
    int64_t ten_perc = requestedkmers / 10;
    while (getline(*(UFX_f->zf), line)) {
        istringstream iss(line);
        iss >> (*kmersarr)[kmer_i] >> (*counts)[kmer_i] >> (*lefts)[kmer_i] >> (*rights)[kmer_i];
        kmer_i++;
        if (!my_rank && !(kmer_i % ten_perc)) cout << (10 * kmer_i / ten_perc) << "% " << kmer_i << endl << flush;
    }
    if (kmer_i != requestedkmers) {
        cerr << "Warning: thread " << my_rank << " could not read all the requested " << requestedkmers << " ufx entries; read " << kmer_i << endl;
        return -1;
    }
    std::chrono::duration<double> t_elapsed = std::chrono::high_resolution_clock::now() - t;
    if (!my_rank) cout << "Elapsed time for reading ufx: " << std::setprecision(2) << std::fixed << t_elapsed.count() << " s\n";
    return kmer_i;
}

static array<pair<char, int>, 4> get_sorted_counts(int *counts)
{
    array<pair<char, int>, 4> counts_pairs = {make_pair('A', counts[0]), make_pair('C', counts[1]),
                                              make_pair('G', counts[2]), make_pair('T', counts[3])};
    sort(std::begin(counts_pairs), std::end(counts_pairs),
         [](const auto &elem1, const auto &elem2) {
             if (elem1.second == elem2.second) return elem1.first > elem2.first;
             else return elem1.second > elem2.second;
         });
    return counts_pairs;
}

int writeNewUFX(char *filename, int *merDepths, char *kmersAndExts, int64_t nNewEntries, int my_rank, const char * base_dir,
                int *leftCounts, int *rightCounts, int kmer_len, int min_depth_cutoff, double dynamic_min_depth)
{
    char my_fname[MAX_FILE_PATH];
    sprintf(my_fname, "%s/%s.gz", base_dir, filename);
    get_rank_path(my_fname, my_rank);
    zstr::ofstream ufx_ofs(my_fname, std::ios::out | std::ios::app);

    int64_t kmers_pos = 0;
    int64_t counts_pos = 0;
    ostringstream out_buf;
    for (int64_t i = 0; i < nNewEntries; i++) {
        int count = merDepths[i];
        char left = kmersAndExts[kmers_pos + kmer_len];
        char right = kmersAndExts[kmers_pos + kmer_len + 1];
        char *kmer = &kmersAndExts[kmers_pos];
        kmer[kmer_len] = '\0';
        auto sorted_counts_left = get_sorted_counts(&(leftCounts[counts_pos]));
        auto sorted_counts_right = get_sorted_counts(&(rightCounts[counts_pos]));
        int leftmax = sorted_counts_left[0].second;
        int leftmin = sorted_counts_left[1].second;
        int rightmax = sorted_counts_right[0].second;
        int rightmin = sorted_counts_right[1].second;
        int dmin_dyn = (1.0 - dynamic_min_depth) * count;
        if (dmin_dyn < min_depth_cutoff) dmin_dyn = min_depth_cutoff;
        if (leftmax < dmin_dyn) left = 'X';
        else if (leftmin >= dmin_dyn) left = 'F';
        if (rightmax < dmin_dyn) right = 'X';
        else if (rightmin >= dmin_dyn) right = 'F';
        out_buf << kmer << " " << count << " " << left << " " << right << endl;
        kmers_pos += kmer_len + 2;
        counts_pos += 4;
        if (!(i % 1000)) {
            ufx_ofs << out_buf.str();
            out_buf.str("");
        }
    }
    if (!out_buf.str().empty()) ufx_ofs << out_buf.str();
    return 0;
}

void DeAllocateAll(char ***kmersarr, int **counts, char **lefts, char **rights, int64_t initialread)
{
    for (int64_t i = 0; i < initialread; i++) free((*kmersarr)[i]);
    if (*kmersarr) free(*kmersarr);
    if (*counts) free(*counts);
    if (*lefts) free(*lefts);
    if (*rights) free(*rights);
}
