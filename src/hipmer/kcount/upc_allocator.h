#ifndef _UPC_ALLOCATOR_H
#define _UPC_ALLOCATOR_H

#ifdef __cplusplus
extern "C" {
#endif

extern char *upc_new(uint64_t sz);
extern void upc_delete(char *ptr);

#ifdef __cplusplus
}
#endif

#endif
