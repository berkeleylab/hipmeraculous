#ifndef _READUFX_H_
#define _READUFX_H_

#include <zlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ufx_file_t;

struct ufx_file_t *UFXInitOpen(char *filename, int64_t *myshare, int my_rank, const char * base_dir, int kmer_length);
void UFXClose(struct ufx_file_t *f);
int64_t UFXRead(char ***kmersarr, int **counts, char **lefts, char **rights, int64_t requestedkmers, int dmin,
                double errorRate, int reuse, int myrank, int kmer_length, struct ufx_file_t *f);
void DeAllocateAll(char ***kmersarr, int **counts, char **lefts, char **rights, int64_t initialread);
int writeNewUFX(char *filename, int *merDepths, char *kmersAndExts, int64_t nNewEntries, int my_rank, const char * base_dir,
                int *leftCounts, int *rightCounts, int kmer_length, int min_depth_cutoff, double dynamic_min_depth);

#ifdef __cplusplus
}
#endif
#endif
