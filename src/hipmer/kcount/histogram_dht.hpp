#ifndef __HISTOGRAM_DHT
#define __HISTOGRAM_DHT

#include <limits>
#include <iostream>
#include <map>
#include <deque>
#include <fstream>
#include <stdarg.h>
#include <upcxx/upcxx.hpp>
#include "zstr.hpp"

#include "bytell_hash_map.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"
#include "upcxx_utils/two_tier_aggr_store.hpp"

using namespace upcxx_utils;

using std::vector;
using std::deque;
using std::pair;
using std::ostream;
using std::ostringstream;
using std::sort;
using std::numeric_limits;
using std::make_shared;
using std::make_pair;
using std::shared_ptr;
using std::swap;
using std::array;

using upcxx::intrank_t;
using upcxx::rank_me;
using upcxx::rank_n;
using upcxx::barrier;
using upcxx::dist_object;
using upcxx::dist_id;
using upcxx::reduce_one;
using upcxx::reduce_all;
using upcxx::op_fast_add;
using upcxx::op_fast_max;
using upcxx::progress;
using upcxx::make_view;
using upcxx::view;

class HistogramDHT {
private:
    
  //using hist_map_t = ska::bytell_hash_map<uint64_t, uint64_t>; // distributed hash to store all counts
  using hist_map_t = std::unordered_map<uint64_t, uint64_t>; // distributed hash to store all counts
  using bucket_entry_t = pair<uint64_t, uint64_t>;

  static const uint64_t HIGH_COUNT = 512;
  using cache_vector_t = vector<uint64_t>; // local cache to store the low order counts < HIGH_COUT before a global reduction

  hist_map_t hist_map;
  cache_vector_t cache_vector;

  intrank_t get_hist_target_rank(uint64_t bucket) {
    return std::hash<uint64_t>{}(bucket) % rank_n();
  }

  class InsertOrAddHistEntry {
    hist_map_t &hist_map;
  public:
    InsertOrAddHistEntry(hist_map_t &hm) : hist_map(hm) {}
    void operator()(bucket_entry_t &elem) {
        if (elem.second == 0) return; // do nothing
        // find it - if it isn't found then insert it, otherwise increment the counts
        const auto it = hist_map.find(elem.first);
        if (it == hist_map.end()) {
            hist_map.insert(elem);
        } else {
            assert(it->first == elem.first);
            it->second += elem.second;
        }
    }
  };
  dist_object<InsertOrAddHistEntry> insert_or_add_hist_entry;
  using hist_store_t = TwoTierAggrStore< dist_object<InsertOrAddHistEntry>, bucket_entry_t >;
  hist_store_t hist_store;
  
  void add_low_counts() {
    BarrierTimer timer(__func__);
    for(int i = 0 ; i < HIGH_COUNT; i++) {
      if (cache_vector[i] > 0) {
        add_count(i, cache_vector[i], true);
        cache_vector[i] = 0;
      }
    }
    // implicit barrier() when BarrierTimer is destroyed //
  }

public:

  HistogramDHT() : hist_map(), insert_or_add_hist_entry(InsertOrAddHistEntry(hist_map)), hist_store(insert_or_add_hist_entry, "histogram") {
#ifdef DEBUG
    hist_store.set_size(0); // force no store for testing
#else
    hist_store.set_size(4*1024*1024);
#endif
    cache_vector.resize(HIGH_COUNT, 0);
  }

  void clear() {
    for (auto it = hist_map.begin(); it != hist_map.end(); ) {
      it = hist_map.erase(it);
    }
    hist_map.clear();
    cache_vector.clear();
    cache_vector.resize(HIGH_COUNT, 0);
  }
  
  ~HistogramDHT() {
    clear();
  }

  void add_count(uint64_t bucket, uint64_t count = 1, bool forcePush = false) {
    // get the lexicographically smallest
    if (bucket < HIGH_COUNT && !forcePush) {
        cache_vector[bucket] += count;
        return;
    }
    auto target_rank = get_hist_target_rank(bucket);
    bucket_entry_t entry = {bucket, count};
    hist_store.update(target_rank, entry);
  }

  void flush_updates() {
    BarrierTimer timer(__func__);
    add_low_counts();
    hist_store.flush_updates();
    // implicit barrier() when BarrierTimer is destroyed //
  }

  // one file output by process 0
  // one line per bucket\tcount
  void write_histogram(string hist_fname) {
    BarrierTimer timer(__func__);
    uint64_t myMaxBucket = 0;
    for (auto elem : hist_map) {
        if (elem.first > myMaxBucket) myMaxBucket = elem.first;
    }
    
    uint64_t maxBucket = reduce_one(myMaxBucket, op_fast_max, 0).wait();
    SOUT("Allocating histogram with ", maxBucket+1, " entries on rank0 using ", get_size_str((maxBucket+1) * sizeof(int64_t)), "\n");
    upcxx::dist_object< vector<uint64_t> > sortedHistogram(upcxx::world());
    if (rank_me() == 0) {
        sortedHistogram->resize(maxBucket+1, 0);
    }
    // serialize local maps into a vector of non-zero map entries
    vector< bucket_entry_t > entries;
    entries.reserve( hist_map.size() );
    size_t num_nonzero = 0;
    for (auto elem : hist_map) {
        if (elem.second > 0) {
            entries.push_back( elem );
            num_nonzero++;
        }
    }
    LOG("myMaxBucket=", myMaxBucket, " ", hist_map[myMaxBucket], ", num_nonzero=", num_nonzero, "\n");

    // set the values it the array on rank 0
    auto fut = rpc(
        0,
        [](upcxx::dist_object< vector<uint64_t> > &sh, view< bucket_entry_t > entries) {
           auto & hist = *sh;
           if (hist.empty()) DIE("!\n");
           for( auto & entry : entries ) {
               if (entry.first >= hist.size()) DIE("entry out of bounds!\n");
               auto &val = hist[ entry.first ];
               if (val != 0) DIE("Detected duplicate for entry ", entry.first, "\n");
               val = entry.second;
           }
           return make_future(); // ensure all RPCs have completed
        }, sortedHistogram, make_view(entries.begin(), entries.end()));
    fut.wait();
    upcxx::barrier();

    DBG("Finished updating\n");
    if (rank_me() == 0) {
        SLOG("Writing to ", hist_fname, "\n");
        ofstream hist_file(hist_fname);
        auto sh = *sortedHistogram;
        assert(sh.size() == maxBucket+1);
        for( uint64_t i = 1; i < sh.size(); i++) {
            hist_file << i << "\t" << sh[i] << "\n";
        }
        hist_file.close();
        sortedHistogram->clear();
    }
    // implicit barrier() when BarrierTimer is destroyed //
  }
};

#endif
