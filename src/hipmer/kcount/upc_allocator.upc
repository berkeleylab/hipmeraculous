#include <assert.h>
#include <upc.h>


#include "upc_allocator.h"
#include "upc_common.h"

extern char *upc_new(uint64_t sz) 
{
  shared [] char* gp = NULL;
  UPC_ALLOC_CHK(gp, sz);
  assert(gp);
  assert(upc_threadof(gp) == MYTHREAD);
  char *lp = (char*)gp;
  assert(lp);
  return lp;
}

extern void upc_delete(char *ptr) 
{
  assert(ptr);
  shared [] char* gp = bupc_inverse_cast(ptr);
  assert(gp);
  assert(upc_threadof(gp) == MYTHREAD);
  UPC_FREE_CHK(gp);
}
