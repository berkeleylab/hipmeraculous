#ifndef _UPC_ALLOCATOR_HPP
#define _UPC_ALLOCATOR_HPP

#include "upc_allocator.h"


static int64_t upc_mem_alloced = 0, upc_mem_freed = 0, upc_mem_peak = 0;

template <typename T>
struct UPCAllocator 
{
  typedef uint64_t size_type;
  typedef ptrdiff_t difference_type;
  typedef T* pointer;
  typedef const T* const_pointer;
  typedef T& reference;
  typedef const T& const_reference;
  typedef T value_type;

  UPCAllocator() {}

  ~UPCAllocator() {}

  template <class U> struct rebind { typedef UPCAllocator<U> other; };
  template <class U> UPCAllocator(const UPCAllocator<U>&){}

  pointer address(reference x) const {return &x;}
  const_pointer address(const_reference x) const {return &x;}
  size_type max_size() const throw() {return size_type(-1) / sizeof(value_type);}

  pointer allocate(size_type n) {
    upc_mem_alloced += n * sizeof(T);
    auto mem_diff = upc_mem_alloced - upc_mem_freed;
    if (mem_diff > upc_mem_peak) upc_mem_peak = mem_diff;
    return (pointer)(upc_new(n * sizeof(T)));
  }

  void deallocate(pointer p, size_type n) {
    upc_mem_freed += n * sizeof(T);
    upc_delete((char*)p);
  }

  void construct(pointer p, const T& val) {
    new(static_cast<void*>(p)) T(val);
  }

  void construct(pointer p) {
    new(static_cast<void*>(p)) T();
  }

  void destroy(pointer p) {
    p->~T();
  }

};

template <typename T>
bool operator!=(const UPCAllocator<T>& left, const UPCAllocator<T>& right)
{
  return false;
}


#endif
