#ifndef __KMER_DHT
#define __KMER_DHT

#include <limits>
#include <iostream>
#include <map>
#include <fstream>
#include <stdarg.h>
#include <upcxx/upcxx.hpp>

#include "zstr.hpp"
#include "bytell_hash_map.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"
#include "kmer.hpp"
#include "bloom.hpp"
#include "histogram_dht.hpp"
#include "upc_allocator.hpp"
#include "upcxx_utils/two_tier_aggr_store.hpp"
#include "misc_utils.hpp"

using namespace upcxx_utils;

using std::vector;
using std::pair;
using std::ostream;
using std::ostringstream;
using std::sort;
using std::numeric_limits;
using std::make_shared;
using std::make_pair;
using std::shared_ptr;
using std::swap;
using std::array;

using upcxx::intrank_t;
using upcxx::rank_me;
using upcxx::rank_n;
using upcxx::barrier;
using upcxx::dist_object;
using upcxx::dist_id;
using upcxx::reduce_one;
using upcxx::reduce_all;
using upcxx::op_fast_add;
using upcxx::op_fast_max;
using upcxx::progress;

//#define DEBUG
#define USE_BYTELL

#ifndef BLOOM_FP
#define BLOOM_FP 0.05
#endif

enum PASS_TYPE { BLOOM_SET_PASS, BLOOM_COUNT_PASS, NO_BLOOM_PASS, CTG_BLOOM_SET_PASS, CTG_KMERS_PASS, INVALID_PASS };

class KmerDHT {
private:
  using ext_count_t = uint16_t;
    
  struct ExtCounts {
    ext_count_t count_A;
    ext_count_t count_C;
    ext_count_t count_G;
    ext_count_t count_T;

    array<pair<char, int>, 4> get_sorted() {
      array<pair<char, int>, 4> counts = {make_pair('A', (int)count_A), make_pair('C', (int)count_C),
                                          make_pair('G', (int)count_G), make_pair('T', (int)count_T)};
      sort(std::begin(counts), std::end(counts),
           [](const auto &elem1, const auto &elem2) {
             if (elem1.second == elem2.second) return elem1.first > elem2.first;
             else return elem1.second > elem2.second;
           });
      return counts;
    }

    bool is_zero() {
      if (count_A + count_C + count_G + count_T == 0) return true;
      return false;
    }
  };

  // total bytes: 2+8+8=18
  struct KmerCounts {
    // how many times this kmer has occured: don't need to count beyond 65536
    // count of high quality forward and backward exts
    ExtCounts left_exts;
    ExtCounts right_exts;
    uint16_t count;
    bool from_ctg;

    pair<char, char> get_exts(int min_depth_cutoff, double dynamic_min_depth) {
      auto sorted_lefts = left_exts.get_sorted();
      auto sorted_rights = right_exts.get_sorted();
      char left = sorted_lefts[0].first;
      int leftmax = sorted_lefts[0].second;
      int leftmin = sorted_lefts[1].second;
      char right = sorted_rights[0].first;
      int rightmax = sorted_rights[0].second;
      int rightmin = sorted_rights[1].second;
      int dmin_dyn = (1.0 - dynamic_min_depth) * count;      // integer floor
      if (dmin_dyn < min_depth_cutoff) dmin_dyn = min_depth_cutoff;
      if (leftmax < dmin_dyn) left = 'X';
      else if (leftmin >= dmin_dyn) left = 'F';
      if (rightmax < dmin_dyn) right = 'X';
      else if (rightmin >= dmin_dyn) right = 'F';
      return {left, right};
    }
  };

  // total bytes for k = 51: 16+18+18=52
  struct MerarrAndExt {
    Kmer::MERARR merarr;
    char left_ext, right_ext;
    uint16_t count;
  };

  
#ifdef USE_BYTELL
  using kmer_map_t = ska::bytell_hash_map<Kmer, KmerCounts, KmerHash, KmerEqual, UPCAllocator<pair<const Kmer, KmerCounts> > >;
#else
  using kmer_map_t = std::unordered_map<Kmer, KmerCounts, KmerHash, KmerEqual, UPCAllocator<pair<const Kmer, KmerCounts> > >;
#endif
  kmer_map_t kmers;

  using bloom_filter_t = BloomFilter;
  // The first bloom filter stores all kmers and is used to check for single occurrences to filter out
  bloom_filter_t bloom_filter1;
  // the second bloom filer stores only kmers that are above the repeat depth, and is used for correctly sizing the kmer hash table
  bloom_filter_t bloom_filter2;
  PASS_TYPE active_pass;

  int min_depth_cutoff;
  double dynamic_min_depth;
  int64_t max_kmer_store_bytes;
  int64_t initial_kmer_dht_reservation;
  int64_t estimated_kmers;
  int64_t total_estimated_kmers;
  int64_t bloom1_cardinality;
  
public:
  int64_t num_prev_mers_from_ctgs;
private:

  class InsertKmer {
    kmer_map_t &kmers;
  public:
    InsertKmer(kmer_map_t &_kmers): kmers(_kmers) {}
    void operator()(MerarrAndExt &merarr_and_ext) {
      Kmer new_kmer(merarr_and_ext.merarr);
      // find it - if it isn't found then insert it, otherwise increment the counts
      const auto it = kmers.find(new_kmer);
      if (it == kmers.end()) {
        KmerCounts kmer_counts = { .left_exts = {0}, .right_exts = {0}, .count = merarr_and_ext.count, .from_ctg = false };
        inc_ext(kmer_counts.left_exts, merarr_and_ext.left_ext);
        inc_ext(kmer_counts.right_exts, merarr_and_ext.right_ext);
        auto prev_bucket_count = kmers.bucket_count();
        kmers.insert({new_kmer, kmer_counts});
        if (prev_bucket_count < kmers.bucket_count())
          SOUT("*** Hash table on rank 0 was resized from ", prev_bucket_count, " to ", kmers.bucket_count(), "***\n");
      } else {
        auto kmer = &it->second;
        int newCount = kmer->count + merarr_and_ext.count;
        kmer->count = (newCount < numeric_limits<uint16_t>::max()) ? newCount : numeric_limits<uint16_t>::max();
        inc_ext(kmer->left_exts, merarr_and_ext.left_ext);
        inc_ext(kmer->right_exts, merarr_and_ext.right_ext);
      }
    }
  };
  dist_object<InsertKmer> insert_kmer;
  TwoTierAggrStore<dist_object<InsertKmer>, MerarrAndExt> insert_kmer_store;

  class BloomSet {
    bloom_filter_t &bloom_filter1, &bloom_filter2;
  public:
    BloomSet(bloom_filter_t &b1, bloom_filter_t &b2) : bloom_filter1(b1), bloom_filter2(b2) {}
    void operator()(Kmer::MERARR &merarr) {
      // look for it in the first bloom filter - if not found, add it just to the first bloom filter
      // if found, add it to the second bloom filter
      Kmer new_kmer(merarr);
      assert( rank_me() == get_kmer_target_rank(new_kmer) );
      if (!bloom_filter1.possibly_contains(new_kmer.getBytes(), new_kmer.getNumBytes())) 
        bloom_filter1.add(new_kmer.getBytes(), new_kmer.getNumBytes());
      else 
        bloom_filter2.add(new_kmer.getBytes(), new_kmer.getNumBytes());
    }
  };
  dist_object<BloomSet> bloom_set;
  TwoTierAggrStore<dist_object<BloomSet>, Kmer::MERARR> bloom_set_store;

  class CtgBloomSet {
    bloom_filter_t &bloom_filter2;
  public:
    CtgBloomSet(bloom_filter_t &b2): bloom_filter2(b2) {}
    void operator()(Kmer::MERARR &merarr) {
      // only add to bloom_filter2
      Kmer new_kmer(merarr);
      bloom_filter2.add(new_kmer.getBytes(), new_kmer.getNumBytes());
    }
  };
  dist_object<CtgBloomSet> ctg_bloom_set;
  TwoTierAggrStore<dist_object<CtgBloomSet>, Kmer::MERARR> ctg_bloom_set_store;

  class BloomCount {
    kmer_map_t &kmers;
    bloom_filter_t &bloom_filter2;
  public:
    BloomCount(kmer_map_t &k, bloom_filter_t &b): kmers(k), bloom_filter2(b) {}
    void operator()(MerarrAndExt &merarr_and_ext) {
      Kmer new_kmer(merarr_and_ext.merarr);
      assert( rank_me() == get_kmer_target_rank(new_kmer) );
      // if the kmer is not found in the bloom filter, skip it
      if (!bloom_filter2.possibly_contains(new_kmer.getBytes(), new_kmer.getNumBytes())) return;
      // add or update the kmer count
      const auto it = kmers.find(new_kmer);
      if (it == kmers.end()) {
        KmerCounts kmer_counts = { .left_exts = {0}, .right_exts = {0}, .count = merarr_and_ext.count, .from_ctg = false };
        inc_ext(kmer_counts.left_exts, merarr_and_ext.left_ext);
        inc_ext(kmer_counts.right_exts, merarr_and_ext.right_ext);
        auto prev_bucket_count = kmers.bucket_count();
        kmers.insert({new_kmer, kmer_counts});
        // this shouldn't happen 
        if (prev_bucket_count < kmers.bucket_count()) {
          if (!rank_me()) {
            WARN("Hash table was resized from ", prev_bucket_count, " to ", kmers.bucket_count(), "\n");
          } else {
            DBG("Hash table was resized from ", prev_bucket_count, " to ", kmers.bucket_count(), "\n");
          }
        }
      } else {
        auto kmer = &it->second;
        int newCount = kmer->count + merarr_and_ext.count;
        kmer->count = (newCount < numeric_limits<uint16_t>::max()) ? newCount : numeric_limits<uint16_t>::max();
        inc_ext(kmer->left_exts, merarr_and_ext.left_ext);
        inc_ext(kmer->right_exts, merarr_and_ext.right_ext);
      }
    }
  };
  dist_object<BloomCount> bloom_count;
  TwoTierAggrStore<dist_object<BloomCount>, MerarrAndExt> bloom_count_store;

  class InsertCtgKmer {
    kmer_map_t &kmers;
    int min_depth_cutoff;
    double dynamic_min_depth;
  public:
    InsertCtgKmer(kmer_map_t &k, int min_d, double dynamic_m) : kmers(k), min_depth_cutoff(min_d), dynamic_min_depth(dynamic_m) {}
    void operator()(MerarrAndExt &merarr_and_ext) {
      // insert a new kmer derived from the previous round's contigs
      Kmer new_kmer(merarr_and_ext.merarr);
      assert( rank_me() == get_kmer_target_rank(new_kmer) );
      const auto it = kmers.find(new_kmer);
      bool insert = false;
      if (it == kmers.end()) {
        // if it isn't found then insert it
        insert = true;
        DBG("new ", merarr_and_ext.count, " ", merarr_and_ext.left_ext, " ", merarr_and_ext.right_ext, "\n");
      } else {
        auto kmer_counts = &it->second;
        DBG(new_kmer, " old/new ", kmer_counts->count, " ", merarr_and_ext.count, " ",
            merarr_and_ext.left_ext, " ", merarr_and_ext.right_ext, " ",
            "A", kmer_counts->left_exts.count_A, " C", kmer_counts->left_exts.count_C, " ", 
            "G", kmer_counts->left_exts.count_G, " T", kmer_counts->left_exts.count_T, " ", 
            "A", kmer_counts->right_exts.count_A, " C", kmer_counts->right_exts.count_C, " ", 
            "G", kmer_counts->right_exts.count_G, " T", kmer_counts->right_exts.count_T, "\n");
        if (!kmer_counts->from_ctg) {
          // existing kmer is from a read, only replace with new contig kmer if the existing kmer is not UU
          auto exts = kmer_counts->get_exts(min_depth_cutoff, dynamic_min_depth);
          if (exts.first == 'X' || exts.first == 'F' || exts.second == 'X' || exts.second == 'F') {
            // non-UU, replace
            insert = true;
            // but keep the count from the read kmer
            //if (kmer_counts->count > *min_depth_cutoff) merarr_and_ext.count = kmer_counts->count;
            DBG("replace non-UU read kmer\n");
          }
        } else {
          // existing kmer is from the previous round's contigs
          // if this was previously a conflict and the depth was set to zero, do nothing
          if (!kmer_counts->count) {
            DBG("skip conflicted kmer, depth 0\n");
          } else {
            // if the two contig kmers disagree on exts, set to purge this one by setting the count to 0
            auto exts = kmer_counts->get_exts(min_depth_cutoff, dynamic_min_depth);
            if (exts.first != merarr_and_ext.left_ext || exts.second != merarr_and_ext.right_ext) {
              merarr_and_ext.count = 0;
              insert = true;
              DBG("purge mismatch\n");
            } else {
              // we have multiple occurrences of the same kmer derived from different contigs or
              // parts of contigs - sum the depths
              int sum_counts = (int)merarr_and_ext.count + kmer_counts->count;
              if (sum_counts > numeric_limits<uint16_t>::max()) sum_counts = numeric_limits<uint16_t>::max();
              merarr_and_ext.count = sum_counts;
              insert = true;
              DBG("increase count\n");
            }
          }
        }
      }
      if (insert) {
        uint16_t count = merarr_and_ext.count;
        KmerCounts kmer_counts = { .left_exts = {0}, .right_exts = {0}, .count = count, .from_ctg = true };
        inc_ext(kmer_counts.left_exts, merarr_and_ext.left_ext, count);
        inc_ext(kmer_counts.right_exts, merarr_and_ext.right_ext, count);
        kmers[new_kmer] = kmer_counts;
      }
    }
  };
  dist_object<InsertCtgKmer> insert_ctg_kmer;
  TwoTierAggrStore<dist_object<InsertCtgKmer>, MerarrAndExt> insert_ctg_kmer_store;
   
  static intrank_t get_kmer_target_rank(Kmer &kmer) {
    static std::hash<Kmer> hash_func = {};
    return hash_func(kmer) % rank_n();
  }

  static void inc_ext(ExtCounts &ext_counts, char ext, int count=1) {
    switch (ext) {
      case 'A': 
        count += ext_counts.count_A;
        ext_counts.count_A = (count < numeric_limits<ext_count_t>::max()) ? count : numeric_limits<ext_count_t>::max();
        break;
      case 'C': 
        count += ext_counts.count_C;
        ext_counts.count_C = (count < numeric_limits<ext_count_t>::max()) ? count : numeric_limits<ext_count_t>::max();
        break;
      case 'G':
        count += ext_counts.count_G;
        ext_counts.count_G = (count < numeric_limits<ext_count_t>::max()) ? count : numeric_limits<ext_count_t>::max();
        break;
      case 'T': 
        count += ext_counts.count_T;
        ext_counts.count_T = (count < numeric_limits<ext_count_t>::max()) ? count : numeric_limits<ext_count_t>::max();
        break;
    }
  }

public:

  KmerDHT(uint64_t cardinality, int max_kmer_store_bytes, int min_depth_cutoff, double dynamic_min_depth, bool use_bloom)
    : kmers(), min_depth_cutoff(min_depth_cutoff), dynamic_min_depth(dynamic_min_depth)
    , bloom_filter1(), bloom_filter2(), active_pass(INVALID_PASS)
    , insert_kmer(InsertKmer(kmers)), insert_kmer_store(insert_kmer, "insert_kmer")
    , bloom_set(BloomSet(bloom_filter1, bloom_filter2)), bloom_set_store(bloom_set, "bloom_set")
    , ctg_bloom_set(CtgBloomSet(bloom_filter2)), ctg_bloom_set_store(ctg_bloom_set, "ctg_bloom_set")
    , bloom_count(BloomCount(kmers, bloom_filter2)), bloom_count_store(bloom_count, "bloom_count")
    , insert_ctg_kmer(InsertCtgKmer(kmers,min_depth_cutoff, dynamic_min_depth))
    , insert_ctg_kmer_store(insert_ctg_kmer, "insert_ctg_kmer")
    , max_kmer_store_bytes(max_kmer_store_bytes), initial_kmer_dht_reservation(0), bloom1_cardinality(0)
    , num_prev_mers_from_ctgs(0) {
    
    if (use_bloom) {
      change_pass_type(BLOOM_SET_PASS);
    } else {
      change_pass_type(NO_BLOOM_PASS);
    }
    
#ifdef USE_BYTELL
    SOUT("Using bytell hash map\n");
#else
    SOUT("Using std::unordered_map\n");
#endif
    if (use_bloom) {
      // in this case we get an accurate estimate of the hash table size after the first bloom round, so the hash table space is
      // reserved then
      double init_mem_free = get_free_mem_gb();
      bloom_filter1.init(cardinality, BLOOM_FP);
      bloom_filter2.init(cardinality/4, BLOOM_FP); // second bloom will have far fewer entries - assume 75% are filtered out
      SOUT("Bloom filters used ", (init_mem_free - get_free_mem_gb()), "GB memory on node 0\n");
    } else {
      double init_mem_free = get_free_mem_gb();
      barrier();
      // in this case we have to roughly estimate the hash table size - we do the allocation here
      // rough estimate at 5x depth
      cardinality /= 5;
      initial_kmer_dht_reservation = cardinality;    
      kmers.reserve(cardinality);
      double kmers_space_reserved = cardinality * (sizeof(Kmer) + sizeof(KmerCounts));
      SOUT("Rank 0 is reserving ", get_size_str(kmers_space_reserved), " for kmer hash table with ", cardinality, " entries (",
           kmers.bucket_count(), " buckets)\n");
      barrier();
      SOUT("After reserving space for local hash tables, ", (init_mem_free - get_free_mem_gb()), "GB memory was used on node 0\n");
    }
  }
  
  void clear_aggr_stores() {
    insert_kmer_store.clear();
    bloom_set_store.clear();
    ctg_bloom_set_store.clear();
    bloom_count_store.clear();
    insert_ctg_kmer_store.clear();
  }
  void clear() {
    clear_aggr_stores();
    for (auto it = kmers.begin(); it != kmers.end(); ) {
      it = kmers.erase(it);
    }
  }
  
  ~KmerDHT() {
    clear();
  }

  int64_t get_num_kmers(bool all = false) {
    if (!all) return reduce_one(kmers.size(), op_fast_add, 0).wait();
    else return reduce_all(kmers.size(), op_fast_add).wait();
  }

  float max_load_factor() {
    return reduce_one(kmers.max_load_factor(), op_fast_max, 0).wait();
  }
  
  float load_factor() {
    auto cardinality_fut = reduce_one(initial_kmer_dht_reservation, op_fast_add, 0);
    auto max_lf_fut = reduce_one(kmers.load_factor(), op_fast_max, 0);
    auto sum_lf_fut = reduce_one(kmers.load_factor(), op_fast_add, 0);
    auto worst_underestimate_fut = reduce_one((float) kmers.size() / (float) estimated_kmers, op_fast_max, 0);
    int64_t num_kmers = get_num_kmers();
    int64_t cardinality = cardinality_fut.wait();
    float max_lf = max_lf_fut.wait();
    float avg_lf = sum_lf_fut.wait() / upcxx::rank_n();
    float worst_underestimate = worst_underestimate_fut.wait();
    SOUT("Originally reserved ", cardinality, ", estimated ", total_estimated_kmers, " and now have ", num_kmers, " elements (", get_float_str(100.0*num_kmers/total_estimated_kmers), "%, max ", get_float_str(100.0*worst_underestimate), "%), max_load: ", get_float_str(max_lf), ", avg_load: ", get_float_str(avg_lf), ", balance: ", get_float_str(avg_lf/max_lf), "\n");
    return avg_lf;
  }
  
  int64_t get_local_num_kmers(void) {
    return kmers.size();
  }

  void change_pass_type(PASS_TYPE pass_type) {
    DBG("change_pass_type(pass_type=", pass_type, "): active_pass=", active_pass, "\n");
    if (active_pass == pass_type) {
      return;
    }
    clear_aggr_stores();
    switch (pass_type) {
      case BLOOM_SET_PASS:
        bloom_set_store.set_size(max_kmer_store_bytes);
        break;
      case CTG_BLOOM_SET_PASS:
        ctg_bloom_set_store.set_size(max_kmer_store_bytes);
        break;
      case BLOOM_COUNT_PASS:
        bloom_count_store.set_size(max_kmer_store_bytes);
        break;
      case NO_BLOOM_PASS:
        insert_kmer_store.set_size(max_kmer_store_bytes);
        break;
      case CTG_KMERS_PASS:
        insert_ctg_kmer_store.set_size(max_kmer_store_bytes);
        break;
      default: DIE("Invalid pass_type: ", pass_type, "\n");
    }
    active_pass = pass_type;
  }

  void add_kmer(Kmer kmer, char left_ext, char right_ext, uint16_t count) {
    // get the lexicographically smallest
    Kmer kmer_twin = kmer.twin();
    if (kmer_twin < kmer) {
      kmer = kmer_twin;
      swap(left_ext, right_ext);
      left_ext = comp_nucleotide(left_ext);
      right_ext = comp_nucleotide(right_ext);
    }
    auto target_rank = get_kmer_target_rank(kmer);
    MerarrAndExt merarr_and_ext = { kmer.getArray(), left_ext, right_ext, count };
    switch (active_pass) {
      case BLOOM_SET_PASS:
        if (count != 0)
            bloom_set_store.update(target_rank, merarr_and_ext.merarr);
        break;
      case CTG_BLOOM_SET_PASS:
        ctg_bloom_set_store.update(target_rank, merarr_and_ext.merarr);
        break;
      case BLOOM_COUNT_PASS:
        bloom_count_store.update(target_rank, merarr_and_ext);
        break;
      case NO_BLOOM_PASS:
        insert_kmer_store.update(target_rank, merarr_and_ext);
        break;
      case CTG_KMERS_PASS:
        insert_ctg_kmer_store.update(target_rank, merarr_and_ext);
        break;
      default: DIE("Invalid pass_type: ", active_pass, "\n");
    };
  }

  void reserve_space_and_clear_bloom1() {
    BarrierTimer timer(__func__);
    // at this point we're done with generating the bloom filters, so we can drop the first bloom filter and
    // allocate the kmer hash table

    // purge the kmer store and prep the kmer + count
    flush_updates();
    change_pass_type(BLOOM_COUNT_PASS);

    int64_t cardinality1 = bloom_filter1.estimate_num_items();
    int64_t bits_on2 = bloom_filter2.get_bits_on();
    double fp2 = bloom_filter2.estimate_fp_rate(bits_on2);
    int64_t cardinality2 = bloom_filter2.estimate_num_items(bits_on2);
    bloom1_cardinality = cardinality1;
    SOUT("Rank 0: first bloom filter size estimate is ", cardinality1, " and second size estimate is ", cardinality2,
         " ratio is ", (double)cardinality2 / cardinality1, "\n");
    bloom_filter1.clear(); // no longer need it

    double init_mem_free = get_free_mem_gb();
    barrier();
    int64_t erroneous_kmers = cardinality1 - cardinality2;
    if (erroneous_kmers < 10000) erroneous_kmers = 10000;
    // two blooms - low_count * fp_rate, card2 * (1+fp_rate)
    estimated_kmers= (int64_t) (cardinality2 * (1+fp2) + erroneous_kmers * (fp2));
    auto total_estimate_fut = reduce_one(estimated_kmers, op_fast_add, 0);
    // reserve 5% more than the final total modified by the load_factor - enough to ensure it will not rehash
    initial_kmer_dht_reservation = (int64_t) (1.05 * estimated_kmers);
    kmers.reserve( initial_kmer_dht_reservation );
    double kmers_space_reserved = kmers.bucket_count() * (sizeof(kmer_map_t::value_type));
    total_estimated_kmers = total_estimate_fut.wait();
    SOUT("Rank 0 is reserving ", get_size_str(kmers_space_reserved), " for est ", estimated_kmers, " in kmer hash table with ", initial_kmer_dht_reservation, " entries (",
         kmers.bucket_count(), " buckets)\n");
    SOUT("Estimating ", total_estimated_kmers, " will be stored in global hash table ", get_size_str(kmers_space_reserved * rank_n()), "\n");
    // implict barrier when BarrierTimer is destoryed // barrier();
  }

  void flush_updates() {
    Timer timer(__func__);
    if (active_pass == BLOOM_SET_PASS) {
        bloom_set_store.flush_updates();
    } else if (active_pass == CTG_BLOOM_SET_PASS) {
        ctg_bloom_set_store.flush_updates();
    } else if (active_pass == BLOOM_COUNT_PASS) {
        bloom_count_store.flush_updates();
    } else if (active_pass == NO_BLOOM_PASS) {
        insert_kmer_store.flush_updates();
    } else if (active_pass == CTG_KMERS_PASS) {
        insert_ctg_kmer_store.flush_updates();
    } else {
      DIE("bad pass type:", active_pass, "\n");
    }
    // implict barrier() when BarrierTimer is destoryed // barrier();
  }

  int64_t purge_kmers(int threshold) {
    BarrierTimer timer(__func__);
    int64_t num_purged = 0;
    for (auto it = kmers.begin(); it != kmers.end(); ) {
      auto kmer_counts = make_shared<KmerCounts>(it->second);
      if ((kmer_counts->count < threshold) || (kmer_counts->left_exts.is_zero() && kmer_counts->right_exts.is_zero())) {
        num_purged++;
        it = kmers.erase(it);
      } else {
        ++it;
      }
    }
    return reduce_one(num_purged, op_fast_add, 0).wait();
  }

  // one line per kmer, format:
  // KMERCHARS LR N
  // where L is left extension and R is right extension, one char, either X, F or A, C, G, T
  // where N is the count of the kmer frequency
  void dump_kmers(int k, string dump_fname) {
    BarrierTimer timer(__func__);
    dump_fname += "ALL_INPUTS.fofn-" + to_string(k) + ".ufx.bin";
    dump_fname += ".gz";
    get_rank_path(dump_fname, rank_me());
    zstr::ofstream dump_file(dump_fname);
    ostringstream out_buf;
    ProgressBar progbar(kmers.size(), "Dumping kmers to " + dump_fname);
    int64_t i = 0;
    for (auto &elem : kmers) {
      auto kmer = &elem.first;
      auto kmer_counts = &elem.second;
      auto exts = kmer_counts->get_exts(min_depth_cutoff, dynamic_min_depth);
      out_buf << *kmer << " " << kmer_counts->count << " " << exts.first << " " << exts.second << endl;
      i++;
      if (!(i % 1000)) {
        dump_file << out_buf.str();
        out_buf.str(""); // reset
      }
      progbar.update();
    }
    if (!out_buf.str().empty()) dump_file << out_buf.str();
    dump_file.close();
    progbar.done();
    SOUT("Dumped ", this->get_num_kmers(), " kmers\n");
    string entries_fname = dump_fname + ".entries";
    std::ofstream entries_file(entries_fname);
    entries_file << kmers.size() << endl;
    entries_file.close();
    // implict barrier when BarrierTimer is destoryed // barrier();
  }

  // build and output histogram
  void write_histogram() {
    BarrierTimer timer(__func__);
    SOUT("Generating histogram\n");
    shared_ptr<HistogramDHT> histogram = make_shared<HistogramDHT>();
    uint64_t numKmers = 0;
    for (auto &elem : kmers) {
        uint64_t count = elem.second.count;
        histogram->add_count(count,1);
        numKmers++;
    }
    if (bloom1_cardinality > 0) {
        int64_t num_one_counts = bloom1_cardinality - numKmers;
        if (num_one_counts > 0) {
            histogram->add_count(1, num_one_counts);
        }
    }
    histogram->flush_updates();
    string hist_fname = "per_rank/histogram_k" + std::to_string(Kmer::k) + ".txt";
    histogram->write_histogram(hist_fname);
    // implict barrier when BarrierTimer is destoryed // barrier();
  }
};

#endif
