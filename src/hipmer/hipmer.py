#!/usr/bin/env python

from __future__ import print_function

import signal
import subprocess
import sys
import os
import platform
import shutil
import argparse
import glob
#import git
#import thread
import datetime
import time
import traceback
from timeit import default_timer as dtimer
import multiprocessing
import threading
import re


cached_io = None

has_psutil = None
try:
    import psutil
    has_psutil = []
    p = psutil.Process(os.getpid())
    try:
        keys = p.as_dict().keys()
        for k in ('cpu_times', 'memory_info', 'io_counters'):
            if k in keys:
                has_psutil.append(k) 
    except:
        print("no as_dict() method for psutil: ", p, "\n")
        pass
except ImportError:
    print("Warning: Could not find the psutil module, memory information and profiling may be inaccurate\n", file=sys.stderr)
    has_psutil = None

mem_tracker_stop_flag = threading.Event()

user_messages = []
def message(*args):
    global user_messages
    print(*args)
    user_messages.append(args)

def write_messages(log):
    global user_messages
    for message in user_messages:
        print(*message, file=log)
    log.flush()

def print_log(log, *args):
    print(*args)
    print(*args, file=log)

STAGE_DESC_SIZE = 52

    
def which(file_name):
    for path in os.environ["PATH"].split(os.pathsep):
        full_path = os.path.join(path, file_name)
        if os.path.exists(full_path) and os.access(full_path, os.X_OK):
            return full_path
    return None

        
def die(*args):
    print('\nFATAL ERROR:', *args, file=sys.stderr)
    sys.stdout.flush()
    sys.stderr.flush()
    mem_tracker_stop_flag.set()
    exit_all(1)

def check_exec(cmd, args, expected):
    test_exec = which(cmd)
    if not test_exec:
        die('Cannot find', cmd)
    try:
        result = subprocess.check_output([test_exec, args]).decode()
        if expected not in result:
            die(test_exec, 'failed to execute')
    except subprocess.CalledProcessError as err:
        die('Could not execute', test_exec +':', err)


def check_striping(fname):
    if not which('lfs'):
        return
    try:
        fname = os.path.realpath(fname)
        result = subprocess.check_output(['lfs', 'getstripe', fname]).decode()
        lines = result.split('\n')
        if lines > 1:
            _, stripe_count = lines[1].split()
            if int(stripe_count) == 1:
                message('WARNING: lfs is available but', fname, 'has a stripe count of 1, which could result in poor IO performance.')
            #else:
            #    print('Library', fname, 'is striped with count', stripe_count)
    except:
        pass


def stripe_dir(outdir, stripe_count):
   try:
        if subprocess.call(['lfs', 'setstripe', '-c', stripe_count, outdir]) != 0:
            message('WARNING: lfs is available but could not set Lustre striping on directory:', outdir)
        message("Changed stripe count on ", outdir, " to ", stripe_count)
   except:
       message("WARNING: could not set stripe on ", outdir, " to ", stripe_count, sys.exc_info()[0])
       pass


def mkdir_if_missing(dir):
    if not os.path.isdir(dir):
        os.mkdir(dir)
        return True
    return False

def setup_outdir(options):
    if not os.path.isdir(options.outdir):
        message('Creating output directory "' + options.outdir + '"')
    else:
        message('Using existing output directory "' + options.outdir + '"')
    needsStripe  = mkdir_if_missing(options.outdir) 
    needsStripe0 = mkdir_if_missing(options.outdir + '/per_rank')
    needsStripe1 = mkdir_if_missing(options.outdir + '/results')
    needsStripe2 = mkdir_if_missing(options.outdir + '/intermediates')
    needsStripe = needsStripe | needsStripe0 | needsStripe1 | needsStripe2
    if not needsStripe:
        return
    lfs_exec = which('lfs')
    if not lfs_exec:
        return
    # find the number of active osts leave a few inactive and find a count that is a multiple of threads
    countOsts = 0
    result = subprocess.check_output(['lfs', 'osts', options.outdir]).decode()
    for line in result.split('\n'):
        if line.find("ACTIVE") > 0:
            countOsts += 1
    countOsts = countOsts * 9 // 10 # use up to 90% of the OSTs
    if countOsts > 72:
        countOsts = 72
    while countOsts > 16:
        if options.threads % countOsts == 0:
            break
        else:
            countOsts = countOsts - 1
    if countOsts == 0:
        message("Lustre install found but discovered no OSTs\n")
        return
    message("Striping results and intermediates with %d LUSTRE OSTs\n" % (countOsts))
    stripe_dir(options.outdir + '/results', '%d' % (countOsts))
    stripe_dir(options.outdir + '/intermediates', '%d' % (countOsts))
    message("Striping outdir and per_rank with 1 OST\n")
    stripe_dir(options.outdir, '1')
    stripe_dir(options.outdir + '/per_rank', '1')

       
stage_descs = {'loadfq':                   'loading fastq into memory',
               #'ufx':                      'kmer counting',
               'kcount':                   'kmer counting',
               'meraculous':               'traversing de Bruijn graph',
               'contigMerDepth':           'computing contig depths',
               'contigEndAnalyzer':        'analyzing contig terminations',
               'bubbleFinder':             'finding bubbles',
               'progressiveRelativeDepth': 'pruning contig graph',
               'merAligner':               'aligning reads to contigs',
               'merAlignerAnalyzer':       'determining insert sizes',
               'localassm':                'local assembly',
               'prefixMerDepthExtended':   'computing depths of extended contigs',
               'canonical_assembly':       'generating canonical contigs',
               'findNewMers':              'extracting new kmers from prior contigs',
               'splinter':                 'getting links from single reads',
               'spanner':                  'getting links from paired reads',
               'bmaToLinks':               'merging links',
               'oNo':                      'ordering and orienting contigs',
               'gapclosing':               'closing gaps',
               'parCC':                    'computing components in contig graph',
               'localize_reads':           'localize reads',
               'cgraph':                   'traversing contig graph',
               'merge_reads':              'merging overlapping paired reads',
               }


class OrderedArgumentParser():
    def __init__(self, epilog, *args, **kwargs):
        self.advanced_names = {}
        self.ap = argparse.ArgumentParser(add_help=False, usage=argparse.SUPPRESS, *args, **kwargs)
        self.basic_group = self.ap.add_argument_group(title='Options')
        self.advanced_group = self.ap.add_argument_group(title='Advanced options')
        self.epilog = epilog

    def add_basic_argument(self, *args, **kwargs):
        self.ap.add_argument(*args, **kwargs)
        
    def add_advanced_argument(self, *args, **kwargs):
        name = kwargs['dest']
        self.advanced_names[name] = True
        self.ap.add_argument(*args, **kwargs)
        
    def parse_args(self):
        return self.ap.parse_args()

    def print_args(self, mods=None, always=None):
        args = vars(self.ap.parse_args())
        #print(self.ap.description + '\n')
        message('Options')
        for action in self.ap._actions:
            if (action.default is None and args[action.dest] is not None) or args[action.dest] != action.default or \
               (always and action.dest in always):
                opt_val = mods[action.dest] if mods and action.dest in mods else args[action.dest]
                if isinstance(opt_val, list):
                    dest = action.dest
                    is_lib = 0
                    if '_libs' in dest:
                        is_lib = 1
                        if 'paired_libs' in dest:
                            is_lib = 2
                    for opt in opt_val:
                        if is_lib:
                            files = [opt]
                            if '.fofn' in opt:
                                fofn_name = opt.split(':')[0];
                                files = []
                                with open(fofn_name) as f:
                                    for line in f.readlines():
                                        files.append(line.rstrip())
                            file_size = 0
                            for line in files:
                                fields = line.split(':')
                                lib_file_str = fields[0]
                                if is_lib == 1:
                                    for fn in glob.glob(lib_file_str):
                                        file_size += os.path.getsize(fn)
                                elif is_lib == 2:
                                    if '*' in lib_file_str:
                                        die('Wildcards are not supported for split-read paired libraries; please list the file pairs explicitly.')
                                    check_for_pair(lib_file_str)
                                    lib_file = lib_file_str.split(',')[0]
                                    file_size += os.path.getsize(lib_file)
                                    lib_file = lib_file_str.split(',')[1]
                                    file_size += os.path.getsize(lib_file)
                            message('  %-17s %9.2f GB %s' % (dest.replace('_', ' '), file_size/1024.0/1024.0/1024.0, opt))
                        else:
                            message('  %-30s %s' % (dest.replace('_', ' '), opt))
                        dest = ''
                else:
                    message('  %-30s %s' % (action.dest.replace('_', ' '), opt_val))

    def print_help_group(self, basic=True):
        for action in self.ap._actions:
            if (basic and action.dest not in self.advanced_names) or (not basic and action.dest in self.advanced_names):
                if action.help == argparse.SUPPRESS:
                    continue
                opt_str = ''
                for opt in action.option_strings:
                    if opt_str:
                        opt_str += ', '
                    ext = ''
                    if action.type == int:
                        ext = ' INT'
                    elif action.type == str:
                        ext = ' STRING'
                    elif action.type == float:
                        ext = ' FLOAT'
                    opt_str += opt + ext
                    default_str = ' (' + str(action.default) + ')' if action.default else ''
                print('  %-35s %s' % (opt_str, action.help + default_str))
        
    def print_help(self, advanced=False):
        #print(self.ap.description)
        print('\nOptions:\n')
        self.print_help_group()
        if advanced:
            print('\nAdvanced options:\n')
            self.print_help_group(basic=False)
        print(self.epilog)

import os, sys

def mac_memory_usage():

    result = dict()

    for l in [l.split(':') for l in os.popen('vm_stat').readlines()[1:9]]:
        result[l[0].strip(' "').replace(' ', '_').lower()] = int(l[1].strip('.\n '))

    return result

def mac_mem_free_mb():
    mac_mem = mac_memory_usage()
    avail_pages = mac_mem['pages_free'] + mac_mem['pages_speculative'] + mac_mem['pages_purgeable']
    avail_mem_mb = avail_pages * 4 / 1024
    return avail_mem_mb

tracking_pid = None
def track_pid(pid = None):
    global tracking_pid
    global has_psutil
    if has_psutil:
        if tracking_pid is not None and tracking_pid.is_running():
            pass # okay this instance of main-* is still running
        else:
            # find an instance of main-* that is running on this node
            if tracking_pid is not None:
                pass # print("Stopped tracking as it is no longer running: ", str(tracking_pid))
            tracking_pid = None
            try:
                for test_pid in psutil.pids():
                    p = psutil.Process(test_pid)
                    if p.is_running() and 'main-' in p.name():
                        tracking_pid = p
                        # print("Found one instance of main to follow and use to profile memory: ", str(tracking_pid))
                        break
            except:
                pass # print("Could not find hipmer main executing on this node\n")
    if pid is None:
        pid = tracking_pid
    else:
        pid = psutil.Process(pid)
    if pid is not None:
        s = str(pid.as_dict(has_psutil))
        return s
    else:
        return ""
               

def get_mem_stats():
    mem = ""
    process = ""
    global has_psutil
    if has_psutil:
        mem = psutil.virtual_memory()
        tot_mem_mb = mem.total / 1024 / 1024
        avail_mem_mb = mem.available / 1024 / 1024
    else:
        reclaimable_mem_mb = 0
        tot_mem_mb = -1
        avail_mem_mb = -1
        if platform.system() == 'Darwin':
            for line in os.popen('sysctl -n hw.memsize').readlines():
                 tot_mem_mb = int(line) / 1024 / 1024
            avail_mem_mb = mac_mem_free_mb()
        if tot_mem_mb == -1:
            with open('/proc/meminfo') as f:
                for line in f.readlines():
                    if line.startswith('MemTotal'):
                        tot_mem_mb = float(line.split()[1]) / 1024
                    elif line.startswith('MemAvailable'):
                        avail_mem_mb = float(line.split()[1]) / 1024
                    elif line.startswith('MemFree:') or line.startswith('SReclaimable:') or line.startswith('Cached:') or \
                         line.startswith('Buffers:'):
                        reclaimable_mem_mb = reclaimable_mem_mb + float(line.split()[1]) / 1024
        # some older systems do not have MemAvailable in /proc/meminfo
        if avail_mem_mb == -1:
            avail_mem_mb = reclaimable_mem_mb
            if avail_mem_mb > tot_mem_mb:
                avail_mem_mb = tot_mem_mb * 90 / 100
    return tot_mem_mb, 100.0 * avail_mem_mb / tot_mem_mb, avail_mem_mb, mem


# Using a timer to determine peak memory usage on a single node
class RepeatTimer(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event
        self.start_mem = get_mem_stats()[2]
        self.peak_mem = 0
        self.output_file = None
        self.profile = ()
        self.devshm = None
        self.last_flush = 0
        if os.path.exists("/dev/shm/"):
            self.devshm = True

    def open(self,out_file):
        global has_psutil
        import os
        exists = os.path.exists(out_file)
        self.output_file = open(out_file, "a")
        if not exists:
            if has_psutil:
                print("\t".join(["Time","TotalMem", "%AvailableMem", "AvailableMem", "MemUsed", "PeakMemUsed", "DevShmSize", "DevShmAvail", "DevShmFree", "psutil"]), file=self.output_file)
            else:
                print("\t".join(["Time","TotalMem", "%AvailableMem", "AvailableMem", "MemUsed", "PeakMemUsed", "DevShmSize", "DevShmAvail", "DevShmFree"]), file=self.output_file)
        if len(self.profile) > 0:
            print("\n".join( self.profile ), file=self.output_file)
        self.profile = ()

    def close(self):
        if self.output_file:
            self.output_file.close()

    def run(self):
        while not self.stopped.wait(0.5):
            mem_stats = get_mem_stats()
            mem = mem_stats[2]
            devshm_size = 0
            devshm_avail = 0
            devshm_free = 0
            if self.devshm:
                devshm = os.statvfs('/dev/shm/')
                devshm_size = devshm.f_bsize * devshm.f_blocks
                devshm_avail = devshm.f_bsize * devshm.f_bavail
                devshm_free = devshm.f_bsize * devshm.f_bfree
            mem_used = self.start_mem - mem
            if mem_used > self.peak_mem:
                self.peak_mem = mem_used
            outstr = "\t".join([str(datetime.datetime.now()), str(mem_stats[0]), str(mem_stats[1]), str(mem_stats[2]), str(mem_used), str(self.peak_mem), str(devshm_size), str(devshm_avail), str(devshm_free)])
            if mem_stats[3] is not None:
                outstr += "\t" + str(mem_stats[3])
                try:
                    outstr += "," + track_pid()
                    try:
                        outstr += "," + str(psutil.net_io_counters(pernic=True))
                    except:
                        pass
                except:
                    pass
            if self.output_file:
                print(outstr, file=self.output_file)
                self.last_flush += 1
                if self.last_flush > 30:
                    self.output_file.flush()
                    self.last_flush = 0
            else:
                self.profile.append(outstr)
        self.close()
            

def get_version():
    try:
        with open(sys.path[0] +'/../HIPMER_VERSION', 'r') as f:
            version = f.readline().strip()
        return version
    except IOError:
        return 'unknown version'
    #repo = git.Repo(__file__, search_parent_directories=True)
    #return 'version ' + str(repo.active_branch) + '-' + str(repo.head.object.hexsha[:8])


def list_stages(outdir):
    stage_list = []
    list_str = ''
    # some stages have side effects that do not support restarting
    # from an arbitray point in the pipeline
    # but should support an immediate restart when it is the last stage
    # Issue #195
    exclude_stages = ['findNewMers', 'progressiveRelativeDepth' ] # FIXME can these be supported yet?
    exclude_stages_always = ['loadfq', 'canonical_assembly-' ]
    try:
        run_file = open(outdir + '/run.log', 'r')
        i = 0
        last_stage = None
        for line in run_file:
            if line.find('# Starting stage') != -1:
                stage = line.split(' ')[3]
                okay = True
                for exclude in exclude_stages_always:
                    if stage.find(exclude) >= 0:
                        okay = False
                if okay:
                    for exclude in exclude_stages:
                        if stage.find(exclude) >= 0:
                            okay = False
                if okay and (stage not in stage_list) and (last_stage is not None) and \
                            ( ( stage.find('localize_reads') < 0 and last_stage.find('localize_reads') >= 0) or \
                              ( stage.find('merge_reads') < 0 and last_stage.find('merge_reads') >= 0) ):
                    # remove all previous stages as we can not restart prior to this next stage after localize_reads or merge_reads
                    stage_list = [] 
                    list_str = ""
                    i = 0
                if okay and stage not in stage_list:
                    last_stage = stage
                    list_str += str(i) + ': ' + stage + '\n'
                    stage_list.append(stage)
                    i += 1
        if (last_stage is not None) and (last_stage not in stage_list):
            stage_list.append(stage)

    except IOError:
        die('No previous stages found for restart')
    return stage_list, list_str

def glob_expand(line, mods, filepath):
    libs = []
    line = line.strip()
    if ',' in line:
        files = line.split(',')
        filenames = glob.glob(files[0])
        filenames2 = glob.glob(files[1])
        if len(filenames2) != len(filenames) or len(filenames) == 0:
            die('Invalid entry', line, 'in fofn', filepath, " glob-expanded ", filenames, " and ", filenames2)
        for i in range(len(filenames)):
            if not os.path.exists(filenames[i]):
                die("Could not find the expanded file ", filenames[i], " from ", filepath)
            if not os.path.exists(filenames2[i]):
                die("Could not find the expanded file ", filenames2[i], " from ", filepath)
            #message("Expanded paired: ", filenames[i], filenames2[i], " with mods ", mods)
            if mods:
                libs.append("%s,%s:%s" % (filenames[i], filenames2[i], ':'.join(mods)))
            else:
                libs.append("%s,%s" % (filenames[i], filenames2[i]))
    else:
        filenames = glob.glob(line)
        if len(filenames) == 0:
            die('Invalid entry', line, 'in fofn', filepath)
        for lib_filename in filenames:
            if os.path.splitext(lib_filename)[1] not in ['.fq', '.fastq']:
                die('File "' + lib_filename + '" in fofn "' + filepath + '" needs to be valid fastq')
            if not os.path.exists(lib_filename):
                die("Could not find the expanded file ", lib_filename, " from ", filepath)
            #message("Expanded: ", lib_filename, " with mods ", mods)
            if mods:
                libs.append("%s:%s" % (lib_filename, ':'.join(mods)))
            else: # no mods
                libs.append(lib_filename)
    return libs

def unpack_fofn(fnames):
    if len(fnames) == 0:
        return fnames
    libs = []
    for fname in fnames:
        fields = fname.split(':')
        filepath = fields[0]
        mods = fields[1:]
        if os.path.splitext(filepath)[1] == '.fofn':
            #message('found fofn', filepath)
            # expand it, include any mods to all files within the fofn
            try:
                with open(filepath) as f:
                    for line in f.readlines():
                        libs.extend( glob_expand( line, mods, filepath ) )
            except IOError:
                die('Cannot open "' + filepath + '" as a file-of-filenames. ' + \
                      'This needs to either be a valid file-of-filenames or a fastq file.')
        else: # just a file or glob
            libs.extend( glob_expand( filepath, mods, filepath ) )
    return libs

_defaultCores = None
def get_hdw_cores_per_node():
    """Query the hardware for physical cores"""
    global _defaultCores
    if _defaultCores is not None:
        return _defaultCores
    try:
        import psutil
        cores = psutil.cpu_count(logical=False)
        print("Found cpus from psutil:", cores)
    except (NameError, ImportError):
        print("Could not get cpus from psutil")
        pass
    # always trust lscpu, not psutil
    # NOTE some versions of psutil has bugs and comes up with the *WRONG* physical cores 
    if True:
        import platform
        cpus = multiprocessing.cpu_count()
        hyperthreads = 1
        if platform.system() == 'Darwin':
            for line in os.popen('sysctl -n hw.physicalcpu').readlines():
                 hyperthreads = cpus / int(line) 
            print("Found %d cpus and %d hyperthreads from sysctl" % (cpus, hyperthreads))
        else:
            for line in os.popen('lscpu').readlines():
                if line.startswith('Thread(s) per core'):
                    hyperthreads = int(line.split()[3])
            print("Found %d cpus and %d hyperthreads from lscpu" % (cpus, hyperthreads))
        cores = int(cpus / hyperthreads)
    _defaultCores = cores
    return cores
            
def get_job_id():
    """Query the environment for a job"""
    for key in ['PBS_JOBID', 'SLURM_JOBID', 'LSB_JOBID', 'JOB_ID', 'COBALT_JOBID', 'LOAD_STEP_ID', 'LBS_JOBID']:
      if key in os.environ:
        return os.environ.get(key)
    return None

def get_job_name():
    """Query the env for the name of a job"""
    for key in ['PBS_JOBNAME', 'JOBNAME', 'SLURM_JOB_NAME', 'LSB_JOBNAME', 'JOB_NAME', 'LSB_JOBNAME']:
      if key in os.environ:
        return os.environ.get(key)
    return None

def is_cobalt_job():
    return os.environ.get('COBALT_JOBID') is not None

def is_slurm_job():
    return os.environ.get('SLURM_JOB_ID') is not None

def is_pbs_job():
    return os.environ.get('PBS_JOBID') is not None

def is_lsb_job():
    return os.environ.get('LSB_JOBID') is not None

def is_ge_job():
    return os.environ.get('JOB_ID') is not None

def is_ll_job():
    return os.environ.get('LOAD_STEP_ID') is not None



def get_slurm_cores_per_node(defaultCores = 0):
    # Only trust this environment variable from slurm, otherwise trust the hardware
    if defaultCores == 0:
        defaultCores = get_hdw_cores_per_node()
    ntasks_per_node = os.environ.get('SLURM_NTASKS_PER_NODE')
    if ntasks_per_node:
        print("Found tasks per node from SLURM_NTASKS_PER_NODE=", ntasks_per_node)
        return int(ntasks_per_node)
    # This SLURM variable defaults to all the hyperthreads if not overriden by the sbatch option --ntasks-per-node
    ntasks_per_node = os.environ.get('SLURM_TASKS_PER_NODE') # SLURM_TASKS_PER_NODE=32(x4)
    if ntasks_per_node:
        if ntasks_per_node.find('(') > 0:
            ntasks_per_node = int(ntasks_per_node[:ntasks_per_node.find('(')])
        else:
            ntasks_per_node = int(ntasks_per_node)
        if ntasks_per_node <= defaultCores:
            print("Found reduced tasks per node from SLURM_TASKS_PER_NODE=", os.environ.get('SLURM_TASKS_PER_NODE'))
            return ntasks_per_node
        print("Using default cores of ", defaultCores, ". Ignoring tasks per node ", ntasks_per_node, " from SLURM_TASKS_PER_NODE=", os.environ.get('SLURM_TASKS_PER_NODE'))
    return defaultCores

def get_cobalt_cores_per_node():
    ntasks_per_node = os.environ.get('COBALT_PARTCORES')
    return int(ntasks_per_node)
    
def get_lsb_cores_per_node():
    # LSB_MCPU_HOSTS=batch2 1 h22n07 42 h22n08 42 
    lsb_mcpu = os.environ.get('LSB_MCPU_HOSTS')
    host_core = lsb_mcpu.split()
    return int(host_core[-1])


def get_job_cores_per_node(defaultCores = 0):
    """Query the job environment for the number of cores per node to use, if available"""
    if defaultCores == 0:
        defaultCores = get_hdw_cores_per_node()
    if 'GASNET_PSHM_NODES' in os.environ:
        print("Detected procs_per_node from GASNET_PSHM_NODES=",os.getenv('GASNET_PSHM_NODES'))
        return int(os.getenv('GASNET_PSHM_NODES'))
    ntasks_per_node = None
    if is_slurm_job():
        ntasks_per_node = get_slurm_cores_per_node(defaultCores)
    if is_lsb_job():
        ntasks_per_node = get_lsb_cores_per_node()
    if is_cobalt_job():
        ntasks_per_node = get_cobalt_cores_per_node()
    if ntasks_per_node is not None:
        return ntasks_per_node
    return defaultCores

def get_slurm_job_nodes():
    """Query the SLURM job environment for the number of nodes"""
    nodes = os.environ.get('SLURM_JOB_NUM_NODES')
    if nodes is None:
        nodes = os.environ.get('SLURM_NNODES')
    if nodes:
        return int(nodes)
    message("Warning: could not determine the number of nodes in this SLURM job (%s). Only using 1" % (get_job_id()))
    return 1

def get_lsb_job_nodes():
    """Query the LFS job environment for the number of nodes"""
    # LSB_MCPU_HOSTS=batch2 1 h22n07 42 h22n08 42 
    nodes = os.environ.get('LSB_MCPU_HOSTS')
    if nodes:
        return int( (len(nodes.split()) - 2) / 2)
    message("Warning: could not determine the number of nodes in this LSF job (%s). Only using 1" % (get_job_id()))
    return 1


def get_pbs_job_nodes():
    """Query the PBS job environment for the number of nodes"""
    nodesfile = os.environ.get('PBS_NODEFILE')
    if nodesfile is not None:
        nodes = 0
        with open(nodesfile, 'r') as f:
            for line in f:
                nodes += 1
        return nodes
    message("Warning: could not determine the number of nodes in this PBS job (%s). Only using 1" % (get_job_id()))
    return 1

def get_ge_job_nodes():
    """Query the Grid Engine job environment for the number of nodes"""
    nodes = os.environ.get("NHOSTS")
    if nodes is not None:
        return int(nodes)
    message("Warning: could not determine the number of nodes in this SGE job (%s). Only using 1" % (get_job_id()))
    return 1
    
def get_cobalt_job_nodes():
    """Query the COBALT job environment for the number of nodes"""
    nodes = os.environ.get("COBALT_JOBSIZE")
    if nodes is not None:
        return int(nodes)
    message("Warning: could not determine the number of nodes in this COBALT job (%s). Only using 1" % (get_job_id()))
    return 1
    
def get_job_nodes():
    """Query the job environment for the number of nodes"""
    if is_slurm_job():
        return get_slurm_job_nodes()
    if is_lsb_job():
        return get_lsb_job_nodes()
    if is_pbs_job():
        return get_pbs_job_nodes()
    if is_ge_job():
        return get_ge_job_nodes()
    if is_cobalt_job():
        return get_cobalt_job_nodes()
    message("Warning: could not determine the number of nodes in this unsupported scheduler job (%s). Only using 1" % (get_job_id()))
    return 1

def get_job_desc():
    return get_job_id() + " (" + get_job_name() + ")"

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def round(s):
    return int(float(s) + 0.5)

def load_args():
    # some defaults
    #message("Detecting hardware and jobs")
    version = get_version()
    cores = get_hdw_cores_per_node()
    nodes = 1
    if get_job_id() is not None:
        cores = get_job_cores_per_node(cores)
        nodes = get_job_nodes()
        message("Detected a job id=%s with %d nodes and %d cores_per_node and head_node: %s\n" % (get_job_id(), nodes, cores, os.uname()[1]))
    threads = nodes * cores
    isMultiNode = nodes != 1
 
    desc = 'HipMer De Novo Genome Assembler, ' + version
    datestr = str(datetime.datetime.now())
    exec_file_time = os.path.getmtime(__file__)
    message(desc, '(' + str(datetime.datetime.fromtimestamp(exec_file_time)) + ')\n')
    parser = OrderedArgumentParser(description=desc,
                                   epilog="""
Multiple libraries can be specified for each of -s, -p and -i, with paths to fastq file(s) and optional modifiers applied to each, for example:

 -p libA-R1.fastq,libA-R2.fastq:nosplint -i libB.fastq:i500:s75
                                   
If multiple fastq files exist for a library, a library definition file can be passed in instead of listing all the fastqs on the command line.
The definition files must end  with ".fofn" extensions are treated as files of file names, with ONE file per line for single-read (-s) or interleaved-pairs (-i) input, or TWO comma-separated files per line for split-read pairs (-p), e.g.:

 -p libA.fofn:nosplint -s libB.fofn

Additionally, for single-read (-s) or interleaved-pairs (-i) libraries, the files can be specfied with glob expansion wildcards within the fofn
   (and on the command line with appropriate escaping to prevent shell expansion)       

Libraries (and fofn) can be specified with the following modifiers:
   :i<insertSize> with :s<standardDeviation>  (for paired or interleaved libraries, of course). If omitted, HipMer will calculate these automatically based on the assembly. 
   :rc (library is reverse forward <--  -->, and has a small % of "innie" reads with a very short insert size: --> <--)
   :nocontig (do not use this library in the contig stages - i.e. high indel rate)
The following modifiers apply only to single genome assembly:
   :nosplint (do not use this library for splinting - i.e. high chimeric rate)
   :nogapclosing (do not use this library in the gapclosing stage)
""")

    local_tmp = os.environ.get("HIPMER_LOCAL_TMP")
    if local_tmp is None:
        local_tmp = "/dev/shm"

    default_outdir = 'hipmer-run'
    if get_job_id() is not None:
        name = get_job_name()
        if name is not None and name not in [ "wrap", "sh", "csh", "bash" ]:
            default_outdir = name
        default_outdir += "-" + get_job_id()
    default_outdir += '-' + datestr.replace(' ', '-').replace(':', '')
    parser.add_basic_argument('-h', '--help', dest='help_opt', default=False, action='store_true', help='Show options')
    parser.add_basic_argument('-o', '--outdir', dest='outdir', type=str, default=default_outdir,
                              help='Output directory')
    parser.add_basic_argument('-k', '--kmer', dest='kmer_lens', type=str, default='21,33,55,77,99', 
                              help='Comma-separated list of kmer lengths')
    parser.add_basic_argument('-s', '--single', dest='single_libs', type=str, default=[], action='append',
                              help='Single read library, with modifiers - see below')
    parser.add_basic_argument('-p', '--paired', dest='paired_libs', type=str, default=[], action='append',
                              help='Paired read library, with modifiers - see below')
    parser.add_basic_argument('-i', '--interleaved', dest='interleaved_libs', type=str, default=[], action='append',
                              help='Interleaved read library, with modifiers - see below')
    
    parser.add_basic_argument('--meta', dest='meta', default=False, action='store_true',
                              help='Assemble metagenome')
    parser.add_basic_argument('--merge-reads', dest='merge_reads', type=int, default=1, choices=[0,1,2],
                              help="Merge overlapping paired reads: 0 - never, 1 - always, 2 - in metagenome scaffolding only")
    parser.add_basic_argument('--quality', dest='quality_tuning', default="normal", type=str, 
                              help="Quality tuning for metagenomes: minerror, normal or maxctgy")
    
    parser.add_basic_argument('--cached-io', dest='cached_io', default=isMultiNode, type=str2bool, nargs='?', const=True,
                              help='Use cached IO, on by default when multi-node')
    parser.add_basic_argument('--local-tmp-dir', dest='local_tmp_dir', default=local_tmp, type=str, 
                              help='Local disk where temporary per-rank files can be stored')
    parser.add_basic_argument('--checkpoint', dest='checkpoint', default=None, type=str2bool, nargs='?', const=True,
                              help='Checkpoint after successful stages, on by default with cached-io')
    parser.add_basic_argument('--cleanup', dest='cleanup', default=None, type=str2bool, nargs='?', const=True,
                              help='Cleanup all intermediate files after successful completion (default True)\n')
    parser.add_basic_argument('--auto-restart', dest='auto_restart', default=True, type=str2bool, nargs='?', const=True,
                              help='Attempt to restart if a stage fails')
    parser.add_basic_argument('--resume', dest='resume', default=None, type=str2bool, nargs='?', const=True, 
                              help='Attempt to resume an aborted or interrupted run. Default is to autodetect.\n' + \
                              '                                      Set to False to avoid an attempted resume of an existing run directory.')
    parser.add_basic_argument('-v', '--verbose', dest='verbose', default=False, action='store_true',
                              help='Verbose output: a *lot* of detailed run information')
    
    parser.add_basic_argument('--advanced-help', dest='advanced_help_opt', default=False, action='store_true',
                              help='Show more advanced options')

    # restarting
    parser.add_advanced_argument('--restart-stage', dest='restart_stage', type=int, default=None, 
                                 help='Restart from given stage')
    parser.add_advanced_argument('--list-stages', dest='list_stages', default=False, action='store_true', 
                                 help='List the stages previously executed')
    parser.add_advanced_argument('--rebuild-config', dest='rebuild_config', default=False, action='store_true', 
                                 help='Rebuild the config file before restarting')

    # overridable default parameters
    parser.add_advanced_argument("--min-scaffold-length", dest='assm_scaff_len_cutoff', type=int, default=500, 
                              help="Minimum length of output scaffolds in the resulting assembly (shorter will be in a separate file)")
    parser.add_advanced_argument('--num-scaffold-cycles', dest='num_scaff_loops', type=int, default=1,
                                 help='Number of scaffolding cycles using all libraries/sets')    
    parser.add_advanced_argument('--scaff-kmer-len', dest='scaff_kmer_len', type=int, default=None,
                                 help='kmer length for scaffolding')
    parser.add_advanced_argument('--min-depth', dest='min_depth', type=int, default=None, 
                                 help='Min depth. default 0 (autodetect) for single genoems or 2 for metagenomes')
    parser.add_advanced_argument('--dynamic-min-depth', dest='dynamic_min_depth', type=float, default=1.0,
                                 help='Dynamic min depth')
    parser.add_advanced_argument('--alpha', dest='alpha', type=float, default=0.1, help='alpha')
    parser.add_advanced_argument('--beta', dest='beta', type=float, default=0.2, help='beta')
    parser.add_advanced_argument('--tau', dest='tau', type=float, default=2.0, help='tau')
    
    parser.add_advanced_argument('--bubble-depth-cutoff', dest='bubble_depth_cutoff', type=int, default=None,
                                 help='Bubble min depth cutoff. default 0 (autodetect) for single genome or 1 for metagenome')
    parser.add_advanced_argument('--ono-pair-thresholds', dest='ono_pair_thresholds', type=str, default='NOT_SPECIFIED',
                                 help='oNo pair thresholds. by default 3,10 for single genomes or 1,2 for metagenome')
    parser.add_advanced_argument('--diploidy', dest='diploid', type=str, default='none', choices=['none', 'low', 'high'],
                                 help='Diploidy - none, low or high')
    parser.add_advanced_argument('--long-reads', dest='long_reads', default=None, type=str2bool, nargs='?', const=True,
                                 help='Long reads for scaffolding (pacBio CCS reads currently the only type supported)')

    parser.add_advanced_argument('--min-gap-size', dest='min_gap_size', type=int, default=3, help='min gap size')
    parser.add_advanced_argument('--localize-reads', dest='localize_reads', default=None, type=str2bool, nargs='?', const=True,
                                 help='Localize reads (on by default with cached_io)')
    parser.add_advanced_argument('--hmm', dest='hmm', type=str, default=None, help='HMM file')
    parser.add_advanced_argument('--hll', dest='hll', default=isMultiNode, type=str2bool, nargs='?', const=True,
                                 help='Hyper-Log-Log estimates (HLL) in UFX stage on by default when multi-node')
    parser.add_advanced_argument('--hh', dest='hh', default=isMultiNode, type=str2bool, nargs='?', const=True,
                                 help='HeavyHitters (must have HLL too) in UFX stage on by default when multi-node')
    parser.add_advanced_argument('--cached-reads', dest='cached_reads', default=None, type=str2bool, nargs='?', const=True,
                                 help="Load reads to cached IO if that is set, default true when --cached-io is true")

    parser.add_advanced_argument('--la-rating-thres', dest='la_rating_thres', type=int, default=0, 
                                 help='Rating threshold for local assembly')

    parser.add_advanced_argument('--fast', dest='fast', default=False, type=str2bool, nargs='?', const=True,
                                 help='Run faster assemblies (not on by uses more memory)\n')

    # job parameters
    parser.add_advanced_argument('-t', '--threads', dest='threads', type=int, default=threads, 
                              help='Number of threads. default autodetect')
    parser.add_advanced_argument('--cores-per-node', dest='cores_per_node', type=int, default=cores,
                              help='Cores per node. default autodetect')
    parser.add_advanced_argument('--shared-heap', dest='shared_heap', type=int, default=None, 
                                 choices=range(2, 91),
                                 help='Size of UPC shared heap as percentage of system memory, per thread')
    parser.add_advanced_argument('--physmem-max', dest='physmem_max', type=int, default=0,
                                 help='Sized of physical memory so as to avoid a slow memory probe by gasnet')

    parser.add_advanced_argument('-I', '--illumina_version', dest='illumina_version', type=int, default=None, choices=[13, 15, 18], 
                                 help='Specify Illumina version')

    # developer options

    parser.add_advanced_argument('--benchmark', dest='benchmark', default=False, action='store_true',
                                 help="Optimize IO so checkpoints will not be saved")
    parser.add_advanced_argument('--upc-verbose', dest='upc_verbose', default=False, action='store_true',
                                 help="call upcrun with extra verbosity")
    parser.add_advanced_argument('--test-checkpointing', dest="test_checkpointing", default=False, action='store_true',
                                 help="Developer testing mode that dies at every stage and purges local checkpoint files")
    parser.add_advanced_argument('--canonicalize_output', dest='canonicalize_output', default=False, action='store_true',
                                 help='Canonicalize the final assembly for use with checksums and validation')
    parser.add_advanced_argument('--no-slurm-bcast', dest='no_slurm_bcast', default=False, action='store_true',
                                 help="disable entirely the SLURM_BCAST optimization that helps large jobs start")
    parser.add_advanced_argument('--DoNotUse-CheckpointRestart', dest='checkpoint_restart', default=None, 
                                 help="internally used to prevent infinite restarts of failed checkpoints")
    
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args()
    # translate outdir to be fully qualified
    if args.outdir[0] != '/':
        args.outdir = os.path.join(os.getcwd(), args.outdir)
    has_run_log = os.path.exists(args.outdir + '/run.log')
    if args.help_opt:
        parser.print_help()
        sys.exit(0)
    if args.advanced_help_opt:
        parser.print_help(advanced=True)
        sys.exit(0)
    if args.list_stages:
        _, list_str = list_stages(args.outdir)
        message('Available stages (use index to request):\n\n' + list_str)
        sys.exit(0)
    if (args.resume is not None and not args.resume) and has_run_log:
        os.rename(args.outdir + "/run.log", args.outdir + "/run.log-%s" % (datetime.datetime.now().strftime('%Y%m%d_%H%M%S')))
    if args.restart_stage is None and args.resume is None and has_run_log:
        message('Detected an existing run in ', args.outdir, ', setting --resume=True\n')
        args.resume = True
    if not args.single_libs and not args.paired_libs and not args.interleaved_libs and not args.resume \
       and args.restart_stage is None:
        die('Need to specify at least one library or restart a previous run')
    if os.path.exists(args.outdir + '/ALL_INPUTS.fofn') and (not args.resume) and args.restart_stage is None:
        # This is a clean restart.  remove any fofns that may be lingering aroung
        if os.path.isdir(args.outdir + '/.old_run'):
            shutil.rmtree(args.outdir + '/.old_run', ignore_errors=True)
        os.mkdir(args.outdir + '/.old_run')
        fofns = glob.glob(args.outdir + "/[a-z][a-z].fofn*") + glob.glob(args.outdir + "/[a-z][a-z]-[0-9]*.fofn*") + \
                glob.glob(args.outdir + "/ALL_INPUTS.fofn*")
        #message("Purging old fofns: ", fofns)
        for fofn in fofns:
            os.rename(fofn, args.outdir + '/.old_run/' + os.path.basename(fofn))
    if args.resume:
        stage_list, _ = list_stages(args.outdir)
        if len(stage_list) == 0:
            print("There are no stages available to resume from.  Restarting from the beginning")
            args.resume = False
            args.restart_stage = None
        else:
            args.restart_stage = len(stage_list) - 1
    if args.restart_stage is not None:
        stage_list, _ = list_stages(args.outdir)
        if len(stage_list) < args.restart_stage:
            _, list_str = list_stages(args.outdir)
            die('Request for stage', args.restart_stage, 'outside of stage list. Available stages are:\n\n' + list_str)
        args.restart_stage = stage_list[args.restart_stage]
    if args.cores_per_node > args.threads:
        args.cores_per_node = args.threads
    if not args.list_stages:
        always = ['threads', 'cores_per_node', 'outdir', 'kmer_lens']
        if args.restart_stage is not None:
            parser.print_args(always=always, mods={'restart_stage': args.restart_stage})
        else:
            parser.print_args(always=always)
    kmer_lens = args.kmer_lens.split(',')
    if args.scaff_kmer_len is None:
        if args.meta:
            if len(kmer_lens) > 2:
                args.scaff_kmer_len = kmer_lens[1]
            else:
                args.scaff_kmer_len = kmer_lens[-1]
        else:
            args.scaff_kmer_len = kmer_lens[-1]
    else:
        if int(args.scaff_kmer_len) > int(kmer_lens[-1]):
            print("Scaffold kmer length (", args.scaff_kmer_len, ") should be <= max kmer length (", kmer_lens[-1], ")")
            sys.exit(0)
    if args.ono_pair_thresholds == 'NOT_SPECIFIED':
        if args.meta:
           args.ono_pair_thresholds = '1,2'
        else:
           args.ono_pair_thresholds = '3,10'
    if args.min_depth is None:
        if args.meta:
            args.min_depth = 2
        else:
            args.min_depth = 0
    if args.bubble_depth_cutoff is None:
        if args.meta:
            args.bubble_depth_cutoff = 1
        else:
            args.bubble_depth_cutoff = 0
    if args.cached_io and args.checkpoint is None:
        args.checkpoint = True
    if args.localize_reads is None:
        args.localize_reads = args.cached_io
    if args.cached_reads is None:
        args.cached_reads = args.cached_io
    if args.cleanup is None:
        args.cleanup = True
    if args.benchmark and args.cleanup:
        args.cleanup = False
    if args.benchmark and args.checkpoint:
        args.checkpoint = False
    if args.checkpoint is None:
        args.checkpoint = False
    args.single_libs = unpack_fofn(args.single_libs)
    args.paired_libs = unpack_fofn(args.paired_libs)
    args.interleaved_libs = unpack_fofn(args.interleaved_libs)
    return args


mem_tracker = RepeatTimer(mem_tracker_stop_flag)

orig_sighdlr = None
proc = None

def exit_all(status):
    mem_tracker_stop_flag.set()
    if proc:
        #os.kill(proc.pid, signal.SIGINT)
        try:
            proc.terminate()
        except OSError:
            pass
            #print("Process ", proc, " is already terminated\n")
    sys.exit(status)

def handle_interrupt(signum, frame):
    print('\n\nInterrupt received, signal', signum)
    signal.signal(signal.SIGINT, orig_sighdlr)
    exit_all(1)


def check_for_pair(lib):
    lib_names = lib.split(',')
    if len(lib_names) != 2 or os.path.splitext(lib_names[0])[1] not in ['.fq', '.fastq'] \
       or os.path.splitext(lib_names[1])[1] not in ['.fq', '.fastq']:
        die('For paired lib, must specify two fastq files, comma separated for paired reads. "' + lib + '" is not valid.')


def get_read_len(lib_name):
    try:
        with open(lib_name, 'r') as f:
            i = 0
            read_len = None
            for line in f:
                if i % 4 == 0 and line[0] != '@':
                    die('Corrupted fastq file', lib_name, line)
                if i % 4 == 1:
                    if read_len and len(line.strip()) != read_len:
                        #print('found lines of length', read_len, len(line.strip()))
                        die('Currently variable length reads are not supported with paired read files')
                    read_len = len(line.strip())
                if i > 400:
                    return read_len
                i += 1
    except IOError:
        die('Could not open or read or parse', lib_name)

        
def setup_config_file(options):
    with open(options.outdir + '/config.txt', 'w') as cfg_file:
        cfg_file.write('%-30s %s\n' % ('mer_sizes', options.kmer_lens))
        cfg_file.write('%-30s %s\n' % ('scaff_mer_size', options.scaff_kmer_len))
        cfg_file.write('%-30s %d\n' % ('is_metagenome', 1 if options.meta else 0))
        cfg_file.write('%-30s %s\n' % ('min_depth_cutoff', options.min_depth))
        if options.meta and options.dynamic_min_depth == 1.0:
            options.dynamic_min_depth = 0.9
        cfg_file.write('%-30s %d\n' % ('assm_scaff_len_cutoff', options.assm_scaff_len_cutoff))
        cfg_file.write('%-30s %s\n' % ('alpha', options.alpha))
        cfg_file.write('%-30s %s\n' % ('beta', options.beta))
        cfg_file.write('%-30s %s\n' % ('tau', options.tau))
        cfg_file.write('%-30s %s\n' % ('dynamic_min_depth', options.dynamic_min_depth))
        cfg_file.write('%-30s %s\n' % ('error_rate', options.dynamic_min_depth))
        cfg_file.write('%-30s %d\n' % ('bubble_min_depth_cutoff', options.bubble_depth_cutoff))
        cfg_file.write('%-30s %d\n' % ('merge_reads', options.merge_reads))
        if options.diploid != 'none':
            cfg_file.write('%-30s %d\n' % ('is_diploid', 1))
            if options.diploid == 'high':
                cfg_file.write('%-30s %d\n' % ('high_heterozygosity', 1))
        if options.long_reads:
            cfg_file.write('%-30s %d\n' % ('ono_w_long_reads', 1))

        cfg_file.write('%-30s %d\n' % ('num_scaff_loops', options.num_scaff_loops)) 
        cfg_file.write('%-30s %s\n' % ('ono_pair_thresholds', options.ono_pair_thresholds))
        cfg_file.write('%-30s %d\n' % ('min_gap_size', options.min_gap_size))
        cfg_file.write('%-30s %d\n' % ('localize_reads', 1 if options.localize_reads else 0))
        if options.hmm:
            cfg_file.write('%-30s %s\n' % ('hmm_model_file', options.hmm))
        if options.canonicalize_output:
            cfg_file.write('%-30s %s\n' % ('canonicalize_output', 1))

        # localassm parameters
        cfg_file.write('%-30s %f\n' % ('la_depth_dist_thres', 1.0))
        cfg_file.write('%-30s %f\n' % ('la_nvote_diff_thres', 2.0))
        cfg_file.write('%-30s %d\n' % ('la_mer_size_min', 19))
        la_mer_size_max = 121
        max_kmer = int(options.kmer_lens.split(',')[-1])
        if la_mer_size_max <= max_kmer:
            la_mer_size_max = max_kmer + 2
        cfg_file.write('%-30s %d\n' % ('la_mer_size_max', la_mer_size_max))
        cfg_file.write('%-30s %d\n' % ('la_qual_thres', 10))
        cfg_file.write('%-30s %d\n' % ('la_hi_qual_thres', 20))
        cfg_file.write('%-30s %f\n' % ('la_min_viable_depth', 0.2))
        cfg_file.write('%-30s %f\n' % ('la_min_expected_depth', 0.3))
        cfg_file.write('%-30s %f\n' % ('la_rating_thres', options.la_rating_thres))
        if options.meta:
            cfg_file.write('%-30s %f\n' % ('gap_close_rpt_depth_ratio', 0))
            cfg_file.write('%-30s %d\n' % ('cgraph_scaffolding', 1))
        else:
            cfg_file.write('%-30s %f\n' % ('gap_close_rpt_depth_ratio', 1.75))
        
        # libraries
        libs = []
        for lib in options.single_libs + options.paired_libs + options.interleaved_libs:
            if lib in options.single_libs:
                files_per_pair = 0
            elif lib in options.interleaved_libs:
                files_per_pair = 1
            else:
                files_per_pair = 2
                
            # strip all modifiers
            if ':' in lib:
                fields = lib.split(':')
                lib_name = fields[0]
                mods = fields[1:]
                fail = False
                for mod in mods:
                    if    mod == 'rc' or mod == "nocontig" or mod == 'nosplint' or mod == 'nogapclosing' \
                       or (mod[0] in ['i', 's'] and is_number(mod[1:])) \
                       or ('fivewiggle' in mod and mod[10:].isdigit()) \
                       or ('threewiggle' in mod and mod[11:].isdigit()): 
                        continue
                    die('Invalid library "' + lib_name + '" modifier: "' + mod + '", only allowable values are "rc" and/or ' + \
                        '"i<number>" with "s<number>" and/or "nocontig" and/or "nosplint" and/or "nogapclosing" and/or ' + \
                        '"fivewiggle<number>" and/or "threewiggle<number>"')
            else:
                lib_name = lib
                mods = []
            first_lib_name = lib_name.split(',')[0] # could be paired
            if os.path.splitext(first_lib_name)[1] in ['.fq', '.fastq']:
                if files_per_pair == 2:
                    check_for_pair(lib_name)
                libs.append((lib_name, mods, files_per_pair))
            else:
                die("Failed to expand to .fq or .fastq: ", first_lib_name)

        # generate some names for lib prefixes
        prefixes = []
        for i in range(ord('a'), ord('z') + 1):
            for j in range(ord('a'), ord('z') + 1):
                prefixes.append(chr(i) + chr(j))

        ono_set_id = 1 
        lib_id = 0
        last_lib_ins = 0
        num_paired = 0
        cfg_file.write("\n# lib_seq [ wildcard ][ prefix ][ insAvg ][ insSdev ][ avgReadLen ][ hasInnieArtifact ][ isRevComped ]" +\
                       "[ useForContigging ][ onoSetId ][ useForGapClosing ][ 5pWiggleRoom ][3pWiggleRoom] [FilesPerPair] " +\
                       "[ useForSplinting ]\n\n")
        for lib in libs:
            lib_names = lib[0].split(',')
            full_lib_names = ''
            i = 0
            for lib_name in lib_names:
                if i == 1:
                    full_lib_names += ','
                if lib_name[0] != '/':
                    lib_name = os.path.abspath(os.getcwd() + '/' + lib_name)
                if not os.path.isfile(lib_name):
                    die('File "' + lib_name + '" does not exist or cannot be accessed')
                check_striping(lib_name)
                full_lib_names += lib_name
                symlink = options.outdir + '/' + os.path.split(lib_name)[1]
                if (os.path.exists(symlink) and not os.path.samefile(lib_name,symlink)) and (not os.path.islink(symlink)):
                    os.symlink(lib_name, symlink)
                i += 1

            _, mods, files_per_pair = lib
            cfg_file.write('lib_seq  %s %s ' % (full_lib_names, prefixes[lib_id]))
            read_len = 0
            if files_per_pair == 2:
                # get read length from file
                read_len = get_read_len(full_lib_names.split(',')[0])
            # ins and stddev
            lib_ins = 0 # unknown
            for mod in mods:
                if 'i' in mod and is_number(mod[1:]):
                    lib_ins = round(mod[1:])
                    break
            cfg_file.write('%d ' % (lib_ins))  # insavg
            lib_std = 0 # unknown
            for mod in mods:
                if 's' in mod and is_number(mod[1:]):
                    lib_std = round(mod[1:])
                    break
            cfg_file.write('%d ' % (lib_std))  #insstddev

            cfg_file.write('%d  ' % read_len)  # avgreadlen
            if 'rc' in mods:
                cfg_file.write('1 1 ') # innie, revcomped
            else:
                cfg_file.write('0 0 ')

            # use for contigs
            if 'nocontig' in mods:
                cfg_file.write('0  ') # useforcontig
            else:
                cfg_file.write('1  ') # useforcontig

            # ono set id
            if files_per_pair > 0: # paired libraries will increase the ono_set_id when insert sizes change significantly
#                if options.meta and num_paired == 0: # metagenome pairs start on ono_set_id == 2
#                    ono_set_id += 1 # start at ono_set 2 as the first round will be splints only
#                    print("increasing ono_set_id as this is the first metagenome paired library: ", ono_set_id, lib)
                # greater than 10% change in insert size, so use another ono round
                if lib_ins > 0 and num_paired > 0 and abs(lib_ins - last_lib_ins) / float(lib_ins) > 0.1: 
                    ono_set_id += 1
                    print("increasing ono_set_id as the insert size is significantly different from the last paired library: ",
                          ono_set_id, lib)
                num_paired += 1;
            last_lib_ins = lib_ins
            cfg_file.write('%2d ' % ono_set_id) # onosetid

            # use for gapclosing
            if 'nogapclosing' in mods:
                cfg_file.write('0 ') # not for gapclosing
            else:
                cfg_file.write('1 ') # use for gapclosing

            fivewiggle=5
            for mod in mods:
                if 'fivewiggle' in mod and mod[10:].isdigit():
                    fivewiggle=int(mod[10:])
                    break
            threewiggle=5
            for mod in mods:
                if 'threewiggle' in mod and mod[11:].isdigit():
                    threewiggle=int(mod[11:])
                    break
   
            cfg_file.write('%d %d  ' %(fivewiggle,threewiggle)) # wiggle rooms

            cfg_file.write('%d  ' % files_per_pair)  # files_per_pair

            # use for splinting
            if 'nosplint' in mods:
                cfg_file.write('0\n') # useforsplinting
            else:
                cfg_file.write('1\n') # useforsplinting
            lib_id += 1

def get_libname_mappings(config_file):
    libname_mappings = {}
    with open(config_file, 'r') as f:
        for line in f.readlines():
            if not line.startswith('lib_seq'):
                continue
            fields = line.split()
            libname_mappings[fields[2]] = fields[1]
    return libname_mappings


def get_libname_from_line(line, libname_mappings, key, split_char, no_ext=True):
    libname = line[(line.find(key) + len(key)):].split(split_char)[0].strip()
    if libname in libname_mappings:
        libname = os.path.split(libname_mappings[libname])[1]
        if no_ext:
            return os.path.splitext(libname)[0]
        else:
            return libname
    else:
        return libname

def handle_failure_termination(proc, run_log_file, verbose=False):
    # write remaining stderr to log
    print_log(run_log_file, "hipmer detected a failure in process pid=%d, reading output and terminating at %s" % \
              (proc.pid, str(datetime.datetime.now())))

    # first capture contents of /dev/shm
    try:
        print_log(run_log_file, "Contents of /dev/shm:\n")
        cmd = [ which('ls'), '-alR', '/dev/shm']
        devshm_proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in iter(devshm_proc.stdout.readline, b''):
            line = line.decode()
            run_log_file.write(line)

        devshm_proc.wait()
        run_log_file.flush()
    except:
        print_log(run_log_file, "Could not dump /dev/shm contents")

    # get any remaininginput, then send two interrupts and a term
    if proc:
        try:
            try:
                if proc.poll() is None:
                    time.sleep(1)
                    if proc.poll() is None:
                        run_log_file.write("Sending SIGINT at %s\n" % (str(datetime.datetime.now())))
                        proc.send_signal(2)
            except OSError:
                run_log_file.write("Process is already gone\n")
                pass
                  
            if proc.poll() is None:
                time.sleep(1)
                if proc.poll() is None:
                    run_log_file.write("Sending SIGINT again at %s\n" % (str(datetime.datetime.now())))
                    proc.send_signal(2)
                    time.sleep(1)
                    if proc.poll() is None:
                        time.sleep(1)
                        if proc.poll() is None:
                            run_log_file.write("Sending SIGTERM at %s\n" % (str(datetime.datetime.now())))
                            proc.terminate()
                            time.sleep(1)
                            if proc.poll() is None:
                                run_log_file.write("Sending SIGKILL at %s\n" % (str(datetime.datetime.now())))
                                proc.kill()
                                time.sleep(1)

            outs,errs = proc.communicate(None)
            if outs:
                outs = outs.decode()
                print_log(run_log_file, "Got some stdout from the failed process at %s" % (str(datetime.datetime.now())))
                if verbose:
                    print(outs)
                    sys.stdout.flush()
                run_log_file.write(outs)
                for line in outs:
                    if 'srun: error' in line and 'REQUEST_FILE_BCAST' in line and os.environ.get('SLURM_BCAST') is not None: # Issue228
                        print_log(run_log_file, "Detected a SLURM_BCAST error, disabling that optimization.")
                        del os.environ['SLURM_BCAST']
                        os.environ['NO_SLURM_BCAST'] = '1'
            if errs:
                errs = errs.decode()
                print_log(run_log_file, "Got some stderr from the failed process at %s" % (str(datetime.datetime.now())))
                if verbose:
                    print(errs)
                    sys.stderr.write(errs)
                    sys.stderr.flush()
                    sys.stdout.flush()
                run_log_file.write(errs)
                for line in errs:
                    if 'srun: error' in line and 'REQUEST_FILE_BCAST' in line and os.environ.get('SLURM_BCAST') is not None: # Issue228
                        print_log(run_log_file, "Detected a SLURM_BCAST error, disabling that optimization.")
                        del os.environ['SLURM_BCAST']
                        os.environ['NO_SLURM_BCAST'] = '1'
        except OSError:
            run_log_file.write("Process is gone\n")
            pass
        except:
            print_log(run_log_file, "Unexpected error: ", sys.exc_info())
            traceback.print_tb(sys.exc_info()[2], limit=100)
            traceback.print_tb(sys.exc_info()[2], limit=100, file=run_log_file)
            pass
        
def run_upc(options, main_exec, nodes, restart_stage, run_log_file, libname_mappings):
    global proc
    
    cmd = ['upcrun', '-q' if not options.upc_verbose else '-v', '-n', str(options.threads), '-c', str(options.cores_per_node), '-N',
           str(int(nodes)), main_exec, '-f', 'config.txt', '-N', str(options.cores_per_node)]

    if options.hll:
        cmd.append('-H')
        if not options.hh:
            cmd.append('-n')

    if which("stdbuf"):
        cmd = [which("stdbuf"), '-oL', '-eL'] + cmd # force unbuffered stdout/stderr

    if options.cached_io:
        cmd.extend(['-B'])
        if not options.cached_reads:
            cmd.extend(['-b'])
    if options.local_tmp_dir:
        cmd.extend(['-l', options.local_tmp_dir])
    if options.illumina_version:
        cmd.extend(['-I', str(options.illumina_version)])
    if options.test_checkpointing:
        cmd.extend(['-T'])
        options.checkpoint = True
        options.auto_restart = True
        print_log(run_log_file, "Removing pre-existing per_rank files as we are in test-checkpointing mode")
        if options.cached_io:
            shutil.rmtree("%s/per_rank/00000000/"%(options.local_tmp_dir), ignore_errors=True)
        else:
            shutil.rmtree("per_rank/00000000/", ignore_errors=True)
    if options.checkpoint or options.restart_stage or options.auto_restart:
        cmd.extend(['-S'])
    if not options.checkpoint:
        cmd.extend(['-x'])
    if not options.cleanup:
        cmd.extend(['-c'])
    if restart_stage:
        cmd.extend(['-s', restart_stage + '-end'])
    if options.meta:
        cmd.extend(['-z'])
    if not options.fast:
        cmd.extend(['-F'])
    cmd.extend(['-q', options.quality_tuning])
    
    status = True
    stage = None

    try:
        # for debugging produce the execution environment in the run.log
        #runenv = dict(os.environ, UPC_SHARED_HEAP_SIZE=options.shared_heap, GASNET_PHYSMEM_MAX="%dm" % (options.physmem_max),
        #              GASNET_NOPROBE=str(1))
        runenv = dict(os.environ, UPC_SHARED_HEAP_SIZE=options.shared_heap)
        for env,val in runenv.items():
          if "SLURM" in env or "GASNET" in env or "CRAY" in env or "UPC" in env or "UPCXX" in env:
              run_log_file.write("environment: %s=%s\n" % (env,val))

        print_log(run_log_file, str(datetime.datetime.now()) + '\n' + 'Executing:\n' + ' '.join(cmd) + '\n')
        run_log_file.flush()

        if not options.verbose:
            print_log(run_log_file, '\n%-10s %-*s %-8s %-14s %s' % ('Time', STAGE_DESC_SIZE, 'Stage', 'Seconds', 'Mem avail (%)', 'Peak mem (GB)'))
        
        proc = subprocess.Popen(cmd, env=runenv,
                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stage = None
        desc = None
        final_success = None
        for line in iter(proc.stdout.readline, b''):
            line = line.decode()
            run_log_file.write(line)
            run_log_file.flush()
            if options.verbose:
                print(line, end='')
                sys.stdout.flush()
            if True:
                if 'step creation' in line:
                    print(line.strip())
                if 'DIE' in line:
                    if 'Could not load UFX: ALL_INPUTS.fofn' in line:
                        print_log(run_log_file, '\n\nERROR: cannot restart, missing checkpoint files\n')
                    elif not options.verbose:
                        print('\n\n' + line + '\n')
                    if options.test_checkpointing and 'Stopping to test checkpointing' not in line:
                        print_log(run_log_file, "Checkpointing test failed with an unexpected error! ", line)
                        options.auto_restart = False
                        stage = None
                    if 'Can not read checkpoint' in line and options.restart_stage is not None and restart_stage is not None and restart_stage == options.restart_stage and options.checkpoint_restart is None:
                        test_tmp_file = re.search("Can not read checkpoint '([^']+)' ", line)
                        check_file = "./intermediates/%s.tmp"%(test_tmp_file.group(1))
                        print_log(run_log_file, "Could not restore checkpoint looking for %s\n" % (check_file))
                        if test_tmp_file.group(1) is not None and os.path.exists(check_file):
                            all_stages, _ = list_stages(".")
                            prev_stage = None
                            for s in all_stages:
                                if s == restart_stage:
                                    break
                                prev_stage = s 
                            if prev_stage is not None and prev_stage != restart_stage:
                                print_log(run_log_file, "Could not restart at stage %s as the restart files are not present, rewinding one more stage: %s" % (restart_stage, prev_stage))
                                options.restart_stage = prev_stage
                                options.checkpoint_restart = options.auto_restart
                                options.auto_restart = True
                                stage = prev_stage
                                status = False
                                break 

                    print_log(run_log_file, "Aborting on DIE ...");
                    status = False
                    break
                if ('ERROR' in line and not ('ENV parameter:' in line or 'MPICH_' in line)) or 'srun: error' in line or 'UPC Runtime error' in line:
                    if not options.verbose:
                        print('\n\n' + line + '\x1B[0m\n')
                    if 'srun: error' in line and 'REQUEST_FILE_BCAST' in line and os.environ.get('SLURM_BCAST') is not None: # Issue228
                        print_log(run_log_file, "Detected a SLURM_BCAST error, disabling that optimization.")
                        del os.environ['SLURM_BCAST']
                        os.environ['NO_SLURM_BCAST'] = '1'
                    print_log(run_log_file, "Aborting on 'ERROR' or 'srun: error' or 'UPC runtime error' ...", line);
                    status = False
                    break
                if 'WARN' in line:
                    skip_warning = False
                    for msg in ['Distributed hash table had']:
                        if msg in line:
                            skip_warning = True
                            break
                    if not skip_warning:
                        if not options.verbose:
                            pos = line.find(']:')
                            if pos > 0:
                                print('\nWARNING:', line[pos + 2:].strip())
                            else:
                                print('\n' + line.strip())
                        if desc and not options.verbose:
                            print('%-10s %-*s' % ('...', STAGE_DESC_SIZE, desc), end=' ')
                if 'UPC Runtime warning' in line:
                    print_log(run_log_file, "\n" + line.strip())
                if line.find('# Starting stage') != -1:
                    stage_t = dtimer()
                    stage = line.split(' ')[3]
                    stage_name = stage.split('-')[0]
                    if not options.verbose and stage_name in stage_descs:
                        if stage_name == 'ufx' or stage_name == 'kcount':
                            print('\n%10s Contig generation, k = %s' % ('', stage.split('-')[1]))
                        desc = stage_descs[stage_name]
                        if not options.meta:
                            if stage_name in ['merAligner', 'splinter', 'spanner', 'merAlignerAnalyzer']:
                                desc += ', ' + get_libname_from_line(line, libname_mappings, '-l ', '-')
                            elif stage_name == 'bmaToLinks':
                                desc += ', ' + get_libname_from_line(line, libname_mappings, '-b ', '-')
                        print('%-10s %-*s' % (datetime.datetime.now().strftime('%H:%M:%S'), STAGE_DESC_SIZE, desc), end=' ')
                    sys.stdout.flush()
                if line.find('# Finished ') != -1:
                    _, avail_mem_perc, _, mem = get_mem_stats()
                    if not options.verbose:
                        print('%-8.1f %-14.0f %.2f' % (dtimer() - stage_t, avail_mem_perc, (float(mem_tracker.peak_mem) / 1024)))
                    sys.stdout.flush()
                if line.find('scaffolding loop') != -1 or line.find('Scaffolding') != -1:
                    print('\n%10s Scaffolding' % '')
                if line.find('# Success, exit 0') != -1:
                    final_success = True

        if status:
            proc.wait()
        else:
            handle_failure_termination(proc, run_log_file, options.verbose)
            proc.wait()

        if proc.returncode not in [0, -15] or not status:
            if not status:
                print_log(run_log_file, "\nWARNING: reading the log shows an error");
            if final_success:
                print_log(run_log_file, "\nWARNING: upcrun subprocess terminated with exit code", proc.returncode, "... but HipMer DID completed successfully\n")
                status = True
            else:
                print_log(run_log_file, '\nERROR: upcrun subprocess terminated with exit code', proc.returncode, '\nCheck run.log for errors')
                status = False

        run_log_file.flush()

        if status:
            cmd = ['parse_run_log.pl', 'run.log']
            with open('run.summary', 'w') as f:
                proc2 = subprocess.Popen(cmd, env=runenv,
                                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                lastLine = ""
                for line in iter(proc2.stdout.readline, b''):
                    line = line.decode()
                    lastLine = line
                    f.write(line)
                proc2.wait() # do not check returncode -- okay to fail
                print_log(run_log_file, "\n")
                print_log(run_log_file, lastLine)
            
        #print_log(run_log_file, "Returning status", status, "stage", stage)
        return status, stage

    except:
        if proc:
            try:
                print_log(run_log_file, "Terminating process after exception: ", sys.exc_info(), "\n")
                traceback.print_tb(sys.exc_info()[2], limit=100)
                traceback.print_tb(sys.exc_info()[2], limit=100, file=run_log_file)
                proc.terminate()
            except OSError:
                pass
            except:
                print_log(run_log_file, "Unexpected error in final except: ", sys.exc_info())
                traceback.print_tb(sys.exc_info()[2], limit=100)
                traceback.print_tb(sys.exc_info()[2], limit=100, file=run_log_file)
                raise
        raise


def get_interleaved_libs(options):
    libs = []
    with open('config.txt', 'r') as f:
        for line in f.readlines():
            if line.startswith('lib_seq'):
                line.rstrip()
                lib_fields = line.split()
                libfile = lib_fields[1]
                libname = lib_fields[2]
                files_per_pair = int(lib_fields[13])
                nosplint = lib_fields[14]
                if nosplint == '0':
                    #print("excluding no splint lib", libname)
                    continue
                if files_per_pair == 2:
                    # okay now, paired libs will still be merged
                    path = os.path.split(libfile.split(',')[0])[1]
                    if options.merge_reads == 1:
                        path = path + "-merged.fastq"
                    if options.meta and options.localize_reads and "," in options.kmer_lens:
                        path = path + "_shuffled.fastq"
                    if options.merge_reads == 2:
                        path = path + "-merged.fastq"
                    libs.append((libname, path, files_per_pair))
                    #print("added paired library: ", libname)
                elif files_per_pair == 1:
                    path = os.path.split(libfile)[1]
                    if options.merge_reads == 1:
                        path = path + "-merged.fastq"
                    if options.meta and options.localize_reads and "," in options.kmer_lens:
                        path = path + "_shuffled.fastq"
                    if options.merge_reads == 2:
                        path = path + "-merged.fastq"
                    libs.append((libname, path, files_per_pair))
                    #print("added interleaved library: ", libname)
                else:
                    path = os.path.split(libfile)[1]
                    if options.meta and options.localize_reads and "," in options.kmer_lens:
                        path = path + "_shuffled.fastq"
                    libs.append((libname, path, files_per_pair))
                    #print("added single library: ", libname)
    return libs


def main():
    global orig_sighdlr
    global cached_io

    start_t = dtimer()

    orig_sighdlr = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, handle_interrupt)
    options = load_args()
    cached_io = options.cached_io
    
    tot_mem_mb, avail_mem_perc, _, mem = get_mem_stats()
    options.physmem_max = tot_mem_mb;
    message('\nTotal physical memory on node zero: %.2f GB' % (tot_mem_mb / 1024))
    message('Available memory on node zero: %.0f %%' % avail_mem_perc)
    
    # assist memory management at scale
    
    # set GASNET_COLL_SCRATCH_SIZE
    coll_scratch_threshold = 80 * 1024 # above 85k ranks needs more than 2048K, the default in upc & upc++ 2020.3
    if options.threads > coll_scratch_threshold and os.environ.get('GASNET_COLL_SCRATCH_SIZE') is None:
        required_col_scratch_mb = int((2048 * int(options.threads) + coll_scratch_threshold - 1) / coll_scratch_threshold)
        os.environ['GASNET_COLL_SCRATCH_SIZE'] = '%dK' % required_col_scratch_mb
        message("Set GASNET_COLL_SCRATCH_SIZE to ", os.environ['GASNET_COLL_SCRATCH_SIZE'])
        
    memory_overhead = options.threads * 0.025 / options.cores_per_node
    message('Expecting an extra %0.3f MB overhead per rank from GASNET at this scale.  Removing half this from requested shared heap.' % (memory_overhead))


    # use shared heap from environment, if not specified on the command line
    # options.shared_heap will be a string afterwards
    if options.shared_heap is None and os.environ.get("UPC_SHARED_HEAP_SIZE") is not None:
        message("Using the UPC_SHARED_HEAP_SIZE from the environment: %s\n" % (os.environ.get("UPC_SHARED_HEAP_SIZE")))
        options.shared_heap = os.environ.get("UPC_SHARED_HEAP_SIZE")
        if not options.shared_heap[-1].isdigit():
            # transform to mb
            unit = options.shared_heap[-1]
            if unit in ('m','M'):
                options.shared_heap = str(int(options.shared_heap[:-1]))
            elif unit in ('k','K'):
                options.shared_heap = str((int(options.shared_heap[:-1]) + 1023) / 1024)
        if options.shared_heap[-1].isdigit():
            # assume is megabyte
            sh = int(options.shared_heap) - int(memory_overhead / 2)
            if sh > 100:
                options.shared_heap = str(sh)
                message("Adjusting UPC_SHARED_HEAP_SIZE to %dm accounting for GASNET overhead\n" % sh)
            options.shared_heap = options.shared_heap + "m"
    else:
        if options.shared_heap is None:
            if options.cached_reads:
                # half if the reads will be stored in /dev/shm
                options.shared_heap = 50
            elif options.cached_io:
                # 2/3rds if /dev/shm will be used for checkpoints
                options.shared_heap = 67
            else:
                # 3/4ths if no /dev/shm will be used
                options.shared_heap = 75
        options.shared_heap = int(float(options.shared_heap) / 100.0 * avail_mem_perc / 100.0 * tot_mem_mb / options.cores_per_node - memory_overhead/2)
        message('Setting shared heap per thread to %s MB' % (options.shared_heap))
        options.shared_heap = "%dm" % (options.shared_heap)

    nodes = int( (options.threads + options.cores_per_node - 1) / options.cores_per_node)
    if nodes > 1:
        message("Executing on %d nodes" % (nodes))
    else:
        message("Executing on a single node: %s" % (os.uname()[1]))

    if os.path.exists(options.outdir):
        if options.resume or options.restart_stage is not None:
            if not options.rebuild_config:
                message('Restarting with stage %s, will ignore all options and use previous values' % (options.restart_stage))
                if not os.path.isfile(options.outdir + '/config.txt'):
                    die('Cannot restart previous run in the existing output directory "' + options.outdir + '"')
            else:
                message("Restarting with stage %s, will rebuild the config file with current options" % (options.restart_stage))

    setup_outdir(options)

    if options.rebuild_config or options.restart_stage is None:
        #message("Creating config file");
        setup_config_file(options)

    try:
        os.chdir(options.outdir)
    except OSError as err:
        die('Cannot change to output directory ' + options.outdir + ' (' + str(err) + ')\n')

    libname_mappings = get_libname_mappings('config.txt')

    check_exec('upcrun', '-version', 'This is upcrun')
    # get max kmer value for exec
    max_kmer_len = ((int(options.kmer_lens.split(',')[-1]) + 31) // 32) * 32
    #message("maximum kmer length is %d" % (max_kmer_len))
    upcrun_main = 'main-' + str(max_kmer_len)
    main_exec = which(upcrun_main)
    if not main_exec:
        die('Could not find upc executable,', upcrun_main)

    if options.restart_stage is not None:
        message('\nRestarting assembly at', str(datetime.datetime.now()))
    else:
        message('\nStarting assembly at', str(datetime.datetime.now()))

    prev_last_stage = None
    last_stage = None
    if options.restart_stage:
        if os.path.exists("results/final_assembly.fa"):
            message("Renaming existing final assembly")
            os.rename("results/final_assembly.fa", "results/final_assembly.fa-%s" % (datetime.datetime.now().strftime('%Y%m%d_%H%M%S')))
            short_results = "results/final_assembly-%d.fa" % (options.assm_scaff_len_cutoff)
            if os.path.exists(short_results):
                os.rename(short_results, "%s-%s" % \
                          (short_results, datetime.datetime.now().strftime('%Y%m%d_%H%M%S')))
        
    prev_run_log = os.path.exists('run.log')
    with open('run.log', 'a' if options.restart_stage else 'w') as run_log_file:
        mem_tracker.open('run.profile.csv')
        mem_tracker.start()
        # record all the user messages so far 
        write_messages(run_log_file)
        messages=""

        if options.no_slurm_bcast or (os.environ.get('NO_SLURM_BCAST') is not None and os.environ.get('NO_SLURM_BCAST') != '0'):
            os.environ['UPC_SRUN_BCAST'] = '0' # tell upcrun not to bcast either

        if is_slurm_job() and os.environ.get('NO_SLURM_BCAST') is None and not options.no_slurm_bcast:
            # broadcast the executable for large runs
            # OLD WAY THAT HAS SOME ISSUES ON CORI 
            # -- os.environ['SLURM_BCAST'] = '/tmp/%s-%s' % (main_exec[main_exec.rfind('/')+1:], get_job_id())
            print_log(run_log_file, "Broadcasting executable to all %d nodes\n" % (nodes))
            dest = '/tmp/%s-%s' % (main_exec[main_exec.rfind('/')+1:], get_job_id())
            ### FIXME when SLURM can sbcast again - INC0144729
            ### cmd = ['srun', '-v', '--ntasks=%d'%(nodes), '--ntasks-per-node=1', 'cp', '-p', main_exec, dest]
            start_t = dtimer()
            cmd = ['sbcast', '--verbose', '--compress', '--timeout', '60', '--preserve', '--force', main_exec, dest]
            try:
                print_log(run_log_file, str(datetime.datetime.now()) + '\n' + 'Executing:\n' + ' '.join(cmd) + '\n')
                # first copy to local machine (in case on a mom node)
                status = 1
                proc2 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                for line in iter(proc2.stdout.readline, b''):
                    line = line.decode()
                    run_log_file.write(line)
                    if "error" in line and status == 1:
                        status = 0
                        print_log(run_log_file, "sending SIGTERM to %s as errors were detected\n" % (cmd[0]))
                        proc2.terminate() 
                if proc2.wait() != 0:
                    raise 
                if not os.path.exists(dest):
                    os.symlink(main_exec, dest) # to support upcrun on a mom node, upcrun needs to verify the file exists
                main_exec = dest
                print_log(run_log_file, str(datetime.datetime.now()) + '\n' + 'sbcast returned successfully in %0.2f seconds\n' % (dtimer() - start_t))
                os.environ['UPC_SRUN_BCAST'] = '0' # tell upcrun not to bcast as it is already done
            except:
                print_log(run_log_file, "WARNING: Could not sbcast the executable to all nodes!", sys.exc_info()[0], "\n")

        errorRunRestarts = 0
        while True:

            if prev_run_log and options.restart_stage:
                run_log_file.write('\n******************************************\n')
                run_log_file.write('************** RESTARTING ****************\n')
                run_log_file.write('******************************************\n\n')

                if options.restart_stage:
                    last_stage = options.restart_stage
                    kmer_str = options.restart_stage.split('-')[-1]
                    if kmer_str.isdigit() and int(kmer_str) > 9:
                        print_log(run_log_file, '\n%10s Contig generation, k = %s' % ('', kmer_str))
                    else:
                        print_log(run_log_file, '\n%10s Scaffolding' % '')
            
            run_log_file.write(str(datetime.datetime.now()) + '\n')
            run_log_file.write("Executed as: " + ' '.join(sys.argv) + '\n' ) # a little repetetive but needed for provenance in the log!
    
            (success, last_stage) = run_upc(options, main_exec, nodes, last_stage, run_log_file, libname_mappings)
            if success:
                break
            print_log(run_log_file, 'Assembly failed')
            if last_stage is None:
                if errorRunRestarts > 0:
                    print_log(run_log_file, "unsuccessful execution attempt and no stages were observed. Aborting any " +\
                              "potential restarts\n")
                    break
                else:
                    print_log(run_log_file, "unsuccessful execution attempt and no stages were observed. Attempting 1 more time\n")
                    errorRunRestarts += 1
            else:
                if options.auto_restart:
                    if prev_last_stage is None or prev_last_stage != last_stage:
                        print_log(run_log_file, 'Attempting to restart from stage: ', last_stage, ' previous:', prev_last_stage)
                        prev_last_stage = last_stage
                        # use the current last stage not what was given as a command line option
                        options.restart_stage = last_stage
                        if not options.test_checkpointing:
                            print_log(run_log_file, 'Waiting 60 seconds for shared memory to clear up')
                            time.sleep(60)
                    else:
                        if last_stage:
                            print_log(run_log_file, '\nERROR: repeat failure on stage ', last_stage, '. Aborting...')
                        else:
                            print_log(run_log_file, '\nERROR: repeat failure on start. Aborting...')
                        break
                    if options.checkpoint_restart is not None:
                        options.auto_restart = options.checkpoint_restart
                else:
                    print_log(run_log_file, '\nAuto-restart is disabled. Aborting at stage: ',  last_stage)
                    break
                
        if success and os.path.exists('results/final_assembly.fa'):
            run_log_file.flush()
            # read stats from file
            with open('run.log') as f:
                lines = f.readlines()
                for i in range(len(lines) - 1, 0, -1):
                    if "Assembly statistics" in lines[i]:
                        print("")
                        for j in range(i, i + 11):
                            print(lines[j], end='')
                        break

            print_log(run_log_file, '\nCompleted run at', str(datetime.datetime.now()), 'in %.2f s,' % (dtimer() - start_t),
                      ' peak memory used on node 0: %.2f GB\n' % (float(mem_tracker.peak_mem) / 1024))
            min_len = "%d" % options.assm_scaff_len_cutoff
            print_log(run_log_file, 'Assembly of contigs >= ', min_len, 'bp can be found in ' + options.outdir + '/results/final_assembly.fa')
            print_log(run_log_file, 'Assembly of contigs < ', min_len, 'bp can be found in ' + options.outdir + '/results/final_assembly-%d.fa' % (options.assm_scaff_len_cutoff))
        else:
            if not os.path.exists('results/final_assembly.fa'):
                print_log(run_log_file, "Missing final assembly: results/final_assembly.fa")
            print_log(run_log_file, '\nFAILED run at', str(datetime.datetime.now()), 'in %.2f s\n' % (dtimer() - start_t))
            print_log(run_log_file, "Assembly failed. Please look in the run.log for details: " + options.outdir + '/run.log')
            return 1
    return 0

if __name__ == "__main__":
    # remove the .py from this script as the hipmer wrapper needs to be excecuted for proper environment variable detection
    sys.argv[0] = os.path.splitext(sys.argv[0])[0] 
    #message("Starting hipmer, executed as: " + ' '.join(sys.argv) + '\n' )
    status = 1
    try:
        status = main()
    except SystemExit:
        raise
    except:
        e = sys.exc_info()[0]
        print("\nCaught an exception %s in hipmer.py!\n\n" % e, file=sys.stderr); 
        traceback.print_exc(file=sys.stderr)
    finally:
        if has_psutil:
            print("\nThis wrapper script: %s\n" % (track_pid(os.getpid())))
        exit_all(status)

