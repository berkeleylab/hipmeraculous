#!/bin/bash

USAGE="PROPER USAGE:

$0 install-path meraculous.config

"

PHYS_MEM_MB=${PHYS_MEM_MB:=$(awk '/MemTotal:/ { t=$2 ; print int(0.80 * t / 1024)}' /proc/meminfo || echo 3000)}
export GASNET_PHYSMEM_MAX=${GASNET_PHYSMEM_MAX:=${PHYS_MEM_MB}MB}
export GASNET_PHYSMEM_NOPROBE=${GASNET_PHYSMEM_NOPROBE:=1}
UPC_PTHREADS=${UPC_PTHREADS:=}
UPC_PTHREADS_OPT=
if [ -n "$UPC_PTHREADS" ] && [ $UPC_PTHREADS -ne 0 ]
then
  UPC_PTHREADS_OPT="-pthreads=${UPC_PTHREADS}"
fi

if [ -z "$1" -o -z "$2" ]
then
  echo "$USAGE" 1>&2
  echo "Please specify the hipmer install path and config file" 1>&2
  exit 1
fi

INSTALL_PATH=$(readlink -f $1 2>/dev/null)
if [ ! -d "${INSTALL_PATH}" ]
then
  echo "$USAGE" 1>&2
  echo "Invalid hipmer install path: ${INSTALL_PATH}" 1>&2
  exit 1
fi

CONFIG=$(readlink -f $2)
if [ ! -f "${CONFIG}" ]
then
  echo "$USAGE
Please specify a config file! '${CONFIG}' is invalid!
" 1>&2
  exit 1
fi
export PATH=${INSTALL_PATH}/bin:${PATH}

echo "# Starting '$0 $@' at $(date) in $(pwd) on $(hostname) (${SLURM_JOB_ID}${PBS_JOBID}${JOB_ID} pid:$$)"

DRYRUN=${DRYRUN:=0}

LAST_LOG=
bail()
{
  if [ "$DRYRUN" != "0" ]
  then
     echo "# Dryrun: Skipping check $@"
     return
  fi
  echo "

ERROR: at $(date) in $(pwd) on $(hostname):
$@
    ${LAST_LOG}

" 1>&2
  exit 1
}

get_config()
{
  field=$1
  vals=($(grep "^${field}" ${CONFIG} || true))
  echo ${vals[1]}
}

get_rankpath()
{
  rank="0000000${1}"
  echo "${PER_THREAD_DIR}/${rank: -7}"
}
  

libfiles=
libname=
libinsavg=
libinsstddev=
libreadlen=
libinnie=
librevcomp=
libcontigging=
libonosetid=
libgapclosing=
lib5pwiggle=
lib3pwiggle=
libfilesperpair=
libsplinting=
get_libseq_vars()
{
  fileline=$1
  read -a vals < ${fileline}
  libfiles=($(echo $(echo "${vals[1]}" | tr ',' ' ')))
  libname="${vals[2]}"
  libinsavg=${vals[3]}
  libinsstddev=${vals[4]}
  libreadlen=${vals[5]}
  libinnie=${vals[6]}
  librevcomp=${vals[7]}
  libcontigging=${vals[8]}
  libonosetid=${vals[9]}
  libgapclosing=${vals[10]}
  lib5pwiggle=${vals[11]}
  lib3pwiggle=${vals[12]}
  libfilesperpair=${vals[13]}
  libsplinting=${vals[14]}
  if [ "${vals[1]%,*}" != "${vals[1]}" ]
  then
    [ ${libfilesperpair} -eq 2 ] || bail "Invalid config file: ${libname} has a comma in the filename field but ${libfilesperpair} in FilesPerPair"
    # comma separates paired files, shuffle ordering of files from: a1 b1 c1 a2 b2 c2 to: a1 a2 b1 b2 c1 c2
    declare -a newlibfiles
    numFiles=${#libfiles[*]}
    halfFiles=$((numFiles/2))
    for fnum in $(seq 1 ${halfFiles})
    do
       newlibfiles=(${newlibfiles[@]} ${libfiles[$((fnum-1))]} ${libfiles[$((fnum+halfFiles-1))]})
    done
    libfiles=(${newlibfiles[@]})
  fi
  # if [ "${lib5pwiggle}" == 0 ]
  # then
  #     lib5pwiggle=5
  #     #echo "# Setting 5' wiggle room in $libname to the default of 5"
  # fi
  # if [ "${lib3pwiggle}" == 0 ]
  # then
  #     lib3pwiggle=5
  #     #echo "# Setting 3' wiggle room in $libname to the default of 5"
  # fi
}

# this is the base dir in which all the temporary pipeline files are written
if [ "$USE_SHM" ]
then 
    BASE_DIR="/dev/shm"
    echo "# Using SHM for caching files"
else
    BASE_DIR="."
    echo "# Using base directory '$BASE_DIR'"
fi

MIN_DEPTH_CUTOFF=${MIN_DEPTH_CUTOFF:=$(get_config min_depth_cutoff)}
SCAFFOLDING_KMER_LENGTH=${SCAFFOLDING_KMER_LENGTH:=$(get_config scaffolding_mer_size)}
NUM_ALIGNMENTS=${NUM_ALIGNMENTS:=192000}

EXCLUDE_REPEATS=$(get_config gap_close_rpt_depth_ratio)
if [ "$EXCLUDE_REPEATS" == "" ]; then
  echo "# No value set for EXCLUDE_REPEATS, using 2.0"    
  EXCLUDE_REPEATS="2.0"
else
  echo "# using ${EXCLUDE_REPEATS} for EXCLUDE_REPEATS (a.k.a. gap_close_rpt_depth_ratio or merauderRepeatCopyCount)"
fi

is_diploid=$(get_config is_diploid)
if [ "$is_diploid" == "" ]; then
  is_diploid=0
fi

if [ $is_diploid -eq 0 ]; then
  echo "# Diploid NOT set"
  POLYMODE=""
else
  echo "# Diploid SET"
  POLYMODE="-P"
fi

#ILLUMINA_VERSION=${ILLUMINA_VERSION:=18}
HIPMER_SW_CACHE=${HIPMER_SW_CACHE:=6144}
RUNDIR=${RUNDIR:=$(pwd)/$(hostname)-$$-$(date '+%Y%m%d-%H%M%S')}


trap ctrl_c INT

function ctrl_c() {
    trap "" INT
    [ -n "KEEP_RUNDIR" ] || echo "deleting $RUNDIR... hit ctrl-c again to abort purge"
    sleep 5
    echo "aborting "
    pkill -2 -P $$
    [ -n "KEEP_RUNDIR" ] || rm -rf $RUNDIR
}


guess_qual_offset()
{
  fastq=$1
  if [ ! -f "$fastq" ]
  then
    echo "Error: $fastq does not exist"
    exit 1
  fi

  check0=$(head -n50000 $fastq | grep -A1 "^\+" | grep -v "^\+" | grep "[\"#$%&'()*,./0123456789:;<=>?]" |wc -l)
  if [ $check0 -eq 0 ]
  then
    echo 64
  else
    echo 33
  fi
}

if [ -z "${CORES_PER_NODE}" ] || [ $CORES_PER_NODE -eq 0 ]
then
  # auto-detect the cores per node
  if [ -x $(which lscpu) ]
  then
    export CORES_PER_NODE=$(($(lscpu -p | tail -1 | awk -F, '{print $2}')+1))
    echo "# Autodetected CORES_PER_NODE=${CORES_PER_NODE} from lscpu"
  else 
    SOCKETS=$((grep '^physical id' /proc/cpuinfo | sort | uniq | wc -l))
    CORES_PER_SOCKET=$((grep '^core id' /proc/cpuinfo | sort | uniq | wc -l))
    export CORES_PER_NODE=$((SOCKETS*CORES_PER_SOCKET))
    echo "# Autodetected CORES_PER_NODE=${CORES_PER_NODE} from /proc/cpuinfo"
  fi
fi

HYPERTHREADS=${HYPERTHREADS:=1}
CORES_PER_NODE=$((CORES_PER_NODE*HYPERTHREADS))
UFX_HLL=${UFX_HLL:=0}
UFX_HLL_OPT=
if [ "${UFX_HLL}" != "0" ]
then
  UFX_HLL_OPT="-H"
fi
POLITE_SYNC=
if [ ${HYPERTHREADS} -gt 1 ]
then
  POLITE_SYNC="-polite-sync"
fi

THREADS=${THREADS:=${CORES_PER_NODE}}
if [ ${THREADS} -lt ${CORES_PER_NODE} ]
then
  # support 1 thread or less than the cores in one node on a single node
  echo "Overriding CORES_PER_NODE from ${CORES_PER_NODE} to ${THREADS}"
  export CORES_PER_NODE=${THREADS}
elif [ $((THREADS % CORES_PER_NODE)) -ne 0 ]
then
  echo "Overriding THREADS from ${THREADS} to be a mulitple of ${CORES_PER_NODE}"
  THREADS=$((THREADS - (THREADS % CORES_PER_NODE)))
fi
echo "# Executing on a total of ${THREADS} threads"

NODES=$((${THREADS}/${CORES_PER_NODE}))
ONO_ACTIVE_THREADS=${ONO_ACTIVE_THREADS:=${CORES_PER_NODE}}

echo "# Using $NODES nodes, with $CORES_PER_NODE cores per node and ${HYPERTHREADS} hyperthreads per core"
echo "# Using $ONO_ACTIVE_THREADS active threads for oNo"


if [ -z "${CAN_SPLIT_JOB}" ]
then
  if [ -n "${SLURM_JOB_ID}" ]
  then
    CAN_SPLIT_JOB=1
  else
    CAN_SPLIT_JOB=0
  fi
fi
if [ -n "${CAN_SPLIT_JOB}" ]; then
    echo "# Can split job: ${CAN_SPLIT_JOB}"
fi

MAX_UPC_SHARED_HEAP_SIZE=$((PHYS_MEM_MB/CORES_PER_NODE))
if [ -z "${UPC_SHARED_HEAP_SIZE}" ]
then
  UPC_SHARED_HEAP_SIZE=${MAX_UPC_SHARED_HEAP_SIZE}
fi
MAX_ALLOWED_SHARED_HEAP_SIZE=${MAX_ALLOWED_SHARED_HEAP_SIZE:=15500}
# maximum allowed shared heap in bupc...
if [ ${UPC_SHARED_HEAP_SIZE} -gt ${MAX_ALLOWED_SHARED_HEAP_SIZE} ]
then
  UPC_SHARED_HEAP_SIZE=${MAX_ALLOWED_SHARED_HEAP_SIZE}
fi

echo "# Using -shared-heap=${UPC_SHARED_HEAP_SIZE}M"

MPIRUN=${MPIRUN:="srun -n"}
MPIRUN="${MPIRUN} $((THREADS/HYPERTHREADS))"

UPCRUN=${UPCRUN:="upcrun -n"}
# parse into _UPCRUN="upcrun" and the arguments
_UPCRUN=${UPCRUN%% *}
UPCRUN_NO_PTHREADS="${_UPCRUN} ${POLITE_SYNC} -shared-heap=${UPC_SHARED_HEAP_SIZE}M ${UPCRUN#${_UPCRUN}}"
UPCRUN="${_UPCRUN} ${UPC_PTHREADS_OPT} ${POLITE_SYNC} -shared-heap=2500M ${UPCRUN#${_UPCRUN}}"

# Parameters for the metagenome pipeline
mer_size_min=$(get_config mer_size_min)
mer_size_max=$(get_config mer_size_max)
mer_size_step=$(get_config mer_size_step)
error_rate=$(get_config error_rate)
dmin_base=$(get_config dmin_base)
alpha=$(get_config alpha)
beta=$(get_config beta)
tau=$(get_config tau)
MIN_DEPTH_CUTOFF_WALKS=$(get_config min_depth_cutoff_walks)
run_error_correction=$(get_config error_correction_step)
MIN_VIABLE_WALKS=$(get_config min_viable_walks)

ENVS="
environmental variables with some effects:

Physical memory properties:
  PHYS_MEM_MB=${PHYS_MEM_MB}

The number of UPC threads in the job (MPI will not use hyperthreads):
  THREADS=${THREADS} HYPERTHREADS=${HYPERTHREADS}

The rundirectory to place all the files (will be created if necessary):
  RUNDIR=${RUNDIR}
  DATADIR=${DATADIR}

Gasnet properties (by default 80% of physmem):
  GASNET_PHYSMEM_MAX=${GASNET_PHYSMEM_MAX}
  GASNET_PHYSMEM_NOPROBE=${GASNET_PHYSMEM_NOPROBE}

UPC properties:
  UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE} (Do not set to use 80% of the node memory)
  UPC_PTHREADS=${UPC_PTHREADS}
  UPC_PTHREADS_OPT=${UPC_PTHREADS_OPT}

metaHipMer options (will override config file defaults):
  MIN_VIABLE_WALKS=${MIN_VIABLE_WALKS}
  MIN_DEPTH_CUTOFF=${MIN_DEPTH_CUTOFF}
  MIN_MER_SIZE=${mer_size_min}
  MAX_MER_SIZE=${mer_size_max}
  MER_SIZE_STEP=${mer_size_step}
  ERROR_PARAM=${error_rate}
  DMIN_BASE=${dmin_base}
  ALPHA=${alpha}
  BETA=${beta}
  TAU=${tau}
  SCAFFOLDING_KMER_LENGTH=${SCAFFOLDING_KMER_LENGTH}
  NUM_ALIGNMENTS=${NUM_ALIGNMENTS}
  ILLUMINA_VERSION=${ILLUMINA_VERSION}
  HIPMER_SW_CACHE=${HIPMER_SW_CACHE}
  NO_CONCAT=${NO_CONCAT}
  CAN_SPLIT_JOB=${CAN_SPLIT_JOB}
  ONO_ACTIVE_THREADS=${ONO_ACTIVE_THREADS}


MPI/UPC environment:
  MPIRUN=${MPIRUN}
  UPCRUN=${UPCRUN}
"
USAGE="${USAGE}
environmental variables with some effects:
${ENVS}

"

set -e

MEMTIME=memtime
which ${MEMTIME} > /dev/null || MEMTIME=
which ${UPCRUN%% *} > /dev/null || bail "${USAGE} upcrun (${UPCRUN}) must be installed and in your PATH!"
which ${MPIRUN%% *} > /dev/null || bail "${USAGE} mpirun (${MPIRUN}) must be installed and in your PATH!"
which meraculous-${mer_size_min} > /dev/null || bail "${USAGE}Could not find meraculous-${mer_size_min}!  Have you compiled with the proper option: '-DHIPMER_KMER_LENGTHS=${mer_size_min}'?"
which ufx-${mer_size_min} > /dev/null || bail "could not find ufx-${mer_size_min}! Have you compiled with the proper option '-D$mer_size_min'?"

echo "Using:
   upcrun: ${UPCRUN}
   mpirun: ${MPIRUN}
${ENVS}
"


cd ${RUNDIR} || bail "Could not find ${RUNDIR} from $(pwd) on $(uname -n)"
RUNDIR=$(pwd -P)
PER_THREAD_DIR=per_rank
rankpath=$(get_rankpath 0)

mkdir -p ${PER_THREAD_DIR}
# if on luster, set stripe to be 1 for per_rank files
[ ! -x "$(which lfs)" ] || lfs setstripe -c 1 ${PER_THREAD_DIR} 2>/dev/null || true

mkdir -p ${rankpath}

# set all other new files defaulted to be on all the OSTs
[ ! -x "$(which lfs)" ] || lfs setstripe -c -1  ${RUNDIR}/. 2>/dev/null || true

CONFIG_PATH=${CONFIG%/*}
# split each lib_seq entry in the config file into its own separate file
if [ ! -f config.LIBSEQaa ]
then
  grep '^lib_seq' ${CONFIG} | split -l 1 - ${PER_THREAD_DIR}/config.LIBSEQ
  [ -f ${PER_THREAD_DIR}/config.LIBSEQaa ] || bail "Could not generate config.LIBSEQaa!"
  for i in ${PER_THREAD_DIR}/config.LIBSEQ* ; do ln $i ; done
fi

# now each config.LIBSEQ* file will have 1 entry of the form:
# lib_seq [ wildcard ][ prefix ][ insAvg ][ insSdev ][ avgReadLen ][ hasInnieArtifact ][ isRevComped ][ useForContigging ][ onoSetId ][ useForGapClosing ][ 5pWiggleRoom ][3pWiggleRoom] [FilesPerPair] [ useForSplinting ]

GAPLIBS=
ALLLIBS=
CONTIGLIBS=
MAX_INSERT_SIZE=0
MAX_INSERT_SIGMA=0
MAX_ONO_SET_ID=0
IN=ALL_INPUTS.fofn
rm -f ${IN} ${PER_THREAD_DIR}/${IN}
# Check for existance of libname.fofn for each library
for x in config.LIBSEQ*
do
  get_libseq_vars $x
  if [ -n "$ALLLIBS" ]
  then
    ALLLIBS="${ALLLIBS},${libname}"
  else
    ALLLIBS="${libname}"
  fi
  if [ $libgapclosing -eq 1 ]
  then
    if [ -n "$GAPLIBS" ]
    then
      GAPLIBS="${GAPLIBS},${libname}"
    else
      GAPLIBS="${libname}"
    fi
  fi
  if [ ${MAX_ONO_SET_ID} -lt ${libonosetid} ]
  then
    MAX_ONO_SET_ID=${libonosetid}
  fi
  if [ -f "${libname}.fofn" ]
  then
    echo "# For library ${libfiles[@]} ${libname} using ${libname}.fofn:"
    echo "$(cat ${libname}.fofn)" 
  else
    for file in ${libfiles[@]}
    do
      if [ -f ${file} ]
      then
        echo $(readlink -f ${file})
      elif [ -d "${DATADIR}" ] && [ -f ${DATADIR}/${file} ]
      then
        echo $(readlink -f ${DATADIR}/${file})
      else
        for file1 in ${CONFIG_PATH}/${file}
        do
          if [ -f ${file1} ]
          then
            echo $(readlink -f ${file1})
          else
            bail "Could not find $file or $file1 specified as ${libfile} for Library ${libname}"
         fi
       done
      fi
    done > ${PER_THREAD_DIR}/${libname}.fofn
    ln -f ${PER_THREAD_DIR}/${libname}.fofn
  fi
  if [ ${libcontigging} -eq 1 ]
  then
    cat ${libname}.fofn >> ${PER_THREAD_DIR}/${IN}
    if [ -n "$CONTIGLIBS" ]
    then
      CONTIGLIBS="${CONTIGLIBS},${libname}"
    else
      CONTIGLIBS="${libname}"
    fi
  fi
done 
ln -f ${PER_THREAD_DIR}/${IN}

INUFX=${IN}.ufx.bin

TIMINGS=${TIMINGS:=${PER_THREAD_DIR}/timings.log}

# needed in the rerun_stage.sh script 
echo "# Starting '$0 $@' at $(date) in $(pwd) on $(hostname)
# Install path: $INSTALL_PATH
# RUNDIR: ${RUNDIR}" >> $TIMINGS
ln -f ${TIMINGS} 2>/dev/null || true

log_and_run()
{
  if [ -z "${RETRY}" ]
  then
    RETRY=0
  else
    RETRY=$((RETRY-1))
  fi
  log=${PER_THREAD_DIR}/$1
  loglink=$1
  LAST_LOG=$(pwd)/${log}.$(date '+%Y%m%d_%H%M%S')
  shift
  if [ -f ${log} ] && [  -f ${loglink} ]
  then
    echo "# Skipping, already executed: '$@'"
  else
    echo "# Starting '$@' at $(date) log in ${LAST_LOG}"
    echo "# Starting $@ at $(date)" >> ${TIMINGS}
    secs=$SECONDS
    if [ "$DRYRUN" != "0" ]
    then
      echo "# DRY run would execute:
   $@
"
      return
    else
      echo "$@" >> ${TIMINGS}
      echo "Starting '$@' at $(date)" > ${LAST_LOG}
      $@ >> ${LAST_LOG} 2>&1 \
        || ( [ ${RETRY} -gt 0 ] && RETRY=${RETRY} log_and_run ${loglink} $@ ) \
        || bail "Could not execute $@:
    (exit: $? after $((SECONDS-$secs)) s) See the log:
    ${LAST_LOG}"
    fi
    runsecs=$((SECONDS-$secs))
    echo "# Finished (${runsecs} s) '$@' at $(date)"
    echo "# ${log} ${runsecs} walltime-secs $((runsecs*THREADS)) core-seconds at $(date)" >> ${TIMINGS}
    echo "# ${log} ${runsecs} walltime-secs $((runsecs*THREADS)) core-seconds at $(date)" >> ${LAST_LOG}
    ln -f ${LAST_LOG} $log
    ln -f ${log} ${loglink}
    LAST_LOG=

    sleep 1 # insert a small delay between invocations of srun / upcrun to allow shared memory to clean up
  fi
}

if [ -z "$ILLUMINA_VERSION" ]; then
  a_lib_fname=$(head -n1 ${IN})
  [ ! -f "$a_lib_fame" ] || bail "Could not find $a_lib_fname to guess ILLUMINA format"
  # guess the illumina version
  qual_offset=$(guess_qual_offset $a_lib_fname)
  echo "# Guessed phred offset=${qual_offset}. Set ILLUMINA_VERSION to override."
else
  if [ $ILLUMINA_VERSION -eq 13 ] || [ $ILLUMINA_VERSION -eq 15 ]; then
    qual_offset=64
  elif [ $ILLUMINA_VERSION -eq 18 ]; then
    qual_offset=33
  fi
  echo "# Phred offset $qual_offset"
fi

if [ -z "$qual_offset" ]; then
  echo "Could not guess illumina version... assuming phred offset is 33"
  qual_offset=33
fi

if [ "$USE_SHM" ]
then
    SHM_FLAG=-s
    log_and_run loadfq.log $MEMTIME $UPCRUN ${THREADS} loadfq -f ${ALLLIBS} 
fi

KMER_LENGTH=${mer_size_min}

# This should create ALL_INPUTS.fofn.ufx.bin
log_and_run ufx.${KMER_LENGTH}.log $MEMTIME $MPIRUN ufx-${KMER_LENGTH} -Q $qual_offset -f ${IN} -e ${MIN_DEPTH_CUTOFF} ${UFX_HLL_OPT} $SHM_FLAG
EXTUFX=${INUFX}
[ -f ${EXTUFX} ] || bail "No ${EXTUFX} file!"

TIGS=UUtigs
OUT=contigs.${KMER_LENGTH}.fasta

# Now traverse the de Bruijn graph (which is enhanced with the de Bruijn graphs of previous iterations)
log_and_run contigs.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS}  meraculous-${KMER_LENGTH} -N ${CORES_PER_NODE} -i ${INUFX} -m ${KMER_LENGTH} -o ${OUT} -c 100 -l 1 -d ${dmin_base} -e ${error_rate} -B ${BASE_DIR}
    
# Now find the depths of the generated contigs (required for bubbleFinder module)    
merDepthPrefix=merDepth_${OUT}
log_and_run contigMerDepth.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} contigMerDepth-${KMER_LENGTH} -i ${INUFX} -k ${KMER_LENGTH} -c ${OUT} -d ${dmin_base} -e ${error_rate} -s 100 -B ${BASE_DIR}

# Now analyze the termination status of the generated contigs (required for bubbleFinder module)
log_and_run contigEndAnalyzer.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} contigEndAnalyzer-${KMER_LENGTH} -i ${INUFX} -k ${KMER_LENGTH} -c ${OUT} -d ${dmin_base} -e ${error_rate} -s 100 -B ${BASE_DIR}

# Now run the bubble Finder module (that also trims the contig graph from the "hair structure")
nContigs=$(cat ${PER_THREAD_DIR}/n${TIGS}_${OUT}.txt)
log_and_run bubbleFinder.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} bubbleFinder-${KMER_LENGTH} -N ${nContigs} -m my${TIGS}_${OUT} -f ${OUT} -d ${OUT} -c ${OUT} -o bubble_${OUT} -B ${BASE_DIR}

OUT=bubble_${OUT}
TIGS=Contigs
nContigs=$(cat ${PER_THREAD_DIR}/n${TIGS}_${OUT}.txt)
merDepthPrefix=merDepth_${OUT}

# Now run the iterative graph pruning module
log_and_run progressiveRelativeDepth.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} progressiveRelativeDepth-${KMER_LENGTH} -a ${alpha} -t ${tau} -b ${beta}  -i ${INUFX} -k ${KMER_LENGTH} -c ${OUT} -s 100 -d  ${dmin_base} -e ${error_rate} -B ${BASE_DIR} -N ${nContigs} -C ${merDepthPrefix}

nFilteredContigs=$(cat ${PER_THREAD_DIR}/nFilteredContigs.${KMER_LENGTH}.txt)

# Now run merAligner on all the libraries used for contigging
which merAligner-${KMER_LENGTH} > /dev/null || bail "Could not find merAligner-${KMER_LENGTH} have you compiled with -DKMER_LENGTH=${KMER_LENGTH}?"
nTotalAlignments=0
list_revcomp_contigging=
list_5pwiggle_contigging=
list_3pwiggle_contigging=
list_insertSize_contigging=
list_insertSigma_contigging=
max_readlen=0
maxInsertSize=0

for x in config.LIBSEQ*
do
    get_libseq_vars $x
    if [ ${libcontigging} -eq 1 ]
    then
        cache_contig_length=400
	# make sure the -e parameter is at least 3 * the overlap
	min_cache_contig_length=$((3 * 2 * (${libreadlen} + 20 - ${KMER_LENGTH})))
	if [ ${cache_contig_length} -lt ${min_cache_contig_length} ] 
	then
	    cache_contig_length=${min_cache_contig_length}
	fi
	log_and_run merAligner-${libname}.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} merAligner-${KMER_LENGTH} -N ${CORES_PER_NODE} -j 1.2 -r ${libname}.fofn -l ${libname} -P ${libfilesperpair} -c ${HIPMER_SW_CACHE} -k 16228 -x 200000 -m ${KMER_LENGTH} -e ${cache_contig_length} -B ${BASE_DIR} -L ${libreadlen}

   nContigs=$(cat ${PER_THREAD_DIR}/nFilteredContigs.${KMER_LENGTH}.txt)

   minThreads=${CORES_PER_NODE}
   [ "$NODES" -gt 1 ] && minThreads=$((2*$CORES_PER_NODE))
   threadsToUsePerNode=$(($minThreads/$NODES))
   [ "$threadsToUsePerNode" -lt 1 ] && threadsToUsePerNode=1
   totThreadsToUse=$(($threadsToUsePerNode*$NODES))
   numAlignsPerThread=$(($NUM_ALIGNMENTS/$totThreadsToUse))
   opts="-l ${libname} -m ${SCAFFOLDING_KMER_LENGTH} -i ${libinsavg} -s ${libinsstddev} -c ${numAlignsPerThread} "
   [ ${librevcomp} -eq 0 ] || opts="$opts -R" # reverse complement
   [ ${libinnie} -eq 0 ] || opts="$opts -A" # innie removal
   [ ${lib3pwiggle} -eq 0 ] || opts="$opts -T ${lib3pwiggle}" # 3 prime wiggle
   [ ${lib5pwiggle} -eq 0 ] || opts="$opts -F ${lib5pwiggle}" # 5 prime wiggle


   log_and_run histogrammer-${libname}.log $MEMTIME $UPCRUN ${THREADS} merAlignerAnalyzer ${opts} -B ${BASE_DIR} -N $CORES_PER_NODE -p ${threadsToUsePerNode}


	# Now run error correction module
	log_and_run error_correction.${KMER_LENGTH}.log $MEMTIME $UPCRUN ${THREADS} errorCorrection -l ${libname} -B ${BASE_DIR} -c ${OUT}.pruned -R ${libname}.fofn -n ${nFilteredContigs} -P

    fi
done

echo "# Total ${SECONDS} walltime-sec $((SECONDS*THREADS)) core-secs at $(date)" >> $TIMINGS

if [ "$RUNDIR" != "." ]; then
    cd ..
    rm -f latest_output
    echo "adding link latest_output->${RUNDIR}"
    ln -s ${RUNDIR} latest_output || true
fi

if [ -n "${POST_RUN}" ]
then
  echo "Running POST_RUN: ${POST_RUN}"
  ( source ${POST_RUN} )
  echo "# After POST_RUN ${SECONDS} walltime-sec $((SECONDS*THREADS)) core-secs at $(date)" >> $RUNDIR/$TIMINGS
fi

echo "# Finished at $(date) in $SECONDS s: $0 $@ (${SLURM_JOB_ID}${PBS_JOBID}${JOB_ID} pid:$$)"
