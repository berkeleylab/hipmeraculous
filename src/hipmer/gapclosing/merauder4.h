#ifndef MERAUDER4_H_
#define MERAUDER4_H_

/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 *
 * Based on merauder.pl by Jarrod Chapman <jchapman@lbl.gov> Tue Jun 2
 * 07:48:59 PDT 2009 Copyright 2009 Jarrod Chapman. All rights reserved.
 */

/*
 * Three files are read in:
 * 1. The gap data, where all of the info is stored in an array of gap structs.
 * 2. The contigs from a FASTA file, where only the contigs for relevant
 * sequences are stored.
 * 3. The scaffold report file, which is used to determine the start, end and
 * depth of the scaffold.
 *
 * Each thread outputs to a separate file.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#ifndef __APPLE__
#include <malloc.h>
#endif
#include <upc.h>
#include <upc_collective.h>
#include <ctype.h>
#include <upc_tick.h>
#include <libgen.h>
#include <unistd.h>

#include "gapclosing_timerdefs.h"

#include "upc_common.h"
#include "common.h"
#include "upc_output.h"


#include "timers.h"
#include "tracing.h"
#include "utils.h"
#include "Buffer.h"
#include "htable.h"
#include "gaps.h"
#include "gapclosing_config.h"

// for debugging
//#define SOLO_GAP 62

#define PRINT_TIMERS
#define DBG_SCAFF_FASTA

#define PARTIAL_GAP_CLOSURE


int merauder_main(int argc, char **argv);


#endif // MERAUDER4_H_
