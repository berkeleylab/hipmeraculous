/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 */

#ifndef __GAPS_H
#define __GAPS_H

#include <stdio.h>
#include "htable.h"
#include "gapclosing_config.h"

#include "Buffer.h"

typedef struct {
    int64_t cid;
    int64_t gapi_3;
    int64_t gapi_5;
    int     s_start;
    int     s_end;
    char    strand;
} scaff_contig_t;

typedef struct {
    shared [] char *seq;
    int seq_len;
    shared [] scaff_contig_t * scaff_contig;
} contig_t;

typedef struct {
    int64_t id;
    double  length;
    double  depth;
    double  weight;
    shared [] scaff_contig_t * scaff_contigs;
    int64_t ncontigs;
    int64_t max_contigs;
} scaffold_t;

typedef struct {
    int64_t id;
    int64_t scaff_id;
    int64_t contig1_id;
    int64_t contig2_id;
    char *  primer1;
    char *  primer2;
    double  uncertainty;
    int     start;
    int     end;
    int64_t max_reads;
    int64_t nreads;
    int     size;
    int     len_closure;
    int     len_right_walk;
    int     len_left_walk;
    shared [] int *reads_lens;
    shared [] char *reads_nts;
    shared [] char *reads_quals;
    shared [] char *closure;
    shared [] char *left_walk;
    shared [] char *right_walk;
    int64_t sortIdx;
    int8_t contig_ext1;
    int8_t contig_ext2;
    int8_t cp_contigs;
} gap_t;

typedef struct {
    shared [] gap_t * gaps;
    int64_t ngaps;
    int64_t gapi_offset;
    int64_t found_ngaps;
    int64_t nprocessed;
    SharedHeapListPtr heapList;
    Buffer gapAllocs;
} gap_set_t;

typedef struct {
    int     triedNodes;
    int     triedMyNode;
} steal_state_t;
    
typedef struct {
    shared [] gap_t * gaps;
    int64_t ngaps;
    steal_state_t stealState;
} batch_of_gaps_t;

typedef struct {
    int64_t gap_id;
    char    name[MAX_READ_NAME_LEN];
    char    orient;
    char    lib_is_rc;
} read_orient_t;

typedef struct {
    read_orient_t *reads;
    long           num;
    long           size;
} read_list_t;

#define NULL_ON_FAIL 0
#define ABORT_ON_FAIL 1

void init_globals(config_t *cfg, shared gap_set_t *gaps);
void free_gaps(void);

#define GET_GAP(gi, abort_on_fail) get_gap((gi), __LINE__, __FILENAME__, (abort_on_fail))
shared [] gap_t *get_gap(int64_t gi, int line, const char *fname, int abort_on_fail);
int localize_gap_reads(gap_t *gap);
void free_gap_reads(gap_t *gap);
int get_steal_thread(steal_state_t *stealState);
void increment_steal_thread(steal_state_t *stealState);
void reset_steal_state(steal_state_t *stealState);
int64_t get_next_batch_of_gaps(batch_of_gaps_t *batch);
void free_primers(batch_of_gaps_t *batch);
char *get_gap_info(gap_t *gap, char *buf, int nchar);
char *get_report_line(gap_t *gap, Buffer buf);
void fprint_gap(FILE *f, gap_t *gap);
void gc_process_contigs_file(int poly_mode);
int64_t process_scaffold_report_file(void);
void gc_process_meraligner_file(char *library_name, int reverse_complement, int insert_size, int insert_sigma, int five_prime_wiggle, int three_prime_wiggle, read_list_t *read_list);
void place_reads_in_dhtable(read_list_t *read_list);
int64_t gc_process_fastq_file(char *library_name, uint8_t libnum);
void finish_fastq_files(int64_t num_reads);
void sort_gaps_by_difficulty();
void unsort_gaps();
void print_gaps(void);
shared contig_t *get_contig(int64_t id);
#define get_contig_chk(contig, id)                      \
    do {                                                \
        contig = get_contig(id);                        \
        if (!contig) {                                    \
            DIE("Could not get contig for %lld\n", (lld)id); }   \
    } while (0)
shared scaffold_t *get_scaffold(int64_t id);
char *get_primer(shared contig_t *contig, int contig_ext, int reverse_flag, int len);
shared contig_t *get_contigs(void);
shared scaffold_t *get_scaffolds(void);
int64_t get_max_contigs(void);
int64_t get_max_scaffolds(void);
void load_balance_gaps(void);

#endif
