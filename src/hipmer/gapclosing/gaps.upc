/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <upc.h>
#include <upc_nb.h>

extern double rc_time;


#include "gapclosing_timerdefs.h"

#include "../fqreader/fq_reader.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "Buffer.h"
#include "kseq.h"
#include "utils.h"
#include "timers.h"
#include "tracing.h"
#include "dhtable.h"
#include "upc_circular_buffer.h"
#include "upc_output.h"

#include "gapclosing_config.h"
#include "gaps.h"

//#define DBG_GET_ALIGNMENT
//#define DBG_PAIR_ALNS
//#define DBG_PUT_READ_ORIENT
//#define DBG_READ_DATA
//#define DBG_CONTIGS
//#define DBG_SCAFFOLDS
//#define DBG_SEQUENCES
//#define DBG_DREAD_AFTER
//#define DBG_FASTQ

#define SHOW_MEMORY
//#define CHECK_READ_DATA_HASH
// It seems that work stealing is broken. DO NOT enable.
//#define STEAL_PLACEMENTS

// primes for the hash table
// 393241, 786433, 1572869, 3145739, 6291469, 12582917, 25165843,
// 50331653, 100663319, 201326611, 402653189, 805306457, 1610612741

#ifndef DEFAULT_READ_LEN
#define DEFAULT_READ_LEN 1024
#endif

#define MAX_READ_GAP_IDS 100
#define MAX_READS_PER_GAP 1500
#define MAX_SEQ_LEN 5000000
#define MAX_BM_LINE_LEN 256
#define MAX_PAIR_ALNS 6
#define MAX_STEAL_BLOCK 100

// to create the hash table for reads, multiplied by the number of reads - gives approx load factor
#define READ_HASH_TABLE_LOAD_FACTOR 0.2

#define REJECT_FORMAT 0
#define REJECT_MINLEN 1
#define REJECT_UNINFORMATIVE 2
#define REJECT_5_TRUNCATED 3
#define REJECT_3_TRUNCATED 4
#define REJECT_SINGLETON 5
#define REJECT_NO_ASSOCIATED_GAP 6
#define REJECT_TRUNC5_FAIL 7
#define REJECT_TRUNC3_FAIL 8
#define REJECT_NO_SCAFFOLD_INFO 9
#define REJECT_POTENTIAL_INNIE 10
#define REJECT_POTENTIAL_SHORTY 11
#define REJECT_HEADER 12
#define NUM_REJECT_REASONS (REJECT_HEADER + 1)
#define GOOD_ALIGNMENT 100

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define GET_SCAFF_ID(scaff) (atol((scaff) + 8))
#define GET_CONTIG_ID(contig) ((contig)[6] == '_' ? atol((contig) + 7) : atol((contig) + 6))

#define GET_READ(s, i) ((s) + ((_cfg->max_read_len + 1) * (i)))

const char *reject_reasons[] = {
    "FORMAT",           "MINLEN",            "UNINFORMATIVE",    "5-TRUNCATED", "3-TRUNCATED",
    "SINGLETON",        "NO_ASSOCIATED_GAP", "TRUNC5_FAIL",      "TRUNC3_FAIL",
    "NO_SCAFFOLD_INFO", "POTENTIAL_INNIE",   "POTENTIAL_SHORTY", "FILE HEADER"
};

//#define FTIME_TO_LINE(t) tprintf_flush("%s, line %d: time %.4f\n", __func__, __LINE__, ELAPSED_TIME(t))

typedef struct {
    //char name[MAX_READ_NAME_LEN];
    int     q_start, q_stop, q_length;
    int     s_start, s_stop, s_length;
    int64_t contig_id;
    int8_t  dirn;
    char    strand;
    //int score;
    //int e_value;
    //int identities;
    //int align_length;
    char    start_status, end_status;
} alignment_t;

typedef struct {
    int64_t gap_id;
    int64_t contig_id;
    int8_t  dirn;
    char    read_name[MAX_READ_NAME_LEN];
    char    strand;
} read_gap_pair_t;

typedef struct {
    long aln;
    char name[MAX_READ_NAME_LEN];
} read_aln_t;

typedef struct {
    // if we store an index into a separate array of alignments instead of the actual
    // alignments, we can probably reduce space usage by 50%
    // however, the code is more complicated, requiring two different structures instead
    // of one. It's only worth doing if we are running out of memory, or getting too
    // high a loss of excessive alns.
    shared [] alignment_t * alns;
    char    name[MAX_READ_NAME_LEN];
    uint8_t nalns;
} pair_alns_t;

typedef struct {
    char        name[MAX_READ_NAME_LEN];
    uint8_t     nalns;
    alignment_t alns[MAX_PAIR_ALNS];
} tmp_pair_alns_t;

typedef struct {
    shared [] pair_alns_t * pairs;
    long              npairs;
    long              next_pair;
    SharedHeapListPtr alnsHeap;
} pair_alns_array_t;


DEFINE_HTABLE_TYPE(read_gap_pair);
DEFINE_HTABLE_TYPE(read_aln);

static config_t *_cfg;

static int64_t _ngaps_block = 0;
static int64_t _max_ngaps = 0;
static shared contig_t *_contigs = NULL;
static int64_t _max_contigs = 0;
static int64_t _max_scaff_contigs = 0;
static shared scaffold_t *_scaffolds = NULL;
static int64_t _max_scaffolds = 0;
static shared gap_set_t *_gaps = NULL;
static int64_t _nreads = 0;

static shared int _peak_depth;

// this is also called from merauder, hence not inline
shared [] gap_t *get_gap(int64_t gi, int line, const char *fname, int abort_on_fail)
{
    int64_t thread = gi / _ngaps_block;

    if (thread < 0 || thread >= THREADS) {
        DIE("At %s:%d: Thread out of range in get_gapi: %lld, gi=%lld, _ngaps_block=%lld\n",
            (fname ? fname : __FILE__), line, (lld)thread, (lld)gi, (lld) _ngaps_block);
    }
    int64_t gapi = gi % _ngaps_block;
    CACHE_LOCAL_RO(gap_set_t, _g, &_gaps[thread]);
    int64_t ngaps = _g->ngaps;
    if (gapi < 0 || gapi >= ngaps) {
        if (abort_on_fail == ABORT_ON_FAIL) {
            DIE("At %s:%d, Gapi out of range in get_gapi: %lld ! in [%d, %lld)\n",
                fname ? fname : __FILE__, line, (lld)gapi, 0, (lld)ngaps);
        } else {
            return NULL;
        }
    }
    DBG2("get_gap(%lld): thread=%lld, gapi=%lld, ngaps=%lld, _ngaps_block=%lld\n",
         (lld)gi, (lld)thread, (lld)gapi, (lld)ngaps, (lld)_ngaps_block);
    shared [] gap_t * g = _g->gaps + gapi;
    assert(upc_threadof(g) == thread);
    return g;
}

void free_gap_reads(gap_t *gap)
{
    double startTime = START_TIMER(T_LOCALIZE_GAP_READS);
    UPC_FREE_CHK(gap->reads_lens);
    UPC_FREE_CHK(gap->reads_nts);
    UPC_FREE_CHK(gap->reads_quals);
    double stopTime = stop_timer(T_LOCALIZE_GAP_READS);
}

int localize_gap_reads(gap_t *gap)
{
    double startTime = START_TIMER(T_LOCALIZE_GAP_READS);
    shared [] int * tmplens = NULL;
    shared [] char * tmpnts = NULL;
    shared [] char * tmpquals = NULL;
    int thread = upc_threadof(gap->reads_nts);
    if (thread != MYTHREAD) {
        UPC_ALLOC_CHK(tmplens, gap->nreads * sizeof(int));
        upc_memget((int*) tmplens, gap->reads_lens, gap->nreads * sizeof(int));

        UPC_ALLOC_CHK(tmpnts, gap->nreads * (_cfg->max_read_len+1));
        upc_memget((char*) tmpnts, gap->reads_nts, gap->nreads * (_cfg->max_read_len+1));

        UPC_ALLOC_CHK(tmpquals, gap->nreads * (_cfg->max_read_len+1));
        upc_memget((char*) tmpquals, gap->reads_quals, gap->nreads * (_cfg->max_read_len+1));

        // old allocations will be freed at the end via the HeapList of the thread that allocated it

        gap->reads_lens = tmplens;
        gap->reads_nts = tmpnts;
        gap->reads_quals = tmpquals;
    }
    double stopTime = stop_timer(T_LOCALIZE_GAP_READS);
    DBG("Localized from thread %d in %0.3f s\n", thread, stopTime - startTime);
    return tmplens != NULL;
}

int64_t try_get_batch(batch_of_gaps_t *batch, shared gap_set_t *gap_set) {
    CACHE_LOCAL_RO(gap_set_t, gs, gap_set);
    int64_t remaining = gs->ngaps - gs->nprocessed;
    DBG2("try_get_batch: from thread %d, remaining=%lld\n", upc_threadof(gap_set), remaining);
    if (remaining <= 0) return 0;

    // try to allocate a fraction of the remaining, with smaller fractions as we near the end because threads will start stealing

    // the first 10% are the hardest and most likely to be on the same node, so do one at a time
    int allocateSize = 1;
    if (gs->nprocessed > gs->ngaps / 10) {
        // more than 10% complete
        int64_t startingSlices = THREADS / MYSV.cores_per_node; // slice proportional to how many nodes there are
        if (startingSlices < 50) startingSlices = 50;    // have at least 50 large slices to start (2% per batch)
        int64_t minLastSlice = MYSV.cores_per_node * 2; // increment the last gaps by 1
        int64_t slice = (gs->ngaps + startingSlices - 1) / startingSlices;
        allocateSize = (remaining <= minLastSlice) ? 1 : ((slice < remaining - minLastSlice) ? slice : remaining - minLastSlice);
    }
    if (allocateSize <= 0) allocateSize = 1;
    int64_t start;
    UPC_ATOMIC_FADD_I64_NEAR(&start, &(gap_set->nprocessed), allocateSize);
    if (start >= gs->ngaps) return 0;
    batch->gaps = (shared [] gap_t *) &gs->gaps[start];
    batch->ngaps = (start + allocateSize) <= gs->ngaps ? allocateSize : gs->ngaps - start;
    assert(batch->ngaps > 0);
    DBG("try_get_batch: from thread %d, remaining=%lld returning=%lld\n", upc_threadof(gap_set), (lld) remaining, (lld) batch->ngaps);
    return batch->ngaps;
}

// steal state.
// First try all threads on the same node
// Then try one thread on a subset of nodes (nodes / cores_per_node) at the same local thread modulus
// each thread will be tested cores_per_node times by node-local threads
// once there is no more work local to the node,
// then each thread will be tested nodes / cores_per_node times (up to a maximum of 2*cores_per_node) by remote threads
// returns -1 when there are no more valid threads to test
int get_steal_thread(steal_state_t *stealState) {
    if (stealState->triedNodes * MYSV.cores_per_node >= THREADS || stealState->triedNodes > 2 * MYSV.cores_per_node *  MYSV.cores_per_node) {
        assert(stealState->triedMyNode == MYSV.cores_per_node);
        LOGF("All threads designated to be stolen from have been tried.\n");
        return -1;
    }
    int nodes = THREADS / MYSV.cores_per_node;
    int myNode = MYTHREAD / MYSV.cores_per_node;
    int tryNode = (myNode + stealState->triedNodes) % nodes;
    int tryThread = (tryNode * MYSV.cores_per_node) + (MYTHREAD + stealState->triedMyNode) % MYSV.cores_per_node;
    return tryThread;
}

void increment_steal_thread(steal_state_t *stealState) {
    if (stealState->triedMyNode < MYSV.cores_per_node) {
        if (stealState->triedMyNode == 0) {
            LOGF("steal state finished all work on my thread\n");
        }
        stealState->triedMyNode++;
        if (stealState->triedMyNode == MYSV.cores_per_node) {
            LOGF("steal state finished stealing from all the threads on my node. %d through %d\n", (MYTHREAD / MYSV.cores_per_node) * MYSV.cores_per_node, (MYTHREAD / MYSV.cores_per_node + 1) * MYSV.cores_per_node - 1);
            assert(stealState->triedNodes == 0);
        } else {
            LOGFN(".");
        }
    }
    if (stealState->triedMyNode == MYSV.cores_per_node) {
        int nodes = THREADS / MYSV.cores_per_node;
        int step = nodes / (2*MYSV.cores_per_node); // hit just a subset of the nodes at scale
        if (step < 1) {
            step = 1; // hit every node
        }
        if (stealState->triedNodes == 0 && step > 1) {
            // this is the first off node attempt
            // offset starting node for each thread on this node
            stealState->triedNodes += (MYTHREAD % step) + 1;
        } else {
            stealState->triedNodes += step;
        }
        LOGFN("*");
    }
}

void reset_steal_state(steal_state_t *stealState) {
    stealState->triedMyNode = 0;
    stealState->triedNodes = 0;
}

int64_t get_next_batch_of_gaps(batch_of_gaps_t *batch) 
{
    double start = START_TIMER(T_STEAL_GAPS);
    DBG("get_next_batch_of_gaps\n");
    shared gap_set_t *gap_set;
    int targetThread;
    while ( (targetThread = get_steal_thread( &batch->stealState )) >= 0) {
        gap_set = (shared gap_set_t *) &_gaps[ targetThread ];
        if (try_get_batch(batch, gap_set)) {
            double end = stop_timer(T_STEAL_GAPS);
            return batch->ngaps;
        }
        increment_steal_thread( &batch->stealState );
    }

    LOGF("No threads I tested have any work to steal\n");    
    double end = stop_timer(T_STEAL_GAPS);
    return 0; 
}

char *get_gapi_info(int64_t gi, char *buf)
{
    shared [] gap_t * _gap = get_gap(gi, __LINE__, NULL, ABORT_ON_FAIL);
    CACHE_LOCAL_RO(gap_t, gap, _gap);
    sprintf(buf, "Scaffold%lld:Contig%lld.%d<-[%d +/- %.0f]->Contig%lld.%d",
            (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1,
            gap->size, gap->uncertainty, (lld)gap->contig2_id,
            gap->contig_ext2);
    return buf;
}

shared contig_t *get_contig(int64_t id)
{
    if (id > _max_contigs || id < 0) {
        //WARN("Requested contig out of range %d > %d\n", id, _max_contigs);
        return NULL;
    }
    assert(_contigs && IS_VALID_UPC_PTR(_contigs));
    if (!_contigs[id].scaff_contig) {
        //WARN("No Contig%lld exists\n", (lld) id);
        return NULL;
    }
    return &_contigs[id];
}

shared scaffold_t *get_scaffold(int64_t id)
{
    if (id > _max_scaffolds || id < 0) {
        DIE("Requested scaffold out of range %lld >= %lld\n", (lld)id, (lld)_max_scaffolds);
    }
    if (_scaffolds[id].id == -1) {
        DIE("No scaffold%lld exists\n", (lld)id);
    }
    return &_scaffolds[id];
}

shared contig_t *get_contigs(void)
{
    assert(_contigs && IS_VALID_UPC_PTR(_contigs));
    return _contigs;
}

shared scaffold_t *get_scaffolds(void)
{
    assert(_scaffolds && IS_VALID_UPC_PTR(_scaffolds));
    return _scaffolds;
}

int64_t get_max_contigs(void)
{
    return _max_contigs;
}

int64_t get_max_scaffolds(void)
{
    return _max_scaffolds;
}

void init_globals(config_t *cfg, shared gap_set_t *gaps)
{
    _cfg = cfg;
    _gaps = gaps;
    _gaps[MYTHREAD].gapAllocs = initBuffer(8192);

    _ngaps_block = 0;
    _max_ngaps = 0;
    _max_contigs = 0;
    _max_scaff_contigs = 0;
    _max_scaffolds = 0;
    _nreads = 0;

#ifdef DBG_GET_ALIGNMENT
    serial_printf("DBG_GET_ALIGNMENT\n");
#endif
#ifdef DBG_PAIR_ALNS
    serial_printf("DBG_PAIR_ALNS\n");
#endif
#ifdef DBG_PUT_READ_ORIENT
    serial_printf("DBG_PUT_READ_ORIENT\n");
#endif
#ifdef DBG_READ_DATA
    serial_printf("DBG_READ_DATA\n");
#endif
#ifdef DBG_CONTIGS
    serial_printf("DBG_CONTIGS\n");
#endif
#ifdef DBG_SEQUENCES
    serial_printf("DBG_SEQUENCES\n");
#endif
#ifdef DBG_SCAFFOLDS
    serial_printf("DBG_SCAFFOLDS\n");
#endif
}

char *get_report_line(gap_t *gap, Buffer buf)
{
    printfBuffer(buf, "REPORT:\tScaffold%lld\tContig%lld.%d\t%s\tContig%lld.%d\t%s\t%d\t%.0f",
                 (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1,
                 gap->primer1, (lld)gap->contig2_id, gap->contig_ext2,
                 gap->primer2, gap->size, gap->uncertainty);
    return buf->buf;
}

char *get_gap_info(gap_t *gap, char *buf, int nchar)
{
    snprintf(buf, nchar, "Scaffold%lld:Contig%lld.%d<-[%d +/- %.0f]->Contig%lld.%d",
             (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1,
             gap->size, gap->uncertainty, (lld)gap->contig2_id,
             gap->contig_ext2);
    return buf;
}


static char *get_aln_info(alignment_t *aln, char *name, char *buf)
{
    sprintf(buf, "'%s'/%d\t%d\t%d\t%d\tContig%lld\t%d\t%d\t%d\t%c\t%c\t%c",
            name, aln->dirn, aln->q_start, aln->q_stop, aln->q_length,
            (lld)aln->contig_id, aln->s_start, aln->s_stop, aln->s_length, aln->strand,
            aln->start_status, aln->end_status);
    return buf;
}


void fprint_gap(FILE *f, gap_t *gap)
{
    START_TIMER(T_PRINT_GAPS2);
    int gotCopy = localize_gap_reads(gap);
    fprintf(f, "Scaffold%lld\tContig%lld.%d\t%s\tContig%lld.%d\t%s\t%d\t%.0f",
            (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1,
            gap->primer1, (lld)gap->contig2_id, gap->contig_ext2,
            gap->primer2, gap->size, gap->uncertainty);
    for (int64_t j = 0; j < gap->nreads; j++) {
        fprintf(f, "\t%s:%s", (char *)GET_READ(gap->reads_nts, j), (char *)GET_READ(gap->reads_quals, j));
    }
    fprintf(f, "\n");
    if (gotCopy) free_gap_reads(gap);
    stop_timer(T_PRINT_GAPS2);
}


#ifndef HIPMER_MAX_DEPTH
#define HIPMER_MAX_DEPTH 20000
#endif
#define MAX_DEPTH HIPMER_MAX_DEPTH
#define DEPTH_HIST(thread, depth) depth_hist[(thread) + THREADS * (depth)]

static void calc_scaffold_depths(void)
{
    _peak_depth = 0;
    shared [1] double *depth_hist = NULL;
    UPC_ALL_ALLOC_CHK(depth_hist, MAX_DEPTH * ((uint64_t)THREADS), sizeof(double));
    if (depth_hist == NULL) {
        DIE("Could not allocate %lld bytes over %d threads\n", (lld)THREADS * sizeof(double) * MAX_DEPTH, THREADS);
    }
    for (int depth = 0; depth < MAX_DEPTH; depth++) {
        int testThread = upc_threadof(&DEPTH_HIST(MYTHREAD, depth));
        if (testThread != MYTHREAD) {
            DIE("Invalid assumption here depth=%d but DEPTH_HIST if from thread %d\n", depth, testThread);
        }
        shared [1] double *a, *b;
        a = &(DEPTH_HIST(MYTHREAD, depth));
        int64_t testRange = a - depth_hist;
        if (testRange >= MAX_DEPTH * (int64_t)THREADS) {
            DIE("depth_hist of %lld exceeds boundaries MAX_DEPTH=%lld max=%lld\n", (lld)testRange, (lld)MAX_DEPTH, (lld)MAX_DEPTH * (int64_t)THREADS);
        }
        DEPTH_HIST(MYTHREAD, depth) = 0;
    }

    int64_t num_too_large = 0;
    int max_depth_found = 0;
    for (int64_t scaffidx = MYTHREAD; scaffidx < _max_scaffolds; scaffidx += THREADS) {
        if (_scaffolds[scaffidx].id == -1) {
            continue;
        }
        double mean_depth = _scaffolds[scaffidx].depth / _scaffolds[scaffidx].weight;
        _scaffolds[scaffidx].depth = mean_depth;
        if ((int)mean_depth < 0) {
            DIE("mean_depth %f < 0 for scaffidx %lld id: %lld\n", mean_depth, (lld)scaffidx, (lld)_scaffolds[scaffidx].id);
        }
        int md = bankers_round(mean_depth);
        if (md < 0) {
            DIE("Depth is negative for scaffold %lld: %d < %d. scaffolds.depth=%f scaffolds.weight=%f\n",
                (lld)_scaffolds[scaffidx].id, MAX_DEPTH, md, _scaffolds[scaffidx].depth,
                _scaffolds[scaffidx].weight);
        }
        if (md >= MAX_DEPTH) {
            num_too_large++;
            if (max_depth_found < md) {
                max_depth_found = md;
            }
        } else {
            shared [1] double *a = &DEPTH_HIST(MYTHREAD, md);
            if ((a - depth_hist) >= MAX_DEPTH * ((int64_t)THREADS)) {
                DIE("depth_hist of %lld exceeds boundaries MAX_DEPTH=%lld max=%lld\n", (lld)(a - depth_hist), (lld)MAX_DEPTH, (lld)MAX_DEPTH * (int64_t)THREADS);
            }
            DEPTH_HIST(MYTHREAD, md) += _scaffolds[scaffidx].weight;
        }
    }
    if (num_too_large > _max_scaffolds / 100) {
        WARN("Depth hist limit is too low (%d) for %lld out of %lld scaffolds (max depth %d): "
             "recompile with HIMPER_MAX_DEPTH set to a larger value\n",
             MAX_DEPTH, (lld)num_too_large, (lld)_max_scaffolds, max_depth_found);
    }
    UPC_TIMED_BARRIER;
    for (int depth = MYTHREAD; depth < MAX_DEPTH; depth += THREADS) {
        double depthCount = 0;
        for (int thread = 0; thread < THREADS; thread++) {
            depthCount += DEPTH_HIST(thread, depth);
        }
        DEPTH_HIST(0, depth) = depthCount;
    }
    UPC_TIMED_BARRIER;
    if (!MYTHREAD) {
        double max_weight = 0;
        for (int depth = 0; depth < MAX_DEPTH; depth++) {
            if (DEPTH_HIST(0, depth) > max_weight) {
                _peak_depth = depth;
                assert(upc_threadof(&(DEPTH_HIST(0, depth))) == MYTHREAD);
                max_weight = DEPTH_HIST(0, depth);
            }
        }
    }
    UPC_TIMED_BARRIER;
    UPC_ALL_FREE_CHK(depth_hist);
    int64_t peak_depth = broadcast_long(_peak_depth, 0);

    tprintf_flush("Modal scaffold depth %ld\n", peak_depth);
    for (int64_t i = MYTHREAD; i < _max_scaffolds; i += THREADS) {
        if (_scaffolds[i].id == -1) {
            continue;
        }
        assert(_scaffolds && _scaffolds[i].depth >= 0.0);
        _scaffolds[i].depth /= peak_depth;
#ifdef DBG_SCAFFOLDS
        for (int64_t j = 0; j < _scaffolds[i].nscaff_contigs; j++) {
            dbg("DSCAF %lld\t%lld\t%.0f\t%.0f\t%.0f\n",
                (lld)_scaffolds[i].id, (lld)_scaffolds[i].contig_ids[j],
                _scaffolds[i].length, _scaffolds[i].depth, _scaffolds[i].weight);
        }
#endif
    }

    UPC_TIMED_BARRIER;
}

static void get_srf_counts(GZIP_FILE f, char *fname)
{
    // read file to get gap count
    _gaps[MYTHREAD].ngaps = 0;
    _gaps[MYTHREAD].found_ngaps = 0;
    _gaps[MYTHREAD].nprocessed = 0;
    int64_t max_scaff_id = 0;
    int64_t max_contig_id = 0;
    int64_t tot_contigs = 0;
    int slen = strlen("Scaffold");
    int clen = strlen("CONTIG");
    const int buf_size = 256;
    char buf[256];
    while (GZIP_GETS(buf, buf_size, f)) {
        CHECK_LINE_LEN(buf, buf_size, fname);
        if (strstr(buf, "GAP")) {
            _gaps[MYTHREAD].found_ngaps++;
        } else {
            tot_contigs++;
            char *end_scaff = strchr(buf, '\t');
            if (!end_scaff) {
                DIE("couldn't get tab\n");
            }
            end_scaff[0] = 0;
            int64_t scaff_id = atol(buf + slen);
            if (scaff_id > max_scaff_id) {
                max_scaff_id = scaff_id;
            }
            char *contig = strstr(end_scaff + 1, "Contig");
            if (!contig) {
                DIE("Couldn't find the Contig in '%s'\n", buf);
            }
            char *end_contig = strchr(contig, '\t');
            if (!end_contig) {
                DIE("couldn't get tab\n");
            }
            end_contig[0] = 0;
            int64_t contig_id = atol(contig + clen);
            if (contig_id > max_contig_id) {
                max_contig_id = contig_id;
            }
        }
    }
    _max_ngaps = reduce_long(_gaps[MYTHREAD].found_ngaps, UPC_ADD, ALL_DEST);
    // each thread gets the same number of gaps, take the ceiling
    _ngaps_block = (_max_ngaps + (int64_t)THREADS - 1) / (int64_t)THREADS;
    // Hard to stop here if there are no gaps... just make sure allocations succeed.
    if (_ngaps_block == 0) {
        _ngaps_block = 1;
    }

    UPC_TIMED_BARRIER;
    // for each thread, the gapi offset depends on the previous counts of found gaps
    _gaps[MYTHREAD].gapi_offset = reduce_prefix_long(_gaps[MYTHREAD].found_ngaps, UPC_ADD, NULL) - _gaps[MYTHREAD].found_ngaps;

    serial_printf("Found %lld total gaps to close - %lld per thread\n", (lld) _max_ngaps, (lld) _ngaps_block);
    tprintf_flush("Found %lld gaps, my offset %lld, max gaps in block %lld, max gaps total %lld\n",
                  (lld)_gaps[MYTHREAD].found_ngaps, (lld)_gaps[MYTHREAD].gapi_offset, (lld)_ngaps_block,
                  (lld)_max_ngaps);
    tprintf_flush("Max contig id %lld, max scaff id %lld\n", (lld)max_contig_id, (lld)max_scaff_id);

    _max_contigs = reduce_long(max_contig_id + 1, UPC_MAX, ALL_DEST);
    _max_scaff_contigs = reduce_long(tot_contigs, UPC_ADD, ALL_DEST);
//    if (_max_scaff_contigs != _max_contigs)
//        SWARN("Mismatch in scaff vs pure contigs %lld != %lld\n", (lld)_max_contigs, (lld)_max_scaff_contigs);
    _max_scaffolds = reduce_long(max_scaff_id + 1, UPC_MAX, ALL_DEST);

    tprintf_flush("Shared allocation: %lld contigs and %lld scaffolds\n",
                  (lld)_max_contigs, (lld)_max_scaffolds);
    GZIP_REWIND(f);
}

void balance_read_allocations()
{
    double startTime = START_TIMER(T_BALANCE_READ_ALLOC);
    int64_t myReadCount = 0, totalReadCount, prefixReadCount, i, blockSize;
    int64_t prefixGaps, totalGaps;
    shared [1] int64_t * first_gapi = NULL;
    UPC_ALL_ALLOC_CHK(first_gapi, THREADS, sizeof(int64_t));
    first_gapi[MYTHREAD] = 0;
    gap_t *g = (gap_t*) _gaps[MYTHREAD].gaps;
    for(i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        int64_t effectiveCount = g[i].max_reads <= MAX_READS_PER_GAP ? g[i].max_reads : MAX_READS_PER_GAP;
        myReadCount += effectiveCount;
    }
    prefixReadCount = reduce_prefix_long(myReadCount, UPC_ADD, &totalReadCount) - myReadCount;
    prefixGaps = reduce_prefix_long(_gaps[MYTHREAD].ngaps, UPC_ADD, &totalGaps) - _gaps[MYTHREAD].ngaps;
    if (totalGaps != _max_ngaps) DIE("Found a different number of gaps! totalGaps=%lld _max_ngaps=%lld\n", (lld) totalGaps, (lld) _max_ngaps);
    UPC_LOGGED_BARRIER;

    blockSize = (totalReadCount + THREADS - 1) / THREADS;
    LOGF("Finding gap index along read count partitions. totalReadCount=%lld blockSize=%lld, prefixReadCount=%lld ngaps=%lld totalGaps=%lld\n", (lld) totalReadCount, (lld) blockSize, (lld) prefixReadCount, (lld) _gaps[MYTHREAD].ngaps, (lld) totalGaps);
    int64_t gapi_offset = prefixGaps, readCounts = prefixReadCount;
    for(i = 0 ; i < _gaps[MYTHREAD].ngaps; i++) {
        int64_t gapi = gapi_offset + i;
        assert(upc_threadof( _gaps[MYTHREAD].gaps + i ) == MYTHREAD);
        gap_t * gap = (gap_t*) (_gaps[MYTHREAD].gaps + i);
        // test for gapi to cross a blockSize boundary of reads
        int64_t effectiveCount = gap->max_reads <= MAX_READS_PER_GAP ? gap->max_reads : MAX_READS_PER_GAP;
        DBG("Testing gapi=%lld - modulus=%lld , block=%lld nextBlock=%lld readCounts=%lld, effectiveCount=%lld\n", (lld) gapi, readCounts % blockSize, (lld) readCounts / blockSize, (lld) (readCounts + effectiveCount) / blockSize, (lld) readCounts, (lld) effectiveCount);
        if ( (((readCounts) % blockSize == 0) && (effectiveCount > 0)) || ((readCounts / blockSize) != ((readCounts + effectiveCount) / blockSize)) ) {
            int thr =  (readCounts) % blockSize == 0 ? (readCounts) / blockSize : (readCounts + effectiveCount) / blockSize;
            if (thr >= THREADS) {
                LOGF("Invalid thread calculated by readCounts=%lld + max_reaads=%lld (%lld)  / blockSize=%lld == thr=%d . gapi=%lld\n", (lld) readCounts, (lld) effectiveCount, (lld) (readCounts+effectiveCount), (lld) blockSize, thr, (lld) gapi);
                break;
            }
            LOGF("first_gapi[%d] = %lld readCounts=%lld\n", thr, (lld) gapi, (lld) readCounts);
            first_gapi[ thr ] = gapi;
        }
        readCounts += effectiveCount;
    }
    UPC_LOGGED_BARRIER;
    // ensure all first_gapi entries are filled
    if (MYTHREAD == 0 || first_gapi[MYTHREAD] != 0) {
        for(i = MYTHREAD+1; i < THREADS; i++) {
            if (first_gapi[i] == 0) {
                first_gapi[i] = first_gapi[MYTHREAD];
            } else {
                break;
            }
        }
    }
    UPC_LOGGED_BARRIER;

    int64_t myFirstOffset = first_gapi[MYTHREAD];
    int64_t myEndOffset = (MYTHREAD == THREADS-1) ? _max_ngaps : first_gapi[MYTHREAD+1];
    if (myEndOffset > _max_ngaps) DIE("How is this possible myEndOffset=%lld but max is %lld. first_gapi[MYTHREAD+1]=%lld\n", (lld) myEndOffset, (lld)  _max_ngaps, MYTHREAD < THREADS-1 ? (lld) first_gapi[MYTHREAD+1] : -1);
    SharedHeapListPtr hl = NULL;
    initHeapList(hl, ONE_MB); // start with 1MB, will grow as needed
    
    LOGF("Allocating reads from gapi=%lld up to %lld\n", (lld) myFirstOffset, (lld) myEndOffset);
    int64_t allocated = 0, remote = 0, totalMem = 0, empty = 0;
    for(int64_t gapi = myFirstOffset; gapi < myEndOffset; gapi++) {
        shared [] gap_t *_gap = GET_GAP(gapi, NULL_ON_FAIL);
        if (_gap == NULL) continue;
        CACHE_LOCAL_RO(gap_t, gap, _gap);
         
        int64_t max_reads = gap->max_reads;
        int64_t effective_max_reads = max_reads;
        if (max_reads > MAX_READS_PER_GAP) {
            tprintf_flush("WARNING: excluding gap %lld: too many reads %ld > %d. Will be subsampling\n",
                          (lld)gap->id, max_reads, MAX_READS_PER_GAP);
            effective_max_reads = MAX_READS_PER_GAP;
        }
        if (max_reads < 0) {
            DIE("negative max_reads %lld\n", (lld)max_reads);
        }
        if (gap->nreads) {
            DIE("This should not happen anymore. gap reads should only be allocated the once now\n");
        }

        if (gap->max_reads) {
            assert(gap->reads_lens == NULL);
            assert(gap->reads_nts == NULL);
            assert(gap->reads_quals == NULL);
            allocated++;
            if (upc_threadof(_gap) != MYTHREAD) remote++;
            int64_t mem = effective_max_reads * (sizeof(int) + 2 * _cfg->max_read_len + 2);
            totalMem += mem;

            DBG("Allocting max_reads %lld new reads %0.3f KB\n", (lld)effective_max_reads, mem * PER_KB);
            SharedCharPtr tmp;
            tmp = allocFromHeapList(hl, effective_max_reads * sizeof(int));
            _gap->reads_lens  = (shared [] int *) tmp;
            tmp = allocFromHeapList(hl, effective_max_reads * (_cfg->max_read_len + 1));
            _gap->reads_nts   = (shared [] char *) tmp;
            tmp = allocFromHeapList(hl, effective_max_reads * (_cfg->max_read_len + 1));
            _gap->reads_quals = (shared [] char *) tmp;
        } else {
            empty++;
        }
    }

    UPC_LOGGED_BARRIER;

    // remember the HeapList for deallocation in free_gaps
    _gaps[MYTHREAD].heapList = hl;

    int64_t globalMem = reduce_long(totalMem, UPC_ADD, ALL_DEST);
    int64_t maxMem = reduce_long(totalMem, UPC_MAX, ALL_DEST);
    double imbalance = 1.0 * maxMem * THREADS / globalMem;
    double endTime = stop_timer(T_BALANCE_READ_ALLOC);
    UPC_ALL_FREE_CHK(first_gapi);

    LOGF("Allocated reads for %lld gaps (%lld remote) for a total of %0.3f MB. %lld empty\n", (lld) allocated, (lld) remote, totalMem * PER_MB, (lld) empty);
    serial_printf("Allocated reads for %0.3f MB globally, %0.3f MB max - %0.3f imbalance in %0.3f s\n", globalMem * PER_MB, maxMem * PER_MB, imbalance, endTime - startTime);
}


static void get_srf_contigs_per_scaffold(GZIP_FILE f, char *fname)
{
    int slen = strlen("Scaffold");
    int clen = strlen("CONTIG");
    const int buf_size = 256;
    char buf[256];

    while (GZIP_GETS(buf, buf_size, f)) {
        CHECK_LINE_LEN(buf, buf_size, fname);
        if (strstr(buf, "GAP")) {
            continue;
        }

        char *end_scaff = strchr(buf, '\t');
        if (!end_scaff) {
            DIE("couldn't get tab\n");
        }
        end_scaff[0] = 0;
        int64_t scaff_id = atol(buf + slen);
        _scaffolds[scaff_id].max_contigs++;
    }
    GZIP_REWIND(f);
}

static void setup_gaps_from_srf(GZIP_FILE f, char *fname)
{
    get_srf_counts(f, fname);
    // all threads have the same number of gaps for load balance
    _gaps[MYTHREAD].ngaps = _ngaps_block;
    int64_t start_offset = _gaps[MYTHREAD].ngaps * MYTHREAD;
    if (start_offset >= _max_ngaps) {
        _gaps[MYTHREAD].ngaps = 0;
    } else if (_gaps[MYTHREAD].ngaps > _max_ngaps - start_offset) {
        _gaps[MYTHREAD].ngaps = _max_ngaps - start_offset;
    }

    if (_gaps[MYTHREAD].ngaps < 0) {
        DIE("Negative ngaps: %lld\n", (lld)_gaps[MYTHREAD].ngaps);
    }

    tprintf_flush("Allocating %lld ngaps (%0.3f MB) for my gaps\n",
                  (lld)_gaps[MYTHREAD].ngaps, ((double)_gaps[MYTHREAD].ngaps) * sizeof(gap_t) / ONE_MB);
    if (_gaps[MYTHREAD].ngaps) {
        UPC_ALLOC_CHK(_gaps[MYTHREAD].gaps, _gaps[MYTHREAD].ngaps * sizeof(gap_t));
    }

    for (int64_t i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        gap_t *gap = (gap_t *)&(_gaps[MYTHREAD].gaps[i]);
        gap->nreads = 0;
        gap->max_reads = 0;
        gap->primer1 = NULL;
        gap->primer2 = NULL;
        gap->cp_contigs = 0;
    }

    DBG2("Allocating %lld _max_contigs\n", (lld)_max_contigs);
    UPC_ALL_ALLOC_CHK(_contigs, _max_contigs, sizeof(contig_t));
    for (int64_t i = MYTHREAD; i < _max_contigs; i += THREADS) {
        _contigs[i].seq = NULL;
        _contigs[i].seq_len = 0;
        _contigs[i].scaff_contig = NULL;
    }

    UPC_ALL_ALLOC_CHK(_scaffolds, _max_scaffolds, sizeof(scaffold_t));

    for (int64_t i = MYTHREAD; i < _max_scaffolds; i += THREADS) {
        _scaffolds[i].id = -1;
        _scaffolds[i].ncontigs = 0;
        _scaffolds[i].max_contigs = 0;
        _scaffolds[i].depth = 0;
        _scaffolds[i].weight = 0;
        _scaffolds[i].scaff_contigs = NULL;
    }
    UPC_TIMED_BARRIER;
    get_srf_contigs_per_scaffold(f, fname);
    UPC_TIMED_BARRIER;
    for (int64_t i = MYTHREAD; i < _max_scaffolds; i += THREADS) {
        if (_scaffolds[i].max_contigs) {
            UPC_ALLOC_CHK(_scaffolds[i].scaff_contigs, _scaffolds[i].max_contigs * sizeof(scaff_contig_t));
        }
    }
    UPC_TIMED_BARRIER;
}

void free_primers(batch_of_gaps_t *batch) {
    START_TIMER(T_FREE_PRIMERS);
    for (long gapi = 0; gapi < batch->ngaps; gapi++) {
        shared gap_t *gap = (shared gap_t *) &batch->gaps[gapi];
        CACHE_LOCAL_RO(gap_t, g, gap);
        if (g->primer1) {
            free_chk(g->primer1);
            gap->primer1 = NULL;
        }
        if (g->primer2) {
            free_chk(g->primer2);
            gap->primer2 = NULL;
        }
    } 
    stop_timer(T_FREE_PRIMERS);
}
void free_gaps()
{
    double startTime = START_TIMER(T_FREE_GAPS);
    LOGF("free_gaps()\n");
    freeHeapList(_gaps[MYTHREAD].heapList);
    if (_gaps[MYTHREAD].gaps) {
        int64_t numRemote=0;
        LOGF("Freeing my %lld gaps\n", (lld) _gaps[MYTHREAD].ngaps);
        for (long gapi = 0; gapi < _gaps[MYTHREAD].ngaps; gapi++) {
           
            gap_t *g = (gap_t *)&(_gaps[MYTHREAD].gaps[gapi]);
            if (g->primer1) {
                DIE("Primers need to be freed before free_gaps!\n"); // free_chk(g->primer1);
            }
            if (g->primer2) {
                DIE("Primers need to be freed before free_gaps!\n"); // free_chk(g->primer2);
            }
            if (g->closure) {
                if (upc_threadof(g->closure) != MYTHREAD) numRemote++;
                UPC_FREE_CHK(g->closure);
            }
            if (g->left_walk) {
                if (upc_threadof(g->left_walk) != MYTHREAD) numRemote++;
                UPC_FREE_CHK(g->left_walk);
            }
            if (g->right_walk) {
                if (upc_threadof(g->right_walk) != MYTHREAD) numRemote++;
                UPC_FREE_CHK(g->right_walk);
            }
        }
        UPC_FREE_CHK(_gaps[MYTHREAD].gaps);
        LOGF("Free %lld remote of %lld\n", (lld) numRemote, _gaps[MYTHREAD].ngaps * 6);
    }
    UPC_LOGGED_BARRIER;

    int64_t numRemote = 0;
    // _contigs are generally ordered by rank, so choose a load balanced starting point at some mulitple of THREADS to start the loop
    // this is important for infiband as hangs have been observed with the original implementataion with no startOffset
    LOAD_BALANCED_FORALL(i, _max_contigs) {
        DBG2("Freeing contig %lld of %lld\n", (lld) i, (lld) _max_contigs);
        if (_contigs[i].seq) {
            if (upc_threadof(_contigs[i].seq) != MYTHREAD) numRemote++;
            UPC_FREE_TRACK(_contigs[i].seq);
        }
        UPC_POLL; // This should not be necessary, but stalls happen without it on some machines
    }
    UPC_FREE_ALL_TRACKED(_gaps[MYTHREAD].gapAllocs);
    freeBuffer( *((Buffer*) &_gaps[MYTHREAD].gapAllocs) );
    LOGF("Freed %lld remote seqs of %lld\n", (lld) numRemote, (lld) _max_contigs / THREADS);
    UPC_LOGGED_BARRIER;

    UPC_ALL_FREE_CHK(_gaps);
    LOGF("All-Freed _gaps\n");
    UPC_ALL_FREE_CHK(_contigs);
    LOGF("All-freed _contigs\n");
    numRemote = 0;
    // _scaffolds are generally ordered by rank, so choose a load balanced starting point at some multiple of THREADS to start the loop
    LOAD_BALANCED_FORALL(i, _max_scaffolds) {
        shared scaffold_t *scaff = &_scaffolds[i];
        if (scaff->id == -1) {
            continue;
        }
        DBG2("Freeing scaff %lld of %lld\n", (lld) i, (lld) _max_scaffolds);
        if (scaff->scaff_contigs) {
            if (upc_threadof(scaff->scaff_contigs) != MYTHREAD) numRemote++;
            UPC_FREE_CHK(scaff->scaff_contigs);
        }
        UPC_POLL; // This should not be necessary, but stalls happen without it on some machines
    }
    double stopTime = stop_timer(T_FREE_GAPS);
    LOGF("Freed %lld remote scaffolds of %lld in %0.3f s\n", (lld) numRemote, (lld) _max_scaffolds / THREADS, stopTime - startTime);
    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(_scaffolds);
    LOGF("All-freed _scaffolds\n");
    
}

// process one or more lines from a srf file that contains the complete records for at least 1 scaffold
#define GAPS_SRF_MAX_TOKS 6
void process_scaffold_report_batch(Buffer batchBuffer, int thread, int64_t gapi, long line)
{
    assert(isValidBuffer(batchBuffer));
    const int max_toks = GAPS_SRF_MAX_TOKS;
    char *tokens[GAPS_SRF_MAX_TOKS];                         // = malloc_chk(max_toks * sizeof(char *));
    int tot_len;
    INIT_CACHE_LOCAL(scaffold_t, scaffold);                  // shared scaffold_t *scaffold = NULL;
    INIT_CACHE_LOCAL_ARRAY(gap_t, 0, gap);                   // shared [] gap_t *gap = NULL;
    INIT_CACHE_LOCAL_ARRAY(scaff_contig_t, 0, scaff_contig); // shared [] scaff_contig_t *scaff_contig = NULL;
    shared contig_t *contig = NULL;
    int64_t contig_id = -1;
    int64_t scaff_start = 0;
    int64_t scaff_end = 0;
    int64_t open_gap = 0;
    Buffer tmpBuf = NULL;
    tmpBuf = initBuffer(1024);
    memset(tokens, 0, sizeof(tokens));
    LOGF("processing batch len=%lld thread=%d gapi=%lld line=%lld\n", (lld)getLengthBuffer(batchBuffer), thread, (lld)gapi, (lld)line);
    while (getsBufferIntoBuffer(batchBuffer, tmpBuf)) {
        if (getReadLengthBuffer(tmpBuf) == 0) {
            continue;
        }
        char *buf = getStartBuffer(tmpBuf);
        DBG2("Processing '%s'\n", buf);
        int ntoks = get_tokens(buf, tokens, &tot_len, max_toks);
        if (ntoks < 3) {
            DIE("Could not process srf line '%s' line=%lld thread=%d gapi=%lld\n", buf, (lld)line, thread, (lld)gapi);
        }
        if (tokens[1][0] == 'C') {
            int64_t scaff_id = GET_SCAFF_ID(tokens[0]);

            if (scaff_id > _max_scaffolds || scaff_id < 0) {
                DIE("scaffold id %lld is too high > %lld\n", (lld)scaff_id, (lld)_max_scaffolds);
            }
            assert(_scaffolds && IS_VALID_UPC_PTR(_scaffolds));

            CACHE_LOCAL(scaffold, &_scaffolds[scaff_id]); // scaffold = &_scaffolds[scaff_id];
            if (scaffold->id == -1) {
                scaffold->id = scaff_id;
            }

            if (!scaffold->scaff_contigs) {
                DIE("Scaffold %lld has null scaff_contigs\n", (lld)scaff_id);
            }

            contig_id = GET_CONTIG_ID(tokens[2] + 1);
            if (contig_id >= _max_contigs || contig_id < 0) {
                DIE("contig id %lld is too high > %lld\n", (lld)contig_id, (lld)_max_contigs);
            }

            int contig_is_cp = strstr(tokens[2], ".cp") ? 1 : 0;

            assert(_contigs && IS_VALID_UPC_PTR(_contigs));
            contig = &_contigs[contig_id];

            int64_t contig_i = scaffold->ncontigs++;
            if (contig_i >= scaffold->max_contigs || contig_i < 0) {
                DIE("Scaffold %lld - Contig id out of range: %lld >= %lld\n",
                    (lld)scaff_id, (lld)contig_i, (lld)scaffold->max_contigs);
            }

            CACHE_LOCAL(scaff_contig, &scaffold->scaff_contigs[contig_i]); //scaff_contig = &scaffold->scaff_contigs[contig_i];

            scaff_contig->gapi_3 = -1;                                     // undef
            scaff_contig->gapi_5 = -1;

            if (!contig_is_cp) {
                if (contig->scaff_contig) {
                    DIE("scaff_contig should be null for contig %s (contig_id=%lld) line=%lld thread=%d\n", tokens[2], (lld)contig_id, (lld)line, thread);
                }
                contig->scaff_contig = CACHE_LOCAL_SHARED_PTR(scaff_contig);
                DBG2("For scaffold %lld set scaff_contig on contig_id=%lld to scaff_contigs contig_i=%lld\n", (lld)scaff_id, (lld)contig_id, (lld)contig_i);
            } else {
                //tprintf_flush("Detected extra contig->scaffold mappings: Contig%lld (%s), Scaffold%lld\n",
                //              (lld) contig_id, tokens[2], (lld) scaff_id);
            }

            scaff_contig->cid = contig_id;
            scaff_contig->strand = tokens[2][0];
            scaff_contig->s_start = atoi(tokens[3]);
            scaff_contig->s_end = atoi(tokens[4]);

            scaff_start = scaff_contig->s_start;
            scaff_end = scaff_contig->s_end;
            scaffold->length = atoi(tokens[4]);
            if (ntoks > 5) {
                double depth = atof(tokens[5]);
                double weight = scaff_end - scaff_start + 1;
                scaffold->depth += (depth * weight);
                scaffold->weight += weight;
            } else if (_cfg->exclude_repeats > 0) {
                tprintf("Warning: Depth information not available. "
                        "Repeat exclusion turned off.\n");
                _cfg->exclude_repeats = 0;
            }
            if (open_gap) {
                //int64_t gi = (gapi - 1) + MYTHREAD * _ngaps_block;
                //gap_t *gap = get_mygap(gi);
                int64_t gi = (gapi - 1) + _gaps[thread].gapi_offset;
                CACHE_LOCAL(gap, get_gap(gi, __LINE__, __FILE__, ABORT_ON_FAIL)); // gap = get_gap(gi, __LINE__, __FILE__, ABORT_ON_FAIL);
                if (gi != gap->id) {
                    DIE("Incorrect gap id: expected %lld, got %lld\n", (lld)gi, (lld)gap->id);
                }
                gap->contig2_id = contig_id;
                if (scaff_contig->strand == '+') {
                    gap->contig_ext2 = 5;
                } else { gap->contig_ext2 = 3; }
                if (scaff_contig->strand == '+') {
                    scaff_contig->gapi_3 = gi;
                } else { scaff_contig->gapi_5 = gi; }
                open_gap = 0;
                if (CACHE_LOCAL_SHARED_PTR(scaff_contig) != contig->scaff_contig) {
                    if (!contig_is_cp) {
                        DIE("Contig should be a copy %s\n", tokens[2]);
                    }
                    gap->cp_contigs = 1;
                    //WARN("dup scaff contig for contig %ld, gap %ld\n", scaff_contig->cid, gap->id);
                }
            }
        } else { // a gap
            if (tokens[1][0] != 'G') {
                DIE("Expected a GAP at line %lld\n", (lld)line);
            }
            if (!contig) {
                DIE("couldn't get contig for gap %lld (contig %lld)\n", (lld)gapi, (lld)contig_id);
            }
            //int64_t gi = gapi + thread * _ngaps_block;
            //gap_t *gap = get_mygap(gi);
            int64_t gi = gapi + _gaps[thread].gapi_offset;
            CACHE_LOCAL(gap, get_gap(gi, __LINE__, __FILE__, ABORT_ON_FAIL)); // gap = get_gap(gi, __LINE__, __FILE__, ABORT_ON_FAIL);
            gap->id = gi;
            gap->size = atoi(tokens[2]);;
            gap->uncertainty = atoi(tokens[3]);
            if (!scaffold) {
                DIE("couldn't get scaffold for gap %lld\n", (lld)gi);
            }
            gap->scaff_id = scaffold->id;
            gap->start = scaff_end;
            gap->end = gap->start + gap->size + 1;
            if (scaff_contig->strand == '+') {
                gap->contig_ext1 = 3;
            } else { gap->contig_ext1 = 5; }
            gap->contig1_id = contig_id;
            gap->nreads = 0;
            if (scaff_contig->strand == '+') {
                scaff_contig->gapi_5 = gi;
            } else { scaff_contig->gapi_3 = gi; }
            if (CACHE_LOCAL_SHARED_PTR(scaff_contig) != contig->scaff_contig) {
                gap->cp_contigs = 1;
                //WARN("dup scaff contig for contig %ld, gap %ld\n", scaff_contig->cid, gap->id);
            }
            open_gap = 1;
            gapi++;
        }
        resetBuffer(tmpBuf);
        line++;
    }
    FINISH_CACHE_LOCAL(scaffold);
    FINISH_CACHE_LOCAL(scaff_contig);
    FINISH_CACHE_LOCAL(gap);
    resetBuffer(batchBuffer);
}

typedef struct {
    shared [] char *buf;
    long    line;
    int64_t gapi;
    long    len;
    int     thread;
} SRF_batch_t;

void process_scaffold_report_batch2(SRF_batch_t *batch, Buffer scratchBuffer)
{
    resetBuffer(scratchBuffer);
    growBuffer(scratchBuffer, batch->len + 1);
    upc_memget(appendBuffer(scratchBuffer, batch->len), batch->buf, batch->len);
    UPC_FREE_CHK(batch->buf);
    process_scaffold_report_batch(scratchBuffer, batch->thread, batch->gapi, batch->line);
}

int try_push_srf_batch_buffer(CircularBuffer cb, Buffer batchBuffer, int thread, int64_t last_gapi, int64_t last_line)
{
    if (isFullCircularBuffer(cb, thread)) {
        return 0;
    }
    SRF_batch_t tmpbatch;
    memset(&tmpbatch, 0, sizeof(SRF_batch_t));
    tmpbatch.line = last_line;
    tmpbatch.gapi = last_gapi;
    tmpbatch.len = getLengthBuffer(batchBuffer);
    tmpbatch.thread = thread;
    UPC_ALLOC_CHK(tmpbatch.buf, tmpbatch.len);
    upc_memput(tmpbatch.buf, getStartBuffer(batchBuffer), tmpbatch.len);
    upc_fence;
    if (tryPushCircularBuffer(cb, thread, &tmpbatch, NULL, NULL)) {
        // successful do not reuse the buffer
        resetBuffer(batchBuffer);
        return 1;
    } else {
        // failed, free shared memory and leave the batchBuffer untouched
        UPC_FREE_CHK(tmpbatch.buf);
        return 0;
    }
}

// Reads in scaffolds and computes weight and depth statistics
// Only reads those scaffolds relevant to this thread (set of gaps)
// Format of file:
// scaffold_id CONTIGi contig_id start end depth
// scaffold_id GAPi gap_size gap_uncertainty
// returns the number of gaps in a block
int64_t process_scaffold_report_file(void)
{
    START_TIMER(T_PROCESS_SRF);
    const int buf_size = 256;
    char *buf = malloc_chk(buf_size);
    char *buf2 = malloc_chk(buf_size);

    char fname[MAX_FILE_PATH * 2 + 10];
    sprintf(fname, "%s" GZIP_EXT, _cfg->sr_file);
    GZIP_FILE f = openCheckpoint(fname, "r");
    START_TIMER(T_SRF_COUNTS);
    setup_gaps_from_srf(f, fname);
    stop_timer(T_SRF_COUNTS);

    UPC_TIMED_BARRIER;

    int scaffoldsPerBatch = 2000;
    int numCB = (MYTHREAD == 0 ? (THREADS * 2 / MYSV.cores_per_node) : 0) + MYSV.cores_per_node; // a lot for thread 0, a few for the rest

    CircularBuffer cb = initCircularBuffer(numCB, sizeof(SRF_batch_t));
    GlobalFinished gf = initGlobalFinished(MYSV.cores_per_node);

    const int max_toks = GAPS_SRF_MAX_TOKS;
    char *tokens[GAPS_SRF_MAX_TOKS]; // = malloc_chk(max_toks * sizeof(char *));
    int tot_len;
    int64_t nscaffolds = 0;
    int64_t ncontigs = 0;
    int64_t gapi = 0, last_gapi = 0;
    int scaff_depths_avail = 0;

    serial_printf("Processing scaffold_report %s... ", fname);
    tprintf_flush("Processing scaffold_report %s... ", fname);
    int64_t last_scaff_id = -1;
    long line = 0, last_line = 0;
    int64_t batchesPushed = 0, batchesProcessed = 0;

    START_TIMER(T_SRF_READS);
    Buffer batchBuffer = initBuffer(8192);
    while (GZIP_GETS(buf, buf_size, f)) {
        strcpy(buf2, buf);
        int ntoks = get_tokens(buf2, tokens, &tot_len, max_toks);
        if (tokens[1][0] == 'C') {
            ncontigs++;
            if (ntoks > 5) {
                scaff_depths_avail = 1;
            }
            int64_t scaff_id = GET_SCAFF_ID(tokens[0]);
            if (last_scaff_id != scaff_id) {
                nscaffolds++;
                if (last_scaff_id >= 0 && nscaffolds % scaffoldsPerBatch == 0) {
                    int isPushed = try_push_srf_batch_buffer(cb, batchBuffer, MYTHREAD, last_gapi, last_line);
                    if (!isPushed) {
                        process_scaffold_report_batch(batchBuffer, MYTHREAD, last_gapi, last_line);
                        batchesProcessed++;
                    } else {
                        LOGF("Pushed a batch on my queue last_gapi=%lld last_line=%lld\n", (lld)last_gapi, (lld)last_line);
                        batchesPushed++;
                    }
                    last_line = line;
                    last_gapi = gapi;
                    resetBuffer(batchBuffer);
                }
                last_scaff_id = scaff_id;
            }
        } else { // a gap
            if (tokens[1][0] != 'G') {
                DIE("Expected a GAP at line %lld\n", (lld)line);
            }
            gapi++;
        }
        assert(buf[strlen(buf) - 1] == '\n');
        printfBuffer(batchBuffer, "%s", buf);
        line++;
    }
    //
    // cleanup
    //

    // process last batchBuffer
    if (getLengthBuffer(batchBuffer) > 0) {
        LOGF("Processing my last batch. last_gapi=%lld last_line=%lld\n", (lld)last_gapi, (lld)last_line);
        process_scaffold_report_batch(batchBuffer, MYTHREAD, last_gapi, last_line);
        batchesProcessed++;
    }

    // process remaining batches in my queue, then steal from other threads
    SRF_batch_t tmpBatch;
    int stealThread = MYTHREAD;
    int iAmFinished = 0;
    int64_t workStolen = 0, stealRounds = 0;
    while (!isGlobalFinished(gf)) {
        resetBuffer(batchBuffer);
        int didPop = tryPopCircularBuffer(cb, stealThread, &tmpBatch, NULL, NULL);
        if (didPop) {
            if (stealThread != MYTHREAD) {
                workStolen++;
            }
            process_scaffold_report_batch2(&tmpBatch, batchBuffer);
            batchesProcessed++;
        } else {
            if (isEmptyCircularBuffer(cb, stealThread)) {
                if (MYTHREAD == stealThread) {
                    if (!iAmFinished) {
                        iAmFinished = 1;
                        incrementGlobalFinished(gf);
                        LOGF("I am finished now... starting to steal\n");
                    } else {
                        if (stealRounds % 100 == 0) {
                            LOGF("Going around again (%lld), waiting for isGlobalFinished: %lld (%lld)\n", (lld)stealRounds + 1, (lld)getGlobalFinishedCountSlow(gf), (lld)getGlobalFinishedCountFast(gf));
                        }
                    }
                }
                stealThread = (stealThread + 1) % THREADS;
                if (stealThread == MYTHREAD) {
                    stealRounds++;
                }
            }
            UPC_POLL;
        }
    }

    freeGlobalFinished(&gf);
    freeCircularBuffer(&cb);
    freeBuffer(batchBuffer);

    LOGF("batchesPushed=%lld batchesProcessed=%lld workStolen=%lld stealRounds=%lld\n", (lld)batchesPushed, (lld)batchesProcessed, (lld)workStolen, (lld)stealRounds);

    stop_timer(T_SRF_READS);
    closeCheckpoint(f);
    free_chk(buf);
    free_chk(buf2);

//    if (gapi != _gaps[MYTHREAD].ngaps)
//        DIE("Wrong gap count second time around %lld != %lld\n", (lld) gapi, (lld) _gaps[MYTHREAD].ngaps);
    tprintf_flush("\n... %ld gaps found, %ld scaffolds found, %ld contigs found\n",
                  gapi, nscaffolds, ncontigs);
#ifdef CONFIG_SANITY_CHECK
    UPC_TIMED_BARRIER;

    int64_t tot_gaps_processed = reduce_long(gapi, UPC_ADD, ALL_DEST);
    if (tot_gaps_processed != _max_ngaps) {
        SDIE("Gap count mismatch: expected %lld total, found %lld instead\n",
             _max_ngaps, tot_gaps_processed);
    }

    for (int64_t i = MYTHREAD; i < _max_scaffolds; i += THREADS) {
        scaffold = &_scaffolds[i];
        if (scaffold->id == -1) {
            continue;
        }
        if (scaffold->ncontigs != scaffold->max_contigs) {
            DIE("Scaffold %ld: didn't find all contigs: %lld != %lld\n",
                scaffold->id, (lld)scaffold->ncontigs, (lld)scaffold->max_contigs);
        }
    }
#endif
    UPC_TIMED_BARRIER;
#ifdef DBG_CONTIGS
    char gbuf[256];
    assert(_contigs && IS_VALID_UPC_PTR(_contigs));
    for (int64_t i = 0; i < _max_contigs; i++) {
        if (_contigs[i].id == -1) {
            continue;
        }
        dbg("DCONT %cContig%lld\t%d\t%d\n", _contigs[i].strand, (lld)_contigs[i].id,
            _contigs[i].s_start, _contigs[i].s_end);
    }
#endif
    START_TIMER(T_CALC_SCAFF_DEPTH);
    scaff_depths_avail = reduce_int(scaff_depths_avail, UPC_MAX, ALL_DEST);
    if (scaff_depths_avail) {
        calc_scaffold_depths();
    }
    stop_timer(T_CALC_SCAFF_DEPTH);
    stop_timer(T_PROCESS_SRF);
    serial_printf("done in %.3f s\n", get_elapsed_time(T_PROCESS_SRF));
    serial_printf("Modal scaffold depth %d\n", _peak_depth);
    ADD_DIAG("%d", "modal_scaffold_depth", _peak_depth);
    print_memory_used();
    return _ngaps_block;
}

// get list of contigs and add to hash table of contigs
// file format:
// >contig_id
// contig (split over multiple lines)
//
// Each thread reads the same whole contigs file. It only gets the data it needs
// for the gaps it processes. This makes the read quicker and uses less memory.
// An attempt to do this as a shared data structure, loading the whole file into
// memory is actually a lot slower. So we'll keep it this simpler way.
void gc_process_contigs_file(int lc_masked)
{
    START_TIMER(T_PROCESS_CONTIGS);

    assert(_contigs && IS_VALID_UPC_PTR(_contigs));
    long num_entries = 0;
    INIT_CACHE_LOCAL(contig_t, contig); // shared contig_t *contig;
    char fname[MAX_FILE_PATH * 3];
    sprintf(fname, "%s.fasta" GZIP_EXT, _cfg->contigs_file);
    gzFile f = openCheckpoint1(fname, "r");
    if (!f) {
        DIE("Could not open %s\n", fname);
    }
    serial_printf("Processing %s... ", fname);
    tprintf_flush("Processing %s...\n", fname);
    kseq_t *ks = kseq_init(f);
    while (kseq_read(ks) >= 0) {
        int64_t contig_id = GET_CONTIG_ID(ks->name.s);
        if (contig_id >= _max_contigs || contig_id < 0) {
            tprintf_flush("Skipping unused contig: contig_id %lld > max_contigs %lld\n",
                          (lld)contig_id, (lld)_max_contigs);
            continue;
        }
        assert(contig_id >= 0);
        assert(_contigs && IS_VALID_UPC_PTR(_contigs));
        if (!_contigs[contig_id].scaff_contig) {
            continue;
        }
        CACHE_LOCAL(contig, &_contigs[contig_id]); // contig = &_contigs[contig_id];
        int seq_len = ks->seq.l;
        DBG2("Allocating new contig len %lld\n", (lld)seq_len);
        if (contig->seq) {
            WARN("contig %lld has already been assigned a sequence!\n", (lld)contig_id);
            UPC_FREE_TRACK(contig->seq);
        }
        UPC_ALLOC_TRACK(contig->seq, seq_len + 1, _gaps[MYTHREAD].gapAllocs);
        if (!contig->seq) {
            DIE("Could not allocate %d for new seq\n", seq_len);
        }

        // in diploid mode expect mixed case input; convert all to uppercase
        if (lc_masked) {
            for (int i = 0; i < ks->seq.l; i++) {
                if (ks->seq.s[i] >= 'a') {
                    ks->seq.s[i] -= ('a' - 'A'); // or subtract 32 per ASCII table; converts to uppercase
                }
            }
        }
        upc_memput(contig->seq, ks->seq.s, seq_len + 1);
        contig->seq_len = seq_len;
        num_entries++;
    }
    FINISH_CACHE_LOCAL(contig);
    kseq_destroy(ks);
    closeCheckpoint1(f);
    stop_timer(T_PROCESS_CONTIGS);
    serial_printf("done in %.3f s\n", get_elapsed_time(T_PROCESS_CONTIGS));
    tprintf_flush("\n... %ld contigs added\n", num_entries);
    print_memory_used();
    UPC_TIMED_BARRIER;
#ifdef DBG_SEQUENCES
    if (MYTHREAD == 0) {
        assert(_contigs && IS_VALID_UPC_PTR(_contigs));
        for (int64_t i = 0; i < _max_contigs; i++) {
            if (_contigs[i].id != -1) {
                UPC_MEMGET_STR(seq, _contigs[i].seq, _contigs[i].seq_len + 1);
                if (_contigs[i].id != i) {
                    DIE("wrong contig id %d != %d\n", _contigs[i].id, i);
                }
                dbg("DSEQ %d %d %s\n", _contigs[i].id, _contigs[i].seq_len, seq);
            }
        }
    }
#endif
}

static inline char classify_alignment(int unaligned, int projected, int s_length,
                                      int wiggle_room, int truncate, int truncate_val)
{
    int projected_off = 0;

    if (projected < 1) {
        projected_off = 1 - projected;
    } else if (projected > s_length) {
        projected_off = projected - s_length;
    }
    int missing_bases = unaligned - projected_off;
    // classify this alignment
    if (unaligned == 0) {
        return 'F'; // FUL
    }
    if (projected_off > 0 && missing_bases < wiggle_room) {
        return 'G'; // GAP
    }
    if (unaligned < wiggle_room || truncate == truncate_val) {
        return 'I'; // INC
    }
    return 0;       // rejected
}


#define MAX_ALIGNMENT_TOKENS 14
static int get_alignment(char *buf, alignment_t *aln, char *pair_name)
{
    assert(buf != NULL);
    assert(aln != NULL);
    assert(strlen(buf));
    assert(buf[0] == 'M');
    START_TIMER(T_GET_ALN);
    // 14 fields, tab separated
    const int max_toks = MAX_ALIGNMENT_TOKENS;
    char *tokens[MAX_ALIGNMENT_TOKENS];
    int tot_len;
    int ntoks = get_tokens(buf, tokens, &tot_len, max_toks);
    if (ntoks != max_toks) {
        return REJECT_FORMAT;
    }
    assert(tokens[0][0] == 'M'); assert(tokens[0][10] == '-'); // MERALIGNER-?
    if (tokens[0][11] == 'F') {
        return REJECT_NO_ASSOCIATED_GAP;
    }
    //aln->identities = atoi(tokens[12]);
    //if (aln->identities < _cfg->mer_size)
    //   return REJECT_MINLEN;
    aln->contig_id = GET_CONTIG_ID(tokens[5]);
    shared contig_t *_contig = get_contig(aln->contig_id);
    if (!_contig) {
        return REJECT_NO_SCAFFOLD_INFO;
    }
    CACHE_LOCAL_RO(contig_t, contig, _contig);
    // If there's no gap associated with the contig, skip the alignment
    if (!contig->scaff_contig) {
        return REJECT_NO_ASSOCIATED_GAP;
    }
    if (contig->scaff_contig->gapi_3 == -1 && contig->scaff_contig->gapi_5 == -1) {
        return REJECT_NO_ASSOCIATED_GAP;
    }
    char *name;
    if (!get_fq_name_dirn(tokens[1], &name, &aln->dirn)) {
        // not a paired read
        pair_name[0] = '\0';
    } else {
        if (!pair_name[0]) {
            strcpy(pair_name, name);
        }
    }

    aln->q_start = atoi(tokens[2]);
    aln->q_stop = atoi(tokens[3]);
    aln->q_length = atoi(tokens[4]);

    aln->s_start = atoi(tokens[6]);
    aln->s_stop = atoi(tokens[7]);
    aln->s_length = atoi(tokens[8]);
    char *strand = tokens[9];
    if (strcmp(strand, "Plus") != 0 && strcmp(strand, "Minus") != 0) {
        DIE("Invalid entry in meraligner file:\n%s\n", buf);
    }
    aln->strand = (strand[0] == 'P' ? '+' : '-');
    //aln->score = atoi(tokens[10]);
    //aln->e_value = atoi(tokens[11]);

    //aln->align_length = atoi(tokens[13]);
    aln->start_status = 0;
    aln->end_status = 0;
    stop_timer(T_GET_ALN);
#ifdef DBG_GET_ALIGNMENT
    char alnbuf[200];
    dbg("DGET %s\n", get_aln_info(aln, name, alnbuf));
#endif
    return GOOD_ALIGNMENT;
}

static void add_read_gap_pair(htable_t pairs, int64_t gap_id, const alignment_t *aln, const char *name)
{
    char buf[100];

    sprintf(buf, "%s/%d:%lld", name, aln->dirn, (lld)gap_id);
    read_gap_pair_t *pair = htable_get_read_gap_pair(pairs, buf);
    if (!pair) {
        pair = malloc_chk0(sizeof(read_gap_pair_t));
        strcpy(pair->read_name, name);
        pair->gap_id = gap_id;
        CHECK_ERR(htable_put_read_gap_pair(pairs, strdup_chk0(buf), pair, NO_CHECK_DUPS));
    }
    pair->dirn = aln->dirn;
    pair->strand = aln->strand;
    pair->contig_id = aln->contig_id;
}

// to compute alignment, sum up 3 constants, e.g. LEFT+PLUS+FIVE means the read
// is aligned on the Plus strand to the contig to the Left of the gap whose 5'
// end is adjacent to the gap
static const int LEFT = 0;
static const int PLUS = 0;
static const int FIVE = 0;
static const int THREE = 1;
static const int MINUS = 2;
static const int RIGHT = 4;
static const char combos[] = { '-', '+', '+', '-', '+', '-', '-', '+' };

static char get_orient(int64_t contig_id, char strand, int64_t gi, int line)
{
    if (gi == -1) {
        DIE("gi is -1\n");
    }
    int combo = 0;
    CACHE_LOCAL_RO(gap_t, gap, get_gap(gi, __LINE__, NULL, ABORT_ON_FAIL)); //gap = get_gap(gi, __LINE__, NULL, ABORT_ON_FAIL);
    int64_t cid_1 = gap->contig1_id;
    int64_t cid_2 = gap->contig2_id;
    if (cid_1 == contig_id) {
        combo = LEFT + (gap->contig_ext1 == 3 ? THREE : FIVE);
    } else if (cid_2 == contig_id) {
        combo = RIGHT + (gap->contig_ext2 == 3 ? THREE : FIVE);
    } else {
        DIE("From line %d: Neither Contig%lld nor Contig%lld match Contig%lld for gap %lld\n",
            line, (lld)cid_1, (lld)cid_2, (lld)contig_id, (lld)gi);
    }
    combo += (strand == '+' ? PLUS : MINUS);
    return combos[combo];
}

static int _num_reallocs = 0;

static void add_to_read_list(read_list_t *read_list, char *read_name, int64_t gap_id, char orient, int lib_is_rc)
{
    read_orient_t *read_orient = &read_list->reads[read_list->num];

    strcpy(read_orient->name, read_name);
    read_orient->gap_id = gap_id;
    read_orient->orient = orient;
    read_orient->lib_is_rc = lib_is_rc;
    read_list->num++;
    if (read_list->num == read_list->size) {
        read_list->size *= 1.5;
        read_list->reads = realloc_chk(read_list->reads, read_list->size * sizeof(read_orient_t));
        _num_reallocs++;
    }
    // increment the number of reads for this gap
    UPC_ATOMIC_FADD_I64(NULL, &(get_gap(gap_id, __LINE__, NULL, ABORT_ON_FAIL)->max_reads), 1);
}

static long direct_alignments(const pair_alns_t *pair_alns, read_list_t *read_list, int lib_is_rc)
{
    START_TIMER(T_DIRECT_ALNS);
    const long max_pairs = pair_alns->nalns * 2;
    htable_t pairs = create_htable(max_pairs * 4, "pairs");
    long nplaced = 0;
    for (long i = 0; i < pair_alns->nalns; i++) {
        alignment_t *aln, _aln;
        if (upc_threadof(pair_alns->alns + i) == MYTHREAD) {
            aln = (alignment_t *)(pair_alns->alns + i);
        } else {
            _aln = pair_alns->alns[i];
            aln = &_aln;
        }
        DBG2("Finding contig %lld from alignment %lld of %lld\n", (lld)aln->contig_id, (lld)i, (lld)pair_alns->nalns);
        shared contig_t *contig;
        get_contig_chk(contig, aln->contig_id);
        // If the read aligns into a gap, add a read<->gap pair
        if (aln->start_status == 'G') {
            int64_t gap_id = aln->strand == '+' ? contig->scaff_contig->gapi_3 : contig->scaff_contig->gapi_5;
            if (gap_id != -1) {
                add_read_gap_pair(pairs, gap_id, aln, pair_alns->name);
            }
        }
        if (aln->end_status == 'G') {
            int64_t gap_id = aln->strand == '+' ? contig->scaff_contig->gapi_5 : contig->scaff_contig->gapi_3;
            if (gap_id != -1) {
                add_read_gap_pair(pairs, gap_id, aln, pair_alns->name);
            }
        }
    }
    char read_name[MAX_READ_NAME_LEN + 10];
    htable_iter_t iter = htable_get_iter(pairs);
    read_gap_pair_t *pair;
    // For reads aligning directly into a gap, record the orientation of the
    // read wrt to the gap
    while ((pair = htable_get_next_read_gap_pair(pairs, iter)) != NULL) {
        sprintf(read_name, "%s/%d", pair->read_name, pair->dirn);
        char orient = get_orient(pair->contig_id, pair->strand, pair->gap_id, __LINE__);
        // Store the read_name <-> (gap_id, orient) mapping. We then later use
        // this read_name as a lookup for getting nts and qual info from the
        // fastq file
        add_to_read_list(read_list, read_name, pair->gap_id, orient, lib_is_rc);
        nplaced++;
        free_chk0(pair);
    }
    free_chk0(iter);
    destroy_htable(pairs, FREE_KEYS);
    stop_timer(T_DIRECT_ALNS);
    return nplaced;
}

static char *get_mate_name(char *name)
{
    char *mate = strdup_chk0(name);
    int mi = strlen(mate) - 1;

    mate[mi] = (name[mi] == '1' ? '2' : '1');
    return mate;
}

static long projected_alignments(const pair_alns_t *pair_alns, read_list_t *read_list, int insert_size,
                                 int insert_sigma, int lib_is_rc)
{
    START_TIMER(T_PROJECTED_ALNS);
    const long max_pairs = pair_alns->nalns * 2;
    htable_t reads_to_alns = create_htable(max_pairs * 4, "reads_to_alns");
    int max_aligned_scaff_len = 0;
    long nprojected = 0;
    shared contig_t *contig = NULL;
    for (long i = 0; i < pair_alns->nalns; i++) {
        alignment_t _aln; const alignment_t *aln;
        if (upc_threadof(pair_alns->alns) == MYTHREAD) {
            aln = (const alignment_t *)&(pair_alns->alns[i]);
        } else {
            _aln = pair_alns->alns[i];
            aln = &_aln;
        }
        contig = NULL;
        get_contig_chk(contig, aln->contig_id);
        CACHE_LOCAL_RO(scaff_contig_t, scaff_contig, contig->scaff_contig);
        if (aln->contig_id != scaff_contig->cid) {
            DIE("Contig id %ld != scaff contig %ld\n", aln->contig_id, scaff_contig->cid);
        }
        // one alignment is recorded for each read to be used to project the
        // read's mate if needed the alignment to the largest scaffold is
        // retained
        // each read is assigned to a single contig/scaffold for this purpose
        // mate pairs may only be projected into gap(s) on a single scaffold by
        // this construction (although direct read alignments may place reads in
        // gaps on multiple scaffolds)
        int64_t gap_id = aln->strand == '+' ? scaff_contig->gapi_5 : scaff_contig->gapi_3;
        if (gap_id == -1) {
            continue;
        }
        //shared scaffold_t *scaffold = get_scaffold(get_gap(gap_id, __LINE__, NULL, ABORT_ON_FAIL)->scaff_id);
        shared [] gap_t * _gap = get_gap(gap_id, __LINE__, NULL, NULL_ON_FAIL);
        if (!_gap) {
            DIE("Gap %ld out of range, obtained from contig %ld (gaps %ld, %ld)\n",
                gap_id, scaff_contig->cid, scaff_contig->gapi_3, scaff_contig->gapi_5);
        }
        shared scaffold_t *scaffold = get_scaffold(_gap->scaff_id);
        int scaff_len = scaffold->length;
        char buf[MAX_READ_NAME_LEN * 2];
        sprintf(buf, "%s/%d", pair_alns->name, aln->dirn);
        read_aln_t *read_aln = htable_get_read_aln(reads_to_alns, buf);
        if (read_aln) {
            if (scaff_len > max_aligned_scaff_len) {
                read_aln->aln = i;
                max_aligned_scaff_len = scaff_len;
            }
        } else {
            read_aln = malloc_chk0(sizeof(read_aln_t));
            strcpy(read_aln->name, buf);
            read_aln->aln = i;
            CHECK_ERR(htable_put_read_aln(reads_to_alns, read_aln->name, read_aln, NO_CHECK_DUPS));
            max_aligned_scaff_len = 0;
        }
    }
    const int project_z = 2;
    int max_project = insert_size + project_z * insert_sigma;
    int min_project = insert_size - project_z * insert_sigma;
    DBG2("Iterating through reads_to_alns\n");
    read_aln_t *read_aln;
    htable_iter_t iter = htable_get_iter(reads_to_alns);
    while ((read_aln = htable_get_next_read_aln(reads_to_alns, iter)) != NULL) {
        DBG2("got read %s\n", read_aln->name);
        char *mate_name = get_mate_name(read_aln->name);
        if (!mate_name) {
            DIE("Could not determine mate name for read %s\n", read_aln->name);
        }
        // Attempt to project the unplaced mate into a gap
        // assumes unplaced mate is same length as the placed one
        if (htable_get_read_aln(reads_to_alns, mate_name)) {
            free_chk0(mate_name);
            continue;
        }
        // only project if the mate has no alignment
        alignment_t _aln; const alignment_t *aln;
        if (upc_threadof(pair_alns->alns) == MYTHREAD) {
            aln = (const alignment_t *)&(pair_alns->alns[read_aln->aln]);
        } else {
            _aln = pair_alns->alns[read_aln->aln];
            aln = &_aln;
        }
        int unaligned_start = aln->q_start - 1;
        int far_projection = max_project - unaligned_start;
        int near_projection = min_project - aln->q_length - unaligned_start;
        int64_t start_gap = -1;
        shared contig_t *contig;
        get_contig_chk(contig, aln->contig_id);
        CACHE_LOCAL_RO(scaff_contig_t, scaff_contig, contig->scaff_contig);
        if (aln->strand == '+') {
            far_projection = aln->s_start + far_projection;
            near_projection = aln->s_start + near_projection;
            start_gap = scaff_contig->gapi_5;
        } else {
            far_projection = aln->s_stop - far_projection;
            near_projection = aln->s_stop - near_projection;
            start_gap = scaff_contig->gapi_3;
        }
        if (start_gap == -1) {
            free_chk(mate_name);
            continue;
        }
        int64_t test_gapi = start_gap;
        CACHE_LOCAL_RO(gap_t, sgap, get_gap(start_gap, __LINE__, NULL, ABORT_ON_FAIL)); // shared [] gap_t *sgap = get_gap(start_gap, __LINE__, NULL, ABORT_ON_FAIL);
        int64_t scaff_id = sgap->scaff_id;
        int64_t cid_1 = sgap->contig1_id;
        int64_t cid_2 = sgap->contig2_id;
        // find anchor-contig origin position in scaffold coord, assign
        // direction of scan via iterator and transform projection bounds to
        // scaffold coordinate system
        int left_projection = -1;
        int right_projection = -1;
        int orient = -1;
        int iterator = 0;
        orient = get_orient(aln->contig_id, aln->strand, start_gap, __LINE__);
        if (aln->contig_id == cid_1) {
            // the contig to the left of the gap is aligned to the projector
            iterator = 1;
            if (aln->strand == '+') {
                int contig_origin = sgap->start - aln->s_length + 1;
                left_projection = contig_origin + near_projection;
                right_projection = contig_origin + far_projection;
            } else {
                int contig_origin = sgap->start;
                left_projection = contig_origin - near_projection;
                right_projection = contig_origin - far_projection;
            }
        } else if (aln->contig_id == cid_2) {
            // the contig to the right of the gap is aligned to the projector
            iterator = -1;
            if (aln->strand == '+') {
                int contig_origin = sgap->end + aln->s_length - 1;
                left_projection = contig_origin - far_projection;
                right_projection = contig_origin - near_projection;
            } else {
                int contig_origin = sgap->end;
                left_projection = contig_origin + far_projection;
                right_projection = contig_origin + near_projection;
            }
        } else {
            DIE("should never get here\n");
        }
        // The orientation of the projected read is opposite the projector
        orient = (orient == '+') ? '-' : '+';
        // Iterate over gaps, projecting mate into each gap that is within range
        // Note that we can be iterating up or down, i.e. iterator can be 1 or -1
        // Each thread is allocated a fixed block of gaps of size _ngaps_block, but only
        // _gaps[MYTHREAD].ngaps of those are used. So to iterate over all gaps, we need to skip
        // over the unused entries at the end of each thread block. We do this setting the
        // get_gap function to return NULL when a gap is out of range of allocated gaps
        for (; test_gapi < _max_ngaps && test_gapi >= 0; test_gapi += iterator) {
            shared [] gap_t * _gap = get_gap(test_gapi, __LINE__, NULL, NULL_ON_FAIL);
            // only process if we have are in the range of gaps for this thread
            if (_gap) {
                CACHE_LOCAL_RO(gap_t, gap, _gap);
                if (gap->scaff_id != scaff_id) {
                    break;
                }
                if (gap->cp_contigs) {
                    break;
                }
                int placed = 0;
                int left = gap->start;
                int right = gap->end;
                if (gap->start >= gap->end) {
                    left = gap->end;
                    right = gap->start;
                }
                if (right_projection > left && left_projection < right) {
                    placed = 1;
                } else if ((iterator == 1 && left > right_projection) ||
                           (iterator == -1 && right < left_projection)) {
                    break;
                }
                if (placed) {
                    add_to_read_list(read_list, mate_name, test_gapi, orient, lib_is_rc);
                    nprojected++;
                }
            }
        }
        free_chk0(mate_name);
    }
    free_chk0(iter);
    destroy_htable(reads_to_alns, FREE_VALS);
    stop_timer(T_PROJECTED_ALNS);
    return nprojected;
}


static int get_pair_aln(tmp_pair_alns_t *pair_alns, char *buf, long *rejected,
                        int reverse_complement, int five_prime_wiggle, int three_prime_wiggle)
{
    alignment_t aln;

    DBG2("get_pair_aln: buf=%.*s\n", 40, buf);

    int res = get_alignment(buf, &aln, pair_alns->name); // tokenizes buf!

    if (res != GOOD_ALIGNMENT) {
        rejected[res]++;
        return pair_alns->nalns;
    }
    // Assess alignment for completeness (do this before scaffold
    // coordinate conversion!)
    // Important: Truncations are performed before reverse complementation
    // and apply to the end of the actual read
    int unaligned_start = aln.q_start - 1;
    int projected_start = (aln.strand == '+' ?
                           aln.s_start - unaligned_start :
                           aln.s_stop + unaligned_start);
    aln.start_status = classify_alignment(unaligned_start, projected_start, aln.s_length,
                                          five_prime_wiggle, _cfg->truncate, 5);
    if (!aln.start_status) {
        rejected[REJECT_5_TRUNCATED]++;
        return pair_alns->nalns;
    }

    int unaligned_end = aln.q_length - aln.q_stop;
    int projected_end = (aln.strand == '+' ?
                         aln.s_stop + unaligned_end :
                         aln.s_start - unaligned_end);
    aln.end_status = classify_alignment(unaligned_end, projected_end, aln.s_length,
                                        three_prime_wiggle, _cfg->truncate, 3);
    if (!aln.end_status) {
        rejected[REJECT_3_TRUNCATED]++;
        return pair_alns->nalns;
    }
    // Re-orient alignment if requested
    if (reverse_complement) {
        aln.strand = (aln.strand == '+' ? '-' : '+');
        int tmp_q_start = aln.q_start;
        aln.q_start = aln.q_length - aln.q_stop + 1;
        aln.q_stop = aln.q_length - tmp_q_start + 1;
    }
    pair_alns->alns[pair_alns->nalns] = aln;
    pair_alns->nalns++;
    assert(pair_alns->nalns <= MAX_PAIR_ALNS);
    return pair_alns->nalns;
}

// returns the base name of a pair: @pair from any: '@pair/1' '@pair 1:Y:...' etc
static int get_base_name(char *name, const char *buf)
{
    char *start_name = strchr(buf, '@');

    if (!start_name) {
        return 0;
    }
    assert(start_name != buf);
    assert(*(start_name - 1) == '\t');

    char *end_name = strchr(start_name, '\t');
    if (!end_name) {
        DIE("Invalid format for alignment: %s\n", buf);
    }
    int len = (end_name - start_name) - 1;
    if (len > 3 && start_name[len - 2] == '/' && (start_name[len - 1] == '1' || start_name[len - 1] == '2')) {
        // @readpair/1 or @readpair/2  return @readpair
        len -= 2;
    }
    strncpy(name, start_name, len);
    name[len] = '\0';
    return 1;
}

static long count_read_pairs(GZIP_FILE f, char *fname)
{
    char name[MAX_READ_NAME_LEN], last[MAX_READ_NAME_LEN];
    char buf[MAX_BM_LINE_LEN];
    long max_pair_alns = 0, line = 0;

    name[0] = '\0';
    last[0] = '\0';
    while (GZIP_GETS(buf, MAX_BM_LINE_LEN - 1, f)) {
        if (buf[0] == '\n') {
            LOGF("Empty line at line %lld of %s, byte=%lld\n", (lld)line, fname, (lld)GZIP_FTELL(f));
            break;
        }
        line++;
        if (!get_base_name(name, buf)) {
            DIE("Could not find '%s' at line %lld of %s, buf='%.*s'\n", name, (lld)line, fname, 40, buf);
        }
        if (strcmp(name, last) != 0) {
            max_pair_alns++;
            strcpy(last, name);
        }
    }
    GZIP_REWIND(f);
    LOGF("For %s, found %lld lines and %lld max_pair_alns\n", fname, (lld) line, (lld) max_pair_alns);
    return max_pair_alns;
}

static int read_pair_alns(GZIP_FILE f, char *fname, tmp_pair_alns_t *pair_alns, char *buf, char *curr,
                          long *line, long *rejected, int reverse_complement,
                          int five_prime_wiggle, int three_prime_wiggle, long *excessive_alns)
{
    int readAligns = 0;

    if (GZIP_EOF(f)) {
        DBG("read_pair_alns: found EOF on %s\n", fname);
        //return 0;
    }
    // this happens on every new read (after first invocation)
    if (curr[0]) { // the last record which should be a new read
        if (buf[0] == '\0') {
            //LOGF("read_pair_alns(): no more reads\n");
            return 0;
        }
        if (strcmp(buf, curr) != 0) {
            WARN("Buffer changed from '%s' to '%s'!\n", curr, buf);
            //strcpy(buf, curr);
        }
    }
    // this happens on the first invocation only
    if (!buf[0]) {
        assert(curr[0] == '\0');
        (*line)++;
        if (!GZIP_GETS(buf, MAX_BM_LINE_LEN - 1, f)) {
            DBG("read_pair_alns did not read anything\n");
            return 0;
        }
        DBG2("read_pair_alns read first line of %s buf=%.*s\n", fname, 40, buf);
    }
    curr[0] = '\0'; // first read
    char name[MAX_READ_NAME_LEN] = "", curr_name[MAX_READ_NAME_LEN] = "";
    do {
        CHECK_LINE_LEN(buf, MAX_BM_LINE_LEN, fname);
        DBG2("read_pair_alns line %lld of %s name=%s curr_name=%s, buf=%.*s curr=%.*s\n", (lld) * line, fname, name, curr_name, 40, buf, 40, curr);
        assert(buf[0] == 'M'); assert(buf[10] == '-'); assert(buf[12] == '\t'); // "MERACULOUS-?\t@...\t"
        if (!get_base_name(name, buf)) {
            DIE("Could not find name '%s' at line %lld in %s. buf=%.*s, curr_name=%s\n", name, (lld) * line, fname, 40, buf, curr_name);
        }
        DBG2("read_pair_alns: found %s from %s\n", name, buf);
        if (curr[0]) {
            if (!get_base_name(curr_name, curr)) {
                DIE("Could not find name '%s' in curr=%.*s line %lld of %s\n", curr_name, 40, curr, (lld) * line, fname);
            }
            if (strcmp(curr_name, name) == 0) {
                // accumulate alns
                DBG2("read_pair_alns: same alignment: nalns=%lld %s\n", (lld)pair_alns->nalns, curr_name);
                if (pair_alns->nalns == MAX_PAIR_ALNS) {
                    (*excessive_alns)++;
                } else {
                    get_pair_aln(pair_alns, buf, rejected, reverse_complement,
                                 five_prime_wiggle, three_prime_wiggle);
                }
            } else {
                // switching to new read
                strcpy(curr, buf);
                return 1;
            }
        } else {
            strcpy(curr, buf);
            if (pair_alns->nalns == MAX_PAIR_ALNS) {
                (*excessive_alns)++;
            } else {
                get_pair_aln(pair_alns, buf, rejected, reverse_complement,
                             five_prime_wiggle, three_prime_wiggle);
            }
        }
        DBG2("read_pair_alns line %lld of %s got name %s\n", (lld) * line, fname, name);
        (*line)++;
    } while (GZIP_GETS(buf, MAX_BM_LINE_LEN - 1, f));
    //LOGF("EOF of %s\n", fname);
    assert(curr[0]);
    buf[0] = '\0'; // if nothing new was read, reset the buffer
    return 1;
}


static int set_pair_alns(pair_alns_t *pa, tmp_pair_alns_t *tpa, pair_alns_array_t *my_pair_alns_array)
{
    assert(pa->nalns == 0 && pa->alns == NULL);   // i.e. it is not set yet
    memcpy(pa->name, tpa->name, MAX_READ_NAME_LEN);
    pa->nalns = tpa->nalns;
    if (pa->nalns > 0) {
        int64_t size = tpa->nalns * sizeof(alignment_t);
        assert(upc_threadof(my_pair_alns_array->alnsHeap) == MYTHREAD);
        //pa->alns = (shared[] alignment_t*) allocFromHeapList( my_pair_alns_array->alnsHeap, size );
        SharedCharPtr tmp = allocFromHeapList(my_pair_alns_array->alnsHeap, size);
        pa->alns = (shared[] alignment_t *)tmp;
        assert(upc_threadof(pa->alns) == MYTHREAD);
        memcpy((alignment_t *)pa->alns, tpa->alns, size);
    } else {
        pa->alns = NULL;
    }
    DBG2("set_pair_alns(): nalns=%lld name=%s\n", (lld)pa->nalns, pa->name);
    return pa->nalns;
}

static void _fix_stolen_alns(pair_alns_t *pair_alns_block, int contiguous_to_fix, shared [] alignment_t *start, shared [] alignment_t *end, SharedHeapListPtr *stolenAlnsHeap)
{
    // copy and fix from start to i-1
    int64_t numAlns = (end - start);

    shared [] alignment_t * aln = (shared [] alignment_t *)_allocFromHeapList(stolenAlnsHeap, sizeof(alignment_t) * numAlns);
    int64_t alnIdx = 0;
    assert(upc_threadof(aln) == MYTHREAD);
    upc_memget((alignment_t *)aln, start, numAlns * sizeof(alignment_t));
    for (int64_t j = 0; j < contiguous_to_fix; j++) {
        if (pair_alns_block[j].nalns > 0) {
#ifdef DEBUG
            shared [] alignment_t * tmp = pair_alns_block[j].alns;
            alignment_t test = tmp[0];
            assert(upc_threadof(aln + alnIdx) == MYTHREAD);
            assert(memcmp(&test, (alignment_t *)(aln + alnIdx), sizeof(alignment_t)) == 0);
#endif
            pair_alns_block[j].alns = aln + alnIdx;
            alnIdx += pair_alns_block[j].nalns;
        }
    }
    assert(alnIdx == numAlns);
}

// adjusts contiguous sets of pair_alns_t alns fields to be local shared memory
static void fix_stolen_alns(pair_alns_t *pair_alns_block, int num_pairs_in_block, SharedHeapListPtr *stolenAlnsHeap)
{
    assert(stolenAlnsHeap != NULL);
    assert(upc_threadof(*stolenAlnsHeap) == MYTHREAD);
    shared [] alignment_t * start = NULL, *end = NULL;
    int stolenFrom = MYTHREAD;
    int startIdx = 0, i;
    for (i = 0; i < num_pairs_in_block; i++) {
        if (pair_alns_block[i].nalns > 0) {
            shared [] alignment_t * aln = pair_alns_block[i].alns;
            if (upc_threadof(aln) == MYTHREAD) {
                if (startIdx < i && start != NULL) {
                    assert(start != NULL && end != NULL);
                    _fix_stolen_alns(pair_alns_block + startIdx, i - startIdx, start, end, stolenAlnsHeap);
                }
                start = NULL; end = NULL; startIdx = i + 1; // this one does not need to be copied
            } else {
                stolenFrom = upc_threadof(aln);
                if (start == NULL || end != pair_alns_block[i].alns) {
                    if (startIdx < i && start != NULL) {
                        assert(start != NULL && end != NULL);
                        _fix_stolen_alns(pair_alns_block + startIdx, i - startIdx, start, end, stolenAlnsHeap);
                    }
                    start = pair_alns_block[i].alns;
                    end = start + pair_alns_block[i].nalns;
                    startIdx = i;
                } else {
                    // more in this contiguous aligment_t block
                    assert(end != NULL && end == pair_alns_block[i].alns);
                    end += pair_alns_block[i].nalns;
                }
            }
        }
    }
    if (startIdx < i && start != NULL) {
        assert(start != NULL && end != NULL);
        _fix_stolen_alns(pair_alns_block + startIdx, i - startIdx, start, end, stolenAlnsHeap);
    }
}

// Reads in alignments. Each line consists of:
// BLAST_TYPE  QUERY Q_START Q_STOP Q_LENGTH SUBJECT S_START S_STOP S_LENGTH STRAND SCORE E_VALUE IDENTITIES ALIGN_LENGTH
// The subject is the contig id.
// Expects the file to be split, one per thread
void gc_process_meraligner_file(char *library_name, int reverse_complement, int insert_size,
                                int insert_sigma, int five_prime_wiggle, int three_prime_wiggle, read_list_t *read_list)
{
    int stealBlock = MAX_STEAL_BLOCK;

#ifdef STEAL_PLACEMENTS
    // pick a common stealBlock size that is about 10% of the work
    serial_printf("Using work stealing to balance placements with block size %d for total gaps %lld\n",
                  stealBlock, (lld)_max_ngaps);
#endif
    double startTime = START_TIMER(T_PROCESS_MERALIGNER);
    START_TIMER(T_READ_MERALIGNER);
    long *rejected = calloc_chk(NUM_REJECT_REASONS, sizeof(long));
    long num_aligned_gap_reads = 0;
    long num_projected_gap_reads = 0;
    long nalns = 0;
    // expect two files per library per thread, with endings alternating Read1 and Read2
    char fname[2][2 * MAX_FILE_PATH];
    GZIP_FILE f[2] = { NULL, NULL };
    for (int i = 0; i < 2; i++) {
        sprintf(fname[i], "%s-%s_Read%d" GZIP_EXT, library_name, _cfg->mer_file, i + 1);
        if (i == 0 || doesCheckpointExist(fname[i])) { // Read1 or has a Read2
            f[i] = openCheckpoint(fname[i], "r");
        } else {
            // unpaired reads can still be used too but will not have a Read1 file
            f[i] = NULL;
        }
    }
    if (!f[1]) {
        tprintf("Could not open second file %s... using unpaired reads\n", fname[1]);
    }

    serial_printf("Processing %s/%s-%s_*_Read*... ", _cfg->base_dir, library_name, _cfg->mer_file);
    tprintf_flush("Processing %s (+1)...\n", fname[0]);

    long line0 = 0, line1 = 0;
    char buf0[MAX_BM_LINE_LEN] = "", buf1[MAX_BM_LINE_LEN] = "";
    char curr0[MAX_BM_LINE_LEN] = "", curr1[MAX_BM_LINE_LEN] = "";

    // count lines in file to determine maximum alignment blocks needed
    long max_pair_alns = count_read_pairs(f[0], fname[0]) + 1; // plus one to ensure end of file is the end of the alignments
    buf0[0] = 0; buf1[0] = 0; curr0[0] = 0; curr1[0] = 0;
    LOGF("Allocating %lld max_pair_alns alignments %0.3f s elapsed\n", (lld)max_pair_alns, now() - startTime);

    shared pair_alns_array_t *pair_alns_array = NULL;
    UPC_ALL_ALLOC_CHK(pair_alns_array, THREADS, sizeof(pair_alns_array_t));
    int cpn = (_sv != NULL) ? MYSV.cores_per_node : 1;
#ifdef STEAL_PLACEMENTS
    GlobalFinished finished_threads = initGlobalFinished(cpn);
#endif
    UPC_TIMED_BARRIER;
    UPC_ALLOC_CHK(pair_alns_array[MYTHREAD].pairs, sizeof(pair_alns_t) * max_pair_alns);
    pair_alns_array_t *my_pair_alns_array = (pair_alns_array_t *)&(pair_alns_array[MYTHREAD]);
    memset((pair_alns_t *)my_pair_alns_array->pairs, 0, sizeof(pair_alns_t) * max_pair_alns);
    my_pair_alns_array->npairs = 0;
    my_pair_alns_array->next_pair = 0;
    initHeapList(my_pair_alns_array->alnsHeap, 128000 / sizeof(alignment_t));

    long npairs;
    int max_nalns = 0;
    long excessive_alns = 0;
    long num_alns = 0;
    double readPairTime = now();
    for (npairs = 0; npairs < max_pair_alns; npairs++) {
        pair_alns_t *real_pair_alns = (pair_alns_t *)&(my_pair_alns_array->pairs[npairs]);
        tmp_pair_alns_t tmp_pair_alns;
        tmp_pair_alns.nalns = 0;
        tmp_pair_alns.name[0] = 0;
        if (!read_pair_alns(f[0], fname[0], &tmp_pair_alns, buf0, curr0, &line0, rejected,
                            reverse_complement, five_prime_wiggle, three_prime_wiggle,
                            &excessive_alns)) {
            break;
        }
        DBG2("read from read1 tmp_pair_alns.name=%s nalns=%lld\n", tmp_pair_alns.name, (lld)tmp_pair_alns.nalns);
        if (f[1] && !read_pair_alns(f[1], fname[1], &tmp_pair_alns, buf1, curr1, &line1,
                                    rejected, reverse_complement, five_prime_wiggle,
                                    three_prime_wiggle, &excessive_alns)) {
            DIE("EOF before finding matching pair for %s\n", tmp_pair_alns.name);
        }
        if (max_nalns < tmp_pair_alns.nalns) {
            max_nalns = tmp_pair_alns.nalns;
        }
        num_alns += set_pair_alns(real_pair_alns, &tmp_pair_alns, my_pair_alns_array);
    }
    if (npairs != max_pair_alns - 1) {
        WARN("Did not read the entire set of paired alignments.  Expected %lld, read %lld\n",
             (lld)max_pair_alns - 1, (lld)npairs);
    }
    assert(npairs < max_pair_alns); // make sure we got to the end of the file!
    tprintf_flush("... done reading %ld aln pairs (max %ld ) in %0.3f s\n", npairs, max_pair_alns, now() - readPairTime);
    tprintf_flush("Maximum alignments per pair: %d\n", max_nalns);
    closeCheckpoint(f[0]);
    if (f[1] != NULL) {
        closeCheckpoint(f[1]);
    }
    stop_timer(T_READ_MERALIGNER);

    pair_alns_array[MYTHREAD].npairs = npairs;
    tprintf_flush("... done reading %lld aln pairs (max %lld )\n", (lld)npairs, (lld)max_pair_alns);
    tprintf_flush("Maximum alignments per pair: %d\n", max_nalns);
    double mb_for_pairs = (double)(sizeof(pair_alns_t) * max_pair_alns) / ONE_MB;
    double mb_for_alns = (double)(sizeof(alignment_t) * num_alns) / ONE_MB;
    tprintf_flush("Allocated %.3f MB for %lld pairs and %.3f MB for %lld alignments\n",
                  mb_for_pairs, (lld)max_pair_alns, mb_for_alns, (lld)num_alns);
    if (excessive_alns) {
        tprintf_flush("Pairs with alignments > %d: %lld\n", MAX_PAIR_ALNS, (lld)excessive_alns);
    }

    // we need a barrier here because we could steal from other threads
    UPC_TIMED_BARRIER;

    START_TIMER(T_STEAL_MERALIGNER);

    // The stealing strategy is to randomly pick a victim initially to steal from, and then
    // iterate from that victim onwards for subsequent steals. Also, for each steal, 20 victims
    // are checked, and the one with the most available to steal is chosen.
    // To make sure we terminate, each thread increments a counter to indicate that it has
    // finished all its local entries, so it knows when to break out of the loop

    unsigned rseed = MYTHREAD;
    srand(rseed);
    int stealing = 0;
    pair_alns_t *pair_alns;
    pair_alns_t pair_alns_block[MAX_STEAL_BLOCK];
    SharedHeapListPtr stolenAlnsHeap = NULL;
    int64_t next_pair;
    int64_t num_pairs_in_block, total_num_pairs_in_block = 0, numLoops = 0;
    int64_t num_steals = 0;
    int64_t tot_num_pairs = 0;
    int num_failed_steals = 0;
    double gapStartTime = now();
    double maxGapLoopTime = 0.0;
    double stealStartTime = 0.0;
    double totalGapLoopTime = 0.0;
    while (1) {
        double gapLoopStart = now();
        num_pairs_in_block = 0;
        if (!stealing) {
            UPC_ATOMIC_FADD_I64(&next_pair, &(pair_alns_array[MYTHREAD].next_pair), stealBlock);
            if (next_pair >= pair_alns_array[MYTHREAD].npairs) {
#ifdef STEAL_PLACEMENTS
                if (!stealing) {
                    incrementGlobalFinished(finished_threads);
                    //LOGF("Now starting to steal\n");
                }
                stealing = 1;
#else
                break;
#endif
            } else {
                num_pairs_in_block = MIN(pair_alns_array[MYTHREAD].npairs - next_pair, stealBlock);
                DBG2("Working on my placements: num_pairs_in_block = %lld, next_pair = %lld, npairs = %lld\n", (lld)num_pairs_in_block, (lld)next_pair, (lld)pair_alns_array[MYTHREAD].npairs);
            }
        }
#ifdef STEAL_PLACEMENTS
        if (stealing) {
            if (stealStartTime == 0.0) {
                stealStartTime = now();
            }
            // terminate stealing if we fail too often - this means either there's not enough work
            // to steal or contention is very high
            if (num_failed_steals > 3) {
                break;
            }
            if (isGlobalFinished(finished_threads)) {
                break;
            }
            num_pairs_in_block = 0;
            int victim = -1, max_remainder = 0;
            // find slowest target thread in random subset
            for (int i = 0; i < 10; i++) {
                // first steal from local threads on the same node, if there are any running
                int myNodeThreads = getThreadsRunningOnNode(finished_threads, MYTHREAD);
                int myNodeLeader = MYTHREAD - MYTHREAD % cpn;
                int new_victim = MYTHREAD;
                int randomTry = 0;
                while (randomTry++ < cpn) {
                    if (myNodeThreads > 0) {
                        new_victim = (rand_r(&rseed) % cpn) + myNodeLeader;
                        if (new_victim != MYTHREAD) {
                            break;
                        }
                    } else {
                        new_victim = rand_r(&rseed) % THREADS;
                        // do not try a thread from this node as they are already complete!
                        if (new_victim < myNodeLeader || new_victim >= myNodeLeader + cpn) {
                            break;
                        }
                    }
                }

//                new_victim = 0;

                if (new_victim != MYTHREAD && new_victim != victim) {
                    int64_t oldVal;
                    UPC_ATOMIC_READ_I64(&oldVal, &(pair_alns_array[new_victim].next_pair));
                    int remainder = pair_alns_array[new_victim].npairs - oldVal;
                    if (remainder > max_remainder) {
                        max_remainder = remainder;
                        victim = new_victim;
                    }
                }
            }
            if (victim != -1 && max_remainder >= stealBlock) {
                UPC_ATOMIC_FADD_I64(&next_pair, &(pair_alns_array[victim].next_pair), stealBlock);
                long victimNPairs = pair_alns_array[victim].npairs;
                if (next_pair < victimNPairs - stealBlock) {
                    num_pairs_in_block = MIN(victimNPairs - next_pair, stealBlock);
                    // copy the remote data to local
                    upc_memget(&pair_alns_block, &(pair_alns_array[victim].pairs[next_pair]),
                               sizeof(pair_alns_t) * num_pairs_in_block);
                    assert(stolenAlnsHeap == NULL);
                    initHeapList(stolenAlnsHeap, num_pairs_in_block * MAX_PAIR_ALNS * sizeof(alignment_t));
                    fix_stolen_alns(pair_alns_block, num_pairs_in_block, &stolenAlnsHeap);
                    num_steals += num_pairs_in_block;
                    WARN("Stole %lld from victim %d (next_pair = %lld, victimNPairs = %lld) in %0.3f s\n",
                         (lld)num_pairs_in_block, victim, (lld)next_pair, (lld)victimNPairs,
                         now() - gapLoopStart);
                } else {
                    num_failed_steals++;
                    //LOGF("No more work to steal from victim %d next_pair %lld >= victimNPairs %lld in %0.3f s. num_failed_steal = %d\n", victim, (lld) next_pair, (lld) victimNPairs, now() - gapLoopStart, num_failed_steals);
                }
            } else {
                num_failed_steals++;
                //LOGF("Failed to find a victim.  num_failed_steals = %d\n", num_failed_steals);
            }
        }
#endif
        tot_num_pairs += num_pairs_in_block;
        for (int64_t pi = 0; pi < num_pairs_in_block; pi++) {
            if (!stealing) {
                pair_alns = (pair_alns_t *)&(pair_alns_array[MYTHREAD].pairs[pi + next_pair]);
            } else {
                pair_alns = &(pair_alns_block[pi]);
            }
            if (pair_alns->nalns) {
                num_aligned_gap_reads += direct_alignments(pair_alns, read_list, reverse_complement);
                if (_cfg->pair_projection && f[1] > 0) {
                    num_projected_gap_reads += projected_alignments(pair_alns, read_list,
                                                                    insert_size, insert_sigma, reverse_complement);
                }
                nalns += pair_alns->nalns;
            }
        }
        if (stolenAlnsHeap) {
            freeHeapList(stolenAlnsHeap);
        }
        double gapLoopTime = now() - gapLoopStart;
        if (maxGapLoopTime < gapLoopTime) {
            maxGapLoopTime = gapLoopTime;
        }
        numLoops++;
        total_num_pairs_in_block += num_pairs_in_block;
        totalGapLoopTime += gapLoopTime;
        DBG2("Gap placement loop processed %lld pairs in %0.3f s\n", (lld)num_pairs_in_block, gapLoopTime);
    }
    DBG("Gap placement loop processed %d loops %lld pairs in %0.3f s\n", numLoops, (lld)total_num_pairs_in_block, totalGapLoopTime);
    double gapEndTime = now();
    if (stealStartTime == 0.0) {
        stealStartTime = gapEndTime;
    }
    stop_timer(T_STEAL_MERALIGNER);
    LOGF("Gap placements took %0.3f s total, %0.3f stealing.  Max placement loop %0.3f s\n", gapEndTime - gapStartTime, gapEndTime - stealStartTime, maxGapLoopTime);
    UPC_TIMED_BARRIER;

    // free pairs and alignments
    freeHeapList(my_pair_alns_array->alnsHeap);
    UPC_FREE_CHK(my_pair_alns_array->pairs);

    UPC_TIMED_BARRIER;
    UPC_ALL_FREE_CHK(pair_alns_array);
#ifdef STEAL_PLACEMENTS
    freeGlobalFinished(&finished_threads);
#endif

    long tot_unused = 0;
    for (int i = 0; i < NUM_REJECT_REASONS; i++) {
        tot_unused += rejected[i];
    }
    tprintf_flush("Found %lld alignments found, used %.3f fraction\n",
                  (lld)nalns, (double)nalns / (nalns + tot_unused));
    tprintf_flush("Number of steals: %lld (%.1f %%)\n",
                  (lld)num_steals, (100.0 * num_steals / tot_num_pairs));
    tprintf_flush("Unused alignments:\n");
    for (int i = 0; i < NUM_REJECT_REASONS; i++) {
        if (rejected[i]) {
            tprintf_flush("\t%s\t%ld\n", reject_reasons[i], rejected[i]);
        }
    }
    tprintf_flush("Total reads placed in gaps = %ld (aligned) + %ld (projected)\n",
                  num_aligned_gap_reads, num_projected_gap_reads);
    free_chk(rejected);
    tprintf_flush("Direct placement took %.3f s, projected placement took %.3f s\n",
                  get_elapsed_time(T_DIRECT_ALNS), get_elapsed_time(T_PROJECTED_ALNS));

    // Count total reads in gaps
    int64_t numReads = 0, maxReads = 0, cappedGaps = 0;
    gap_t *myGaps = (gap_t*) _gaps[MYTHREAD].gaps;
    for(int64_t i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        numReads += myGaps[i].nreads;
        if (myGaps[i].max_reads > MAX_READS_PER_GAP) {
            cappedGaps++;
            maxReads += MAX_READS_PER_GAP;
        } else {
            maxReads += myGaps[i].max_reads;
        }
    }
    int64_t largestMaxReads = reduce_long(maxReads, UPC_MAX, ALL_DEST);
    int64_t totalMaxReads = reduce_long(maxReads, UPC_ADD, ALL_DEST);
    double endTime = stop_timer(T_PROCESS_MERALIGNER);
    tprintf_flush("Reading meraligner took %.3f s\n", get_elapsed_time(T_PROCESS_MERALIGNER));
    serial_printf("done in %0.3f s\n", endTime - startTime);    
    serial_printf("Counted numReads=%lld maxReads=%lld for my ngaps=%lld (%lld capped), so %0.3f MB will be allocated. largest=%lld total=%lld imbalance=%0.2f.\n", (lld) numReads, (lld) maxReads, (lld) _gaps[MYTHREAD].ngaps, (lld) cappedGaps, maxReads * PER_MB * (sizeof(int) + 2 * (_cfg->max_read_len + 1)), (lld) largestMaxReads, (lld) totalMaxReads, 1.0 * largestMaxReads * THREADS / totalMaxReads);
}

void place_reads_in_dhtable(read_list_t *read_list)
{

    balance_read_allocations();

    // now put reads in shared hash table
    long tot_reads = reduce_long(read_list->num, UPC_ADD, ALL_DEST);
    long max_reads = reduce_long(read_list->num, UPC_MAX, ALL_DEST);

    // potential candidate for work stealing here
    serial_printf("For read placement myReads=%lld max_reads=%lld tot_reads=%lld - imbalance=%0.2f\n", (lld) read_list->num, (lld) max_reads, (lld) tot_reads, 1.0 * THREADS * max_reads / tot_reads);
 
    tprintf_flush("Found %lld reads, total %lld\n", (lld)read_list->num, (lld)tot_reads);
    serial_printf("Placing %lld reads...", (lld) tot_reads);

    dhtable_init(tot_reads, READ_HASH_TABLE_LOAD_FACTOR);

    START_TIMER(T_PUT_READ_ORIENT);
    tprintf_flush("Number of reads in list %ld and number of reads allocated %ld (reallocs %d)\n",
                  read_list->num, read_list->size, _num_reallocs);
    long num_put_failures = 0;
    long num_puts = 0;
    char name_buf[MAX_READ_NAME_LEN + 2];
    for (long i = 0; i < read_list->num; i++) {
        num_puts++;
        read_orient_t *read_orient = &(read_list->reads[i]);
        // if this is an outtie library, we need to flip back the orientation. We had to
        // flip it before in get_pair_aln in order to ensure the projected mate read
        // is projected in the correct direction, but now we need to flip it back to make
        // sure the read is aligned correctly
        if (read_orient->lib_is_rc) {
            read_orient->orient = (read_orient->orient == '+' ? '-' : '+');
        }
        sprintf(name_buf, "%c%s", read_orient->orient, read_orient->name);
        if (!dhtable_put(name_buf, read_orient->gap_id)) {
            num_put_failures++;
        }
    }

    long tot_num_put_failures = reduce_long(num_put_failures, UPC_ADD, ALL_DEST);
    if (tot_num_put_failures) {
        long tot_num_puts = reduce_long(num_puts, UPC_ADD, SINGLE_DEST);
        double perc_fails = (100.0 * tot_num_put_failures) / tot_num_puts;
        if (!MYTHREAD && perc_fails >= 5) {
             serial_printf("\n");
             WARN("Distributed hash table had %lld put failures out of %lld puts (%.2f %%).\n"
                  "[Indicates potential loss of efficiency and possibly fewer gaps closed]\n"
                  "Depth of dhtable is %lld\n",
                  (lld)tot_num_put_failures, (lld)tot_num_puts, perc_fails, (lld)dhtable_depth());
        }
        //check_read_data_hash();
    }
    free_chk(read_list->reads);

    stop_timer(T_PUT_READ_ORIENT);
#ifdef CHECK_READ_DATA_HASH
    dhtable_check();
#endif

    print_memory_used();
    serial_printf("done in %.3f s\n", get_elapsed_time(T_PUT_READ_ORIENT));
    UPC_TIMED_BARRIER;
}

char *get_primer(shared contig_t *_contig, int contig_ext, int reverse_flag, int len)
{
    double start_timer2, end_timer;

    CACHE_LOCAL_RO(contig_t, contig, _contig);

    if (contig->seq_len < len) {
        DIE("Contig is smaller than mer size: %d < %d\n", contig->seq_len, len);
    }
    char *primer = malloc_chk(len + 1);
    if (contig_ext == 5) {
        upc_memget(primer, contig->seq, len);
        primer[len] = 0;
    } else {
        char *seq = malloc_chk0(contig->seq_len + 1);
        UPC_MEMGET_STR(seq, contig->seq, contig->seq_len + 1);
        int offset = contig->seq_len - len;
        strcpy(primer, seq + offset);
        primer[len] = 0;
        free_chk0(seq);
    }
    start_timer2 = UPC_TICKS_NOW();
    if (contig_ext == reverse_flag) {
        switch_code(reverse(primer));
    }
    end_timer = UPC_TICKS_NOW();
    rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));
    return primer;
}

void print_gaps(void)
{
    serial_printf("Printing gaps... ");
    START_TIMER(T_PRINT_GAPS);
    char fname[MAX_FILE_PATH * 2];
    sprintf(fname, "gaps");
    FILE *f = openCheckpoint0(fname, "w+");
    int64_t max_nreads = 0;
    int64_t tot_nreads = 0;
    for (int64_t gapIdx = 0; gapIdx < _gaps[MYTHREAD].ngaps; gapIdx++) {
        gap_t *_gap = (gap_t *)&(_gaps[MYTHREAD].gaps[gapIdx]);
        CACHE_LOCAL_TMP(gap_t, gap, _gap);
        int gotCopy = localize_gap_reads(gap);
        tot_nreads += gap->nreads;
        if (gap->nreads > max_nreads) {
            max_nreads = gap->nreads;
        }
        shared contig_t *contig1;
        get_contig_chk(contig1, gap->contig1_id);
        shared contig_t *contig2;
        get_contig_chk(contig2, gap->contig2_id);
        char *primer1 = get_primer(contig1, gap->contig_ext1, 5, _cfg->mer_size);
        if (!primer1) {
            DIE("Could not find primer1 for gap %lld\n", (lld)gapIdx);
        }
        char *primer2 = get_primer(contig2, gap->contig_ext2, 3, _cfg->mer_size);
        if (!primer2) {
            DIE("Could not find primer2 for gap %lld\n", (lld)gapIdx);
        }

        char buf[1000];
        sprintf(buf, "Scaffold%lld\tContig%lld.%d\t%s\tContig%lld.%d\t%s\t%d\t%.0f\t%lld\t",
                (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1,
                primer1, (lld)gap->contig2_id, gap->contig_ext2,
                primer2, gap->size, gap->uncertainty, (lld)gap->nreads);
        for (int64_t j = 0; j < gap->nreads; j++) {
            fprintf(f, "%s %s:%s\n", buf, (char *)GET_READ(gap->reads_nts, gapIdx),
                    (char *)GET_READ(gap->reads_quals, j));
        }
        free_chk(primer1);
        free_chk(primer2);
        if (gotCopy) free_gap_reads(gap);
    }
    closeCheckpoint0(f);
    stop_timer(T_PRINT_GAPS);
    serial_printf("done in %.3f s\n", get_elapsed_time(T_PRINT_GAPS));
}

static int get_reads_for_orient(char orient, char *read_name, int64_t *gap_ids, char *curr_read_nts,
                                char *curr_read_quals)
{
    char buf[MAX_READ_NAME_LEN + 2];
    double start_timer2, end_timer;

    sprintf(buf, "%c%s", orient, read_name);
    // the dhtable can have multiple enties per read, so we get them all in an array of gap ids
    int64_t ngap_ids = dhtable_get(buf, gap_ids, MAX_READ_GAP_IDS);
    if (!ngap_ids) {
        return 0;
    }
    if (orient == '-') {
        start_timer2 = UPC_TICKS_NOW();
        switch_code(reverse(curr_read_nts));
        end_timer = UPC_TICKS_NOW();
        rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));

        reverse(curr_read_quals);
    }
    int curr_read_len = strlen(curr_read_nts);
    START_TIMER(T_GAP_COPY);
    shared [] gap_t * gap = NULL;
    size_t hash = 0;
    for (int64_t i = 0; i < ngap_ids; i++) {
        int64_t gi = gap_ids[i];
        gap = get_gap(gi, __LINE__, NULL, ABORT_ON_FAIL);
        CACHE_LOCAL_RO(gap_t, ro_gap, gap); // gap = get_gap(gi, __LINE__, NULL, ABORT_ON_FAIL);
        int64_t max_reads = ro_gap->max_reads;
        if (max_reads > MAX_READS_PER_GAP) {
            // if max_reads > MAX, include at a chance based on hash of name, but check later to not exceed MAX_READS_PER_GAP
            if (hash == 0) {
                hash = htable_hash(read_name);
            }
            if (hash % (max_reads * 120 / 100) >= MAX_READS_PER_GAP) { // 20% extra chance of a miss
                // skip
                DBG("Skipping excessive read for gap %lld with %lld of %lld assigned - %s\n", (lld) gi, (lld) gap->nreads, (lld) MAX_READS_PER_GAP, read_name);
                continue;
            }
        }
        if (!max_reads) {
            continue;
        }
        if (ro_gap->cp_contigs) {
            continue;
        }
        int64_t ri;
        UPC_ATOMIC_FADD_I64(&ri, &(gap->nreads), 1);
        if (ri >= max_reads || ri >= MAX_READS_PER_GAP) {
            if (max_reads <= MAX_READS_PER_GAP) {
                char buf[256];
                WARN("max reads exceeded for gap %lld, %s, max is %lld\n"
                     "Duplicate reads in FASTQ? Latest read:\n%s\n",
                     (lld)gi, get_gapi_info(gi, buf), (lld)max_reads, read_name);
            }
            // take it back and skip
            UPC_ATOMIC_FADD_I64(&ri, &(gap->nreads), -1);
            continue;
        }
        if (!ro_gap->reads_lens || !ro_gap->reads_nts || !ro_gap->reads_quals) {
            DIE("gap->reads is NULL for gap %lld\n", (lld)gi);
        }
        ro_gap->reads_lens[ri] = curr_read_len;
        upc_memput(GET_READ(ro_gap->reads_nts, ri), curr_read_nts, curr_read_len + 1);
        upc_memput(GET_READ(ro_gap->reads_quals, ri), curr_read_quals, curr_read_len + 1);
    }
    stop_timer(T_GAP_COPY);
    return 1;
}

// returns the number of reads
int64_t gc_process_fastq_file(char *library_name, uint8_t libnum)
{
    START_TIMER(T_PROCESS_FASTQ);
    // Just verify that reads have been allocated
    // the allocation could be located on a different thread now
    int num_no_read_gaps = 0;
    for (int64_t i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        gap_t *gap = (gap_t *)&(_gaps[MYTHREAD].gaps[i]);

        if (gap->cp_contigs) {
            continue;
        }

        int64_t max_reads = gap->max_reads;
        int64_t effective_max_reads = max_reads;
        if (max_reads > MAX_READS_PER_GAP) {
            effective_max_reads = MAX_READS_PER_GAP;
        }
        if (max_reads == 0) {
            num_no_read_gaps++;
            continue;
        }
        if (max_reads < 0) {
            DIE("negative max_reads %lld\n", (lld)max_reads);
        }

        if (gap->reads_lens == NULL || gap->reads_nts == NULL || gap->reads_quals == NULL) {
            DIE("gap (i=%lld of %lld) has %lld max_reads but has not yet been allocated space!\n", (lld) i, (lld)  _gaps[MYTHREAD].ngaps, max_reads);
        }
         
    }
    UPC_TIMED_BARRIER;
    if (num_no_read_gaps) {
        tprintf("Number of gaps without reads (found in srf but not meraligner): %d (%.2f fraction)\n",
                num_no_read_gaps, (double)num_no_read_gaps / _gaps[MYTHREAD].ngaps);
    }

    long num_reads = 0, num_reads_used = 0;
    int64_t gap_ids[MAX_READ_GAP_IDS];
    char *read_name = NULL, *curr_read_nts = NULL, *curr_read_quals = NULL;
    Buffer idBuf = initBuffer(MAX_READ_NAME_LEN), seqBuf = initBuffer(DEFAULT_READ_LEN),
           qualBuf = initBuffer(DEFAULT_READ_LEN);
    int curr_read_len;

    // Get FileOfFileNames by library
    char fofn[MAX_FILE_PATH], fname[MAX_FILE_PATH];
    snprintf(fofn, MAX_FILE_PATH, "%s.fofn", library_name);
    serial_printf("Processing fastq files from %s...\n", fofn);
    tprintf_flush("Processing fastq files from %s...\n", fofn);
    Buffer fofnBuffer = broadcast_file(fofn);
    uint64_t read1ID = MYTHREAD, read2ID = MYTHREAD;
    while (getsBuffer(fofnBuffer, fname, MAX_FILE_PATH)) {
        fname[strlen(fname) - 1] = '\0';

        fq_reader_t fqr = create_fq_reader();
        LOGF("Openting fastq cached_io_rads=%d\n", _cfg->cached_io_reads);
        int isCachedIO = _cfg->cached_io_reads;
        if (isCachedIO) {
            if (!doesLocalCheckpointExist(fname)) {
                restoreCheckpoint(fname);
                char uncomp[MAX_FILE_PATH+30];
                sprintf(uncomp, "%s.uncompressedSize", fname);
                restoreCheckpoint(uncomp);
            }
        }
        open_fq(fqr, fname, isCachedIO, _cfg->base_dir, isCachedIO ? -1 : broadcast_file_size(fname));

        while (get_next_fq_record(fqr, idBuf, seqBuf, qualBuf)) {
            hexifyId(getStartBuffer(idBuf), libnum, &read1ID, &read2ID, THREADS);
            if (getLengthBuffer(seqBuf) > _cfg->max_read_len) {
                DIE("Line length in FASTQ file is higher than maximum from -l parameter: %lld > %d\n",
                    (lld)getLengthBuffer(seqBuf), _cfg->max_read_len);
            }
            read_name = getStartBuffer(idBuf);
            curr_read_nts = getStartBuffer(seqBuf);
            curr_read_quals = getStartBuffer(qualBuf);
            num_reads++;

#ifdef DBG_FASTQ
            if (read_name[strlen(read_name) - 1] == '2') {
                dbg("%s\t", read_name);
                dbg("%s\t", curr_read_nts);
                dbg("+%s\t", read_name + 1);
                //dbg("+\n");
                dbg("%s\n", curr_read_quals);
            }
#endif
            num_reads++;
            // try both orientations
            int use_read = 0;
            if (get_reads_for_orient('+', read_name, gap_ids, curr_read_nts, curr_read_quals)) {
                use_read = 1;
            }
            if (get_reads_for_orient('-', read_name, gap_ids, curr_read_nts, curr_read_quals)) {
                use_read = 1;
            }
            num_reads_used += use_read;
        } // reading fastq
        destroy_fq_reader(fqr);
    }     // reading fofn

    freeBuffer(fofnBuffer);


    freeBuffer(seqBuf);
    freeBuffer(qualBuf);

    tprintf_flush("In FASTQ: %ld reads found, used %.3f fraction, in %.3f s\n",
                  num_reads, (double)num_reads_used / num_reads, get_elapsed_time(T_PROCESS_FASTQ));
    stop_timer(T_PROCESS_FASTQ);
    print_memory_used();
    UPC_TIMED_BARRIER;
    serial_printf("Done with FASTQ files in %s in %.3f s\n", library_name, get_elapsed_time(T_PROCESS_FASTQ));
    return num_reads;
}

void finish_fastq_files(int64_t num_reads) {

    UPC_TIMED_BARRIER;

    dhtable_free();

    long num_missing_reads = 0;
    for (int64_t i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        gap_t *_gap = (gap_t *)&(_gaps[MYTHREAD].gaps[i]);
        CACHE_LOCAL_TMP(gap_t, gap, _gap);
        if (gap->max_reads < gap->nreads) {
            gap->nreads = gap->max_reads;
        }
        num_missing_reads += ((gap->max_reads > MAX_READS_PER_GAP ? MAX_READS_PER_GAP : gap->max_reads) - gap->nreads);
#ifdef DBG_DREAD_AFTER
        int gotCopy = localize_gap_reads(gap);
        char buf[200];
        for (int64_t j = 0; j < gap->nreads; j++) {
            dbg("DREADAFTER %s %s\n", get_gapi_info(gap->id, buf), (char *)gap->reads_nts[j]);
        }
        if (gotCopy) free_gap_reads(gap);
#endif
    }
    if (num_missing_reads) {
        double perc_missing = (100.0 * num_missing_reads) / num_reads;
        if (perc_missing > 5) {
            tprintf_flush("WARNING: found %lld (%.2f %%) reads in meraligner files that are either "
                          "not in FASTQ files or not in hash table\n",
                          (lld)num_missing_reads, perc_missing);
        }
    }

}

int sort_gaps_by_max_reads(const void *a, const void *b) {
    gap_t *ga = (gap_t*) a, *gb = (gap_t*) b;
    return (ga->max_reads < gb->max_reads);
}

int sort_gaps_by_sort_idx(const void *a, const void *b) {
    gap_t *ga = (gap_t*) a, *gb = (gap_t*) b;
    return (ga->sortIdx > gb->sortIdx);
}


void sort_gaps_by_difficulty()
{

    // now sort my gaps by max_reads as the gapi index can now be rearranged with all the reads saved to each gap
    double startSort = now();
    for(int64_t i = 0; i < _gaps[MYTHREAD].ngaps; i++) {
        gap_t *gap = (gap_t*) &_gaps[MYTHREAD].gaps[i];
        gap->sortIdx = i;
    }
    qsort( (gap_t*) _gaps[MYTHREAD].gaps, _gaps[MYTHREAD].ngaps, sizeof(gap_t), sort_gaps_by_max_reads);
    double endSort = now();
    LOGF("sorted %lld gaps by max_read in %0.3f s\n", (lld) _gaps[MYTHREAD].ngaps, endSort - startSort);

    UPC_TIMED_BARRIER;
}

void unsort_gaps()
{
    UPC_TIMED_BARRIER;
    double startSort = now();
    qsort( (gap_t*) _gaps[MYTHREAD].gaps, _gaps[MYTHREAD].ngaps, sizeof(gap_t), sort_gaps_by_sort_idx);
    double endSort = now();
    LOGF("unsorted %lld gaps by max_read in %0.3f s\n", (lld) _gaps[MYTHREAD].ngaps, endSort - startSort);
    UPC_TIMED_BARRIER;
}

static void correct_contig_gapi(int64_t contig_id, int64_t old_gi, int64_t new_gi)
{
    shared contig_t *contig;

    get_contig_chk(contig, contig_id);
    if (!contig) {
        DIE("Missing contig %lld for gap %lld\n", (lld)contig_id, (lld)old_gi);
    }
    if (contig->scaff_contig->gapi_5 == old_gi) {
        contig->scaff_contig->gapi_5 = new_gi;
    } else if (contig->scaff_contig->gapi_3 == old_gi) {
        contig->scaff_contig->gapi_3 = new_gi;
    }
    //else
    //    WARN("Could not find gap id %lld (to replace with %lld) for contig %lld,"
    //        " gapi_5 %lld gapi_3 %lld\n",
    //        (lld) old_gi, (lld) new_gi, (lld) contig_id,
    //        (lld) contig->scaff_contig->gapi_5, (lld) contig->scaff_contig->gapi_3);
}

