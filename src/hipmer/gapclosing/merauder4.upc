/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 *
 * Based on merauder.pl by Jarrod Chapman <jchapman@lbl.gov> Tue Jun 2
 * 07:48:59 PDT 2009 Copyright 2009 Jarrod Chapman. All rights reserved.
 */

/*
 * Three files are read in:
 * 1. The gap data, where all of the info is stored in an array of gap structs.
 * 2. The contigs from a FASTA file, where only the contigs for relevant
 * sequences are stored.
 * 3. The scaffold report file, which is used to determine the start, end and
 * depth of the scaffold.
 *
 * Each thread outputs to a separate file.
 */

#include "merauder4.h"

double rc_time = 0.0;

#define CHECK_SEQ_POS(x) do {                               \
        char label[500];                                    \
        sprintf(label, "  in %s:%d\n", __FILE__, __LINE__); \
        check_seqs(x, label, &num_abig);                    \
} while (0)

#define MAX_SCAFF_SEQ_LEN 1000000
#define FASTA_LINE_WIDTH 60

#define tprintf_vvv(fmt, ...) if (_cfg.verbose > 2) tprintf(fmt, ## __VA_ARGS__)
#define tprintf_vv(fmt, ...) if (_cfg.verbose >= 2) tprintf(fmt, ## __VA_ARGS__)
#define tprintf_v(fmt, ...) if (_cfg.verbose == 1) tprintf(fmt, ## __VA_ARGS__)

#define DIRN_RIGHT 0
#define DIRN_LEFT 1

static const char *DIRNS[2] = { "right", "left" };

// Limit for all htables. Exceeding this limit won't cause a failure, it will
// just cause inefficiencies, since collisions are resolved through linked lists
// note that this number gets rounded up to a prime close to 2^i
#define MAX_HTABLE_ENTRIES 20000 // This parameter has a HUGE impact on merwalking performance - RSE

// For output only
#define MAX_BUF 2000
#define MAX_WALK_LEN 8000
#define MAX_BEST_GUESS_SEQ_LEN 2000

#define MAX_SCAFF_CTG_SIZE 1000000L


#define GET_READ(s, i) ((s) + ((_cfg.max_read_len + 1) * (i)))

typedef struct {
    char *splint;
    int   freq;
} splint_info_t;

typedef struct {
    char *mer;
    int   freqs[4][4];
} qual_freqs_t;

typedef struct {
    char base;
    int  rating;
    int  n_hi_q;
    int  n_ext;
} qual_t;

typedef struct {
    char *mer;
    char  base;
    char  fork_bases[4];
    int   fork_n_ext[4];
} mer_base_t;

typedef struct {
    shared int *scaff_lens;
    shared int *contig_lens;
    int         scaff_tot;
    long        scaff_len_tot;
    int         contig_tot;
    long        contig_len_tot;
    long        num_ns;
} assembly_stats_t;

static config_t _cfg;

static int _nwalks = 0;
static int _nsuccess = 0;
static int _nfailure = 0;
static double _max_gap_time = 0.0;
static long _max_kmer_freqs = 0;

static long _num_rejected_splint_closures = 0;
static long _num_checked_splint_closures = 0;
static long _num_rejected_left_walk_closures = 0;
static long _num_checked_left_walk_closures = 0;
static long _num_rejected_right_walk_closures = 0;
static long _num_checked_right_walk_closures = 0;
static long _num_rejected_patch_closures = 0;
static long _num_checked_patch_closures = 0;

static const int _min_mer_len = 13;

DEFINE_HTABLE_TYPE(int32)
DEFINE_HTABLE_TYPE(splint_info);
DEFINE_HTABLE_TYPE(qual_freqs);
DEFINE_HTABLE_TYPE(mer_base);

static const char BASES[4] = { 'A', 'C', 'G', 'T' };

static inline char *get_seq(shared contig_t *contig, char *primer, int dirn)
{
    START_TIMER(T_GET_SEQ);
    double start_timer2, end_timer;

    if (!contig->seq) {
        stop_timer(T_GET_SEQ);
        return NULL;
    }
    char *seq = malloc_chk(contig->seq_len + 1);
    UPC_MEMGET_STR(seq, contig->seq, contig->seq_len + 1);
    if (contig->scaff_contig->strand == '-') {
        start_timer2 = UPC_TICKS_NOW();
        switch_code(reverse(seq));
        end_timer = UPC_TICKS_NOW();
        rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));
    }
#ifdef CONFIG_SANITY_CHECK
    if ((dirn == DIRN_LEFT && endswith(seq, primer) != 0) ||
        (dirn == DIRN_RIGHT && startswith(seq, primer) != 0)) {
        DIE("Contig sequence doesn't match primer at %s (strand %c):\nseq (%d): '%s'\nprimer: '%s'\n",
            dirn == DIRN_LEFT ? "end" : "start", contig->scaff_contig->strand, contig->seq_len, seq, primer);
    }
#endif
    stop_timer(T_GET_SEQ);
    return seq;
}

static char *last_strstr(char *haystack, char *needle)
{
    char *temp = haystack, *before = 0;

    while ((temp = strstr(temp, needle))) {
        before = temp++;
    }
    return before;
}

static char *splint(char *primer1, char *primer2, int *reads_lens, char *reads_nts, char *reads_quals,
                    int nreads)
{
    tprintf_vvv("Attempting splint: %s -> %s (%d read(s) available)\n", primer1, primer2, nreads);

    char *ret = NULL;
    htable_t pure_splints = create_htable(nreads * 4, "pure_splints");
    splint_info_t *first_splint = NULL;
    char *splint = NULL;
    char *nts, *p1_substr, *p2_substr;
    splint_info_t *splint_info;
    for (int i = 0; i < nreads; i++) {
        nts = GET_READ(reads_nts, i);
        if (!nts) {
            DIE("not nts, length %d\n", reads_lens[i]);
        }
        p1_substr = strstr(nts, primer1);
        // get the longest splint
        p2_substr = last_strstr(nts, primer2);
        if (p1_substr && p2_substr) {
            if (p1_substr <= p2_substr) {
                // a string starting with primer1 and finishing with primer2
                splint = strdup_chk0(p1_substr);
                splint[p2_substr + strlen(primer2) - p1_substr] = '\0';
                splint_info = htable_get_splint_info(pure_splints, splint);
                if (splint_info) {
                    splint_info->freq++;
                    free_chk0(splint);
                } else {
                    splint_info = malloc_chk0(sizeof(splint_info_t));
                    splint_info->freq = 1;
                    splint_info->splint = splint;
                    CHECK_ERR(htable_put_splint_info(pure_splints, splint_info->splint, splint_info, NO_CHECK_DUPS));
                    if (!first_splint) {
                        first_splint = splint_info;
                    }
                }
            }
        }
    }
    int nsplints = htable_num_entries(pure_splints);
    tprintf_vvv("%d distinct splintning sequence(s) found\n", nsplints);
    if (nsplints == 1 && first_splint->freq > 1) {
        tprintf_vv("Unique splintning sequence found: %d/%d reads splint the gap.\n",
                   first_splint->freq, nreads);
        ret = strdup_chk(first_splint->splint);
    } else if (_cfg.poly_mode) {
        int max_splint_freq = 0;
        htable_iter_t iter = htable_get_iter(pure_splints);
        splint_info_t *splint_info = NULL;
        int new_len, prev_len;
        while ((splint_info = htable_get_next_splint_info(pure_splints, iter)) != NULL) {
            if (splint_info->freq > max_splint_freq) {
                splint = splint_info->splint;
                max_splint_freq = splint_info->freq;
            } else if (splint_info->freq == max_splint_freq) {
                new_len = strlen(splint_info->splint);
                prev_len = strlen(splint);
                if (new_len > prev_len) {
                    // always prefer the longest splint for the same frequency
                    splint = splint_info->splint;
                } else if (new_len == prev_len && strcmp(splint_info->splint, splint) < 0) {
                    // prefer lexicographically smallest f or consistency
                    splint = splint_info->splint;
                }
            }
        }
        free_chk0(iter);
//        if (max_splint_freq > 1) {
        if (max_splint_freq > 0) {
            tprintf_vv("Maximum frequency splinting sequence found: %d/%d reads splint the gap.\n",
                       max_splint_freq, nreads);
            ret = strdup_chk(splint);
        } else {
            tprintf_vvv("No splintning sequence with frequency > 1 found\n");
        }
    } else {
        tprintf_vvv("No unique splintning sequence with frequency > 1 found\n");
    }
    destroy_htable(pure_splints, FREE_KEYS | FREE_VALS);
    return ret;
}

#define MIN3(a, b, c) ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

static int edit_dist(char *s1, char *s2)
{
    unsigned int x, y, s1len, s2len;

    s1len = strlen(s1);
    s2len = strlen(s2);
    unsigned int matrix[s2len + 1][s1len + 1];
    matrix[0][0] = 0;
    for (x = 1; x <= s2len; x++) {
        matrix[x][0] = matrix[x - 1][0] + 1;
    }
    for (y = 1; y <= s1len; y++) {
        matrix[0][y] = matrix[0][y - 1] + 1;
    }
    for (x = 1; x <= s2len; x++) {
        for (y = 1; y <= s1len; y++) {
            matrix[x][y] = MIN3(matrix[x - 1][y] + 1, matrix[x][y - 1] + 1,
                                matrix[x - 1][y - 1] + (s1[y - 1] == s2[x - 1] ? 0 : 1));
        }
    }
    return matrix[s2len][s1len];
}

static int check_closure(char *closure, char *primer1, char *primer2)
{
    /* int clen = strlen(closure); */

    /* int p1len = strlen(primer1); */
    /* char *cstart = strndup(closure, p1len); */
    /* int p1_match = edit_dist(cstart, primer1); */
    /* free(cstart); */

    /* int p2len = strlen(primer1); */
    /* int cend_off = (clen <= p2len ? 0 : clen - p2len); */
    /* char *cend = strndup(closure + cend_off, p2len); */
    /* int p2_match = edit_dist(cend, primer2); */
    /* free(cend); */

    int p1_match = startswith(closure, primer1);
    int p2_match = endswith(closure, primer2);

    if (p1_match != 0 || p2_match != 0) {
        //int max_mismatches = _cfg.mer_size - _min_mer_len;
        //int max_mismatches = 1;
        //if (p1_match > max_mismatches || p2_match > max_mismatches) {
        tprintf_vvv("closure [%s] rejected because it disagrees with primers\n", closure);
        return 1;
    }
    return 0;
}

//#define add_to_report(report_note, fmt, ...) printfBuffer(report_note, fmt, __VA_ARGS__)
#define add_to_report(...)

static char *splinting_reads(gap_t *gap, int *splint_check, Buffer report_note)
{
    START_TIMER(T_SPLINTING);
    char *splint_closure = splint(gap->primer1, gap->primer2, (int *)gap->reads_lens,
                                  (char *)gap->reads_nts, (char *)gap->reads_quals, gap->nreads);
    *splint_check = 0;
    if (splint_closure) {
        _num_checked_splint_closures++;
        *splint_check = check_closure(splint_closure, gap->primer1, gap->primer2);
        add_to_report(report_note, "splintClosure=%s;splintCheck=%d;", splint_closure, *splint_check);
        if (*splint_check != 0) {
            _num_rejected_splint_closures++;
            free(splint_closure);
            splint_closure = NULL;
        }
    }
    stop_timer(T_SPLINTING);
    return splint_closure;
}

static inline int get_base_index(char base)
{
    switch (base) {
    case 'A': return 0;
    case 'C': return 1;
    case 'G': return 2;
    case 'T': return 3;
    }
    return -1;
}

static int get_valid_base_str(char *s, int max_len, int *start, int *len)
{
    *start = 0;
    int first_N = 0;
    for (int j = 0; j < *len; j++) {
        if (s[j] == 'N') {
            first_N = j;
            if (j - *start >= max_len) {
                *len = j - *start;
                break;
            } else {
                while (s[j] == 'N') {
                    j++;
                }
                if (j < *len - 1) {
                    *start = j;
                } else {
                    *len = first_N - *start;
                }
            }
        }
        if (j == *len - 1) {
            *len -= *start;
        }
    }
    if (*len >= max_len) {
        return 1;
    } else {
        return 0;
    }
}

static int categorize_extension(int *ext)
{
    int min_viable = 1;

//    int min_viable = 3;
    if (min_viable > _cfg.min_depth) {
        tprintf_vv("Warning: in categorizeExtension minViable reset to match minDepth (%d)\n",
                   _cfg.min_depth);
        min_viable = _cfg.min_depth;
    }

    // 0 = No votes
    // 1 = One vote
    // 2 = nVotes < minViable
    // 3 = minDepth > nVotes >= minViable, nHiQ < minViable
    // 4 = minDepth > nVotes >= minViable ; nHiQ >= minViable
    // 5 = nVotes >= minDepth ; nHiQ < minViable
    // 6 = nVotes >= minDepth ; minViable < nHiQ < minDepth
    // 7 = nHiQ >= minDepth

    //    Ignore q<10 bases
    //    my $n = $ext[0]+$ext[1]+$ext[2]+$ext[3];
    int n = ext[1] + ext[2] + ext[3];
    int n_hi_q = ext[2] + ext[3];
    int category = -1;
    if (n == 0) {
        category = 0;
    } else if (n == 1) {
        category = 1;
    } else if (n < min_viable) {
        category = 2;
    } else {
        if ((n < _cfg.min_depth) || (n == min_viable)) {
            if (n_hi_q < min_viable) {
                category = 3;
            } else {
                category = 4;
            }
        } else {
            if (n_hi_q < min_viable) {
                category = 5;
            } else if (n_hi_q < _cfg.min_depth) {
                category = 6;
            } else {
                category = 7;
            }
        }
    }

    if (category == -1) {
        DIE("Undefined extension category");
    }
    return category;
}

static inline int cmp_quals(qual_t *q1, qual_t *q2)
{
    if (q1->rating > q2->rating) {
        return 1;
    }
    if (q1->rating < q2->rating) {
        return -1;
    }
    if (q1->n_hi_q > q2->n_hi_q) {
        return 1;
    }
    if (q1->n_hi_q < q2->n_hi_q) {
        return -1;
    }
    if (q1->n_ext > q2->n_ext) {
        return 1;
    }
    if (q1->n_ext < q2->n_ext) {
        return -1;
    }
    return 0;
}

static inline void sort_quals(qual_t **quals)
{
    for (int i = 1; i < 4; i++) {
        for (int k = i; k > 0 && cmp_quals(quals[k], quals[k - 1]) > 0; k--) {
            qual_t *tmp = quals[k];
            quals[k] = quals[k - 1];
            quals[k - 1] = tmp;
        }
    }
}

static void compute_qual_freqs(int mer_len, int *reads_lens, char *reads_nts, char *reads_quals,
                               int nreads, htable_t full_info)
{
    START_TIMER(T_COMP_QUAL_FREQ);
    //int max_read_len = 0;
    int mer_plus = mer_len + 1;
    int printedWarning = 0;

    for (int i = 0; i < nreads; i++) {
        int start = 0;
        int nts_len = reads_lens[i];
        //if (nts_len > max_read_len)
        //    max_read_len = nts_len;
        if (mer_len >= nts_len) {
            continue;
        }

        int subseq_len = nts_len;
        int subseq_pos = 0;
        while (get_valid_base_str(GET_READ(reads_nts, i) + subseq_pos, mer_plus, &start, &subseq_len)) {
            char *nts = strndup_chk0(GET_READ(reads_nts, i) + subseq_pos + start, subseq_len);
            char *quals = strndup_chk0(GET_READ(reads_quals, i) + subseq_pos + start, subseq_len);

            for (int j = 0; j < subseq_len - mer_len; j++) {
                char *mer = strndup_chk0(nts + j, mer_len);
                char extension = (nts + j)[mer_len];

                int offs = j + mer_plus - 1;
                assert(offs < subseq_len);
                int q = (quals[offs] - _cfg.qual_offset) / 10;
                if (q < 0) {
                    if (!printedWarning++) {
                        WARN("Invalid q value read %d (%d - %d) / 10 == %d. offs = %d, subseq_len = %d: %s\t%s\n", i, quals[offs], _cfg.qual_offset, q, offs, subseq_len, nts, quals);
                    }
                    q = 0;
                }
                if (q < 0) {
                    DIE("Invalid q value (%d - %d) / 10 == %d. offs = %d, subseq_len = %d\n", quals[offs], _cfg.qual_offset, q, offs, subseq_len);
                }
                if (q > 3) {
                    q = 3;
                }

                qual_freqs_t *qual_freqs = htable_get_qual_freqs(full_info, mer);
                if (!qual_freqs) {
                    qual_freqs = calloc_chk0(1, sizeof(qual_freqs_t));
                    qual_freqs->mer = mer;
                    CHECK_ERR(htable_put_qual_freqs(full_info, mer, qual_freqs, NO_CHECK_DUPS));
                } else {
                    free_chk0(mer);
                }
                int bi = get_base_index(extension);
                if (bi < 0 || bi > 3) {
                    tprintf("[%d:%d] %s\n", i, nts_len, GET_READ(reads_nts, i));
                    DIE("Invalid base at %d: %d\n", offs, extension);
                }
                qual_freqs->freqs[bi][q]++;
            }
            free_chk0(nts);
            free_chk0(quals);

            subseq_pos += (start + subseq_len);
            if (subseq_pos >= nts_len) {
                break;
            }
            subseq_len = nts_len - subseq_pos;
        }
    }
    stop_timer(T_COMP_QUAL_FREQ);
}

static void analyze_qual_freqs(htable_t full_info, htable_t mers)
{
    START_TIMER(T_ANALYZE_QUAL);
    //Full analysis of quality/frequency profile
    qual_freqs_t *qual_freqs;
    htable_iter_t iter = htable_get_iter(full_info);

    while ((qual_freqs = htable_get_next_qual_freqs(full_info, iter)) != NULL) {
        qual_t *quals[4];
        int n_total = 0;
        for (int i = 0; i < 4; i++) {
            quals[i] = malloc_chk0(sizeof(qual_t));
            quals[i]->base = BASES[i];
            // ignore q<10 bases
            quals[i]->n_ext = qual_freqs->freqs[i][1] + qual_freqs->freqs[i][2] +
                              qual_freqs->freqs[i][3];
            quals[i]->n_hi_q = qual_freqs->freqs[i][2] + qual_freqs->freqs[i][3];
            n_total += quals[i]->n_ext;
            quals[i]->rating = categorize_extension(qual_freqs->freqs[i]);
        }

        sort_quals(quals);

        //Rules for choosing next base
        //ratings:
        //0 = No votes
        //1 = One vote
        //2 = nVotes < minViable
        //3 = minDepth > nVotes >= minViable, nHiQ < minViable
        //4 = minDepth > nVotes >= minViable ; nHiQ >= minViable
        //5 = nVotes >= minDepth ; nHiQ < minViable
        //6 = nVotes >= minDepth ; minViable <= nHiQ < minDepth
        //7 = nHiQ >= minDepth

        int top_rating = quals[0]->rating;
        int runner_up = quals[1]->rating;
        int top_rated_base = quals[0]->base;

        mer_base_t *mer_base = calloc_chk0(1, sizeof(mer_base_t));
        mer_base->mer = strdup_chk0(qual_freqs->mer);
        if (top_rating < 3) {         // must have at least minViable bases
            //mer_base->base = 'X';
        } else if (top_rating == 3) { // must be uncontested
            if (runner_up == 0) {
                mer_base->base = top_rated_base;
            } else {
                mer_base->base = 0;
            }
        } else if (top_rating < 6) {
            if (runner_up < 3) {
                mer_base->base = top_rated_base;
            } else {
                mer_base->base = 0;
            }
        } else if (top_rating == 6) {  // viable and fair hiQ support
            if (runner_up < 4) {
                mer_base->base = top_rated_base;
            } else {
                mer_base->base = 0;
            }
        } else {                     // strongest rating trumps
            if (runner_up < 7) {
                mer_base->base = top_rated_base;
            } else {
                int k = 0;
                for (int b = 0; b < 4; b++) {
                    if (quals[b]->rating == 7) {
                        mer_base->fork_bases[k] = quals[b]->base;
                        mer_base->fork_n_ext[k] = quals[b]->n_ext;
                        k++;
                    } else {
                        break;
                    }
                }
            }
        }
        CHECK_ERR(htable_put_mer_base(mers, mer_base->mer, mer_base, CHECK_DUPS));
        for (int i = 0; i < 4; i++) {
            free_chk0(quals[i]);
        }
    }
    free_chk0(iter);
    stop_timer(T_ANALYZE_QUAL);
}

static char *get_fork_str(char *s, mer_base_t *b)
{
    strcpy(s, "F");
    for (int i = 0; i < 4; i++) {
        if (!b->fork_bases[i]) {
            break;
        }
        char buf[100];
        sprintf(buf, "%c%d", b->fork_bases[i], b->fork_n_ext[i]);
        strcat(s, buf);
    }
    return s;
}

#define EXTEND_WALK(walk, base, len)                                \
    do {                                                            \
        if (len >= MAX_WALK_LEN) { DIE("walk too long, %lld\n", (lld)len); } \
        strncat(walk, &(base), 1);                                  \
    } while (0)

static int walk_mers(char *primer1, char *walk_result, htable_t mers, htable_t full_info,
                     char *walk, char *true_primer2)
{
    double startTime = START_TIMER(T_WALK_MERS);
    char *step_buf = malloc_chk(MAX_WALK_LEN);
    char *step = step_buf;

    strcpy(step, primer1);
    long step_len = strlen(step);
    int success = 0;
    int n_forks = 0;
    int n_steps = 0;
    htable_t loop_check = create_htable(3500, "loop_check");
    int val = 1;

    while (1) {
        if (htable_get_int32(loop_check, step)) {
            strcpy(walk_result, "R");
            break;
        } else {
            CHECK_ERR(htable_put_int32(loop_check, strdup_chk0(step), &val, NO_CHECK_DUPS));
        }
        mer_base_t *next = htable_get_mer_base(mers, step);
        if (next) {
            n_steps++;
            if (_cfg.verbose > 2) {
                tprintf("%d : %s->", n_steps, step);
                if (next->base) {
                    tprintf("%c\t", next->base);
                } else if (next->fork_bases[0]) {
                    char buf[100];
                    tprintf("%s\t", get_fork_str(buf, next));
                } else {
                    tprintf("X\t");
                }
                qual_freqs_t *qf = htable_get_qual_freqs(full_info, step);
                for (int q = 0; q < 4; q++) {
                    tprintf("[%d %d %d %d]", qf->freqs[q][0], qf->freqs[q][1], qf->freqs[q][2],
                            qf->freqs[q][3]);
                }
                tprintf("\n");
            }
            if (next->base) {
                step++;
                step_len++;
                EXTEND_WALK(step, next->base, step_len);
                EXTEND_WALK(walk, next->base, strlen(walk));
                if (endswith(walk, true_primer2) == 0) {
                    success = 1;
                    break;
                }
            } else if (_cfg.poly_mode && !n_forks && next->fork_bases[0]) {
                //  Biallelic positions only (for now) maximum vote path is taken
                char c = 0;
                if (next->fork_bases[1] && !next->fork_bases[2]) {
                    c = next->fork_n_ext[0] > next->fork_n_ext[1] ?
                        next->fork_bases[0] : next->fork_bases[1];
                    tprintf_vvv("Polymorphic conditions met .. "
                                "attempting max frequency resolution.\n");
                } else {
                    char buf[100];
                    strcpy(walk_result, get_fork_str(buf, next));
                    break;
                }
                step++;
                step_len++;
                EXTEND_WALK(walk, c, strlen(walk));
                EXTEND_WALK(step, c, step_len);
                n_forks++;
                if (endswith(walk, true_primer2) == 0) {
                    success = 1;
                    walk_result[0] = 0;
                    break;
                }
            } else {
                if (next->fork_bases[0]) {
                    char buf[100];
                    strcpy(walk_result, get_fork_str(buf, next));
                } else {
                    strcpy(walk_result, "X");
                }
                break;
            }
        } else {
            strcpy(walk_result, "X");
            break;
        }
    }
    free_chk(step_buf);
    destroy_htable(loop_check, FREE_KEYS);
    double endTime = stop_timer(T_WALK_MERS);
    DBG("walk_mers finished in %0.3f s\n", endTime - startTime);
    return success;
}

static void bridge_iter(char *c1_seq, char *c2_seq, int *seq_ref_lens, char *seq_ref_nts,
                        char *seq_ref_quals, int nreads, int dirn, char **dwalk, char *dfail)
{
    double startTime = START_TIMER(T_BRIDGING), startTime2, endTime2;
    tprintf_vvv("Attempting %s bridge: (%d read(s) available)\n", DIRNS[dirn], nreads);

    int *reads_lens = malloc_chk(sizeof(int) * nreads);
    char *reads_nts = malloc_chk((_cfg.max_read_len + 1) * nreads);
    char *reads_quals = malloc_chk((_cfg.max_read_len + 1) * nreads);
    int max_read_length = 0;
    double start_timer2, end_timer;

    DBG("brige_iter on %lld reads\n", (lld)nreads);
    for (int i = 0; i < nreads; i++) {
        reads_lens[i] = seq_ref_lens[i];
        DBG2("brige_iter on read %d of %d: %s (len: %lld) %s\n", i, nreads,
             GET_READ(seq_ref_nts, i), (lld)strlen(GET_READ(seq_ref_nts, i)), GET_READ(seq_ref_quals, i));
        assert(isACGTN(*(GET_READ(seq_ref_nts, i))));
        strcpy(GET_READ(reads_nts, i), GET_READ(seq_ref_nts, i));
        if (reads_lens[i] > max_read_length) {
            max_read_length = reads_lens[i];
        }
        assert(*(GET_READ(seq_ref_quals, i)) >= _cfg.qual_offset);
        strcpy(GET_READ(reads_quals, i), GET_READ(seq_ref_quals, i));
        if (dirn == DIRN_LEFT) {
            start_timer2 = UPC_TICKS_NOW();
            switch_code(reverse(GET_READ(reads_nts, i)));
            end_timer = UPC_TICKS_NOW();
            rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));
            reverse(GET_READ(reads_quals, i));
        }
    }
    DBG("bridge_iter got %lld reads\n", (lld) nreads);

    int mer_len = _cfg.mer_size;
    int downshift = 0;
    int upshift = 0;

    char *max_walk = NULL;
    int max_walk_len = 0;

    char *c1s, *c2s;
    if (dirn == DIRN_LEFT) {
        c1s = c2_seq;
        c2s = c1_seq;
        start_timer2 = UPC_TICKS_NOW();
        switch_code(reverse(c1s));
        switch_code(reverse(c2s));
        end_timer = UPC_TICKS_NOW();
        rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));
    } else {
        c1s = c1_seq;
        c2s = c2_seq;
    }

    int sl1 = strlen(c1s);

    char *true_primer2 = strndup_chk(c2s, _cfg.mer_size);
    char *primer1 = NULL;
    char *primer2 = NULL;
    char walk_result[100] = "";
    int64_t iterations = 0;
    while (1) {
        iterations++;
        walk_result[0] = 0;
        // Allow k-mers less than mer_size to be used
        if (mer_len < _cfg.mer_size) {
            int the_rest = _cfg.mer_size - mer_len;
            primer1 = strndup_chk(c1s + sl1 - _cfg.mer_size, mer_len);
            primer2 = strndup_chk(c2s + the_rest, mer_len);
        } else {
            primer1 = strdup_chk(c1s + sl1 - mer_len);
            primer2 = strndup_chk(c2s, mer_len);
        }
        if (strlen(primer1) < mer_len) {
            free_chk(primer1);
            primer1 = NULL;
        }
        if (strlen(primer2) < mer_len) {
            free_chk(primer2);
            primer2 = NULL;
        }
        if (!primer1 || !primer2) {
            tprintf_vvv("Unable to find k=%d seeds\n", mer_len);
            if (!max_walk) {
                max_walk = malloc_chk(1);
                max_walk[0] = 0;
                strcpy(walk_result, "X");
                strcpy(dfail, walk_result);
            }
            if (primer1) {
                free_chk(primer1);
            }
            primer1 = NULL;
            if (primer2) {
                free_chk(primer2);
            }
            primer2 = NULL;
            break;
        }

        startTime2 = START_TIMER(T_COMPUTE_QUALS);
        int64_t maxKmers = nreads * (_cfg.max_read_len - mer_len + 1);
        if (maxKmers > _max_kmer_freqs) _max_kmer_freqs = maxKmers;
        htable_t full_info = create_htable(maxKmers * 2, "full_info");
        compute_qual_freqs(mer_len, reads_lens, reads_nts, reads_quals, nreads, full_info);
        int nentries = htable_num_entries(full_info);

        htable_t mers = create_htable(nentries * 5 / 2, "mers");
        analyze_qual_freqs(full_info, mers);

        endTime2 = stop_timer(T_COMPUTE_QUALS);

        DBG2("Created htables mer_len=%d nreads=%lld maxKmers=%lld nentries=%lld merEntries=%lld in %0.3f s\n", (lld) mer_len, (lld) nreads, (lld) maxKmers, (lld) nentries, (lld) htable_num_entries(mers), endTime2 - startTime2); 

        char *walk = malloc_chk(MAX_WALK_LEN);
        strcpy(walk, primer1);

        if (!max_walk) {
            max_walk = strdup_chk(walk);
            max_walk_len = strlen(max_walk);
            strcpy(dfail, walk_result);
        }

        int success = walk_mers(primer1, walk_result, mers, full_info, walk, true_primer2);

        // clean up
        destroy_htable(full_info, FREE_KEYS | FREE_VALS);
        destroy_htable(mers, FREE_KEYS | FREE_VALS);

        // Trim off extra lead bases if upshifted
        int additional_bases = mer_len - _cfg.mer_size;
        if (additional_bases < 0) {
            additional_bases = 0;
        }
        int walk_len = strlen(walk) - additional_bases;
        if (success || (walk_len > max_walk_len)) {
            if (max_walk) {
                free_chk(max_walk);
            }
            max_walk = strdup_chk(walk + additional_bases);
            max_walk_len = walk_len;
            strcpy(dfail, walk_result);
        }

        if (primer1) {
            free_chk(primer1);
        }
        if (primer2) {
            free_chk(primer2);
        }
        free_chk(walk);
        primer1 = NULL;
        primer2 = NULL;

        if (walk_result[0] == 'F' || walk_result[0] == 'R') {
            mer_len += 2;
            upshift = 1;
            if (downshift == 1 || mer_len >= max_read_length) {
                strcpy(dfail, walk_result);
                break;
            }
            tprintf_vvv("Degeneracy encountered; upshifting (k->%d)\n", mer_len);
        } else if (walk_result[0] == 'X') {
            mer_len -= 4;
            downshift = 1;
            if (upshift == 1 || mer_len < _min_mer_len) {
                strcpy(dfail, walk_result);
                break;
            }
            tprintf_vvv("Termination encountered; downshifting (k->%d)\n", mer_len);
        } else {
            break;
        }
    }

    *dwalk = max_walk;

    tprintf_vv("MAX%s [%s%s]\n", dirn == DIRN_RIGHT ? "RIGHT" : "LEFT",
               *dwalk ? *dwalk : "", dfail);

    free_chk(true_primer2);
    free_chk(reads_lens);
    free_chk(reads_nts);
    free_chk(reads_quals);
    double endTime = stop_timer(T_BRIDGING);
    DBG("finished bridge in %0.3f s for %lld reads\n", endTime - startTime, (lld) nreads);
    return;
}

static char *patch(char *right_walk, char *left_walk, int gap_size, double gap_uncertainty)
{
    int min_acceptable_overlap = 10;
    int right_len = strlen(right_walk);
    int left_len = strlen(left_walk);
    int gu = bankers_round(gap_uncertainty);
    int ideal_len = 2 * _cfg.mer_size + gap_size;
    int chop_left = left_len - ideal_len;
    int chop_right = right_len - ideal_len;

    tprintf_vvv("Attempting patch [%d][%d] (gap: %d +/- %.1f)\n",
                right_len, left_len, gap_size, gap_uncertainty);

    char *rwalk = strdup_chk(right_walk);
    char *lwalk = strdup_chk(left_walk);
    char *test_right = rwalk;
    char *test_left = lwalk;
    if (chop_left > 0) {
        test_left += chop_left;
    }
    int tl_len = strlen(test_left);
    if (chop_right > 0) {
        test_right[right_len - chop_right] = 0;
    }
    int tr_len = strlen(test_right);

    int max_buf = MAX_BEST_GUESS_SEQ_LEN;
    char *best_guess_seq = malloc_chk(max_buf);
    best_guess_seq[0] = 0;
    double best_guess_delta = gap_uncertainty + 1;
    int n_best_guesses = 0;
    int n_guesses = 0;
    for (int o = -gu; o <= gu; o++) {
        int overlap = tr_len + tl_len - (ideal_len + o);
        if (overlap < min_acceptable_overlap || overlap > tr_len || overlap > tl_len) {
            continue;
        }
        char *p1_suffix = test_right + tr_len - overlap;
        char *p2_suffix = test_left + overlap;
        if (strncmp(p1_suffix, test_left, overlap) != 0) {
            continue;
        }
        int delta = abs(o);
        n_guesses++;
        int newlen = strlen(test_right) + strlen(p2_suffix);
        if (newlen >= max_buf) {
            //WARN("buf for best guess seq len %d is too small\n", newlen);
            max_buf = newlen + 2;
            best_guess_seq = realloc_chk(best_guess_seq, max_buf);
        }
        if (delta < best_guess_delta) {
            sprintf(best_guess_seq, "%s%s", test_right, p2_suffix);
            best_guess_delta = delta;
            n_best_guesses = 1;
        } else if (delta == best_guess_delta) {
            if (strlen(test_right) + strlen(p2_suffix) < strlen(best_guess_seq)) {
                sprintf(best_guess_seq, "%s%s", test_right, p2_suffix);
                best_guess_delta = delta;
            }
            n_best_guesses++;
        }
    }
    free_chk(rwalk);
    free_chk(lwalk);

    if (n_guesses) {
        tprintf_vv("%d potential patches identified.  "
                   "%d best guess(es) differ from gap estimate by %.0f\n",
                   n_guesses, n_best_guesses, best_guess_delta);
        return best_guess_seq;
    }
    free_chk(best_guess_seq);
    tprintf_vv("No valid patches found.\n");
    return NULL;
}

static char *mer_walk_dirn(gap_t *gap, char *c1_seq, char *c2_seq, Buffer report_note, char **dwalk,
                           char *dfail, int *check, int dirn)
{
    _nwalks++;
    START_TIMER(T_MER_WALKS);
    double start_timer2, end_timer;
    *dwalk = NULL;
    dfail[0] = 0;
    char *closure = NULL;
    bridge_iter(c1_seq, c2_seq, (int *)gap->reads_lens, (char *)gap->reads_nts, (char *)gap->reads_quals,
                gap->nreads, dirn, dwalk, dfail);
    if (strcmp(*dwalk, "") == 0) {
        DIE("empty dwalk\n");
    }
    if (!*dwalk) {
        return NULL;
    }
    if (dirn == DIRN_LEFT) {
        start_timer2 = UPC_TICKS_NOW();
        switch_code(reverse(*dwalk));
        end_timer = UPC_TICKS_NOW();
        rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));
        if (dfail[0] == 'F') {
            switch_code(dfail);
        }
    }
    if (!dfail[0]) {
        if (dirn == DIRN_RIGHT) {
            _num_checked_right_walk_closures++;
        } else { _num_checked_left_walk_closures++; }
        *check = check_closure(*dwalk, gap->primer1, gap->primer2);
        add_to_report(report_note, "%sClosure=%s;%sCheck=%d;", DIRNS[dirn], *dwalk, DIRNS[dirn],
                      *check);
        if (*check == 0) {
            closure = strdup_chk(*dwalk);
        } else {
            if (dirn == DIRN_RIGHT) {
                _num_rejected_right_walk_closures++;
            } else { _num_rejected_left_walk_closures++; }
        }
    }
    stop_timer(T_MER_WALKS);
    return closure;
}

static void lowercase_gap(gap_t *gap, char *closure, int clen)
{
    // For more clear output - lowercase the sequence that was closed
    //  Lower case as little of the primer sequences as possible:
    int min_gap_mask = 2 * 5;
    int p1len = strlen(gap->primer1);
    int p2len = strlen(gap->primer2);
    int start_gap = p1len;
    int end_gap = clen - p2len;

    if (clen < p1len + p2len + min_gap_mask) {
        start_gap = clen / 2 - min_gap_mask / 2;
        end_gap = clen / 2 + min_gap_mask / 2;
        if (clen % 2) {
            end_gap++;
        }
    }
    for (int i = start_gap; i < end_gap; i++) {
        closure[i] = tolower(closure[i]);
    }
}

static void print_rejected_closures(long rejected, long checked, const char *name)
{
    long tot_rejected = reduce_long(rejected, UPC_ADD, SINGLE_DEST);
    long tot_checked = reduce_long(checked, UPC_ADD, SINGLE_DEST);

    serial_printf("\t%s: %ld (%.2f %%)\n", name, tot_rejected, (100.0 * tot_rejected) / tot_checked);
}

typedef struct {
    int n_splint_closures;
    int n_left_closures;
    int n_right_closures;
    int n_patch_closures;
    int n_no_reads;
    int n_walk_fails_x;
    int n_walk_fails_r;
    int n_walk_fails_f;
    int n_walk_fails_0;
    int n_cp_contigs;
} gap_report_t;

#define CLOSE_GAPS_NUM_VALS 10
static void close_gaps(shared [] gap_t *gaps, int ngaps, gap_report_t *report)
{
    double startTime = START_TIMER(T_CLOSE_GAPS);
    LOGF("Attempting to close %d gaps from thread %d...", ngaps, upc_threadof(gaps));

    int success = 0;
    int64_t nreads = 0;
    char buf[MAX_BUF];
    Buffer report_note = initBuffer(MAX_BUF * 2), report_line = initBuffer(MAX_BUF);
    INIT_CACHE_LOCAL(gap_t, gap);
    for (int64_t gapIdx = 0; gapIdx < ngaps; gapIdx++) {
        DBG("Closing %lld of %lld\n", (lld) gapIdx, (lld) ngaps);
        double startGap = now();
        // for debugging
#ifdef SOLO_GAP
        if (gapIdx != SOLO_GAP) {
            continue;
        }
#endif
        shared gap_t * _gap = (shared gap_t *) &gaps[gapIdx];
        CACHE_LOCAL(gap, _gap);
        gap->closure = NULL;
        // if the closure fails, we record the left and right walks and use them to partially fill the gap
        gap->right_walk = NULL;
        gap->left_walk = NULL;
        gap->len_right_walk = 0;
        gap->len_left_walk = 0;
        if (!gap->nreads) {
            report->n_no_reads++;
            //tprintf_flush("Gap %ld has no reads mapped, skipping...\n", gapIdx);
            _nfailure++;
            continue;
        }
        if (gap->cp_contigs) {
            report->n_cp_contigs++;
            //tprintf_flush("Gap %ld has copy contigs, skipping...\n", gapIdx);
            _nfailure++;
            continue;
        }
        nreads += gap->nreads;
        resetBuffer(report_note);
        shared contig_t *contig1;
        get_contig_chk(contig1, gap->contig1_id);
        if (!contig1) {
            DIE("Missing contig for gap %lld: %ld\n", (lld)gap->id, gap->contig1_id);
        }
        if (!contig1->seq) {
            DIE("Missing contig seq for gap %lld: %ld\n", (lld)gap->id, gap->contig1_id);
        }
        shared contig_t *contig2;
        get_contig_chk(contig2, gap->contig2_id);
        if (!contig2) {
            DIE("Missing contig for gap %lld: %ld\n", (lld)gap->id, gap->contig2_id);
        }
        if (!contig2->seq) {
            DIE("Missing contig seq for gap %lld: %ld\n", (lld)gap->id, gap->contig2_id);
        }
        shared scaffold_t *scaffold = get_scaffold(gap->scaff_id);
        if (!scaffold) {
            DIE("Missing scaffold for gap %lld: %ld\n", (lld)gap->id, gap->scaff_id);
        }

        // compute the primers here
        gap->primer1 = get_primer(contig1, gap->contig_ext1, 5, _cfg.mer_size);
        gap->primer2 = get_primer(contig2, gap->contig_ext2, 3, _cfg.mer_size);
#ifdef CONFIG_SANITY_CHECK
        if (strlen(gap->primer1) != _cfg.mer_size) {
            DIE("primer1 is wrong length %ld != %d\n", strlen(gap->primer1), _cfg.mer_size);
        }
        if (strlen(gap->primer2) != _cfg.mer_size) {
            DIE("primer2 is wrong length %ld != %d\n", strlen(gap->primer2), _cfg.mer_size);
        }
#endif
        resetBuffer(report_line);
        get_report_line(gap, report_line);
        if (_cfg.exclude_repeats) {
            if (scaffold->depth > _cfg.exclude_repeats) {
                if (_cfg.verbose >= 2) {
                    tprintf_vv("\n*************\nRepeat scaffold gap excluded. (depth = %.6f) %s\n",
                               scaffold->depth, get_gap_info(gap, buf, MAX_BUF - 1));
                    tprintf_vv("%s\tFAILED\tscaffDepth=%.6f", report_line->buf, scaffold->depth);
                }
                _nfailure++;
                continue;
            }
        }

        if (_cfg.verbose >= 2) {
            tprintf_vv("\n*************\nAttempt to close %lld: %s\n",
                       (lld)gapIdx, get_gap_info(gap, buf, MAX_BUF - 1));
        }

        int gotCopy = localize_gap_reads(gap); // now copy reads into local shared memory, if they are remote
        int splint_check;
        char *closure = splinting_reads(gap, &splint_check, report_note);
        // If splints fail try a mer-walk
        int right_check = 0;
        char *right_walk = NULL;
        char right_fail[100] = "";
        int left_check = 0;
        char *left_walk = NULL;
        char left_fail[100] = "";

        if (closure) {
            report->n_splint_closures++;
        } else {
            char *c1_seq = get_seq(contig1, gap->primer1, DIRN_LEFT);
            char *c2_seq = get_seq(contig2, gap->primer2, DIRN_RIGHT);

            if (c1_seq && c2_seq) {
                closure = mer_walk_dirn(gap, c1_seq, c2_seq, report_note, &right_walk, right_fail,
                                        &right_check, DIRN_RIGHT);
                if (closure) {
                    report->n_right_closures++;
                } else {
                    closure = mer_walk_dirn(gap, c1_seq, c2_seq, report_note, &left_walk, left_fail,
                                            &left_check, DIRN_LEFT);
                    if (closure) {
                        report->n_left_closures++;
                    }
                }
            }
            if (c1_seq) {
                free_chk(c1_seq);
            }
            if (c2_seq) {
                free_chk(c2_seq);
            }
        }
        snprintf(buf, MAX_BUF - 1, "[%lld\t%s\t%d\t%s]", (lld)gap->nreads,
                 gap->primer1, gap->size, gap->primer2);

        START_TIMER(T_PATCHING);
        // If walks failed to close, try to patch between left and right walk
        if (!_cfg.no_patching && !closure && right_fail[0] && left_fail[0]) {
            closure = patch(right_walk, left_walk, gap->size, 3 * gap->uncertainty + 5.5);
            if (closure) {
                _num_checked_patch_closures++;
                int patch_check = check_closure(closure, gap->primer1, gap->primer2);
                add_to_report(report_note, "patchClosure=%s;patchCheck=%d;", closure, patch_check);
                if (patch_check != 0) {
                    free_chk(closure);
                    closure = NULL;
                    _num_rejected_patch_closures++;
                } else {
                    report->n_patch_closures++;
                }
            }
        }
        stop_timer(T_PATCHING);

        if (closure) {
            int closed_gap_size = strlen(closure) - 2 * _cfg.mer_size;
            success = 1;
            _nsuccess++;
            tprintf_vv("%s successfully closed gap %lld: %d %s\n",
                       buf, (lld)gapIdx, closed_gap_size, closure);
#ifdef CONFIG_SANITY_CHECK
            if (startswith(closure, gap->primer1) != 0) {
                DIE("closure for gap %lld doesn't start with primer1\n", (lld)gapIdx);
            }
            if (endswith(closure, gap->primer2) != 0) {
                DIE("closure for gap %lld doesn't end with primer2\n", (lld)gapIdx);
            }
#endif
            //lowercase_gap(gap, closure, strlen(closure));
            int len_closure = strlen(closure);

            if (len_closure < 4) {
                WARN("short closure len %d: %s\n", len_closure, closure);
            }

            assert(gap->closure == NULL);
            DBG("Closed gap len %lld\n", (lld)len_closure);
            UPC_ALLOC_CHK(gap->closure, len_closure + 1);
            upc_memput(gap->closure, closure, len_closure + 1);
            gap->len_closure = len_closure;
        } else {
            success = 0;
            _nfailure++;
            switch (right_fail[0]) {
            case 'X': report->n_walk_fails_x++; break;
            case 'R': report->n_walk_fails_r++; break;
            case 'F': report->n_walk_fails_f++; break;
            case   0: report->n_walk_fails_0++; break;
            default: LOGF("\nTh%d: Unknown failure mode right: %s\n", MYTHREAD, right_fail);
            }
            switch (left_fail[0]) {
            case 'X': report->n_walk_fails_x++; break;
            case 'R': report->n_walk_fails_r++; break;
            case 'F': report->n_walk_fails_f++; break;
            case   0: report->n_walk_fails_0++; break;
            default: LOGF("\nTh%d: Unknown failure mode left: %s\n", MYTHREAD, left_fail);
            }
            tprintf_vv("%s failed to close gap %lld (%d;%d:%s;%d:%s)\n",
                       buf, (lld)gapIdx, splint_check, right_check, right_fail, left_check, left_fail);
            // no closure - save the left and right walks to partially fill the gap
            if (right_walk && startswith(right_walk, gap->primer1) == 0) {
                gap->len_right_walk = strlen(right_walk);
                UPC_ALLOC_CHK(gap->right_walk, gap->len_right_walk + 1);
                upc_memput(gap->right_walk, right_walk, gap->len_right_walk + 1);
            }
            if (left_walk && endswith(left_walk, gap->primer2) == 0) {
                gap->len_left_walk = strlen(left_walk);
                UPC_ALLOC_CHK(gap->left_walk, gap->len_left_walk + 1);
                upc_memput(gap->left_walk, left_walk, gap->len_left_walk + 1);
            }
        }

        if (right_walk) {
            free_chk(right_walk);
        }
        if (left_walk) {
            free_chk(left_walk);
        }
        if (closure) {
            free_chk(closure);
        }

        tprintf_vv("%s\n", report_line->buf);
        tprintf_vv("%s\t%s\n", success ? "SUCCESS" : "FAILED", report_note->buf);
        tprintf_v("%lld: %s\t%s\n", (lld)gapIdx, success ? "SUCCESS" : "FAILED", report_note->buf);

#ifdef CONFIG_SANITY_CHECK
        if (strlen(gap->primer1) != _cfg.mer_size) {
            DIE("primer1 is wrong length %ld != %d\n", strlen(gap->primer1), _cfg.mer_size);
        }
        if (strlen(gap->primer2) != _cfg.mer_size) {
            DIE("primer2 is wrong length %ld != %d\n", strlen(gap->primer2), _cfg.mer_size);
        }
#endif
        if (gotCopy) {
            free_gap_reads(gap);
        }
        double gap_time = now() - startGap;
        if (_max_gap_time < gap_time) {
            _max_gap_time = gap_time;
        }
    }
    FINISH_CACHE_LOCAL(gap);

    double endTime = stop_timer(T_CLOSE_GAPS);
    LOGFN("processed %lld gaps with %lld reads in %0.3fs %0.3fs gaps/s\n", (lld) ngaps, (lld) nreads, endTime - startTime, ngaps / (endTime - startTime));
    freeBuffer(report_line);
    freeBuffer(report_note);
}

static void report_gap_stats(gap_report_t *report)
{
    tprintf_flush("...  successfully closed %d gaps (%d failed to close)\n",
                  _nsuccess, _nfailure);
    tprintf_flush("Closures: %d splints, %d right, %d left, %d patched\n",
                  report->n_splint_closures, report->n_right_closures, report->n_left_closures, report->n_patch_closures);
    tprintf_flush("Number of walks: %d\n", _nwalks);
    long tot_nsuccess = reduce_long(_nsuccess, UPC_ADD, SINGLE_DEST);
    long tot_nfailure = reduce_long(_nfailure, UPC_ADD, SINGLE_DEST);
    double max_gap_time = reduce_double(_max_gap_time, UPC_MAX, SINGLE_DEST);
    long max_kmer_freqs = reduce_long(_max_kmer_freqs, UPC_MAX, SINGLE_DEST);
    //double t_all = get_timer_all_max(T_CLOSE_GAPS);
    //serial_printf("done in %.3f s\n",  t_all);
    tprintf_flush("done in %.3f s\n", get_elapsed_time(T_CLOSE_GAPS));

    UPC_LOGGED_BARRIER;

    serial_printf("done in %.3f s\n", get_elapsed_time(T_CLOSE_GAPS));
    serial_printf("successfully closed %ld gaps (%ld failed to close %0.1f %%). max_time %0.3f s (my max %0.3f s) maxKmers=%lld (myMaxKmers=%lld)\n",
                  tot_nsuccess, tot_nfailure, 100.0 * tot_nfailure / (tot_nsuccess+tot_nfailure),
                  max_gap_time, _max_gap_time,
                  (lld) max_kmer_freqs, (lld) _max_kmer_freqs);

    double myvals[CLOSE_GAPS_NUM_VALS] = { report->n_splint_closures, report->n_right_closures, report->n_left_closures,
                                           report->n_patch_closures,  report->n_no_reads,       report->n_cp_contigs,
                                           report->n_walk_fails_x,    report->n_walk_fails_r,   report->n_walk_fails_f,
                                           report->n_walk_fails_0 };
    double min_vals[CLOSE_GAPS_NUM_VALS], max_vals[CLOSE_GAPS_NUM_VALS], tot_vals[CLOSE_GAPS_NUM_VALS];

    reduce_block(myvals, CLOSE_GAPS_NUM_VALS, min_vals, max_vals, tot_vals);

    if (!MYTHREAD) {
        long tot_nsplints = tot_vals[0];
        long tot_nright = tot_vals[1];
        long tot_nleft = tot_vals[2];
        long tot_npatches = tot_vals[3];
        serial_printf("Closures: %lld splints, %lld right, %lld left, %lld patched\n",
                      (lld) tot_nsplints, (lld) tot_nright, (lld) tot_nleft, (lld) tot_npatches);
        long tot_n_no_reads = tot_vals[4];
        long tot_n_cp_contigs = tot_vals[5];
        serial_printf("Skipped gaps: %lld with no reads, %lld copied contigs\n",
                      (lld) tot_n_no_reads, (lld) tot_n_cp_contigs);
        long tot_walk_fails_x = tot_vals[6];
        long tot_walk_fails_r = tot_vals[7];
        long tot_walk_fails_f = tot_vals[8];
        long tot_walk_fails_0 = tot_vals[9];
        serial_printf("Walk failures: %lld X, %lld R, %lld F %lld null\n", (lld) tot_walk_fails_x, (lld) tot_walk_fails_r, (lld) tot_walk_fails_f, (lld) tot_walk_fails_0);
        ADD_DIAG("%lld", "total_splint_closures", (lld) tot_nsplints);
        ADD_DIAG("%lld", "total_right_walk_closures", (lld) tot_nright);
        ADD_DIAG("%lld", "total_left_walk_closures", (lld) tot_nleft);
        ADD_DIAG("%lld", "total_patch_closure", (lld) tot_npatches);
    }

    serial_printf("Number of rejected closures:\n");
    print_rejected_closures(_num_rejected_splint_closures, _num_checked_splint_closures, "splint");
    print_rejected_closures(_num_rejected_right_walk_closures, _num_checked_right_walk_closures, "rightwalk");
    print_rejected_closures(_num_rejected_left_walk_closures, _num_checked_left_walk_closures, "leftwalk");
    print_rejected_closures(_num_rejected_patch_closures, _num_checked_patch_closures, "patch");
}

static void print_closures(shared [] gap_t *gaps, int ngaps, GZIP_FILE closures_file)
{
    LOGF("printing closures for %lld gaps\n", (lld) ngaps);
    for (int gapIdx = 0; gapIdx < ngaps; gapIdx++) {
        shared gap_t *_gap = (shared gap_t *) &gaps[gapIdx];
        CACHE_LOCAL_RO(gap_t, gap, _gap);
        if (gap->closure) {
            int res = GZIP_PRINTF(closures_file, "Scaffold%lld\tContig%lld.%d\t%s\tContig%lld.%d\t%s\t%s\n",
                                  (lld)gap->scaff_id, (lld)gap->contig1_id, gap->contig_ext1, gap->primer1,
                                  (lld)gap->contig2_id, gap->contig_ext2, gap->primer2, (char *)gap->closure);
            if (res == 0) {
                DIE("Could not write to colsures file! %s\n", strerror(errno));
            }
        }
    }
}


static void print_fasta(GZIP_FILE f, int64_t scaff_id, char *seq, int seq_len, const char *prefix, int line_width)
{
    GZIP_PRINTF(f, ">%s%lld\n", prefix, (lld)scaff_id);
    if (line_width) {
        int nlines = INT_CEIL(seq_len, FASTA_LINE_WIDTH);
        char buf[FASTA_LINE_WIDTH + 1];
        for (int i = 0; i < nlines; i++) {
            strncpy(buf, seq + (i * FASTA_LINE_WIDTH), FASTA_LINE_WIDTH);
            int end = FASTA_LINE_WIDTH;
            if (i == nlines - 1) {
                end = seq_len - FASTA_LINE_WIDTH * (nlines - 1);
            }
            buf[end] = 0;
#ifdef CHECK_CHARS_IN_OUTPUT
            CHECK_SEQ_POS(buf);
#endif
            GZIP_PRINTF(f, "%s\n", buf);
        }
    } else {
        char *buf = strndup(seq, seq_len + 1);
        buf[seq_len] = 0;
        GZIP_PRINTF(f, "%s\n", buf);
        free(buf);
    }
}

static char *to_nearest_units(char *s, long val)
{
    const double one_gb = 1000000000;
    const double one_mb = 1000000;
    const double one_kb = 1000;

    if (val >= one_gb) {
        sprintf(s, "%.1f GB", (double)val / one_gb);
    } else if (val >= one_mb) {
        sprintf(s, "%.1f MB", (double)val / one_mb);
    } else if (val >= one_kb) {
        sprintf(s, "%.1f KB", (double)val / one_kb);
    } else {
        sprintf(s, "%ld", val);
    }
    return s;
}

static int cmp_int(const void *x, const void *y)
{
    int xi = *(int *)x;
    int yi = *(int *)y;

    if (xi < yi) {
        return 1;
    }
    if (xi > yi) {
        return -1;
    }
    return 0;
}

static void calc_n50(shared int *lens, int n, int max_i, const char *desc)
{
    if (!n) {
        SDIE("Cannot calculate assembly stats for 0 elements\n");
    }
    UPC_TIMED_BARRIER;
    // just sort in thread 0
    if (MYTHREAD == 0) {
        long tot_size = 0;
        int *local_lens = malloc_chk(sizeof(int) * n);
        int j = 0;
        for (int i = 0; i < max_i; i++) {
            if (!lens[i]) {
                continue;
            }
            local_lens[j] = lens[i];
            tot_size += local_lens[j];
            j++;
        }
        // now use system qsort
        qsort(local_lens, n, sizeof(int), cmp_int);
        //for (int i = 0; i < n; i++)
        //    dbg("%d\t%d\n", i, local_lens[i]);
        // now run the algorithm
        long running_tot = 0;
        int n_index = 1;
        int N50 = 0;
        int L50 = 0;
        char buf[100];
        for (int i = 0; i < n; i++) {
            running_tot += local_lens[i];
            while (running_tot > tot_size * n_index / 100) {
                if (n_index == 50) {
                    L50 = local_lens[i];
                    N50 = i + 1;
                }
                n_index++;
            }
        }
        free_chk(local_lens);
        serial_printf("\t%s N/L50: %d/%s\n", desc, N50, to_nearest_units(buf, L50));
    }
}

#define ASSEMBLY_STATS_NUM_VALS 5
static void calc_assembly_stats(assembly_stats_t *stats)
{
    START_TIMER(T_CALC_ASSEMBLY_STATS);
    char buf[100];

    double myvals[ASSEMBLY_STATS_NUM_VALS] = { stats->scaff_tot,      stats->contig_tot, stats->scaff_len_tot,
                                               stats->contig_len_tot, stats->num_ns };

    double min_vals[ASSEMBLY_STATS_NUM_VALS], max_vals[ASSEMBLY_STATS_NUM_VALS], tot_vals[ASSEMBLY_STATS_NUM_VALS];

    reduce_block(myvals, ASSEMBLY_STATS_NUM_VALS, min_vals, max_vals, tot_vals);

    if (!MYTHREAD) {
        stats->scaff_tot = tot_vals[0];
        stats->contig_tot = tot_vals[1];
        stats->scaff_len_tot = tot_vals[2];
        if (stats->scaff_len_tot == 0) {
            stats->scaff_len_tot = 1;
        }
        stats->contig_len_tot = tot_vals[3];
        stats->num_ns = tot_vals[4];

        serial_printf("Assembly stats (excluding scaffolds < %d):\n", _cfg.scaff_len_cutoff);
        serial_printf("\tScaffold total: %s\n", to_nearest_units(buf, stats->scaff_tot));
        serial_printf("\tContig total:   %s\n", to_nearest_units(buf, stats->contig_tot));
        serial_printf("\tScaffold sequence total: %s\n", to_nearest_units(buf, stats->scaff_len_tot));
        serial_printf("\tContig sequence total:   %s (->  %.1f%% gap)\n",
                      to_nearest_units(buf, stats->contig_len_tot),
                      stats->scaff_len_tot == 0 ? 0.0 :
                      100.0 * (1.0 - (double)stats->contig_len_tot / stats->scaff_len_tot));
        // this is done better in canonical assembly
        //calc_n50(stats->scaff_lens, stats->scaff_tot, get_max_scaffolds(), "scaffold");
        //calc_n50(f, stats->contig_lens, stats->contig_tot, get_max_contigs(), "contig");
        serial_printf("\tNumber of N's: %ld ( %.2f per 100 kpb)\n",
                      stats->num_ns, (double)stats->num_ns / ((double)stats->scaff_len_tot / 100000));
    }
    stop_timer(T_CALC_ASSEMBLY_STATS);
}

typedef struct {
    char *s;
    int   size;
} seq_buf_t;

#define EXTEND_SEQ_BUF(seq_buf, len) do {                               \
        if (len < 0) {                                                    \
            DIE("Sequence  length < 0, i.e. %d\n", len); }                \
        if (len >= seq_buf->size) {                                     \
            seq_buf->size = 2 *len;                                    \
            seq_buf->s = realloc_chk(seq_buf->s, seq_buf->size);        \
        }                                                               \
} while (0)

static seq_buf_t *init_seq_buf(int size)
{
    seq_buf_t *seq_buf = calloc_chk(1, sizeof(seq_buf_t));

    seq_buf->s = calloc_chk(size, 1);
    seq_buf->size = size;
    return seq_buf;
}

shared int64_t ctg_output_index = 0;

static void scaff_to_fasta(assembly_stats_t *stats, shared gap_set_t *gaps, int tot_gaps)
{
    tprintf_flush("Converting scaffolds and gaps to fasta...");
    serial_printf("Converting scaffolds and gaps to fasta...");
    START_TIMER(T_SCAFF_TO_FASTA);
    char fname[MAX_FILE_PATH * 2];
    sprintf(fname, "assembly.fa" GZIP_EXT);
    GZIP_FILE assembly_file = openCheckpoint(fname, "w");
    GZIP_FILE assembly_short = NULL;
    if (_cfg.scaff_len_cutoff) {
        sprintf(fname, "assembly-%d.fa" GZIP_EXT, _cfg.scaff_len_cutoff);
        assembly_short = openCheckpoint(fname, "w");
    }

    char contigs_fname[MAX_FILE_PATH * 2];
    sprintf(contigs_fname, "scaff_contigs.fa" GZIP_EXT);
    GZIP_FILE contigs_file = openCheckpoint(contigs_fname, "w");

    double start_timer2, end_timer;

    shared scaffold_t *scaffolds = get_scaffolds();
    int max_scaffolds = get_max_scaffolds();

    // statistics
    UPC_ALL_ALLOC_CHK(stats->scaff_lens, max_scaffolds + 1, sizeof(int));

    seq_buf_t *scaff_seq = init_seq_buf(MAX_SCAFF_SEQ_LEN);
    seq_buf_t *contig_seq = init_seq_buf(MAX_SCAFF_SEQ_LEN);
    seq_buf_t *closure = init_seq_buf(MAX_SCAFF_SEQ_LEN);
    seq_buf_t *right_walk = init_seq_buf(MAX_SCAFF_SEQ_LEN);
    seq_buf_t *left_walk = init_seq_buf(MAX_SCAFF_SEQ_LEN);

    int slen, clen, offset, scaff_ctg_start_offset;
    shared scaffold_t *scaffold;
    shared contig_t *c1, *c2;
    int s_start, gap_size, gap_excess, cutback1, cutback2, ncount, breaks, scaff_seq_len;
    int num_extended = 0;
    int num_mismatched_walks = 0;
    long num_short_scaff_contigs = 0;

    long tot_nsuccess = reduce_long(_nsuccess, UPC_ADD, SINGLE_DEST);
    long tot_nfailure = reduce_long(_nfailure, UPC_ADD, SINGLE_DEST);

    long num_neg_unclosed_gaps = 0;
    long num_neg_gap_ns = 0;
    long num_neg_gap_bases = 0;
    long num_pos_gap_bases = 0;

#ifdef PARTIAL_GAP_CLOSURE
    long num_bases_filled = 0;
#endif

    long ngaps = 0;
    long ngaps_closed = 0;
    long ngaps_closed_spans = 0;
    long ngaps_unclosed_spans = 0;

    for (int64_t i = MYTHREAD; i < max_scaffolds; i += THREADS) {
        scaff_seq->s[0] = 0;
        slen = 0;
        clen = 0;
        offset = 0;

        scaffold = &scaffolds[i];
        if (scaffold->id == -1) {
            DIE("Found unused scaffold %ld\n", i);
            //continue;
        }
        for (int j = 0; j < scaffold->ncontigs; j++) {
            shared [] scaff_contig_t * scaff_contig1 = &scaffold->scaff_contigs[j];
            get_contig_chk(c1, scaff_contig1->cid);
#ifdef DBG_SCAFF_FASTA
            dbg("Scaffold%lld\tContig%lld %c\n", (lld)scaffold->id, (lld)scaff_contig1->cid,
                scaff_contig1->strand);
#endif
            if (c1->seq_len == 0) { 
                LOGF("Found 0 length contig! scaffold %lld contig %d\n", (lld) i, j);
                continue;
            }
            EXTEND_SEQ_BUF(contig_seq, c1->seq_len + 1);
            UPC_MEMGET_STR(contig_seq->s, c1->seq, c1->seq_len + 1);
            start_timer2 = UPC_TICKS_NOW();
            if (scaff_contig1->strand == '-') {
                switch_code(reverse(contig_seq->s));
            }
            end_timer = UPC_TICKS_NOW();
            rc_time += (UPC_TICKS_TO_SECS(end_timer - start_timer2));

            clen = strlen(contig_seq->s) - offset;
            EXTEND_SEQ_BUF(scaff_seq, clen + slen + 1);
            strcat(scaff_seq->s, contig_seq->s + offset);
            slen += clen;
            offset = 0;

            shared [] gap_t * gap = NULL;
            if (scaff_contig1->strand == '-' && scaff_contig1->gapi_3 != -1) {
                gap = get_gap(scaff_contig1->gapi_3, __LINE__, __FILE__, NULL_ON_FAIL);
            } else if (scaff_contig1->strand == '+' && scaff_contig1->gapi_5 != -1) {
                gap = get_gap(scaff_contig1->gapi_5, __LINE__, __FILE__, NULL_ON_FAIL);
            }
            if (!gap) {
                continue;
            }

            int min_gap_size = (gap->uncertainty <= 1 ? 0 : _cfg.min_gap_size);
            //int min_gap_size = _cfg.min_gap_size;

            ngaps++;

            if (gap->closure) {
                ngaps_closed++;
                if (gap->uncertainty > 1) {
                    ngaps_closed_spans++;
                }
                EXTEND_SEQ_BUF(closure, gap->len_closure + 1);
                UPC_MEMGET_STR(closure->s, gap->closure, gap->len_closure + 1);
#ifdef DBG_SCAFF_FASTA
                c2 = NULL;
                if (gap->contig1_id == scaff_contig1->cid) {
                    get_contig_chk(c2, gap->contig2_id);
                } else {
                    get_contig_chk(c2, gap->contig1_id);
                }
                dbg("    closure Contig%lld:Contig%lld\n", (lld)scaff_contig1->cid,
                    (lld)scaff_contig1->cid);
#endif
                clen = strlen(closure->s);
                EXTEND_SEQ_BUF(scaff_seq, slen - _cfg.mer_size + clen + 1);
                if (slen < _cfg.mer_size) {
                    DIE("offset is negative: %d - %d < 0\n", slen, _cfg.mer_size);
                }
                strcpy(scaff_seq->s + slen - _cfg.mer_size, closure->s);
                slen += (clen - _cfg.mer_size);
                offset = _cfg.mer_size;
            } else {
                // fill in with NNs
                if (j + 1 < scaffold->ncontigs) {
                    shared [] scaff_contig_t * scaff_contig2 = &scaffold->scaff_contigs[j + 1];
                    get_contig_chk(c2, scaff_contig2->cid);
                    s_start = scaff_contig2->s_start;
                    gap_size = s_start - scaff_contig1->s_end - 1;
#ifdef DBG_SCAFF_FASTA
                    dbg("    seq no closure, gap size %d, start %d, coord %d\n",
                        gap_size, s_start, scaff_contig1->s_end);
#endif
                    if (gap->uncertainty > 1) {
                        ngaps_unclosed_spans++;
                    }
                    if (gap_size <= 0) {
                        num_neg_unclosed_gaps++;
                        num_neg_gap_bases += (-gap_size);
                    } else {
                        num_pos_gap_bases += gap_size;
                    }
                    // Trim contigs back around residual negative gaps to eliminate potential
                    // redundancy
                    if (gap_size < min_gap_size) {
                        gap_excess = min_gap_size - gap_size;
                        cutback1 = !(gap_excess % 2) ? gap_excess / 2 : (gap_excess + 1) / 2;
                        cutback2 = !(gap_excess % 2) ? gap_excess / 2 : (gap_excess - 1) / 2;
#ifdef DBG_SCAFF_FASTA
                        dbg("    Nibbling back around gap: Contig%lld:Contig%lld\n",
                            (lld)scaff_contig1->cid, (lld)scaff_contig2->cid);
#endif
                        slen -= cutback1;
                        EXTEND_SEQ_BUF(scaff_seq, slen + 1);
                        scaff_seq->s[slen] = 0;
                        if (cutback2 + _cfg.mer_size >= c2->seq_len && slen >= cutback2) {
                            // Don't trim the upcoming contig if the trimming would leave less than
                            // a primer-size of sequence.  We will need at least that much sequence
                            // to prime the next gap.  Instead, cut back more from the already
                            // processed scaffold
                            slen -= cutback2;
                            EXTEND_SEQ_BUF(scaff_seq, slen + 1);
                            scaff_seq->s[slen] = 0;
                        } else {
                            offset = cutback2;
                        }
                        gap_size = min_gap_size;
                    }
                    if (gap_size < min_gap_size) {
                        DIE("At this point gap size should never be less than the min, %ld\n", gap_size);
                    }
                    if (gap_size == 0) {
                        continue;
                    }

                    // now the gap size is positive
                    // first memset all the remaining spots to N's
                    int start_slen = slen;
                    EXTEND_SEQ_BUF(scaff_seq, slen + gap_size + 1);
                    memset(scaff_seq->s + slen, 'N', gap_size);
                    slen += gap_size;
                    scaff_seq->s[slen] = 0;
                    int gap_remaining = gap_size - min_gap_size;

#ifdef PARTIAL_GAP_CLOSURE
                    if (gap_remaining > 0) {
                        // if we are doing partial gap closing, overwrite the Ns where appropriate,
                        // always leaving a min gap size
                        int ext_len = gap->len_right_walk - _cfg.mer_size;
                        if (ext_len > gap_remaining) {
                            ext_len = gap_remaining;
                        }
                        if (ext_len > 0) {
                            EXTEND_SEQ_BUF(right_walk, gap->len_right_walk + 1);
                            UPC_MEMGET_STR(right_walk->s, gap->right_walk, gap->len_right_walk + 1);
                            strncpy(scaff_seq->s + start_slen, right_walk->s + _cfg.mer_size, ext_len);
                            gap_remaining -= ext_len;
                            num_extended++;
                            num_bases_filled += ext_len;
                        } else {
                            num_mismatched_walks++;
                        }
                        if (gap_remaining > 0) {
                            ext_len = gap->len_left_walk - _cfg.mer_size;
                            if (ext_len > gap_remaining) {
                                ext_len = gap_remaining;
                            }
                            if (ext_len > 0) {
                                EXTEND_SEQ_BUF(left_walk, gap->len_left_walk + 1);
                                UPC_MEMGET_STR(left_walk->s, gap->left_walk, gap->len_left_walk + 1);
                                strncpy(scaff_seq->s + slen - ext_len, left_walk->s, ext_len);
                                gap_remaining -= ext_len;
                                num_extended++;
                                num_bases_filled += ext_len;
                            } else {
                                num_mismatched_walks++;
                            }
                        }
                        if (scaff_seq->s[slen] != 0) {
                            DIE("Scaff seq not null terminated\n");
                        }
                    }
#endif
                    stats->num_ns += (gap_remaining + min_gap_size);
                }
            }
        }
        scaff_seq_len = strlen(scaff_seq->s);
        assert(scaff_seq_len == slen);

        if (scaff_seq_len >= _cfg.scaff_len_cutoff) {
            print_fasta(assembly_file, scaffold->id, scaff_seq->s, scaff_seq_len, "Scaffold", FASTA_LINE_WIDTH);

            // print out the split contigs
            int scaff_ctg_offset = 0;
            for (int si = 0; si < scaff_seq_len; si++) {
                if (scaff_seq->s[si] == 'N') {
                    if (si - scaff_ctg_offset >= _cfg.mer_size) {
                        int64_t ctg_i;
                        UPC_ATOMIC_FADD_I64(&ctg_i, &ctg_output_index, 1);
                        print_fasta(contigs_file, ctg_i, scaff_seq->s + scaff_ctg_offset,
                                    si - scaff_ctg_offset, "Contig_", 0);
                    } else {
                        num_short_scaff_contigs++;
                    }
                    while (scaff_seq->s[si] == 'N') {
                        si++;
                    }
                    scaff_ctg_offset = si;
                }
            }
            if (scaff_ctg_offset < scaff_seq_len) {
                if (scaff_seq_len - scaff_ctg_offset >= _cfg.mer_size) {
                    int64_t ctg_i;
                    UPC_ATOMIC_FADD_I64(&ctg_i, &ctg_output_index, 1);
                    print_fasta(contigs_file, ctg_i, scaff_seq->s + scaff_ctg_offset,
                                scaff_seq_len - scaff_ctg_offset, "Contig_", 0);
                } else {
                    num_short_scaff_contigs++;
                }
            }

            // gather assembly stats
            stats->scaff_lens[scaffold->id] = scaff_seq_len;
            stats->scaff_tot++;
            // compute number of contigs by splitting over at least 25 consecutive N's
            ncount = 0;
            breaks = 0;
            for (int j = 0; j < scaff_seq_len; j++) {
                if (scaff_seq->s[j] == 'N') {
                    ncount++;
                } else {
                    if (ncount >= 25) {
                        stats->contig_tot++;
                        breaks += ncount;
                    }
                    ncount = 0;
                }
            }
            // the scaffold itself is a contig
            stats->contig_tot++;
            stats->contig_len_tot += (scaff_seq_len - breaks);
            stats->scaff_len_tot += scaff_seq_len;
        } else if (scaff_seq_len > _cfg.mer_size) {
            print_fasta(assembly_short, scaffold->id, scaff_seq->s, scaff_seq_len, "Scaffold", FASTA_LINE_WIDTH);
            stats->scaff_lens[scaffold->id] = 0;
        }
    }
    closeCheckpoint(assembly_file);
    closeCheckpoint(contigs_file);
    //printf("thread %d has %d extended, %d mismatched\n", MYTHREAD, num_extended, num_mismatched_walks);
    UPC_TIMED_BARRIER;
    if (_cfg.scaff_len_cutoff) {
        closeCheckpoint(assembly_short);
    }

    free_chk(scaff_seq->s); free_chk(scaff_seq);
    free_chk(contig_seq->s); free_chk(contig_seq);
    free_chk(closure->s); free_chk(closure);
    free_chk(right_walk->s); free_chk(right_walk);
    free_chk(left_walk->s); free_chk(left_walk);

    UPC_TIMED_BARRIER;
    stop_timer(T_SCAFF_TO_FASTA);
    tprintf_flush("done in %.4f s\n", get_elapsed_time(T_SCAFF_TO_FASTA));
    serial_printf("done in %.4f s\n", get_elapsed_time(T_SCAFF_TO_FASTA));
    tprintf_flush("Number of short scaff ctgs %ld\n", num_short_scaff_contigs);
    long tot_neg_unclosed_gaps = reduce_long(num_neg_unclosed_gaps, UPC_ADD, SINGLE_DEST);
    serial_printf("Number of unclosed negative gaps: %ld (%.2f %% of unclosed gaps)\n",
                  tot_neg_unclosed_gaps, (double)tot_neg_unclosed_gaps * 100.0 / tot_nfailure);
    long tot_neg_gap_bases = reduce_long(num_neg_gap_bases, UPC_ADD, SINGLE_DEST);
    long tot_pos_gap_bases = reduce_long(num_pos_gap_bases, UPC_ADD, SINGLE_DEST);
    long tot_gap_bases = tot_neg_gap_bases + tot_pos_gap_bases;
    serial_printf("Number of unclosed gap bases: %ld (%.2f %% pos, %.2f %% neg)\n",
                  tot_gap_bases, (100.0 * tot_pos_gap_bases) / tot_gap_bases,
                  (100.0 * tot_neg_gap_bases) / tot_gap_bases);
    long tot_num_ns = reduce_long(stats->num_ns, UPC_ADD, SINGLE_DEST);
    serial_printf("Number of Ns: %ld (%.2f %% of unclosed bases)\n",
                  tot_num_ns, (100.0 * tot_num_ns) / tot_gap_bases);
#ifdef PARTIAL_GAP_CLOSURE
    long tot_bases_filled = reduce_long(num_bases_filled, UPC_ADD, SINGLE_DEST);
    serial_printf("Partial walks filled %ld unclosed bases (%.2f %%)\n",
                  tot_bases_filled, (100.0 * tot_bases_filled) / tot_gap_bases);
    stats->num_ns -= num_bases_filled;
#endif

    long tot_ngaps = reduce_long(ngaps, UPC_ADD, SINGLE_DEST);
    long tot_ngaps_closed = reduce_long(ngaps_closed, UPC_ADD, SINGLE_DEST);
    long tot_ngaps_closed_spans = reduce_long(ngaps_closed_spans, UPC_ADD, SINGLE_DEST);
    long tot_ngaps_unclosed_spans = reduce_long(ngaps_unclosed_spans, UPC_ADD, SINGLE_DEST);
    serial_printf("NGAPS %ld, closed %ld, closed spans %ld, unclosed spans %ld\n",
                  tot_ngaps, tot_ngaps_closed, tot_ngaps_closed_spans, tot_ngaps_unclosed_spans);
}


int merauder_main(int argc, char **argv)
{
    if (argc == 1) {
        return 0;
    }

    upc_tick_t start_time = upc_ticks_now();
    LOG_VERSION;

    _nwalks = 0;
    _nsuccess = 0;
    _nfailure = 0;
    _max_gap_time = 0.0;
    _max_kmer_freqs = 0;

    set_log_prefix("gapclosing");

    init_timers();
    START_TIMER(T_OVERALL);
    UPC_TIMED_BARRIER;
    gapclosing_get_config(argc, argv, &_cfg);
#ifdef CONFIG_SANITY_CHECK
    tprintf_flush("  sanity checking enabled\n");
#endif
//#ifdef CONFIG_CHECK_SEQS
//    tprintf_flush("  checking final sequences for illegal characters\n");
//#endif
#ifdef PARTIAL_GAP_CLOSURE
    serial_printf("Partial gap closing enabled\n");
#endif

    shared gap_set_t *gaps = NULL;
    UPC_ALL_ALLOC_CHK(gaps, THREADS, sizeof(gap_set_t));
    init_globals(&_cfg, gaps);
    int64_t ngaps_block = process_scaffold_report_file();
    gc_process_contigs_file(_cfg.poly_mode);  // TODO: replace with a flag signifying high-heterozygosity (diploid_mode 2)

    // guess 50 reads per gap - will be dynamically increased if needed
    read_list_t read_list = { .num = 0, .size = 50 * ngaps_block };
    read_list.reads = calloc_chk(read_list.size, sizeof(read_orient_t));
    serial_printf("Preprocessing meraligner files to count total reads per gap\n");
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        gc_process_meraligner_file(_cfg.library_names[i], _cfg.reverse_complement[i],
                                   _cfg.insert_size[i], _cfg.insert_sigma[i],
                                   _cfg.five_prime_wiggle_room[i], _cfg.three_prime_wiggle_room[i], &read_list);
    }
    serial_printf("Finished processing all meraligner files, starting to process read files\n");

    place_reads_in_dhtable(&read_list);
    serial_printf("Finished placing all reads to the gaps they fill\n");

    int64_t num_reads = 0;
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        num_reads += gc_process_fastq_file(_cfg.library_names[i], i);
    }

    finish_fastq_files(num_reads);

    if (_cfg.print_gaps) {
        print_gaps();
    }
    if (!MYTHREAD) {
        int64_t tot_ngaps = 0;
        for (int i = 0; i < THREADS; i++) {
            tot_ngaps += gaps[i].ngaps;
        }
        serial_printf("Attempting to close %lld gaps...\n", (lld)tot_ngaps);
    }

    // shuffle the gaps by estimated difficulty -- gapi is now invalid
    sort_gaps_by_difficulty();

#ifdef CONFIG_SHOW_PROGRESS
    int tick_size = (gaps[MYTHREAD].ngaps + 10 - 1) / 10;
    if (tick_size <= 0) tick_size = 1;
    int last_tick = -1;
#endif
    char closures_fname[MAX_FILE_PATH * 2];
    sprintf(closures_fname, "closures.out" GZIP_EXT);
    GZIP_FILE closures_file = NULL;
    if (_cfg.print_closures) {
        GZIP_FILE closures_file = openCheckpoint(closures_fname, "w");
    }
    gap_report_t *report = calloc_chk(1, sizeof(gap_report_t));
    batch_of_gaps_t my_batch; memset(&my_batch, 0, sizeof(my_batch));
    while ( get_next_batch_of_gaps( &my_batch ) ) {
        close_gaps(my_batch.gaps, my_batch.ngaps, report);
#ifdef CONFIG_SHOW_PROGRESS
        if (gaps[MYTHREAD].nprocessed > last_tick * tick_size) {
            int new_tick = gaps[MYTHREAD].nprocessed / tick_size;
            if (new_tick > last_tick) {
                last_tick = new_tick;
                serial_printf("%lld ", (lld)last_tick);
            }
        }
#endif
        if (_cfg.print_closures) {
            print_closures(my_batch.gaps, my_batch.ngaps, closures_file);
        }
        free_primers(&my_batch);
    }

    // restore the original ordering of gaps so gapi is valid again
    unsort_gaps();

    report_gap_stats(report);
    free_chk(report);
    if (_cfg.print_closures) {
        closeCheckpoint(closures_file);
    }

    int tot_ngaps_closed = reduce_int(_nsuccess, UPC_ADD, SINGLE_DEST);
    int tot_ngaps_unclosed = reduce_int(_nfailure, UPC_ADD, SINGLE_DEST);
    int tot_gaps = tot_ngaps_closed + tot_ngaps_unclosed;
    double perc_failed = tot_gaps == 0 ? 0.0 : (double)tot_ngaps_unclosed / tot_gaps * 100.0;
    if (MYTHREAD == 0) {
        tprintf_flush("Total gaps closed %d (%d failed to close, %.2f %%)\n",
                      tot_ngaps_closed, tot_ngaps_unclosed, perc_failed);
    }
    serial_printf("Total gaps closed %d (%d failed to close, %.2f %%)\n",
                  tot_ngaps_closed, tot_ngaps_unclosed, perc_failed);
    ADD_DIAG("%d", "total_gaps_closed", tot_ngaps_closed);
    ADD_DIAG("%d", "total_gaps_failed_to_close", tot_ngaps_unclosed);
    assembly_stats_t stats = { 0 };
    UPC_TIMED_BARRIER;
    scaff_to_fasta(&stats, gaps, tot_gaps);
    calc_assembly_stats(&stats);
    UPC_ALL_FREE_CHK(stats.scaff_lens);

    free_gaps();
    gaps = NULL;

    UPC_TIMED_BARRIER;
    stop_timer(T_OVERALL);
    double t_all = get_timer_all_max(T_OVERALL);
    tprintf_flush("Peak memory usage: %d mb\n", get_max_mem_usage_mb());

    serial_printf("Elapsed time for all threads: %.4f s\n", t_all);

#ifdef PRINT_TIMERS
    print_timers(0);
#endif

    UPC_TIMED_BARRIER;
    //serial_printf("Overall reverse complement time is %f\n", rc_time);

    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]),
                  ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));

    return 0;
}
