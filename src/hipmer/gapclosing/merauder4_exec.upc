#include "merauder4.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("merauder");
    int ret = merauder_main(argc, argv);
    CLOSE_MY_LOG;
    return ret;
}
