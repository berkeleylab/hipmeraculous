#ifndef LOCALASSM_H_
#define LOCALASSM_H_
/*
 * Do a walk from a contig, using the reads projected to not have an aligned pair
 *
 * ------
 *
 * Need a contig_reads array that keeps a list of all the reads pertinent to a contig.
 * Need a dhtable that maps read names to contig ids (which are indexes into the contig array)
 *
 * Since the dhtable is upc shared memory, we need to size it beforehand, which means that we need
 * to count up the maximum number of read->contig mappings first. So we do a pass through the meraligner
 * file to compute this, then we allocate the dhtable.
 *
 * When processing meraligner, the association of a read with a contig is recorded, i.e. build up the
 * dhtable with read->contig mappings, and build up the contigs array (without the reads info, but
 * recording the number of reads for that contig). Note that a single read can map to multiple
 * contigs, but this is taken care of by putting them directly in the dhtable, because it records
 * duplicates.
 *
 * Then process fastq and for each read name, if the read is in the dhtable, then save the data
 * to the contig entry in the contigs array (each contig has a list of reads)
 *
 * Then throw away the dhtable.
 *
 * Then process contigs file to get actual value for contigs. It may be that we only need to store
 * some fraction of the contig (primer) rather than the whole thing.
 *
 * Then use the contigs array to do the walks.
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <upc.h>
#include <upc_tick.h>
#include <upc_nb.h>

#include "localassm_timerdefs.h"

#include "upc_common.h"
#include "common.h"
#include "utils.h"
#include "dhtable.h"
#include "timers.h"
#include "tracing.h"

#include "localassm_config.h"
#include "contigs.h"
#include "alignments.h"
#include "fastq.h"
#include "walk.h"

#define SHOW_MEMORY
//#define CHECK_READ_DATA_HASH

int localassm_main(int argc, char **argv);

#endif // LOCALASSM_H_
