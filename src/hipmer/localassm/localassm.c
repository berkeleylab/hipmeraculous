/*
 * Do a walk from a contig, using the reads projected to not have an aligned pair
 *
 * ------
 *
 * Need a contig_reads array that keeps a list of all the reads pertinent to a contig.
 * Need a dhtable that maps read names to contig ids (which are indexes into the contig array)
 *
 * Since the dhtable is upc shared memory, we need to size it beforehand, which means that we need
 * to count up the maximum number of read->contig mappings first. So we do a pass through the meraligner
 * file to compute this, then we allocate the dhtable.
 *
 * When processing meraligner, the association of a read with a contig is recorded, i.e. build up the
 * dhtable with read->contig mappings, and build up the contigs array (without the reads info, but
 * recording the number of reads for that contig). Note that a single read can map to multiple
 * contigs, but this is taken care of by putting them directly in the dhtable, because it records
 * duplicates.
 *
 * Then process fastq and for each read name, if the read is in the dhtable, then save the data
 * to the contig entry in the contigs array (each contig has a list of reads)
 *
 * Then throw away the dhtable.
 *
 * Then process contigs file to get actual value for contigs. It may be that we only need to store
 * some fraction of the contig (primer) rather than the whole thing.
 *
 * Then use the contigs array to do the walks.
 *
 *
 */

#include "localassm.h"

// to create the hash table for reads, multiplied by the number of reads - gives approx load factor
#define READ_HASH_TABLE_LOAD_FACTOR 0.2

static void put_reads_in_dhtable(read2contig_set_t *read2contig_set)
{
    tprintf_flush("Putting %ld reads into distributed hash table\n", read2contig_set->num);
    long tot_read2contigs = reduce_long(read2contig_set->num, UPC_ADD, ALL_DEST);
    serial_printf("Putting %ld read-to-contig mappings into distributed hash table\n",
                  tot_read2contigs);
    ADD_DIAG("%ld", "read_to_contig_mappings", tot_read2contigs);
    dhtable_init(tot_read2contigs, READ_HASH_TABLE_LOAD_FACTOR);
    UPC_TIMED_BARRIER;

    // now put all the reads into the dhtable
    START_TIMER(T_PUT_READS);
    long num_put_failures = 0;
    long num_puts = 0;
    char name_buf[MAX_READ_NAME_LEN + 10];
    for (int i = 0; i < read2contig_set->num; i++) {
        num_puts++;
        read2contig_t *read2contig = &(read2contig_set->read2contigs[i]);
        sprintf(name_buf, "%c%c%s/%c", read2contig->contig_side, read2contig->strand,
                read2contig->read_name, read2contig->pair_num);
        if (!dhtable_put(name_buf, read2contig->contig_id)) {
            num_put_failures++;
        }
    }
    if (read2contig_set->read2contigs != NULL) {
        free_chk(read2contig_set->read2contigs);
    }

    long tot_num_put_failures = reduce_long(num_put_failures, UPC_ADD, ALL_DEST);
    if (tot_num_put_failures) {
        long tot_num_puts = reduce_long(num_puts, UPC_ADD, SINGLE_DEST);
        double perc_fails = (100.0 * tot_num_put_failures) / tot_num_puts;
        if (!MYTHREAD && perc_fails >= 5) {
            serial_printf("\n");
            WARN("\nDistributed hash table had %ld put failures out of %ld puts (%.2f %%).\n"
                 "[Indicates potential loss of efficiency and possibly fewer walks]\n",
                 tot_num_put_failures, tot_num_puts, perc_fails);
        }
    }
    stop_timer(T_PUT_READS);
#ifdef CHECK_READ_DATA_HASH
    dhtable_check();
#endif
    UPC_TIMED_BARRIER;
}

int localassm_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    set_log_prefix("localassm");

    init_timers();
    START_TIMER(T_OVERALL);
    localassm_get_config(argc, argv);

    serial_printf("Processing %lld contigs\n", (lld)_cfg.max_contigs);

#ifdef CONFIG_CHECK_WALK_WITH_READS
    serial_printf("WARNING: Checking resulting walks against reads (this is very slow)\n");
#endif
#ifdef CONFIG_DBG_WALKS
    serial_printf("WARNING: Debugging walks (lots of output!)\n");
#endif

    init_contigs();

    process_contigs_file();
    print_memory_used();

    // we process all the meraligner files first, and then all the fastq files.
    // this is a simpler approach and does not involve reallocating shared data
    // for the contigs, but it does use more memory
    read2contig_set_t read2contig_set = { .read2contigs = NULL, .num = 0, .max = 0 };
    START_TIMER(T_PROCESS_MERALIGNER);
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        process_meraligner_file(&read2contig_set, _cfg.lib_names[i], _cfg.revcomp[i],
                                _cfg.insert_size[i], _cfg.insert_sigma[i],
                                _cfg.fp_wiggle[i], _cfg.tp_wiggle[i]);
    }
    UPC_TIMED_BARRIER;
    stop_timer(T_PROCESS_MERALIGNER);
    print_memory_used();
    serial_printf("Process meraligner files took %.3f s\n", get_elapsed_time(T_PROCESS_MERALIGNER));

    long ncontig_sides = 0;
    for (int64_t i = MYTHREAD; i < _cfg.max_contigs; i += THREADS) {
        if (_contigs[i].reads_start.nreads_alns) {
            ncontig_sides++;
        }
        if (_contigs[i].reads_end.nreads_alns) {
            ncontig_sides++;
        }
    }
    tprintf("Can walk %lld contig sides out of %lld ( %.3f fraction)\n",
            (lld)ncontig_sides, (lld)_cfg.max_contigs * 2 / THREADS,
            (double)ncontig_sides / _cfg.max_contigs / 2 / THREADS);
    UPC_TIMED_BARRIER;
    long tot_ncontig_sides = reduce_long(ncontig_sides, UPC_ADD, SINGLE_DEST);
    serial_printf("Can walk %lld contig sides out of %lld ( %.3f fraction)\n",
                  (lld)tot_ncontig_sides, (lld)_cfg.max_contigs * 2,
                  (double)tot_ncontig_sides / _cfg.max_contigs / 2);
    ADD_DIAG("%ld", "contig_sides_to_walk", tot_ncontig_sides);

    put_reads_in_dhtable(&read2contig_set);
    print_memory_used();

    alloc_contigs();
    print_memory_used();

    START_TIMER(T_PROCESS_FASTQ);
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        process_fastq_file(_cfg.lib_names[i], i);
    }
    UPC_TIMED_BARRIER;
    stop_timer(T_PROCESS_FASTQ);
    print_memory_used();
    serial_printf("Process FASTQ files took %.3f s\n", get_elapsed_time(T_PROCESS_FASTQ));

    dhtable_free();

#ifdef CONFIG_SANITY_CHECK
    check_contig_read_counts();
#endif

    // now walk all the contigs, may have to load balance them first?
    walk_contigs();

    destroy_contigs();
    print_memory_used();

    stop_timer(T_OVERALL);
    double t_all = get_timer_all_max(T_OVERALL);
    serial_printf("Elapsed time for all threads: %.4f s\n", t_all);
    fflush(stdout);
    UPC_TIMED_BARRIER;
    print_timers(0);
    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]),
                  ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    localassm_free_config();

    return EXIT_SUCCESS;
}
