#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <sys/time.h>
#include <upc.h>
#include <upc_tick.h>

#include "localassm_timerdefs.h"

#include "upc_common.h"
#include "common.h"
#include "utils.h"
#include "timers.h"
#include "tracing.h"
#include "htable.h"

#include "localassm_config.h"
#include "contigs.h"
#include "alignments.h"
#include "fastq.h"
#include "walk.h"
#include "upc_output.h"

//#define CONFIG_DBG_ANALYZE_MER_FREQS
//#define CHECK_CHARS_IN_OUTPUT
//#define CONFIG_CHECK_WALK_WITH_READS
//#define CONFIG_DBG_WALKS
#ifdef CONFIG_DBG_WALKS
#define DBG_WALKS(...) dbg(__VA_ARGS__)
#else
#define DBG_WALKS(...)
#endif

#define FASTA_LINE_WIDTH 50

#define DIRN_RIGHT 0
#define DIRN_LEFT 1

typedef struct {
    char *mer;
    // freqs for all reasonable quality bases
    int   base_freqs[4];
    // freqs for high quality bases
    int   base_freqs_hi_q[4];
} mer_freqs_t;

typedef struct {
    char base;
    int  rating;
    int  nvotes_hi_q;
    int  nvotes;
} base_qual_t;

typedef struct {
    char *mer;
    char  base;
    int   nvotes;
    char  fork_bases[4];
    int   fork_nvotes[4];
} mer_base_t;

DEFINE_HTABLE_TYPE(mer_freqs);
DEFINE_HTABLE_TYPE(mer_base);
DEFINE_HTABLE_TYPE(int32)

static const char BASES[4] = { 'A', 'C', 'G', 'T' };
static int _max_walk_len = 0;
static int _nforks = 0, _nterms = 0, _nrepeats = 0;

static void print_fasta(GZIP_FILE f, int64_t contig_id, char *seq, int seq_len)
{
    assert(strlen(seq) == seq_len);
    GZIP_PRINTF(f, ">Contig_%lld\n", (lld)contig_id);
    GZIP_FWRITE(seq, 1, seq_len, f);
    GZIP_PRINTF(f, "\n");
}

static inline int get_base_index(char base)
{
    switch (base) {
    case 'A': return 0;
    case 'C': return 1;
    case 'G': return 2;
    case 'T': return 3;
    }
    return -1;
}

// try to get substring without N's that's at least max_len long
static int get_valid_base_str(char *s, int max_len, int *start, int *len)
{
    *start = 0;
    int first_N = 0;
    for (int j = 0; j < *len; j++) {
        if (s[j] == 'N') {
            first_N = j;
            if (j - *start >= max_len) {
                *len = j - *start;
                break;
            } else {
                while (s[j] == 'N') {
                    j++;
                }
                if (j < *len - 1) {
                    *start = j;
                } else {
                    *len = first_N - *start;
                }
            }
        }
        if (j == *len - 1) {
            *len -= *start;
        }
    }
    if (*len >= max_len) {
        return 1;
    } else {
        return 0;
    }
}

// split reads into kmers and count frequency of medium and high quality extensions
static void compute_mer_freqs(int mer_len, read_block_t *read_block, htable_t mer_freqs_htable)
{
    START_TIMER(T_COMPUTE_FREQS);
    int htable_puts = 0;
    int max_possible_mers = 0;
    for (int i = 0; i < read_block->nreads; i++) {
        int start = 0;
        int nts_len = GET_READ_LEN(read_block->reads, i);
        if (mer_len >= nts_len) {
            continue;
        }

        int subseq_len = nts_len;
        int subseq_pos = 0;
        char *read_nts = GET_READ_SEQ(read_block->reads, i);
        char *read_quals = GET_READ_QUAL(read_block->reads, i);
#ifdef CONFIG_SANITY_CHECK
        if (nts_len != strlen(read_nts)) {
            DIE("nts_len in read block mismatch with actual len: %d != %lld\n",
                nts_len, (lld)strlen(read_nts));
        }
#endif
        // in the while loop, we split the read into substrings without Ns that
        // are at least mer_len long
        while (get_valid_base_str(read_nts + subseq_pos, mer_len + 1, &start, &subseq_len)) {
            char *subseq_nts = read_nts + subseq_pos + start;
            char *subseq_quals = read_quals + subseq_pos + start;
            // iterate through all mers in subsequence, finding quality of extension for each mer
            for (int j = 0; j < subseq_len - mer_len; j++) {
                max_possible_mers++;
                char *mer = strndup_chk0(subseq_nts + j, mer_len);
                mer_freqs_t *mer_freqs = htable_get_mer_freqs(mer_freqs_htable, mer);
                if (!mer_freqs) {
                    mer_freqs = calloc_chk0(1, sizeof(mer_freqs_t));
                    mer_freqs->mer = mer;
                    CHECK_ERR(htable_put_mer_freqs(mer_freqs_htable, mer, mer_freqs, NO_CHECK_DUPS));
                    htable_puts++;
                } else {
                    free_chk0(mer);
                }
                int offs = j + mer_len;
                char extension = subseq_nts[offs];
                int bi = get_base_index(extension);
                if (bi < 0) {
                    DIE("\nj %d k %d sublen %d Invalid base at %d: %d\nread (%lld): %s\n"
                        "subseq (%lld): %s\n",
                        j, mer_len, subseq_len, offs, extension, (lld)strlen(read_nts), read_nts,
                        (lld)strlen(subseq_nts), subseq_nts);
                }
                int q = subseq_quals[offs] - _cfg.qual_offset;
                // ignore low quality bases
                if (q >= _cfg.min_qual) {
                    mer_freqs->base_freqs[bi]++;
                }
                // high quality
                if (q > _cfg.min_hi_qual) {
                    mer_freqs->base_freqs_hi_q[bi]++;
                }
            }
            subseq_pos += (start + subseq_len);
            if (subseq_pos >= nts_len) {
                break;
            }
            subseq_len = nts_len - subseq_pos;
        }
    }
    stop_timer(T_COMPUTE_FREQS);
    DBG_WALKS("In compute mer freqs for mer len %d, put %d mers into the table, max possible %lld\n",
              mer_len, htable_puts, (lld) max_possible_mers);
}

static int rate_base_ext(base_qual_t *base_qual, int contig_depth)
{
    // 0 = No votes
    // 1 = One vote
    // 2 = nVotes < minViable
    // 3 = minDepth > nVotes >= minViable, nHiQ < minViable
    // 4 = minDepth > nVotes >= minViable ; nHiQ >= minViable
    // 5 = nVotes >= minDepth ; nHiQ < minViable
    // 6 = nVotes >= minDepth ; minViable < nHiQ < minDepth
    // 7 = nHiQ >= minDepth

    // make thresholds dependent on contig depth
    double min_viable = _cfg.min_viable_depth * contig_depth;

    if (min_viable < 2) min_viable = 2;
    
    double min_expected_depth = _cfg.min_expected_depth * contig_depth;
    if (min_expected_depth < 2) min_expected_depth = 2;

    if (base_qual->nvotes == 0) {
        return 0;
    }
    if (base_qual->nvotes == 1) {
        return 1;
    }
    if (base_qual->nvotes < min_viable) {
        return 2;
    }
    if (base_qual->nvotes < min_expected_depth || base_qual->nvotes == min_viable) {
        if (base_qual->nvotes_hi_q < min_viable) {
            return 3;
        } else {
            return 4;
        }
    }
    if (base_qual->nvotes_hi_q < min_viable) {
        return 5;
    }
    if (base_qual->nvotes_hi_q < min_expected_depth) {
        return 6;
    }
    return 7;
}

static inline int cmp_quals(base_qual_t *q1, base_qual_t *q2)
{
    if (q1->rating > q2->rating) {
        return 1;
    }
    if (q1->rating < q2->rating) {
        return -1;
    }
    if (q1->nvotes_hi_q > q2->nvotes_hi_q) {
        return 1;
    }
    if (q1->nvotes_hi_q < q2->nvotes_hi_q) {
        return -1;
    }
    if (q1->nvotes > q2->nvotes) {
        return 1;
    }
    if (q1->nvotes < q2->nvotes) {
        return -1;
    }
    return 0;
}

static void analyze_mer_freqs(htable_t mer_freqs_htable, htable_t mers_htable, int contig_depth)
{
    START_TIMER(T_ANALYZE_FREQS);
    //Full analysis of quality/frequency profile
    mer_freqs_t *mer_freqs;
    htable_iter_t iter = htable_get_iter(mer_freqs_htable);
    base_qual_t base_quals[4];
    DBG_WALKS("Analyzing mer frequencies for %lld entries\n", (lld) htable_num_entries(mer_freqs_htable));
    // iterate through all the mers
    while ((mer_freqs = htable_get_next_mer_freqs(mer_freqs_htable, iter)) != NULL) {
        for (int i = 0; i < 4; i++) {
            base_quals[i].base = BASES[i];
            base_quals[i].nvotes = mer_freqs->base_freqs[i];
            base_quals[i].nvotes_hi_q = mer_freqs->base_freqs_hi_q[i];
            base_quals[i].rating = rate_base_ext(&base_quals[i], contig_depth);
        }

        // sort bases in order of quality, from best to worst
        for (int i = 1; i < 4; i++) {
            for (int k = i; k > 0 && cmp_quals(&base_quals[k], &base_quals[k - 1]) > 0; k--) {
                base_qual_t tmp = base_quals[k];
                base_quals[k] = base_quals[k - 1];
                base_quals[k - 1] = tmp;
            }
        }
#ifdef CONFIG_DBG_ANALYZE_MER_FREQS
        DBG_WALKS("mer: %s [", mer_freqs->mer);
        for (int b = 0; b < 4; b++) {
            DBG_WALKS("%c:%d/%d ", BASES[b], mer_freqs->base_freqs[b], mer_freqs->base_freqs_hi_q[b]);
        }
        DBG_WALKS("] <");
        for (int b = 0; b < 4; b++) {
            if (!base_quals[b].nvotes && !base_quals[b].nvotes_hi_q) {
                break;
            }
            DBG_WALKS("%c:%d/%d/%d ", base_quals[b].base, base_quals[b].nvotes,
                      base_quals[b].nvotes_hi_q, base_quals[b].rating);
        }
        DBG_WALKS(">");
#endif

        int top_rating = base_quals[0].rating;
        int runner_up = base_quals[1].rating;
        int top_rated_base = base_quals[0].base;
        mer_base_t *mer_base = calloc_chk0(1, sizeof(mer_base_t));
/*
 *      if (top_rating > 2) {
 *          if (top_rating > runner_up) {
 *              mer_base->base = top_rated_base;
 *              mer_base->nvotes = base_quals[0].nvotes;
 *          } else {
 *              int k = 0;
 *              for (int b = 0; b < 4; b++) {
 *                  if (base_quals[b].rating > 0) {
 *                      mer_base->fork_bases[k] = base_quals[b].base;
 *                      mer_base->fork_nvotes[k] = base_quals[b].nvotes;
 *                      k++;
 *                  } else {
 *                      break;
 *                  }
 *              }
 *          }
 *      }
 */
        // no extension (base = 0) if the runner up is close to the top rating
        // except, if rating is 7 (best quality), then all bases of rating 7 are forks
        if (top_rating > _cfg.rating_thres) {         // must have at least minViable bases
            if (top_rating <= 3) {    // must be uncontested
                if (runner_up == 0) {
                    mer_base->base = top_rated_base;
                }
            } else if (top_rating < 6) {
                if (runner_up < 3) {
                    mer_base->base = top_rated_base;
                }
            } else if (top_rating == 6) {  // viable and fair hiQ support
                if (runner_up < 4) {
                    mer_base->base = top_rated_base;
                }
            } else {                     // strongest rating trumps
                if (runner_up < 7) {
                    mer_base->base = top_rated_base;
                } else {
                    // runner up is also 7, at least 2-way fork
                    int k = 0;
                    for (int b = 0; b < 4; b++) {
                        if (base_quals[b].rating == 7) {
                            mer_base->fork_bases[k] = base_quals[b].base;
                            mer_base->fork_nvotes[k] = base_quals[b].nvotes;
                            k++;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (mer_base->base == top_rated_base) {
            mer_base->nvotes = base_quals[0].nvotes;
        }

        // only put in the hash table if it has a valid extension or fork
        if (mer_base->base || mer_base->fork_bases[0]) {
#ifdef CONFIG_DBG_ANALYZE_MER_FREQS
            DBG_WALKS(" (");
            if (mer_base->base) {
                DBG_WALKS("%c:%d", mer_base->base, mer_base->nvotes);
            }
            if (mer_base->fork_bases[0]) {
                DBG_WALKS(" F-");
            }
            for (int fi = 0; fi < 4; fi++) {
                if (!mer_base->fork_bases[fi]) {
                    break;
                }
                DBG_WALKS("%c:%d ", mer_base->fork_bases[fi], mer_base->fork_nvotes[fi]);
            }
            DBG_WALKS(")");
#endif
            mer_base->mer = strdup_chk0(mer_freqs->mer);
            CHECK_ERR(htable_put_mer_base(mers_htable, mer_base->mer, mer_base, CHECK_DUPS));
        } else {
            if (mer_base->mer) {
                free_chk0(mer_base->mer);
            }
            free_chk0(mer_base);
        }
#ifdef CONFIG_DBG_ANALYZE_MER_FREQS
        DBG_WALKS("\n");
#endif
    }
    free_chk0(iter);
    DBG_WALKS("Found %d valid kmers out of %d possibilities\n", htable_num_entries(mers_htable),
              htable_num_entries(mer_freqs_htable));
    stop_timer(T_ANALYZE_FREQS);
}

static void analyze_mer_freqs_NO_QUALS(htable_t mer_freqs_htable, htable_t mers_htable, int contig_depth)
{
    START_TIMER(T_ANALYZE_FREQS);
    //Full analysis of quality/frequency profile
    mer_freqs_t *mer_freqs;
    htable_iter_t iter = htable_get_iter(mer_freqs_htable);
    base_qual_t base_quals[4];
    DBG_WALKS("Analyzing mer frequencies for %lld entries\n", (lld) htable_num_entries(mer_freqs_htable));

    // iterate through all the mers
    while ((mer_freqs = htable_get_next_mer_freqs(mer_freqs_htable, iter)) != NULL) {
        int nbases = 0;
        mer_base_t *mer_base = calloc_chk0(1, sizeof(mer_base_t));
        for (int i = 0; i < 4; i++) {
            // must have an absolute frequency of at least 2
            if (mer_freqs->base_freqs[i] < 3) {
                continue;
            }
            double diff = abs(contig_depth - mer_freqs->base_freqs[i]);
            if (diff == 0) {
                diff = 1;
            }
            double dist = diff / contig_depth;
            if (dist < _cfg.depth_dist_thres) {
                base_quals[nbases].base = BASES[i];
                // the nvotes is determined by the closeness of the frequency to the contig depth
                base_quals[nbases].nvotes = 1.0 / dist;
                if (base_quals[nbases].nvotes < 1) {
                    base_quals[nbases].nvotes = 1;
                }
                nbases++;
            }
        }
        // sort bases in order of dist, from best to worst
        for (int i = 1; i < nbases; i++) {
            for (int k = i; k > 0 && (base_quals[k].nvotes > base_quals[k - 1].nvotes); k--) {
                base_qual_t tmp = base_quals[k];
                base_quals[k] = base_quals[k - 1];
                base_quals[k - 1] = tmp;
            }
        }
        // discard extra bases if they are far from the first one
        if (nbases > 1) {
            // sanity check
            if (base_quals[0].nvotes < base_quals[1].nvotes) {
                DIE("sort is wrong: %d < %d\n", base_quals[0].nvotes, base_quals[1].nvotes);
            }
            for (int i = 0; i < nbases; i++) {
                if (i && base_quals[0].nvotes > base_quals[i].nvotes * _cfg.nvote_diff_thres) {
                    nbases = i;
                    break;
                }
                mer_base->fork_bases[i] = base_quals[i].base;
                mer_base->fork_nvotes[i] = base_quals[i].nvotes;
            }
        }
        if (nbases == 1) {
            mer_base->base = base_quals[0].base;
            mer_base->nvotes = base_quals[0].nvotes;
        }
        if (nbases >= 1) {
            mer_base->mer = strdup_chk0(mer_freqs->mer);
            CHECK_ERR(htable_put_mer_base(mers_htable, mer_base->mer, mer_base, CHECK_DUPS));
        } else {
            if (mer_base->mer) {
                free_chk0(mer_base->mer);
            }
            free_chk0(mer_base);
        }
    }
    free_chk0(iter);
    DBG_WALKS("Found %d valid kmers out of %d possibilities\n", htable_num_entries(mers_htable),
              htable_num_entries(mer_freqs_htable));
    stop_timer(T_ANALYZE_FREQS);
}

#define SHIFT_MER(mer, base, len)                   \
    do {                                            \
        mer++;                                      \
        mer[len - 1] = base;                        \
        mer[len] = 0;                               \
        assert(strlen(mer) == mer_len);             \
    } while (0)

#define NBASE(b) ((b) == 0 ? ' ' : (b))
#define FORK_BASES_STR(b) NBASE(b[0]), NBASE(b[1]), NBASE(b[2]), NBASE(b[3])

// return the result of the walk (F, R or X)
static char walk_mers(htable_t mers_htable, char *walk, int *depth, int mer_len)
{
    START_TIMER(T_WALK_MERS);
    DBG_WALKS("walk for k %d, starting at primer %s\n", mer_len, walk);
    // the walk starts as the primer
    char *mer = walk;
    assert(strlen(mer) == mer_len);
    int have_forked = 0;
    int nsteps = 0;
    int tot_ext_votes = 0;
    htable_t loop_check_htable = create_htable(3500, "loop_check_htable");
    int val = 1;
    char walk_result = 'X';
    *depth = 0;
    while (1) {
        DBG_WALKS("%s\n", walk);
        // check for a cycle in the graph
        if (htable_get_int32(loop_check_htable, mer)) {
            walk_result = 'R';
            DBG_WALKS("Done after %d steps: repeated %s\n", nsteps, mer);
            // if repeated, remove the final mer, since that is what repeated
            int walk_len = strlen(walk);
            assert(walk_len >= mer_len);
            walk[walk_len - mer_len] = 0;
            // we actually walked one less step
            nsteps--;
            break;
        } else {
            CHECK_ERR(htable_put_int32(loop_check_htable, strdup_chk0(mer), &val, NO_CHECK_DUPS));
        }
        mer_base_t *next = htable_get_mer_base(mers_htable, mer);
        if (!next) {
            walk_result = 'X';
            DBG_WALKS("Done after %d steps: couldn't find follower for mer %s\n", nsteps, mer);
            // the final base is not part of the walk, since the mer couldn't be found
            if (nsteps) {
                mer[mer_len - 1] = 0;
            }
            break;
        }
        char ext = next->base;
        int nvotes = next->nvotes;
        if (!ext) {
            if (_cfg.poly_mode && !have_forked && next->fork_bases[0]) {
                //  Biallelic positions only (for now) maximum vote path is taken
                if (next->fork_bases[2]) {
                    walk_result = 'F';
                    DBG_WALKS("Done after %d steps: poly fork detected for %s [%c%c%c%c]\n",
                              nsteps, mer, FORK_BASES_STR(next->fork_bases));
                    break;
                }
                if (next->fork_nvotes[0] > next->fork_nvotes[1]) {
                    ext = next->fork_bases[0];
                    nvotes = next->fork_nvotes[0];
                } else {
                    ext = next->fork_bases[1];
                    nvotes = next->fork_nvotes[1];
                }
                //DBG_WALKS("Polymorphic conditions met .. attempting max frequency resolution.\n");
                have_forked = 1;
            } else {
                if (next->fork_bases[0]) {
                    DBG_WALKS("Done after %d steps: non-poly fork detected %s [%c%c%c%c]\n",
                              nsteps, mer, FORK_BASES_STR(next->fork_bases));
                    walk_result = 'F';
                } else {
                    DBG_WALKS("Done after %d steps: no extension for %s\n", nsteps, mer);
                    walk_result = 'X';
                }
                break;
            }
        }
        SHIFT_MER(mer, ext, mer_len);
        tot_ext_votes += nvotes;
        nsteps++;
        if (nsteps >= _max_walk_len)
            break;
    }
    destroy_htable(loop_check_htable, FREE_KEYS);
    if (nsteps > 0) {
        *depth = tot_ext_votes / nsteps;
    }
    if (nsteps < 0) {
        DIE("nsteps is negative %d, walk result %c\n", nsteps, walk_result);
    }
    if (*depth > 0 && !walk[0]) {
        DIE("walk depth is %d but there is no walk, nsteps %d, result %c\n", *depth, nsteps, walk_result);
    }
    stop_timer(T_WALK_MERS);
    return walk_result;
}

static void check_walk_with_reads(char *walk, int mer_len, read_block_t *read_block)
{
    DBG_WALKS("Checking walk against reads...");
    int walk_len = strlen(walk);
    // make sure each kmer in the walk comes from an aligned read
    for (int ki = 0; ki < walk_len - mer_len; ki++) {
        char *kmer = strndup_chk(walk + ki, mer_len);
        int found = 0;
        // now find the kmer in the reads
        for (int ri = 0; ri < read_block->nreads; ri++) {
            int nts_len = GET_READ_LEN(read_block->reads, ri);
            if (mer_len >= nts_len) {
                continue;
            }
            char *nts = GET_READ_SEQ(read_block->reads, ri);
            if (strstr(nts, kmer)) {
                found = 1;
                break;
            }
        }
        if (!found) {
            DIE("Could not find a matching read for kmer %s\n", kmer);
        }
        free_chk(kmer);
    }
    DBG_WALKS(" all kmers found in reads\n");
}

static char *iterative_walks(char *seq, int slen, int sdepth, read_block_t *read_block,
                             int *longest_walk_depth, const char *dirn)
{
    *longest_walk_depth = 0;
    if (!read_block->nreads) {
        return calloc_chk(1, 10);
    }

    START_TIMER(T_ITERATIVE_WALKS);
    DBG_WALKS(KLBLUE "%s walk: contig seq of len %d, num reads %d, depth %d:\n%s\n" KNORM,
              dirn, slen, read_block->nreads, sdepth, seq);
    // leave space for the primer too
    int min_mer_len = _cfg.min_mer_size;
    int max_mer_len = _cfg.max_mer_size < slen ? _cfg.max_mer_size : slen;
    // space for max walk plus primer
    char *walk = malloc_chk(_max_walk_len + max_mer_len + 1);
    DBG_WALKS("Max mer len is %d, max walk len is %d\n", max_mer_len, _max_walk_len + max_mer_len + 1);
    char walk_result = 'X';
    // iteratively walk starting from mer_size, increasing mer size on a fork (F) or repeat (R),
    // and decreasing on an end of path (X)
    // look for the longest walk. Note we have to restart from the beginning for each walk to ensure
    // that all loops will be detected
    char *longest_walk = malloc_chk(_max_walk_len + max_mer_len + 1);
    longest_walk[0] = 0;
    int longest_walk_len = 0;
    char longest_walk_result = 'X';
    const int SHIFT_SIZE = 8;
    int shift = 0;
    // estimate the max number of mers for the hash tables
    long max_mers = 2 * read_block->nreads * (_cfg.max_read_len - min_mer_len);
    if (max_mers <= 0) {
        DIE("Estimted maximum number of mers is negative!  max_read_len=%lld min_mer_len=%lld\n", (lld)_cfg.max_read_len, (lld)min_mer_len);
    }
    htable_t mer_freqs_htable = create_htable(max_mers, "mer_freqs_htable");
    htable_t mers_htable = create_htable(max_mers, "mers_htable");
    for (int mer_len = _cfg.mer_size; mer_len >= min_mer_len && mer_len <= max_mer_len; mer_len += shift) {
        compute_mer_freqs(mer_len, read_block, mer_freqs_htable);
#ifdef USE_QUALS
        analyze_mer_freqs(mer_freqs_htable, mers_htable, sdepth);
#else
        analyze_mer_freqs_NO_QUALS(mer_freqs_htable, mers_htable, sdepth);
#endif
        clear_htable(mer_freqs_htable, FREE_KEYS | FREE_VALS);
        // make sure the walk starts with the longest possible kmer
        strcpy(walk, seq + slen - mer_len);
        assert(strlen(walk) == mer_len);
        int depth = 0;
        walk_result = walk_mers(mers_htable, walk, &depth, mer_len);
        clear_htable(mers_htable, FREE_KEYS | FREE_VALS);
        int walk_len = strlen(walk) - mer_len;
        if (walk_len > longest_walk_len) {
            strcpy(longest_walk, walk + mer_len);
            longest_walk_len = walk_len;
            *longest_walk_depth = depth;
            longest_walk_result = walk_result;
            DBG_WALKS("Found longest walk: %d\n", longest_walk_len);
        }
/*
 *      if (walk_len != 0) {
 *          dbg("%s walk k %d ended with %c, length %d and depth %d:\n",
 *              dirn, mer_len, walk_result, walk_len, depth);
 *      }
 */
#ifdef CONFIG_DBG_WALKS
        DBG_WALKS("From k %d walk stopped after %d steps, %d depth, with result %c:\n%s\n",
                  mer_len, walk_len, depth, walk_result, walk + mer_len);
#endif
#ifdef CONFIG_CHECK_WALK_WITH_READS
        check_walk_with_reads(walk, mer_len, read_block);
#endif
        if (walk_result == 'X') {
            // walk reaches a dead-end, downshift, unless we were upshifting
            if (shift == SHIFT_SIZE) {
                break;
            }
            shift = -SHIFT_SIZE;
            DBG_WALKS("Downshifting k: %d -> %d\n", mer_len, mer_len + shift);
        } else {
            // otherwise walk must end with a fork or repeat, so upshift
            if (shift == -SHIFT_SIZE) {
                break;
            }
            if (mer_len > slen) {
                break;
            }
            shift = SHIFT_SIZE;
            DBG_WALKS("Upshifting k: %d -> %d\n", mer_len, mer_len + shift);
        }
    }
    destroy_htable(mer_freqs_htable, FREE_KEYS | FREE_VALS);
    destroy_htable(mers_htable, FREE_KEYS | FREE_VALS);
    free_chk(walk);
    if (longest_walk_len) {
        DBG_WALKS("%s walk ended with %c, and is of length %d and depth %d:\n%s\n",
                  dirn, longest_walk_result, strlen(longest_walk), *longest_walk_depth, longest_walk);
        if (longest_walk_result == 'F') {
            _nforks++;
        } else if (longest_walk_result == 'R') {
            _nrepeats++;
        } else {
            _nterms++;
        }
    }
    stop_timer(T_ITERATIVE_WALKS);
    return longest_walk;
}

static void flip_contig(local_contig_t *contig)
{
    switch_code(reverse(contig->seq));
    for (int i = 0; i < contig->reads_start.nreads; i++) {
        switch_code(reverse(GET_READ_SEQ(contig->reads_start.reads, i)));
        reverse(GET_READ_QUAL(contig->reads_start.reads, i));
    }
}

#define WALK_CONTIGS_NUM_VALS 8
void walk_contigs(void)
{
    START_TIMER(T_WALK_CONTIGS);
    serial_printf("Walking contigs...\n");

    _nforks = 0;
    _nterms = 0;
    _nrepeats = 0;

    // walk should never be more than this. Note we use the maximum insert size from all libraries
    int insert_size = 0;
    int insert_sigma = 0;
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        if (insert_size < _cfg.insert_size[i]) {
            insert_size = _cfg.insert_size[i];
            insert_sigma = _cfg.insert_sigma[i];
        }
    }
    _max_walk_len = insert_size + 2 * insert_sigma;
    serial_printf("Limiting walks to %d length\n", _max_walk_len);
    DBG_WALKS("Max walk len %d\n", _max_walk_len);

    serial_printf("Max contigs %lld\n", (lld) _cfg.max_contigs);
    char fname[MAX_FILE_PATH];
    sprintf(fname, "walks_%s.txt" GZIP_EXT, _cfg.contigs_file);
    GZIP_FILE f_walk = openCheckpoint(fname, "w");
    tprintf_flush("Walking contigs, output in %s\n", fname);
    sprintf(fname, "walks_%s.fasta" GZIP_EXT, _cfg.contigs_file);
    GZIP_FILE f_fasta = openCheckpoint(fname, "w");
    long nwalks = 0;
    long ncontigs = 0;
    int64_t ncontigs10perc = _cfg.max_contigs / THREADS / 10;
    int ticks_made = 0;
    long sum_clen = 0;
    long sum_ext = 0;
    long sum_ext_clen = 0;
    int longest_walk_len = 0;

    const int64_t BLOCK_SIZE = _cfg.max_contigs / THREADS / 10 + 1;
    serial_printf("Block size is %lld\n", (lld) BLOCK_SIZE);
    shared int64_t *next_contig = NULL;
    UPC_ALL_ALLOC_CHK(next_contig, 1, sizeof(int64_t));
    if (!MYTHREAD) {
        *next_contig = BLOCK_SIZE * THREADS;
    }
    UPC_TIMED_BARRIER;

    int64_t contigIdxStart = BLOCK_SIZE * MYTHREAD;
    //fprintf(stderr, "THread %d starting at %lld\n", MYTHREAD, (lld) contigIdxStart);
    upc_tick_t t_max_walk = 0;
    int64_t max_walk_nreads = 0;
    int64_t tot_nreads = 0;
    while (contigIdxStart < _cfg.max_contigs) {
        for (int64_t contigIdx = contigIdxStart;
             contigIdx < contigIdxStart + BLOCK_SIZE && contigIdx < _cfg.max_contigs; contigIdx++) {
            _contigs[contigIdx].walked++;
            int left_depth = 0, right_depth = 0;
            ncontigs++;
            if (ncontigs && ncontigs10perc && !(ncontigs % ncontigs10perc)) {
                tprintf_flush("%lld ", (lld) ncontigs / ncontigs10perc);
                serial_printf("%lld ", (lld) ncontigs / ncontigs10perc);
                if (!MYTHREAD) {
                    fflush(stdout);
                }
                ticks_made++;
            }
            local_contig_t *local_contig = get_local_contig(contigIdx);
            if (!local_contig) {
                continue;
            }

            sum_clen += local_contig->slen;

            upc_tick_t t_walk_start = upc_ticks_now();

            // have to do right first because the contig needs to be flipped for the left
            char *right_walk = iterative_walks(local_contig->seq, local_contig->slen, local_contig->depth,
                                               &local_contig->reads_end, &right_depth, "right");
            int right_walk_len = strlen(right_walk);
            if (right_walk_len) {
                nwalks++;
                if (longest_walk_len < right_walk_len) {
                    longest_walk_len = right_walk_len;
                }
            }

            flip_contig(local_contig);
            char *left_walk = iterative_walks(local_contig->seq, local_contig->slen, local_contig->depth,
                                              &local_contig->reads_start, &left_depth, "left");

            upc_tick_t t_walk_elapsed = upc_ticks_now() - t_walk_start;
            if (t_walk_elapsed > t_max_walk) {
                t_max_walk = t_walk_elapsed;
                max_walk_nreads = local_contig->reads_start.nreads + local_contig->reads_end.nreads;
            }
            tot_nreads += local_contig->reads_start.nreads + local_contig->reads_end.nreads;

            flip_contig(local_contig);
            switch_code(reverse(left_walk));
            int left_walk_len = strlen(left_walk);
            if (left_walk_len) {
                nwalks++;
                if (longest_walk_len < left_walk_len) {
                    longest_walk_len = left_walk_len;
                }
            }

            if (left_walk_len || right_walk_len) {
                sum_ext_clen += local_contig->slen;
            }

            if (!right_walk_len) {
                strcpy(right_walk, "N");
                right_walk_len = strlen(right_walk);
                if (right_depth) {
                    DIE("right depth is %d and should be 0\n", right_depth);
                }
            } else {
                if (!right_depth) {
                    DIE("right depth is 0\n");
                }
                sum_ext += strlen(right_walk);
            }
            if (!left_walk_len) {
                strcpy(left_walk, "N");
                left_walk_len = strlen(left_walk);
                if (left_depth) {
                    DIE("left depth is %d and should be 0\n", left_depth);
                }
            } else {
                if (!left_depth) {
                    DIE("left depth is 0\n");
                }
                sum_ext += strlen(left_walk);
            }
            DBG("Writing walks for Contig_%lld\n", (lld)contigIdx);
            assert(local_contig->slen == strlen(local_contig->seq));
            assert(left_walk_len == strlen(left_walk));
            assert(right_walk_len == strlen(right_walk));
            /* Spit the (gz)print and use GZIP_FWRITE for contigs which could be >8kb where gzprintf would fail */
            GZIP_PRINTF(f_walk, ">Contig_%lld\t%d\t%d\n",
                        (lld)contigIdx, left_depth, right_depth);
            GZIP_FWRITE(left_walk, 1, left_walk_len, f_walk);
            GZIP_PRINTF(f_walk, "\n");
            GZIP_FWRITE(local_contig->seq, 1, local_contig->slen, f_walk);
            GZIP_PRINTF(f_walk, "\n");
            GZIP_FWRITE(right_walk, 1, right_walk_len, f_walk);
            GZIP_PRINTF(f_walk, "\n");
            int ext_len = left_walk_len + local_contig->slen + right_walk_len;
            char *ext_seq = malloc_chk(ext_len + 1);
            ext_seq[0] = 0;
            strncat(ext_seq, left_walk, left_walk_len);
            strcat(ext_seq, local_contig->seq);
            strncat(ext_seq, right_walk, right_walk_len);
            print_fasta(f_fasta, contigIdx, ext_seq, ext_len);
            DBG("Freeing local contig\n");
            free_local_contig(&local_contig);
            free_chk(ext_seq);
            free_chk(left_walk);
            free_chk(right_walk);
        }
        UPC_ATOMIC_FADD_I64(&contigIdxStart, next_contig, BLOCK_SIZE);
        //fprintf(stderr, "Thread %d got block starting at %lld\n", MYTHREAD, (lld) contigIdxStart);
    }
    if (ticks_made) {
        tprintf_flush("\n");
        serial_printf("\n");
    }
    closeCheckpoint(f_walk);
    closeCheckpoint(f_fasta);
    stop_timer(T_WALK_CONTIGS);
    tprintf_flush("Found %lld walks in %.3f s, %lld number of contigs, %lld total contig length, "
                  "%lld total extension, %.3f fraction extended, max walk time %.5f s, max walk reads %lld "
                  "total reads %lld\n",
                  (lld) nwalks, get_elapsed_time(T_WALK_CONTIGS), (lld) ncontigs, (lld) sum_clen, (lld) sum_ext,
                  (double)(sum_clen + sum_ext) / sum_clen, TICKS_TO_S(t_max_walk), (lld) max_walk_nreads,
                  (lld) tot_nreads);

    START_TIMER(T_WALK_CONTIGS);
    UPC_TIMED_BARRIER;
    UPC_ALL_FREE_CHK(next_contig);
    stop_timer(T_WALK_CONTIGS);

#ifdef CONFIG_SANITY_CHECK
    for (int64_t contigIdx = 0; contigIdx < _cfg.max_contigs; contigIdx += THREADS) {
        if (_contigs[contigIdx].walked != 1) {
            WARN("Contig %lld walk is incorrect: %d\n", (lld) _contigs[contigIdx].walked);
        }
    }
#endif

    double myvals[WALK_CONTIGS_NUM_VALS] = { nwalks,  sum_ext, sum_clen, sum_ext_clen, longest_walk_len, _nterms,
                                             _nforks, _nrepeats };
    double min_vals[WALK_CONTIGS_NUM_VALS], max_vals[WALK_CONTIGS_NUM_VALS], tot_vals[WALK_CONTIGS_NUM_VALS];

    reduce_block(myvals, WALK_CONTIGS_NUM_VALS, min_vals, max_vals, tot_vals);

    if (!MYTHREAD) {
        long tot_nwalks = tot_vals[0];
        long tot_sum_ext = tot_vals[1];
        long tot_sum_clen = tot_vals[2];
        long tot_sum_ext_clen = tot_vals[3];
        int max_longest_walk_len = max_vals[4];

        long av_walk_len = 0;
        if (tot_nwalks) {
            av_walk_len = tot_sum_ext / tot_nwalks;
        }

        // compute the average walk length
        serial_printf("Done contig walks in %.3f s\nFound %lld walks, %lld total contig length, "
                      "%lld total extension, %lld total extended contig length, "
                      "%.3f fraction extended, av walk len %lld, max walk len %lld\n",
                      get_elapsed_time(T_WALK_CONTIGS), (lld) tot_nwalks, (lld) tot_sum_clen,
                      (lld) tot_sum_ext, (lld) tot_sum_ext_clen + tot_sum_ext,
                      (double)(tot_sum_clen + tot_sum_ext) / tot_sum_clen,
                      (lld) av_walk_len, (lld) max_longest_walk_len);

        int tot_nterms = tot_vals[5];
        int tot_nforks = tot_vals[6];
        int tot_nrepeats = tot_vals[7];

        serial_printf("Walk end results: %d X, %d F, %d R\n", tot_nterms, tot_nforks, tot_nrepeats);
        ADD_DIAG("%lld", "found_walks", (lld) tot_nwalks);
        ADD_DIAG("%lld", "contig_length", (lld) tot_sum_clen);
        ADD_DIAG("%lld", "extension_length", (lld) tot_sum_ext);
        ADD_DIAG("%lld", "av_walk_length", (lld) av_walk_len);
        ADD_DIAG("%d", "walk_terminations_X", tot_nterms);
        ADD_DIAG("%d", "walk_forks_F", tot_nforks);
        ADD_DIAG("%d", "walk_repeats_R", tot_nrepeats);
    }
}
