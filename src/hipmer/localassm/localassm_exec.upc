#include "localassm.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("localassm");
    int ret = localassm_main(argc, argv);
    CLOSE_MY_LOG;
    return ret;
}
