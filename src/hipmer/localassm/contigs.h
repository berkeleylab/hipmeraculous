#ifndef __CONTIGS_H
#define __CONTIGS_H

#include <assert.h>

#include "upc_common.h"
#include "Buffer.h"

extern Buffer _contigAllocs;

typedef struct {
    int32_t seqLen;
    uint8_t nameLen;
    uint8_t padding;
    char    nts_and_quals[2]; /* start of allocated memory to (seqLen+1)*2 + nameLen+1 + padding bytes, first len is seq (null terminated at len), quals start at len+1. if nameLen>0, then the name is null terminated after 2*len+2 bytes */
} __read_t;
typedef __read_t *read_ptr_t; /* opaque type, must always be allocated */
typedef shared [] __read_t *sh_read_ptr_t;


#define CONTIGS_PAD 8
#define READ_GET_DATA_LEN(seqLen, nameLen) (sizeof(__read_t) + 2 * ((seqLen) + 1) + (((nameLen) == 0) ? 0 : ((nameLen) + 1)))
#define READ_GET_PAD_LEN(totLen) ((((totlen) % CONTIGS_PAD) == 0) ? 0 : CONTIGS_PAD - ((totlen) % CONTIGS_PAD))

#define ALLOC_READ(read_ptr, name, seq, qual) do { \
        int32_t seqLen = strlen(seq), nameLen = strlen(name), padding = 0, totlen; \
        assert(nameLen < 255); \
        assert(seqLen == strlen(qual)); \
        totlen = READ_GET_DATA_LEN(seqLen, nameLen); \
        padding = READ_GET_PAD_LEN(totlen); \
        (read_ptr) = (read_ptr_t)calloc_chk0(1, totlen + padding); \
        (read_ptr)->seqLen = seqLen; \
        (read_ptr)->nameLen = nameLen; \
        (read_ptr)->padding = padding; \
        strcpy((read_ptr)->nts_and_quals, seq); \
        strcpy((read_ptr)->nts_and_quals + (seqLen + 1), qual); \
        if (nameLen) { strcpy((read_ptr)->nts_and_quals + 2 * (seqLen + 1), name); } \
} while (0)

#define ALLOC_READ_FROM_SHARED(read_ptr, sh_read_ptr) do { \
        assert((sh_read_ptr) != NULL); \
        __read_t tmpHeader = *(sh_read_ptr); \
        int32_t totlen = READ_GET_DATA_LEN(tmpHeader.seqLen, tmpHeader.nameLen) + tmpHeader.padding; \
        (read_ptr) = (read_ptr_t)malloc_chk0(totlen); \
        upc_memget((read_ptr), (sh_read_ptr), totlen); \
} while (0)

#define FREE_READ(readt) do { (readt)->seqLen = (readt)->nameLen = (readt)->padding = 0; free_chk0(readt); } while (0)

#define UPC_ALLOC_READ(sh_read_ptr, name, seq, qual) do { \
        assert((sh_read_ptr) == NULL); \
        read_ptr_t tmpRead; \
        ALLOC_READ(tmpRead, name, seq, qual); \
        int32_t totlen = READ_GET_DATA_LEN(tmpRead->seqLen, tmpRead->nameLen) + tmpRead->padding; \
        UPC_ALLOC_TRACK((sh_read_ptr), totlen, _contigAllocs); \
        upc_memput((sh_read_ptr), tmpRead, totlen); \
        FREE_READ(tmpRead); \
} while (0)

#define UPC_FREE_READ(sh_read_ptr) do { \
    UPC_FREE_TRACK(sh_read_ptr); \
} while (0)

typedef struct {
    // this value is accumulated during the reading of the FASTQ file
    int32_t nreads_alns;
    // this value is accumulated during the alignments processing
    int32_t nreads_fastq;
    shared [] sh_read_ptr_t * reads; /* shared array of sh_read_ptr_t (shared pointers themselves) */
} sh_read_block_t;

typedef struct {
    int32_t     nreads;
    read_ptr_t *reads; /* array of read_ptr_t (private pointers themselves) */
} read_block_t;

typedef struct {
    int32_t         slen;
    shared [] char *seq;
    int32_t         depth;
    sh_read_block_t reads_start;
    sh_read_block_t reads_end;
    int             walked;
} contig_t;

typedef struct {
    int32_t      slen;
    char *       seq;
    int32_t      depth;
    read_block_t reads_start;
    read_block_t reads_end;
    int          walked;
} local_contig_t;


#define GET_READ_LEN(reads, i) ((reads)[i]->seqLen)
#define GET_READ_NAMELEN(reads, i) ((reads)[i]->nameLen)
#define GET_READ_SEQ(reads, i) ((reads)[i]->nts_and_quals)
#define GET_READ_QUAL(reads, i) (GET_READ_SEQ(reads, i) + (GET_READ_LEN(reads, i) + 1))
#define GET_READ_NAME(reads, i) (GET_READ_NAMELEN(reads, i) > 0 ? GET_READ_SEQ(reads, i) + 2 * GET_READ_LEN(reads, i) + 2 : NULL)

#define GET_CONTIG_ID(contig) ((contig)[6] == '_' ? atol((contig) + 7) : atol((contig) + 6))

extern shared contig_t *_contigs;

void init_contigs(void);
void destroy_contigs(void);
void alloc_contigs(void);
void check_contig_read_counts(void);
void process_contigs_file(void);
local_contig_t *get_local_contig(int64_t contig_id);
void free_local_contig(local_contig_t **_contig);
void free_sh_contig(shared [] contig_t *contig);

#endif // __CONTIGS_H
