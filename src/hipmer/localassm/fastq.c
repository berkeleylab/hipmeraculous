#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <upc.h>
#include <upc_tick.h>
#include <upc_nb.h>

#include "localassm_timerdefs.h"

#include "../fqreader/fq_reader.h"
#include "Buffer.h"
#include "upc_common.h"
#include "dhtable.h"
#include "timers.h"
#include "tracing.h"
#include "utils.h"
#include "upc_output.h"

#include "localassm_config.h"
#include "contigs.h"

#define MAX_READ_CONTIG_IDS 100
// to create the hash table for reads, multiplied by the number of reads - gives approx load factor
#define READ_HASH_TABLE_LOAD_FACTOR 0.2

static void put_in_read_block(int64_t contig_id, char *name, shared [] sh_read_block_t *read_block,
                              int read_len, char *nts, char *quals, char contig_side, char orient)
{
    int32_t ri;

    UPC_ATOMIC_FADD_I32(&ri, &(read_block->nreads_fastq), 1);
    if (ri >= read_block->nreads_alns) {
        DIE("max reads exceeded for contig %lld, read %s, %d > %d, %c %c \n",
            (lld)contig_id, name, ri, read_block->nreads_alns, contig_side, orient);
    }
    if (read_block->reads == NULL) {
        DIE("Contig %lld not initialized properly\n", (lld)contig_id);
    }
    UPC_ALLOC_READ(read_block->reads[ri], name, nts, quals);
}

static int get_reads(char contig_side, char orient, char *name, char *nts, char *quals)
{
    char buf[MAX_READ_NAME_LEN + 2];

    sprintf(buf, "%c%c%s", contig_side, orient, name);
    int64_t contig_ids[MAX_READ_CONTIG_IDS];
    // the dhtable can have multiple enties per read, so we get them all in an array of gap ids
    int ncontig_ids = dhtable_get(buf, contig_ids, MAX_READ_CONTIG_IDS);
    if (!ncontig_ids) {
        return 0;
    }
    int read_len = strlen(nts);
#ifdef CONFIG_SANITY_CHECK
    if (read_len > _cfg.max_read_len) {
        DIE("Read length too great: %d > %d\n", read_len, _cfg.max_read_len);
    }
#endif
    START_TIMER(T_CONTIG_READ_COPY);
    for (int i = 0; i < ncontig_ids; i++) {
        int64_t id = contig_ids[i];
        shared contig_t *contig = &_contigs[id];
        DBG2("get_reads: READ %s, %c %c: %s\n", name, orient, contig_side, nts);
        if (contig_side == 'S') {
            put_in_read_block(id, name, &contig->reads_start, read_len, nts, quals, contig_side, orient);
        } else {
            put_in_read_block(id, name, &contig->reads_end, read_len, nts, quals, contig_side, orient);
        }
    }
    stop_timer(T_CONTIG_READ_COPY);
    return 1;
}

void process_fastq_file(char *lib_name, uint8_t libnum)
{
    // then when the fastq file is processed, simply look up the contig id in the dhtable
    // and use that to index into the contigs array, atomically incr the nreads and
    // reads len and then copy across the read sequence
    long nreads = 0, nreads_used = 0;
    Buffer id_buf = initBuffer(MAX_READ_NAME_LEN);
    Buffer seq_buf = initBuffer(1024), quals_buf = initBuffer(1024);

    // Get FileOfFileNames by library
    char fofn[MAX_FILE_PATH], fname[MAX_FILE_PATH];

    snprintf(fofn, MAX_FILE_PATH, "%s.fofn", lib_name);
    serial_printf("Processing fastq files from %s...\n", fofn);
    tprintf_flush("Processing fastq files from %s...\n", fofn);
    
    FILE *fofnFD = fopen_chk(fofn, "r");
    uint64_t read1ID = MYTHREAD, read2ID = MYTHREAD;
    while (fgets(fname, MAX_FILE_PATH, fofnFD)) {
        fname[strlen(fname) - 1] = '\0';

        fq_reader_t fqr = create_fq_reader();
        int reads_are_per_thread = _cfg.cached_io_reads;
        if (reads_are_per_thread) {
            if (!doesLocalCheckpointExist(fname)) {
                restoreCheckpoint(fname);
                char uncomp[MAX_FILE_PATH+30];
                sprintf(uncomp, "%s.uncompressedSize", fname);
                restoreCheckpoint(uncomp);
            }
        }
        open_fq(fqr, fname, reads_are_per_thread, _cfg.base_dir, reads_are_per_thread ? -1 : broadcast_file_size(fname));

        while (get_next_fq_record(fqr, id_buf, seq_buf, quals_buf)) {
            hexifyId(getStartBuffer(id_buf), libnum, &read1ID, &read2ID, THREADS);
            char *id = getStartBuffer(id_buf);
            char *seq = getStartBuffer(seq_buf);
            char *quals = getStartBuffer(quals_buf);
            int use_read = 0;
            // try both orientations and both sides of the contig
            use_read += get_reads('S', '+', id, seq, quals);
            use_read += get_reads('E', '+', id, seq, quals);
            // flip for '-' reads
            switch_code(reverse(seq));
            reverse(quals);
            use_read += get_reads('S', '-', id, seq, quals);
            use_read += get_reads('E', '-', id, seq, quals);
            if (use_read) {
                nreads_used++;
            }
            nreads++;
        }
        destroy_fq_reader(fqr);
    }
    fclose_track(fofnFD);

    freeBuffer(id_buf);
    freeBuffer(seq_buf);
    freeBuffer(quals_buf);

    tprintf_flush("In FASTQ: %ld reads found, used %ld ( %.3f fraction)\n",
                  nreads, nreads_used, (double)nreads_used / nreads);
    long tot_nreads_used = reduce_long(nreads_used, UPC_ADD, SINGLE_DEST);
    long tot_nreads = reduce_long(nreads, UPC_ADD, SINGLE_DEST);
    serial_printf("In FASTQ: %ld reads found, used %ld ( %.3f fraction)\n",
                  tot_nreads, tot_nreads_used, (double)tot_nreads_used / tot_nreads);
    UPC_TIMED_BARRIER;
}
