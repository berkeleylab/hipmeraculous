#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <upc.h>
#include <upc_tick.h>
#include <assert.h>
#include <zlib.h>

#include "localassm_timerdefs.h"

#include "upc_common.h"
#include "kseq.h"
#include "timers.h"
#include "tracing.h"
#include "utils.h"
#include "Buffer.h"
#include "upc_output.h"

#include "localassm_config.h"
#include "contigs.h"
#include "alignments.h"


shared contig_t *_contigs = NULL;
Buffer _contigAllocs = NULL;

void init_contigs(void)
{
    // contigs are allocated round-robin across threads to prevent hotspots
    UPC_ALL_ALLOC_CHK(_contigs, _cfg.max_contigs, sizeof(contig_t));
    for (int64_t i = MYTHREAD; i < _cfg.max_contigs; i += THREADS) {
        assert(upc_threadof(&(_contigs[i])) == MYTHREAD);
        _contigs[i].walked = 0;
        _contigs[i].slen = 0;
        _contigs[i].depth = 0;
        _contigs[i].seq = NULL;
        _contigs[i].reads_start.nreads_alns = 0;
        _contigs[i].reads_start.nreads_fastq = 0;
        _contigs[i].reads_start.reads = NULL;
        _contigs[i].reads_end.nreads_alns = 0;
        _contigs[i].reads_end.nreads_fastq = 0;
        _contigs[i].reads_end.reads = NULL;
    }
    _contigAllocs = initBuffer(8192);
    UPC_TIMED_BARRIER;
}

void destroy_contigs(void)
{
    for (int64_t i = MYTHREAD; i < _cfg.max_contigs; i += THREADS) {
        assert(upc_threadof(&(_contigs[i])) == MYTHREAD);
        free_sh_contig((shared [] contig_t *) & (_contigs[i]));
    }
    UPC_FREE_ALL_TRACKED(_contigAllocs);
    freeBuffer(_contigAllocs);
    _contigAllocs = NULL;
    UPC_ALL_FREE_CHK(_contigs);
}

void alloc_read_block(shared [] sh_read_block_t *read_block)
{
    if (read_block->nreads_alns) {
        DBG("Allocating nreads_alns %d read block alignments %0.3f MB\n", read_block->nreads_alns, ((double)read_block->nreads_alns) * (sizeof(sh_read_ptr_t)) / ONE_MB);
        UPC_ALLOC_TRACK(read_block->reads, read_block->nreads_alns * sizeof(*(read_block->reads)), _contigAllocs);
    } else {
        read_block->nreads_fastq = 0;
        read_block->reads = NULL;
    }
}

void alloc_contigs(void)
{
    for (int64_t i = MYTHREAD; i < _cfg.max_contigs; i += THREADS) {
        alloc_read_block(&(_contigs[i].reads_start));
        alloc_read_block(&(_contigs[i].reads_end));
    }
    UPC_TIMED_BARRIER;
}

void check_contig_read_counts(void)
{
    long missing_nreads = 0;
    long nreads = 0;

    UPC_TIMED_BARRIER;
    for (int64_t i = MYTHREAD; i < _cfg.max_contigs; i += THREADS) {
        shared contig_t *contig = &_contigs[i];
        missing_nreads += (contig->reads_start.nreads_alns - contig->reads_start.nreads_fastq);
        nreads += contig->reads_start.nreads_alns;
        missing_nreads += (contig->reads_end.nreads_alns - contig->reads_end.nreads_fastq);
        nreads += contig->reads_end.nreads_alns;
    }
    if (missing_nreads) {
        double perc_missing = (100.0 * missing_nreads) / nreads;
        if (perc_missing > 5) {
            tprintf_flush("WARNING: found %ld (%.2f %%) reads in meraligner files that are either "
                          "not in FASTQ files or not in hash table\n",
                          missing_nreads, perc_missing);
        }
    }
    UPC_TIMED_BARRIER;
}

void process_contigs_file(void)
{
    START_TIMER(T_PROCESS_CONTIGS);
    long ncontigs = 0;

    char fname[MAX_FILE_PATH];
    sprintf(fname, "%s.fasta" GZIP_EXT, _cfg.contigs_file);
    serial_printf("Processing %s... ", fname);
    tprintf_flush("Processing %s... ", fname);

    gzFile fastaf = openCheckpoint1(fname, "r");
    if (!fastaf) {
        DIE("Could not open %s\n", fname);
    }
    kseq_t *ks = kseq_init(fastaf);
    while (kseq_read(ks) >= 0) {
        int64_t contig_id = GET_CONTIG_ID(ks->name.s);
        if (contig_id >= _cfg.max_contigs || contig_id < 0) {
            DIE("Error: contig id %lld out of range (%d, %lld)\n", (lld)contig_id, 0, (lld)_cfg.max_contigs);
        }
        assert(contig_id >= 0);
        assert(_contigs);
        shared contig_t *contig = &_contigs[contig_id];

        int32_t seq_len = ks->seq.l;
        DBG("Allocting new sequence len %lld\n", (lld)seq_len);
        UPC_ALLOC_TRACK(contig->seq, seq_len + 1, _contigAllocs);
        if (!contig->seq) {
            DIE("Could not allocate %d for new seq\n", seq_len);
        }
        upc_memput(contig->seq, ks->seq.s, seq_len + 1);
        contig->slen = seq_len;
        ncontigs++;
    }
    kseq_destroy(ks);
    closeCheckpoint1(fastaf);
    tprintf_flush("Finished processing %ld contigs\n", ncontigs);

    // now get data from contig depths
    sprintf(fname, "%s.txt" GZIP_EXT, _cfg.contig_depths_file);
    GZIP_FILE f = openCheckpoint(fname, "r");
    int64_t contig_id;
    int32_t contig_mers, line = 0;
    double contig_depth;
    Buffer buf = initBuffer(1024);
    while (1) {
        char *bytesRead = gzgetsBuffer(buf, 1024, f);
        if (!bytesRead) {
            break;
        }
        line++;
        int32_t num_scanned = scanfBuffer(buf, "Contig%lld\t%d\t%lf\n", &contig_id, &contig_mers, &contig_depth);
        if (num_scanned == EOF) {
            break;
        }
        if (num_scanned < 3) {
            DIE("Incorrect format in %s file at line %d, expected 3 values, got %d\n",
                fname, line, num_scanned);
        }
        if (contig_id >= _cfg.max_contigs || contig_id < 0) {
            DIE("contig id %lld out of range (%d, %lld)\n", (lld)contig_id, 0, (lld)_cfg.max_contigs);
        }
        shared contig_t *contig = &_contigs[contig_id];
        contig->depth = contig_depth;
#ifdef CONFIG_SANITY_CHECK
        int32_t clen = contig_mers + _cfg.mer_size - 1;
        if (contig->slen && clen != contig->slen) {
            WARN("Contig %lld has different lengths in merDepths vs merAligner: %d != %d\n",
                 (lld)contig_id, clen, contig->slen);
        }
#endif
    }
    closeCheckpoint(f);
    freeBuffer(buf);
    stop_timer(T_PROCESS_CONTIGS);
    serial_printf("done in %.3f s\n", get_elapsed_time(T_PROCESS_CONTIGS));
    tprintf_flush("\n... %ld contigs added\n", ncontigs);
    UPC_TIMED_BARRIER;
}

static void get_local_read_block(read_block_t *local_block, sh_read_block_t *sh_block)
{
    int32_t nreads = sh_block->nreads_fastq;

    local_block->nreads = nreads;
    if (!nreads) {
        if (local_block->reads != NULL) {
            free_chk(local_block->reads);
        }
        local_block->reads = NULL;
        return;
    }
    //DBG("allocating for %lld reads\n", (lld)nreads);
    /* first copy the array of shared pointers */
    sh_read_ptr_t *tmp;
    tmp = (sh_read_ptr_t *)malloc_chk(nreads * sizeof(sh_read_ptr_t));
    upc_memget(tmp, sh_block->reads, nreads * sizeof(sh_read_ptr_t));

    local_block->reads = (read_ptr_t *)realloc_chk(local_block->reads, nreads * sizeof(read_ptr_t));
    for (int32_t i = 0; i < nreads; i++) {
        ALLOC_READ_FROM_SHARED(local_block->reads[i], tmp[i]);
    }
    free_chk(tmp);
}

local_contig_t *get_local_contig(int64_t contig_id)
{
    assert(contig_id < _cfg.max_contigs);
    assert(contig_id >= 0);
    shared contig_t *contig = &_contigs[contig_id];
    contig_t tmp = *contig; // copy the struct locally
    if (tmp.slen == 0) {
        DBG("Found empty contig: %lld\n", (lld)contig_id);
        return NULL;
    }
    assert(tmp.seq != NULL);
    local_contig_t *local_contig = calloc_chk(1, sizeof(local_contig_t));
    local_contig->slen = tmp.slen;
    local_contig->depth = tmp.depth;

    local_contig->seq = upc_memget_alloc(tmp.seq, tmp.slen + 1);

    get_local_read_block(&(local_contig->reads_start), &(tmp.reads_start));
    get_local_read_block(&(local_contig->reads_end), &(tmp.reads_end));

#ifdef CONFIG_SANITY_CHECK
    for (int32_t i = 0; i < local_contig->reads_start.nreads; i++) {
        if (local_contig->reads_start.lens[i] > _cfg.max_read_len) {
            DIE("contig %lld i %d wrong read len %d != %d, nreads %d %d, orig %d\n",
                (lld)contig_id, i, local_contig->reads_start.lens[i], _cfg.max_read_len,
                local_contig->reads_start.nreads, contig->reads_start.nreads_fastq,
                contig->reads_start.lens[i]);
        }
    }
#endif
    return local_contig;
}

static void free_read_block(read_block_t *read_block)
{
    if (!read_block->nreads) {
        return;
    }
    for (int32_t i = 0; i < read_block->nreads; i++) {
        FREE_READ(read_block->reads[i]);
    }
    free_chk(read_block->reads);
}

void free_local_contig(local_contig_t **_contig)
{
    assert(_contig);
    local_contig_t *contig = *_contig;
    if (contig) {
        free_read_block(&contig->reads_start);
        free_read_block(&contig->reads_end);
        free_chk(contig->seq);
        free_chk(contig);
    }
    *_contig = NULL;
}

static void free_sh_read_block(shared [] sh_read_block_t *read_block)
{
    int32_t nreads = read_block->nreads_fastq;

    if (!nreads) {
        return;
    }
    assert(read_block->reads != NULL);
    /* first copy the array locally to speed up loop */
    sh_read_ptr_t *reads;
    if (upc_threadof(read_block->reads) == MYTHREAD) {
        reads = (sh_read_ptr_t *)read_block->reads;
    } else {
        reads = malloc_chk(nreads * sizeof(*reads));
        upc_memget(reads, read_block->reads, nreads * sizeof(*reads));
    }
    for (int32_t i = 0; i < nreads; i++) {
        UPC_FREE_READ(reads[i]);
    }
    if (upc_threadof(read_block->reads) != MYTHREAD) {
        free_chk(reads);
    }
    UPC_FREE_TRACK(read_block->reads);
}

void free_sh_contig(shared [] contig_t *contig)
{
    free_sh_read_block(&contig->reads_start);
    free_sh_read_block(&contig->reads_end);
    if (contig->seq) {
        UPC_FREE_TRACK(contig->seq);
    }
}
