#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <math.h>
#include <upc.h>
#include <upc_tick.h>

#include "localassm_timerdefs.h"

#include "upc_common.h"
#include "common.h"

#include "colors.h"
#include "../fqreader/fq_reader.h"
#include "utils.h"
#include "timers.h"
#include "tracing.h"

#include "localassm_config.h"
#include "alignments.h"
#include "contigs.h"
#include "upc_output.h"

//#define CONFIG_DBG_ALNS

#define NO_ALN 0
#define OVERLAPS_CONTIG 1
#define EXTENDS_CONTIG 2

#define REJ_SAME_CONTIG_ALN 1
#define REJ_WILL_OVERLAP 2

#define MAX_BM_LINE_LEN 256
#define MAX_ALN_TOKENS 14
#define MAX_ALNS_PER_PAIR 100
// similar to max reads per gap in gap closing, which is also set to 5000
#define MAX_ALNS_PER_READ 5000

static long _nalns_parsed = 0;
static long _nalns_found = 0;
static long _nalns_invalid = 0;

// returns the base name of a pair: @pair from any: '@pair/1' '@pair 1:Y:...' etc
static int get_base_name(char *name, char *buf)
{
    char *start_name = strchr(buf, '@');

    if (!start_name) {
        return 0;
    }
    assert(start_name != buf);
    assert(*(start_name - 1) == '\t');

    char *end_name = strchr(start_name, '\t');
    if (!end_name) {
        DIE("Invalid format for alignment: %s\n", buf);
    }
    int len = (end_name - start_name) - 1;
    if (len > 3 && start_name[len - 2] == '/' &&
        (start_name[len - 1] == '1' || start_name[len - 1] == '2')) {
        // @readpair/1 or @readpair/2  return @readpair
        len -= 2;
    }
    strncpy(name, start_name, len);
    name[len] = '\0';
    return 1;
}

static const char *ALN_STATUS[] = { "none", "overlaps", "extends" };

static void dbg_aln(aln_t *aln)
{
    DBG("%s/%c %d %d %d Contig%lld %d %d %d %c %s %s\n",
        aln->read_name, aln->pair_num, aln->read_aln_start, aln->read_aln_stop, aln->read_len,
        (lld)aln->contig_id, aln->contig_aln_start, aln->contig_aln_stop, aln->contig_len,
        aln->strand, ALN_STATUS[aln->start_status], ALN_STATUS[aln->end_status]);
}

// format for meraligner files:
// MERALIGNER-[0,1,F]  rname  raln_start  raln_stop  rlen  cid  caln_start  caln_stop  clen  strand
static int parse_aln(char *buf, aln_t *aln)
{
    assert(buf != NULL);
    assert(aln != NULL);
    START_TIMER(T_GET_ALN);
    // 14 fields, tab separated
    const int max_toks = MAX_ALN_TOKENS;
    char *tokens[MAX_ALN_TOKENS];
    int tot_len;
    int ntoks = get_tokens(buf, tokens, &tot_len, max_toks);
    if (ntoks != max_toks) {
        DIE("Incorrect format in alignment file, incorrect number of tokens, found %d, expected %d\n",
            ntoks, max_toks);
    }
    assert(strlen(tokens[0]) > 11);
    if (tokens[0][11] == 'F') {
        return 0;
    }

    char *name;
    int8_t pair_num;
    if (!get_fq_name_dirn(tokens[1], &name, &pair_num)) {
        // unpaired read
        aln->pair_num = '0';
    } else {
        aln->pair_num = (pair_num == 1 ? '1' : '2');
    }
    strcpy(aln->read_name, name);

    aln->read_aln_start = atoi(tokens[2]);
    aln->read_aln_stop = atoi(tokens[3]);
    aln->read_len = atoi(tokens[4]);

    aln->contig_id = GET_CONTIG_ID(tokens[5]);
    if (aln->contig_id < 0 || aln->contig_id > _cfg.max_contigs) {
        DIE("Contig id is out of range (0, %lld): %lld\n", (lld)_cfg.max_contigs, (lld)aln->contig_id);
    }

    aln->contig_aln_start = atoi(tokens[6]);
    aln->contig_aln_stop = atoi(tokens[7]);
    aln->contig_len = atoi(tokens[8]);
    char *strand = tokens[9];
    if (strcmp(strand, "Plus") != 0 && strcmp(strand, "Minus") != 0) {
        DIE("Invalid entry in meraligner file:\n%s\n", buf);
    }
    aln->strand = (strand[0] == 'P' ? '+' : '-');

    aln->start_status = 0;
    aln->end_status = 0;
    stop_timer(T_GET_ALN);
    return 1;
}

static unsigned char classify_aln(int runaligned, int cunaligned, int wiggle)
{
    if (runaligned > cunaligned && cunaligned < wiggle) {
        return EXTENDS_CONTIG;
    }
    if (runaligned <= cunaligned && runaligned < wiggle) {
        return OVERLAPS_CONTIG;
    }
    return 0;
}

static int get_aln(aln_t *aln, char *buf, int revcomp, int fp_wiggle, int tp_wiggle)
{
    _nalns_parsed++;

    if (!parse_aln(buf, aln)) {
        return NO_ALN;
    }

#ifdef CONFIG_DBG_ALNS
    dbg_aln(aln);
#endif

    _nalns_found++;

    if (aln->strand == '+') {
        aln->start_status = classify_aln(aln->read_aln_start - 1,
                                         aln->contig_aln_start - 1, fp_wiggle);
        aln->end_status = classify_aln(aln->read_len - aln->read_aln_stop,
                                       aln->contig_len - aln->contig_aln_stop, tp_wiggle);
    } else {
        // for '-' strand, aln is between read and revcomp of contig
        aln->start_status = classify_aln(aln->read_aln_start - 1,
                                         aln->contig_len - aln->contig_aln_stop, fp_wiggle);
        aln->end_status = classify_aln(aln->read_len - aln->read_aln_stop,
                                       aln->contig_aln_start - 1, tp_wiggle);
    }

    DBG2("start_status: %s, end_status %s\n", ALN_STATUS[aln->start_status],
         ALN_STATUS[aln->end_status]);

    if (aln->start_status == NO_ALN || aln->end_status == NO_ALN) {
        _nalns_invalid++;
        return 0;
    }

    // Re-orient alignment if requested
    if (revcomp) {
        aln->strand = (aln->strand == '+' ? '-' : '+');
        int orig_start = aln->read_aln_start;
        aln->read_aln_start = aln->read_len - aln->read_aln_stop + 1;
        aln->read_aln_stop = aln->read_len - orig_start + 1;
    }
    return 1;
}

// gets array of alignments for a single read, returns nalns, or EOF on file end
static int get_alns(GZIP_FILE f, char *fname, aln_t *alns, char *buf, char *curr,
                    long *line, int revcomp, int fp_wiggle, int tp_wiggle)
{
    if (GZIP_EOF(f)) {
        return EOF;
    }
    // this happens on the first invocation only
    if (!buf[0] && !GZIP_GETS(buf, MAX_BM_LINE_LEN - 1, f)) {
        return EOF;
    }

    char name[MAX_READ_NAME_LEN] = "", curr_name[MAX_READ_NAME_LEN] = "";
    aln_t aln;
    int nalns = 0;
    do {
        CHECK_LINE_LEN(buf, MAX_BM_LINE_LEN, fname);
        (*line)++;
        if (!get_base_name(name, buf)) {
            DIE("Could not find %s at line %ld in %s\n", name, *line, fname);
        }
        if (curr[0]) {
            if (!get_base_name(curr_name, curr)) {
                DIE("Could not find %s in %s in %s\n", curr_name, curr, fname);
            }
            if (strcmp(curr_name, name) == 0) {
                // accumulate alns
                if (get_aln(&aln, buf, revcomp, fp_wiggle, tp_wiggle)) {
                    if (nalns == MAX_ALNS_PER_PAIR) {
                        //WARN("Read %s has too many alignments, max %d\n", aln.read_name, nalns);
                        continue;
                    }
                    alns[nalns++] = aln;
                }
            } else {
                // switching to new read
                strcpy(curr, buf);
                return nalns;
            }
        } else {
            strcpy(curr, buf);
            if (get_aln(&aln, buf, revcomp, fp_wiggle, tp_wiggle)) {
                if (nalns == MAX_ALNS_PER_PAIR) {
                    //WARN("Read %s has too many alignments, max %d\n", aln.read_name, nalns);
                    continue;
                }
                alns[nalns++] = aln;
            }
        }
    } while (GZIP_GETS(buf, MAX_BM_LINE_LEN - 1, f));
    return EOF;
}

static int add_read_to_set(read2contig_set_t *read2contig_set, char *read_name, int64_t contig_id,
                           char strand, char pair_num, char contig_side)
{
    shared contig_t *contig = &_contigs[contig_id];

    if (contig_side == 'S') {
        int32_t nreads;
        UPC_ATOMIC_FADD_I32(&nreads, &(contig->reads_start.nreads_alns), 1);
        if (nreads >= MAX_ALNS_PER_READ) {
            //tprintf("Exceeding %d read alns for contig %lld at start\n", MAX_ALNS_PER_READ, (lld) contig_id);
            UPC_ATOMIC_SET_I32(NULL, &(contig->reads_start.nreads_alns), MAX_ALNS_PER_READ);
            return 0;
        }
    } else if (contig_side == 'E') {
        int32_t nreads;
        UPC_ATOMIC_FADD_I32(&nreads, &(contig->reads_end.nreads_alns), 1);
        if (nreads >= MAX_ALNS_PER_READ) {
            //tprintf("Exceeding %d read alns for contig %lld at end\n", MAX_ALNS_PER_READ, (lld) contig_id);
            UPC_ATOMIC_SET_I32(NULL, &(contig->reads_end.nreads_alns), MAX_ALNS_PER_READ);
            return 0;
        }
    }

    if (read2contig_set->num + 1 >= read2contig_set->max) {
        read2contig_set->max = read2contig_set->num * 1.5 + 16;
        read2contig_set->read2contigs = realloc_chk(read2contig_set->read2contigs,
                                                    read2contig_set->max * sizeof(read2contig_t));
    }
    read2contig_t *read2contig = &read2contig_set->read2contigs[read2contig_set->num];
    strcpy(read2contig->read_name, read_name);
    read2contig->contig_id = contig_id;
    read2contig->strand = strand;
    read2contig->pair_num = pair_num;
    read2contig->contig_side = contig_side;
    read2contig_set->num++;
    return 1;
}

static int pair_alns_match_contig(int64_t contig_id, aln_t *alns, int nalns)
{
    for (int i = 0; i < nalns; i++) {
        if (alns[i].contig_id == contig_id) {
            return 1;
        }
    }
    return 0;
}

#define PROCESS_MERALIGNER_FILE_NUM_VALS 6
void process_meraligner_file(read2contig_set_t *read2contig_set, char *lib_name, int revcomp,
                             int insert_size, int insert_sigma, int fp_wiggle, int tp_wiggle)
{
    _nalns_parsed = 0;
    _nalns_found = 0;
    _nalns_invalid = 0;
    serial_printf("Processing %s/%s-%s_*_Read*... ", _cfg.base_dir, lib_name, _cfg.mer_file);
    // expect two files per library per thread, with endings alternating Read1 and Read2
    char fname[2][MAX_FILE_PATH];
    GZIP_FILE f[2] = { NULL, NULL };
    int numReadFiles = 2;
    for (int i = 0; i < numReadFiles; i++) {
        sprintf(fname[i], "%s-%s_Read%d" GZIP_EXT, lib_name, _cfg.mer_file, i + 1);
        if (i == 0 || doesCheckpointExist(fname[i])) { // Read1 or has a Read2
            f[i] = openCheckpoint(fname[i], "r");
        } else {
            f[i] = NULL; // unpaired reads
            numReadFiles = 1;
        }
    }
    tprintf_flush("Processing %s (+1)...\n", fname[0]);
    // start off with a rough conservative estimate at how many alignments will be counted
    // assume each one is roughly 60 bytes, and that 1/2 will be used
    char tmp[MAX_FILE_PATH];
    checkpointToLocal(fname[0], tmp);
    read2contig_set->max += get_file_size(tmp) * 5 / 60 / 2 + 16;
    read2contig_set->read2contigs = realloc_chk(read2contig_set->read2contigs,
                                                read2contig_set->max * sizeof(read2contig_t));
    int min_pair_len = insert_size + 3 * insert_sigma - 2;
    long line0 = 0, line1 = 0;
    char buf0[MAX_BM_LINE_LEN] = "", buf1[MAX_BM_LINE_LEN] = "";
    char curr0[MAX_BM_LINE_LEN] = "", curr1[MAX_BM_LINE_LEN] = "";
    aln_t pair_alns[2][MAX_ALNS_PER_PAIR];
    int pair_nalns[2];
    long max_nalns = 0;
    long ndirect = 0;
    long nproj = 0;
    // each loop finds all the alignments for one read pair
    while (1) {
        // get all alignments for the next read and its mate
        pair_nalns[0] = get_alns(f[0], fname[0], pair_alns[0], buf0, curr0, &line0, revcomp,
                                 fp_wiggle, tp_wiggle);
        if (pair_nalns[0] == EOF) {
            break;
        }
        max_nalns++;
        if (f[1]) {
            pair_nalns[1] = get_alns(f[1], fname[1], pair_alns[1], buf1, curr1, &line1, revcomp,
                                     fp_wiggle, tp_wiggle);
            if (pair_nalns[1] == EOF) {
                DIE("EOF before finding read pair mate, from line %ld: %s\n", line0, buf0);
            }
        }
        for (int pair_i = 0; pair_i < numReadFiles; pair_i++) {
            aln_t *alns = pair_alns[pair_i];
            int nalns = pair_nalns[pair_i];
            if (!nalns) {
                continue;
            }
            aln_t *aln = &alns[0];
            // if a read has multiple alignments, choose the longest
            if (nalns > 1) {
                int max_aln = 0;
                for (int i = 0; i < nalns; i++) {
                    int aln_len = alns[i].read_aln_stop - alns[i].read_aln_start;
                    if (aln_len > max_aln) {
                        max_aln = aln_len;
                        aln = &alns[i];
                    }
                }
            }
            // if read alignment extends a contig, use directly for the walk
            if (aln->start_status == EXTENDS_CONTIG) {
                if (add_read_to_set(read2contig_set, aln->read_name, aln->contig_id, aln->strand,
                                    aln->pair_num, aln->strand == '+' ? 'S' : 'E')) {
                    ndirect++;
                    DBG2(KLGREEN "DIRECT" KNORM " %s/%c, contig %lld %c\n", aln->read_name,
                        aln->pair_num, (lld)aln->contig_id, aln->strand == '+' ? 'S' : 'E');
                }
            }
            if (aln->end_status == EXTENDS_CONTIG) {
                if (add_read_to_set(read2contig_set, aln->read_name, aln->contig_id, aln->strand,
                                    aln->pair_num, aln->strand == '+' ? 'E' : 'S')) {
                    ndirect++;
                    DBG2(KLGREEN "DIRECT" KNORM " %s/%c, contig %lld %c\n", aln->read_name,
                        aln->pair_num, (lld)aln->contig_id, aln->strand == '+' ? 'E' : 'S');
                }
            }
            if (aln->end_status != EXTENDS_CONTIG && aln->start_status != EXTENDS_CONTIG) {
                DBG2(KLRED "REJECT direct" KNORM " %s/%c, contig %lld: no extension\n",
                     aln->read_name, aln->pair_num, (lld)aln->contig_id);
            }
            if (numReadFiles == 1) {
                continue;                    // use splints from unpaired reads
            }
            int rej = 0;

            // make sure that the mate won't overlap the same contig
            if (aln->strand == '+') {
                int proj_len = min_pair_len - aln->read_len - aln->read_aln_start + 1;
                if (proj_len <= aln->contig_len - aln->contig_aln_start) {
                    DBG2("min_pair_len %d, proj_len %d, contig off %d\n", min_pair_len, proj_len,
                         aln->contig_len - aln->contig_aln_start);
                    rej = REJ_WILL_OVERLAP;
                }
            } else {
                int proj_len = min_pair_len - 2 * aln->read_len + aln->read_aln_start - 1;
                if (proj_len <= aln->contig_aln_start) {
                    DBG2("min_pair_len %d, proj_len %d, contig off %d\n", min_pair_len, proj_len,
                         aln->contig_aln_start);
                    rej = REJ_WILL_OVERLAP;
                }
            }
            if (!rej) {
                if (pair_alns_match_contig(aln->contig_id, pair_alns[!pair_i], pair_nalns[!pair_i])) {
                    // don't add the mate pair if it matches the same contig
                    rej = REJ_SAME_CONTIG_ALN;
                } else if (aln->pair_num != '0') {
                    // now we can add the mate pair
                    if (add_read_to_set(read2contig_set, aln->read_name, aln->contig_id,
                                        aln->strand == '+' ? '-' : '+',
                                        aln->pair_num == '1' ? '2' : '1',
                                        aln->strand == '+' ? 'E' : 'S')) {
                        DBG2(KLBLUE "PROJCT" KNORM " %s/%c, contig %lld %c\n", aln->read_name,
                             (aln->pair_num == '1' ? '2' : '1'), (lld)aln->contig_id,
                             (aln->strand == '+' ? 'E' : 'S'));
                        nproj++;
                    }
                }
            }
            if (rej) {
                DBG2(KLRED "REJECT proj" KNORM " %s/%c, contig %lld %c: %s\n", aln->read_name,
                     (aln->pair_num == '1' ? '2' : '1'), (lld)aln->contig_id,
                     (aln->strand == '+' ? 'E' : 'S'),
                     (rej == REJ_SAME_CONTIG_ALN ? "aln same contig" : "will overlap"));
            }
        }
    }
    closeCheckpoint(f[0]);
    if (f[1]) {
        closeCheckpoint(f[1]);
    }

    UPC_TIMED_BARRIER;

    double myvals[PROCESS_MERALIGNER_FILE_NUM_VALS] = { max_nalns, ndirect, nproj, _nalns_found, _nalns_parsed, _nalns_invalid };
    double min_vals[PROCESS_MERALIGNER_FILE_NUM_VALS], max_vals[PROCESS_MERALIGNER_FILE_NUM_VALS], tot_vals[PROCESS_MERALIGNER_FILE_NUM_VALS];

    reduce_block(myvals, PROCESS_MERALIGNER_FILE_NUM_VALS, min_vals, max_vals, tot_vals);

    if (!MYTHREAD) {
        long tot_max_nalns = tot_vals[0];
        long tot_ndirect = tot_vals[1];
        long tot_nproj = tot_vals[2];
        long tot_nalns_found = tot_vals[3];
        long tot_nalns_parsed = tot_vals[4];
        long tot_invalid = tot_vals[5];

        long nalns_found = tot_ndirect + tot_nproj;
        serial_printf("... done, found %ld alns out of %ld ( %.3f )\n",
                      nalns_found, tot_max_nalns, (double)nalns_found / tot_max_nalns);
        serial_printf("Alignment stats:\n"
                      "  parse attempts: %ld\n"
                      "  parsed:         %ld\n"
                      "  invalid:        %ld\n"
                      "  direct:         %ld\n"
                      "  projected:      %ld\n",
                      tot_nalns_parsed, tot_nalns_found, tot_invalid, tot_ndirect, tot_nproj);
        ADD_DIAG("%ld", "aln_parse_attempts", tot_nalns_parsed);
        ADD_DIAG("%ld", "aln_parse_success", tot_nalns_found);
        ADD_DIAG("%ld", "aln_invalid", tot_invalid);
        ADD_DIAG("%ld", "aln_direct", tot_ndirect);
        ADD_DIAG("%ld", "aln_projected", tot_nproj);
    }
}
