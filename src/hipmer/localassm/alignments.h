#ifndef __ALIGNMENTS_H
#define __ALIGNMENTS_H

typedef struct {
    char          read_name[MAX_READ_NAME_LEN];
    int           read_aln_start, read_aln_stop;
    int           read_len;
    int           contig_aln_start, contig_aln_stop;
    int           contig_len;
    int64_t       contig_id;
    char          strand;
    unsigned char start_status, end_status;
    char          pair_num; // 1 or 2
} aln_t;

typedef struct {
    char    read_name[MAX_READ_NAME_LEN];
    int64_t contig_id;
    char    strand;
    char    pair_num;    // 1 or 2
    char    contig_side; // start or end
} read2contig_t;

typedef struct {
    read2contig_t *read2contigs;
    long           num;
    long           max;
} read2contig_set_t;

void process_meraligner_file(read2contig_set_t *read2contig_set, char *lib_name, int revcomp, int insert_size, int insert_sigma, int fp_wiggle, int tp_wiggle);

#endif
