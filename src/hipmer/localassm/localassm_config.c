#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <libgen.h>
#include <upc.h>

#include "optlist.h"
#include "upc_common.h"
#include "utils.h"
#include "tracing.h"

#include "localassm_config.h"

#define print_param(...)                        \
    do {                                        \
        if (MYTHREAD) { tprintf(__VA_ARGS__); }     \
        serial_printf(__VA_ARGS__);             \
    } while (0)

static const char *program_version = "local assembly 0.9";

static const char *_usage = "-i insert_size,.., -I insert_sigma,.., -m mer_size "
                            "-P (poly_mode) -Q qual_offset -c contigs_fasta_prefix -a meraligner_prefix "
                            "-b lib,.., -B base_dir -X (cached_io_reads) -F five_prime_wiggle_room -T three_prime_wiggle_room -l max_read_len "
                            "-r revcomp -N max_contigs -q qual_thres -H hi_qual_thres "
                            "-d min_viable_depth -D min_expected_depth -M max_mer_size -C contig_depths_prefix "
                            "-t depth_dist_thres -v nvote_diff_thres -z min_mer_size -R rating_thres";

config_t _cfg;
char *_optionList = NULL;
char *_usageCopy = NULL;

static char *get_option_list(void)
{
    char *usage = strdup_chk(_usage);

    _usageCopy = usage;
    // get option characters from usage
    char *options = malloc_chk(1000);
    int pos = 0;
    char *aux = NULL;
    char *token = strtok_r(usage, " ", &aux);
    while (token) {
        if (token[0] == '-') {
            options[pos++] = token[1];
        } else if (token[0] != '(') {
            options[pos++] = ':';
        }
        token = strtok_r(NULL, " ", &aux);
    }
    options[pos] = 0;
    return options;
}

static void get_lib_names(char *s)
{
    _cfg.num_lib_names = 0;
    int len = strlen(s);
    if (len >= MAX_LIBRARIES * 4) {
        DIE("Too long a library string\n");
    }
    int start = 0;
    int name_i = 0;
    for (int i = 0; i < len; i++) {
        if (s[i] == ',') {
            int l = i - start;
            strncpy(_cfg.lib_names[name_i], s + start, l);
            _cfg.lib_names[name_i][l] = 0;
            _cfg.num_lib_names++;
            name_i++;
            if (name_i >= MAX_LIBRARIES) {
                DIE("Too many libraries!\n");
            }
            start = i + 1;
        }
    }
    strcpy(_cfg.lib_names[name_i], s + start);
    _cfg.num_lib_names++;
}

static int get_int_array(int max_entries, int *flags, char *s)
{
    char *tmp = s;

    for (int i = 0;; i++) {
        char *token = strtok(tmp, ",");
        if (!token) {
            break;
        }
        if (i >= max_entries) {
            return 0;
        }
        flags[i] = atoi(token);
        tmp = NULL;
    }
    return 1;
}

static void print_param_array(const char *label, int *array, int num)
{
    tprintf("%s", label);
    serial_printf("%s", label);
    for (int i = 0; i < num; i++) {
        tprintf("%d%c", array[i], i == num - 1 ? ' ' : ',');
        serial_printf("%d%c", array[i], i == num - 1 ? ' ' : ',');
    }
    tprintf("\n");
    serial_printf("\n");
}

void localassm_get_config(int argc, char **argv)
{
    memset(&_cfg, 0, sizeof(config_t));
    // defaults
    _cfg.qual_offset = 33;
    _cfg.min_qual = 10;
    _cfg.min_hi_qual = 20;
    _cfg.min_viable_depth = 0.2;
    _cfg.min_expected_depth = 0.5;
    _cfg.max_mer_size = 0;
    _cfg.min_mer_size = 13;
    _cfg.base_dir = strdup_chk(".");
    _cfg.depth_dist_thres = 0.5;
    _cfg.nvote_diff_thres = 1.5;
    _cfg.rating_thres = 2;

    if (argc == 1 || (argc == 2 && strcmp(argv[1], "-h") == 0)) {
        serial_printf("Usage: %s %s\n", basename(argv[0]), _usage);
        upc_global_exit(1);
    }

    option_t *this_opt;
    _optionList = get_option_list();
    option_t *opt_list = GetOptList(argc, argv, _optionList);
    print_args(opt_list, "localassm_main");
    char fail[10000] = "";
    char buf[100];
    while (opt_list) {
        this_opt = opt_list;
        opt_list = opt_list->next;
        switch (this_opt->option) {
        case 'i':
            if (!get_int_array(MAX_LIBRARIES, _cfg.insert_size, this_opt->argument)) {
                strcat(fail, "\tToo many insert sizes\n");
            }
            break;
        case 'I':
            if (!get_int_array(MAX_LIBRARIES, _cfg.insert_sigma, this_opt->argument)) {
                strcat(fail, "\tToo many insert sigmas\n");
            }
            break;
        case 'm': _cfg.mer_size = atoi(this_opt->argument); break;
        case 'P': _cfg.poly_mode = 1; break;
        case 'Q': _cfg.qual_offset = atoi(this_opt->argument); break;
        case 'c': _cfg.contigs_file = this_opt->argument; break;
        case 'C': _cfg.contig_depths_file = this_opt->argument; break;
        case 'a': _cfg.mer_file = this_opt->argument; break;
        case 'b': get_lib_names(this_opt->argument); break;
        case 'B': if (_cfg.base_dir) {
                free_chk(_cfg.base_dir);
        }
            _cfg.base_dir = strdup_chk(this_opt->argument); break;
        case 'X': _cfg.cached_io_reads = 1; break;
        case 'F':
            if (!get_int_array(MAX_LIBRARIES, _cfg.fp_wiggle, this_opt->argument)) {
                strcat(fail, "\tToo many 5p\n");
            }
            break;
        case 'T':
            if (!get_int_array(MAX_LIBRARIES, _cfg.tp_wiggle, this_opt->argument)) {
                strcat(fail, "\tToo many 3p\n");
            }
            break;
        case 'l': _cfg.max_read_len = atoi(this_opt->argument); break;
        case 'r':
            if (!get_int_array(MAX_LIBRARIES, _cfg.revcomp, this_opt->argument)) {
                strcat(fail, "\tToo many reverse complements\n");
            }
            break;
        case 'N': _cfg.max_contigs = atol(this_opt->argument); break;
        case 'q': _cfg.min_qual = atoi(this_opt->argument); break;
        case 'H': _cfg.min_hi_qual = atoi(this_opt->argument); break;
        case 'd': _cfg.min_viable_depth = atof(this_opt->argument); break;
        case 'D': _cfg.min_expected_depth = atof(this_opt->argument); break;
        case 'M': _cfg.max_mer_size = atoi(this_opt->argument); break;
        case 't': _cfg.depth_dist_thres = atof(this_opt->argument); break;
        case 'v': _cfg.nvote_diff_thres = atof(this_opt->argument); break;
        case 'z': _cfg.min_mer_size = atoi(this_opt->argument); break;
        case 'R': _cfg.rating_thres = atoi(this_opt->argument); break;
        default:
            sprintf(buf, "\tInvalid Option: %c\n", this_opt->option);
            strcat(fail, buf);
            break;
        }
    }
    // check params
    for (int l = 0; l < _cfg.num_lib_names; l++) {
/* Okay for unpaired libs
 * TODO check for entry that can be 0
 *      if (!_cfg.insert_size[l])
 *          strcat(fail, "\tinsert_size required for each lib (-i int,int,..)\n");
 *      if (!_cfg.insert_sigma[l])
 *          strcat(fail, "\tinsert_sigma required for each lib (-I int,int,..)\n");
 */
        if (!_cfg.fp_wiggle[l]) {
            _cfg.fp_wiggle[l] = 5;
        }
        if (!_cfg.tp_wiggle[l]) {
            _cfg.tp_wiggle[l] = 5;
        }
    }
    if (!_cfg.mer_size) {
        strcat(fail, "\tmer_size required (-m)\n");
    }
    if (!_cfg.max_mer_size) {
        strcat(fail, "\tmax_mer_size required (-M)\n");
    }
    if (_cfg.max_mer_size < _cfg.mer_size) {
        strcat(fail, "\tmax_mer_size (-M) should be more than mer_size (-m)\n");
    }
    if (!_cfg.contigs_file) {
        strcat(fail, "\tcontigs FASTA file required (-c)\n");
    }
    if (!_cfg.contig_depths_file) {
        strcat(fail, "\tcontig depths file is  required (-C)\n");
    }
    if (!_cfg.mer_file) {
        strcat(fail, "\tmeraligner file required (-a)\n");
    }
    if (!_cfg.max_contigs) {
        strcat(fail, "\tmax number of contigs required (-N)\n");
    }
    if (!_cfg.max_read_len) {
        strcat(fail, "\tmax read len required (-l)\n");
    }
    if (_cfg.min_mer_size >= _cfg.max_mer_size) {
        strcat(fail, "\tmin_mer_size must be less than max_mer_size\n");
    }
    if (fail[0]) {
        serial_printf("Error in inputs:\n%s\nUsage: %s %s\n", fail, basename(argv[0]), _usage);
        upc_global_exit(1);
    }
    upc_barrier;

    if (_cfg.min_viable_depth > _cfg.min_expected_depth) {
        serial_printf("Error: min_viable_depth (-d %.2f) > min_expected_depth (-D %.2f)\n",
                      _cfg.min_viable_depth, _cfg.min_expected_depth);
        upc_global_exit(1);
    }
    set_output_dir(_cfg.base_dir);

    print_param("%s\n", program_version);
    print_param_array("  Insert_size (-i):          ", _cfg.insert_size, _cfg.num_lib_names);
    print_param_array("  Insert sigma (-I):         ", _cfg.insert_sigma, _cfg.num_lib_names);
    print_param("  mer_size (-m):             %d\n", _cfg.mer_size);
    print_param("  polymorphic mode (-P):     %s\n", _cfg.poly_mode ? "true" : "false");
    print_param("  qual_offset (-Q):          %d\n", _cfg.qual_offset);
    print_param("  Contigs FASTA file (-c):   %s\n", _cfg.contigs_file);
    print_param("  Contig depths file (-C):   %s\n", _cfg.contig_depths_file);
    print_param("  Meraligner file (-a):      %s\n", _cfg.mer_file);
    print_param("  library names (-b):        ");
    for (int i = 0; i < _cfg.num_lib_names; i++) {
        print_param("%s%c", _cfg.lib_names[i], i == _cfg.num_lib_names - 1 ? ' ' : ',');
    }
    print_param("\n");
    print_param("  Base dir (-B):             %s\n", _cfg.base_dir);
    print_param_array("  5' wiggle room (-F):       ", _cfg.fp_wiggle, _cfg.num_lib_names);
    print_param_array("  3' wiggle room (-T):       ", _cfg.tp_wiggle, _cfg.num_lib_names);
    print_param("  Max read len (-l):         %d\n", _cfg.max_read_len);
    print_param_array("  Reverse complement (-r):   ", _cfg.revcomp, _cfg.num_lib_names);
    print_param("  Max number contigs (-N):   %lld\n", (lld)_cfg.max_contigs);
    print_param("  Quality threshold (-q):    %d\n", _cfg.min_qual);
    print_param("  High quality thresh (-H):  %d\n", _cfg.min_hi_qual);
    print_param("  Min viable depth (-d):     %.2f\n", _cfg.min_viable_depth);
    print_param("  Min expected depth (-D):   %.2f\n", _cfg.min_expected_depth);
    print_param("  Max mer size (-M):         %d\n", _cfg.max_mer_size);
    print_param("  Min mer size (-z):         %d\n", _cfg.min_mer_size);
    print_param("  Depth dist thres (-t):     %.2f\n", _cfg.depth_dist_thres);
    print_param("  Nvote diff thres (-v):     %.2f\n", _cfg.nvote_diff_thres);
    print_param("  Rating thres (-R):         %d\n", _cfg.rating_thres);
    tflush();
    print_param("Compile time settings:\n");
#ifdef USE_QUALS
    print_param("  Using quality scores\n");
#else
    print_param("  Ignoring quality scores\n");
#endif
}
void localassm_free_config()
{
    if (_cfg.base_dir) {
        free_chk(_cfg.base_dir);
    }
    if (_usageCopy) {
        free_chk(_usageCopy);
    }
    if (_optionList) {
        free_chk(_optionList);
    }
}
