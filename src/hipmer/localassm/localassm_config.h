/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 */

#ifndef __LOCALASSM_CONFIG_H
#define __LOCALASSM_CONFIG_H

#include <limits.h>
#include "upc_common.h"
#include "utils.h"

typedef struct {
    int     insert_size[MAX_LIBRARIES];
    int     insert_sigma[MAX_LIBRARIES];
    int     mer_size;
    int     poly_mode;
    int     qual_offset;
    int     fp_wiggle[MAX_LIBRARIES];
    int     tp_wiggle[MAX_LIBRARIES];
    int     revcomp[MAX_LIBRARIES];
    char    lib_names[MAX_LIBRARIES][LIB_NAME_LEN];
    int     num_lib_names;
    char *  contigs_file;
    char *  mer_file;
    char *  base_dir;
    int     cached_io_reads;
    char *  contig_depths_file;
    int64_t max_contigs;
    int     max_read_len;
    int     max_mer_size;
    int     min_mer_size;
    double  min_viable_depth;
    double  min_expected_depth;
    int     min_qual;
    int     min_hi_qual;
    double  depth_dist_thres;
    double  nvote_diff_thres;
    int     rating_thres;
} config_t;

extern config_t _cfg;

void localassm_get_config(int argc, char **argv);
void localassm_free_config();

#endif
