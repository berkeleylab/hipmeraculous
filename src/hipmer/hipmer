#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

#echo "Discovered base path of $DIR"

# place this HipMer-install/bin path at the front of PATH
export PATH=${DIR}:${PATH/${DIR}/}

hipmer_install=${DIR%/*}
if [ -z "${HIPMER_ENV}" ] && [ -f "${hipmer_install}/env.sh" ]
then
    echo "Sourcing HipMer Environment: ${hipmer_install}/env.sh"
    source ${hipmer_install}/env.sh
fi

if ( [ -n "${CHECK_FREE_HUGEPAGES_MB}" ] &&  [ "${CHECK_FREE_HUGEPAGES_MB}" != "0" ] ) || [ -n "$MIN_NODES" ] || [ -n "$MAX_NODES" ]
then
    # test the nodes for enough hugepage memory and kick out a few if necessary
    if [ -z "$MIN_NODES" ]
    then
        if [ $SLURM_JOB_NUM_NODES -gt ${MIN_NODES_FOR_HUGEPAGE_CHECK:=4} ]
        then
            MIN_NODES=$((3*SLURM_JOB_NUM_NODES/4))
        else
            MIN_NODES=${SLURM_JOB_NUM_NODES}
        fi
    fi
    if [ -n "${SLURM_JOB_ID}" ] && [ -f /proc/buddyinfo ]
    then
        if [ ! -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            check_hugepage_and_reduce_slurm_job.sh MIN_NODES=${MIN_NODES} || echo "Failed to check hugepages.  Continuing"
        fi
        if [ -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            echo "Adjusting job environment: source $TMPDIR/new_slurm_env.SLURM_JOB_ID"
            set -x
            source $TMPDIR/new_slurm_env.$SLURM_JOB_ID
            set +x
        else
            echo "There is no file to adjust the environment: $TMPDIR/new_slurm_env.$SLURM_JOB_ID"
        fi
    fi
    export CHECK_FREE_HUGEPAGES_MB=0
fi

export PYTHONUNBUFFERED=1

auto_opts=
if [ -n "$CORES_PER_NODE" ] 
then
  auto_opts="${auto_opts} --cores-per-node=${CORES_PER_NODE}"
fi

if [ -n "$auto_opts" ]
then
  echo "Automatically set the following options from the environment: ${auto_opts}"
fi

echo "Executing: hipmer.py ${auto_opts} $@"
echo ""

hipmer.py ${auto_opts} "$@"

