#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <upc.h>
#include <upc_tick.h>
#include <libgen.h>
#include <sys/types.h>
#include <dirent.h>

#include "defines.h"

#include "loadfq.h"
#include "optlist.h"
#include "upc_common.h"
#include "common_base.h"
#include "common.h"
#include "timers.h"
#include "../fqreader/fq_reader.h"
#include "Buffer.h"
#include "utils.h"
#include "upc_output.h"


static char library_names[MAX_LIBRARIES][LIB_NAME_LEN];
static int num_lib_names = 0;

static void get_library_names(char *s)
{
    num_lib_names = 0;
    int len = strlen(s);
    if (len >= MAX_LIBRARIES * 4) {
        DIE("Too long a library string\n");
    }
    int start = 0;
    int name_i = 0;
    for (int i = 0; i < len; i++) {
        if (s[i] == ',') {
            int l = i - start;
            strncpy(library_names[name_i], s + start, l);
            library_names[name_i][l] = 0;
            num_lib_names++;
            serial_printf("Using lib %d: %s\n", name_i, library_names[name_i]);
            name_i++;
            if (name_i >= MAX_LIBRARIES) {
                DIE("Too many libraries!\n");
            }
            start = i + 1;
        }
    }
    strcpy(library_names[name_i], s + start);
    serial_printf("Using lib %d: %s\n", name_i, library_names[name_i]);
    num_lib_names++;
}

int64_t loadfq_library(const char *libname, const char *base_dir)
{
    int64_t totalFileSize = 0;
    double start0 = now();
    char fofn[MAX_FILE_PATH], fname[MAX_FILE_PATH];
    snprintf(fofn, MAX_FILE_PATH, "%s.fofn-LOADFQ", libname);
    if ( !all_does_file_exist(fofn) ) {
        sprintf(fofn, "%s.fofn", libname);
    }
    serial_printf("Processing fastq files from %s...\n", fofn);
    Buffer newFofnBuffer = initBuffer(MAX_FILE_PATH);
    Buffer fofn_Buffer = broadcast_file(fofn);
    while (getsBuffer(fofn_Buffer, fname, MAX_FILE_PATH)) {
        double start1 = now();
        fname[strlen(fname) - 1] = '\0';
        char *tmp = strdup(fname);
        char newFile[MAX_FILE_PATH];
        sprintf(newFile, "%s%s", basename(tmp), GZIP_EXT);
        printfBuffer(newFofnBuffer, "%s\n", newFile);
        free(tmp);
        LOGF("New file in fofn will be %s\n", newFile);

        if (doesLocalCheckpointExist(newFile)) {
            serial_printf("Skipping already loaded file: %s -> %s\n", fname, newFile);
            continue;
        }
 
        serial_printf("Loading %s into memory on %d threads\n", fname, THREADS);

        fq_reader_t fqr = create_fq_reader();
        int64_t fileSize;
        if (MYTHREAD == 0) {
            fileSize = get_file_size(fname);
        }
        fileSize = broadcast_long(fileSize, 0);
        totalFileSize += fileSize;
        int err = load_fq(fqr, fname, base_dir, fileSize);
        if (err < 0) {
            DIE("Could not load %s into memory: %s\n", fname, strerror(-err));
        }

        if (MYSV.checkpoint_path) {
            char globalCheckpoint[MAX_FILE_PATH];
            checkpointToGlobal(newFile, globalCheckpoint);
            setNoAggregate(globalCheckpoint);
            strcat(newFile, ".uncompressedSize");
            restoreCheckpoint(newFile);
        }

        double end1 = now();
        double elapsed1 = end1 - start1;
        double MB = 1.0 / ONE_MB * (fqr->end_read - fqr->start_read);
        LOGF("Loading times for %s: read %0.3f MB in %0.3f s wrote in %0.3f s (%0.3f MB/s)\n", fname, MB, fqr->readTime, fqr->writeTime, (elapsed1 != 0 ? MB / elapsed1 : 0.0));
        destroy_fq_reader(fqr);
    }
    freeBuffer(fofn_Buffer);
    if (!MYTHREAD) {
        snprintf(fname, MAX_FILE_PATH, "%s.fofn-LOADFQ", libname);
        if (! does_file_exist(fname) ) {
            serial_printf("Moving fofn to %s in support of restarts\n", fname);
            rename(fofn, fname);
            serial_printf("Replacing fofn: %s\n", fofn);
            LOGF("newFofnBuffer:\n%s", getStartBuffer(newFofnBuffer));
            FILE * f = fopen_chk(fofn, "w");
            fwrite_chk(getStartBuffer(newFofnBuffer), 1, getLengthBuffer(newFofnBuffer), f);
            fclose_track(f);
        }
    }
    freeBuffer(newFofnBuffer);
    UPC_LOGGED_BARRIER;
    double end0 = now();
    double elapsed0 = end0 - start0;
    double MB = totalFileSize * 1.0 / ONE_MB;
    serial_printf("Loaded and compressed lib %s fastq %0.3f MB in %0.3f s (%0.3f MB/s per thread)\n", libname, MB, elapsed0, elapsed0 != 0 ? MB / elapsed0 / THREADS : 0.0);
    return totalFileSize;
}

int loadfq_main(int argc, char **argv)
{
    option_t *this_opt;
    option_t *opt_list = GetOptList(argc, argv, "f:N:B:p");

    print_args(opt_list, __func__);
    char *libnames_ext = NULL;
    const char *base_dir = "/dev/shm";
    int coresPerNode = 1;
    int purgeShm = 0;
    while (opt_list) {
        this_opt = opt_list;
        opt_list = opt_list->next;
        switch (this_opt->option) {
        case 'f':
            libnames_ext = this_opt->argument;
            break;
        case 'N':
            coresPerNode = atoi(this_opt->argument);
            SET_CORES_PER_NODE(coresPerNode);
            break;
        case 'B':
            base_dir = this_opt->argument;
            break;
        case 'p':
            purgeShm = 1;
            break;
        default:
            serial_printf("Invalid option %c\n", this_opt->option);
            upc_global_exit(1);
        }
    }
    if (!libnames_ext) {
        serial_printf("Usage: %s -f fofn_base_name\n", argv[0]);
        upc_global_exit(1);
    }
    upc_tick_t t = upc_ticks_now();

    // delete any files from /dev/shm to ensure we have adequate space
    if (MYTHREAD % coresPerNode == 0 && purgeShm) {
        lld bytes = 0;
        int files = 0, dirs = 0;
        int purged = purgeDir("/dev/shm/per_rank", &bytes, &files, &dirs);
        if (purged) {
            LOG("purged %d files from /dev/shm/per_rank in %0.2f s (%0.3lf GB)\n",
                purged, ((double)upc_ticks_to_ns(upc_ticks_now() - t) / 1000000000.0), ((double)bytes) / ONE_GB);
            if (purged != files) {
                WARN("Could not purge %d files in /dev/shm/per_rank!\n", files - purged);
            }
        }
    }
    UPC_LOGGED_BARRIER;
    // now create the /dev/shm directories
    char my_dir[MAX_FILE_PATH];
    strcpy(my_dir, "/dev/shm/.");
    if (MYTHREAD % coresPerNode == 0) {
        get_rank_path(my_dir, -1);
    }
    UPC_LOGGED_BARRIER;
    strcpy(my_dir, "/dev/shm/.");
    get_rank_path(my_dir, MYTHREAD);

    serial_printf("Loading FASTQ files from %s\n", libnames_ext);
    get_library_names(libnames_ext);

    UPC_LOGGED_BARRIER;

    int64_t totalFileSize = 0;
    for (int libi = 0; libi < num_lib_names; libi++) {
        double start = now();
        totalFileSize += loadfq_library(library_names[libi], base_dir);
    }

    UPC_LOGGED_BARRIER;
    double secs = ((double)upc_ticks_to_ns(upc_ticks_now() - t) / 1000000000.0);
    serial_printf("Loaded %0.3f MB Globally (%0.3f MB/thread) and compressed at %0.3f MB/s Globally (%0.3f MB/s / thread)\n", totalFileSize * PER_MB, totalFileSize / THREADS * PER_MB, totalFileSize * PER_MB / secs, totalFileSize / THREADS * PER_MB / secs);
    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]), secs);
    return 0;
}
