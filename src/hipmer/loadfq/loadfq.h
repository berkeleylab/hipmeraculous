#ifndef LOADFQ_H_
#define LOADFQ_H_

#include <stdio.h>
#include <stdint.h>

#include "Buffer.h"

int64_t loadfq_library(const char *libname, const char * base_dir);
int loadfq_main(int argc, char **argv);

#endif // LOADFQ_H_
