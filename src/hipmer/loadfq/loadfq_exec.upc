#include <upc.h>

#include "upc_common.h"
#include "common.h"
#include "log.h"

int loadfq_main(int argc, char **argv);

int main(int argc, char **argv)
{
    OPEN_MY_LOG("loadfq");
    return loadfq_main(argc, argv);
}
