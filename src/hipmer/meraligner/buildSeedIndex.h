#ifndef __BUILD_SEED_INDEX_H
#define __BUILD_SEED_INDEX_H

#include <assert.h>

#include "StaticVars.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "hash_funcs.h"
#include "merAligner.h"
#include "kmer_hash2.h"
#include "utils.h"
#include "fasta.h"

extern StaticVars _sv;
extern int64_t seedsExtracted;
#define MAX_CONTIG_SIZE 900000

typedef shared [] LIST_T * SHARED_LIST_T;
typedef shared [] SHARED_LIST_T * SHARED_LIST_T_PTR;
/* Builds the distributed seed index in parallel */
HASH_TABLE_T *buildSeedIndex(int64_t size, char *inputContigsFileName, MEMORY_HEAP_T *memory_heap_res, int subContigEffectiveLength, int64_t IDoffset, int64_t myNSubcontigs, int read_length, int kmer_len, int cores_per_node, int avoid_masked, int allow_repetitive_seeds);

#endif
