#include "buildSeedIndex.h"

#define MAX_EXTRA_CONTIGS 10

/* Builds the distributed seed index in parallel */
HASH_TABLE_T *buildSeedIndex(int64_t size, char *inputContigsFileName, MEMORY_HEAP_T *memory_heap_res, int subContigEffectiveLength, int64_t IDoffset, int64_t myNSubcontigs, int read_length, int kmer_len, int cores_per_node, int avoid_masked, int allow_repetitive_seeds)
{
    HASH_TABLE_T *dist_hashtable;
    MEMORY_HEAP_T memory_heap;
    FASTAFILE *ffp;
    char *contigBuffer = NULL;
    char *contigId = NULL;
    int64_t contigID;
    int contigLen;

    shared[] contig_t * cur_contig = NULL;
    char *contig = NULL;
    shared[] char *contig_content = NULL;
    int lex_ind, foundOccurence;
    int64_t i;
    char INVALID_PACKED_KMER[MAX_KMER_PACKED_LENGTH];
    memset(INVALID_PACKED_KMER, 0xff, MAX_KMER_PACKED_LENGTH);
    char cur_kmer[MAX_KMER_SIZE], auxiliary_kmer[MAX_KMER_SIZE];
    char *kmer_to_store;
    memset(cur_kmer, 0, MAX_KMER_SIZE);       // initialize all bytes
    memset(auxiliary_kmer, 0, MAX_KMER_SIZE); // initialize all bytes
    auxiliary_kmer[kmer_len] = '\0';
    cur_kmer[kmer_len] = '\0';
    LIST_T new_entry;
    memset(&new_entry, 0, sizeof(LIST_T)); // initialilze all bytes
    int64_t hashval;
    int64_t store_pos;
    int remote_thread, bufferOffset, startingSeedingPoint, endingSeedingPoint;
    contigInfo new_contig_info;
    SHARED_LIST_T_PTR lastPtr;
    shared[] LIST_T * curPtr;
    LIST_T curPtrCopy;
    memset(&curPtrCopy, 0, sizeof(LIST_T)); // initialilze all bytes
    int64_t subcontigsStoredSofar = 0;
    int64_t nCurSubContigs;
    int64_t x;
    int64_t foundNs = 0, nContigs = 0, nSubContigs = 0;
    int actualLength;
    char *contig_id;
    size_t chunk_size = get_chunk_size(sizeof(LIST_T), cores_per_node);

    UPC_TICK_T start_read, end_read, start_storing, end_storing, start_setup, end_setup;
    UPC_TICK_T first_pass_start, first_pass_end, put_start, put_end;
    double put_profile = 0.0;

    /* Initialize lookup-table --> necessary for packing routines */
    init_LookupTable();

    UPC_LOGGED_BARRIER;

    /* First do a first pass over contigs/subconitgs to calculate exact requirements for seed heaps */
    ffp = OpenFASTA(inputContigsFileName);
    subcontigsStoredSofar = 0;
    double overlap = 2 * (read_length + SLACK) - kmer_len;
    double contigMemorySize = 0.0;
    /* Array to do the actual counting for memory requirements  */
    int64_t *kmer_owners;
    kmer_owners = (int64_t*) malloc (THREADS * sizeof(int64_t));
    memset(kmer_owners, 0, THREADS * sizeof(int64_t));

    while (subcontigsStoredSofar != myNSubcontigs && ReadFASTA(ffp, &contigBuffer, &contig_id, &contigLen)) {
        /* Chop the contig into smaller subpieces */
        nContigs++;
        if (subContigEffectiveLength <= 0) {
            nCurSubContigs = 1;
        } else {
            nCurSubContigs = (contigLen + subContigEffectiveLength - 1) / subContigEffectiveLength;
        }
        DBG2("contig_id=%lld contigLen=%d nCurSubContigs=%d subContigEffectiveLength=%d\n", (lld) contig_id, contigLen, nCurSubContigs, subContigEffectiveLength);
        nSubContigs += nCurSubContigs;
        for (x = 0; x < nCurSubContigs; x++) {
            if (nCurSubContigs == 1) {
                actualLength = contigLen;
            } else {
                /* The first  subcontig of a contig does not suffer from the "overlap" overhead */
                /* Note: OVERLAP should be 2*(read_length+slack) - kmer_len */
                assert(subContigEffectiveLength > 0);
                actualLength = (x == 0) ? subContigEffectiveLength : subContigEffectiveLength + overlap;
                if (x == (nCurSubContigs - 1)) {
                    actualLength = contigLen - subContigEffectiveLength * (nCurSubContigs - 1) + overlap;
                }
            }
            bufferOffset = (x == 0) ? 0 : x * subContigEffectiveLength - overlap;
            contig = (char*)contigBuffer + bufferOffset;
            startingSeedingPoint = (x == 0) ? 0 : read_length - kmer_len + SLACK;
            endingSeedingPoint = actualLength - 1 - (read_length + SLACK);
            if (x == (nCurSubContigs - 1)) {
                endingSeedingPoint = actualLength - kmer_len;
            }
            /* Mark the k-mers of the contig appropriately */
            for (i = startingSeedingPoint; i <= endingSeedingPoint; i++) {
                memcpy(cur_kmer, contig + i, kmer_len * sizeof(char));
                if (avoid_masked) {
                    /* Skip the seed if contains lower case base */
                    int lcFound = 0;
                    int pos;
                    for (pos = 0; pos < kmer_len; pos++) {
                        if (cur_kmer[pos] >= 'a' && cur_kmer[pos] <= 't') {
                            lcFound = 1;
                            break;
                        }
                    }
                    if (lcFound) {
                        i += pos;
                        continue;
                    }
                }

                // skip any 'N' in the target
                if (strchr(cur_kmer, 'N')) {
                    foundNs++;
                    continue;
                }

                /* Search for the canonical kmer */
                reverseComplementKmer(cur_kmer, auxiliary_kmer, kmer_len);
                lex_ind = strcmp(cur_kmer, auxiliary_kmer);
                if (lex_ind < 0) {
                    kmer_to_store = cur_kmer;
                } else {
                    kmer_to_store = auxiliary_kmer;
                }

                /* Pack and insert kmer info into local_list_t buffer */
                hashval = hashkey(size, (char *)kmer_to_store, kmer_len);
                remote_thread = hashval % THREADS;
                kmer_owners[remote_thread] += 1;
            }
        }
        free_chk0(contigBuffer);
        free_chk0(contig_id);
    }
    LOGF("Done reading fasta file (%lld Ns) nContigs=%lld nSubContigs=%lld subContigEffectiveLength=%d\n", (lld) foundNs, (lld) nContigs, (lld) nSubContigs, subContigEffectiveLength);
    int64_t totalNs = reduce_long(foundNs, UPC_ADD, SINGLE_DEST);
    int64_t nTotalContigs = reduce_long(nContigs, UPC_ADD, SINGLE_DEST);
    int64_t nTotalSubContigs = reduce_long(nSubContigs, UPC_ADD, SINGLE_DEST);
    serial_printf("Found %lld Ns in the %lld contigs, %lld sub-contigs\n", (lld) totalNs, (lld) nTotalContigs, (lld) nTotalSubContigs);

    if (ffp != NULL) {
        CloseFASTA(ffp);
    }

    /* Now reduce these k-mer numbers to get exact memory requirements  */
    shared[1] int64_t *heap_size_global = NULL;
    int64_t prevval;
    UPC_ALL_ALLOC_CHK(heap_size_global, THREADS, sizeof(int64_t));
    heap_size_global[MYTHREAD] = 0;
    UPC_LOGGED_BARRIER;
    for (i=0; i<THREADS; i++) {
      if (kmer_owners[i] > 0) {
          UPC_ATOMIC_FADD_I64(&prevval, &(heap_size_global[i]), kmer_owners[i]);
      }
    }
    UPC_LOGGED_BARRIER;
    int64_t myExactSeedToReceive = heap_size_global[MYTHREAD];
    UPC_ALL_FREE_CHK(heap_size_global);
    free(kmer_owners);
    UPC_LOGGED_BARRIER;

    /* Done with seed/heap calculations */
    start_setup = UPC_TICKS_NOW();
    /* Create and initialize hashtable */
// FIXME myExactSeedToReceive is zero when scaffEffectiveSize == 0!!!
    dist_hashtable = create_hash_table_MA_exact(size, myExactSeedToReceive, &memory_heap);
    end_setup = UPC_TICKS_NOW();
    serial_printf("Threads done with setting-up hash table %lld size\n", (lld)size);
    UPC_LOGGED_BARRIER;

    first_pass_start = UPC_TICKS_NOW();

    start_storing = UPC_TICKS_NOW();

    ffp = OpenFASTA(inputContigsFileName);
    subcontigsStoredSofar = 0;
    overlap = 2 * (read_length + SLACK) - kmer_len;
    contigMemorySize = 0.0;

    if (subContigEffectiveLength <= 0) {
      serial_printf("Skipping chopping optimization\n");
    }

    while (subcontigsStoredSofar != myNSubcontigs && ReadFASTA(ffp, &contigBuffer, &contig_id, &contigLen)) {
      /* Process a Contig */
      contigID = atol(contig_id + 7);  // Assume target names are in the form ContigXXXXXX

      /* Chop the contig into smaller subpieces */
      if (subContigEffectiveLength <= 0) {
        nCurSubContigs = 1;
      } else {
        assert(subContigEffectiveLength > 0);
        nCurSubContigs = (contigLen + subContigEffectiveLength - 1) / subContigEffectiveLength;
      }

      for (x = 0; x < nCurSubContigs; x++) {
        if (nCurSubContigs == 1) {
          actualLength = contigLen;
        } else {
          /* The first  subcontig of a contig does not suffer from the "overlap" overhead */
          /* Note: OVERLAP should be 2*(read_length+slack) - kmer_len */
          assert(subContigEffectiveLength > 0);
          actualLength = (x == 0) ? subContigEffectiveLength : subContigEffectiveLength + overlap;
          if (x == (nCurSubContigs - 1)) {
            actualLength = contigLen - subContigEffectiveLength * (nCurSubContigs - 1) + overlap;
          }
        }

        /* Allocate space for the contig in shared memory */
        DBG2("Allocating shared memory for contig %lld (%lld len) of %lld and %lld bytes\n", (lld)contigID, (lld)actualLength, (lld)sizeof(contig_t), (lld)(actualLength + 1) * sizeof(char));
        //cur_contig = (shared [] contig_t*) allocFromHeapList( memory_heap.contigs, sizeof(contig_t));
        SharedCharPtr _cur_contig = allocFromHeapList(memory_heap.contigs, sizeof(contig_t));
        cur_contig = (shared [] contig_t *)_cur_contig;
        assert(upc_threadof(cur_contig) == MYTHREAD);
        contig_content = (shared [] char *)allocFromHeapList(memory_heap.contig_content, (actualLength + 1) * sizeof(char));
        assert(upc_threadof(contig_content) == MYTHREAD);
        contigMemorySize += sizeof(contig_t) + actualLength + 1;
        bufferOffset = (x == 0) ? 0 : x * subContigEffectiveLength - overlap;
        memcpy((char *)contig_content, contigBuffer + bufferOffset, actualLength * sizeof(char));
        contig_content[actualLength] = '\0';
        cur_contig->contig = contig_content;
        cur_contig->contig_id = subcontigsStoredSofar + IDoffset;
        subcontigsStoredSofar++;
        cur_contig->length = actualLength;
        cur_contig->parentLength = contigLen;
        cur_contig->parentContigID = contigID;
        cur_contig->offsetInParent = bufferOffset;
        cur_contig->uuType = UU_CONTIG;
        contig = (char *)contig_content;

        startingSeedingPoint = (x == 0) ? 0 : read_length - kmer_len + SLACK;
        endingSeedingPoint = actualLength - 1 - (read_length + SLACK);
        if (x == (nCurSubContigs - 1)) {
          endingSeedingPoint = actualLength - kmer_len;
        }

        /* Mark the k-mers of the contig appropriately */
        for (i = startingSeedingPoint; i <= endingSeedingPoint; i++) {
          memcpy(cur_kmer, contig + i, kmer_len * sizeof(char));

          if (avoid_masked) {
            /* Skip the seed if contains lower case base */
            int lcFound = 0;
            int pos;
            for (pos = 0; pos < kmer_len; pos++) {
              if (cur_kmer[pos] >= 'a' && cur_kmer[pos] <= 't') {
                lcFound = 1;
                break;
              }
            }
            if (lcFound) {
              i += pos;
              continue;
            }
          }
          // skip any 'N' in the target
          if (strchr(cur_kmer, 'N')) {
              foundNs++;
              continue;
          }

          /* Search for the canonical kmer */
          reverseComplementKmer(cur_kmer, auxiliary_kmer, kmer_len);
          lex_ind = strcmp(cur_kmer, auxiliary_kmer);
          if (lex_ind < 0) {
            kmer_to_store = cur_kmer;
          } else {
            kmer_to_store = auxiliary_kmer;
          }

          /* Pack and insert kmer info into local_list_t buffer */
          hashval = hashkey(dist_hashtable->size, (char *)kmer_to_store, kmer_len);
          remote_thread = upc_threadof(&(dist_hashtable->table[hashval]));
          packSequence((unsigned char *)kmer_to_store, new_entry.packed_key, kmer_len);
          new_entry.next = NULL;
          new_entry.contigInfoArrayPtr = NULL;
          new_contig_info.posInContig = i;
          new_contig_info.my_contig = cur_contig;
          new_entry.firstContig = new_contig_info;
          new_entry.nExtraContigs = 0;
          new_entry.posInSubArray = 0;

          seedsExtracted++;

          put_start = UPC_TICKS_NOW();
          ADD_TO_HEAP(&memory_heap, new_entry, remote_thread);
          put_end = UPC_TICKS_NOW();
          put_profile += UPC_TICKS_TO_SECS(put_end - put_start);
        }
      }

      free_chk0(contigBuffer);
      free_chk0(contig_id);
    }
    LOGF("Done reading fasta file. (%lld Ns)\n", foundNs);

    if (ffp != NULL) {
      CloseFASTA(ffp);
    }
    //printf("Thread %d done with seeding part\n", MYTHREAD);
    /* Sanity check */
    if (subcontigsStoredSofar != myNSubcontigs) {
      DIE("Fatal ERROR: Incorrectly choped parent contig %lld VS %lld !\n", (lld)subcontigsStoredSofar, (lld)myNSubcontigs);
    }

    FINAL_FLUSH_HEAP(&memory_heap);
    FREE_LOCAL_BUFFERS(&memory_heap);
    LOGF("Done allocating my contigs %lld of %0.3f MB\n", (lld)myNSubcontigs, contigMemorySize / ONE_MB);
    UPC_LOGGED_BARRIER;
    first_pass_end = UPC_TICKS_NOW();
    serial_printf("Done allocating and reading contigs AND memput the seeds in shared heaps in %f seconds (%f seconds pure fadd and put)\n", UPC_TICKS_TO_SECS(first_pass_end - first_pass_start), put_profile);

    UPC_LOGGED_BARRIER;

    first_pass_start = UPC_TICKS_NOW();

    if (memory_heap.heap_struct[MYTHREAD].len > memory_heap.heap_ptr_sizes[MYTHREAD]) {
       DIE("Memory over run on thread %d: %lld vs %lld capacity. Increase the expansion factor for the meraligner dist hashtable!\n", MYTHREAD, (lld) memory_heap.heap_struct[MYTHREAD].len, (lld) memory_heap.heap_ptr_sizes[MYTHREAD]);
    }

    /* First pass over the seed heap to insert then first occurence of each seed in the hash-table and to count the number of occurences */

    int64_t heap_entry;
    int64_t recvMultipleLocs = 0, sendMultipleLocs = 0;

    shared[] LIST_T * local_filled_heap = (memory_heap.heap_struct[MYTHREAD].ptr);
    assert(upc_threadof(local_filled_heap) == MYTHREAD);
    unsigned char unpacked_kmer[MAX_KMER_SIZE];
    memset(unpacked_kmer, 0, MAX_KMER_SIZE); // initialize all bytes
    unpacked_kmer[kmer_len] = '\0';
    shared[] contigInfo * extraContigInfoArray = NULL;

    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
    LOGF("Processing %lld heap_indicies\n", (lld)memory_heap.heap_struct[MYTHREAD].len);
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
      unpackSequence((unsigned char *)&(local_filled_heap[heap_entry].packed_key), (unsigned char *)unpacked_kmer, kmer_len);
      hashval = hashstr(dist_hashtable->size, (char *)(unpacked_kmer));
      assert(hashval == hashkey(dist_hashtable->size, (char *)unpacked_kmer, kmer_len));
      /* Check if already an occurence of the seed exists in the hash table */
      DBG2("Processing heap_entry=%lld, kmer=%.*s, hashval=%lld\n", (lld)heap_entry, kmer_len, unpacked_kmer, (lld)hashval);
      if (upc_threadof(&(dist_hashtable->table[hashval])) != MYTHREAD) {
          DIE("Incorrect hashval %lld to thread %d - '%s'\n", (lld) hashval, upc_threadof(&(dist_hashtable->table[hashval])), (char*) unpacked_kmer);
      }
      assert(upc_threadof(&(dist_hashtable->table[hashval])) == MYTHREAD);
      curPtr = dist_hashtable->table[hashval].head;
      shared [] LIST_T *head = curPtr;
      foundOccurence = FALSE;
      while ((foundOccurence == FALSE) && (curPtr != NULL)) {
        curPtrCopy = *curPtr;
        if (head == curPtr && dist_hashtable->table[hashval].cachedEntryPtr != head) {
            // populate the cachedEntry in the bucket
            dist_hashtable->table[hashval].cachedEntry = curPtrCopy;
            dist_hashtable->table[hashval].cachedEntryPtr = curPtr;
            DBG3("Set cache for hashval=%lld\n", hashval);
        }

        if (curPtrCopy.firstContig.my_contig == NULL) {
          DIE("Invalid assumption - every kmer has a non-null firstContig.my_contig!\n");
        }
        if (memcmp((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)curPtrCopy.packed_key, kmer_packed_len * sizeof(unsigned char)) == 0) {
          assert(curPtr != &(local_filled_heap[heap_entry]));
          assert(curPtr->nExtraContigs >= 0);
          assert(local_filled_heap[heap_entry].nExtraContigs == 0);

          foundOccurence = TRUE;
          assert(upc_threadof(curPtr) == MYTHREAD);
          if (curPtr->nExtraContigs < MAX_EXTRA_CONTIGS) {
            int oldExtra = (curPtr->nExtraContigs)++;
            assert(oldExtra + 1 == curPtr->nExtraContigs);
            //            if (oldExtra + 1 != curPtr->nExtraContigs)
            //                DIE("failed assertion: oldExtra %d, curPtr->nExtraContigs %d\n", oldExtra, curPtr->nExtraContigs);
            assert(local_filled_heap[heap_entry].nExtraContigs == 0);
            local_filled_heap[heap_entry].nExtraContigs = -1; // Mark that this entry contains additional info!!!
            sendMultipleLocs++;
          }
        } else {
          curPtr = curPtrCopy.next;
        }
      }

      /* If not already in the hashtable, add it there */
      if (foundOccurence == FALSE) {
        assert(upc_threadof(&(dist_hashtable->table[hashval])) == MYTHREAD);
        assert(upc_threadof(&local_filled_heap[heap_entry]) == MYTHREAD);
        local_filled_heap[heap_entry].next = dist_hashtable->table[hashval].head;
        head = dist_hashtable->table[hashval].head = &local_filled_heap[heap_entry];

        // populate the cachedEntry in the bucket
        dist_hashtable->table[hashval].cachedEntry = local_filled_heap[heap_entry];
        dist_hashtable->table[hashval].cachedEntryPtr = head;
        DBG3("Set cache for hashval=%lld\n", hashval);

      }
    }
    LOGF("Completed first pass\n");

    first_pass_end = UPC_TICKS_NOW();
    serial_printf("Done first pass over heap in %f seconds and redjusted hash table kmer pointers\n", UPC_TICKS_TO_SECS(first_pass_end - first_pass_start));
    first_pass_start = UPC_TICKS_NOW();

    /* Second pass over the seed heap to count the number of extraContigs to allocate */
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
      assert(upc_threadof(&local_filled_heap[heap_entry]) == MYTHREAD);
      if (local_filled_heap[heap_entry].nExtraContigs > 0) {
        recvMultipleLocs += local_filled_heap[heap_entry].nExtraContigs;
      }
    }

    first_pass_end = UPC_TICKS_NOW();
    serial_printf("Done second pass over heap in %f seconds \n", UPC_TICKS_TO_SECS(first_pass_end - first_pass_start));
    first_pass_start = UPC_TICKS_NOW();

    LOGF("Completed second pass to count extraContigs\n");
    assert(recvMultipleLocs == sendMultipleLocs); /* all operations are local */
    DBG("sending %lld, receiving %lld\n", (lld)sendMultipleLocs, (lld)recvMultipleLocs);

    /* Allocate an array for the extra contigs */
    assert(memory_heap.contigInfoArrays[MYTHREAD] == NULL);
    if (recvMultipleLocs != 0) {
      LOGF("Allocating %lld extra contigs %0.3f MB\n", (lld)recvMultipleLocs, 1.0 / ONE_MB * (recvMultipleLocs * sizeof(contigInfo)));
      UPC_ALLOC_CHK(extraContigInfoArray, recvMultipleLocs * sizeof(contigInfo));
      assert(memory_heap.contigInfoArrays[MYTHREAD] == NULL);
      memory_heap.contigInfoArrays[MYTHREAD] = extraContigInfoArray;
    }
    LOGF("Allocated %lld extra contigs %0.3f MB (%0.3f%% entries are duplicated and %s)\n", (lld)recvMultipleLocs, ((double)recvMultipleLocs) * sizeof(contigInfo) / ONE_MB, 100.0 * sendMultipleLocs / memory_heap.heap_struct[MYTHREAD].len, allow_repetitive_seeds ? " will be used" : " will be purged");
    int64_t posInBuffer = 0;
    //printf("Thread ID is %d identified %lld multiple locations\n", MYTHREAD, nMultipleLocs);

    /* Third pass over the seed heap to allocate appropriately the information for the contigInfoArray from allocation */
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
      /* Should do some action only if extra contigs exist and this is the instance in the table */
      if (local_filled_heap[heap_entry].nExtraContigs > 0) {
        assert(upc_threadof(local_filled_heap + heap_entry) == MYTHREAD);
        assert(posInBuffer + local_filled_heap[heap_entry].nExtraContigs <= recvMultipleLocs);
        /* We should do the allocation - This is the first occurence of the seed */
        assert(extraContigInfoArray != NULL);
        assert(upc_threadof(&(extraContigInfoArray[posInBuffer])) == MYTHREAD);
        local_filled_heap[heap_entry].contigInfoArrayPtr = (shared[] contigInfo *) & (extraContigInfoArray[posInBuffer]);
        posInBuffer += local_filled_heap[heap_entry].nExtraContigs;
        assert(local_filled_heap[heap_entry].posInSubArray == 0);
      }
    }
    LOGF("Finished Third pass\n");
    first_pass_end = UPC_TICKS_NOW();
    serial_printf("Done third pass over heap in %f seconds \n", UPC_TICKS_TO_SECS(first_pass_end - first_pass_start));
    first_pass_start = UPC_TICKS_NOW();
    assert(posInBuffer == recvMultipleLocs);
    int64_t sent = 0;

    /* Fourth pass over the seed heap to insert appropriately the information for the contigInfoArray */
    for (heap_entry = 0; heap_entry < memory_heap.heap_struct[MYTHREAD].len; heap_entry++) {
      if (local_filled_heap[heap_entry].nExtraContigs == -1) {
        /* This includes extra contig info, so find the appropriate entry in the hash table and add that info */
        unpackSequence((unsigned char *)&(local_filled_heap[heap_entry].packed_key), (unsigned char *)unpacked_kmer, kmer_len);
        hashval = hashstr(dist_hashtable->size, (char *)(unpacked_kmer));
        lastPtr = &(dist_hashtable->table[hashval].head);
        curPtr = dist_hashtable->table[hashval].head;
        if (allow_repetitive_seeds) {
            assert( curPtr != NULL );
            assert( upc_threadof(curPtr) == MYTHREAD );
        }
        foundOccurence = FALSE;
        while ((foundOccurence == FALSE) && (curPtr != NULL)) {
          curPtrCopy = *curPtr;
          assert( curPtrCopy.firstContig.my_contig != NULL );
          assert(upc_threadof(local_filled_heap[heap_entry].packed_key) == MYTHREAD);
          if (memcmp((unsigned char *)local_filled_heap[heap_entry].packed_key, (unsigned char *)curPtrCopy.packed_key, kmer_packed_len * sizeof(unsigned char)) == 0) {
            assert(curPtrCopy.contigInfoArrayPtr != NULL);
            assert(curPtrCopy.contigInfoArrayPtr == curPtr->contigInfoArrayPtr);
            foundOccurence = TRUE;
            assert(upc_threadof(curPtr) == MYTHREAD);
            if (allow_repetitive_seeds) {
              int posInSubArray = (curPtr->posInSubArray)++; // no need for atomic. this thread is only one with this kmer
              assert(posInSubArray == curPtrCopy.posInSubArray);
              assert(posInSubArray + 1 == curPtr->posInSubArray);
              assert(posInSubArray < curPtrCopy.nExtraContigs);
              assert(upc_threadof(&(curPtrCopy.contigInfoArrayPtr[posInSubArray])) == MYTHREAD);
              assert(upc_threadof(&(local_filled_heap[heap_entry].firstContig)) == MYTHREAD);
              //curPtrCopy.contigInfoArrayPtr[posInSubArray] = local_filled_heap[heap_entry].firstContig;
              memcpy((void *)&(curPtrCopy.contigInfoArrayPtr[posInSubArray]), (void *)&local_filled_heap[heap_entry].firstContig, sizeof(contigInfo));
              assert(curPtrCopy.contigInfoArrayPtr[posInSubArray].my_contig != NULL);
              sent++;
            } else {
              // delete this seed by invaliding the packed kmer so no match is possible and splicing it out of the linked list in the hash table
              local_filled_heap[heap_entry].nExtraContigs = 0;
              memcpy((void*) local_filled_heap[heap_entry].packed_key, INVALID_PACKED_KMER, kmer_packed_len);
              curPtr->nExtraContigs = 0;
              memcpy((void*) curPtr->packed_key, INVALID_PACKED_KMER, kmer_packed_len);
              *lastPtr = curPtr->next;
            }
          } else {
            lastPtr = &(curPtr->next);
            curPtr = curPtrCopy.next;
          }
        }
        if (allow_repetitive_seeds) { assert(foundOccurence); }
      }
      /* If nExtraContigs != 0 then mark the corresponding contig as NON_UU_CONTIG */
      if (local_filled_heap[heap_entry].nExtraContigs != 0) {
        int64_t dummy;
        UPC_ATOMIC_CSWAP_I32(&dummy, &(local_filled_heap[heap_entry].firstContig.my_contig->uuType), UU_CONTIG, NON_UU_CONTIG);
      }
    }
    LOGF("Finished Fourth pass\n");
    first_pass_end = UPC_TICKS_NOW();
    serial_printf("Done fourth pass over heap in %f seconds \n", UPC_TICKS_TO_SECS(first_pass_end - first_pass_start));
    if (allow_repetitive_seeds) { assert(sent == sendMultipleLocs); }
    upc_fence;
    end_storing = UPC_TICKS_NOW();

#ifdef PROFILE
    UPC_LOGGED_BARRIER;

    if (MYTHREAD == 0) {
      printf("\n************* SET - UP TIME *****************");
      printf("\nTime spent on setting up the distributed hash table is %d seconds\n", ((int)UPC_TICKS_TO_SECS(end_setup - start_setup)));
    }
#endif

    int64_t totalEntries = reduce_long(memory_heap.heap_struct[MYTHREAD].len, UPC_ADD, SINGLE_DEST);
    int64_t totaldups = reduce_long(sendMultipleLocs, UPC_ADD, SINGLE_DEST);
    serial_printf("Distributed kmer table has %lld entries, %lld duplicated (%0.3f%%)%s\n", (lld) totalEntries, (lld) totaldups, 100.0 * totaldups / totalEntries, (totaldups == 0 || allow_repetitive_seeds) ? "" : " dups are PURGED");
    UPC_LOGGED_BARRIER;

    //free_chk(local_buffs);
    //free_chk(local_index);
    (*memory_heap_res) = memory_heap;
    return dist_hashtable;
}
