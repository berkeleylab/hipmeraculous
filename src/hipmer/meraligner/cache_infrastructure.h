#ifndef __CACHE_INFRASTRUCTURE_H
#define  __CACHE_INFRASTRUCTURE_H

#include "upc_common.h"
#include "common.h"
#include "StaticVars.h"
#include "hash_funcs.h"
#include "upc_atomic_data.h"
#include "merAligner.h"
#include "timers.h"

#define MAX_CONTIG_METADATA_SLOTS 25165843 /* a large but not too large prime number */

typedef shared[] char *contigDataPtr;
typedef shared[] LIST_T kmerData;
typedef shared[] LIST_T *kmerDataPtr;

// contig cache
typedef struct {
    contigDataPtr contigAndSeq;
    shared[] contig_t * contig_ptr;
} contigCacheData;

INIT_ATOMIC_DATA(contigCache, contigCacheData)

enum CacheResultType {
    SET, HIT, MISS, CLASH, ON_NODE, CACHE_FULL, NOTHING_TO_CACHE, MAX_CACHE_RESULT
};

#define SET_CACHE_RESULT(countTimers, resultType, time) addCountTime(countTimers + resultType, time)

typedef struct __contig_cache_t {
    shared [1] int64_t * cacheSizesArray;
    shared [1] contigCache_AtomicData * ccAD;
    int64_t nContigsIn;
    int64_t cacheSize; // per node
    int64_t cacheTablesSize; // global
    int useHash;
    CountTimer countTimers[MAX_CACHE_RESULT];
} _contig_cache_t;
typedef _contig_cache_t *contig_cache_t;

/* Collective function that creates the cache infrastructure */
contig_cache_t createContigCache(int64_t nContigsIn, int64_t capacityPerCache);

void freeContigCache(contig_cache_t *_contigCache);

contigDataPtr findRemoteContigPtr(contig_cache_t contigCache, shared[] contig_t *contig_ptr, contig_t *local_contig_copy);

// kmer cache
INIT_ATOMIC_DATA(kmerCache, LIST_T)

typedef struct __kmer_cache_t {
    shared[1] kmerCache_AtomicData * kcAD;
    int64_t cacheCapacity;
    int64_t cacheTablesSize;
    CountTimer countTimers[MAX_CACHE_RESULT];
} _kmer_cache_t;
typedef _kmer_cache_t *kmer_cache_t;

/* Collective function that creates the cache infrastructure for k-mers */
/* Returns the number of slots for k-mers per node cache */
kmer_cache_t createKmerCache(int64_t capacityPerCache);
void destroyKmerCache(kmer_cache_t *kmerCache);

/* Returns a pointer to the requested k-mer. If the k-mer is cached then the pinter is node-local. If not, then the processor tries to cache the kmer in the node-local cache. Eventually the pointer is a valid one shared ptr to the requested k-mer (local or not)  */
kmerDataPtr lookupKmerInCache(HASH_TABLE_T *hashtable, kmer_cache_t kmerCache, unsigned char *kmer, int kmer_len, LIST_T *cached_kmer);

#endif
