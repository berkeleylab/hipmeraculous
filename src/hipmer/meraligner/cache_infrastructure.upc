#include "cache_infrastructure.h"

#include "kmer_hash2.h"
#include "upc_atomic_data.h"
#include "timers.h"

// implementations for contig and kmer AtomicData api
ADD_ATOMIC_TYPED_DEFS(contigCache, contigCacheData)
ADD_ATOMIC_TYPED_DEFS(kmerCache, LIST_T)

/* Collective function that creates the cache infrastructure */
contig_cache_t createContigCache(int64_t nContigsIn, int64_t capacityPerCache)
{
    UPC_TICK_T start, end;

    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    int use_hash = 1; // always use hash now: (nContigsIn + 2 * MYSV.cores_per_node < MAX_CONTIG_METADATA_SLOTS) ? 0 : 1;
    int64_t nEffectiveContigs = ((nContigsIn * 3 + MYSV.cores_per_node - 1) / MYSV.cores_per_node) * MYSV.cores_per_node + MYSV.cores_per_node;
    int64_t nContigs = nEffectiveContigs > MAX_CONTIG_METADATA_SLOTS ? MAX_CONTIG_METADATA_SLOTS : nEffectiveContigs;
    shared[1] contigDataPtr * tmpCacheTable = NULL;
    shared[1] int64_t * tmpIdMetaData = NULL;
    contig_cache_t contigCache;


    int64_t i;
    int64_t myNode = MYTHREAD / MYSV.cores_per_node;
    int64_t nodes = (int64_t)ceil((double)THREADS / (double)MYSV.cores_per_node);
    int64_t cacheTablesSize = nodes * nContigs + THREADS;
    float MB_per_thread = 1.0 / ONE_MB * (cacheTablesSize * sizeof(contigCache_AtomicData) / THREADS);
    LOGF("createCache(nContigsIn=%lld, Effective nContigs=%lld, capacityPerCache=%lld, use_hash=%d): %0.3f MB per thread\n", (lld) nContigsIn, (lld)nContigs, (lld)capacityPerCache, (int)use_hash, MB_per_thread);

    start = UPC_TICKS_NOW();

    contigCache = (contig_cache_t)calloc_chk(1, sizeof(_contig_cache_t));
    contigCache->nContigsIn = nContigsIn;
    contigCache->cacheSize = nContigs;
    contigCache->cacheTablesSize = cacheTablesSize;
    contigCache->useHash = use_hash;

    /* Create cache tables that will contain pointers to node-local contigs */
    serial_printf("Cache size meta-data is %f GB globally\n", (cacheTablesSize * sizeof(contigCache_AtomicData)) / 1024.0 / 1024.0 / 1024.0);
    UPC_ALL_ALLOC_CHK(contigCache->ccAD, cacheTablesSize, sizeof(contigCache_AtomicData));

    if (contigCache->ccAD == NULL) {
        DIE("Failed to allocate %lld of %llu for cacheTable\n", (lld)cacheTablesSize, (llu)sizeof(contigCache_AtomicData));
    }

    /* Create array with int values that indicate the current size of node-local metadata heap */
    UPC_ALL_ALLOC_CHK(contigCache->cacheSizesArray, THREADS, sizeof(int64_t));
    (contigCache->cacheSizesArray)[MYTHREAD] = capacityPerCache / MYSV.cores_per_node; // capacityPerCache is a per-node amount

    end = UPC_TICKS_NOW();
    serial_printf("Done allocating the contig caches in %f seconds (%0.3f MB/node)\n", UPC_TICKS_TO_SECS(end - start), MB_per_thread*MYSV.cores_per_node);
    start = UPC_TICKS_NOW();

    contigCache_AtomicData empty;
    contigCache_initAtomicData(&empty);
    for (i = MYTHREAD; i < cacheTablesSize; i += THREADS) {
        contigCache->ccAD[i] = empty;
    }

    end = UPC_TICKS_NOW();
    serial_printf("Done NULL-ing the caches with %d payloadSize in %f seconds\n", empty.checksum.csv.payloadSize, UPC_TICKS_TO_SECS(end - start));
    start = UPC_TICKS_NOW();

    upc_barrier;
    end = UPC_TICKS_NOW();
    serial_printf("Time AFTER NULL-ing the caches + synchronization %f seconds\n", UPC_TICKS_TO_SECS(end - start));

    return contigCache;
}

void freeContigCache(contig_cache_t *_contigCache)
{
    if (_contigCache == NULL || *_contigCache == NULL) {
        WARN("Duplicate call to freeContigCache!\n");
        return;
    }
    contig_cache_t contigCache = *_contigCache;
    LOGF("freeContigCache()\n");
    UPC_LOGGED_BARRIER;
    for (int64_t i = MYTHREAD; i < contigCache->cacheTablesSize; i += THREADS) {
        if (contigCache->ccAD[i].payload.contigAndSeq) {
            UPC_FREE_CHK0(contigCache->ccAD[i].payload.contigAndSeq);
        }
    }
    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(contigCache->ccAD);
    UPC_ALL_FREE_CHK(contigCache->cacheSizesArray);
    CountTimer *ct = contigCache->countTimers;
    int64_t totalQueries = 0;
    double totalTime = 0.0;
    for(int i = 0; i < (int) MAX_CACHE_RESULT; i++) {
        totalQueries += ct[i].count;
        totalTime += ct[i].time;
    }
    LOGF("contigCache stats: queries=%lld %0.3fs, sets=%lld %0.3fs, hits=%lld %0.3fs, misses=%lld %0.3fs, clashes=%lld %0.3fs, already_on_node=%lld %0.3fs, full=%lld %0.3fs, nothing=%lld %0.3fs\n",
         (lld) totalQueries, totalTime,
         (lld) ct[SET].count, ct[SET].time,
         (lld) ct[HIT].count, ct[HIT].time,
         (lld) ct[MISS].count, ct[MISS].time,
         (lld) ct[CLASH].count, ct[CLASH].time,
         (lld) ct[ON_NODE].count, ct[ON_NODE].time,
         (lld) ct[CACHE_FULL].count, ct[CACHE_FULL].time,
         (lld) ct[NOTHING_TO_CACHE].count, ct[NOTHING_TO_CACHE].time);
    free_chk(contigCache);
    *_contigCache = NULL;
}

contigDataPtr findRemoteContigPtr(contig_cache_t contigCache, shared[] contig_t *contig_ptr, contig_t *local_contig_copy)
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    if (contig_ptr == NULL) {
        DIE("findRemoteContigPtr given null contig_ptr!\n");
    }
    int use_hash = contigCache->useHash;
    assert(use_hash);
    // get the node-local hash location.  cacheSize is the size per node
    int64_t contigRequested = hashkey(contigCache->cacheSize, &contig_ptr, sizeof(shared[] contig_t *));
    if (contigRequested < 0) {
        DIE("findRemoteContigPtr(contigRequested=%d:%lld) -- invalid contig\n", upc_threadof(contig_ptr), (lld) upc_addrfield(contig_ptr));
    }
    UPC_TICK_T startTick = UPC_TICKS_NOW();
    enum CacheResultType resultType = MISS;
    contigDataPtr resultPtr = NULL;

    /* Look up node-local cache */
    int64_t chunk = contigRequested / MYSV.cores_per_node;
    int64_t offset = contigRequested % MYSV.cores_per_node;
    int64_t nid = MYTHREAD / MYSV.cores_per_node;
    int64_t nodes = (THREADS + MYSV.cores_per_node - 1) / MYSV.cores_per_node;
    int64_t loc = nid * MYSV.cores_per_node + chunk * ((int64_t)THREADS) + offset;
    int copied = 0;
    if (loc < 0 || loc >= contigCache->cacheTablesSize) DIE("Invalid loc=%lld: cacheSize=%lld cacheTablesSize=%lld, contigRequested=%lld\n", (lld) loc, (lld) contigCache->cacheSize, (lld) contigCache->cacheTablesSize, (lld) contigRequested);
    assert(loc >= 0);
    if ( !(upc_threadof(&(contigCache->ccAD[loc])) >= nid * MYSV.cores_per_node && upc_threadof(&(contigCache->ccAD[loc])) < nid * MYSV.cores_per_node + MYSV.cores_per_node) ) DIE("loc=%lld is not located on this node=%d th=%d-%d it is on thread=%d\n", (lld) loc, nid, nid*MYSV.cores_per_node, (nid+1)*MYSV.cores_per_node -1, upc_threadof(&(contigCache->ccAD[loc])));
    assert(upc_threadof(&(contigCache->ccAD[loc])) >= nid * MYSV.cores_per_node && upc_threadof(&(contigCache->ccAD[loc])) < nid * MYSV.cores_per_node + MYSV.cores_per_node);
    shared[] char *tmp_res = NULL;

    assert(loc >= 0);

    if (upc_threadof(contig_ptr) / MYSV.cores_per_node == nid) {
        // no need to cache a node-local contig
        *local_contig_copy = *contig_ptr;
        copied = 1;
        resultPtr = (contigDataPtr)(&(local_contig_copy->contig[0]));
        resultType = ON_NODE;
    } else {
        // first test the cache (non-atomic upc_memget)
        uint16_t versionRequested = getVersionAtomicData(contigRequested);
        contigCache_AtomicData ccAD;
        contigCache_initAtomicData(&ccAD);
        if ( &ccAD == contigCache_tryGetAtomicData(&ccAD, &(contigCache->ccAD[loc]), __FILENAME__, __LINE__) ) {
            // Got a valid record
            if (ccAD.checksum.csv.version == versionRequested && ccAD.payload.contig_ptr == contig_ptr) {
                // CACHE HIT
                resultPtr = ccAD.payload.contigAndSeq;
                if (resultPtr == NULL) {
                    /* Case where the actual contig pointer has not been written yet, it's OK to use remote pointer... pretend it is a cache miss */
                    WARN("this still should not happen\n");
                } else {
                    upc_memget(local_contig_copy, resultPtr, sizeof(contig_t));
                    copied = 1;
                    resultPtr = resultPtr + sizeof(contig_t);
                    resultType = HIT;
                }
            }
        } else if (contigCache_isEmptyAtomicData( &ccAD ) ) {
            AtomicDataChecksum empty  = ccAD.checksum;
            *local_contig_copy = *contig_ptr; // network copy
            copied = 1;
            int contigLength = local_contig_copy->length;
            /* Check if there is sufficient size in the thread-local cache */
            if (contigCache->cacheSizesArray[MYTHREAD] > contigLength + sizeof(contig_t)) {
                // attempt to fill the cache
                // allocate and set the data
                UPC_ALLOC_CHK0(tmp_res, contigLength * sizeof(char) + sizeof(contig_t));
                contigCacheData payload;
                payload.contig_ptr = contig_ptr;
                payload.contigAndSeq = tmp_res;
                // put contig_t in the cache
                *((contig_t*) tmp_res) = *local_contig_copy;
                // append the sequence to the cache
                upc_memget(((char *)tmp_res) + sizeof(contig_t), &(local_contig_copy->contig[0]), contigLength * sizeof(char));
                contigCache_setAtomicData( &ccAD, &payload, versionRequested, __FILENAME__, __LINE__ );
                if (contigCache_tryPutAtomicData_Near(&(contigCache->ccAD[loc]), &ccAD, empty, __FILENAME__, __LINE__)) {
                    // Just won the cswap, so all is good
                    contigCache->cacheSizesArray[MYTHREAD] -= contigLength - sizeof(contig_t); // decrement the remaining space
                    assert (contigCache->cacheSizesArray[MYTHREAD] >= 0);
                    resultPtr = tmp_res + sizeof(contig_t);
                    resultType = SET;
                } else { 
                    // lost the cswap, release the memory that will not be used
                    UPC_FREE_CHK0(tmp_res);
                    resultType = CLASH;
                }
            } else {
                resultType = CACHE_FULL;
            }
        }
        if (resultPtr == NULL) {
            // default: no good hit, return pointer to remote
            if (!copied) {
                *local_contig_copy = *contig_ptr; // network copy
                copied = 1;
            }
            resultPtr = (contigDataPtr)(&(local_contig_copy->contig[0]));
        }
    }

    SET_CACHE_RESULT(contigCache->countTimers, resultType, ELAPSED_TIME(startTick));
    assert(resultPtr != NULL);
    return resultPtr;
}

/* Collective function that creates the cache infrastructure for k-mers */
/* Returns the number of slots for k-mers per node cache */
kmer_cache_t createKmerCache(int64_t capacityPerCache)
{
    upc_tick_t start = upc_ticks_now();
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    kmer_cache_t kmerCache = (kmer_cache_t)calloc_chk(1, sizeof(_kmer_cache_t));

    int64_t datasize = sizeof(kmerCache_AtomicData);
    int64_t nSlots = (int64_t)floor((double)capacityPerCache / (double)datasize);
    nSlots = (nSlots / MYSV.cores_per_node) * MYSV.cores_per_node + MYSV.cores_per_node;
    int64_t nSlotsPerthread = (int64_t)floor((double)nSlots / (double)MYSV.cores_per_node);

    int64_t i;
    int64_t myNode = MYTHREAD / MYSV.cores_per_node;
    int64_t nodes = (int64_t)ceil((double)THREADS / (double)MYSV.cores_per_node);
    int64_t cacheTablesSize = nodes * nSlots + THREADS;
    kmerCache->cacheTablesSize = cacheTablesSize;

    /* Create cache tables that will contain pointers to node-local contigs */
    serial_printf("Cache table and cache flags is %f Gigabytes (%f / node), nslots per node %lld\n",
                  (cacheTablesSize * datasize) / 1024.0 / 1024.0 / 1024.0,
                  (cacheTablesSize * datasize) / 1024.0 / 1024.0 / 1024.0 / (THREADS / MYSV.cores_per_node), (lld)nSlots);

    UPC_ALL_ALLOC_CHK(kmerCache->kcAD, cacheTablesSize, datasize);
    if (kmerCache->kcAD == NULL) {
        DIE("Failed to allocate %lld kmer cache table entries\n", (lld)cacheTablesSize);
    }

    /* Initialize with guard values */
    kmerCache_AtomicData empty;
    kmerCache_initAtomicData( &empty );
    for (i = 0; i < nSlotsPerthread; i++) {
        int64_t offset = MYTHREAD + i * ((int64_t)THREADS);
        shared[1] kmerCache_AtomicData *ad = &(kmerCache->kcAD[offset]);
        assert(upc_threadof( ad ) == MYTHREAD);
        *ad = empty;
    }

    upc_barrier;

    serial_printf("Kmer Cache initialized in %0.3f s with %d payloadSize and %lld slots %0.3f MB per node\n", ELAPSED_TIME(start), empty.checksum.csv.payloadSize, (lld) nSlots, datasize * nSlots * PER_MB / nodes);
    kmerCache->cacheCapacity = nSlots;

    return kmerCache;
}


void destroyKmerCache(kmer_cache_t *_kmerCache)
{
    if (_kmerCache == NULL || *_kmerCache == NULL) {
        DIE("duplicate call to destroyKmerCache!\n");
    }
    kmer_cache_t kmerCache = *_kmerCache;
    UPC_ALL_FREE_CHK(kmerCache->kcAD);
    CountTimer *ct = kmerCache->countTimers;
    int64_t totalQueries = 0;
    double totalTime = 0.0;
    for(int i = 0; i < (int) MAX_CACHE_RESULT; i++) {
        totalQueries += ct[i].count;
        totalTime += ct[i].time;
    }
    LOGF("kmerCache stats: queries=%lld %0.3fs, sets=%lld %0.3fs, hits=%lld %0.3fs, misses=%lld %0.3fs, clashes=%lld %0.3fs, already_on_node=%lld %0.3fs, full=%lld %0.3fs, nothing=%lld %0.3fs\n",
         (lld) totalQueries, totalTime,
         (lld) ct[SET].count, ct[SET].time,
         (lld) ct[HIT].count, ct[HIT].time,
         (lld) ct[MISS].count, ct[MISS].time,
         (lld) ct[CLASH].count, ct[CLASH].time,
         (lld) ct[ON_NODE].count, ct[ON_NODE].time,
         (lld) ct[CACHE_FULL].count, ct[CACHE_FULL].time,
         (lld) ct[NOTHING_TO_CACHE].count, ct[NOTHING_TO_CACHE].time);
    free_chk(kmerCache);
    *_kmerCache = NULL;
}

/* Returns a pointer to the requested k-mer. If the k-mer is cached then the pinter is node-local. If not, then the processor tries to cache the kmer in the node-local cache. Eventually the pointer is a valid one shared ptr to the requested k-mer (local or not)  */
kmerDataPtr lookupKmerInCache(HASH_TABLE_T *hashtable, kmer_cache_t kmerCache, unsigned char *kmer, int kmer_len, LIST_T *cached_kmer)
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    UPC_TICK_T startTick = UPC_TICKS_NOW();
    enum CacheResultType resultType = MISS;
    assert(hashtable != NULL);
    assert(kmer != NULL);
    assert(kmerCache != NULL);
    int64_t cacheCapacity = kmerCache->cacheCapacity;
    assert(cacheCapacity > 0);
    /* Look up node-local cache */
    int64_t kmerRequested = hashkey(cacheCapacity, (void *)kmer, kmer_len);
    assert(kmerRequested < cacheCapacity);
    assert(kmerRequested >= 0);
    int64_t chunk = kmerRequested / MYSV.cores_per_node;
    int64_t offset = kmerRequested % MYSV.cores_per_node;
    int64_t nid = MYTHREAD / MYSV.cores_per_node;
    int64_t nodes = (int64_t)ceil((double)THREADS / (double)MYSV.cores_per_node);
    int64_t loc = nid * MYSV.cores_per_node + chunk * ((int64_t)THREADS) + offset;
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH];
    uint16_t atomicVersion = getVersionAtomicData(kmerRequested);
    if (loc < 0 || loc >= kmerCache->cacheTablesSize) DIE("invalid loc=%lld cacheCapacity=%lld cacheTablesSize=%lld\n", (lld) loc, (lld) kmerCache->cacheCapacity, (lld) kmerCache->cacheTablesSize);
    int nodeThread = upc_threadof(&kmerCache->kcAD[loc]);
    if (nodeThread < nid*MYSV.cores_per_node || nodeThread >= (nid+1)*MYSV.cores_per_node) DIE("loc=%lld is not on node=%d, it is on thread=%d\n", (lld) loc, nid, nodeThread);

    kmerDataPtr resultPtr = NULL;
    LIST_T localKmer;
    memset(&localKmer, 0, sizeof(LIST_T));         // initialize memory
    memset(packed_key, 0, MAX_KMER_PACKED_LENGTH); // initialize all bytes

    assert(kmerCache->kcAD != NULL);

    // First get the whole record (non-atomic upc_memget)
    kmerCache_AtomicData kcAD;
    kmerCache_initAtomicData(&kcAD);
    if (kmerCache_tryGetAtomicData( &kcAD, &kmerCache->kcAD[loc], __FILENAME__, __LINE__ ) == &kcAD ) {
        // successful atomic copy 
        if (kcAD.checksum.csv.version == atomicVersion) {
            /* The cache-slot is not empty and partially matches the hash - check if this is the requested kmer */
            localKmer = kcAD.payload;
            packSequence(kmer, packed_key, kmer_len);
            int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);
            if (comparePackedSeq(packed_key, localKmer.packed_key, kmer_packed_len) == 0) {
                /* This is indeed the requested k-mer, return the cached entry */
                (*cached_kmer) = localKmer;
                resultPtr = (kmerDataPtr) & (kmerCache->kcAD[loc].payload);
                resultType = HIT;
                assert(resultPtr != NULL);
                assert( upc_threadof( resultPtr ) / MYSV.cores_per_node == nid );
            } else {
                /* cache miss - the slot is occupied by another k-mer than the one requested... */
            }
        } else {
            /* cache miss too */
        }
    }

    if (resultPtr == NULL) {
        // look up this miss, if it exists
        resultPtr = lookup_kmer_MA(hashtable, kmer, kmer_len, cached_kmer);
    }
    if (resultPtr == NULL) {
        /* The k-mer was not found in the global hash table, so return NULL     */
        resultType = NOTHING_TO_CACHE;
    } else {
        // there was a kmer match, verify the contig is okay
        if (cached_kmer->firstContig.my_contig == NULL) {
            DIE("cached_kmer.firstContig.my_contig is null! kmer='%.*s' real: %s firstCopy: %s kmerRequested=%lld kmer is on thread %d\n", kmer_len, kmer, resultPtr->firstContig.my_contig == NULL ? "is null indeed" : "is NOT NULL", cached_kmer->firstContig.my_contig == NULL ? "is NULL" : "is NOT NULL", (lld) kmerRequested, upc_threadof(resultPtr));
        }
    }

    if (resultPtr != NULL && kmerCache_isEmptyAtomicData( &kcAD ) ) {
        AtomicDataChecksum empty = kcAD.checksum;
        /* store this hit, if off node */
        if (upc_threadof(resultPtr) / MYSV.cores_per_node == nid) {
            resultType = ON_NODE;
        } else {
            kmerCache_setAtomicData( &kcAD, cached_kmer, atomicVersion, __FILENAME__, __LINE__ );
            if ( kmerCache_tryPutAtomicData_Near(&(kmerCache->kcAD[loc]), &kcAD, empty, __FILENAME__, __LINE__) ) {
                // won the cswap so this cache entry is now live
                resultType = SET;
            } else {
                /* failed to cswap, so lost the race to store the cache */
                resultType = CLASH;
            }
        }
    }

    SET_CACHE_RESULT(kmerCache->countTimers, resultType, ELAPSED_TIME(startTick));
    return resultPtr;
}
