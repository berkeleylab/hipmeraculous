#ifndef _FASTA_H_
#define _FASTA_H_

/* fasta.h
 * Declarations for simple FASTA i/o library
 * SRE, Sun Sep  8 05:37:38 2002 [AA2721, transatlantic]
 * CVS $Id: fasta.h,v 1.1 2003/10/05 18:43:39 eddy Exp $
 */

#include <stdio.h>
#include <upc.h>

#include "upc_common.h"
#include "common.h"

#define FASTA_MAXLINE 512   /* Requires FASTA file lines to be <512 characters */

typedef struct fastafile_s {
    GZIP_FILE fp;
    char      buffer[FASTA_MAXLINE];
} FASTAFILE;

extern FASTAFILE *OpenFASTA(char *seqfile);
extern int        ReadFASTA(FASTAFILE *fp, char **ret_seq, char **ret_name, int *ret_L);
extern void       CloseFASTA(FASTAFILE *ffp);

#endif // _FASTA_H_
