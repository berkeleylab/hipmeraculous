#ifndef __KMER_HASH2_H
#define __KMER_HASH2_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"

#include "../contigs/packingDNAseq.h"
#include "merAligner.h"

#include "kmer_hash_generic.h"

/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *create_hash_table_MA_exact(int64_t size, int64_t my_exact_size,  MEMORY_HEAP_T *memory_heap);

void destroy_hash_table_MA(HASH_TABLE_T **_ht, MEMORY_HEAP_T *memory_heap);

shared[] LIST_T *lookup_kmer_in_bucket_MA(shared[] BUCKET_T *bucket, const unsigned char *packed_key, int kmer_len, LIST_T *cached_kmer);

/* Use this lookup function when no writes take place in the distributed hashtable */
shared[] LIST_T *lookup_kmer_MA(HASH_TABLE_T *hashtable, const unsigned char *kmer, int kmer_len, LIST_T *cached_kmer);

// returns the entry from the bucket that was either already present or this new one
shared[] LIST_T *add_entry_to_bucket_MA(shared[] BUCKET_T *bucket, shared[] LIST_T *new_entry);

#endif // __KMER_HASH2_H
