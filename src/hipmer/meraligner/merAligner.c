#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <upc_tick.h>
#include <libgen.h>

#include "optlist.h"

#include "upc_common.h"
#include "common.h"
#include "upc_compatibility.h"
#include "timers.h"
#include "utils.h"
#include "merAligner.h"
#include "cache_infrastructure.h"
#include "upc_output.h"

#include "fasta.h"

#ifndef MAX_NODES_FOR_CACHE
#ifdef ENFORCE_KMER_CACHE
#define MAX_NODES_FOR_CACHE 0
#else
#define MAX_NODES_FOR_CACHE 4
#endif
#endif

#define BLAST 0
#define SAM 1
int _output_format = SAM;

#include "../contigs/kmer_handling.h"
#include "contigDB.h"
#include "buildSeedIndex.h"

#ifdef PROFILE
double rev_time;
double setup_cache;
double readmap_time;
double fetch_contigs_time;
double fetch_extra_contigs_time;
double exact_alignment_time;
double inexact_alignment_time;
double sw_alignment_time;
double hash_table_lookups;
double input_IO;
double output_IO;
double contig_metadata_lookup_time;
double hash_time;
int64_t seed_lookup;
#endif

int64_t cache_hits;
int64_t cache_queries;
int64_t seedsExtracted;

#define NOT_EXCLUDE
#ifdef NOT_EXCLUDE
#include "aligningPhase.h"
#endif

shared int64_t globalSeedCardinality;
shared int64_t globalSubcontigCardinality;

void createMeralignerInterModuleState(int max_read_len)
{
    LOGF("createMeralignerInterModuleState(%d)\n", max_read_len);
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    if (MYSV.interModuleState != NULL) {
        DIE("createMeralignerInterModuleState() was called with an active interModuleState!\n");
    }
    meraligner_inter_module_state_t *state = calloc_chk(1, sizeof(meraligner_inter_module_state_t));
    assert(state->dist_hashtable == NULL);
    state->readLength = max_read_len;
    state->scEffLength = -1;
    MYSV.interModuleState = state;
    serial_printf("Created meraligner intermodule state for caches with readLength=%d\n", state->readLength);
}


void calcMeralignerSeedCardinality(char *referenceFileName, int scEffLength, int kmer_len)
{
    int64_t mySeedCardinality, partial_result;

    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
    if (state == NULL) {
        DIE("calcMeralignerSeedCardinality() was called with an inactive interModuleState!\n");
    }
#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    upc_tick_t start_time;
    if (MYTHREAD == 0) {
        start_time = UPC_TICKS_NOW();
    }
#endif

    if (scEffLength > MAX_EFFECTIVE_CONTIG_LENGTH) {
        scEffLength = -1;
    }
    mySeedCardinality = seedAndContigCardinality(referenceFileName, &(state->localSubContigCardinality), scEffLength, kmer_len);
    // if this is empty, it does no harm. Some files may be empty if there is very little data.
    //if (!mySeedCardinality)
    //WARN("Could not open file, or file is empty: %s in seedAndContigCardinality\n", referenceFileName);

    // TODO replace with reduce_prefix
    UPC_ATOMIC_FADD_I64_RELAXED(&partial_result, &globalSeedCardinality, mySeedCardinality);
    UPC_ATOMIC_FADD_I64_RELAXED(&(state->IDoffset), &globalSubcontigCardinality, state->localSubContigCardinality);
    LOGF("partial_result=%lld mySeedCardinality=%lld IDoffset=%lld localSubContigCardinality=%lld\n", (lld)partial_result, (lld)mySeedCardinality, (lld)state->IDoffset, (lld)state->localSubContigCardinality);

    UPC_LOGGED_BARRIER;

    state->FseedCardinality = broadcast_long(globalSeedCardinality, 0);
    state->Fsubcontigs = broadcast_long(globalSubcontigCardinality, 0);

    serial_printf("Global seed cardinality: %lld\n", (lld) globalSeedCardinality);

    if (!state->FseedCardinality) {
        SDIE("global seed cardinality is 0\n");
    }

#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        upc_tick_t end_time = UPC_TICKS_NOW();
        float con_time = (UPC_TICKS_TO_SECS(end_time - start_time));
        printf("\nTime for calculating seed cardinality is : %f seconds\n", con_time);
    }
#endif
}


HASH_TABLE_T *getOrSetMeralignerSeedIndex(double exp_factor, char *referenceFileName, int scEffLength, int readLength, int kmer_len, int avoid_masked, int allow_repetitive_seeds)
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
    if (state == NULL) {
        DIE("getOrSetMeralignerInterModuleState() was called with an inactive interModuleState!\n");
    }
    scEffLength = scEffLength > MAX_EFFECTIVE_CONTIG_LENGTH ? -1 : scEffLength;
    if (state->scEffLength > 0 || state->readLength > 0) {
        if ((state->scEffLength != -1 && state->scEffLength != scEffLength) || state->readLength < readLength) {
            // must recalc cardinality and seed index (and caches)
            serial_printf("Cannot reuse seed index as scEffLength (%d) != %d or readLength (%d) != %d from previous run\n", scEffLength, state->scEffLength, readLength, state->readLength);
            destroyMeralignerInterModuleState();
            createMeralignerInterModuleState(readLength);
            state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
        }
        state->scEffLength = scEffLength;
    }
    if (state->dist_hashtable == NULL) {
        calcMeralignerSeedCardinality(referenceFileName, state->scEffLength, kmer_len);
        state->dist_hashtable = buildSeedIndex(state->FseedCardinality, referenceFileName, &(state->memory_heap), state->scEffLength, state->IDoffset, state->localSubContigCardinality, state->readLength, kmer_len, MYSV.cores_per_node, avoid_masked, allow_repetitive_seeds);
    } else {
        SLOG("Reusing previous seed index\n");
    }
    return state->dist_hashtable;
}

contig_cache_t getOrSetContigCache(int cache_size_in_MB)
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
    if (state == NULL) {
        DIE("getOrSetMeralignerInterModuleState() was called with an inactive interModuleState!\n");
    }
    contig_cache_t contigCache = (contig_cache_t)state->contigCache;
#ifdef USE_SWCACHE
    if (contigCache == NULL) {
        if (THREADS > MYSV.cores_per_node && cache_size_in_MB > 0) {
            int64_t expanded_size2 = ((int64_t)cache_size_in_MB) * ((int64_t)1024 * 1024);
            contigCache = state->contigCache = createContigCache(state->Fsubcontigs, expanded_size2);
            LOGF("Allocated cache of %0.3f MB/thread for contigs\n", expanded_size2 / ONE_MB / MYSV.cores_per_node);
        } else {
            SLOG("No SW cache for contigs as it is 0 or there is only one node\n");
        }
    } else {
        SLOG("Reusing previous contig cache\n");
    }
#endif
    return contigCache;
}


int64_t countMeralignerMemoryHeapEntries()
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
    if (state == NULL) {
        DIE("countMeralignerMemoryHeapEntries() was called with an inactive interModuleState!\n");
    }
    int64_t tmpres = 0;
    for (int i = 0; i < THREADS; i++) {
        tmpres += state->memory_heap.heap_struct[i].len;
    }
    return tmpres;
}

void destroyMeralignerInterModuleState()
{
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    LOGF("destroyMeralignerInterModuleState\n");
    meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
    if (state == NULL) {
        DIE("destroyMeralignerInterModuleState() was called with an inactive interModuleState!\n");
    }
    if (state->dist_hashtable != NULL) {
        destroy_hash_table_MA(&(state->dist_hashtable), &(state->memory_heap));
    }
    if (state->contigCache) {
        freeContigCache((contig_cache_t *)&(state->contigCache));
    }
    memset(state, 0, sizeof(meraligner_inter_module_state_t));
    free_chk(state);
    MYSV.interModuleState = NULL;
}


int merAligner_main(int argc, char **argv)
{
    struct stat sb;
    if (lstat("testrestart", &sb) == 0) upc_global_exit(1);
   
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

#ifdef PROFILE
    rev_time = 0.0;
    setup_cache = 0.0;
    readmap_time = 0.0;
    fetch_contigs_time = 0.0;
    fetch_extra_contigs_time = 0.0;
    exact_alignment_time = 0.0;
    inexact_alignment_time = 0.0;
    sw_alignment_time = 0.0;
    hash_table_lookups = 0.0;
    input_IO = 0.0;
    output_IO = 0.0;
    contig_metadata_lookup_time = 0.0;
    hash_time = 0.0;
    seed_lookup = 0;
#endif

    cache_hits = 0;
    cache_queries = 0;
    seedsExtracted = 0;

    int64_t globalAlignmentsFound;
    int64_t globalReadsProcessed;
    int64_t globalReadsMapped;
    int64_t globalLooks;
    int64_t globalSeedsExtracted;
    seedsExtracted = 0;
    if (MYTHREAD == 0) {
        globalSeedCardinality = 0;
        globalSubcontigCardinality = 0;
    }
    UPC_LOGGED_BARRIER;
    int64_t readNumber;
    int64_t i, Fsubcontigs;
    int64_t size;
    HASH_TABLE_T *dist_hashtable = NULL;
    GZIP_FILE blastresFD1;
    GZIP_FILE blastresFD2;
    char readFileName[MAX_FILE_PATH];
    char outputBlastmap1[MAX_FILE_PATH], outputBlastmap2[MAX_FILE_PATH];

    shared[1] double *fetch_contigs_times = NULL;
    shared[1] double *hash_table_lookups_times = NULL;
    shared[1] double *readmap_times = NULL;
    shared[1] double *outputIO_times = NULL;
    shared[1] double *match_times = NULL;
    shared[1] double *metadata_times = NULL;
    shared[1] double *rev_times = NULL;
    shared[1] double *hash_times = NULL;
    shared[1] double *lookups = NULL;
    shared[1] double *fetch_extra_contigs_times = NULL;
    shared[1] double *exact_alignment_times = NULL;
    shared[1] double *inexact_alignment_times = NULL;
    shared[1] double *sw_alignment_times = NULL;
    double avg_IO, avg_fetch, avg_lookup, avg_readmap, avg_sw, avg_match, avg_meta, avg_rev, avg_hash, avg_lookups, avg_fetch_extra;
    double min_IO, min_fetch, min_lookup, min_readmap, min_sw, min_match, min_meta, min_rev, min_hash, min_lookups, min_fetch_extra;
    double max_IO, max_lookup, max_fetch, max_readmap, max_sw, max_match, max_meta, max_rev, max_hash, max_lookups, max_fetch_extra;
    kmer_cache_t kmerCache = NULL;
    int64_t partial_result;

    int filesPerPair;
    const char *base_dir = ".";
    char *libName, *read_files_name, *minimum_contig_length, *UFX_boundaries, *nContigs, *chunksize, *exp_factor_string, *contigFileName;
    int kmer_cache_in_MB = 0;
    int cache_size_in_MB = 1;
    int64_t seed_space = 1;
    int avoid_masked = 0;
    int allow_repetitive_seeds = 1;
    double exp_factor;
    int read_length = -1; // -L 0 means unknown read length, so used old scaffold effective length, if available
    int scEffLength = 0;
    int libnum = -1;
    char *merAlignerOutput = NULL;
    int is_cgraph = 0;
    int reads_are_per_thread = 0;
    int min_score = 0;
    int _output_format = BLAST;

    ssw_scores_t ssw_scores = {.match = 1, .mismatch = 3, .gap_open = 5, .gap_extension = 2, .ambiguity = 2};

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "l:u::P:r:c:C:m:b:d:n:x:K:e:j:O:B:XN:M:L:k:o:zRS");
    print_args(optList, __func__);
    filesPerPair = 1; // default interleaved

    int64_t outputSize;
    libName = read_files_name = NULL;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'l':
            libName = thisOpt->argument;
            break;
        case 'u':
            libnum = atoi(thisOpt->argument);
            break;
        case 'k':
            kmer_len = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Please compile with a higher MAX_KMER_SIZE (%d) for kmer length %d",
                     MAX_KMER_SIZE, kmer_len);
            }
            break;
        case 'P':
            filesPerPair = atoi(thisOpt->argument);
            break;
        case 'e':
            scEffLength = atoi(thisOpt->argument);
            break;
        case 'r':
            read_files_name = thisOpt->argument;
            break;
        case 'C':
            cache_size_in_MB = atoi(thisOpt->argument);
            break;
        case 'c':
            contigFileName = thisOpt->argument;
            break;
        case 'm':
            minimum_contig_length = thisOpt->argument;
            break;
        case 'b':
            UFX_boundaries = thisOpt->argument;
            break;
        case 'd':
            seed_space = atoi(thisOpt->argument);
            break;
        case 'n':
            nContigs = thisOpt->argument;
            break;
        case 'x':
            chunksize = thisOpt->argument;
            break;
        case 'K':
            kmer_cache_in_MB = atoi(thisOpt->argument);
            break;
        case 'j':
            exp_factor = atof(thisOpt->argument);
            break;
        case 'O':
            outputSize = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'L':
            read_length = atoi(thisOpt->argument);
            break;
        case 'M':
            avoid_masked = 1;
            break;
        case 'R':
            allow_repetitive_seeds = 0;
            break;
        case 'X':
            reads_are_per_thread = 1;
            break;
        case 'N':
            SET_CORES_PER_NODE(atoi(thisOpt->argument));
            break;
        case 'o':
            merAlignerOutput = thisOpt->argument;
            break;
        case 'z':
            is_cgraph = 1;
            break;
        case 'S':
            _output_format = SAM;
            break;
        default:
            break;
        }
    }

    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    if (!MYTHREAD) {
        printf("Using %d cores per node\n", MYSV.cores_per_node);
    }

    if (read_length < 0) {
        meraligner_inter_module_state_t *state = (meraligner_inter_module_state_t *)MYSV.interModuleState;
        if (state) {
            read_length = state->readLength;
        }
    }
        
    if (read_length <= 0) {
        serial_printf("No read length specified (-L) and no interModule state, using scaffold Effective length 0 (was -e %d)\n", scEffLength);
        scEffLength = 0;
        read_length = 0;
    }

    UPC_TICK_T start, end, start_timer, end_timer;
    double con_time, trav_time, blastmap_time, align_call = 0.0, io_handle = 0.0;
    char referenceFileName[MAX_FILE_PATH];

    if (libName == NULL || read_files_name == NULL || libnum < 0) {
        DIE("you must specifiy -l -u and -r (%s %d %s)!\n",
            !libName ? "NULL" : libName, libnum, !read_files_name ? "NULL" : read_files_name);
    }
    /* TODO check other necessary parameters */

    sprintf(referenceFileName, "%s.fasta" GZIP_EXT, contigFileName);
    restoreCheckpoint(referenceFileName);
    checkpointToLocal(referenceFileName, referenceFileName);

    /************************************************/
    /* Estimate an upper bound for seed cardinality */
    /************************************************/


#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    /************************/
    /*   Build seed index   */
    /************************/

#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    dist_hashtable = getOrSetMeralignerSeedIndex(exp_factor, referenceFileName, scEffLength, read_length, kmer_len, avoid_masked, allow_repetitive_seeds);

#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = (UPC_TICKS_TO_SECS(end - start));
        printf("\nTime for building the seed index is : %f seconds\n", con_time);
    }
#endif
    contig_cache_t contigCache = getOrSetContigCache(cache_size_in_MB);

    /*****************************/
    /* Parallel sequence aligner */
    /*****************************/
#ifdef NOT_EXCLUDE

#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        printf("\n\n***************** Starting aligning *******************\n\n");
        start = UPC_TICKS_NOW();
    }
#endif


#ifdef PROFILE
    start_timer = UPC_TICKS_NOW();
#endif
    /* Build cache for k-mer seeds */
#ifdef USE_SWCACHE
    if (!_sv) {
        DIE("Static variables are necessary here please initialize them in the code!\n");
    }
    if (kmer_cache_in_MB > 0 && THREADS > MYSV.cores_per_node && (reads_are_per_thread > 0 || THREADS / MYSV.cores_per_node <= MAX_NODES_FOR_CACHE)) {
        UPC_LOGGED_BARRIER;
        int64_t expanded_size = ((int64_t)kmer_cache_in_MB) * ((int64_t)(1024 * 1024));
        kmerCache = createKmerCache(expanded_size);
        LOGF("Allocated cache of %0.3f MB/thread for kmers\n", expanded_size * 1.0 / ONE_MB / MYSV.cores_per_node );
        UPC_LOGGED_BARRIER;
    } else {
        kmer_cache_in_MB = 0;
        SLOG("No SW cache for kmers will be used as the node count is %d (min 2 max %d)\n", THREADS / MYSV.cores_per_node, MAX_NODES_FOR_CACHE);
    }
#endif

#ifdef PROFILE
    end_timer = UPC_TICKS_NOW();
    setup_cache += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
    UPC_LOGGED_BARRIER;
    start_timer = UPC_TICKS_NOW();
    int64_t readsMapped = 0, readsProcessed = 0, alignmentsFound = 0;

    sprintf(outputBlastmap1, "%s-%s_Read1" GZIP_EXT, libName, merAlignerOutput);
    blastresFD1 = openCheckpoint(outputBlastmap1, "wb1");
    if (filesPerPair > 0) {
        sprintf(outputBlastmap2, "%s-%s_Read2" GZIP_EXT, libName, merAlignerOutput);
        blastresFD2 = openCheckpoint(outputBlastmap2, "wb1");
    } else {
        blastresFD2 = NULL;
    }

    readNumber = 0;
    Buffer readFilesBuffer = broadcast_file(read_files_name);
    uint64_t read1ID = MYTHREAD, read2ID = MYTHREAD;
    end_timer = UPC_TICKS_NOW();
    io_handle += UPC_TICKS_TO_SECS(end_timer - start_timer);
    char buf[MAX_FILE_PATH];
    while (getsBuffer(readFilesBuffer, buf, MAX_FILE_PATH) != NULL) {
        buf[strlen(buf) - 1] = '\0'; // remove trailing newline
        if (buf[0] == '\0') {
            break;
        }

        if (reads_are_per_thread) {
            sprintf(readFileName, "%s", basename(buf));
            
            restoreCheckpoint(readFileName);
            char uncomp[MAX_FILE_PATH+20];
            sprintf(uncomp, "%s.uncompressedSize", readFileName);
            restoreCheckpoint(uncomp);
            checkpointToLocal(readFileName, readFileName);
        } else {
            strcpy(readFileName, buf);
        }
        serial_printf("Aligning readFileName: %s\n", readFileName);
        
        if (filesPerPair == 2) {
            /* switch read number */
            readNumber = (readNumber + 1) % 2;
        }

        start_timer = UPC_TICKS_NOW();
        alignmentsFound += parallelAligner(dist_hashtable, readFileName, blastresFD1, blastresFD2, filesPerPair, readNumber, 0, 0, cache_size_in_MB, atoi(minimum_contig_length), atoi(chunksize), kmerCache, contigCache, &readsMapped, &readsProcessed, base_dir, reads_are_per_thread, libName, kmer_len, (uint8_t)libnum, seed_space, &ssw_scores, min_score, &read1ID, &read2ID, avoid_masked, is_cgraph, _output_format);
        end_timer = UPC_TICKS_NOW();
        align_call += UPC_TICKS_TO_SECS(end_timer - start_timer);
    }

    UPC_LOGGED_BARRIER;
    start_timer = UPC_TICKS_NOW();
    closeCheckpoint(blastresFD1);
    if (filesPerPair > 0) closeCheckpoint(blastresFD2); // just the single filehandle
    freeBuffer(readFilesBuffer);
    end_timer = UPC_TICKS_NOW();
    io_handle += UPC_TICKS_TO_SECS(end_timer - start_timer);

    UPC_LOGGED_BARRIER;

#ifdef PROFILE
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        blastmap_time = (UPC_TICKS_TO_SECS(end - start));
        if (!_sv) {
            DIE("Static variables are necessary here please initialize them in the code!\n");
        }
        if (cache_size_in_MB > 0 && contigCache != NULL) {
            printf("Software contig cache ENABLED with size %f GB/node\n", cache_size_in_MB / 1024.0);
        } else {
            printf("Software contig cache DISABLED\n");
        }
        if (kmer_cache_in_MB > 0 && kmerCache != NULL) {
            printf("Software k-mer cache ENABLED with size %f GB/node\n", kmer_cache_in_MB / 1024.0);
        } else {
            printf("Software k-mer cache DISABLED\n");
        }
        printf("\nSetting up cache took :  %f seconds\n", setup_cache);

        printf("\nParallel merAligner took :  %f seconds (%f seconds in parallelAligner() call, %f seconds in io handle)\n", blastmap_time, align_call, io_handle);
    }
#endif

    double start_timer_time = now();
    /* Measure load imbalance in aligning phase */

    double balance;
    double start_reduce_time = now();
    serial_printf("\n*******  AGGREGATE STATISTICS  *******\n");
    balance = avg_min_max(readmap_time, &avg_readmap, &min_readmap, &max_readmap);
    serial_printf("Readmaps: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_readmap, min_readmap, max_readmap, balance);
    balance = avg_min_max(sw_alignment_time, &avg_sw, &min_sw, &max_sw);
    serial_printf("SSW times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_sw, min_sw, max_sw, balance);
    balance = avg_min_max(exact_alignment_time, &avg_match, &min_match, &max_match); 
    serial_printf("Exact matching times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_match, min_match, max_match, balance);
    balance = avg_min_max(inexact_alignment_time, &avg_match, &min_match, &max_match); 
    serial_printf("In-exact matching times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_match, min_match, max_match, balance);
    balance = avg_min_max(rev_time, &avg_rev, &min_rev, &max_rev);
    serial_printf("Reverse-complementing times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_rev, min_rev, max_rev, balance);
    balance = avg_min_max(hash_time, &avg_hash, &min_hash, &max_hash);
    serial_printf("Hashing times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_hash, min_hash, max_hash, balance);
    balance = avg_min_max((1.0 * seed_lookup) / (1.0 * readsProcessed), &avg_lookups, &min_lookups, &max_lookups);
    serial_printf("Mean lookups per read: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_lookups, min_lookups, max_lookups, balance);
    balance = avg_min_max(hash_table_lookups, &avg_lookups, &min_lookups, &max_lookups);
    serial_printf("Look-ups: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_lookups, min_lookup, max_lookup, balance);
    balance = avg_min_max(contig_metadata_lookup_time, &avg_meta, &min_meta, &max_meta);
    serial_printf("Contig metadata lookup times: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_meta, min_meta, max_meta, balance);
    balance = avg_min_max(fetch_contigs_time, &avg_fetch, &min_fetch, &max_fetch);
    serial_printf("Fetching contigs: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_fetch, min_fetch, max_fetch, balance);
    balance = avg_min_max(fetch_extra_contigs_time, &avg_fetch_extra, &min_fetch_extra, &max_fetch_extra);
    serial_printf("Fetching extra contigs: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_fetch_extra, min_fetch_extra, max_fetch_extra, balance);
    balance = avg_min_max(output_IO, &avg_IO, &min_IO, &max_IO);
    serial_printf("Total output IO: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_IO, min_IO, max_IO, balance);
    balance = avg_min_max(input_IO, &avg_IO, &min_IO, &max_IO);
    serial_printf("Total input IO: Avg %f\tMin %f\tMax %f Bal %0.2f\n", avg_IO, min_IO, max_IO, balance);

    serial_printf("\n**************  ALIGNMENTS STATISTICS  ********************\n");
    globalSeedsExtracted = reduce_long(seedsExtracted, UPC_ADD, SINGLE_DEST);
    globalReadsProcessed = reduce_long(readsProcessed, UPC_ADD, SINGLE_DEST);
    serial_printf("Total reads processed: %lld\n", (lld)globalReadsProcessed);
    globalReadsMapped = reduce_long(readsMapped, UPC_ADD, SINGLE_DEST);
    serial_printf("Total reads mapped: %lld (%f %%)\n", (lld)globalReadsMapped, ((double)globalReadsMapped / (double)globalReadsProcessed) * 100.0);
    globalAlignmentsFound = reduce_long(alignmentsFound, UPC_ADD, SINGLE_DEST);
    serial_printf("Total alignments found: %lld\n", (lld)globalAlignmentsFound);
    globalLooks = reduce_long(cache_queries, UPC_ADD, SINGLE_DEST);
    serial_printf("Global seed lookups: %lld\n", (lld)globalLooks);
    serial_printf("Global seed cardinality is: %lld\n", (lld)globalSeedCardinality);
    serial_printf("Global subcontig cardinality is: %lld\n", (lld)globalSubcontigCardinality);

    if (!MYTHREAD) {
        int64_t tmpres = countMeralignerMemoryHeapEntries();

        {
            char countFileName[MAX_FILE_PATH];
            sprintf(countFileName, "%s-nTotalAlignments.txt", libName);
            char *fname = get_rank_path(countFileName, -1);
            FILE *countFD = fopen_chk(fname, "w");
            fprintf(countFD, "%lld\n", (lld)globalAlignmentsFound);
            fclose_track(countFD);
        }

        serial_printf("Entries in distributed seed index are: %lld\n", (lld)tmpres);
        serial_printf("Number of actual seeds extracted is: %lld\n", (lld)globalSeedsExtracted);

        ADD_DIAG("%lld", "total_reads_processed", (lld)globalReadsProcessed);
        ADD_DIAG("%lld", "total_reads_mapped", (lld)globalReadsMapped);
        ADD_DIAG("%lld", "total_alignments_found", (lld)globalAlignmentsFound);
        ADD_DIAG("%lld", "global_seed_lookups", (lld)globalLooks);
        ADD_DIAG("%lld", "global_seed_cardinality", (lld)globalSeedCardinality);
        ADD_DIAG("%lld", "global_subcontig_cardinality", (lld)globalSubcontigCardinality);
    }

    double reduce_timer_time = now() - start_reduce_time;
    serial_printf("Total timer times: %0.3f elapsed\n", reduce_timer_time);

    UPC_LOGGED_BARRIER;
    //char cache_statistics_name[MAX_FILE_PATH];
    //sprintf(cache_statistics_name, "cache_statistics_%d", MYTHREAD);
    //get_rank_path(cache_statistics_name, MYTHREAD);
    //cacheFD = fopen_chk(cache_statistics_name,"w+");
    //fprintf(cacheFD, "THREAD %d did %lld k-mer queries and cache hits were %lld  -- hit ratio is %f\n", MYTHREAD, cache_queries, cache_hits, (double)cache_hits/(double) cache_queries);
    //fclose_track(cacheFD);

    /* Dump files with detailed statistics */

#endif // ifdef NOT_EXCLUDE


    char detailed_statistics_name[MAX_FILE_PATH];
    FILE *detailedFD;
    sprintf(detailed_statistics_name, "detailed_statistics_%d", MYTHREAD);
    //detailedFD = fopen_chk(detailed_statistics_name,"w+");
    //fprintf(detailedFD, "THREAD %d spent:\nReadmap time = %f seconds\n=========================\nSeed lookups = %f seconds\nFetching contigs = %f seconds\nComputation time = %f seconds\nOutput I/O = %f seconds\n", MYTHREAD, readmap_time, hash_table_lookups, fetch_contigs_time, readmap_time - hash_table_lookups - fetch_contigs_time - output_IO, output_IO );
    //fprintf(detailedFD, "Input I/O = %f seconds\n", input_IO);
    //fclose_track(detailedFD);
    UPC_LOGGED_BARRIER;

    /*
     * char nReadsInFileName[MAX_FILE_PATH];
     * sprintf(nReadsInFileName, "%s/myNreads", base_dir);
     * get_rank_path(nReadsInFileName, MYTHREAD);
     * FILE *nReadsFD = fopen_chk(nReadsInFileName,"w");
     * fprintf(nReadsFD, "%lld\n", (lld) readsProcessed/2);
     * fclose_track(nReadsFD);
     */

    //UPC_ALL_FREE_CHK(hash_times);
#ifdef USE_SWCACHE
    UPC_LOGGED_BARRIER;
    if (kmerCache) {
        destroyKmerCache(&kmerCache);
    }
#endif
    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
