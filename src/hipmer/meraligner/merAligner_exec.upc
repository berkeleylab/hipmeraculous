#include "merAligner.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("merAligner");
    createMeralignerInterModuleState(390);
    int ret = merAligner_main(argc, argv);
    destroyMeralignerInterModuleState();
    return ret;
}
