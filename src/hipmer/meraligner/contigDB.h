/* Calculate the cardinality of seeds in a contig file and the number of subcontigs generated */
#define MAX_CONTIG_SIZE 900000

#include "fasta.h"
#include "timers.h"

int64_t seedAndContigCardinality(char *FASTAfilename, int64_t *subContigCardinality, int subContigEffectiveLength, int kmer_len)
{
    FASTAFILE *ffp;
    int64_t resultCardinality = 0;
    int64_t subcontigCardinality = 0;
    int64_t contigs = 0;
    double start = now();
    char *seq = NULL;
    char *name = NULL;
    int length;

    ffp = OpenFASTA(FASTAfilename);
    if (ffp == NULL) {
        (*subContigCardinality) = 0;
        LOGF("seedAndContigCardinality - no FASTA: %s\n", FASTAfilename);
        return 0;
    }
    while (ReadFASTA(ffp, &seq, &name, &length)) {
        contigs++;
        resultCardinality += length - kmer_len + 1;
        if (subContigEffectiveLength <= 0) {
            subcontigCardinality++;
        } else {
            subcontigCardinality += (length + subContigEffectiveLength - 1) / subContigEffectiveLength;
        }
        free_chk0(name);
        free_chk0(seq);
    }
    CloseFASTA(ffp);

    (*subContigCardinality) = subcontigCardinality;
    LOGF("seedAndContigCardinality found %lld contigs requiring %lld subcontigs and %0.3f MB in %0.3f s\n", (lld)contigs, (lld)subcontigCardinality, 1.0 / ONE_MB * resultCardinality, now() - start);
    return resultCardinality;
}

char *sgets(char *str, int num, char **input)
{
    char *next = *input;
    int numread = 0;
    int strpos = 0;

    while (numread + 1 < num && *next) {
        int isnewline = (*next == '\n');

        str[strpos] = *next++;
        strpos++;
        numread++;
        // newline terminates the line but is included
        if (isnewline) {
            break;
        }
    }

    if (numread == 0) {
        return NULL; // "eof"
    }
    // must have hit the null terminator or end of line
    str[strpos] = '\0'; // null terminate this tring
    // set up input for next call
    *input = next;
    return str;
}
