#ifndef MERALIGNER_H_
#define MERALIGNER_H_

#include <upc.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include "../contigs/packingDNAseq.h"
#include "upc_common.h"
#include "common.h"

#define TRUE 1
#define FALSE 0
#define UU_CONTIG 0
#define NON_UU_CONTIG 1
#define BS 1

typedef shared[BS] UPC_INT64_T *shared_int64_ptr;

/* Conitg data structure */
typedef struct _contig_t contig_t;
struct _contig_t {
    shared[] char *contig;                      // Actual content of the contig
    int64_t contig_id;
    int64_t parentContigID;
    int32_t uuType;
    int32_t length;
    int32_t parentLength;
    int32_t offsetInParent;
};

typedef shared[] contig_t *contig_t_ptr;

/* Data structure to hold a contig pointer and posInContig */
typedef struct _contigInfo contigInfo;
struct _contigInfo {
    shared[] contig_t * my_contig;
    int32_t posInContig;
};

/* Hash-table entries, i.e. seeds */
typedef shared[] contigInfo *contigInfoArrayPtr_t;

#define CONTIGS_DDS_TYPE _merAligner
#define LIST_STRUCT_CONTENTS \
    contigInfoArrayPtr_t contigInfoArrayPtr;                 /* Pointer to the list of contigInfo entries */ \
    contigInfo           firstContig;                        /* Information for the first contig */ \
    int32_t              nExtraContigs;                      /* Number of extra contigs this seed can be found */ \
    int32_t              posInSubArray;                      /* last written record in contigInfoArrayPtr */ \
    unsigned char        packed_key[MAX_KMER_PACKED_LENGTH]; /* The packed key of the seed */ \

#define MEMORY_HEAP_EXTRA_CONTENTS \
    shared[BS] contigInfoArrayPtr_t * contigInfoArrays; /* allocation for extraContigs */ \
    shared[BS] int64_t * heap_ptr_sizes;                /* the capacity of each thread heap */ \
    SharedHeapListPtr contigs;                          /* */ \
    SharedHeapListPtr contig_content;                   /* */ \

#include "hashtable_dds.h"

typedef struct _meraligner_inter_module_state_t meraligner_inter_module_state_t;
struct _meraligner_inter_module_state_t {
    HASH_TABLE_T *dist_hashtable;
    MEMORY_HEAP_T memory_heap;
    int32_t       scEffLength, readLength;
    int64_t       FseedCardinality, Fsubcontigs;
    int64_t       localSubContigCardinality, IDoffset;
    void *        contigCache;
};

void createMeralignerInterModuleState(int max_read_length);
void destroyMeralignerInterModuleState();

int merAligner_main(int argc, char **argv);

#endif // MERALIGNER_H_
