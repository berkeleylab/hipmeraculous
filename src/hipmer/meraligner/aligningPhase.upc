#include "aligningPhase.h"

extern double readmap_time;
extern double fetch_contigs_time;
extern double fetch_extra_contigs_time;
extern double exact_alignment_time;
extern double inexact_alignment_time;
extern double sw_alignment_time;
extern double hash_table_lookups;
extern double input_IO;
extern double output_IO;

/* This table is used to transform nucleotide letters into numbers. */
int8_t nt_table[128] = {
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 3, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 3, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
};

int64_t parallelAligner(HASH_TABLE_T *hashtable,
                        char *filename,
                        GZIP_FILE res_fd1,
                        GZIP_FILE res_fd2,
                        int filesPerPair,
                        int readNumber,
                        double estimatedInsertSize,
                        double estimatedSigma,
                        int64_t cacheSizeInMB,
                        int min_contig_length,
                        int chunk_size,
                        kmer_cache_t kmerCache,
                        contig_cache_t contigCache,
                        int64_t *readsMapped,
                        int64_t *readsProcessed,
                        const char *base_dir,
                        const int reads_are_per_thread,
                        const char *libName,
                        int kmer_len,
                        uint8_t libnum,
                        int SEED_SPACE,
                        ssw_scores_t *ssw_scores,
                        int min_score,
                        uint64_t *read1ID,
                        uint64_t *read2ID,
                        int ref_is_masked,
                        int is_hexified,
                        int _output_format)
{
    long total_read;
    int idlength, read_length;
    int j, k, found_flag, insert_size, err_code, pos1, pos2, orientation, test1, test2, test3, strand1, strand2, sStart1, sStart2, sStop1, sStop2, sLength1, min, max, extent, safeDistance, leftfary, rightBoundary;
    int64_t i, partial_res, readsRead, l, m;
    int locs[4];
    int64_t alignmentsFound = 0, curReadAlignments = 0;
    UPC_TICK_T start_map, end_map, start_red, end_red;
    double map_time = 0.0;
    UPC_TICK_T start_readmap_timer, end_readmap_timer;
    UPC_TICK_T start_timer, end_timer;
    int finished;
    int whichOutput = 0;
    FILE *logFD = NULL;
    double pre_time = 0.0;

#ifdef DEBUG
    char logfile_name[MAX_FILE_PATH];
    sprintf(logfile_name, "parallelAligner_%d.log", MYTHREAD);
    get_rank_path(logfile_name, MYTHREAD);
    logFD = fopen_chk(logfile_name, "a");
    DBG("Opening %s for more detailed logs\n", logfile_name);
    fprintf(logFD, "Starting parallelAligner\n");
#endif

    // initialize scoring matrix for genome sequences
    //  A  C  G  T	N (or other ambiguous code)
    //  2 -2 -2 -2  0	A
    // -2  2 -2 -2  0	C
    // -2 -2  2 -2  0	G
    // -2 -2 -2  2  0	T
    //	0  0  0  0  0	N (or other ambiguous code)
    UPC_LOGGED_BARRIER;
    start_timer = UPC_TICKS_NOW();

    int8_t *mat = (int8_t *)calloc_chk(25, sizeof(int8_t));
    for (l = k = 0; l < 4; ++l) {
        for (m = 0; m < 4; ++m) {
            mat[k++] = l == m ? ssw_scores->match : - ssw_scores->mismatch; /* weight_match : -weight_mismatch */
        }
        mat[k++] = - ssw_scores->ambiguity;               // ambiguous base: no penalty
    }
    for (m = 0; m < 5; ++m) {
        mat[k++] = - ssw_scores->ambiguity;
    }

    if (filesPerPair == 2) {
        whichOutput = readNumber;
    }

    total_read = 0;

    LOGF("Opening read file: reads_are_perthread=%d base_dir=%s filename=%s\n", reads_are_per_thread, base_dir, filename);
    fq_reader_t fqr = create_fq_reader();
    open_fq(fqr, filename, reads_are_per_thread, base_dir, reads_are_per_thread ? -1 : broadcast_file_size(filename));

    UPC_LOGGED_BARRIER;
    double fq_reader_stuff = UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - start_timer);

    if (MYTHREAD == 0) {
        printf("Start with file %s\n", filename);
    }

    Buffer id = initBuffer(MAX_READ_NAME_LEN);
    Buffer seq = initBuffer(DEFAULT_READ_LEN), quals = initBuffer(DEFAULT_READ_LEN);

    DBG("Allocated id and seq buffers\n");

#ifdef DEBUG
    fprintf(logFD, "Thread %d: Starting to read %s\n", MYTHREAD, filename);
#endif

    UPC_LOGGED_BARRIER;
    pre_time = UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - start_timer);
    UPC_TICK_T startAlign = UPC_TICKS_NOW();

    while (1) {
#ifdef PROFILE
        start_timer = UPC_TICKS_NOW();
#endif
        int found_next = get_next_fq_record(fqr, id, seq, quals);

        if (!found_next) {
            break;
        }
        if (!is_hexified) hexifyId(getStartBuffer(id), libnum, read1ID, read2ID, THREADS);
        total_read++;
#ifdef PROFILE
        end_timer = UPC_TICKS_NOW();
        input_IO += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif

#ifdef PROFILE
        start_readmap_timer = UPC_TICKS_NOW();
#endif

        DBG2("processing %s\n", getStartBuffer(id));

        // read_map will automatically retry with seed space of 1 if no alignments are found
        curReadAlignments = read_map(whichOutput == 0 ? res_fd1 : res_fd2, hashtable, seq, id, quals, contigCache, min_contig_length, mat, logFD, chunk_size, kmerCache, kmer_len, SEED_SPACE, ssw_scores, min_score, ref_is_masked, _output_format);

        if (filesPerPair == 1) {
            whichOutput = (whichOutput + 1) % 2;
        }
        alignmentsFound += curReadAlignments;
        if (curReadAlignments > 0) {
            (*readsMapped)++;
        }

#ifdef PROFILE
        end_readmap_timer = UPC_TICKS_NOW();
        readmap_time += (UPC_TICKS_TO_SECS(end_readmap_timer - start_readmap_timer));
#endif
        if (total_read % (THREADS * 1000L) == (MYTHREAD * 1000L)) {
            SLOG("Processed %lld reads and at %0.2f %% complete, %0.1f reads / sec\n", (lld)total_read, getProgress(fqr) * 100.0, total_read / UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - startAlign));
        } else if (total_read % 20000L == 0) {
            LOGF("Processed %lld reads and at %0.2f %% complete, %0.1f reads / sec\n", (lld)total_read, getProgress(fqr) * 100.0, total_read / UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - startAlign));
        }

    }

    // reduce to max read length over all threads and write to file
    double alignTime = UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - startAlign);
    LOGF("Finished processing my reads: %lld over %0.3f s %0.1f reads/s\n", (lld)total_read, alignTime, total_read / (alignTime <= 0.0 ? 1.0 : alignTime));
    double totalTime = reduce_double(alignTime, UPC_ADD, SINGLE_DEST);
    UPC_LOGGED_BARRIER;
    alignTime = UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - startAlign);
    serial_printf("Finished processing all reads: %lld over %0.3f s %0.1f reads/s/thread imbalance=%0.2f\n", (lld)total_read, alignTime, total_read / (alignTime <= 0.0 ? 1.0 : alignTime), alignTime <= 0.0 ? 0.0 : totalTime / (alignTime * THREADS));

    LOGF("Finished processing reads: %lld over %0.3f s %0.1f reads/s\n", (lld)total_read, alignTime, total_read / (alignTime <= 0.0 ? 1.0 : alignTime));

    start_red = UPC_TICKS_NOW();
    int all_max_read_len = reduce_int(fqr->max_read_len, UPC_MAX, ALL_DEST);
    end_red = UPC_TICKS_NOW();
    serial_printf("Time for reduction for MAX_READ_LENS is %f seconds\n", UPC_TICKS_TO_SECS(end_red - start_red));

    DBG("My max readlength %lld\n", (lld)fqr->max_read_len);

    LOGF("Closing fastq %s\n", filename);
    destroy_fq_reader(fqr);

    if (!MYTHREAD) {
        printf("Max read length for lib %s: %d\n", libName, all_max_read_len);
        char buf[MAX_FILE_PATH];
        sprintf(buf, "%s-readlen.txt", libName);
        printf("about to write to %s\n", buf);
        FILE *f = fopen_rank_path(buf, "a", -1);
        fprintf(f, "%d\n", all_max_read_len);
        fclose_track(f);
    }


    (*readsProcessed) += total_read;
    DBG("total_read=%lld readsProcessed=%lld\n", (lld)total_read, (lld) * readsProcessed);
    freeBuffer(id); id = NULL;
    freeBuffer(seq); seq = NULL;
    freeBuffer(quals); quals = NULL;


#ifdef DEBUG
    fprintf(logFD, "Thread %d: Done processing: %ld\n", MYTHREAD, total_read);
    fclose_track(logFD);
#endif

    free_chk(mat);
    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        printf("Done with file %s in %f seconds, total reads read are %ld\n", filename, alignTime, total_read);
        printf("Post alignment time is %f seconds, pretime is %f seconds, %f seconds for fq_reader_stuff\n", UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - start_red), pre_time, fq_reader_stuff);
    }
    return alignmentsFound;
}

ReadData initReadData(const char *name, const char *seq, const char *quals, int readLength, const int8_t *mat, const ssw_scores_t *ssw_scores) {
    ReadData d = (ReadData) malloc_chk0(sizeof(_ReadData));
    DBG2("New ReadData %p for %s\n", d, name);
    d->name = strdup_chk0(name);
    // trim any tabs or spaces in the name
    char *pos = strchr(d->name, ' ');
    if (pos) *pos = '\0';
    pos = strchr(d->name, '\t');
    if (pos) *pos = '\0';
    d->seq = seq;
    d->readLength = readLength;
    d->profile = NULL;
    d->rc_profile = NULL;
    d->mat = mat;
    d->ssw_scores = ssw_scores;
    // data contains reverse complement sequnce, then optionally forward numbers then reverse complement numbers, and reverse quals
    d->_data = (int8_t*) malloc_chk0(readLength * 4);
    d->rc_seq = (char*) d->_data;
    reverseComplementSeq(seq, d->rc_seq, readLength);
    // initialize these as needed
    d->read_num = NULL;
    d->rc_read_num = NULL;
    return d;
}

void freeReadData(ReadData d) {
    free_chk0(d->name);
    free_chk0(d->_data);
    if (d->profile) init_destroy(d->profile);
    if (d->rc_profile) init_destroy(d->rc_profile);
    free_chk0(d);
}

int8_t * _prepReadSSW(int8_t *read_num, const char *read_seq, int read_length, const int8_t *mat, s_profile **profile) {
    for (int m = 0; m < read_length; ++m) {
        read_num[m] = (int8_t)nt_table[(int)read_seq[m]];
    }
    int matchscore = mat[0];
    assert(matchscore > 0);
    int maxAlignLen = read_length + 2 * ( (read_length * ALIGN_EXPAND_FRACTION) + 0.5 + ALIGN_EXPAND_BASES ) + 1;
    assert(maxAlignLen > read_length);
    int maxscore = maxAlignLen * matchscore;
    if (maxscore <= 0 || maxscore < maxAlignLen) DIE("Error you must initialize the score matrix before now\n");
    *profile = ssw_init(read_num, read_length, mat, 5, maxscore >=254 ? 1 : 0);
    DBG2("Prepped read length=%d maxscore=%d\n", read_length, maxscore);
    return read_num;
}

void prepReadSSW(ReadData d, int isRC) {
    if (isRC) {
        if (d->rc_read_num == NULL) {
            d->rc_read_num = _prepReadSSW(d->_data + d->readLength * 2, d->rc_seq, d->readLength, d->mat, &d->rc_profile);
        }
    } else {
        if (d->read_num == NULL) {
            d->read_num = _prepReadSSW(d->_data + d->readLength, d->seq, d->readLength, d->mat, &d->profile);
        }
    }
}

void prepReverseQuals(ReadData d) {
    if (!d->r_quals) {
        d->r_quals = (char*) (d->_data + d->readLength * 3);
        const char *start = d->quals, *end = d->quals + d->readLength - 1;
        char *r_start = d->r_quals;
        while (start < end) {
            *r_start = *end;
            r_start++;
            end--;
        }
    }
}

int rb_tree_cmp_alignment(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b) {
    ContigAlignment a = (ContigAlignment) node_a->value;
    ContigAlignment b = (ContigAlignment) node_b->value;
    int a_thr = upc_threadof(a->contigPtr);
    int b_thr = upc_threadof(b->contigPtr);
    DBG3("rb_tree_cmp_alignment: %d,%lld vs %d,%lld\n", a_thr, (lld) upc_addrfield(a->contigPtr), b_thr, (lld) upc_addrfield(b->contigPtr));
    if (a_thr < b_thr) {
        return -1;
    } else if (a_thr == b_thr) {
        int64_t a_addr = upc_addrfield(a->contigPtr);
        int64_t b_addr = upc_addrfield(b->contigPtr);
        if (a_addr < b_addr) {
            return -1;
        } else if (a_addr == b_addr) {
            return 0;
        } else {
            return 1;
        }
    } else {
        assert(a_thr > b_thr);
        return 1;
    }
}

void freeContigAlignment(ContigAlignment align) {
    if (align == NULL) return;
    if (align->seq) free_chk0(align->seq);
    if (align->nAlignments) {
        for(int i = 0; i < align->nAlignments; i++) {
            if (align->alignments[i].align) align_destroy(align->alignments[i].align);
        }
    }
    if (align->alignments) {
        free_chk0(align->alignments);
    }
    assert(!align->alignments);
    free_chk0(align);
}

void rb_tree_free_alignment(struct rb_tree *self, struct rb_node *node) {
    ContigAlignment a = (ContigAlignment) node->value;
    freeContigAlignment(a);
}

#define USE_EXACT_MATCH_OPTIMIZATION

// quickly find the 0-based position of the first mismatch between two sequences.  returns alignLen if none
int findFirstMismatch(const char * startReadSeq, const char * startContigSeq, int alignLen) {
    int mismatches = 0;
    const int64_t *startReadWord = (const int64_t *) startReadSeq, *startContigWord = (const int64_t *) startContigSeq;
    int alignJumps = alignLen / 8;
    for(int i = 0 ; i < alignJumps; i++) {
        // test 8 bytes at a time
        if ( startReadWord[i] != startContigWord[i] ) {
            // at least one mismatch
            // test each base;
            for(int j = i*8; j < i*8+8; j++) {
                if ( startReadSeq[j] != startContigSeq[j] ) {
                    return j;
                }
            }
        }
    }
    // remainder
    for(int j = alignJumps * 8; j < alignLen; j++) {
        if ( startReadSeq[j] != startContigSeq[j] ) {
            return j;
        }
    }
    return alignLen;
}

// quickly find the 0-based position of the last mismatch between two sequences.  returns alignLen if none
int findLastMismatch(const char * startReadSeq, const char * startContigSeq, int alignLen) {
    int mismatches = 0;
    const int64_t *startReadWord = (const int64_t *) startReadSeq, *startContigWord = (const int64_t *) startContigSeq;
    int alignJumps = alignLen / 8;
    // remainder first
    for(int j = alignLen - 1; j >= alignJumps * 8; j--) {
        if ( startReadSeq[j] != startContigSeq[j] ) {
            return j;
        }
    }
    for(int i = alignJumps-1 ; i >= 0; i--) {
        // test 8 bytes at a time
        if ( startReadWord[i] != startContigWord[i] ) {
            // at least one mismatch
            // test each base;
            for(int j = i*8+7 ; j >= i*8; j--) {
                if ( startReadSeq[j] != startContigSeq[j] ) {
                    return j;
                }
            }
        }
    }
    return alignLen;
}

int countMismatches(const char * startReadSeq, const char * startContigSeq, int alignLen, int maxMismatches) {
    int offset = 0, mismatches = 0;
    while (offset < alignLen) {
        int pos = findFirstMismatch(startReadSeq + offset, startContigSeq + offset, alignLen - offset);
        if (pos + offset < alignLen) {
            mismatches++;
            if (mismatches > maxMismatches) break;
        }
        offset += pos + 1;
    }
    return mismatches;
}

// return dist == 0 and overlap > 0 if the two regions overlap
// or overlap == 0 and dist != 0 (with direction) if regions do not overlap
void getDistAndOverlap(int start, int end, int astart, int aend, int *dist, int *overlap) {
    assert(start < end);
    assert(astart < aend);
    *dist = 0;
    *overlap = 0;
    if (astart <= start && start <= aend) {
        // a is overlapping start
        *dist = 0;
        assert(aend >= start);
        *overlap = aend - start;
    }
    if (astart <= end && end <= aend) {
        // a is overlapping end
        *dist = 0;
        if (*overlap > 0) {
            // a is also overlaping start so it is fully enclosing: astart <= start < end <= aend
            *overlap = end - start;
        } else {
            assert(end >= astart);
            *overlap = end - astart;
        }
    }
    if ( start <= astart && aend <= end ) {
        // a is fully enclosed: start <= astart < aend <= end
        *overlap = aend - astart;
    }
    if (*overlap) return; // done
    // no overlap
    if (end <= astart) {
        // a is to the right, so positive distance
        *dist = astart - end;
    } else {
        // a is to the left, so negative distance
        assert(start >= aend);
        *dist = aend - start;
    }
}

int isAlignmentCloser(int start, int end, ContigAlignment a, Alignment *aa, ContigAlignment b, Alignment *bb) {
    int a_start = aa->align->ref_begin1 + a->local_contig.offsetInParent;
    int a_end = aa->align->ref_end1 + a->local_contig.offsetInParent;
    int b_start = bb->align->ref_begin1 + b->local_contig.offsetInParent;
    int b_end = bb->align->ref_end1 + b->local_contig.offsetInParent;
    int a_dist, a_overlap, b_dist, b_overlap;
    getDistAndOverlap(start, end, a_start, a_end, &a_dist, &a_overlap);
    getDistAndOverlap(start, end, b_start, b_end, &b_dist, &b_overlap);
    if (a_overlap > 0 || b_overlap > 0) {
        return a_overlap > b_overlap;
    } else {
        return abs(a_dist) > abs(b_dist);
   }
}


// merge the overlapping sequences from src into a larger dest
// adjust any existing alignment ref coordinates
void mergeContigAlignment(ContigAlignment dest, ContigAlignment src) {
    assert(dest->contigPtr != src->contigPtr);
    assert(dest->local_contig.parentContigID == src->local_contig.parentContigID);
    assert(dest->local_contig.offsetInParent != src->local_contig.offsetInParent);
    if (src->local_contig.uuType == NON_UU_CONTIG) dest->local_contig.uuType = NON_UU_CONTIG;
    int offset = src->local_contig.offsetInParent - dest->local_contig.offsetInParent;
    if (offset <= 0) DIE("Can not mergeContigAlignment when dest is to the right of src... Change your logic!\n");
    if (offset > 0) {
       // dest is to the left of src
       // validate overlapping region is the same
       int overlapLen = dest->local_contig.length - offset;
       if (overlapLen > src->local_contig.length) overlapLen = src->local_contig.length;
       if (overlapLen < 0) DIE("Invalid assumption!\n");
       if (memcmp(dest->seq + offset, src->seq, overlapLen) != 0) {
           WARN("Looked for overlapLen=%d '%.*s' '%.*s'\n", overlapLen, overlapLen, dest->seq + offset, overlapLen, src->seq);
           DIE("Expected left overlap but it did not match! offset=%d overlapLen=%d : dest=%p src=%p contig_id=%lld %lld offsetInParent=%lld %lld\n", offset, overlapLen, dest, src, (lld) dest->local_contig.contig_id, (lld) src->local_contig.contig_id, dest->local_contig.offsetInParent, src->local_contig.offsetInParent);
       }
       int extraLen = src->local_contig.length - overlapLen;
       if (extraLen > 0) {
           dest->seq = realloc_chk0(dest->seq, dest->local_contig.length + extraLen + 1);
           memcpy(dest->seq + dest->local_contig.length, src->seq + overlapLen, extraLen);
           dest->local_contig.length += extraLen;
           DBG2("Merged two ContigAlignments %lld + %lld (parent %lld len %d): extraLen=%d newLen=%d\n", (lld) dest->local_contig.contig_id, (lld) src->local_contig.contig_id, (lld) dest->local_contig.parentContigID, dest->local_contig.parentLength, extraLen, dest->local_contig.length);
           // offsetInParent does not change
           // existing alignments do not change
       }
    }
}

// using a kmer seed on a read and contig, with known positions either:
// 1) find an existing alignment that covers that kmer seed
// or
// 2) discover a new exact full length alignment of the read
// or
// 3) compute a ssw around that seed
// returns the alignment if a new one is discovered or nothing if no alignment is found or only a duplicate was found
AlignmentResult alignReadToContig(struct rb_tree *tree, ReadData read, int _readPos, int kmer_len, contigInfo contig_info, contig_cache_t contigCache, int ref_is_masked, int withCigar) {
    UPC_TICK_T start_timer, end_timer;
    AlignmentResult ret; memset(&ret, 0, sizeof(AlignmentResult));
    _ContigAlignment test; memset(&test, 0, sizeof(_ContigAlignment));
    test.contigPtr = contig_info.my_contig;
    ContigAlignment align = rb_tree_find(tree, &test);
    DBG2("Checking contig_ptr %d,%lld posInContig=%d\n", upc_threadof(contig_info.my_contig), (lld)upc_addrfield(contig_info.my_contig), contig_info.posInContig);
    if (align) {
        DBG2("Found existing ContigAlignment entry contig %lld parent %lld: %p\n", (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID, align);
        assert(contig_info.my_contig == align->contigPtr);
    } else {
        // new entry, populate it
        align = calloc_chk0(sizeof(_ContigAlignment), 1);
        align->contigPtr = contig_info.my_contig;

#ifdef PROFILE
        start_timer = UPC_TICKS_NOW();
#endif

        contigDataPtr remote_contig_ptr = NULL;
#ifdef USE_SWCACHE
        if (contigCache != NULL) {
            remote_contig_ptr = findRemoteContigPtr(contigCache, align->contigPtr, &(align->local_contig));
        } else {
#endif
            align->local_contig = *(align->contigPtr); // network copy
            remote_contig_ptr = (contigDataPtr)(&(align->local_contig.contig[0]));
#ifdef USE_SWCACHE
        }
#endif
        assert(align->local_contig.length > 0);
        assert(IS_VALID_UPC_PTR(align->local_contig.contig));
        assert(remote_contig_ptr != NULL);
        assert(IS_VALID_UPC_PTR(remote_contig_ptr));
        // copy contig sequence to local memory
        char *tmpseq = malloc_chk0(align->local_contig.length+1);
        upc_memget(tmpseq, remote_contig_ptr, align->local_contig.length * sizeof(char));

#ifdef PROFILE
        end_timer = UPC_TICKS_NOW();
        fetch_contigs_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif

        tmpseq[align->local_contig.length] = '\0'; // ensure null termination
        if (ref_is_masked) {
            /* remove the masking to allow ssw */
            for (int i = 0; i < align->local_contig.length; i++) {
                tmpseq[i] = toupper(tmpseq[i]);
            }
        }
        align->seq = tmpseq;
        int status = rb_tree_insert(tree, align);
        if (status != 1) DIE("Could not insert a new ContigAlignment!\n");
        DBG2("Created new ContigAlignment entry: contig_id=%lld parent=%lld of length %d: %p %d,%lld %s\n", (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID, align->local_contig.length, align, upc_threadof(align->contigPtr), (lld) upc_addrfield(align->contigPtr), align->local_contig.uuType == UU_CONTIG ? "UU" : "DupKmers");
    }

    int readPos = _readPos;

    // 1) look for an existing alignment that covers the contig_info kmer position from this contig
    for(int i = 0; i < align->nAlignments; i++) {
        Alignment *alignment = align->alignments + i;
        if (contig_info.posInContig >= alignment->align->ref_begin1 && contig_info.posInContig + kmer_len - 1  <= alignment->align->ref_end1) {
            // alignment covers this seed
            DBG2("Found existing overlapping alignment for contig %lld (parent %lld) posInContig=%d. readPos=%d start=%d end=%d\n", align->local_contig.contig_id, align->local_contig.parentContigID, contig_info.posInContig, readPos, alignment->align->read_begin1, alignment->align->read_end1);
            return ret; // should be zeros.
        }
    }

    // 1a) check existing alignments to same region in parent contig
    ContigAlignment adjacentAlign = NULL;
    struct rb_iter *iter = rb_iter_create();
    if (iter) {
        for (ContigAlignment testAlign = rb_iter_last(iter, tree); testAlign; testAlign = rb_iter_prev(iter)) {
            if (testAlign == align) continue;
            if (testAlign->local_contig.contig_id == align->local_contig.contig_id) DIE("Detected two entries for the same contig: %lld parent %lld! %p and %p\n", (lld) testAlign->local_contig.contig_id, testAlign->local_contig.parentContigID, align, testAlign);
            if (testAlign->contigPtr == align->contigPtr) DIE("Detected two entries for the same contig2!\n");
            if (testAlign->local_contig.parentContigID != align->local_contig.parentContigID) continue;
            int adjustToParent = testAlign->local_contig.offsetInParent - align->local_contig.offsetInParent;
            // check for overlapping sequence adjacency
            int dist, overlap;
            getDistAndOverlap(align->local_contig.offsetInParent, align->local_contig.offsetInParent + align->local_contig.length,
                              testAlign->local_contig.offsetInParent, testAlign->local_contig.offsetInParent + testAlign->local_contig.length,
                              &dist, &overlap);
            if (overlap > 0) {
                // try to find existing alignment in testAlign contig that overlaps the seed kmer
                for (int i = 0; i < testAlign->nAlignments; i++) {
                    Alignment *testAlignment = testAlign->alignments + i;
                    int testStart = testAlignment->align->ref_begin1 + adjustToParent;
                    int testEnd = testAlignment->align->ref_end1 + adjustToParent;
                    if (contig_info.posInContig >= testStart
                            && contig_info.posInContig + kmer_len - 1 <= testEnd) {
                        // a shortcut where testAlignment covers the seed
                        rb_iter_dealloc(iter);
                        DBG2("Found alternate existing overlapping alignment for contig %lld (parent %lld) posInContig=%d. readPos=%d start=%d end=%d\n", testAlign->local_contig.contig_id, testAlign->local_contig.parentContigID, contig_info.posInContig, readPos, testAlignment->align->read_begin1, testAlignment->align->read_end1);
                        return ret; // should be zeros
                    }
                }
                adjacentAlign = testAlign;
            }
        }
        if (adjacentAlign) {
            // merge reference sequence of adjacentAlign into this align
            assert(align->local_contig.offsetInParent != adjacentAlign->local_contig.offsetInParent);
            if (align->local_contig.offsetInParent < adjacentAlign->local_contig.offsetInParent) {
                // append adjacent to existing align which is to the right of align on the reference
                mergeContigAlignment(align, adjacentAlign);
                // keep adjacentAlign to compare with later
            } else {
                // append existing align to adjacent, continue with adjacent and transformed contig_info
                mergeContigAlignment(adjacentAlign, align); // may be noop if already merged
                // continue with adjacent Alignment and a newly faked contig_info
                int adjustment = align->local_contig.offsetInParent - adjacentAlign->local_contig.offsetInParent;
                DBG2("Adjusting contig_info from contig=%lld pos=%d to contig=%lld pos=%d (offset=%d offset=%d)\n", align->local_contig.contig_id, contig_info.posInContig, adjacentAlign->local_contig.contig_id, contig_info.posInContig + adjustment, align->local_contig.offsetInParent, adjacentAlign->local_contig.offsetInParent);
                align = adjacentAlign;
                contig_info.my_contig = align->contigPtr;
                contig_info.posInContig += adjustment;

                // no need to merge these in later
                adjacentAlign = NULL;
            }
            // now fall through and let the full read re-align to the larger sequence and replace if they end up overlapping by > kmer_len
        }
        rb_iter_dealloc(iter);
    } else DIE("Could not allocate an iterator!\n");

    // TODO use adjacentAlign to merge an overlapping SW into one alignment, if possible 
    // TODO flag if overlaping alignment may need to be merged and do it if necessary (important for long reads!)

#ifdef PROFILE
    start_timer = UPC_TICKS_NOW();
#endif

    // no match, perform a new alignment
    int nAlignment = align->nAlignments++; // will decrement to erase if alignment eventually fails..
    align->alignments = realloc_chk0(align->alignments, sizeof(Alignment) * align->nAlignments); // almost always just the single entry.
    Alignment *alignment = align->alignments + nAlignment;
    memset(alignment, 0, sizeof(Alignment)); // zero out new space

    // check if we should use the forward or reverse complement versions of the read
    int isRC = 0;
    const char *readseq = read->seq;
    const char *contigseq = align->seq + contig_info.posInContig;
    if (memcmp(readseq + readPos, contigseq, kmer_len) != 0) {

        // adjust readSeq & readPos to be in RC here
        readPos = read->readLength - readPos - kmer_len;
        readseq = read->rc_seq;

        if (memcmp(readseq + readPos, contigseq, kmer_len) != 0) {
            DIE("Neither forward nor rc kmers for read at %d match contig(%lld : %lld) at %d! read %s: '%.*s' / '%.*s' vs '%.*s' -- full read: '%.*s' full contig: '%.*s'\n",
                readPos, (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID, (lld) contig_info.posInContig, read->name,
                kmer_len, read->seq + readPos, kmer_len, readseq,
                kmer_len, align->seq + contig_info.posInContig,
                read->readLength, read->seq,
                align->local_contig.length, align->seq);
        }
        isRC = 1;
    }

    // calculate available bases before and after the seeded kmer
    int contigBasesLeftOfKmer = contig_info.posInContig;
    int contigBasesRightOfKmer = align->local_contig.length - contigBasesLeftOfKmer - kmer_len;
    int readBasesLeftOfKmer = readPos;
    int readBasesRightOfKmer = read->readLength - kmer_len - readPos;
    int leftOfKmer = contigBasesLeftOfKmer > readBasesLeftOfKmer ? readBasesLeftOfKmer : contigBasesLeftOfKmer;
    int rightOfKmer = contigBasesRightOfKmer > readBasesRightOfKmer ? readBasesRightOfKmer : contigBasesRightOfKmer;

    const char *startContigSeq = align->seq + contig_info.posInContig - leftOfKmer;
    const char *startReadSeq = readseq + readPos - leftOfKmer; // readseq is already forward or reverse
    int alignLen = leftOfKmer + kmer_len + rightOfKmer;
    DBG2("read=%s isRC=%d, readLen=%d, readPos=%d (%d), alignLen=%d, leftOfKmer=%d, kmer_len=%d, rightOfKmer=%d, contig=%lld parentContig=%lld posInContig=%d, contigLen=%d, clok=%d, crok=%d, rlok=%d, rrok=%d\n",
        read->name, isRC, read->readLength, readPos, _readPos, alignLen, leftOfKmer, kmer_len, rightOfKmer,
        (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID, contig_info.posInContig,
        align->local_contig.length, contigBasesLeftOfKmer, contigBasesRightOfKmer,
        readBasesLeftOfKmer, readBasesRightOfKmer);
    assert(leftOfKmer <= contig_info.posInContig);
    assert(alignLen <= read->readLength);
    assert(alignLen <= align->local_contig.length - (contig_info.posInContig - leftOfKmer));

    // 2) quick check for a full length exact match on first kmer if contig is long enough

#ifdef USE_EXACT_MATCH_OPTIMIZATION
    if (adjacentAlign== NULL && (isRC ? readPos == (read->readLength - kmer_len) : readPos == 0) && alignLen == read->readLength && ((align->local_contig.length - contig_info.posInContig) >= read->readLength)) {
        assert(alignLen == read->readLength);
        assert(readPos - leftOfKmer == 0);
        int match_result = memcmp(startReadSeq, startContigSeq, alignLen * sizeof(char));
        if (match_result == 0) {

            // set alignment metrics
            uint32_t dummy[2];
            s_align *sw = alignment->align = align_alloc();
            sw->read_begin1 = 0;
            sw->read_end1 = alignLen - 1;
            sw->ref_begin1 = contig_info.posInContig - leftOfKmer;
            sw->ref_end1 = sw->ref_begin1 + alignLen - 1;

            if (withCigar) {
                sw->cigarLen = 1;
                sw->cigar = malloc(sizeof(uint32_t) * sw->cigarLen);
                sw->cigar[0] = to_cigar_int(read->readLength, '='); // exact match
            }
            sw->score1 = read->readLength * read->ssw_scores->match; // every position matches
            sw->score2 = 0;  // no other good match
            DBG2("Found exact %s alignment contig %lld parent %lld score=%d,%d read %d-%d ref %d-%d cigarlen=%d\n", isRC ? "revcomp" : "forward",
                (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID,
                sw->score1, sw->score2, sw->read_begin1, sw->read_end1, sw->ref_begin1, sw->ref_end1, sw->cigarLen);

            assert(nAlignment == align->nAlignments - 1);
            if (align->bestAlignment < nAlignment && alignment->align->score1 > align->alignments[align->bestAlignment].align->score1) {
                // This is now the best alignment
                align->bestAlignment = nAlignment;
            }
            alignment->isRC = isRC;
            alignment->mismatches = 0;
            ret.contigAlignment = align;
            ret.alignment = alignment;
#ifdef PROFILE
            end_timer = UPC_TICKS_NOW();
            exact_alignment_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
            return ret;
        } else {
            DBG3("Exact match failed: '%.*s' vs '%.*s'\n", alignLen, startReadSeq, alignLen, startContigSeq);
        }
    } else {
        DBG2("Skipping exact test: isRC=%d readPos=%d alignLen=%d readLen=%d %d < readLength\n", isRC, readPos, alignLen, read->readLength, (align->local_contig.length - contig_info.posInContig));
    }
#endif

#ifdef USE_INEXACT_MATCH_OPTIMIZATION // FIXME inexact has different edging than the full SW alignment
    // if contig position supports full read alignment, align and count mismatches
    // if <=4% of the length disagrees, claim success and fake sw scores
    int allowedMismatches = read->readLength * USE_INEXACT_MATCH_OPTIMIZATION;
    if (adjacentAlign == NULL && alignLen == read->readLength && allowedMismatches > 0) {
        int mismatches = countMismatches(startReadSeq, startContigSeq, alignLen, allowedMismatches);
        if (mismatches <= allowedMismatches) {

            // set alignment metrics
            uint32_t dummy[2];
            s_align *sw = alignment->align = align_alloc();
            sw->read_begin1 = 0;
            sw->read_end1 = alignLen - 1;
            sw->ref_begin1 = contig_info.posInContig - leftOfKmer;
            sw->ref_end1 = sw->ref_begin1 + alignLen - 1;

            int matchLen = alignLen;
            int softclip_head = 0, softclip_tail = 0;

            // softclip edges, where mismatched
            int testClipDelta = 0, deltamismatches = 0;
            for (int i = 0 ; i < mismatches; i++) { 
                if (startReadSeq[softclip_head] != startContigSeq[softclip_head]) {
                    deltamismatches++;
                    testClipDelta += read->ssw_scores->mismatch;
                } else {
                    if (i >= 1) break; // at least 2 to clip
                    testClipDelta -= read->ssw_scores->match;
                }
                softclip_head++;
            }
            if (testClipDelta <= 0) {
                softclip_head = 0;
            } else {
                mismatches -= deltamismatches;
            }
            testClipDelta = 0;
            deltamismatches = 0;
            for (int i = 0 ; i < mismatches; i++) { 
                if (startReadSeq[alignLen - softclip_tail - 1] != startContigSeq[alignLen - softclip_tail - 1]) {
                    deltamismatches++;
                    testClipDelta += read->ssw_scores->mismatch;
                } else {
                    if (i >= 1) break; // at least 2 to clip
                    testClipDelta -= read->ssw_scores->match;
                }
                softclip_tail++;
            }
            if (testClipDelta <= 0) {
                softclip_tail = 0;
            } else {
                mismatches -= deltamismatches;
            }

            // adjust numbers based on softclips
            matchLen -= softclip_head + softclip_tail;
            sw->read_begin1 += isRC ? softclip_tail : softclip_head;
            sw->ref_begin1 += softclip_head;
            sw->read_end1 -= isRC ? softclip_head : softclip_tail;
            sw->ref_end1 -= softclip_tail;

            if (withCigar) {
                sw->cigarLen = 1 + (softclip_head == 0 ? 0 : 1) + (softclip_tail == 0 ? 0 : 1);
                sw->cigar = malloc(sizeof(uint32_t) * sw->cigarLen);
                int op = 0;
                if (softclip_head) {
                    sw->cigar[op++] = to_cigar_int(softclip_head, 'S');
                }
                sw->cigar[op++] = to_cigar_int(matchLen, '='); // exact match
                if (softclip_tail) {
                    sw->cigar[op++] = to_cigar_int(softclip_tail, 'S');
                }
            }

            sw->score1 = (matchLen - mismatches) * read->ssw_scores->match - mismatches * read->ssw_scores->mismatch;
            sw->score2 = 0;  // no other good match
            DBG2("Found inexact %s alignment contig %lld parent %lld score=%d,%d mismatches=%d headClip=%d tailClip=%d read %d-%d ref %d-%d cigarlen=%d matchLen=%d alignLen=%d\n%.*s\n%.*s\n", isRC ? "revcomp" : "forward",
                (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID,
                sw->score1, sw->score2, mismatches, softclip_head, softclip_tail, sw->read_begin1, sw->read_end1, sw->ref_begin1, sw->ref_end1, sw->cigarLen, matchLen, alignLen,
                alignLen, startReadSeq, alignLen, startContigSeq);

            assert(nAlignment == align->nAlignments - 1);
            if (align->bestAlignment < nAlignment && alignment->align->score1 > align->alignments[align->bestAlignment].align->score1) {
                // This is now the best alignment
                align->bestAlignment = nAlignment;
            }
            alignment->isRC = isRC;
            alignment->mismatches = mismatches;
            ret.contigAlignment = align;
            ret.alignment = alignment;
#ifdef PROFILE
            end_timer = UPC_TICKS_NOW();
            inexact_alignment_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
            return ret;
        } else {
            DBG2("Inexact match failed: mismatches=%d alignLen=%d\n", mismatches, alignLen);
        }
    } else {
        DBG2("Skipping inexact test readLength=%d alignLen=%d\n", read->readLength, alignLen);
    }
#endif

    //
    // 3) last restort is to perform a new ssw
    //

    // prep the reference.  Add a few bases on either side, if possible
    int extraContigBases = (alignLen * ALIGN_EXPAND_FRACTION) + 0.5 + ALIGN_EXPAND_BASES;
    int contigAlignLen = alignLen;
    int extraLeft = contigBasesLeftOfKmer - leftOfKmer;
    if (extraLeft > 0) {
        if (extraLeft > extraContigBases) extraLeft = extraContigBases;
        contigAlignLen += extraLeft;
        startContigSeq -= extraLeft;
    } else {
        extraLeft = 0;
    }
    int refOffset = contig_info.posInContig - leftOfKmer - extraLeft;
    assert(refOffset >= 0);
    int extraRight = contigBasesRightOfKmer - rightOfKmer;
    if (extraRight > 0) {
        if (extraRight > extraContigBases) extraRight = extraContigBases;
        contigAlignLen += extraRight;
    } else {
        extraRight = 0;
    }
    DBG2("Prepping contig for ssw contigLen=%d contigAlignLen=%d alignLen=%d extraLeft=%d refOffset=%d extraRight=%d\n", align->local_contig.length, contigAlignLen, alignLen, extraLeft, refOffset, extraRight);
    int8_t *ref_num = malloc_chk0(contigAlignLen);
    for (int m = 0; m < contigAlignLen; ++m) {
        ref_num[m] = (int8_t) nt_table[(int)startContigSeq[m]];
    }

    int flag = withCigar ? 2 : 7;
    int filters = read->ssw_scores->match * kmer_len;
    int filterd = 0;
    int maskLen = read->readLength / 2 < 15 ? 15 : read->readLength / 2;
    prepReadSSW(read, isRC);
    s_align *sw = ssw_align(isRC ? read->rc_profile : read->profile, ref_num, contigAlignLen, read->ssw_scores->gap_open, read->ssw_scores->gap_extension, flag, filters, filterd, maskLen);
    if (!sw) {
        LOGF("No SW alignment!\n");
        align->nAlignments--;
#ifdef PROFILE
        end_timer = UPC_TICKS_NOW();
        sw_alignment_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
        return ret; // should be zeros
    } else {
        DBG2("Found sw: read:%d-%d ref:%d-%d end2=%d score1=%d score2=%d refOffset=%d (not applied yet) isRC=%d\n", sw->read_begin1, sw->read_end1, sw->ref_begin1, sw->ref_end1, sw->ref_end2, sw->score1, sw->score2, refOffset, isRC);
    }

    // fix the cigar to set mismatches (= or X instead of M)
    if (withCigar) {
        assert(sw->cigarLen > 0);
        alignment->mismatches = mark_mismatch(sw->ref_begin1, sw->read_begin1, sw->read_end1, ref_num, isRC ? read->rc_read_num : read->read_num, read->readLength, &(sw->cigar), &(sw->cigarLen));
        assert(sw->cigarLen > 0);
    }

    // fix alignments to actual reference positions
    sw->ref_begin1 += refOffset;
    sw->ref_end1 += refOffset;

    if (isRC) {
        // fix read coordinates back to forward
        int new_start = read->readLength - sw->read_end1 - 1;
        int new_end = read->readLength - sw->read_begin1 - 1;
        sw->read_begin1 = new_start;
        sw->read_end1 = new_end;

        /* if SAM is output, then the sequence will be output reverse complement too. */

    }

    // verify this alignment is not a duplicate (this can arise when short repetetive kmers in a series are the seeds)
    assert(nAlignment == align->nAlignments - 1);
    for (int t = 0; t < nAlignment; t++) {
        s_align *test = align->alignments[t].align;
        if (sw->read_begin1 == test->read_begin1
            && sw->read_end1 == test->read_end1
            && sw->ref_begin1 == test->ref_begin1
            && sw->ref_end1 == test->ref_end1) {
            DBG2("Found duplicate (not overlapping original posInContig seed) alignment of %s %d-%d to %lld parent %lld %d-%d\n", read->name, sw->read_begin1, sw->read_end1, align->local_contig.contig_id, align->local_contig.parentContigID, sw->ref_begin1, sw->ref_end1);
            align->nAlignments--;
#ifdef PROFILE
            end_timer = UPC_TICKS_NOW();
            sw_alignment_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
            return ret; // should be zeros
        }
    }

    alignment->isRC = isRC;
    alignment->align = sw; // transfer ownership.  Will be align_destroy() later

    assert(nAlignment == align->nAlignments - 1);
    if (align->bestAlignment < nAlignment && alignment->align->score1 > align->alignments[align->bestAlignment].align->score1) {
        // This is now the best alignment
        align->bestAlignment = nAlignment;
    }

    DBG2("Found %s sw match (%s to Contig %lld parent %lld): score1=%d score2=%d read (len=%d) %d-%d, ref (len=%d) %d-%d. nAlignment=%d nAlignments=%d\n",
        isRC ? "revcomp" : "forward", read->name, (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID,
        alignment->align->score1, alignment->align->score2, read->readLength, alignment->align->read_begin1, alignment->align->read_end1,
        align->local_contig.length, alignment->align->ref_begin1, alignment->align->ref_end1, nAlignment, align->nAlignments);

#ifdef DEBUG
    if (sw->score1 == read->readLength * read->ssw_scores->match) {
        DBG3("Checking exact match is an exact match: '%.*s' '%.*s'\n", read->readLength, startReadSeq, read->readLength, startContigSeq + extraLeft);
        if (memcmp(startReadSeq, startContigSeq + extraLeft, read->readLength * sizeof(char)) != 0) {
            DIE("sw match of exact is not an exact match!\nFound %s sw match (%s to Contig %lld parent %lld): score1=%d read (len=%d) %d-%d, ref (len=%d) %d-%d\n",
                isRC ? "revcomp" : "forward", read->name, (lld) align->local_contig.contig_id, (lld) align->local_contig.parentContigID,
                alignment->align->score1, read->readLength, alignment->align->read_begin1, alignment->align->read_end1,
                align->local_contig.length, alignment->align->ref_begin1, alignment->align->ref_end1);
        }
    }
#endif

    free_chk0(ref_num);

    ret.contigAlignment = align;
    ret.alignment = alignment;
#ifdef PROFILE
    end_timer = UPC_TICKS_NOW();
    sw_alignment_time += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
    return ret;
}

void print_aln(Buffer b, ContigAlignment contigAlignment, Alignment *alignment, ReadData read, char mer_i, int isBest, int _output_format)
{
    // Translate coordinates to parent contig
    int64_t contig_id = 0;
    int contig_length = 0, cbegin = 0, cend = 0;
    int posInContigPrimer = 0;
    int rbegin = 0, rend = 0;
    int strand = PLUS;
    int score1 = 0, score2 = 0;
    if (contigAlignment && alignment) {
        contig_length = contigAlignment->local_contig.parentLength;
        posInContigPrimer = contigAlignment->local_contig.offsetInParent;
        contig_id = contigAlignment->local_contig.parentContigID;
        strand = alignment->isRC ? MINUS : PLUS;
        s_align *r = alignment->align;
        cbegin = posInContigPrimer + r->ref_begin1 + 1;
        cend = posInContigPrimer + r->ref_end1 + 1;
        rbegin = r->read_begin1 + 1;
        rend = r->read_end1 + 1;
        score1 = r->score1;
        score2 = r->score2;
    } else {
        assert(mer_i == 'F');
    }
    char refName[255];
    sprintf(refName, "Contig%lld", (lld) contig_id);
    int read_length = read->readLength;

    if (_output_format == BLAST) {
        // output blast
        DBG2("Printing alignment for %s to %s\n", read->name, refName);
        printfBuffer(b, "MERALIGNER-%c\t%s\t%d\t%d\t%d\t%s\t%d\t%d\t%d\t%s\t0\t0\t0\t0\t%d\t%d\n",
                mer_i, read->name, rbegin, rend, read_length, refName,
                cbegin, cend, contig_length, (strand == PLUS) ? "Plus" : "Minus", score1, score2);
    } else {
        assert(_output_format == SAM);
        int min_score = 0;
        LOGF("Printing SAM alignment for %s to %s\n", read->name, refName);
        if (score1 >= min_score) {
            // output SAM
            // FIXME for paired reads pick best alignment with both on same contig if possible!
            printfBuffer(b, "%s\t", read->name);
            if (score1 == 0 || !alignment) {
                printfBuffer(b, "4\t*\t0\t255\t*\t*\t0\t0\t*\t*\n");
            } else {
                int32_t c, p;
                uint32_t mapq;
                if (alignment->isRC) prepReverseQuals(read);
                // for perfect match, set to same maximum as used by BWA
                if (score2 == 0) mapq = 42;
                else mapq = -4.343 * log(1 - (double)abs(score1 - score2)/(double)score1);
                mapq = (uint32_t) (mapq + 4.99);
                mapq = mapq < 254 ? mapq : 254;
                int flag = 0;
                const char * seq = read->seq;
                if (strand == MINUS) {
                    flag |= 16; // seq is revcomp
                    seq = read->rc_seq;
                }
                if (!isBest) {
                    flag |= 256;
                }
                printfBuffer(b, "%d\t%s\t%d\t%d\t", flag, refName, cbegin, mapq);

                for (int c = 0; c < alignment->align->cigarLen; ++c) {
                    char letter = cigar_int_to_op(alignment->align->cigar[c]);
                    uint32_t length = cigar_int_to_len(alignment->align->cigar[c]);
                    printfBuffer(b, "%lu%c", (unsigned long)length, letter);
                }
                if (!alignment->align->cigarLen) { 
                    printfBuffer(b, "*");
                    WARN("empty cigar, score1 %d score2 %d\n", score1, score2);
                }
                printfBuffer(b, "\t*\t0\t0\t");
                printfBuffer(b, "%.*s\t%.*s", read_length, seq, read_length, alignment->isRC ? read->r_quals : read->quals);
                if (alignment) printfBuffer(b, "\tNM:i:%d", alignment->mismatches);
                if (alignment) printfBuffer(b, "\tAS:i:%d", score1);
                printfBuffer(b, "\n");
            }
        }
    }
}

int alignment_by_read_pos(const void *_a, const void *_b) {
    AlignmentResult *a = (AlignmentResult*) _a;
    AlignmentResult *b = (AlignmentResult*) _b;
    if (a->alignment->align->read_begin1 == b->alignment->align->read_begin1) {
        if (a->alignment->align->score1 == b->alignment->align->score1) {
            return a->contigAlignment->local_contig.parentContigID > b->contigAlignment->local_contig.parentContigID; // ignore potential duplicate alignment case
        } else {
            return a->alignment->align->score1 < b->alignment->align->score1;
        }
    } else {
        return a->alignment->align->read_begin1 > b->alignment->align->read_begin1;
    }
}

void print_alignments(Buffer outBuffer, int numAlignments, struct rb_tree *read_alignments, ReadData read, int _output_format) {

    if (numAlignments == 0) {
        //printf("The read contains no valid kmer - forward search\n");
        /* FIXME: Remove this "guard alignment" when processing a single read file containing both lanes /1 and /2 */
        /* Print foo alignment in result so that we know that no valid alignment is found */
        /* Include read length to assist in readShuffle downstream */
        print_aln(outBuffer, NULL, NULL, read, 'F', 1, _output_format);
        DBG2("No seed found for %s\n", read->name);
    } else {
        Buffer alignmentsBuffer = initBuffer(sizeof(Alignment *) * 16);
        struct rb_iter *iter = rb_iter_create();
        if (iter) {
            // first print the best alignment
            AlignmentResult bestAlignment = {.contigAlignment = NULL, .alignment = NULL};
            char bestAlignmentKind = 'F';
            int countAlignments = 0;
            ContigAlignment contigAlignment = NULL;
            for (contigAlignment = rb_iter_last(iter, read_alignments); contigAlignment; contigAlignment = rb_iter_prev(iter)) {
                for (int i = 0; i < contigAlignment->nAlignments; i++) {
                    Alignment *alignment = contigAlignment->alignments + i;
                    if (bestAlignment.alignment == NULL || bestAlignment.alignment->align->score1 < alignment->align->score1) {
                        bestAlignment.contigAlignment = contigAlignment;
                        bestAlignment.alignment = alignment;
                        bestAlignmentKind = '1';
                    }
                    AlignmentResult tmp = {.contigAlignment = contigAlignment, .alignment = alignment};
                    *((AlignmentResult*) appendBuffer(alignmentsBuffer, sizeof(AlignmentResult))) = tmp;
                    countAlignments++;
                }
            }
            assert(numAlignments == countAlignments);
            if (bestAlignment.alignment != NULL) {
                if (bestAlignment.alignment->align->score1 == read->readLength * read->ssw_scores->match && numAlignments == 1) {
                    // perfect full length match
                    bestAlignmentKind = '0';
                }
            }

            if (_output_format == BLAST) {
                // sort alignments by read position
                assert(getLengthBuffer(alignmentsBuffer) == numAlignments * sizeof(AlignmentResult));
                AlignmentResult *buf = (AlignmentResult*) getStartBuffer(alignmentsBuffer);
                qsort(buf, numAlignments, sizeof(AlignmentResult), alignment_by_read_pos);
                for(int i = 0; i < numAlignments; i++) {
                    int isBest = bestAlignment.alignment == buf[i].alignment;
                    char kind = isBest ? bestAlignmentKind : '2';
                    print_aln(outBuffer, buf[i].contigAlignment, buf[i].alignment, read, kind, isBest, _output_format);
                }
            } else {
                // just output the best scoring alignment
                print_aln(outBuffer, bestAlignment.contigAlignment, bestAlignment.alignment, read, bestAlignmentKind, 1, _output_format);
            }
            rb_iter_dealloc(iter);
        } else {
            DIE("Could not allocate an iterator\n");
        }
        freeBuffer(alignmentsBuffer);
    }
}

// TODO Optimize this routine kmers are stored in two-bit form, there is a lot of redundant fasta-kmer -> two bit translations and revcomp calculations
//      generate two-bit forward & reverse kmers, and use them instead of all these fasta strings

int read_map(GZIP_FILE resultFd,
             HASH_TABLE_T *hashtable,
             Buffer cur_read_buf,
             Buffer read_info_buf,
             Buffer quals,
             contig_cache_t contigCache,
             int min_contig_length,
             int8_t *mat,
             FILE *logFD,
             int chunk_size,
             kmer_cache_t kmerCache,
             int kmer_len,
             int SEED_SPACE,
             ssw_scores_t *ssw_scores,
             int min_score,
             int ref_is_masked,
             int _output_format)
{
    int64_t front_contig_id, back_contig_id, new_front_contig_id, prev_front_contig_id;
    int front_ptr, prev_front_strand, back_ptr, matched_pos_cont, matched_pos_read, commun_opt_flag, m, k;
    int lex_ind, contig_length, found_flag, bypass_opt, strand, contig_fw, has_been_flipped = OFF;
    int forward_search, interest_zone;
    int posInContig, posInContigPrimer, effective_length;
    int offsetssw;
    int withCigar = _output_format == SAM;

    shared[] LIST_T * lookup_res;
    LIST_T seedEntry;
    memset(&seedEntry, 0, sizeof(LIST_T)); //initialize bytes
    int foundSeed;

    contig_t local_contig;
    const char *kmer_to_search;
    const char *plus_strand = "Plus", *minus_strand = "Minus";
    const char *print_strand;

    const char *cur_kmer, *rc_cur_kmer;
    assert(kmer_len < MAX_KMER_SIZE);

    char *search_ptr;
    int contigs_matched = 0, min_len;
    LIST_T cached_local_kmer;

    // quals is not used, so repurpose for contig_copy
    Buffer contig_copy_buf = quals;
    resetBuffer(contig_copy_buf);
    char *contig_copy = NULL;

    double start_timer, end_timer;

    struct rb_tree *read_alignments = rb_tree_create(rb_tree_cmp_alignment);

    ReadData read = initReadData(getStartBuffer(read_info_buf), getStartBuffer(cur_read_buf), getStartBuffer(quals), getLengthBuffer(cur_read_buf), mat, ssw_scores);
    int read_length = read->readLength;
    if (read_length > 500) withCigar = 1; // High potential for indels, so use cigar to mark matching kmers to UU contigs
    DBG2("Created new rbtree: %p for read %s\n", read_alignments, read->name);
    DBG3("seq=%.*s\n", read->readLength, read->seq);

    int numAlignments = 0;
    Alignment *alignment = NULL;

    /* Lookup for the first "valid" seed in the read */

    int numKmers = read_length - kmer_len + 1;
    char *trackTestPos = numKmers > 0 ? calloc_chk0(numKmers, 1) : NULL;
    int foundOrRetry = 0;
    int lookup_round = 0;
    int start_read_pos = 0;
    while ( start_read_pos + kmer_len <= read_length || foundOrRetry == 0) {
        if ( start_read_pos + kmer_len > read_length ) {
            if (numAlignments == 0 && SEED_SPACE > 1) {
                // retry testing every other kmer seed
                DBG2("Retrying with SEED_SPACE 1 (formerly %d)\n", SEED_SPACE);
                SEED_SPACE = 1;
                start_read_pos = 1; // 0 was already attempted
                foundOrRetry = 1;
                continue;
            } else {
                break;
            }
        }

        lookup_round++;
        front_ptr = start_read_pos;
        lookup_res = NULL;
        foundSeed = 0;
        cur_kmer = NULL;
        rc_cur_kmer = NULL;
        DBG3("Looking for new seed start_read_pos=%d\n", start_read_pos, front_ptr);

        // find the next kmer-seed hit to this read
        while ((lookup_res == NULL) && (front_ptr + kmer_len <= read_length)) {
            if (trackTestPos[front_ptr]++) {
                DBG2("Skipping already attempted seed at front_ptr=%d\n", front_ptr);
                front_ptr += SEED_SPACE;
                continue;
            }
            DBG2("Looking for seed at front_ptr=%d\n", front_ptr);
            cur_kmer = read->seq + front_ptr;
            rc_cur_kmer = read->rc_seq + (read->readLength - front_ptr - kmer_len);
            const char *tmp = NULL;
            if ( (tmp = (const char*) memchr(cur_kmer, 'N', kmer_len)) != NULL) {
                DBG2("Found N, adding %d\n", (tmp - cur_kmer) + 1);
                front_ptr += (tmp - cur_kmer) + 1;
                continue; // skip kmers with an N
            }

            lex_ind = strncmp(cur_kmer, rc_cur_kmer, kmer_len);
            kmer_to_search = (lex_ind < 0) ? cur_kmer : rc_cur_kmer;
#ifdef PROFILE
            start_timer = UPC_TICKS_NOW();
#endif

#ifdef USE_SWCACHE
            if (kmerCache != NULL) {
                assert(kmerCache->cacheCapacity != 0);
                lookup_res = lookupKmerInCache(hashtable, kmerCache, (unsigned char *)kmer_to_search, kmer_len, &cached_local_kmer);
            } else {
#endif
                lookup_res = lookup_kmer_MA(hashtable, (unsigned char *)kmer_to_search, kmer_len, &cached_local_kmer);
#ifdef USE_SWCACHE
           } 
#endif

#ifdef PROFILE
            end_timer = UPC_TICKS_NOW();
            hash_table_lookups += (UPC_TICKS_TO_SECS(end_timer - start_timer));
#endif
            if (lookup_res == NULL) {
                /* The k-mer is not a valid seed */
                //front_ptr += ( (front_ptr - start_read_pos >= SEED_SPACE) | (front_ptr + kmer_len >= read_length - SEED_SPACE - 1) ? SEED_SPACE : 1); // short jumps at edge
                front_ptr += SEED_SPACE;
            } else {
                /* The k-mer has been added to a contig */
                //seedEntry = (LIST_T) *lookup_res;
                seedEntry = cached_local_kmer;
                foundSeed = 1;
                if (seedEntry.firstContig.my_contig == NULL) {
                    DIE("seedEntry.firstContig.my_contig is null! kmer='%.*s' on thread %d nExtraContigs=%d real: %s\n", kmer_len, kmer_to_search, upc_threadof(lookup_res), seedEntry.nExtraContigs, lookup_res->firstContig.my_contig == NULL ? "indeed is null" : "is NOT NULL"); // TODO this happens!
                }

#ifdef ONLY_UNIQUE_SEEDS
                if (seedEntry.nExtraContigs > 0) {
                    lookup_res = NULL;
                    //front_ptr += ( (front_ptr - start_read_pos >= SEED_SPACE) | (front_ptr + kmer_len >= read_length - SEED_SPACE - 1) ? SEED_SPACE : 1); // short jumps at edge
                    front_ptr += SEED_SPACE;
                    foundSeed = 0;
                }
#endif
            }
        }
        // prep for next lookup round
        //start_read_pos = front_ptr + ( (front_ptr - start_read_pos >= SEED_SPACE) | (read_length - front_ptr - kmer_len <= SEED_SPACE) ? SEED_SPACE : 1); // short jumps at edge
        start_read_pos = front_ptr + SEED_SPACE;

        if (lookup_res == NULL) {
            // no more seeds have been found
            assert(front_ptr + kmer_len > read_length);
            continue; // allow a restart
        }
        DBG2("Found seed start_read_pos=%d front_ptr=%d\n", start_read_pos, front_ptr);

        /* Use SSW to align as much as we can from this read (align from start of the read to allow misalignments at the beginning)*/

        AlignmentResult alignmentResult = alignReadToContig(read_alignments, read, front_ptr, kmer_len, seedEntry.firstContig, contigCache, ref_is_masked, withCigar);
        if (alignmentResult.alignment) numAlignments++;

        // skip many seeds if this the only alignment and it was to a UU_CONTIG
        if (alignmentResult.alignment 
            && seedEntry.nExtraContigs == 0
            && alignmentResult.contigAlignment->local_contig.uuType == UU_CONTIG
           ) {

            // determine all the matched kmers in this alignment to this UU_CONTIG and mark them tested in the read

            int alignLen = alignmentResult.alignment->align->read_end1 - alignmentResult.alignment->align->read_begin1 + 1;
            const char *contig_start = alignmentResult.contigAlignment->seq + alignmentResult.alignment->align->ref_begin1;
            int countMarkedKmers = 0, countUnmarked = 0;

            // FIXME - This can be more efficient than alignLen * memcmps...
            // FIXME -- use the CIGAR to find matching regions to check for identical sequence - and support indels

            if (alignmentResult.alignment->isRC) {
              int fromEnd = read->readLength - alignmentResult.alignment->align->read_end1 - 1;
              const char * read_start = read->rc_seq + fromEnd;
              if (withCigar) {
                int readPos = 0, refPos = 0;
                for (int op_i = 0; op_i < alignmentResult.alignment->align->cigarLen; op_i++) {
                    char opchr = bam_cigar_opchr( alignmentResult.alignment->align->cigar[op_i] );
                    int oplen = bam_cigar_oplen( alignmentResult.alignment->align->cigar[op_i] );
                    DBG2("op_i=%d opchr=%c oplen=%d\n", op_i, opchr, oplen);
                    if ( opchr == 'M' || opchr == '=') {
                        int matches = 0;
                        for (int i = 0; i < oplen; i++) {
                            if (read_start[readPos + i] == contig_start[refPos + i]) {
                                matches++;
                                DBG3("   MATCH op_i=%d opchr=%c oplen=%d readPos=%d refPos=%d i=%d '%c' '%c' matches=%d\n", op_i, opchr, oplen, readPos, refPos, i, read_start[readPos + i], contig_start[refPos + i], matches);
                            } else {
                                DBG3("mismatch op_i=%d opchr=%c oplen=%d readPos=%d refPos=%d i=%d '%c' '%c' matches=%d\n", op_i, opchr, oplen, readPos, refPos, i, read_start[readPos + i], contig_start[refPos + i], matches);
                                matches = 0;
                            }
                            if (matches >= kmer_len) {
                                // exact match of this kemr
                                int trackpos = read->readLength - fromEnd - readPos - i - 1;
                                DBG3("trackpos=%d fromEnd=%d readPos=%d i=%d\n", trackpos, fromEnd, readPos, i);
                                assert(trackpos >= 0);
                                assert(trackpos < read->readLength - kmer_len + 1);
                                if (trackTestPos[trackpos] == 0) countUnmarked++;
                                trackTestPos[trackpos] = 1;
                                countMarkedKmers++;
                            }
                        }
                    }
                    int op = bam_cigar_op( alignmentResult.alignment->align->cigar[op_i] );
                    if ( (bam_cigar_type( op ) & 0x1) && opchr != 'S' ) readPos += oplen;
                    if ( bam_cigar_type( op ) & 0x2 ) refPos += oplen;
                }
              } else {
                // no cigar, so assume simple alignment (will not be effective for indels or frame shifts..)
                for(int i = 0; i < alignLen - kmer_len + 1; i++) {
                    if (memcmp( read_start + i, contig_start + i, kmer_len) == 0) {
                        int trackpos = read->readLength - fromEnd - kmer_len - i;
                        assert(trackpos >= 0);
                        assert(trackpos < read->readLength - kmer_len + 1);
                        if (trackTestPos[trackpos] == 0) countUnmarked++;
                        trackTestPos[trackpos] = 1;
                        countMarkedKmers++;
                    }
                }
              }
            } else {
              int fromStart = alignmentResult.alignment->align->read_begin1;
              const char *read_start = read->seq + fromStart;
              if (withCigar) {
                int readPos = 0, refPos = 0;
                for (int op_i = 0; op_i < alignmentResult.alignment->align->cigarLen; op_i++) {
                    char opchr = bam_cigar_opchr( alignmentResult.alignment->align->cigar[op_i] );
                    int oplen = bam_cigar_oplen( alignmentResult.alignment->align->cigar[op_i] );
                    DBG2("op_i=%d opchr=%c oplen=%d\n", op_i, opchr, oplen);
                    if ( opchr == 'M' || opchr == '=') {
                        int matches = 0;
                        for (int i = 0; i < oplen; i++) {
                            if (read_start[readPos + i] == contig_start[refPos + i]) {
                                matches++;
                                DBG3("   MATCH op_i=%d opchr=%c oplen=%d readPos=%d refPos=%d i=%d '%c' '%c' matches=%d\n", op_i, opchr, oplen, readPos, refPos, i, read_start[readPos + i], contig_start[refPos + i], matches);
                            } else {
                                DBG3("mismatch op_i=%d opchr=%c oplen=%d readPos=%d refPos=%d i=%d '%c' '%c' matches=%d\n", op_i, opchr, oplen, readPos, refPos, i, read_start[readPos + i], contig_start[refPos + i], matches);
                                matches = 0;
                            }
                            if (matches >= kmer_len) {
                                // exact match of this kemr
                                int trackpos = fromStart + readPos + i - kmer_len + 1;
                                DBG3("trackpos=%d fromStart=%d readPos=%d i=%d\n", trackpos, fromStart, readPos, i);
                                assert(trackpos >= 0);
                                assert(trackpos < read->readLength - kmer_len + 1);
                                if (trackTestPos[trackpos] == 0) countUnmarked++;
                                trackTestPos[trackpos] = 1;
                                countMarkedKmers++;
                            }
                        }
                    }
                    int op = bam_cigar_op( alignmentResult.alignment->align->cigar[op_i] );
                    if ( (bam_cigar_type( op ) & 0x1) && opchr != 'S' ) readPos += oplen;
                    if ( bam_cigar_type( op ) & 0x2 ) refPos += oplen;
                }
              } else {
                // no cigar, so assume simple alignment (will not be effective for indels or frame shifts..)
                for (int i = 0; i < alignLen - kmer_len + 1; i++) {
                    if (memcmp(read_start + i, contig_start + i, kmer_len) == 0) {
                        int trackpos = i+fromStart;
                        assert(trackpos >= 0);
                        assert(trackpos < read->readLength - kmer_len + 1);
                        if (trackTestPos[trackpos] == 0) countUnmarked++;
                        trackTestPos[trackpos] = 1;
                        countMarkedKmers++;
                    }
                }
              }
            }

            DBG2("Skipping an extra %d (%d matched) UU kmers (front_ptr=%d) alignLen=%d checkedKmers=%d\n", countUnmarked, countMarkedKmers, front_ptr, alignLen, alignLen - kmer_len + 1);
        }

        /* If we have multiple contigs that this seed is extracted from, find alignments with those as well */
        if (seedEntry.nExtraContigs > 0) {

            contigInfo * localContigInfoArray = (contigInfo *)malloc_chk0((seedEntry.nExtraContigs) * sizeof(contigInfo));
            assert(seedEntry.contigInfoArrayPtr != NULL);
            DBG3("Thread %d: getting %lld contigInfoArrayPointers for %s from %d\n", MYTHREAD, (lld)seedEntry.nExtraContigs, cur_kmer, (int)upc_threadof(seedEntry.contigInfoArrayPtr));
            DBG2("Checking %d extra seeds for this kmer\n", seedEntry.nExtraContigs);
            assert(IS_VALID_UPC_PTR(seedEntry.contigInfoArrayPtr));
#ifdef PROFILE
            start_timer = UPC_TICKS_NOW();
#endif
            upc_memget(localContigInfoArray, seedEntry.contigInfoArrayPtr, (seedEntry.nExtraContigs) * sizeof(contigInfo));
#ifdef PROFILE
            fetch_extra_contigs_time += (UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - start_timer));
#endif
            // Try aligning up to 10 more contigs from this redundant seed
            for (int z = 0; z < seedEntry.nExtraContigs; z++) {
                alignmentResult = alignReadToContig(read_alignments, read, front_ptr, kmer_len, localContigInfoArray[z], contigCache, ref_is_masked, withCigar);
                if (alignmentResult.alignment) numAlignments++;
            }
            free_chk0(localContigInfoArray);
        }
    }

    //
    // output the alignments
    //

    Buffer outBuffer = initBuffer((_output_format == SAM ? read_length * 3 * numAlignments : 80 * numAlignments) + 256);
    print_alignments(outBuffer, numAlignments, read_alignments, read, _output_format);
    GZIP_FWRITE(getStartBuffer(outBuffer), 1, getLengthBuffer(outBuffer), resultFd);
    freeBuffer(outBuffer);
    freeReadData(read);
    if (trackTestPos) free_chk0(trackTestPos);
    rb_tree_dealloc(read_alignments, rb_tree_free_alignment);
    return numAlignments;
}

