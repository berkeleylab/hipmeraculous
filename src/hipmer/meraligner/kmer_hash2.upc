#include "upc_common.h"
#include "common.h"
#include "StaticVars.h"

#include "merAligner.h"
#include "kmer_hash2.h"
#include "kmer_hash_generic.upc"

#include "../contigs/packingDNAseq.h"

/* Creates and initializes a distributed hastable - collective function */
HASH_TABLE_T *create_hash_table_MA_exact(int64_t size, int64_t my_exact_size,  MEMORY_HEAP_T *memory_heap)
{
    HASH_TABLE_T *result = CREATE_HASH_TABLE(size, memory_heap, my_exact_size);

    UPC_ALL_ALLOC_CHK(memory_heap->contigInfoArrays, THREADS, sizeof(contigInfoArrayPtr_t));
    UPC_ALL_ALLOC_CHK(memory_heap->heap_ptr_sizes, THREADS, sizeof(int64_t));
    int64_t expanded_size = my_exact_size * sizeof(LIST_T);
    assert(expanded_size);
    memory_heap->contigInfoArrays[MYTHREAD] = NULL;
    memory_heap->heap_ptr_sizes[MYTHREAD] = my_exact_size;
    initHeapList(memory_heap->contigs, sizeof(contig_t) * 5000); // block allocate 5k at a time
    initHeapList(memory_heap->contig_content, ONE_MB);           // block allocate 1MB at a time
    LOGF("Allocated heap of %0.3f MB for %lld kmer entries\n", 1.0 / ONE_MB * expanded_size, (lld)expanded_size / sizeof(LIST_T));
    upc_barrier;

    return result;
}

void destroy_hash_table_MA(HASH_TABLE_T **_ht, MEMORY_HEAP_T *memory_heap)
{
    if (_ht == NULL) {
        DIE("Invalid call to destroy_hash_table!\n");
    }
    assert(memory_heap);
    assert(memory_heap->heap_struct[MYTHREAD].len <= memory_heap->heap_ptr_sizes[MYTHREAD]);
    freeHeapList(memory_heap->contigs);
    freeHeapList(memory_heap->contig_content);
    if (memory_heap->contigInfoArrays[MYTHREAD]) {
        UPC_FREE_CHK(memory_heap->contigInfoArrays[MYTHREAD]);
    }
    UPC_ALL_FREE_CHK(memory_heap->contigInfoArrays);
    UPC_ALL_FREE_CHK(memory_heap->heap_ptr_sizes);

    DESTROY_HASH_TABLE(_ht, memory_heap);
}

/* Computes the hash value of a k-mer key */
/*
 * int64_t  hash(int64_t  hashtable_size, char *kmer)
 * {
 *  unsigned long hashval;
 *  hashval = 5381;
 *  for(; *kmer != '\0'; kmer++) hashval = (*kmer) +  (hashval << 5) + hashval;
 *
 *  return hashval % hashtable_size;
 * }
 */

shared[] LIST_T *lookup_kmer_in_bucket_MA(shared[] BUCKET_T *bucket, const unsigned char *packed_key, int kmer_len, LIST_T *cached_kmer)
{
    assert(bucket != NULL);
    assert(packed_key != NULL);
    LIST_T local_res;
    memset(&local_res, 0, sizeof(LIST_T)); // initialize
    int kmer_packed_len = GET_KMER_PACKED_LEN(kmer_len);

    DBG3("Lookup in bucket\n");
    BUCKET_T cachedBucket = *bucket;
    shared[] LIST_T * result = cachedBucket.head;
    // optimization to avoid a remote lookup by using the bucket as a cache
    if (result != NULL) {
        if (cachedBucket.cachedEntryPtr == NULL) {
            assert(0); // this should have happened while building the seed index
            DBG2("Setting cachedEntry\n");
            // populate this bucket's cachedEntry
            cachedBucket.cachedEntryPtr = result;
            cachedBucket.cachedEntry = *result; // remote get

            bucket->cachedEntry = cachedBucket.cachedEntry; // remote put
            upc_fence;
            bucket->cachedEntryPtr = result; // remote put
        }

        if (result == cachedBucket.cachedEntryPtr) {
            DBG3("Found cachedEntry\n");
            local_res = cachedBucket.cachedEntry;
            if (comparePackedSeq(packed_key, local_res.packed_key, kmer_packed_len) == 0) {
                DBG3("Used cachedEntry\n");
                (*cached_kmer) = local_res;
                return result;
            }
            // fall through
            result = local_res.next;
        }
    }


    while (result != NULL) {
        local_res = *result;
        if (comparePackedSeq(packed_key, local_res.packed_key, kmer_packed_len) == 0) {
            (*cached_kmer) = local_res;
            return result;
        }
        result = local_res.next;
    }
    return result;
}


/* Use this lookup function when no writes take place in the distributed hashtable */
shared[] LIST_T *lookup_kmer_MA(HASH_TABLE_T *hashtable, const unsigned char *kmer, int kmer_len, LIST_T *cached_kmer)
{
    shared[] LIST_T * result_ptr;
    assert(hashtable != NULL);
    assert(hashtable->size > 0);
    assert(hashtable->table != NULL);
    assert(kmer != NULL);
    int64_t hashval;

    hashval = hashkey(hashtable->size, (char *)kmer, kmer_len);
    CHECK_BOUNDS(hashval, hashtable->size);

    unsigned char packed_key[MAX_KMER_PACKED_LENGTH];
    memset(packed_key, 0, MAX_KMER_PACKED_LENGTH); // initialize all bytes

    packSequence(kmer, packed_key, kmer_len);

    shared[] BUCKET_T * bucket = (shared[] BUCKET_T *) & (hashtable->table[hashval]);
    assert(bucket != NULL);
    DBG3("Looking up hashval=%lld\n", hashval);
    result_ptr = lookup_kmer_in_bucket_MA(bucket, packed_key, kmer_len, cached_kmer);
    return result_ptr;
}

// returns the entry from the bucket that was either already present or this new one
shared[] LIST_T *add_entry_to_bucket_MA(shared[] BUCKET_T *bucket, shared[] LIST_T *new_entry)
{
    assert(upc_threadof(new_entry) == MYTHREAD);
    assert(upc_threadof(bucket) == MYTHREAD);
    new_entry->next = bucket->head;
    bucket->head = new_entry;
    return new_entry;
}
