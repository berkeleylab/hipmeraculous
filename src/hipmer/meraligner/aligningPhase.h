#ifndef __ALIGNING_PHASE_H
#define __ALIGNING_PHASE_H

#define PLUS 0
#define MINUS 1
#define ON 1
#define OFF 0
#define SUCCESS 1
#define FAIL 0

#define MAX_ALIGNMENT_LINE_SIZE 200L
#define ALIGN_EXPAND_FRACTION 0.075
#define ALIGN_EXPAND_BASES 3

#include <assert.h>
#include <upc.h>
#include <upc_collective.h>
#include <zlib.h>
#include <ctype.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"


#include "bam_cigar.h"
#include "ssw.h"
#include "cache_infrastructure.h"
#include "kmer_hash2.h"
#include "../fqreader/fq_reader.h"
#include "Buffer.h"
#include "utils.h"
#include "timers.h"
#include "upc_output.h"
#include "rb_tree.h"

#ifndef DEFAULT_READ_LEN
#define DEFAULT_READ_LEN 1024
#endif

#define BLAST 0
#define SAM 1

//#define ONLY_UNIQUE_SEEDS

#ifdef PROFILE
extern double rev_time;
extern double readmap_time;
extern double fetch_contigs_time;
extern double hash_table_lookups;
extern double contig_metadata_lookup_time;
extern double fetch_extra_contigs_time;
extern double exact_alignment_time;
extern double inexact_alignment_time;
extern double sw_alignment_time;
extern double input_IO;
extern double output_IO;
extern int64_t seed_lookup;
#endif

extern int8_t nt_table[128];

typedef struct {
   int match, mismatch, gap_open, gap_extension, ambiguity;
} ssw_scores_t;


int64_t parallelAligner(HASH_TABLE_T *hashtable,
                        char *filename,
                        GZIP_FILE res_fd1,
                        GZIP_FILE res_fd2,
                        int filesPerPair,
                        int readNumber,
                        double estimatedInsertSize,
                        double estimatedSigma,
                        int64_t cacheSizeInMB,
                        int min_contig_length,
                        int chunk_size,
                        kmer_cache_t kmerCache,
                        contig_cache_t contigCache,
                        int64_t *readsMapped,
                        int64_t *readsProcessed,
                        const char *base_dir,
                        const int reads_are_per_thread,
                        const char *libName,
                        int kmer_len,
                        uint8_t libnum,
                        int SEED_SPACE,
                        ssw_scores_t *ssw_scores,
                        int min_score,
                        uint64_t *read1ID,
                        uint64_t *read2ID,
                        int ref_is_masked,
                        int is_hexified,
                        int _output_format);


// TODO Optimize this routine kmers are stored in two-bit form, there is a lot of redundant fasta-kmer -> two bit translations and revcomp calculations
//      generate two-bit forward & reverse kmers, and use them instead of all these fasta strings
int read_map(GZIP_FILE resultFd,
             HASH_TABLE_T *hashtable,
             Buffer cur_read_buf,
             Buffer read_info_buf,
             Buffer quals,
             contig_cache_t contigCache,
             int min_contig_length,
             int8_t *mat,
             FILE *logFD,
             int chunk_size,
             kmer_cache_t kmerCache,
             int kmer_len,
             int SEED_SPACE,
             ssw_scores_t *ssw_scores,
             int min_score,
             int ref_is_masked,
             int _output_format);

typedef struct {
    const char *name;
    const char *seq;
    const char *quals;
    int readLength;
    s_profile *profile, *rc_profile;
    int8_t *_data;
    const int8_t *mat;
    const ssw_scores_t *ssw_scores;
    // the following are just offsets from the allocated _data
    char *rc_seq;
    int8_t *read_num, *rc_read_num;
    char *r_quals;
} _ReadData;
typedef _ReadData * ReadData;

ReadData initReadData(const char *name, const char *seq, const char *quals, int readLength, const int8_t *mat, const ssw_scores_t *ssw_scores);
void freeReadData(ReadData d);

// initialize ssw parameters only when needed and just in time
int8_t * _prepReadSSW(int8_t *read_num, const char *read_seq, int read_length, const int8_t *mat, s_profile **profile);
void prepReadSSW(ReadData d, int isRC);
void prepReverseQuals(ReadData d);

typedef struct {
    s_align *align;
    uint32_t mismatches;
    int8_t isRC;
} Alignment;

typedef struct {
    shared [] contig_t * contigPtr;
    contig_t local_contig;
    char * seq;
    int nAlignments, bestAlignment;
    Alignment *alignments;
} _ContigAlignment;
typedef _ContigAlignment * ContigAlignment;

typedef struct {
    ContigAlignment contigAlignment;
    Alignment *alignment;
} _AlignmentResult;
typedef _AlignmentResult AlignmentResult;

int alignment_by_read_pos(const void *a, const void *b);

int rb_tree_cmp_alignment(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b);

void freeContigAlignment(ContigAlignment align);

void rb_tree_free_alignment(struct rb_tree *self, struct rb_node *node);

// using a kmer seed on a read and contig, with known positions either:
// 1) find an existing alignment that covers that kmer seed
// or
// 2) disvover a new exact full length alignment of the read
// or
// 3) compute a ssw around that seed
// returns the number of read bases that were aligned
AlignmentResult alignReadToContig(struct rb_tree *tree, ReadData read, int readPos, int kmer_len, contigInfo contig_info, contig_cache_t contigCache, int ref_is_masked, int withCigar);
void print_aln(Buffer b, ContigAlignment contigAlignment, Alignment *alignment, ReadData read, char mer_i, int isBest, int _output_format);
void print_alignments(Buffer outBuffer, int numAlignments, struct rb_tree *read_alignments, ReadData read, int _output_format);

#endif
