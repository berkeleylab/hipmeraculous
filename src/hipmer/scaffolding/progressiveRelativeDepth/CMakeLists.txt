MESSAGE("Building progressiveRelativeDepth ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

SET_UPC_PROPS(*.upc)

FOREACH(HIPMER_KMER_LEN ${HIPMER_KMER_LENGTHS})
  add_library(progressiveRelativeDepth_objs-${HIPMER_KMER_LEN} OBJECT progressiveRelativeDepth_main.upc progressiveRelativeDepthUtils.upc )
  add_kmer_defs_to_lib(progressiveRelativeDepth_objs-${HIPMER_KMER_LEN})

  add_library(KmerHashPRD-${HIPMER_KMER_LEN} OBJECT kmer_hash_prd.upc)
  add_kmer_defs_to_lib(KmerHashPRD-${HIPMER_KMER_LEN})

  add_library(BuildUFXPRD-${HIPMER_KMER_LEN} OBJECT buildUFXhashBinary_prd.upc)
  add_kmer_defs_to_lib(BuildUFXPRD-${HIPMER_KMER_LEN})

  IF (HIPMER_FULL_BUILD)
    ADD_EXECUTABLE(progressiveRelativeDepth-${HIPMER_KMER_LEN} progressiveRelativeDepth.upc 
      $<TARGET_OBJECTS:progressiveRelativeDepth_objs-${HIPMER_KMER_LEN}>
      #$<TARGET_OBJECTS:KmerObjects-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:readufx-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:rc_intrinsics> 
      $<TARGET_OBJECTS:PackingDNASeq-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:KmerHandling-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:KmerHashPRD-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:BuildUFXPRD-${HIPMER_KMER_LEN}>
      $<TARGET_OBJECTS:HASH_FUNCS>
      $<TARGET_OBJECTS:upc_common>
      $<TARGET_OBJECTS:COMMON>
      $<TARGET_OBJECTS:HIPMER_VERSION>
      $<TARGET_OBJECTS:Buffer>
      $<TARGET_OBJECTS:MemoryChk>
      $<TARGET_OBJECTS:OptList>
      $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
    )
    SET_TARGET_PROPERTIES(progressiveRelativeDepth-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    TARGET_LINK_LIBRARIES(progressiveRelativeDepth-${HIPMER_KMER_LEN} ${ZLIB_LIBRARIES} ${RT_LIBRARIES})
    INSTALL(TARGETS progressiveRelativeDepth-${HIPMER_KMER_LEN} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/)
    add_kmer_defs(progressiveRelativeDepth-${HIPMER_KMER_LEN})
  ENDIF()

ENDFOREACH()

