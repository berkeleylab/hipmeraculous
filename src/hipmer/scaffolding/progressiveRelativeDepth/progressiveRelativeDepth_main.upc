#include "progressiveRelativeDepth_main.h"

shared int64_t contig_id_pool;

shared char max_changes;

char *getContigBuffer(Buffer buf, contigNeighborhood_t curContig)
{
    assert(curContig.length > 0);
    char *received = resetRawBuffer(buf, curContig.length);
    upc_memget(received, curContig.seq, curContig.length * sizeof(char));
    assert(received[curContig.length] == '\0');
    assert(strlen(received) == curContig.length);
    return received;
}

int appendContigBuffer(Buffer buf, int posInBigContig, const char *received, contigNeighborhood_t curContig, int kmerLength)
{
    assert(posInBigContig >= 0);
    int appendLen = curContig.length;
    int appendOffset = 0;
    assert(appendLen > 0);
    assert(strlen(received) == appendLen);
    if (posInBigContig > 0) {
        // Do not print the overlapping K-2 bases
        char *fc = getStartBuffer(buf);
        assert(getSizeBuffer(buf) > posInBigContig);
        assert(fc[posInBigContig] == '\0');
        assert(strlen(fc) == posInBigContig);

        appendOffset = kmerLength - 2;
        appendLen -= appendOffset;
        assert(appendLen > 0);
        assert(appendLen < curContig.length);
    }
    char *finalContig = resetRawBuffer(buf, posInBigContig + appendLen);
    if (posInBigContig > 0) {
        assert(finalContig[posInBigContig] == '\0');
        assert(strlen(finalContig) == posInBigContig);   // after a potential realloc
    }
    memcpy(finalContig + posInBigContig, received + appendOffset, appendLen);
    assert(finalContig[posInBigContig + appendLen] == '\0');
    //LOG("Contig_%lld, appendOffset: %lld, appendLen: %lld, finalContig: %lld, posInBigContig: %lld, exp: %lld, received: %lld\n", (lld) curContig.myID, (lld) appendOffset, (lld) appendLen, (lld) strlen(finalContig), (lld) posInBigContig, (lld) posInBigContig + appendLen, (lld) strlen(received));
    assert(strlen(finalContig) == posInBigContig + appendLen);
    return appendLen;
}

int progressiveRelativeDepth_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

    if (_sv != NULL) {
        DBG("Zeroing timers\n");
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:k:s:d:B:N:C:a:t:b:D:M:PX");
    print_args(optList, __func__);

    char *input_UFX_name, *contig_file_name;
    int kmerLength = 0; // must be set
    int64_t totalContigs, contigID, nContigs;
    int dmin, chunk_size = 1, contigLen, includePrefixMerDepth = 0;

    const char *base_dir = ".";
    char *contigReportFilePrefix;
    double tau, beta, alpha;
    int64_t mycontigs = 0;
    double dynamicMinDepth = 1.0;
    int merge_paths = 0;
    int is_per_thread = 0;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = kmerLength = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Invalid kmer length %d for this binary compiled with KMER_MAX_SIZE=%d\n", kmer_len, MAX_KMER_SIZE);
            }
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'a':
            alpha = atof(thisOpt->argument);
            break;
        case 't':
            tau = atof(thisOpt->argument);
            break;
        case 'b':
            beta = atof(thisOpt->argument);
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 's':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'D':
            dynamicMinDepth = atof(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'C':
            contigReportFilePrefix = thisOpt->argument;
            break;
        case 'N':
            totalContigs = atol(thisOpt->argument);
            break;
        case 'M':
            merge_paths = atoi(thisOpt->argument);
            break;
        case 'n':
            mycontigs = atol(thisOpt->argument);
            break;
        case 'P':
            includePrefixMerDepth = 1;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        default:
            break;
        }
    }

    //int *myContigIDs;
    int pos = 0;
    if (mycontigs == 0) {
        mycontigs = totalContigs;
    }
    //myContigIDs = (int*) malloc_chk(mycontigs*sizeof(int));

    HASH_TABLE_T *dist_hashtable;
    UPC_TICK_T start, end;

    if (kmerLength <= 0 || kmerLength > MAX_KMER_SIZE) {
        DIE("Invalid kmerLength (-k option) %d.  Must be <= %d\n", kmerLength, MAX_KMER_SIZE);
    }

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
    }
    double con_time, depth_time;

#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
        contig_id_pool = 0;
    }
    upc_barrier;
#endif

    char contigReportFileName[MAX_FILE_PATH];
    char line[MAX_LINE_SIZE];
    char *token, *aux;
    float fdepth;
    char *kmerBuf = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    kmerBuf[kmerLength] = '\0';

    /* PRINT FASTA LOGISTICS */
    int64_t total_written = 0, cur_length;
    int64_t towrite;
    char fastaSegment[SEGMENT_LENGTH];
    fastaSegment[SEGMENT_LENGTH - 1] = '\0';
    char *seqF;

    /* Read the contig report file */
    sprintf(contigReportFileName, "%s.txt" GZIP_EXT, contigReportFilePrefix);
    GZIP_FILE contigReportFD = openCheckpoint(contigReportFileName, "r");

    shared[1] contigNeighborhood_t * contigsInfo = NULL;
    UPC_ALL_ALLOC_CHK(contigsInfo, totalContigs, sizeof(contigNeighborhood_t));
    //for (int64_t i = MYTHREAD;  i < totalContigs; i += THREADS) {
    //   contigsInfo[i].seq = NULL;
    //}
    contigNeighborhood_t curContig, curContigLeft, curContigRight, examineLeft, examineRight;
    shared[1] float *contigDepths = NULL;
    UPC_ALL_ALLOC_CHK(contigDepths, totalContigs, sizeof(float));
    curContig.leftNeighborIDs[0] = INVALID;
    curContig.leftNeighborIDs[1] = INVALID;
    curContig.leftNeighborIDs[2] = INVALID;
    curContig.leftNeighborIDs[3] = INVALID;
    curContig.rightNeighborIDs[0] = INVALID;
    curContig.rightNeighborIDs[1] = INVALID;
    curContig.rightNeighborIDs[2] = INVALID;
    curContig.rightNeighborIDs[3] = INVALID;

    while (GZIP_GETS(line, MAX_LINE_SIZE, contigReportFD) != NULL) {
        token = strtok_r(line, "\t", &aux);
        assert(token != NULL);
        contigID = atol(token + 6);   // Since the string should be ContigXXXXXXXX
        if (contigID >= totalContigs) {
            DIE("too many contigs %lld in contigReport (expecting %lld): %s\n", (lld)contigID, (lld)totalContigs, line);
        }

        token = strtok_r(NULL, "\t", &aux);
        assert(token != NULL);
        contigLen = atoi(token);                // Extract contig's length
        contigLen = contigLen + kmerLength - 1; // Well, we store nKmers instead of the length in the merDepth file
        token = strtok_r(NULL, "\t", &aux);
        if (token != NULL) {
            fdepth = atof(token);    // Extract contig's depth
        }
        contigDepths[contigID] = fdepth;
    }
    closeCheckpoint(contigReportFD);

    if (MYTHREAD == 0) {
        printf("Done with reading contig depths\n");
    }

    /* Build hash table using UFX file that also contains the FX kmers */
    int64_t myshare;
    int dsize;
    int64_t size;
    MEMORY_HEAP_T memory_heap;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Refreshing local checkpoints of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        if (!doesLocalCheckpointExist(tmp)) {
            // force a restore from previous checkpoint, if needed
            restoreLocalCheckpoint(tmp);
            // same for the .entries file
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }   

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) {
        DIE("Could not load UFX: %s is_per_thread: %d\n", input_UFX_name, is_per_thread);
    }
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with total memory / nodes\n", (lld)minMemory, (lld)size);
    }
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, dynamicMinDepth, chunk_size, 1, kmer_len, UFX_f);


#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = UPC_TICKS_TO_SECS(end - start);
        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");
        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
        start = UPC_TICKS_NOW();
    }
#endif

    /* Read contigs and find mean depth for each one */
    char my_contig_file_name[MAX_FILE_PATH];
    char my_output_file_name[MAX_FILE_PATH];
    char my_depth_output_file_name[MAX_FILE_PATH];
    char my_prefix_depth_output_file_name[MAX_FILE_PATH];

    /* TODO use Buffer to allow large contigs */
    Buffer contigBuffer = initBuffer(MAX_CONTIG_SIZE);

    GZIP_FILE contigFile, myOutputFile, myDepthOutputFile, myPrefixDepthOutputFile = NULL;
    int is_least;
    char *firstKmer, *lastKmer, *prevKmer, *nextKmer;
    LIST_T copy;
    shared[] LIST_T * lookup_res = NULL;
    int64_t contigIdx;
    char prevBase, nextBase;
    double meanContigDepth = 0.0;
    int64_t nMers = 0;
    int64_t nMersInContigs = 0;
    int64_t *prefixDepthSumArray = NULL;

    firstKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    firstKmer[kmerLength] = '\0';
    lastKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    lastKmer[kmerLength] = '\0';
    prevKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    prevKmer[kmerLength] = '\0';
    nextKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    nextKmer[kmerLength] = '\0';

    sprintf(my_contig_file_name, "%s.fasta" GZIP_EXT, contig_file_name);

    sprintf(my_output_file_name, "pruned_%s.fasta" GZIP_EXT, contig_file_name);

    sprintf(my_depth_output_file_name, "merDepth_pruned_%s.txt" GZIP_EXT, contig_file_name);

    contigFile = openCheckpoint(my_contig_file_name, "r");

    myOutputFile = openCheckpoint(my_output_file_name, "w");
    myDepthOutputFile = openCheckpoint(my_depth_output_file_name, "w");

    if (includePrefixMerDepth) {
        sprintf(my_prefix_depth_output_file_name, "merDepth_pruned_%s.bin" GZIP_EXT, contig_file_name);
    }

    /* Store contig info in shared data structure */
    resetBuffer(contigBuffer);
    size_t contigsHeapLen = 0;
    while (gzgetsBuffer(contigBuffer, 8192, contigFile) != NULL) {
        /* Read a contig and its length */
        assert(getStartBuffer(contigBuffer)[0] == '>');
        contigID = atol(getStartBuffer(contigBuffer) + 8);
        resetBuffer(contigBuffer);
        if (!gzgetsBuffer(contigBuffer, 8192, contigFile)) {
            break;
        }
        chompBuffer(contigBuffer);
        char *buf = getStartBuffer(contigBuffer);
        contigLen = getLengthBuffer(contigBuffer);
        contigsHeapLen += contigLen + 1;
        nMersInContigs += contigLen - kmerLength + 1;
        assert(isACGT(buf[0]) && isACGT(buf[contigLen - 1]));
        //myContigIDs[pos] = (int) contigID;
        pos++;

        /* Extract the first and prev kmer of the contig */
        memcpy(firstKmer, &buf[0], kmerLength * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, firstKmer, &copy, &is_least, kmer_len);
        if (is_least) {
            prevBase = copy.left_ext;
        } else {
            prevBase = reverseComplementBaseExt(copy.right_ext);
        }
        lookup_res->contID = contigID;

        prevKmer[0] = prevBase;
        memcpy(&prevKmer[1], firstKmer, (kmerLength - 1) * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, prevKmer, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            //lookup_res->contID = contigID;
            memcpy(curContig.firstKmer, prevKmer, kmerLength * sizeof(char));
            if (is_least) {
                curContig.leftExt = copy.left_ext;
            } else {
                curContig.leftExt = reverseComplementBaseExt(copy.right_ext);
            }
        } else {
            memcpy(curContig.firstKmer, firstKmer, kmerLength * sizeof(char));
            curContig.leftExt = prevBase;
        }

        /* Extract the last and next kmer of the contig */
        memcpy(lastKmer, &buf[contigLen - kmerLength], kmerLength * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, lastKmer, &copy, &is_least, kmer_len);
        if (is_least) {
            nextBase = copy.right_ext;
        } else {
            nextBase = reverseComplementBaseExt(copy.left_ext);
        }
        lookup_res->contID = contigID;

        nextKmer[kmerLength - 1] = nextBase;
        memcpy(nextKmer, &lastKmer[1], (kmerLength - 1) * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, nextKmer, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            //lookup_res->contID = contigID;
            memcpy(curContig.lastKmer, nextKmer, kmerLength * sizeof(char));
            if (is_least) {
                curContig.rightExt = copy.right_ext;
            } else {
                curContig.rightExt = reverseComplementBaseExt(copy.left_ext);
            }
        } else {
            memcpy(curContig.lastKmer, lastKmer, kmerLength * sizeof(char));
            curContig.rightExt = nextBase;
        }

        curContig.length = contigLen;
        curContig.myID = contigID;
        curContig.depth = contigDepths[contigID];
        curContig.seq = NULL;

        contigsInfo[contigID] = curContig;

        resetBuffer(contigBuffer);
    }

    GZIP_REWIND(contigFile);


    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Done with storing info in the shared data structure about contigs\n");
    }

    int j;
    char myChanges, totalChanges;
    double neighborhoodDepth = 0.0;

    /* Allocate array where initially all contigs are valid */
    shared[1] int8_t *validContigs = NULL;
    UPC_ALL_ALLOC_CHK(validContigs, totalContigs, sizeof(int8_t));

    /* Find neighbors for all contigs */
    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        curContig = contigsInfo[contigIdx];
        lookup_and_assign_neighors(dist_hashtable, curContig.firstKmer, kmerLength, LEFT, curContig.leftNeighborIDs, kmerBuf, curContig.leftExt, curContig.myID, kmer_len);
        lookup_and_assign_neighors(dist_hashtable, curContig.lastKmer, kmerLength, RIGHT, curContig.rightNeighborIDs, kmerBuf, curContig.rightExt, curContig.myID, kmer_len);
        validContigs[contigIdx] = VALID;
        contigsInfo[contigIdx] = curContig;
    }

    free_chk(kmerBuf);
    free_chk(firstKmer);
    free_chk(lastKmer);
    free_chk(prevKmer);
    free_chk(nextKmer);

    UPC_LOGGED_BARRIER;
    serial_printf("Done with finding neighbors for the contigs\n");

    /* Keep only neighbors that are symmetric (our DB graph is undirected) */

    int64_t myNcontigs = (totalContigs + THREADS - 1) / THREADS;
    int64_t *local_neighborhoods = (int64_t *)malloc_chk(myNcontigs * 8 * sizeof(int64_t));
    int64_t posInLocal, l, found;

    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        posInLocal = contigIdx / THREADS;
        for (j = 0; j < 4; j++) {
            local_neighborhoods[posInLocal * 8 + j] = contigsInfo[contigIdx].leftNeighborIDs[j];
            local_neighborhoods[posInLocal * 8 + j + 4] = contigsInfo[contigIdx].rightNeighborIDs[j];
        }

        for (j = 0; j < 8; j++) {
            if (local_neighborhoods[posInLocal * 8 + j] != INVALID) {
                if (local_neighborhoods[posInLocal * 8 + j] >= totalContigs) {
                    DIE("Invalid local_neighborhoods[posInLocal=%lld * 8 + j=%d]=%lld\n", (lld)posInLocal, j, (lld)local_neighborhoods[posInLocal * 8 + j]);
                }
                curContig = contigsInfo[local_neighborhoods[posInLocal * 8 + j]];
                found = 0;
                if (curContig.leftNeighborIDs[0] == contigIdx) {
                    found = 1;
                } else if (curContig.leftNeighborIDs[1] == contigIdx) {
                    found = 1;
                } else if (curContig.leftNeighborIDs[2] == contigIdx) {
                    found = 1;
                } else if (curContig.leftNeighborIDs[3] == contigIdx) {
                    found = 1;
                } else if (curContig.rightNeighborIDs[0] == contigIdx) {
                    found = 1;
                } else if (curContig.rightNeighborIDs[1] == contigIdx) {
                    found = 1;
                } else if (curContig.rightNeighborIDs[2] == contigIdx) {
                    found = 1;
                } else if (curContig.rightNeighborIDs[3] == contigIdx) {
                    found = 1;
                }

                if (found == 0) {
                    local_neighborhoods[posInLocal * 8 + j] = INVALID;
                }
            }
        }
    }

    UPC_LOGGED_BARRIER;

    /* Update the contigsInfo data structure */
/*
    char my_output_graph_file_name[255];
    sprintf(my_output_graph_file_name, "pruned_%s-kmer-edges.fasta" GZIP_EXT, contig_file_name);
    GZIP_FILE myOutputGraphFile = openCheckpoint(my_output_graph_file_name, "w");
*/
    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        posInLocal = contigIdx / THREADS;
        for (j = 0; j < 4; j++) {
            contigsInfo[contigIdx].leftNeighborIDs[j] = local_neighborhoods[posInLocal * 8 + j];
            contigsInfo[contigIdx].rightNeighborIDs[j] = local_neighborhoods[posInLocal * 8 + j + 4];
        }
/*
        GZIP_PRINTF(myOutputGraphFile, "Contig %ld\nLeft ", contigIdx);
        for (j = 0; j < 4; j++) {
            if (contigsInfo[contigIdx].leftNeighborIDs[j] != INVALID) 
                GZIP_PRINTF(myOutputGraphFile, "%ld ", contigsInfo[contigIdx].leftNeighborIDs[j]);
        }
        GZIP_PRINTF(myOutputGraphFile, "\nRight ");
        for (j = 0; j < 4; j++) {
            if (contigsInfo[contigIdx].rightNeighborIDs[j] != INVALID) 
                GZIP_PRINTF(myOutputGraphFile, "%ld ", contigsInfo[contigIdx].rightNeighborIDs[j]);
        }
        GZIP_PRINTF(myOutputGraphFile, "\n");
*/
    }

//    closeCheckpoint(myOutputGraphFile);

    free_chk(local_neighborhoods);

    UPC_LOGGED_BARRIER;

    int nRightNeighbors, nLeftNeighbors, theNeighbor, posWalkLeft, posWalkRight, strand, nValidsRight, nValidsLeft, foundRight, foundLeft;
    /* FIXME: Might want to make these dynamic sized arrays */
    int leftWalkIDs[10000];
    int leftWalkStrands[10000];
    int rightWalkIDs[10000];
    int rightWalkStrands[10000];
    int posInBigContig, startContig, foundCycle;
    int64_t newId, minID, curID;

    int8_t validity;
    shared[1] char *sharedChanges = NULL;
    UPC_ALL_ALLOC_CHK(sharedChanges, THREADS, sizeof(char));
    int foundN;
    int64_t myValidContigs = 0;

    /* Now we will have mark "validity" & update iterations until we have convergence of the neighborhoods */
    int nFoundRight = 0, nFoundLeft = 0;
    int latestLeft = 0, latestRight = 0;
    int64_t loopCount = 0;
    double myStart = now();
    while (1) {
        loopCount++;
        myChanges = 0;

        /* Mark "validity" step */
        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            nFoundRight = 0;
            nFoundLeft = 0;

            if (validContigs[contigIdx] == INVALID) {
                continue;
            }

            curContig = contigsInfo[contigIdx];
            foundN = 0;
            neighborhoodDepth = 0.0;
            for (j = 0; j < 4; j++) {
                if (curContig.leftNeighborIDs[j] != INVALID) {
                    if (curContig.leftNeighborIDs[j] >= totalContigs || curContig.leftNeighborIDs[j] < 0) {
                        DIE("Invalid ourContig=%lld .leftNeighborIDs[%d]=%lld\n", (lld)contigIdx, j, (lld)curContig.leftNeighborIDs[j]);
                    }
                    foundN++;
                    nFoundLeft++;
                    neighborhoodDepth += contigDepths[curContig.leftNeighborIDs[j]];
                    latestLeft = curContig.leftNeighborIDs[j];
                }
            }
            for (j = 0; j < 4; j++) {
                if (curContig.rightNeighborIDs[j] != INVALID) {
                    if (curContig.rightNeighborIDs[j] >= totalContigs || curContig.rightNeighborIDs[j] < 0) {
                        DIE("Invalid ourContig=%lld .rightNeighborIDs[%d]=%lld\n", (lld)contigIdx, j, (lld)curContig.rightNeighborIDs[j]);
                    }
                    foundN++;
                    nFoundRight++;
                    neighborhoodDepth += contigDepths[curContig.rightNeighborIDs[j]];
                    latestRight = curContig.rightNeighborIDs[j];
                }
            }

            if (foundN != 0) {
                neighborhoodDepth = neighborhoodDepth / (1.0 * foundN);
                if ((curContig.length < 2 * kmerLength) && (curContig.depth < MIN(tau, beta * neighborhoodDepth))) {
                    validContigs[contigIdx] = INVALID;
                    myChanges = 1;
                }
            }

#ifdef HAIR_REMOVAL
            int hair_length = (2 * kmerLength < HAIR_MIN) ? 2 * kmerLength : HAIR_MIN;
            /* Added code to remove dead ends as well */
            if (((nFoundRight == 0) && (nFoundLeft == 1)) || ((nFoundLeft == 0) && (nFoundRight == 1))) {
                if ((curContig.length < hair_length) && (curContig.depth < 100.0)) {
                    validContigs[contigIdx] = INVALID;
                    myChanges = 1;
                }
            }
#endif

#ifdef COMPLEX_HAIR_REMOVAL
            int is_left, is_right;
            int max_length, current_length;
            double max_depth, current_depth;
            int hair_length = (2 * kmerLength < HAIR_MIN) ? 2 * kmerLength : HAIR_MIN;
            /* Added code to remove dead ends as well */
            if ((((nFoundRight == 0) && (nFoundLeft == 1)) || ((nFoundLeft == 0) && (nFoundRight == 1))) && (curContig.length < hair_length)) {
                max_depth = 0.0;
                max_length = 0;

                if (nFoundLeft == 1) {
                    is_left = 0;
                    is_right = 0;
                    examineLeft = contigsInfo[latestLeft];
                    for (int t = 0; t < 4; t++) {
                        if (examineLeft.leftNeighborIDs[t] == contigIdx) {
                            is_left = 1;
                        }
                        if (examineLeft.rightNeighborIDs[t] == contigIdx) {
                            is_right = 1;
                        }
                    }

                    if ((is_left + is_right) == 1) {
                        if (is_left) {
                            for (int t = 0; t < 4; t++) {
                                if ((examineLeft.leftNeighborIDs[t] != contigIdx) && (examineLeft.leftNeighborIDs[t] != INVALID)) {
                                    if (examineLeft.leftNeighborIDs[t] < 0 || examineLeft.leftNeighborIDs[t] >= totalContigs) {
                                        DIE("Invalid examineLeft left neighbor: t=%d %lld latestLeft=%lld\n", t, (lld)examineLeft.leftNeighborIDs[t], (lld)latestLeft);
                                    }
                                    current_depth = contigDepths[examineLeft.leftNeighborIDs[t]];
                                    current_length = contigsInfo[examineLeft.leftNeighborIDs[t]].length;
                                    if (current_depth > max_depth) {
                                        max_depth = current_depth;
                                    }
                                    if (current_length > max_length) {
                                        max_length = current_length;
                                    }
                                }
                            }
                        }


                        if (is_right) {
                            for (int t = 0; t < 4; t++) {
                                if ((examineLeft.rightNeighborIDs[t] != contigIdx) && (examineLeft.rightNeighborIDs[t] != INVALID)) {
                                    if (examineLeft.rightNeighborIDs[t] < 0 || examineLeft.rightNeighborIDs[t] >= totalContigs) {
                                        DIE("Invalid examineLeft rightNeighborIDs neighbor: t=%d %lld latestLeft=%lld\n", t, (lld)examineLeft.rightNeighborIDs[t], (lld)latestLeft);
                                    }
                                    current_depth = contigDepths[examineLeft.rightNeighborIDs[t]];
                                    current_length = contigsInfo[examineLeft.rightNeighborIDs[t]].length;
                                    if (current_depth > max_depth) {
                                        max_depth = current_depth;
                                    }
                                    if (current_length > max_length) {
                                        max_length = current_length;
                                    }
                                }
                            }
                        }
                    }
                }

                if (nFoundRight == 1) {
                    is_left = 0;
                    is_right = 0;
                    examineRight = contigsInfo[latestRight];
                    for (int t = 0; t < 4; t++) {
                        if (examineRight.leftNeighborIDs[t] == contigIdx) {
                            is_left = 1;
                        }
                        if (examineRight.rightNeighborIDs[t] == contigIdx) {
                            is_right = 1;
                        }
                    }

                    if ((is_left + is_right) == 1) {
                        if (is_left) {
                            for (int t = 0; t < 4; t++) {
                                if ((examineRight.leftNeighborIDs[t] != contigIdx) && (examineRight.leftNeighborIDs[t] != INVALID)) {
                                    if (examineRight.leftNeighborIDs[t] < 0 || examineRight.leftNeighborIDs[t] >= totalContigs) {
                                        DIE("Invalid examineRight left neighbor: t=%d %lld latestRight=%lld\n", t, (lld)examineRight.leftNeighborIDs[t], (lld)latestRight);
                                    }
                                    current_depth = contigDepths[examineRight.leftNeighborIDs[t]];
                                    current_length = contigsInfo[examineRight.leftNeighborIDs[t]].length;
                                    if (current_depth > max_depth) {
                                        max_depth = current_depth;
                                    }
                                    if (current_length > max_length) {
                                        max_length = current_length;
                                    }
                                }
                            }
                        }


                        if (is_right) {
                            for (int t = 0; t < 4; t++) {
                                if ((examineRight.rightNeighborIDs[t] != contigIdx) && (examineRight.rightNeighborIDs[t] != INVALID)) {
                                    if (examineRight.rightNeighborIDs[t] < 0 || examineRight.rightNeighborIDs[t] >= totalContigs) {
                                        DIE("Invalid examineRight right neighbor: t=%d %lld latestRight=%lld\n", t, (lld)examineRight.rightNeighborIDs[t], (lld)latestRight);
                                    }
                                    current_depth = contigDepths[examineRight.rightNeighborIDs[t]];
                                    current_length = contigsInfo[examineRight.rightNeighborIDs[t]].length;
                                    if (current_depth > max_depth) {
                                        max_depth = current_depth;
                                    }
                                    if (current_length > max_length) {
                                        max_length = current_length;
                                    }
                                }
                            }
                        }
                    }
                }

                if ((curContig.depth < 0.3 * max_depth)) {
                    validContigs[contigIdx] = INVALID;
                    myChanges = 1;
                }
            }
#endif
        }

        sharedChanges[MYTHREAD] = myChanges;

        /* All reduce in myChanges variables to check if any contigs have been removed in this iteration */
        UPC_LOGGED_BARRIER;
        upc_all_reduceC(&max_changes, sharedChanges, UPC_MAX, THREADS, 1, NULL, UPC_IN_NOSYNC | UPC_OUT_NOSYNC);
        UPC_LOGGED_BARRIER;
        upc_all_broadcast(sharedChanges, &max_changes, sizeof(char), UPC_IN_NOSYNC | UPC_OUT_NOSYNC);
        UPC_LOGGED_BARRIER;

        totalChanges = sharedChanges[MYTHREAD];

        if (totalChanges == 0) {
            break;
        }

        /* Update my contigs's neighbors based on the "new" set of Contigs */
        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            if (validContigs[contigIdx] == INVALID) {
                continue;
            }

            curContig = contigsInfo[contigIdx];
            for (j = 0; j < 4; j++) {
                if (curContig.leftNeighborIDs[j] != INVALID) {
                    validity = validContigs[curContig.leftNeighborIDs[j]];
                    if (validity == INVALID) {
                        curContig.leftNeighborIDs[j] = INVALID;
                    }
                }
            }
            for (j = 0; j < 4; j++) {
                if (curContig.rightNeighborIDs[j] != INVALID) {
                    validity = validContigs[curContig.rightNeighborIDs[j]];
                    if (validity == INVALID) {
                        curContig.rightNeighborIDs[j] = INVALID;
                    }
                }
            }
            contigsInfo[contigIdx] = curContig;
        }

        UPC_LOGGED_BARRIER;

        tau = tau * (1 + alpha);
    }

    double myEnd = now() - myStart;
    LOGF("Completed %lld loops in %0.3f s\n", loopCount, myEnd);
    UPC_LOGGED_BARRIER;
    int64_t total_count = reduce_long(loopCount, UPC_ADD, SINGLE_DEST);
    int64_t max_count = reduce_long(loopCount, UPC_MAX, SINGLE_DEST);
    double avg_count = ((double) total_count) / THREADS;
    serial_printf("Done with pruning the contig graph (my %lld loops) %0.1f avg %lld max, in %0.3f s\n", (lld) loopCount, ((double) total_count) / THREADS, (lld) max_count, now() - myStart);

#ifdef LONG_BUBBLES
    /* Here we identify bubbles in the survived contig graph -- we are interested in the "long" bubbles */

    int frontier[4];
    int r;
    int rightID, leftID;
    int bubbleMap[3][4];
    int64_t bubbleFlag[3];
    double min_depth = 999999999999.0;
    double current_depth;
    int contig_to_prune;

    char my_bubble_file_name[MAX_FILE_PATH];
    sprintf(my_bubble_file_name, "my_bubbles_%d" GZIP_EXT, kmerLength);
    GZIP_FILE myBubbleFile = openCheckpoint(my_bubble_file_name, "w");

    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        /* Find bubbles with cur contig as "left anchor" */
        curContig = contigsInfo[contigIdx];
        frontier[0] = INVALID;
        frontier[1] = INVALID;
        frontier[2] = INVALID;
        frontier[3] = INVALID;
        min_depth = 999999999999.0;

        for (j = 0; j < 3; j++) {
            bubbleFlag[j] = -1;
            for (r = 0; r < 4; r++) {
                bubbleMap[j][r] = 0;
            }
        }

        for (j = 0; j < 4; j++) {
            if (curContig.rightNeighborIDs[j] == INVALID) {
                continue;
            }

            curContigRight = contigsInfo[curContig.rightNeighborIDs[j]];

            /* Make sure we have not found a circular path of contigs */
            if (curContigRight.myID == contigIdx) {
                continue;
            }

            nValidsRight = 0;
            nValidsLeft = 0;

            for (r = 0; r < 4; r++) {
                if (curContigRight.rightNeighborIDs[r] != INVALID) {
                    nValidsRight++;
                    rightID = curContigRight.rightNeighborIDs[r];
                }
                if (curContigRight.leftNeighborIDs[r] != INVALID) {
                    nValidsLeft++;
                    leftID = curContigRight.leftNeighborIDs[r];
                }
            }

            /* This is a circular situation -- just skip it ... */
            if (rightID == leftID) {
                continue;
            }

            /* Consider only "simple" bubbles -- i.e. not nested ones */
            if ((nValidsLeft == 1) && (nValidsRight == 1)) {
                if (leftID == contigIdx) {
                    frontier[j] = rightID;
                } else {
                    frontier[j] = leftID;
                }
            }
        }

        for (j = 0; j < 3; j++) {
            if (frontier[j] == INVALID) {
                continue;
            }

            for (r = j + 1; r < 4; r++) {
                if (frontier[r] == frontier[j]) {
                    bubbleMap[j][r] = 1;
                    bubbleMap[j][j] = 1;
                    bubbleFlag[j] = frontier[r];
                    frontier[r] = INVALID;
                }
            }
            frontier[j] = INVALID;
        }

        for (j = 0; j < 3; j++) {
            if (bubbleFlag[j] != -1) {
                GZIP_PRINTF(myBubbleFile, "---- Contig_%lld (Length: %d - depth: %f ) ---- ", (lld)contigIdx, contigsInfo[contigIdx].length, contigsInfo[contigIdx].depth);
                for (r = 0; r < 4; r++) {
                    if (bubbleMap[j][r] == 1) {
                        current_depth = contigsInfo[curContig.rightNeighborIDs[r]].depth;
                        GZIP_PRINTF(myBubbleFile, " Contig_%lld (Length: %d - depth: %f) , ", (lld)curContig.rightNeighborIDs[r], contigsInfo[curContig.rightNeighborIDs[r]].length, current_depth);
                        if (current_depth < min_depth) {
                            min_depth = current_depth;
                            contig_to_prune = (int)curContig.rightNeighborIDs[r];
                        }
                    }
                }
                GZIP_PRINTF(myBubbleFile, "---- Contig_%lld (Length: %d - depth: %f ) ----\n\n", (lld)bubbleFlag[j], contigsInfo[bubbleFlag[j]].length, contigsInfo[bubbleFlag[j]].depth);

                /* Prune the contig with the minimum depth in the bubble */
                /* FIXME: Use atomic write */
                validContigs[contig_to_prune] = INVALID;
            }
        }

        /* Find bubbles with cur contig as "right anchor" */
        frontier[0] = INVALID;
        frontier[1] = INVALID;
        frontier[2] = INVALID;
        frontier[3] = INVALID;
        min_depth = 999999999999.0;

        for (j = 0; j < 3; j++) {
            bubbleFlag[j] = -1;
            for (r = 0; r < 4; r++) {
                bubbleMap[j][r] = 0;
            }
        }

        for (j = 0; j < 4; j++) {
            if (curContig.leftNeighborIDs[j] == INVALID) {
                continue;
            }

            curContigLeft = contigsInfo[curContig.leftNeighborIDs[j]];

            /* Make sure we have not found a circular path of contigs */
            if (curContigLeft.myID == contigIdx) {
                continue;
            }

            nValidsRight = 0;
            nValidsLeft = 0;

            for (r = 0; r < 4; r++) {
                if (curContigLeft.rightNeighborIDs[r] != INVALID) {
                    nValidsRight++;
                    rightID = curContigLeft.rightNeighborIDs[r];
                }
                if (curContigLeft.leftNeighborIDs[r] != INVALID) {
                    nValidsLeft++;
                    leftID = curContigLeft.leftNeighborIDs[r];
                }
            }

            /* This is a circular situation -- just skip it ... */
            if (rightID == leftID) {
                continue;
            }

            /* Consider only "simple" bubbles -- i.e. not nested ones */
            if ((nValidsLeft == 1) && (nValidsRight == 1)) {
                if (leftID == i) {
                    frontier[j] = rightID;
                } else {
                    frontier[j] = leftID;
                }
            }
        }

        for (j = 0; j < 3; j++) {
            if (frontier[j] == INVALID) {
                continue;
            }

            for (r = j + 1; r < 4; r++) {
                if (frontier[r] == frontier[j]) {
                    bubbleMap[j][r] = 1;
                    bubbleMap[j][j] = 1;
                    bubbleFlag[j] = frontier[r];
                    frontier[r] = INVALID;
                }
            }
            frontier[j] = INVALID;
        }

        for (j = 0; j < 3; j++) {
            if (bubbleFlag[j] != -1) {
                GZIP_PRINTF(myBubbleFile, "---- Contig_%lld (Length: %d - depth: %f ) ---- ", (lld)bubbleFlag[j], contigsInfo[bubbleFlag[j]].length, contigsInfo[bubbleFlag[j]].depth);
                for (r = 0; r < 4; r++) {
                    if (bubbleMap[j][r] == 1) {
                        current_depth = contigsInfo[curContig.leftNeighborIDs[r]].depth;
                        GZIP_PRINTF(myBubbleFile, " Contig_%lld (Length: %d - depth: %f) , ", (lld)curContig.leftNeighborIDs[r], contigsInfo[curContig.leftNeighborIDs[r]].length, current_depth);
                        if (current_depth < min_depth) {
                            min_depth = current_depth;
                            contig_to_prune = (int)curContig.leftNeighborIDs[r];
                        }
                    }
                }
                GZIP_PRINTF(myBubbleFile, "---- Contig_%ld (Length: %d - depth: %f ) ----\n\n", contigIdx, contigsInfo[contigIdx].length, contigsInfo[contigIdx].depth);

                /* Prune the contig with the minimum depth in the bubble */
                /* FIXME: Use atomic write */
                validContigs[contig_to_prune] = INVALID;
            }
        }
    }


    closeCheckpoint(myBubbleFile);
    upc_barrier;

    /* Update my contigs's neighbors based on the "new" set of Contigs */
    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        if (validContigs[contigIdx] == INVALID) {
            continue;
        }

        curContig = contigsInfo[contigIdx];
        for (j = 0; j < 4; j++) {
            if (curContig.leftNeighborIDs[j] != INVALID) {
                validity = validContigs[curContig.leftNeighborIDs[j]];
                if (validity == INVALID) {
                    curContig.leftNeighborIDs[j] = INVALID;
                }
            }
        }
        for (j = 0; j < 4; j++) {
            if (curContig.rightNeighborIDs[j] != INVALID) {
                validity = validContigs[curContig.rightNeighborIDs[j]];
                if (validity == INVALID) {
                    curContig.rightNeighborIDs[j] = INVALID;
                }
            }
        }
        contigsInfo[contigIdx] = curContig;
    }

    upc_barrier;

#endif

    shared[] char *sharedSeq = NULL;

    char sign;

    /* Read the contig sequences and store them in shared address space */
    //LOG("Allocating %lld bytes for contigs\n", (lld) contigsHeapLen);
    if (contigsHeapLen) {
        UPC_ALLOC_CHK(sharedSeq, contigsHeapLen * sizeof(char));
        size_t offset = 0;
        resetBuffer(contigBuffer);
        nContigs = 0;
        while (gzgetsBuffer(contigBuffer, 8192, contigFile) != NULL) {
            char *header = getStartBuffer(contigBuffer);
            //fprintf(stderr, "Thread %d: read(%lld) %s\n", MYTHREAD, (lld) strlen(header), header);
            assert(header[0] == '>' && strlen(header) >= 9);
            contigID = atol(header + 8);

            resetBuffer(contigBuffer);
            if (!gzgetsBuffer(contigBuffer, 8192, contigFile)) {
                break;
            }
            chompBuffer(contigBuffer);
            size_t len = getLengthBuffer(contigBuffer);
            assert(offset + len <= contigsHeapLen);
            memcpy((char *)sharedSeq + offset, getStartBuffer(contigBuffer), len);
            contigsInfo[contigID].seq = sharedSeq + offset;
            offset += len;
            assert(offset <= contigsHeapLen);
            nContigs++;
            resetBuffer(contigBuffer);
        }
    }
    freeBuffer(contigBuffer); contigBuffer = NULL;

    //LOG("Stored %lld contigs in my address space (%lld bytes)\n", (lld) nContigs, (lld) contigsHeapLen);
    upc_barrier;
    if (MYTHREAD == 0) {
        printf("Stored contigs in shared address space\n");
    }

    /* Now for "my" contigs iterate and find contigs that should be linked (unique neighbors in both direstions). NOTE: Contigs overlap in k-2 bases. Also need logic for reverse complement. When we terminate a sequence of contigs that should be connected, the owner of the contig with the LOWEST contig ID is doing the actual concatenation (in this away we avoid duplicate work) */


    /*
     * myOutputFile2 = fopen_chk(my_output_file_name2, "w");
     *
     * for (i=MYTHREAD; i<totalContigs; i+=THREADS) {
     * curContig = contigsInfo[i];
     *
     * if (validContigs[i] == INVALID) continue;
     *
     * foundN =0;
     * fprintf(myOutputFile2, "Neighbors of contig %lld ([%c][%c]) are : ", (lld) i, curContig.leftExt, curContig.rightExt);
     *
     * neighborhoodDepth = 0.0;
     *
     * for (j=0; j<4; j++) {
     *    if (curContig.leftNeighborIDs[j] != INVALID ) {
     *       foundN++;
     *       neighborhoodDepth += contigDepths[curContig.leftNeighborIDs[j]];
     *       fprintf(myOutputFile2, "\t(LEFT-%d: %d)", j, (int) curContig.leftNeighborIDs[j]);
     *    }
     * }
     *
     * for (j=0; j<4; j++) {
     *    if (curContig.rightNeighborIDs[j] != INVALID ) {
     *       foundN++;
     *       neighborhoodDepth += contigDepths[curContig.rightNeighborIDs[j]];
     *       fprintf(myOutputFile2, "\t(RIGHT-%d: %d)", j, (int) curContig.rightNeighborIDs[j]);
     *    }
     * }
     *
     * if (foundN == 0) {
     *    fprintf(myOutputFile2, " -- ");
     * } else {
     *    fprintf(myOutputFile2, "\nMy depth is %.f and my neihborhood average is %.f\n", curContig.depth, neighborhoodDepth/(1.0*foundN));
     * }
     *
     * fprintf(myOutputFile2, "\n");
     * }
     *
     * fclose_track(myOutputFile2);
     */


    /* TODO use Buffer to allow for large contigs */
    Buffer receivedBuffer = initBuffer(MAX_CONTIG_SIZE);
    Buffer finalContigBuffer = initBuffer(MAX_CONTIG_SIZE);
    char *finalContig = NULL, *received = NULL;
    int prevContigId;

    if (!merge_paths) {
        //LOG("!merge_paths");
#ifndef INSERT_GAPS

        int64_t my_num_contigs = 0;
        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            if (validContigs[contigIdx] != INVALID) {
                my_num_contigs++;
            }
        }
        UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, my_num_contigs);
        /*
        char my_output_graph_file_name[255];
        sprintf(my_output_graph_file_name, "pruned_%s-kmer-edges.fasta" GZIP_EXT, contig_file_name);
        GZIP_FILE myOutputGraphFile = openCheckpoint(my_output_graph_file_name, "w");
        */
        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            if (validContigs[contigIdx] == INVALID) {
                continue;
            }

            curContig = contigsInfo[contigIdx];
            received = getContigBuffer(receivedBuffer, curContig);

            // UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 1);

#ifdef TRIM_CONTIGS
            if ((kmer_len == TRIM_K) && ((curContig.length - kmer_len + 1 - 2 * TRIM_LENGTH) > 0)) {
                received[curContig.length - TRIM_LENGTH] = '\0';
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_FWRITE(received + TRIM_LENGTH, 1, strlen(received + TRIM_LENGTH), myOutputFile);
                GZIP_PRINTF(myOutputFile, "\n");
                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, curContig.length - kmer_len + 1 - 2 * TRIM_LENGTH, curContig.depth);
            } else
#endif

            {
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_FWRITE(received, 1, strlen(received), myOutputFile);
                GZIP_PRINTF(myOutputFile, "\n");
                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, curContig.length - kmer_len + 1, curContig.depth);
                /*
                GZIP_PRINTF(myOutputGraphFile, "Contig %ld  Left ", newId);
                for (int j = 0; j < 4; j++) {
                    if (contigsInfo[contigIdx].leftNeighborIDs[j] != INVALID) 
                    FIXME: these neighbor ids are wrong, because they have been remapped. Not sure how to fix this problem
                        GZIP_PRINTF(myOutputGraphFile, "%ld ", contigsInfo[contigIdx].leftNeighborIDs[j]);
                }
                GZIP_PRINTF(myOutputGraphFile, "  Right ");
                for (int j = 0; j < 4; j++) {
                    if (contigsInfo[contigIdx].rightNeighborIDs[j] != INVALID) 
                    FIXME: these neighbor ids are wrong, because they have been remapped. Not sure how to fix this problem
                        GZIP_PRINTF(myOutputGraphFile, "%ld ", contigsInfo[contigIdx].rightNeighborIDs[j]);
                }
                GZIP_PRINTF(myOutputGraphFile, "\n");
                */
            }

            //LOG("Wrote Contig_%lld (%lld bases)\n", (lld) newId, (lld) curContig.length);
            newId++;
        }
        //closeCheckpoint(myOutputGraphFile);

#endif

#ifdef INSERT_GAPS

        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            if (validContigs[contigIdx] == INVALID) {
                continue;
            }

            curContig = contigsInfo[contigIdx];
            received = getContigBuffer(receivedBuffer, curContig);

            if (curContig.length > 2000) {
/* FIXME why print 0-1250 and 1500-end instead of half or an overlapping region ? */
                /* First print contig from positions 0 ... 249 */
                UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 2);
                //LOG("Printing large contig with gaps (%lld): Contig_%lld and Contig_%lld\n", (lld) curContig.length, (lld) newId-1, (lld) newId);

                assert(strlen(received) > 0);
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId - 1);
                GZIP_PRINTF(myOutputFile, "%.*s\n", 1250, received);
                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId - 1, 1250 - kmer_len + 1, curContig.depth);

                /* Now print contig from positions 751 ... cur_contig_length-1 */
                assert(strlen(received + 1500) > 0);
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_PRINTF(myOutputFile, "%s\n", received + 1500);
                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, curContig.length - 1500 - kmer_len + 1, curContig.depth);
            }
/* FIXME what about contigs < 2000 bases? */
        }
#endif
    } else {
        for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
            if (validContigs[contigIdx] == INVALID) {
                continue;
            }

            curContig = contigsInfo[contigIdx];
            //LOG("Processing Contig_%lld\n", (lld) curContig.myID);

            minID = contigIdx;
            startContig = contigIdx;
            foundCycle = 0;

            nLeftNeighbors = 0;
            for (j = 0; j < 4; j++) {
                if (curContig.leftNeighborIDs[j] != INVALID) {
                    nLeftNeighbors++;
                    theNeighbor = curContig.leftNeighborIDs[j];
                }
            }

            posWalkLeft = 0;
            curID = contigIdx;

            while (nLeftNeighbors == 1) {
                curContigLeft = contigsInfo[theNeighbor];
                nValidsRight = 0;
                nValidsLeft = 0;
                foundRight = 0;
                foundLeft = 0;

                /* Check if unique neighbor of "left neighbor" is current neighbor */
                for (j = 0; j < 4; j++) {
                    if (curContigLeft.rightNeighborIDs[j] == curID) {
                        strand = PLUS;
                        foundRight = 1;
                    }
                    if (curContigLeft.rightNeighborIDs[j] != INVALID) {
                        nValidsRight++;
                    }
                    if (curContigLeft.leftNeighborIDs[j] == curID) {
                        strand = MINUS;
                        foundLeft = 1;
                    }
                    if (curContigLeft.leftNeighborIDs[j] != INVALID) {
                        nValidsLeft++;
                    }
                }

                if (((foundRight == 1) && (nValidsRight == 1)) || ((foundLeft == 1) && (nValidsLeft == 1))) {
                    /* Make sure we have not found a circular path of contigs */
                    if (curContigLeft.myID == startContig) {
                        foundCycle = 1;
                        break;
                    }

                    /* "Lock left neighbor" */
                    leftWalkIDs[posWalkLeft] = curContigLeft.myID;
                    curID = curContigLeft.myID;
                    if (curID < minID) {
                        minID = curID;
                    }

                    if (strand == PLUS) {
                        leftWalkStrands[posWalkLeft] = PLUS;
                        nLeftNeighbors = 0;
                        for (j = 0; j < 4; j++) {
                            if (curContigLeft.leftNeighborIDs[j] != INVALID) {
                                nLeftNeighbors++;
                                theNeighbor = curContigLeft.leftNeighborIDs[j];
                            }
                        }
                    }

                    if (strand == MINUS) {
                        leftWalkStrands[posWalkLeft] = MINUS;
                        nLeftNeighbors = 0;
                        for (j = 0; j < 4; j++) {
                            if (curContigLeft.rightNeighborIDs[j] != INVALID) {
                                nLeftNeighbors++;
                                theNeighbor = curContigLeft.rightNeighborIDs[j];
                            }
                        }
                    }

                    posWalkLeft++;
                } else {
                    nLeftNeighbors = 0;
                }
            }
            //LOG("posWalkLeft: %d, nLeftNeighbors: %d foundCycle: %d, startContig: %d, minID: %d\n", posWalkLeft, nLeftNeighbors, foundCycle, startContig, minID);

            /* If we found a circular path of contigs, we break it and print it in the case the seed has the minimum ID of all the contigs in the path */
            if ((foundCycle == 1) && (startContig == minID)) {
                /* Print all the contigs in the left tail of the circular path */
                //LOG("Found circle in left tail");
                posInBigContig = 0;
                nMers = 0;
                meanContigDepth = 0.0;

                for (j = 0; j < posWalkLeft; j++) {
                    curContig = contigsInfo[leftWalkIDs[posWalkLeft - j - 1]];
                    received = getContigBuffer(receivedBuffer, curContig);

                    nMers += curContig.length - kmerLength + 1;
                    meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                    if (leftWalkStrands[posWalkLeft - j - 1] == MINUS) {
                        reverseComplementSeq(received, received, curContig.length);
                    }

                    posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);
                    assert(posInBigContig > 0);

                    prevContigId = curContig.myID;
                }

                /* Print the start contig of the circular contig... */
                curContig = contigsInfo[startContig];
                received = getContigBuffer(receivedBuffer, curContig);

                nMers += curContig.length - kmerLength + 1;
                meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                prevContigId = curContig.myID;
                finalContig = getStartBuffer(finalContigBuffer);

                assert(posInBigContig > 0);
                assert(finalContig[posInBigContig] == '\0');
                assert(strlen(finalContig) == posInBigContig);

                // this atomic only happens if merge_paths is set
                UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 1);
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_FWRITE(finalContig, 1, strlen(finalContig), myOutputFile);
                GZIP_PRINTF(myOutputFile, "\n");

                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, posInBigContig - kmer_len + 1, meanContigDepth / nMers);
                //LOG("Printed Contig_%lld (%lld bases)\n", (lld) newId, (lld) posInBigContig);
            }

            if (foundCycle == 1) {
                // Skip the remainder code since we have already printed the circlular code if that was the case
                continue;
            }

            nRightNeighbors = 0;
            for (j = 0; j < 4; j++) {
                if (curContig.rightNeighborIDs[j] != INVALID) {
                    nRightNeighbors++;
                    theNeighbor = curContig.rightNeighborIDs[j];
                }
            }

            posWalkRight = 0;
            curID = contigIdx;

            while (nRightNeighbors == 1) {
                curContigRight = contigsInfo[theNeighbor];
                nValidsRight = 0;
                nValidsLeft = 0;
                foundRight = 0;
                foundLeft = 0;

                /* Check if unique neighbor of "left neighbor" is current neighbor */
                for (j = 0; j < 4; j++) {
                    if (curContigRight.rightNeighborIDs[j] == curID) {
                        strand = MINUS;
                        foundRight = 1;
                    }
                    if (curContigRight.rightNeighborIDs[j] != INVALID) {
                        nValidsRight++;
                    }
                    if (curContigRight.leftNeighborIDs[j] == curID) {
                        strand = PLUS;
                        foundLeft = 1;
                    }
                    if (curContigRight.leftNeighborIDs[j] != INVALID) {
                        nValidsLeft++;
                    }
                }

                if (((foundRight == 1) && (nValidsRight == 1)) || ((foundLeft == 1) && (nValidsLeft == 1))) {
                    /* Make sure we have not found a circular path of contigs */
                    if (curContigRight.myID == startContig) {
                        foundCycle = 1;
                        break;
                    }
                    /* "Lock right neighbor" */
                    rightWalkIDs[posWalkRight] = curContigRight.myID;
                    curID = curContigRight.myID;
                    if (curID < minID) {
                        minID = curID;
                    }

                    if (strand == PLUS) {
                        rightWalkStrands[posWalkRight] = PLUS;
                        nRightNeighbors = 0;
                        for (j = 0; j < 4; j++) {
                            if (curContigRight.rightNeighborIDs[j] != INVALID) {
                                nRightNeighbors++;
                                theNeighbor = curContigRight.rightNeighborIDs[j];
                            }
                        }
                    }

                    if (strand == MINUS) {
                        rightWalkStrands[posWalkRight] = MINUS;
                        nRightNeighbors = 0;
                        for (j = 0; j < 4; j++) {
                            if (curContigRight.leftNeighborIDs[j] != INVALID) {
                                nRightNeighbors++;
                                theNeighbor = curContigRight.leftNeighborIDs[j];
                            }
                        }
                    }

                    posWalkRight++;
                } else {
                    nRightNeighbors = 0;
                }
            }
            //LOG("posWalkRight: %d, nRightNeighbors: %d foundCycle: %d, startContig: %d, minID: %d\n", posWalkRight, nRightNeighbors, foundCycle, startContig, minID);

            if ((foundCycle == 1) && (startContig == minID)) {
                //LOG("Found circle in right tail");
                /* Print all the contigs in the left tail of the circular path */
                posInBigContig = 0;
                nMers = 0;
                meanContigDepth = 0.0;

                for (j = 0; j < posWalkLeft; j++) {
                    curContig = contigsInfo[leftWalkIDs[posWalkLeft - j - 1]];
                    received = getContigBuffer(receivedBuffer, curContig);

                    nMers += curContig.length - kmerLength + 1;
                    meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                    if (leftWalkStrands[posWalkLeft - j - 1] == MINUS) {
                        reverseComplementSeq(received, received, curContig.length);
                    }

                    posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                    assert(posInBigContig > 0);

                    prevContigId = curContig.myID;
                }

                /* Print the start contig of the circular contig... */
                curContig = contigsInfo[startContig];
                received = getContigBuffer(receivedBuffer, curContig);

                nMers += curContig.length - kmerLength + 1;
                meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                finalContig = getStartBuffer(finalContigBuffer);
                assert(posInBigContig > 0);
                assert(finalContig[posInBigContig] == '\0');
                assert(strlen(finalContig) == posInBigContig);

                prevContigId = curContig.myID;

                for (j = 0; j < posWalkRight; j++) {
                    curContig = contigsInfo[rightWalkIDs[j]];
                    received = getContigBuffer(receivedBuffer, curContig);

                    nMers += curContig.length - kmerLength + 1;
                    meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                    if (rightWalkStrands[j] == MINUS) {
                        reverseComplementSeq(received, received, curContig.length);
                    }

                    posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                    prevContigId = curContig.myID;
                }

                finalContig = getStartBuffer(finalContigBuffer);
                assert(posInBigContig > 0);
                assert(finalContig[posInBigContig] == '\0');
                assert(strlen(finalContig) == posInBigContig);

                // this atomic only happens if merge_path is set
                UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 1);
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_FWRITE(finalContig, 1, strlen(finalContig), myOutputFile);
                GZIP_PRINTF(myOutputFile, "\n");

                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, posInBigContig - kmer_len + 1, meanContigDepth / nMers);

                //LOG("Printed Contig_%lld (%lld bases)\n", (lld) newId, (lld) posInBigContig);
            }

            if (foundCycle == 1) {
                // Skip the remainder code since we have already printed the circlular code if that was the case
                continue;
            }

            /* If the seed contig has the minimum ID then print the contig path (in terms of sequence) */
            if (minID == contigIdx) {
                //LOG("minID: %d\n", minID);
                posInBigContig = 0;
                nMers = 0;
                meanContigDepth = 0.0;

                for (j = 0; j < posWalkLeft; j++) {
                    curContig = contigsInfo[leftWalkIDs[posWalkLeft - j - 1]];
                    received = getContigBuffer(receivedBuffer, curContig);

                    nMers += curContig.length - kmerLength + 1;
                    meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                    if (leftWalkStrands[posWalkLeft - j - 1] == MINUS) {
                        reverseComplementSeq(received, received, curContig.length);
                    }

                    posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                    assert(posInBigContig > 0);

                    prevContigId = curContig.myID;
                }


                curContig = contigsInfo[contigIdx];
                received = getContigBuffer(receivedBuffer, curContig);

                nMers += curContig.length - kmerLength + 1;
                meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                finalContig = getStartBuffer(finalContigBuffer);
                assert(posInBigContig > 0);
                assert(finalContig[posInBigContig] == '\0');
                //LOG("final (%lld) vs posInBig (%lld) for Contig_%lld (%lld) %.40s", (lld) strlen(finalContig), (lld) posInBigContig, (lld) curContig.myID, (lld) strlen(received), received);
                assert(strlen(finalContig) == posInBigContig);

                prevContigId = curContig.myID;

                for (j = 0; j < posWalkRight; j++) {
                    curContig = contigsInfo[rightWalkIDs[j]];
                    received = getContigBuffer(receivedBuffer, curContig);

                    nMers += curContig.length - kmerLength + 1;
                    meanContigDepth += (curContig.length - kmerLength + 1) * curContig.depth;

                    if (rightWalkStrands[j] == MINUS) {
                        reverseComplementSeq(received, received, curContig.length);
                    }

                    posInBigContig += appendContigBuffer(finalContigBuffer, posInBigContig, received, curContig, kmerLength);

                    prevContigId = curContig.myID;
                }

                finalContig = getStartBuffer(finalContigBuffer);
                assert(posInBigContig > 0);
                assert(finalContig[posInBigContig] == '\0');
                assert(strlen(finalContig) == posInBigContig);

                // this atomic only happens if merge_paths is set
                UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 1);
                GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                GZIP_FWRITE(finalContig, 1, strlen(finalContig), myOutputFile);
                GZIP_PRINTF(myOutputFile, "\n");

                GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%d\t%f\n", (lld)newId, posInBigContig - kmer_len + 1, meanContigDepth / nMers);
            }
        }
    }

#ifdef DISCONNECT_WEAK
    int removeFirstKmer = 0;
    int removeLastKmer = 0;
    char *contigStart;

    upc_barrier;

    //int64_t newId;
    for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
        if (validContigs[contigIdx] == INVALID) {
            for (contigIdx = MYTHREAD; contigIdx < totalContigs; contigIdx += THREADS) {
                /* Instead of completely removing the contig from the graph, just "disconnect it" by removing start/end kmers */
                if (validContigs[contigIdx] == INVALID) {
                    removeFirstKmer = 0;
                    removeLastKmer = 0;

                    curContig = contigsInfo[contigIdx];

                    for (j = 0; j < 4; j++) {
                        if (curContig.leftNeighborIDs[j] != INVALID) {
                            removeFirstKmer = 1;
                            break;
                        }
                    }

                    for (j = 0; j < 4; j++) {
                        if (curContig.rightNeighborIDs[j] != INVALID) {
                            removeLastKmer = 1;
                            break;
                        }
                    }


                    received = getContigBuffer(receivedBuffer, curContig);
                    // this atomic only happens if disconnect_weak is set
                    UPC_ATOMIC_FADD_I64(&newId, &contig_id_pool, 1);

                    if (removeFirstKmer == 0) {
                        contigStart = &received[0];
                    } else {
                        contigStart = &received[1];
                    }

                    if (removeLastKmer == 1) {
                        received[curContig.length - 1] = '\0';
                    }

                    GZIP_PRINTF(myOutputFile, ">Contig_%lld\n", (lld)newId);
                    GZIP_FWRITE(contigStart, 1, strlen(contigStart), myOutputFile);
                    GZIP_PRINTF(myOutputFile, "\n");
                    GZIP_PRINTF(myDepthOutputFile, "Contig%lld\t%lu\t%f\n", (lld)newId, strlen(contigStart) - kmer_len + 1, curContig.depth);
                }
            }

            upc_barrier;
#endif

    if (includePrefixMerDepth) {
        int is_least;
        LIST_T copy;
        UPC_TICK_T s1 = UPC_TICKS_NOW();
        serial_printf("Outputting prefix merDepth file\n");
        GZIP_REWIND(contigFile);
        myPrefixDepthOutputFile = openCheckpoint(my_prefix_depth_output_file_name, "w");
        prefixDepthSumArray = (int64_t *)malloc_chk(nMersInContigs * sizeof(int64_t));
        contigBuffer = initBuffer(8192);
        char *curKmer = (char *)malloc_chk(kmer_len + 1);
        curKmer[kmer_len] = '\0';
        char *contigB = NULL;
        int64_t l = 0, posInArray = 0;
        while ((contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile)) != NULL) {
            // ignore name
            resetBuffer(contigBuffer);
            l++;
            if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
                WARN("Missing fasta line in %s at line %lld. expecting fasta with one line per record.\n", my_contig_file_name, (lld)l);
                break;
            }
            l++;
            contigLen = getLengthBuffer(contigBuffer);
            /* for each kmer in the contig extract the depth and append it to the sum array */
            for (int i = 0; i < contigLen - kmerLength; i++) {
                memcpy(curKmer, &contigB[i], kmerLength * sizeof(char));
                lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, curKmer, &copy, &is_least, kmer_len);
                if (i == 0) {
                    prefixDepthSumArray[posInArray] = copy.count;
                } else {
                    prefixDepthSumArray[posInArray] = copy.count + prefixDepthSumArray[posInArray - 1];
                }
                posInArray++;
            }
        }
        serial_printf("Calculated prefix merDepth file in %0.3f sec\n", UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - s1));
        assert(posInArray == nMersInContigs);
        freeBuffer(contigBuffer);
        GZIP_FWRITE(prefixDepthSumArray, 1, nMersInContigs * sizeof(int64_t), myPrefixDepthOutputFile);
        closeCheckpoint(myPrefixDepthOutputFile);
        free_chk(prefixDepthSumArray);
        free_chk(curKmer);
    }

    closeCheckpoint(contigFile);
    closeCheckpoint(myOutputFile);
    closeCheckpoint(myDepthOutputFile);
    freeBuffer(receivedBuffer); receivedBuffer = NULL; received = NULL;
    freeBuffer(finalContigBuffer); finalContigBuffer = NULL; finalContig = NULL;

    UFXClose(UFX_f);


    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);

/*
 * for (i=MYTHREAD; i<totalContigs; i += THREADS) {
 *    if (validContigs[i] == INVALID) continue;
 *    if (contigsInfo[i].seq && upc_threadof(contigsInfo[i].seq))
 *       UPC_FREE_CHK(contigsInfo[i].seq);
 * }
 * upc_barrier;
 */
    UPC_ALL_FREE_CHK(contigsInfo);
    UPC_ALL_FREE_CHK(contigDepths);
    UPC_ALL_FREE_CHK(validContigs);
    UPC_ALL_FREE_CHK(sharedChanges);
    if (sharedSeq != NULL) {
        UPC_FREE_CHK(sharedSeq);
    }

    upc_barrier;

    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        depth_time = UPC_TICKS_TO_SECS(end - start);
        printf("\nTime for progressive relative depth: %f seconds\n", depth_time);
        printf("Eventually found %lld contigs (originally had %lld contigs)\n", (lld)contig_id_pool, (lld)totalContigs);
        ADD_DIAG("%lld", "contigs_found", (lld)contig_id_pool);

        char countFileName[MAX_FILE_PATH];
        sprintf(countFileName, "npruned_%s.txt", contig_file_name);
        FILE *countFD = fopen_rank_path(countFileName, "w", -1);
        fprintf(countFD, "%lld\n", (lld)contig_id_pool);
        fclose_track(countFD);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
