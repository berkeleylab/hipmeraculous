#ifndef PROGRESSIVE_RELATIVE_DEPTH_H_
#define PROGRESSIVE_RELATIVE_DEPTH_H_

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>
#include <upc.h>
#include <upc_collective.h>

#include "optlist.h"

#include "upc_common.h"
#include "common.h"
#include "Buffer.h"
#include "meraculous.h"
#include "utils.h"
#include "timers.h"
#include "upc_output.h"

#include "prd_dds.h"
#include "../../contigs/kmer_handling.h"

#include "kmer_hash_prd.h"
#include "buildUFXhashBinary_prd.h"
#include "progressiveRelativeDepthUtils.h"

#include "../../kcount/readufx.h"

// these are used in buildUFXHashBinary.h and so have to be defined before it is included


/* Define segment length for FASTA sequences */
#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

extern shared int64_t contig_id_pool;

#define PLUS 0
#define MINUS 1
#define COMPLEX_HAIR_REMOVAL
#define TRIM_K 77
#define TRIM_LENGTH 5
//#define DISCONNECT_WEAK
//#define LONG_BUBBLES
#define HAIR_MIN 150
//#define INSERT_GAPS

extern shared char max_changes;

char *getContigBuffer(Buffer buf, contigNeighborhood_t curContig);

int appendContigBuffer(Buffer buf, int posInBigContig, const char *received, contigNeighborhood_t curContig, int kmerLength);

int progressiveRelativeDepth_main(int argc, char **argv);

#endif // PROGRESSIVE_RELATIVE_DEPTH_H_
