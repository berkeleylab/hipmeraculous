#include "progressiveRelativeDepthUtils.h"

void lookup_and_assign_neighors(HASH_TABLE_T *dist_hashtable, char *kmer, int kmerLength, int direction, int64_t *neighborArray, char *kmerBuf, char extensionCode, int64_t myID, int kmer_len)
{
    LIST_T copy;

    shared[] LIST_T * lookup_res = NULL;
    int is_least;
    int posNewChar;

    neighborArray[0] = INVALID;
    neighborArray[1] = INVALID;
    neighborArray[2] = INVALID;
    neighborArray[3] = INVALID;


    if (direction == RIGHT) {
        memcpy(kmerBuf, &kmer[1], (kmerLength - 1) * sizeof(char));
        posNewChar = kmerLength - 1;
    } else if (direction == LEFT) {
        memcpy(&kmerBuf[1], kmer, (kmerLength - 1) * sizeof(char));
        posNewChar = 0;
    }

    if ((extensionCode == 'F') || (extensionCode == 'X')) {
        /* In this case, look up all possible extensions! */
        kmerBuf[posNewChar] = 'A';
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, kmerBuf, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (copy.contID != myID) {
                neighborArray[0] = copy.contID;
            }
        }

        kmerBuf[posNewChar] = 'C';
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, kmerBuf, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (copy.contID != myID) {
                neighborArray[1] = copy.contID;
            }
        }

        kmerBuf[posNewChar] = 'G';
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, kmerBuf, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (copy.contID != myID) {
                neighborArray[2] = copy.contID;
            }
        }

        kmerBuf[posNewChar] = 'T';
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, kmerBuf, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (copy.contID != myID) {
                neighborArray[3] = copy.contID;
            }
        }
    } else {
        /* This is a high quality extension, look up only this! */
        kmerBuf[posNewChar] = extensionCode;
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, kmerBuf, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (extensionCode == 'A') {
                if (copy.contID != myID) {
                    neighborArray[0] = copy.contID;
                }
            } else if (extensionCode == 'C') {
                if (copy.contID != myID) {
                    neighborArray[1] = copy.contID;
                }
            } else if (extensionCode == 'G') {
                if (copy.contID != myID) {
                    neighborArray[2] = copy.contID;
                }
            } else if (extensionCode == 'T') {
                if (copy.contID != myID) {
                    neighborArray[3] = copy.contID;
                }
            }
        }
    }
}
