#include "progressiveRelativeDepth_main.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("progressiveRelativeDepth");
    return progressiveRelativeDepth_main(argc, argv);
}
