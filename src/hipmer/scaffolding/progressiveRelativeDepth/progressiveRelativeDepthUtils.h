#ifndef PROGRESSIVE_RELATIVE_DEPTH_UTILS_H
#define PROGRESSIVE_RELATIVE_DEPTH_UTILS_H

#include "kmer_hash_prd.h"

#define MAX_CONTIG_SIZE 9000000
#define MAX_LINE_SIZE 1000
#define LEFT 0
#define RIGHT 1

#define INVALID (-1)
#define VALID (1)

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

void lookup_and_assign_neighors(HASH_TABLE_T *dist_hashtable, char *kmer, int kmerLength, int direction, int64_t *neighborArray, char *kmerBuf, char extensionCode, int64_t myID, int kmer_len);

#endif // PROGRESSIVE_RELATIVE_DEPTH_UTILS_H
