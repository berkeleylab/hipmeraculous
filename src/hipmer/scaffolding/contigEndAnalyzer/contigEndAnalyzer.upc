#include "contigEndAnalyzer_main.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("contigEndAnalyzer");
    return contigEndAnalyzer_main(argc, argv);
}
