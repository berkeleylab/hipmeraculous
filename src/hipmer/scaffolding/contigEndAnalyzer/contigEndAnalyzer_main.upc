#include "contigEndAnalyzer_main.h"

int contigEndAnalyzer_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

    if (_sv != NULL) {
        DBG("zeroing fileIOTime, cardCalcTime, setupTime and storeTimes\n");
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:k:s:d:D:B:X");
    print_args(optList, __func__);

    char *input_UFX_name, *contig_file_name;
    int kmerLength;
    int nContigs, dmin, chunk_size = 1;
    int is_per_thread = 1;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    double dynamic_dmin = 1.0;

    const char *base_dir = ".";

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = kmerLength = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Please compile with a larger MAX_KMER_SIZE (%d) for kmer_length %d", MAX_KMER_SIZE, kmer_len);
            }
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'D':
            dynamic_dmin = atof(thisOpt->argument);
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 's':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        default:
            break;
        }
    }

    HASH_TABLE_T *dist_hashtable;
    UPC_TICK_T start, end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
    }
    double con_time, depth_time;

#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    /* Build hash table using UFX file that also contains the FX kmers */

    int64_t myshare;
    int dsize;
    int64_t size;
    MEMORY_HEAP_T memory_heap;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Refreshing local checkpoints of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        if (!doesLocalCheckpointExist(tmp)) {
            // force a restore from previous checkpoint, if needed
            restoreLocalCheckpoint(tmp);
            // same for the .entries file
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }   

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) {
        DIE("Could not load UFX: %s is_per_thread: %d base_dir: %s\n", input_UFX_name, is_per_thread, base_dir);
    }
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more total memory / nodes\n", (lld)minMemory, (lld)size);
    }
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, dynamic_dmin, chunk_size, 1, kmer_len, UFX_f);


#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = UPC_TICKS_TO_SECS(end - start);
        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");
        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
        start = UPC_TICKS_NOW();
    }
#endif

    /* Read contigs and find mean depth for each one */

    gzFile contigFile; // must be gzFile, can be uncompressed
    GZIP_FILE myOutputFile;
    int64_t contigID;
    int is_least;
    char *firstKmer, *lastKmer, *prevKmer, *nextKmer;
    LIST_T copy;
    shared[] LIST_T * lookup_res = NULL;
    int nMers = 0, contigLen, i;
    char prevBase, nextBase, prevCodeL, prevCodeR, nextCodeL, nextCodeR;

    firstKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    firstKmer[kmerLength] = '\0';
    lastKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    lastKmer[kmerLength] = '\0';
    prevKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    prevKmer[kmerLength] = '\0';
    nextKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    nextKmer[kmerLength] = '\0';

    {
        char my_contig_file_name[MAX_FILE_PATH];
        sprintf(my_contig_file_name, "%s.fasta" GZIP_EXT, contig_file_name);
        contigFile = openCheckpoint1(my_contig_file_name, "r");
        if (!contigFile) {
            DIE("Could not open %s\n", my_contig_file_name);
        }
    }
    {
        char my_output_file_name[MAX_FILE_PATH];
        sprintf(my_output_file_name, "%s.cea" GZIP_EXT, contig_file_name);
        myOutputFile = openCheckpoint(my_output_file_name, "w");
    }

    kseq_t *ks = kseq_init(contigFile);
    while (kseq_read(ks) >= 0) {
        /* Read a contig and its length */
        contigID = atol(ks->name.s + 7);

        contigLen = ks->seq.l;
        assert(contigLen >= kmerLength);

        /* Extract the first and prev kmer of the contig */
        memcpy(firstKmer, ks->seq.s, kmerLength * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, firstKmer, &copy, &is_least, kmer_len);
        if (is_least) {
            prevBase = copy.left_ext;
        } else {
            prevBase = reverseComplementBaseExt(copy.right_ext);
        }
        prevKmer[0] = prevBase;

        memcpy(&prevKmer[1], firstKmer, (kmerLength - 1) * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, prevKmer, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (is_least) {
                prevCodeL = copy.left_ext;
                prevCodeR = copy.right_ext;
            } else {
                prevCodeL = reverseComplementBaseExt(copy.right_ext);
                prevCodeR = reverseComplementBaseExt(copy.left_ext);
            }
        } else {
            prevCodeL = '0';
            prevCodeR = '0';
        }

        /* Extract the last and next kmer of the contig */
        memcpy(lastKmer, ks->seq.s + contigLen - kmerLength, kmerLength * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, lastKmer, &copy, &is_least, kmer_len);
        if (is_least) {
            nextBase = copy.right_ext;
        } else {
            nextBase = reverseComplementBaseExt(copy.left_ext);
        }
        nextKmer[kmerLength - 1] = nextBase;
        memcpy(nextKmer, &lastKmer[1], (kmerLength - 1) * sizeof(char));
        lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, nextKmer, &copy, &is_least, kmer_len);
        if (lookup_res != NULL) {
            if (is_least) {
                nextCodeL = copy.left_ext;
                nextCodeR = copy.right_ext;
            } else {
                nextCodeR = reverseComplementBaseExt(copy.left_ext);
                nextCodeL = reverseComplementBaseExt(copy.right_ext);
            }
        } else {
            nextCodeL = '0';
            nextCodeR = '0';
        }

        /* Print to the output file */
        GZIP_PRINTF(myOutputFile, "Contig%lld\t[%c%c]\t(%c)\t%s\t%d\t%s\t(%c)\t[%c%c]\n", (lld)contigID, prevCodeL, prevCodeR, prevBase, firstKmer, contigLen, lastKmer, nextBase, nextCodeL, nextCodeR);
    }

    kseq_destroy(ks);
    closeCheckpoint1(contigFile);
    closeCheckpoint(myOutputFile);
    free_chk(firstKmer);
    free_chk(lastKmer);
    free_chk(prevKmer);
    free_chk(nextKmer);

    UFXClose(UFX_f);


    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);

    upc_barrier;
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        depth_time = UPC_TICKS_TO_SECS(end - start);
        printf("\nTime for analyzing the contig ends : %f seconds\n", depth_time);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
