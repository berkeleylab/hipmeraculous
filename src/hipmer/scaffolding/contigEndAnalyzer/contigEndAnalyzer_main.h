#ifndef CONTIG_END_ANALYZER_H_
#define CONTIG_END_ANALYZER_H_

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>
#include <upc.h>
#include <zlib.h>

#include "cea_dds.h"

#include "optlist.h"
#include "upc_common.h"
#include "common.h"
#include "kseq.h"
#include "Buffer.h"
#include "utils.h"
#include "upc_output.h"

#include "meraculous.h"
#include "../../contigs/contigs_dds.h"
#include "../../contigs/kmer_handling.h"
#include "kmer_hash_cea.h"
#include "../../kcount/readufx.h"

// these are used in buildUFXHashBinary.h and so have to be defined before it is included

#include "buildUFXhashBinary_cea.h"

#define MAX_CONTIG_SIZE (1 << 20)

int contigEndAnalyzer_main(int argc, char **argv);

#endif
