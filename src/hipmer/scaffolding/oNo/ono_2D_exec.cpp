#include "StaticVars.h"
#include "log.h"
#include "common.h"

#include "ono_2D.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("oNo_2D");
    return oNo_2D_main(argc, argv);
}
