// FILENAME:  ono_2D.cpp
// PROGRAMMER: Eugene Goltsman
// DATE:12/11/2017
// REQUIRED:
// PURPOSE:   scaffolder for diploid modes in Meraculous/Hipmer framework

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>


extern "C" {
#include "optlist.h"
}

#include "poc_common.h"
#include "common.h"
#include "log.h"

#include "ono_common.h"
#include "ono_2D.h"


using namespace std;


#define MAX_STRAIN 3  //determines how much two links can overlap before they're deemed a tie collision
#define MAX_DEPTH_DROPOFF 0.8
#define SUSPENDABLE  maxAvgInsSize/2

typedef unordered_map<EndId, vector<EndId> > tieSplitsHashDD_t;
typedef unordered_map<int32_t, int32_t> depthHist_t;

int polymorphicMode;
float peakDepth;
int diploidDepthThld;

tieSplitsHashDD_t tieSplitsDD;
vector<EndId> toRevisit;

void initMyStatics()
{
    polymorphicMode = 0;
    peakDepth = 0.0;
    diploidDepthThld = 0;

    tieSplitsDD.clear();
    toRevisit.clear();
}



namespace ono2D   // funcitons with same names/args as those below exist in other versions of ono, thus the separate namespace
{
void parse_crfFile(string, depthHist_t &);
void parse_srfFile(string, depthHist_t &);
void mark_end(End&, EndMarkCounts&);
bool resolve_tie_collision(const End&, const End&, const End&, int, int);
bool is_dd_collision(const Obj *, const Obj *, const Obj *);
bool resolve_DD_collisions(const End *, Obj *, const End *, vector<ObjectId> *, copyTracker_t&, bool);
void find_bestTies(vector<Obj *>&, tiedEndsHash_t&, tiedEndListsH_t&, unordered_map<Obj *, bool>&, unordered_map<int64_t, int>&, bool);
End *best_tie(const End *, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, copyTracker_t&, bool);
int process_suspend_candidates(suspendedHash_t&, objectsVec_t&, const End *, const End *, copyTracker_t&, bool);
int64_t build_scaffolds_w_mask(endLocks_t&, const string, bool, copyTracker_t&, int);

void parse_crfFile(string crfFile, depthHist_t& depthHist)
{
    // For every contig, create a struct and populate it with contig info, including creation of two new "End" instances
    // Then add this structure to the hash of assembly objects

    bool calculatePeakDepth = (!peakDepth) ? true : false;

    LOGF("Parsing CRF %s\n", crfFile.c_str());

    ifstream infile(crfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", crfFile.c_str());
    }

    string l;
    while (getline(infile, l)) {
        string words[4];
        istringstream stm(l);
        string word;
        int n = 0;
        while (stm >> word && n < 4) { // read white-space delimited tokens one by one
            words[n] = word;
            ++n;
        }
        //    assert( ! words[3].empty());
        assert(n == 4);


        // convert contig names like 'Contig123' to int64
        string cName = words[0];
        ObjectId id;
#ifdef CONTIG_ID_PREFIX
        string prefix = CONTIG_ID_PREFIX;
        cName = cName.substr(prefix.size());
#endif
        id = atol(cName.c_str());
        int len = stoi(words[1].c_str());
        float depth = stof(words[2].c_str());
        Obj newObj(id, len, depth);
        int64_t diplotigId = stol(words[3].c_str());
        if (diplotigId) {
            newObj.label = Obj::DIPLOTIG;
        }



        onoObjectsMap.insert(std::make_pair(id, newObj));
        if (calculatePeakDepth) {
            //add to the the depth histogram, ommiting haplotype variant contigs
            if (depth >= diploidDepthThld) {
                int roundedDepth = int(depth + 0.5);
                depthHist[roundedDepth] += len;
            }
        }
    }

    LOGF("Found %lld onoObjectsMap\n", (lld)onoObjectsMap.size());
}


// scaffs: key = "Scaffold$objectId"
void parse_srfFile(string srfFile, depthHist_t& depthHist)
{
    bool calculatePeakDepth = (!peakDepth) ? true : false;

    LOGF("Parsing SRF %s\n", srfFile.c_str());
    ifstream infile(srfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", srfFile.c_str());
    }

    if( ifsIsEmpty(infile)) {
      DBG("Empty srf file");
      return;
    }
    
    
    string l;
    ObjectId currId = -1;
    Obj __tmpObj(0, 0, 0.0);
    Scaf *_currScaf = NULL;
    Obj *_currObj = NULL;
    int scaffRealBases = -1;
    float scaffDepSum = 0;

    while (getline(infile, l)) {
        string words[6];
        istringstream stm(l);
        string word;
        int n = 0;
        while (stm >> word) { // read white-space delimited tokens one by one
            words[n] = word;
            ++n;
        }
        if (n > 6 || n < 4) {
            DIE("Error:  srf file not in expected format!\n");
        }


        //Scaffold459     CONTIG4 -Contig2948227  11      109     3.000000
        //Scaffold459     GAP4    -97     1
        string scaffName = words[0];

#ifdef SCAFF_ID_PREFIX
        string prefix = SCAFF_ID_PREFIX;
        scaffName = scaffName.substr(prefix.size());
#endif
        ObjectId id = atol(scaffName.c_str());

        if (id != currId) {
            if (_currObj != NULL) {
                // add record to the corresponding hashes and reset totals
                _currObj->depth = scaffDepSum / scaffRealBases;

                // classify the objects
                if (_currScaf->Contigs.size() > 1) {
                    _currObj->label = Obj::TRUE_SCAFFOLD;
                } else {
                    //a singleton scaffold classified based on the contig it contains
                    if (_currScaf->Contigs[0].contName.find(".cp") != std::string::npos) {
                        _currObj->label = Obj::COPY_TIG;
                    }
                }

                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));

                scaffDepSum = 0;
                scaffRealBases = 0;
            }

            // initialize new objects
            _currScaf = new Scaf(id);
            _currObj = &__tmpObj;
            *_currObj = Obj(id, 0, 0.0);
        }

        string indexStr = words[1];
        size_t found = indexStr.find("CONTIG");
        if (found != std::string::npos) {
            char ori = words[2][0];
            string cName = words[2].substr(1);
            int start = atoi(words[3].c_str());
            int end = atoi(words[4].c_str());
            float dep = atof(words[5].c_str());
            int cLen = end - start + 1;
            scaffRealBases += cLen;
            scaffDepSum += float(cLen * dep);

            _currObj->len = end;
            _currScaf->push_contig(cName, ori, start, end, dep);

            if (calculatePeakDepth) {
                //add to the the depth histogram, ommiting haplotype variant contigs
                if (dep >= diploidDepthThld) {
                    int roundedDepth = int(dep + 0.5);
                    if (depthHist.find(roundedDepth) != depthHist.end()) {
                        depthHist[roundedDepth] += cLen;
                    } else {
                        depthHist[roundedDepth] = cLen;
                    }
                }
            }
        } else { // GAP
            int size = atoi(words[2].c_str());
            int uncert = atoi(words[3].c_str());
            _currScaf->push_gap(size, uncert);
        }

        currId = id;
    }

    _currObj->depth = scaffDepSum / scaffRealBases;

    if (_currScaf->Contigs.size() > 1) {
        _currObj->label = Obj::TRUE_SCAFFOLD;
    } else {
        if (_currScaf->Contigs[0].contName.find(".cp") != std::string::npos) {
            _currObj->label = Obj::COPY_TIG;
        }
    }

    scaffInfo.insert(std::make_pair(currId, _currScaf));
    onoObjectsMap.insert(std::make_pair(currId, *_currObj));
}


void mark_end(End& __end, EndMarkCounts& endMarkCounts)
{
    End *_end = &__end;
    
    vector<EndTie> ties = _end->export_all_ties();  // a copy of the vector of end ties since we'll be manipulating it, sorting, etc
    if (ties.empty()) {
        _end->setEndMark(EndLabel::NO_TIES);
        endMarkCounts[EndLabel::NO_TIES]++;
	DBG2N("%lld.%d\tNO_TIES\n", _end->getID(), _end->getPrime());
        return;
    }

    ObjectId objId = _end->getID();
    float myDepth = onoObjectsMap.at(objId).depth;

    for (auto tie : ties) {
        if (!tie.tiedEndId.isValid()) {
            continue;
        }
        End *_tiedEnd_i = &getEnd(tie.tiedEndId);
        ObjectId tiedId = _tiedEnd_i->getID();
        float tiedDepth = onoObjectsMap.at(tiedId).depth;
        if (tiedId == objId) {
            _end->setEndMark(EndLabel::SELF_TIE);
            endMarkCounts[EndLabel::SELF_TIE]++;
	    DBG2N("%lld.%d\tSELF_TIE\n", _end->getID(), _end->getPrime());
            return;
        }
        if ((myDepth - tiedDepth) / peakDepth > MAX_DEPTH_DROPOFF) {
            _end->setEndMark(EndLabel::DEPTH_DROP);
            endMarkCounts[EndLabel::DEPTH_DROP]++;
	    DBG2N("%lld.%d\tDEPTH_DROP\n", _end->getID(), _end->getPrime());
            return;
        }
    }

    // sort ties by distance
    std::sort(ties.begin(), ties.end(), by_dist_len());

    unsigned int nTies = ties.size();
    if (nTies >= 2) {
        for (size_t i = 0; i < nTies - 1; ++i) {
            int64_t ID_i = ties[i].tiedEndId.getID();
            int start_i = ties[i].gapEstimate;
            int end_i = start_i + onoObjectsMap.at(ID_i).len - 1;
            int uncertainty_i = ties[i].gapUncertainty;

            int64_t ID_j = ties[i + 1].tiedEndId.getID();
            int start_j = ties[i + 1].gapEstimate;
            int uncertainty_j = ties[i + 1].gapUncertainty;

            int overlap = end_i - start_j + 1;

            if (overlap > merSize - 2) {
                DBG2N("End %lld.%d --< excessive overlap between ties %s (%d - %d) & %s (%d) ...\n", _end->getID(), _end->getPrime(), ties[i].tiedEndId.toString().c_str(), start_i, end_i, ties[i + 1].tiedEndId.toString().c_str(), start_j);
                int excessOverlap = overlap - (merSize - 2);
                float strain = float(excessOverlap) / float(uncertainty_i + uncertainty_j);
                if (strain > MAX_STRAIN) {
                    DBG2N("\tExcessive strain of %f ...", strain);
                    Obj *_testObj = &onoObjectsMap.at(objId);
                    Obj *_obj_i = &onoObjectsMap.at(ID_i);
                    Obj *_obj_j = &onoObjectsMap.at(ID_j);

                    if (resolve_tie_collision(*_end, getEnd(ties[i].tiedEndId), getEnd(ties[i + 1].tiedEndId), start_i, start_j)) {
                        DBG2N("\tTie collision resolved!\n");
                        _end->setEndMark(EndLabel::UNMARKED);
                        endMarkCounts[EndLabel::UNMARKED]++;
			//			DBG2N("\tUNMARKED\n");
                        return;
                    } else if (polymorphicMode == DIPLOID_HIGH && is_dd_collision(_testObj, _obj_i, _obj_j)) {
                        _testObj->label = Obj::DD_TIG;
                        _obj_i->label = Obj::SD_TIG;
                        _obj_j->label = Obj::SD_TIG;

                        _end->setEndMark(EndLabel::TIE_SPLIT_DD);
                        endMarkCounts[EndLabel::TIE_SPLIT_DD]++;
                        DBG2N("\tTIE_SPLIT_DD\n");
                        tieSplitsDD[_end->getEndId()] = { ties[i].tiedEndId, ties[i + 1].tiedEndId };
                        return;
                    } else {
                        _end->setEndMark(EndLabel::TIE_COLLISION);
                        endMarkCounts[EndLabel::TIE_COLLISION]++;
                        DBG2N("\tTIE_COLLISION: %lld.%d --< %s ? %s\n", _end->getID(), _end->getPrime(), ties[i].tiedEndId.toString().c_str(), ties[i + 1].tiedEndId.toString().c_str());
                        return;
                    }
                }
            }
        }
    }
    _end->setEndMark(EndLabel::UNMARKED);
    endMarkCounts[EndLabel::UNMARKED]++;
    //    DBG2N("\tUNMARKED\n");
    return;
}


bool resolve_tie_collision(const End& _end, const End& _tiedEnd_i, const End& _tiedEnd_j, int start_i, int start_j)
{
    End *_otherEnd_i = &other_end(_tiedEnd_i);
    End *_otherEnd_j = &other_end(_tiedEnd_j);

    //  int64_t ID_i = _tiedEnd_i->objectId;
    ObjectId ID_j = _tiedEnd_j.getID();

    //  int len_i = onoObjectsMap.at(ID_i)->len;
    int len_j = onoObjectsMap.at(ID_j).len;

    // test if the coliding objects are themselves strongly linked, i.e., the strain is false
    int i_j_gapUncert = 0;

    if (_otherEnd_i->get_tie(_tiedEnd_j) != NULL) {
        i_j_gapUncert = _otherEnd_i->get_tie(_tiedEnd_j)->gapUncertainty;
    }
    if (is_splinted(*_otherEnd_i, _tiedEnd_j) || i_j_gapUncert == 1) {      // splint or span with highest certainty
        return true;
    } else if (is_splinted(*_otherEnd_j, _tiedEnd_i)) { // splint suggests that current relative order is wrong (most likely due to bad span-based gap estimate)
        int newGapEstimate = start_j + len_j - (merSize - 2) + 1; // make new distance to i larger than dist to j
        _end.correct_gap_estimate(_tiedEnd_i, newGapEstimate);
        _tiedEnd_i.correct_gap_estimate(_end, newGapEstimate);
        return true;
    }

    // //   if the colision involves a SPAN, and the SPLINTed object is small,  make new distance to  SPAN'ed object larger as we suspect the gap estimate to be negatively biased
    //   if ( is_splinted( _end, _tiedEnd_j)   && ( ! is_splinted( _end, _tiedEnd_i))
    //        && len_j <= (maxAvgInsSize + (2*maxAvgInsSDev)) ) {
    //     int newGapEstimate = start_j + len_j - (merSize-2);
    //     _end->correct_gap_estimate(_tiedEnd_i, newGapEstimate);
    //     _tiedEnd_i->correct_gap_estimate(_end, newGapEstimate);
    //     return true;
    //   }
    //   else if ( is_splinted( _end, _tiedEnd_i)   && ( ! is_splinted( _end, _tiedEnd_j))
    //        && len_i <= (maxAvgInsSize + (2*maxAvgInsSDev)) ) {
    //     int newGapEstimate = start_i + len_i - (merSize-2);
    //     _end->correct_gap_estimate(_tiedEnd_j, newGapEstimate);
    //     _tiedEnd_j->correct_gap_estimate(_end, newGapEstimate);
    //     return true;
    //   }

    return false;
}


bool is_dd_collision(const Obj *testObj, const Obj *obj_i, const Obj *obj_j)
{
    if (testObj->label != Obj::UNLABELED && testObj->label != Obj::DD_TIG) {
        // if object is already labeled and is deemed to be anything other than a diploid-depth contig we don't go any further
        DBG2N("(is_dd_collision) Reject - test object not a DD_TIG\n");
        return false;
    }

    int repeatDepthFactor = 2;
    if (testObj->depth > peakDepth * repeatDepthFactor) { //avoid likely repeat boundaries
        DBG2N("(is_dd_collision) Reject - test object depth suggests a repeat\n");
        return false;
    }

    if ((testObj->depth > peakDepth / 2                          // use lax criteria here: suspect homozygous as long as it's larger than the expected avg haploid depth
         && obj_i->depth < peakDepth && obj_j->depth < peakDepth //suspect variants if both smaller than the expected diploid depth
         && (testObj->depth / obj_i->depth >= 1.75 && testObj->depth / obj_j->depth >= 1.75)
         && (testObj->depth / obj_i->depth <= 2.25 && testObj->depth / obj_j->depth <= 2.25)
         )
        ||
        (
            testObj->depth >= diploidDepthThld
            &&
            (obj_i->label == Obj::DIPLOTIG && obj_j->label == Obj::DIPLOTIG)
        )
        ) {
        return true;
    }

    DBG2N("(is_dd_collision) Doesn't meet qualifications for DD-SD split...\n");

    return false;
}


bool resolve_DD_collisions(const End *_leftAnchorEnd, Obj *myObj, const End *_rightAnchorEnd, vector<ObjectId> *_toSuspend, copyTracker_t& copyTracker, bool existsSrfFile)
{
    /*
     # 1. Resolve the colisions by creating a copy of this object and assigning to it the conflicitng ties that are NOT with the "anchor ends".
     # 2. Update tie info with new ties
     */

    //   if the object is not a suspension candidate it could be anchored only at one end  (_rightAnchorEnd and _toSuspend are NULL)

    int nConflictsResolved = 0;
    int64_t curCopyId = 0;  // temp placeholder

    EndIds endIds(*myObj);

    for (auto _myEndId : endIds.getEnds()) {
        vector<EndId> endsToRetie;
        int nTiesToKeep = 0;
        if (!tieSplitsDD.count(_myEndId)) {
            continue;
        }
        DBG2N("\t DD-SD tie split at end %s:  %s, %s\n", _myEndId.toString().c_str(), tieSplitsDD[_myEndId][0].toString().c_str(), tieSplitsDD[_myEndId][1].toString().c_str());

        for (auto _splitEndId : tieSplitsDD[_myEndId]) {
            ObjectId tiedObjId = _splitEndId.getID();
            // keep any ties to this contig's anchors or to any other suspension candidates
            if (_splitEndId == _leftAnchorEnd->getEndId() ||
                (_rightAnchorEnd != NULL && _splitEndId == _rightAnchorEnd->getEndId())) {
                nTiesToKeep++;
            } else if (_toSuspend != NULL &&
                       std::find((*_toSuspend).begin(), (*_toSuspend).end(), tiedObjId) != (*_toSuspend).end()) {
                nTiesToKeep++;
            } else {
                endsToRetie.push_back(_splitEndId);
            }
        }
        //for this to be a valid split, at least one tie should belong here and at least one should be reassigned
        if (!(nTiesToKeep && endsToRetie.size())) {
            DBG2N("\t\t ignoring tie conflict at end %s.\n", _myEndId.toString().c_str());
            continue;
        } else {
            DBG2N("\t\t resolving via copy-object creation + tie re-assignment: \n");
        }

        if (!curCopyId) {
            ObjectId myObjId = myObj->id;
            curCopyId = create_copy_object(myObj, copyTracker, existsSrfFile);
            myObj = &onoObjectsMap.at(myObjId); // map may have changed, so re-acquire the pointer
        }

        Obj *_copyObj = &onoObjectsMap.at(curCopyId);
        End *_copyEnd = (_myEndId.getPrime() == 5) ?  &(_copyObj->end5) : &(_copyObj->end3);
	EndId _copyEndId = _copyEnd->getEndId();
	  
        for (auto _tiedEndId : endsToRetie) {
            // untie this end from original object and tie it to the copy-object
	    DBG2N("\t\t re-tying %s with %s ...\n", _tiedEndId.toString().c_str(), _copyEndId.toString().c_str());
	    reassign_tie_info(&getEnd(_myEndId), _copyEnd, &getEnd(_tiedEndId));
        }

        nConflictsResolved++;
        tieSplitsDD.erase(_myEndId);
        myObj->label = Obj::COPY_TIG;
        myObj->depth /= 2;
        _copyObj->depth = myObj->depth;
        getEnd(_myEndId).setEndMark(EndLabel::UNMARKED);

        // Since we've resolved the collision we will later re-attempt finding the end's best ties
	toRevisit.push_back(_myEndId);
	toRevisit.push_back(_copyEnd->getEndId());
    }

    if (nConflictsResolved == 0) {
        DBG2N("\t No conflicts could be resolved.\n");
        return false;
    }

    return true;
}



void find_bestTies(sortedBy_t& sortedByLen, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyObjTracker, bool existsSrfFile)
{
    // Super-function to evaluate all size-sorted objects' ends and secure a "best tie"
    // Pieces can be inserted between earlier established bestTies (i.e. suspensions) and/or copied in the process.


    LOGF("Finding \"best ties\" (%lld)...\n", (lld)sortedByLen.size());

    int64_t count = 0, progressCt = sortedByLen.size() / 100;
    for (auto tmp : sortedByLen) {
        ObjectId id = tmp.id;

        if (progressCt && ++count % progressCt == 0) {
            LOGF("find_bestTies: %lld of %lld (%0.3f %%) object %lld\n", (lld)count, (lld)sortedByLen.size(), 100.0 * count / sortedByLen.size(), (lld)id);
        }
        DBG2N(" === object %lld\n\n", (lld)id);

        if (suspended.count(id)) {
            DBG2N(" -ineligible (suspended)\n");
            continue;
        }

        Obj *piece = &onoObjectsMap.at(id);

        EndIds ends(*piece);
        for (auto _endId : ends.getEnds()) {
            DBG2N("end %d:\n", _endId.getPrime());

            // Initially only unmarked ends are allowed to have best ties.
            if (_endId.getEndMark() == EndLabel::UNMARKED) {
                End *_bestTie = best_tie(&getEnd(_endId), bestTies, bestTiedBy, suspended, copyObjTracker, existsSrfFile);
                if (_bestTie != NULL) {
                    bestTies.insert(std::pair<EndId, EndId>(_endId, _bestTie->getEndId()));
                    bestTiedBy[_bestTie->getEndId()].push_back(_endId);
                    DBG2N("BEST_TIE: [%s]\t%s\n\n", _bestTie->toString().c_str(), _endId.toString().c_str());
                } else {
                    DBG2N("BEST_TIE: [%d]\t%s\n\n", _endId.getEndMark(), _endId.toString().c_str());
                }
            } else {
                DBG2N("BEST_TIE: [%d]\t%s\n\n", _endId.getEndMark(), _endId.toString().c_str());
            }
        }
    }


    // re-try finding best ties on those ends that have been skipped but which ended up being tie targets and hich had their collision conflicts resolved
    if (polymorphicMode == DIPLOID_HIGH && !toRevisit.empty()) {
      LOGF("Retrying best ties on disambiguated ends... \n\n");
        for (auto & _endId : toRevisit) {
            DBG2N("end: %s\n", _endId.toString().c_str());

            ObjectId id = _endId.getID();
            if (suspended.find(id) != suspended.end()) {
                continue;
            }

            bool alreadyBestTied = false;
            for (auto &it : bestTies) {
                if (it.first == _endId) {
                    DBG2N("BEST_TIE: (pre-existing) [%s]\t%s\n\n", it.second.toString().c_str(), _endId.toString().c_str());
                    alreadyBestTied = true;
                    break;
                }
            }
            if (alreadyBestTied) {
                continue;
            }


            End *_bestTie = best_tie(&getEnd(_endId), bestTies, bestTiedBy, suspended, copyObjTracker, existsSrfFile);
            if (_bestTie != NULL) {
                EndId _bestTieId = _bestTie->getEndId();
                bestTies.insert(std::pair<EndId, EndId>(_endId, _bestTieId));
                bestTiedBy[_bestTieId].push_back(_endId);
                DBG2N("BEST_TIE: [%s]\t%s\n\n", _bestTieId.toString().c_str(), _endId.toString().c_str());
            }
        }
    }

    //  Insert suspended pieces where possible
    place_suspended(suspended, bestTies, bestTiedBy, copyObjTracker, existsSrfFile);
}


End *best_tie(const End *_end, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyTracker, bool existsSrfFile)
{
    Obj *obj = &onoObjectsMap.at(_end->getID());
    bool largeObject = (obj->len > SUSPENDABLE) ? true : false;
    if (largeObject)
      DBG2N("\tLarge object (%d > %d).\n ", obj->len, (SUSPENDABLE));
    int nSuspended = 0;

    vector<EndTie> goodTies;
    for (auto tie : _end->ties) {
        EndLabel::Label tieMark = getEnd(tie.tiedEndId).getEndMark();
        if (tieMark != EndLabel::UNMARKED && tieMark != EndLabel::TIE_SPLIT_DD) {
            // Skip ties to previously marked ends  unless they're split DD-SD type ends - these we will try to resolve
	    DBG2N("\ttied end  %s marked as %s; SKIP..\n", tie.tiedEndId.toString().c_str(), tieMark);  
            continue;
        }
        int64_t obj_i_id = tie.tiedEndId.getID();
        if (suspended.count(obj_i_id)) {
            //Skip ties to suspended objects
	    DBG2N("\ttie %s is on a suspended object - SKIP \n", tie.tiedEndId.toString().c_str());  
            continue;
        }
        DBG2N("\tgood tie: %s\n", tie.tiedEndId.toString().c_str());
        goodTies.push_back(tie);
    }
    if (goodTies.empty()) {
        DBG2N("noGoodTies\n");
        return NULL;
    }

    std::sort(goodTies.begin(), goodTies.end(), by_dist_len());

    objectsVec_t toSuspend;

    // If a large object, return the closest large object if one exists.
    // Small objects may be suspended between large objects
    if (largeObject) {
        End *_closestLarge = NULL;
        for (auto tie : goodTies) {
            EndId _te = tie.getEndId();
            End *_te_end = &getEnd(_te);
            ObjectId obj_id = _te.getID();
            Obj *obj_i = &onoObjectsMap.at(obj_id);
            if (obj_i->len > SUSPENDABLE && _te_end->getEndMark() == EndLabel::UNMARKED) {
                _closestLarge = _te_end;
                break;
            } else {
                toSuspend.push_back(obj_id);
            }
        }
        if (_closestLarge) {
            if (toSuspend.size()) {
                nSuspended = process_suspend_candidates(suspended, toSuspend, _end, _closestLarge, copyTracker, existsSrfFile);
            }
            DBG2N("BT return: closestLarge (%lld suspended)\n", (lld)nSuspended);
            return _closestLarge;
        }
        DBG2N("BT: no closest large found\n");
    }

    //Return the closest extendable object if one exists.  Objects must be unmarked to qualify. Unextendable objects may be suspended
    End *_closestExtendable = NULL;
    toSuspend.clear();
    for (EndTie tie : goodTies) {
        End *_te = &tie.getTiedEnd();
        End *_otherEnd = &other_end(*_te);
        ObjectId obj_id = _te->getID();
        Obj *obj_i = &onoObjectsMap.at(obj_id);
        if (_te->getEndMark() != EndLabel::UNMARKED || _otherEnd->getEndMark() != EndLabel::UNMARKED) {
            /*
             # Note for diploid_mode 2: objects with a DD_TIE_SPLIT are a high risk for haplotype crossover, even after the split is resolved.
             # We suspend these, but the suspensions will remain unplaced if bridging best ties 
             #
             */
            toSuspend.push_back(obj_id);
        } else {
            _closestExtendable = _te;
            break;
        }
    }
    if (_closestExtendable) {
        if (toSuspend.size()) {
            nSuspended = process_suspend_candidates(suspended, toSuspend, _end, _closestExtendable, copyTracker, existsSrfFile);
        }
        DBG2N("BT return: closestExtendable (%lld suspended)\n", (lld)nSuspended);
        return _closestExtendable;
    }
    DBG2N("BT: no closest extendable found\n");

    //Just return the closest object
    toSuspend.clear();
    End *_closestEnd = &getEnd(goodTies[0]);
    DBG2N("\t%s.. is closest\n", _closestEnd->toString().c_str());
    if (polymorphicMode == DIPLOID_HIGH) {
        ObjectId closestEndObjectId = _closestEnd->getID();
        Obj *closestObj = &onoObjectsMap.at(closestEndObjectId);
        End *_testEnd = (_closestEnd->getPrime() == 5) ? &(closestObj->end3) : &(closestObj->end5);
        if (_testEnd->getEndMark() == EndLabel::TIE_SPLIT_DD) {
            //reject if other end is a DD-SD tie split (we already know we can't suspend it)
            return NULL;
        } else if (closestObj->label == Obj::DD_TIG && _closestEnd->getEndMark() == EndLabel::TIE_SPLIT_DD) {
            //  DD-SD tie split: try to resolve it
            //  Note: the DD-split may be wrt other nodes, in which case this collision won't be resolved, but we still don't want to make BTs to this end
            bool resolved = false;
            resolved = resolve_DD_collisions(_end, closestObj, NULL, NULL, copyTracker, existsSrfFile);
            if (!resolved) {
                DBG2N("\t\t\t Couldn't resolve DD-SD tie collision on %s \n", _closestEnd->toString().c_str());
                return NULL;
            }
        }
    }
    DBG2N("BT return: closest\n");
    return _closestEnd;
}


int process_suspend_candidates(suspendedHash_t& suspended, objectsVec_t &_toSuspend, const End *_end, const End *_otherEnd, copyTracker_t& copyTracker, bool existsSrfFile)
{
    int nSusp = 0;

    for (auto _objId : _toSuspend) {
        Obj *_obj = &onoObjectsMap.at(_objId);
        if (polymorphicMode == DIPLOID_HIGH && _obj->label == Obj::DD_TIG) {
	  /* By definition, DD_TIG object must have a DD_collision on at least one end;  attempt to resolve the collisions */
	  /***ok to fail since multiple insertion of the same suspeded piece are alloed in place_suspended() ***/
	  
	  resolve_DD_collisions(_end, _obj, _otherEnd, &_toSuspend, copyTracker, existsSrfFile);
	}
	suspended.insert(std::make_pair(_objId, true));
	DBG2N("BT: suspended object %lld\n", (lld)_objId);
	nSusp++;
    }
    return nSusp;
}



int64_t build_scaffolds_w_mask(endLocks_t& endLocks, const string srfOutFilename, bool existsSrfFile, copyTracker_t& copyTracker, int srfOffset)
{
    enum PathFlag { PATH_CONVERGENCE, PATH_DIVERGENCE, PATH_EXTENSION, PATH_TERMINATION };

    // we use the objectId as the key for local hashes where an Object is the key
    std::unordered_set<ObjectId> visitedObjects;
    visitedObjects.reserve(onoObjectsMap.size());

    LOGF("build_scaffolds %lld\n", (lld)onoObjectsMap.size());
    int64_t count = 0, progressCt = onoObjectsMap.size() / 100;
    ofstream srfOut;
    srfOut.open(srfOutFilename);
    assert(srfOut.is_open());


    // Initialize mask file - a list of contigs to pass as an 'exclude' list to next round's splinter/spanner
    // The list consists of copy-tigs and DD-tigs which have made it into a scaffold of >1 contiga.
    char maskOutFilename[255];
    sprintf(maskOutFilename, "%s.contigs_mask", srfOutFilename.c_str());
    ofstream maskOut;
    maskOut.open(maskOutFilename);
    assert(maskOut.is_open());

    int initBuffer = 8192;
    string newScaffReport;
    newScaffReport.reserve(initBuffer);

    long unsigned int newScaffoldId = srfOffset;
    for (auto objPair : onoObjectsMap) {
        if (progressCt && ++count % progressCt == 0) {
            LOGF("building %lld of %lld %0.1f %%\n", (lld)count, (lld)onoObjectsMap.size(), 100.0 * count / onoObjectsMap.size());
        }
        auto obj = &objPair.second;
        if (visitedObjects.find(obj->id) != visitedObjects.end()) {
            continue;
        }

        End *end = &obj->end5;
        PathFlag preState = PATH_TERMINATION;

        std::unordered_set<ObjectId> loopCheck;
        while (true) {
            if (loopCheck.insert(end->getID()).second == false) {
                break;
            }

            if (endLocks.find(end->endId) != endLocks.end()) {
                auto next = endLocks[end->endId];
                EndId::Flag flag = next.getFlag();
                if (flag == EndId::FLAG_CONVERGENCE || flag == EndId::FLAG_DIVERGENCE) {
                    preState = flag == EndId::FLAG_CONVERGENCE ? PATH_CONVERGENCE : PATH_DIVERGENCE;
                } else {
                    assert(flag == EndId::FLAG_NONE); // End should be a valid obj with no special flags

                    End *tmpend = &getEnd(next);
                    end = &other_end(*tmpend);

                    preState = PATH_EXTENSION;
                }
            } else {
                preState = PATH_TERMINATION;
            }
        }

        // at this point:
        // preState = TERMINATION / DIVERGENCE / CONVERGENCE
        // end->getID() points to the last valid End visited
        auto nextObject = &onoObjectsMap.at(end->getID());

        loopCheck.clear();

        DBG2("Scaffold%lld :: preState %d; Contig%lld (%d) ", (lld)newScaffoldId, preState, (lld)end->getID(), end->getPrime());

        int inScaffold = 0;
        int nextEndPrime = end->getPrime() == 3 ? 5 : 3;
        int nextGap = 0;
        int nextGapUncertainty = 0;
        auto prevObject = nextObject;
        auto prevEndPrime = nextEndPrime;

        long scaffCoord = 1;
        long scaffContigIndex = 1;

        bool toMask;
        vector<string> maskList = {}; // list of DD-tigs that were incorporated into a larger scaffold   OR copy-tigs.  Will be masked out in later ono rounds (i.e. it won't contribute to any new ties)


        while (true) {
            if (loopCheck.insert(nextObject->id).second == false) {
                DBG2("LOOP\n");
                break;
            }
            //			DBG2("\tContig%lld\n", (lld) nextObject->id);
            visitedObjects.insert(nextObject->id);
            unsigned long nextObjectLen = nextObject->len;

            char objectOri = '+';
            if (nextEndPrime == 5) {
                objectOri = '-';
            }

            if (inScaffold) {
                newScaffReport += "Scaffold";
                newScaffReport += std::to_string(newScaffoldId);
                newScaffReport += "\tGAP";
                newScaffReport += std::to_string(scaffContigIndex);
                newScaffReport += "\t";
                newScaffReport += std::to_string(nextGap);
                newScaffReport += "\t";
                newScaffReport += std::to_string(nextGapUncertainty);
                newScaffReport += "\n";

                scaffCoord += nextGap;
                scaffContigIndex++;
            }

            toMask = (nextObject->label == Obj::DD_TIG || nextObject->label == Obj::COPY_TIG) ? true : false;

            if (existsSrfFile) {
                // objects are scaffolds from previous srf
                Scaf nextObjectScaffInfo = *scaffInfo.at(nextObject->id); //shallow cp
                if (objectOri == '-') {
                    nextObjectScaffInfo.reverse_order_ori();
                }
                bool is_a_copy = false;
                int cp_ver;
                if (nextObject->id < 0) { // a copy-object: print all contigs as 'parent_id.cp[version]'
                    is_a_copy = true;
                    cp_ver = copyTracker[nextObject->id];
                }

                // correct coordinates according to new scaffold layout
                for (size_t idx = 0; idx < nextObjectScaffInfo.Contigs.size(); ++idx) {
                    Scaf::contig_t *c = &nextObjectScaffInfo.Contigs[idx];
                    int cLen = c->endCoord - c->startCoord + 1;
                    c->startCoord = scaffCoord;
                    c->endCoord = scaffCoord + cLen - 1;
                    scaffCoord += cLen;

                    if (toMask) {
		      if (is_a_copy) {
			char  cpTigName[32];
			sprintf(cpTigName, "%s.cp%d", c->contName.c_str(), cp_ver);
			maskList.push_back(cpTigName);
		      }else {
			maskList.push_back(c->contName);
		      }
                    }

                    // print CONTIG line
                    newScaffReport += "Scaffold" + std::to_string(newScaffoldId) + "\tCONTIG" + std::to_string(scaffContigIndex) + "\t";
                    if (is_a_copy) {
                        newScaffReport += nextObjectScaffInfo.form_copyTig_string(idx, cp_ver);
                    } else {
                        newScaffReport += nextObjectScaffInfo.form_contig_string(idx);
                    }
                    // print GAP line
                    if (&nextObjectScaffInfo.Contigs[idx] != &nextObjectScaffInfo.Contigs.back()) {
                        newScaffReport += "Scaffold" + std::to_string(newScaffoldId) + "\tGAP" + std::to_string(scaffContigIndex) + "\t";
                        Scaf::gap_t *g = &nextObjectScaffInfo.Gaps[idx];
                        scaffCoord += g->gapSize;
                        newScaffReport += nextObjectScaffInfo.form_gap_string(idx);

                        scaffContigIndex++;
                    }
                }
            } else { // objects are contigs
                auto contigOri = objectOri;
                auto contigStartCoord = scaffCoord;
                auto contigEndCoord = scaffCoord + nextObjectLen - 1;
                auto depth = nextObject->depth;

                char contigOutName[32];
                //string contigOutName = "";
                const char *prefix = "";
#ifdef CONTIG_ID_PREFIX
                prefix = CONTIG_ID_PREFIX;
                //contigOutName = CONTIG_ID_PREFIX;
#endif
                if (nextObject->id < 0) { //copy-object - print out as 'parent_id.cp[version]'
                    sprintf(contigOutName, "%s%lld.cp%d", prefix, (lld)nextObject->parentId, copyTracker[nextObject->id]);
                    //string copyVersion = std::to_string(copyTracker[nextObject->id]);
                    //contigOutName += std::to_string(nextObject->parentId) + ".cp" + copyVersion;
                } else {
                    sprintf(contigOutName, "%s%lld", prefix, (lld)nextObject->id);
                    //contigOutName += std::to_string(nextObject->id);
                }

                if (toMask) {
                    maskList.push_back(contigOutName);
                }


                newScaffReport += "Scaffold";
                newScaffReport += std::to_string(newScaffoldId);
                newScaffReport += "\t";
                newScaffReport += "CONTIG";
                newScaffReport += std::to_string(scaffContigIndex);
                newScaffReport += "\t";
                newScaffReport += contigOri;
                newScaffReport += contigOutName;
                newScaffReport += "\t";
                newScaffReport += std::to_string(contigStartCoord);
                newScaffReport += "\t";
                newScaffReport += std::to_string(contigEndCoord);
                newScaffReport += "\t";
                newScaffReport += std::to_string(depth);
                newScaffReport += "\n";

                scaffCoord += nextObjectLen;
            }

            inScaffold = true;
            auto _end = nextObject->getEnd(nextEndPrime);

            if (endLocks.find(_end.endId) != endLocks.end()) {
                auto next = endLocks[_end.endId];
                EndId::Flag flag = next.getFlag();
                if (flag == EndId::FLAG_CONVERGENCE || flag == EndId::FLAG_DIVERGENCE) {
                    if (maskList.size() &&
                        (!(scaffContigIndex == 1 && nextObject->label == Obj::DD_TIG))) {
                        /* don't print the mask if the DD-tig object din't get incorporated into a larger scaffold, i.e. we will still consider it's conflicting links in the next round */
                        for (const auto &c : maskList) {
                            maskOut << c << "\n";
                        }
                    }

                    break;
                } else {
                    prevObject = nextObject;
                    prevEndPrime = nextEndPrime;

                    nextObject = &onoObjectsMap.at(next.getID());
                    auto nextTie = nextObject->getEnd(next.getPrime()).get_tie(_end.endId);
                    nextGap = nextTie->gapEstimate;
                    nextGapUncertainty = nextTie->gapUncertainty;
                    nextEndPrime = next.getPrime();
                    nextEndPrime = (5 == nextEndPrime) ? 3 : 5;
                }
            } else {
                DBG2N("Scaffold%lld :: TERMINATION; Contig%lld (%d) \n", (lld)newScaffoldId, (lld)nextObject->id, nextEndPrime);
                if (maskList.size() &&
                    (!(scaffContigIndex == 1 && nextObject->label == Obj::DD_TIG))) {
                    /* don't print the mask if the DD-tig object din't get incorporated into a larger scaffold, i.e. we will still consider it's conflicting links in the next round  */
                    for (const auto &c : maskList) {
                        maskOut << c << "\n";
                    }
                }

                break;
            }
        }

        if (newScaffReport.size() > initBuffer / 2) {
            srfOut << newScaffReport;
            newScaffReport.clear();
        }
        newScaffoldId++;
    }

    if (newScaffReport.size() > 0) {
        srfOut << newScaffReport;
        newScaffReport.clear();
    }

    maskOut.close();

    return newScaffoldId - 1;
}
} //namespace ono2D


int oNo_2D_main(int argc, char **argv)
{
    using namespace std;
    using namespace ono2D;

    LOG_VERSION;
    initCommonStatics();
    initMyStatics();

    if (argc < 2) {
    SWARN("Usage: ./ono_2D <-m merSize> <-l linkDataFile> (<-s scaffoldReportFile> || <-c contigReportFile>)  <-o scaffOutFile> <<-p pairThreshold>> <<-d peakDepth>> <<-D diploidDepthThld>> <<-M polymorphicMode [1|2]>> <<-O IDoffset>> \n");
    return 1;
    }

    option_t *optList, *_optList, *thisOpt;
    _optList = GetOptList(argc, argv, "m:l:p:d:s:c:o:O:D:M:V");
    optList = _optList;


    char *linkDataFile = NULL;
    char *srfFile = NULL;
    char *crfFile = NULL;
    char *srfOutFile = NULL;
    int pairThreshold = 2;
    int srfOffset = 0;
    bool existsSrfFile = false;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'm':
            merSize = atoi(thisOpt->argument);
            break;
        case 'l':
            linkDataFile = thisOpt->argument;
            break;
        case 'p':
            pairThreshold = atoi(thisOpt->argument);
            break;
        case 'd':
            peakDepth = atof(thisOpt->argument);
            break;
        case 'D':
            diploidDepthThld = atoi(thisOpt->argument);
            break;
        case 's':
            srfFile = thisOpt->argument;
            existsSrfFile = true;
            break;
        case 'c':
            crfFile = thisOpt->argument;
            break;
        case 'o':
            srfOutFile = thisOpt->argument;
            break;
        case 'O':
            srfOffset = atoi(thisOpt->argument);
            break;
        case 'M':
            polymorphicMode = atoi(thisOpt->argument);
            break;
        }
    }

    if (polymorphicMode < 1 || polymorphicMode > 2) {
        SWARN("You must specify a valid diploid mode [ -M 1|2 ]\n");
        return 1;
    }

    if (!merSize) {
        SWARN("You must specify a -m merSize!\n");
        return 1;
    }
    if (linkDataFile == NULL) {
        SWARN("You must specify a -l linkDataFile!\n");
        return 1;
    }
    if (srfFile == NULL && crfFile == NULL) {
        SWARN("You must specify either -c crfFile or -s srfFile  inputs!\n");
        return 1;
    }
    if (srfOutFile == NULL) {
        SWARN("You must specify an output file!\n");
        return 1;
    }

    unordered_map<string, libInfo_t> libInfoHash;

    vector<string> libMetaData;

    ifstream infile(linkDataFile);
    string line;
    while (getline(infile, line)) {
      // WARNING: a single library can be listed twice - for spans and for splints, in which case it will have different insSize values!  (we use readLength as insert size for splints).  That's ok, since the same combined link file is given for both, and we don't use insert sizes beyond this point.
      
        libInfo_t libInfo;
        istringstream stm(line);
        string word;
        while (stm >> word) { // read white-space delimited tokens one by one
            libMetaData.push_back(word);
        }
        string libName = libMetaData[0];
        libInfo.insSize = stoi(libMetaData[1]);
        libInfo.sdev = stoi(libMetaData[2]);
        libInfo.linkFile = libMetaData[3];

	if (!libInfoHash.count(libName))
	  libInfoHash.insert({ libName, libInfo });

        if (libInfo.insSize > maxAvgInsSize) {
            maxAvgInsSize = libInfo.insSize;
            maxAvgInsSDev = libInfo.sdev;
        }
	libMetaData.clear();
    }

    LOGF("maxAvgInsSize picked as %d\n", maxAvgInsSize);

    // load contig/scaffold info

    depthHist_t depthHist;

    if (crfFile != NULL) {
        parse_crfFile(string(crfFile), depthHist);
    } else if (srfFile != NULL) {
        parse_srfFile(string(srfFile), depthHist);
    }


    if (!onoObjectsMap.size()) { // Not a fatal error since parCC is not guaranteed to produce a CC for every thread;  Write empty srf file and exit;
        FreeOptList(_optList);
        LOGF("No assembly objects found in the input;  \n");
        ofstream srfOut_empty(srfOutFile);
	if ( polymorphicMode == 2 ) {
	  char maskOut_empty[255];
	  sprintf(maskOut_empty, "%s.contigs_mask", srfOutFile);
	  ofstream maskOut;
	}
	
        //  FLUSH_MY_LOG;
        return 0;
    }


    if (!peakDepth) {
        peakDepth = find_peak_depth(depthHist);
    }

    if (polymorphicMode == DIPLOID_HIGH) {
        // test if the homozygous peak depth is within 20% of expected diploid peak (peak = 2*$diploidDepthCutoff/1.5)
        float expectDipDepthLow = 0.8 * (1.33 * diploidDepthThld);
        float expectDipDepthHigh = 1.2 * (1.33 * diploidDepthThld);
        if (peakDepth < expectDipDepthLow || peakDepth > expectDipDepthHigh) {
            float obsPeakD = peakDepth;
            peakDepth = diploidDepthThld * 1.33;
            LOGF("WARNING: Observed modal homozygous depth (%f) is too far off from the expected (2*diploidDepthCutoff/1.5), possibly due to scarcity of homozygous contigs. Will use an approximation..\n", obsPeakD);
        }
    }

    LOGF("Modal depth picked as %f\n", peakDepth);


    //Build the linkage and then ties streaming
    int64_t numLinks = 0;
    int maxUncertainty = 1000000;
    unordered_map<string,bool> seen;
    for (std::pair<string, libInfo_t> element : libInfoHash) {
        string libName = element.first;
        string linkFile = libInfoHash[libName].linkFile;
	if (!seen.count(linkFile)) { //multiple libs -  the same link file
	  numLinks += parse_linkFile_and_build_ties(linkFile, pairThreshold, maxUncertainty, existsSrfFile);
	  seen.insert({linkFile, true});
	}
    }

    
    LOGF("There are %lld ojbects loaded now %0.3f MB and %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj) / ONE_MB, 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    if (scaffInfo.size()) {
        LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    }


    // build a vector of pointers to objects, sorted by sequence length
    sortedBy_t sortedByLen;
    sortedByLen.reserve(onoObjectsMap.size());

    for (auto kv : onoObjectsMap) {
        sortedByLen.push_back(kv.second);
    }

    std::sort(sortedByLen.begin(), sortedByLen.end(), by_length_depth());

    // mark object ends
    int nObjects = sortedByLen.size();
    EndMarkCounts endMarkCounts = {};
    LOGF("Marking ends... (%d objects)\n", nObjects);
    
    for (auto & i : sortedByLen) {
        Obj *obj = &onoObjectsMap.at(i.id);
        mark_end(obj->end5, endMarkCounts);
        mark_end(obj->end3, endMarkCounts);
    }


    // # find a "best tie" for every end.
    tiedEndsHash_t bestTie;     //stores End_sp => this end's bestTie ptr
    tiedEndListsH_t bestTiedBy; // stores End_sp => list of ends that claim this end as their bestTie ptr
    suspendedHash_t suspended;
    copyTracker_t copyObjTracker;
    find_bestTies(sortedByLen, bestTie, bestTiedBy, suspended, copyObjTracker, NULL != srfFile);


    // # Lock ends together with no competing ties
    endLocks_t endLocks;
    lock_ends(endLocks, bestTie, bestTiedBy, suspended, onoObjectsMap);




    // # Traverse end locks to build scaffolds

    int64_t nScaffolds;
    if (polymorphicMode == DIPLOID_HIGH) {
        nScaffolds = ono2D::build_scaffolds_w_mask(endLocks, string(srfOutFile), NULL != srfFile, copyObjTracker, srfOffset);
    } else {
        nScaffolds = build_scaffolds(endLocks, string(srfOutFile), NULL != srfFile, copyObjTracker, srfOffset);
    }
    LOGF("Total of %lld scaffolds made\n", (lld)nScaffolds);



    for (int i = 0; i < EndLabel::MAX_END_LABEL; i++) {
        LOGF("Ends marked %s: %d\n", EndLabel::getName(i).c_str(), endMarkCounts[i]);
    }
    DBG("nr of best ties: %lld\n", (lld)bestTie.size());
    DBG("nr of copy-objects: %lld\n", (lld)copyObjTracker.size());
    DBG("nr of scaffolds: %lld\n", (lld)nScaffolds);


    // Cleanup

    LOGF("Cleaning up data structures\n");
    tiedEndsHash_t().swap(bestTie);
    tiedEndListsH_t().swap(bestTiedBy);
    suspendedHash_t().swap(suspended);
    endLocks_t().swap(endLocks);

    LOGF("Cleaning %lld ojbects loaded now %0.3f MB and %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj) / ONE_MB, 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    objectsHash_t().swap(onoObjectsMap);

    LOGF("Cleaning %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    for (auto &sc : scaffInfo) {
        delete sc.second;
    }
    scaffsHash_t().swap(scaffInfo);

    FreeOptList(_optList);
    LOGF("Done\n");
    FLUSH_MY_LOG;

    // END OF MAIN

    return 0;
}
