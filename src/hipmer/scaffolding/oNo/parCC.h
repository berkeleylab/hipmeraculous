#ifndef _PARCC_H
#define _PARCC_H

#include <stdio.h>
#include <upc.h>
#include <upc_collective.h>
#include <upc_tick.h>
#include <string.h>
#include <time.h>
#include <wordexp.h>

#ifdef HIPMER_EMBED_HMMER
#include "lib_nhmmer.h"
#endif

#include <assert.h>
#include "optlist.h"
#include "version.h"
#include "upc_common.h"
#include "common.h"
#include "utils.h"
#include "defines.h"
#include "timers.h"
#include "../../main/exec_utils.h"
#include "ono_meta.h"
#include "ono_2D.h"
#include "ono4.h"
#include "ono_CCS.h"


#define SPLINT 0
#define SPAN 1
#define CHUNK_SIZE 100
#define NON_LABEL (-1)
#define ALL_LABEL (THREADS)
#define MAX_NAME_SIZE 255

//#define DUMP_CONN_STATS

#include "common.h"
#include "upc_common.h"
//#include "oNoUtils.h"
#include "defines.h"
#include "../../common/Buffer.h"
#include "string.h"
#include "upc_output.h"
#include "SharedBuffer.h"

typedef struct tuple_t {
    int64_t x;
    int64_t y;
} tuple_t;

typedef struct link_t {
    int64_t v1;
    int64_t v2;
    char    end1;
    char    end2;
    char    type;
    int64_t f1;
    int64_t f2;
    int64_t f3;
    int64_t f4;
    int64_t f5;
} link_t;

typedef struct link_cc_t {
    int64_t n_links;
    int64_t cur_pos;
    shared[] link_t * link_list;
} link_cc_t;

typedef struct asmObj_t {
  /*Assembly object - to be used ubiquitously to store input contig/scaffold infor (there's no distinction as far as parCC is concerned) */
  int length; 
  float depth;
  int32_t n_links3, n_links5;
  int diplotig;  // in diploid mode this stores the diplotig that this object belongs to;  otherwise initialize to -1
} asmObj_t;
  
typedef shared [] char *SharedCharArrPtr;

typedef struct SharedString {
  SharedCharArrPtr str;
  int32_t len;
} SharedString;

typedef shared [1] SharedString *SharedStringArrPtr;


char **split_commandline(const char *cmdline, int *argc);

void free_split_commandline(int *argc, char **argv);

#define END_OF_LIST (-1)
#define EXCLUDED_VERTEX (-2)

typedef shared[] tuple_t *sharedTuplePtr;
typedef shared[] link_t *sharedLinkPtr;
typedef shared[] int64_t *sharedLongPtr;
    
typedef int64_t *LongPtr;

typedef struct reverse_map_t {
    int64_t          nEntries;
    int64_t          cur_pos;
    sharedLongPtr    members_list;
} reverse_map_t;

typedef struct reverse_maps_t {
    shared [1] reverse_map_t  *reverse_map;
    int64_t                    largest_CC_ID;
    int64_t                    largest_CC_size;
    shared [1] reverse_map_t  *largest_CC_reverse_map;        
} reverse_maps_t;

typedef struct link_db_t {
    int64_t       nEntries;
    int64_t       cur_pos;
    sharedLinkPtr link_list;
} link_db_t;

typedef struct hmm_t {
    int64_t contig_id;
    int64_t start;
    int64_t end;
    char    strand;
} hmm_t;

typedef struct hmm_db_t {
    int64_t n_entries;
    /* FIXME: Correct handling of HMM input files */
    int64_t cur_pos;
    shared[] hmm_t * hmm_list;
} hmm_db_t;

typedef struct libInfoType {
    int  insertSize;
    int  stdDev;
    char libName[LIB_NAME_LEN];
    char linkFileNamePrefix[MAX_NAME_SIZE];
} libInfoType;


int splitLinkMetaLine(char *line, libInfoType *libInfo);

int parse_hmm_line(char *hmm_line, int64_t *hmm_contig, int64_t *hmm_start, int64_t *hmm_end, char *strand);

int parse_link_line(char *line, int64_t *vertex1, int64_t *vertex2, char *end1_s, char *end2_s);

int parse_full_link_line(char *line, int64_t *vertex1, int64_t *vertex2, link_t *result_link);

// returns 0 when it is okay to skip already processed outputs, 1 if ono must be rerun
int find_connected_components(GZIP_FILE curLinkFileFD, int minimum_link_support, int64_t n_vertices, reverse_maps_t *reverse_maps_result, shared[1] int64_t **ccLabels_result, int64_t **list_of_assigned_cc_ids_result, int64_t *n_assigned_result, shared[1] asmObj_t *object_db, int filter_length, double filter_depth, int filter_connections, shared[1] hmm_db_t *hmm_db);


void redistribute_links(GZIP_FILE curLinkFileFD, int minimum_link_support, int64_t n_vertices, shared[1] int64_t *ccLabels, shared[1] link_cc_t **links_in_cc_result, shared[] link_t **compact_buffer_cc_result, shared[1] link_t **the_largest_cc_compact_buffer_result, shared[1] asmObj_t *object_db, int filter_length, double filter_depth, int filter_connections, shared[1] hmm_db_t *hmm_db);


void build_object_db(GZIP_FILE curReportFileFD, int64_t n_vertices, int merSize, shared[1] asmObj_t **object_db_result, shared [1] SharedString **_srf_arr);

#ifdef HIPMER_EMBED_HMMER
void build_hmm_db(int64_t n_vertices, char *_base_dir, char *s16s23_file_path, char *contig_file_path, shared[1] hmm_db_t **hmm_db_result, shared[] hmm_t **compact_buffer_hmm_result);
#endif

int compareLinks(const void *_a, const void *_b);

void prepare_oNo_inputs(int64_t n_assigned, int merSize, int64_t *list_of_assigned_cc_ids, shared[1] link_cc_t *links_in_cc, shared[1] link_t *the_largest_cc_compact_buffer, reverse_maps_t *reverse_maps, shared[1] hmm_db_t *hmm_db, shared[1] asmObj_t *object_db, FILE *gatheredLinkReportFD, FILE *gatheredObjReportFD, FILE *gatheredHmmReportFD, shared [1] SharedString *_srf_arr);

int64_t get_nScaf_from_srfFile(GZIP_FILE srfReportFD);

int parCC_main(int argc, char **argv);

#endif // _PARCC_H
