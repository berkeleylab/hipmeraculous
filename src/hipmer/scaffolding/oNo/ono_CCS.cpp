// FILENAME:  ono_CCS.cpp
// PROGRAMMER: Eugene Goltsman
// DATE:05/25/2019
// PURPOSE:   scaffolder for use with PacBio CCS long reads

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>

extern "C" {
#include "optlist.h"
}

#include "poc_common.h"
#include "common.h"
#include "log.h"

#include "ono_common.h"
#include "ono_CCS.h"


using namespace std;


#define MAX_STRAIN 2        //determines how much two links can overlap before they're deemed a tie collision
  
namespace onoCCS {
// funcitons with same names/args as those below exist in other versions of ono, thus the separate namespace

  
// up-front declarations

void parse_crfFile(string);
void parse_srfFile(string);

void mark_end(End&, EndMarkCounts&, const refHitsHash_t&);
bool resolve_tie_collision(const End &, EndId, EndId);
void find_bestTies(const sortedBy_t&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, copyTracker_t&, bool, refHitsHash_t&);
  //void find_bestTies_end(tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, copyTracker_t&, bool, tiedEndsHash_t&, tiedEndsHash_t&, End&);
  void walk_from_end(End&, vector<path_info_t>&, unordered_set<EndId>&, unordered_set<EndId>& );
void traverse_graph(path_t&,  const End&, objectSeenCnt_t&, int&, int&, bool&);
bool end_supports_all(EndId, const scoredEndsHash_t&);
  float ext_score(const path_t&, const vector<EndId>&, const EndId, const int);
bool build_contender_list(const path_t&, scoredEndsHash_t&, const vector<EndId>&, const vector<EndTie>&, const objectSeenCnt_t&, const float, float*);
void convert_path_to_bestTies(path_t&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&);
  path_t doctor_path(path_t&, copyTracker_t&, vector<path_info_t>&, const int, tiedEndsHash_t&, bool);

  




#define AVG_CRF_LINE_SIZE 26
void parse_crfFile(string crfFile)
{
    // For every contig, create a struct and populate it with contig info, including creation of two new "End" instances
    // Then add this structure to the hash of assembly objects
    LOGF("Parsing CRF %s\n", crfFile.c_str());

    ifstream infile(crfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", crfFile.c_str());
    }

    int64_t estimatedEntries = (get_file_size(crfFile.c_str()) / AVG_CRF_LINE_SIZE) * 105 / 100 + 1000; // add 5% + 1000
    onoObjectsMap.reserve(estimatedEntries);

    static const int maxLine = 180;
    char l[maxLine];
    vector<char *> words;
    while (infile.getline(l, maxLine - 1)) {
        words.clear();
        char *savePtr, *token;
        token = strtok_r(l, "\t", &savePtr);
        while (token != NULL) { // read tab deliminated tokens
            words.push_back(token);
            token = strtok_r(NULL, "\t", &savePtr);
        }
        assert(words.size() >= 3);

        // convert contig names like 'Contig123' to int64
        char *cName = words[0];
        ObjectId id;
#ifdef CONTIG_ID_PREFIX
        assert(strncmp(cName, CONTIG_ID_PREFIX, strlen(CONTIG_ID_PREFIX)) == 0);
        cName += strlen(CONTIG_ID_PREFIX);
#endif
        id = atol(cName);
        int len = stoi(words[1]);
        float depth = atof(words[2]);
        Obj newObj(id, len, depth);
        onoObjectsMap.insert(std::make_pair(id, newObj));
    }
    LOGF("Found %lld objects\n", (lld)onoObjectsMap.size());
}


// scaffInfo: key = "Scaffold$objectId"
void parse_srfFile(string srfFile)
{
    LOGF("Parsing SRF %s\n", srfFile.c_str());
    ifstream infile(srfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", srfFile.c_str());
    }

    if( ifsIsEmpty(infile)) {
      DBG("Empty srf file");
      return;
    }
    
    static const int maxLine = 180;
    char l[maxLine];
    ObjectId currId = -1;
    Scaf *_currScaf = NULL;
    Obj __tmpObj(0, 0, 0.0);
    Obj *_currObj = NULL;
    int scaffRealBases = -1;
    float scaffDepSum = 0;

    vector<char *> words;
    while (infile.getline(l, maxLine - 1)) {
        words.clear();
        char *token, *savePtr = NULL;
        token = strtok_r(l, "\t", &savePtr);
        while (token != NULL) {
            words.push_back(token);
            token = strtok_r(NULL, "\t", &savePtr);
        }
        assert(words.size() >= 4);
        int n = words.size();
        if (n > 6 || n < 4) {
            DIE("Error:  srf file not in expected format!\n");
        }


        //Scaffold459     CONTIG4 -Contig2948227  11      109     3.000000
        //Scaffold459     GAP4    -97     1
        char *scaffName = words[0];

#ifdef SCAFF_ID_PREFIX
        assert(strncmp(scaffName, SCAFF_ID_PREFIX, strlen(SCAFF_ID_PREFIX)) == 0);
        scaffName += strlen(SCAFF_ID_PREFIX);
#endif
        ObjectId id = atoll(scaffName);

        if (id != currId) {
            if (_currObj != NULL) {
                // add record to the corresponding hashes and reset totals
                _currObj->depth = scaffDepSum / scaffRealBases;
                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));
                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));

                scaffDepSum = 0;
                scaffRealBases = 0;
            }

            // initialize new objects
            _currScaf = new Scaf(id);
            _currObj = &__tmpObj;
            *_currObj = Obj(id, 0, 0.0);
        }

        char *indexStr = words[1];
        char *found = strstr(indexStr, "CONTIG");
        if (found != NULL) {
            assert(n >= 6);
            char ori = words[2][0];
            assert(ori == '+' || ori == '-');
            string cName = words[2] + 1;
            int start = atoi(words[3]);
            int end = atoi(words[4]);
            float dep = atof(words[5]);
            int cLen = end - start + 1;
            scaffRealBases += cLen;
            scaffDepSum += float(cLen * dep);

            _currObj->len = end;
            _currScaf->push_contig(cName, ori, start, end, dep);
        } else { // GAP
            int size = atoi(words[2]);
            int uncert = atoi(words[3]);
            _currScaf->push_gap(size, uncert);
        }

        currId = id;
    }

    _currObj->depth = scaffDepSum / scaffRealBases;
    scaffInfo.insert(std::make_pair(currId, _currScaf));
    onoObjectsMap.insert(std::make_pair(currId, *_currObj));

    LOGF("Found %lld objects\n", (lld)onoObjectsMap.size());
}


bool resolve_tie_collision(const End &_end, EndId _tiedEnd_i, EndId _tiedEnd_j)
{
    End &_otherEnd_i = other_end(_tiedEnd_i);
    End &_otherEnd_j = other_end(_tiedEnd_j);

    ObjectId ID_i = _tiedEnd_i.getID();
    ObjectId ID_j = _tiedEnd_j.getID();

    int len_i = onoObjectsMap.at(ID_i).len;
    int len_j = onoObjectsMap.at(ID_j).len;

    //use splint info to detect false strains
    if (is_splinted(_otherEnd_i, _tiedEnd_j)) {
        return true;
    }    
    return false;
}


void mark_end(End &_end, EndMarkCounts& endMarkCounts)
{

  _end.setEndMark(EndLabel::UNMARKED);  //default

  vector<EndTie> ties = _end.export_all_ties(); // a copy of the vector of end ties since we'll be manipulating it, sorting, etc
    if (!ties.size()) {
        _end.setEndMark(EndLabel::NO_TIES);
        endMarkCounts[EndLabel::NO_TIES]++;
        return;
    }

    for (auto tie : ties) {
        if (!tie.tiedEndId.isValid()) {
            continue;
        }
        End *_tiedEnd_i = &getEnd(tie.tiedEndId);
        if (_tiedEnd_i->getID() == _end.getID()) {
            _end.setEndMark(EndLabel::SELF_TIE);
            endMarkCounts[EndLabel::SELF_TIE]++;
            return;
        }
    }

    unsigned int nTies = ties.size();
    if (nTies < 2) {
      return;
    }

    
    // if multiple ties, check for tie collisions (forks)

    std::sort(ties.begin(), ties.end(), by_dist_len());     // sort ties by distance
    
    for (size_t i = 0; i < nTies - 1; ++i) {
      ObjectId ID_i = ties[i].tiedEndId.getID();
      int start_i = ties[i].gapEstimate;
      int end_i = start_i + onoObjectsMap.at(ID_i).len - 1;
      int uncertainty_i = ties[i].gapUncertainty;

      int start_j = ties[i + 1].gapEstimate;
      int uncertainty_j = ties[i + 1].gapUncertainty;

      int overlap = end_i - start_j + 1;
      if (overlap > merSize - 2) {
	//	cerr << "       (" << _end.toString() << ") overlap btwn tied ends " << ties[i].tiedEndId->toString()<< " & " << ties[i+1].tiedEndId->toString() << " : " << overlap << endl;
	int excessOverlap = overlap - (merSize - 2);
	float strain = float(excessOverlap) / float(uncertainty_i + uncertainty_j - 1);
	if (strain > MAX_STRAIN) {
	  if (resolve_tie_collision(_end, ties[i].tiedEndId, ties[i + 1].tiedEndId)) {
	    continue;
	  } else {
	    _end.setEndMark(EndLabel::TIE_COLLISION);
	    endMarkCounts[EndLabel::TIE_COLLISION]++;
	    //	    cerr << "TIE_COLLISION: " << _end.toString() << " -> " << ties[i].tiedEndId->toString()<< " ? " << ties[i+1].tiedEndId->toString() << endl;
	  }
	  return;  // first collision encounteres is enough to label the end, so we retrun here
	}
      }
    }
}

void gather_adjacent_splints(vector<EndTie>& adjacentSplints, vector<EndTie>& ties)
{
  // find closest splint(s)
  
  std::sort(ties.begin(), ties.end(), by_dist_depth());

  int closest_i = 0;
  while  (closest_i < ties.size() && !ties[closest_i].isSplinted()) {
    DBG2N("[%s] Closest tie is not splinted!\n", __func__);
    closest_i++;
  }
  if (closest_i >= ties.size())
    return;
  
  adjacentSplints.push_back(ties[closest_i]);

  int start_closest = ties[closest_i].gapEstimate;
  ObjectId ID0 = ties[closest_i].tiedEndId.getID();
  int end0 = start_closest + onoObjectsMap.at(ID0).len - 1;
  for (int i = closest_i+1; i < ties.size(); ++i) {
    if  (! ties[i].isSplinted())
      continue;

    int start_i = ties[i].gapEstimate;
    if (start_i == start_closest) {
      adjacentSplints.push_back(ties[i]);
      continue;
    } else {
      // determine if this tie overlaps/collides with the first (closest) tie
      int overlap = end0 - start_i + 1;
      int excessOverlap = overlap - (merSize - 2);
      if (excessOverlap > MAX_STRAIN) {
	adjacentSplints.push_back(ties[i]);
      } else {
	break;  // no excessive overlap. no need to go further
      }
    }
  }
}

void gather_farthest_splints(vector<EndTie>& farthestSplints, vector<EndTie> ties)
{
    // find farthest splint(s)
    //   If multiple equidistant, put highest depth first;  later we will want to traverse high-depth branches first

    // Note:  SPLINT links have gap estimate taken from highest frequency splint (bmaToLinks)

  std::sort(ties.begin(), ties.end(), by_dist_depth());
  int farthest = ties.back().gapEstimate;
  for (int i = ties.size()-1; i>=0; --i) {
    if ( ! ties[i].isSplinted()) {
      continue;
    }	  
    if (ties[i].gapEstimate < farthest) {
      break;
    }
    farthestSplints.push_back(ties[i]);
  }

}


void traverse_graph(path_t& currentPath,  const End &_myEnd, objectSeenCnt_t &seen, int &totDistTraversed, int &level, bool &limitsReached)
{
  /*  
    Recursive direct traversal function; Nodes are Ends, edges are ties.
    Inputs:  myEnd   - end we're trying to extend. Currently last node in myPath

    Losely based on the exSPAnder algorithm.  Relies on cumulative score of potential extension and comparing that against alternative extensions.
    Score is calculated using a scoring_function and winner-loser decision rules.  

  */

  // some tweakable params affecting decision rule  (per exSPAnder paper)
  float theta = 0.5; //min score allowed
  float C = 1.2;   // winner score must exceed runner-up by this ratio

  float currentWinnerScore = 0.0;
  // parameter to limit traversal distance  
  int maxLevel = 100000;
  int maxDist = 100000;

  DBGN("\tDFS: myEnd: %s  (totDistTraversed = %d)\n", _myEnd.endId.toString().c_str(), totDistTraversed);
  ObjectId objID = _myEnd.getID();
  if (!seen.count(objID)) {
    seen[objID] = 1;
  }else {
    ++seen[objID];
  }

  //  DBGN("DEBUG: seen objID %d: %d\n", objID, seen[objID]);
  
  if (++level == maxLevel) {    
    limitsReached=true;
    DBGN("\tDFS: maxLevel reached\n");     
    return;
  }

  vector<EndTie> ties = _myEnd.export_all_ties();
  if (ties.empty()) {
    DBGN("\tDFS: No ties\n");
    return;
  }

  vector<EndTie> adjacentSplints;
  gather_adjacent_splints(adjacentSplints, ties);
  if (! adjacentSplints.size() ){
    DBGN("\tDFS: No adjacent splint ties found!\n\n");
    return;
  }
    
  // evaluate closest ties and pick as contenders those with score >= currentWinnerScore, i.e. they're worth considering as possible extensions

  scoredEndsHash_t contenderH;
  vector<EndId> repeat_nodes;  // will be populated AFTER the preliminalry contenders have been selected

      
  //get preliminary contenders and and the best score among them
  build_contender_list(currentPath, contenderH, repeat_nodes, adjacentSplints, seen, C, &currentWinnerScore);
  if ( !contenderH.size() ) {
    DBGN("\tDFS: no suitable contender extensions found.\n");
    return;
  }

      
  //Find repeat-ends in current path  *relative to the contender collection* (ala exSPAnder) and prevent them from contributing to condender scoring;   then rebuild/rescore the contenders hash.  Single contenders are passed automatically.

  repeat_nodes.clear();
  while (contenderH.size() > 1) {   
    for (size_t i = 0; i < currentPath.size(); ++i) {
      EndId pid = currentPath[i];
      if (end_supports_all(pid, contenderH)) {
	repeat_nodes.push_back(pid);
      }
    }
    if (repeat_nodes.size()) {
      DBGN("\t[DEBUG] repeat_nodes detected in path\n");
    }
    //get revised list of contenders
    int prevNContenders = contenderH.size();
    contenderH.clear();
    build_contender_list(currentPath, contenderH, repeat_nodes, adjacentSplints, seen, C, &currentWinnerScore); 
    // iterate until contender list is not changing
    if (contenderH.size() == prevNContenders) {
      break;
    }
  }
      
  if (!contenderH.size()) {
    DBGN("\tDFS: No suitable contender extensions found.\n");
    //    return;
  }
     

  // pick *strong* winner

  EndId currentWinningEnd;
  float best;
  float runnerUp;
  float winnerScore = 0.0;

  if (contenderH.size() == 1) {
    if (contenderH.begin()->second >= theta) {
      currentWinningEnd = contenderH.begin()->first;
      winnerScore = contenderH.begin()->second;
    }
  }
  else if (contenderH.size() > 1) {
        //sort by score (decreasing)
    vector<std::pair<EndId, float>> sortedByScore(contenderH.begin(), contenderH.end());
    
    auto comp = [] (std::pair<EndId, float> elem1, std::pair<EndId, float> elem2) -> bool
      {
	return elem1.second > elem2.second;
      };
    std::sort(sortedByScore.begin(), sortedByScore.end(), comp);
    assert(sortedByScore[0].second >= sortedByScore[1].second); //DELETE ME
    
    best = sortedByScore[0].second;
    runnerUp = sortedByScore[1].second;
    if (best >= theta && best > C*runnerUp) {
      currentWinningEnd = sortedByScore[0].first;
      winnerScore = best;
    }
  }
  else
    DBG2N("\tDFS: No suitable contender extensions found.\n");
  
  
  if ( ! winnerScore ) {
    DBGN("\tDFS: failed to pick winning extension.\n\n");
    return;
  }
  DBGN("\tDFS: strong winner tied end %s, score=%f\n", currentWinningEnd.toString().c_str(), winnerScore);
  

    // add both ends of the winner object to current path and continue traversal off of the new tip
  
  ObjectId winnerObjId = currentWinningEnd.getID();
  int gapEst = _myEnd.get_tie(currentWinningEnd)->gapEstimate;

  if (gapEst + onoObjectsMap.at(winnerObjId).len  > maxDist) {
    limitsReached =true;
    DBGN("\tDFS: Winning extension found, but path would exceed maxDist!\n");
    return;
  }
  End &_outEnd = other_end(currentWinningEnd);
  currentPath.push_back(currentWinningEnd);
  currentPath.push_back(_outEnd.endId);
  totDistTraversed += (gapEst + onoObjectsMap.at(winnerObjId).len);
  
  traverse_graph(currentPath, _outEnd, seen, totDistTraversed, level, limitsReached);

  return;
}


bool end_supports_all(EndId refEndId, const scoredEndsHash_t& contenderH)
{
  for(auto kv : contenderH) {
    EndId testId = kv.first;
    if (getEnd(refEndId).get_tie(testId) == NULL) {
      return false;
    }
  }
  return true;
}

float ext_score(const path_t& path, const vector<EndId>& repeat_nodes, const EndId myEndId, const int expReach)
{
  /* 
     This function scores tne extension candidate based on cumulative support wrt nodes in currentPath.
     Only the *number* of expected vs present ties is considered, and all objects are treated the same (i.e. no 
     lower expectation for short obects vs long) 
  
  */
  int expected = 0;
  int support = 0;
  int dist = 0;
  float score = 0.0;

  //When two nodes in the path are not adjacent, their separation cam be measused 
  //  i) by direct splint gap estimate and ii) by sum of individual gaps + object size for all the nodes inbetween
  //  This cutoff is the allowed discrepancy between the two
  int maxDistDeviation = merSize;   
  
  // path contains two ends for every object, but we're only interested in the "out" ends, so we skip one each time
  int prevObjSize = 0;
  EndId prevEndId = myEndId;
  for (int i = path.size() - 1; i >= 0; i -= 2) {
    
    EndId outEndId = path[i];
    DBG2N("\t\t\tDFS[ext_score] %d: testing myEnd w path outEndId %s ...\n", i, outEndId.toString().c_str()); 
    End &_outEnd = onoObjectsMap.getEnd(outEndId);
    dist += prevObjSize;
    dist += _outEnd.get_tie(prevEndId)->gapEstimate;    

    if (std::find(repeat_nodes.begin(), repeat_nodes.end(), outEndId) == repeat_nodes.end()){  // dont evaluate repeat nodes
      //update total distance traveled from myEnd
      if (dist <= expReach) {  //we expect a splint from here to myEnd
	expected++;
      } else if (dist >= expReach * 1.25) {  // no point in checking further back	
	break;
      }

      
      EndTie* tieToMyEnd = _outEnd.get_tie(myEndId);
      if (tieToMyEnd != NULL
	  && tieToMyEnd->isSplinted()
	  && (abs(tieToMyEnd->gapEstimate - dist) <= maxDistDeviation)
	  )   {
	// we indeed have a splint here with the gap estimate matchin the distance we've traveled
	support++; 
      }
    }

    DBG2N("\t\t\tDFS[ext_score] dist: %d, exp: %d, supp: %d \n", dist, expected, support);
    prevEndId = outEndId;
    prevEndId.flip_prime();

    prevObjSize = onoObjectsMap.at(prevEndId.getID()).len;
  }

  if (!expected) {
    return 0.0;
  }
  score = float(support)/float(expected);
  return score;
}


bool build_contender_list(const path_t& currentPath, scoredEndsHash_t& contenderH, const vector<EndId>& repeat_nodes, const vector<EndTie>& adjacentSplints, const objectSeenCnt_t& seen, const float C, float *_currentWinnerScore)
 {

   // determine *expected* reach back into the path  
   int expReach = 5000; //TEMP
   
   
   float runningBestScore = 0.0;
   for (size_t i = 0; i < adjacentSplints.size(); ++i) {
     const EndTie* _tie_i = &adjacentSplints[i];
     ObjectId tiedObjID_i = _tie_i->tiedEndId.getID();
     EndIdAndFlag endId_i = adjacentSplints[i].getTiedEnd().getEndId();
     
     // if (adjacentSplints[i].gapEstimate > merSize - 2) {
     //   //check if current head of path is "the closest neighbor to the closest neighbor"
     //   vector<EndTie> tiesBack = adjacentSplints[i].getTiedEnd().export_all_ties();
     //   std::sort(tiesBack.begin(), tiesBack.end(), by_dist_depth());
     //   if (tiesBack[0].tiedEndId.getID() != currentPath.back().getID()) {
     // 	 DBGN("\t\tDFS[%s]: Adjacent splint (%s) doesn't check out the other way; Rejected.\n", __func__, endId_i.toString().c_str());
     // 	 continue;
     //   }
     // }
     if (seen.count(tiedObjID_i)) {
       int seenCnt = seen.at(tiedObjID_i);       
       DBGN("\t\tDFS[%s]: LOOP! (obj: %d, seen: %d) - ", __func__, tiedObjID_i, seenCnt);
       if (seenCnt > 1) { // allow going through a loop once
	 DBGN("Too many visits to object; Rejected.\n");
	 continue;
       }
       DBGN("Allowed\n");
     }

     float score_i;
     DBGN("\t\tDFS[%s]: trying potential contender end %s..\n",__func__, endId_i.toString().c_str());

     //if the candidate has two or more ties, *all* to non-repeat nodes in current path, it gets a score of 0.5 withot further evaluation, i.e. we consider it a viable candidate but with low score.

     vector<EndId> reverseTies = _tie_i->getInverseTiedIds();
     if (reverseTies.size()  >= 2) {
       int nTiedToMyUniqPath = 0;
       for (auto &tiedBackTo : reverseTies) {
	 if (std::find(repeat_nodes.begin(), repeat_nodes.end(), tiedBackTo) == repeat_nodes.end()){
	   continue;
	 }
	 if (std::find(currentPath.begin(), currentPath.end(), tiedBackTo) == currentPath.end()) {
	   continue;
	 }
	 nTiedToMyUniqPath++;
       }
       if (reverseTies.size() == nTiedToMyUniqPath) {
	 score_i = 0.5;
	 contenderH[endId_i] = score_i;
	 *_currentWinnerScore = score_i;
	 DBGN("\t\t\tDFS[build_contender_list]: has ALL of its ties back to non-repeat nodes of current path - gets score of %f\n", score_i);
	 continue;
       }
     }
     
     score_i = ext_score(currentPath, repeat_nodes, endId_i, expReach);
     DBGN("\t\t\tDFS[build_contender_list]: score_i = %f (C*score = %f)\n", score_i, (C*score_i));
     //add to active set if C*score >= winner from *previous bulid_contenders attempt*
     if (C*score_i >= *_currentWinnerScore) {
       runningBestScore = score_i;
       contenderH[endId_i] = score_i;
     }
   }
   DBGN("\t\tDFS[build_contender_list]: %d active contenders\n", contenderH.size());
   if (runningBestScore > *_currentWinnerScore) {
     *_currentWinnerScore = runningBestScore;
   }
   return true;
 }


  path_t doctor_path(path_t& myPath, copyTracker_t& copyObjTracker, vector<path_info_t>& allWalks, const int idx, tiedEndsHash_t& uPathEdgesALL, bool existsSrfFile)
{

  path_t uniqPath;
  
  // First, trim tail if already occurs in larger walks (allWalks is sorted by length)
  if (idx < allWalks.size() -1) {
    
    for (int i = idx+1; i < allWalks.size(); i++) {
      path_t ref_path = allWalks[i].path;
      if (std::find(ref_path.begin(), ref_path.end(), myPath.back()) == ref_path.end())	
	continue;

      int nRedundant = path_suffix_overlap(myPath, ref_path);
      if (nRedundant) {
	int nUniq = myPath.size() - nRedundant;
	DBG2N("[%s] Path of size %d has tail redundancy of %d\n",__func__, myPath.size(), nRedundant);  
	//trim back path
	if (nUniq % 2 != 0)  //make sure we're trimming back to the last "true" edge
	  nUniq--;
	if (nUniq < 2) {
	  return uniqPath;  // return empty path
	}
	myPath.resize(nUniq);
	DBG2N("[%s] Trimmed back redundant tail to %d.\n",__func__, myPath.size());	
      }
    }
  }
  
  //  if any nodes have been incorporated into another finalized path, make copies and build final uniqPath
    bool trueEdge = true; 

    ObjectId prevObjID = 0;
    ObjectId curCopyId;
    unordered_set<EndId> seenHere; // if repeats are present, this is to keep track of object copies in this path only

    for (auto it = myPath.begin(); it != myPath.end(); ++it) {
        trueEdge = (std::distance(myPath.begin(), it) % 2 != 0) ? true : false;
	 //Since path elements are ends, two consecutive ends of same contig may or may not be a true edge (think opposite ends of same contig  vs. a true tandem repeat edge).
	// Here we assume that only ODD elements (0-based) are connected to the preceding element via a true edge
        End &_myEnd = getEnd(*it);
        ObjectId myID = _myEnd.getID();
	
        if (uPathEdgesALL.count(_myEnd.endId)  // alredy seen in other paths
	    || 
	    seenHere.count(_myEnd.endId)  //already seen in this path
	    ||
            (uPathEdgesALL.count(other_end(_myEnd).endId) && (it != myPath.begin() && std::next(it) != myPath.end()))
	    //The last condition is for a case where not this end but the OTHER END of this object already exists in a different r-path,  and the two paths don't attach end-to-end.
	    ) {
            int endPrime = _myEnd.getPrime(); // do this before _myEnd may be invalidated by create_copy_object!
	    if (trueEdge) { 
                curCopyId = create_copy_object(&onoObjectsMap.at(myID), copyObjTracker, existsSrfFile);
		DBGN("[%s]: object %d already part of this or another r-path; created copy-object %d\n", __func__, myID, curCopyId);
            }
            End *_copyEnd = (endPrime == 5) ?  &onoObjectsMap.at(curCopyId).end5 : &onoObjectsMap.at(curCopyId).end3;
            uniqPath.push_back(_copyEnd->endId);

        } else {
            uniqPath.push_back(_myEnd.endId);
        }

	seenHere.insert(_myEnd.endId);
        prevObjID = myID;
    }

    for (size_t i = 0; i < uniqPath.size(); i = i + 2) {
        //update hash of unique r-path edges
        uPathEdgesALL.insert(std::pair<EndId, EndId>(uniqPath[i], uniqPath[i + 1]));
        uPathEdgesALL.insert(std::pair<EndId, EndId>(uniqPath[i + 1], uniqPath[i]));
    }

    return uniqPath;
}


void convert_path_to_bestTies(path_t& path, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended)
{
    //# Aggressively turn the path into reciprocal bestTie entries, overwriting anything that's already present.
    //# This is meant to set in stone a particular path through the graph with no regard to any other linkage.

    //iterate over each *pair* of ends in the path and create RECIPROCAL bestTie & bestTiedBy records
    array<EndId, 2> tiedPair;
    for (size_t i = 0; i < path.size(); i += 2) {
        EndId _teA = path[i];
        EndId _teB = path[i + 1];

        tiedPair = { _teA, _teB };

	//	DBG2N("[%s] Checking existing BT and BTB entries for %s & %s ...\n", __func__, _teA.toString().c_str(), _teB.toString().c_str());
        for (auto & _te : tiedPair) {
            auto teInBestTies = bestTies.find(_te);
            if (teInBestTies != bestTies.end()) {
                // if exists, delete bestTie of this end, plus that end's bestTiedBy entry corresponding to my end.
                EndId _BTof = teInBestTies->second;
                auto & ties = bestTiedBy.at(_BTof);
		//                DBG2N("[%s]: Erasing BEST_TIE of %s: %s\t", __func__,  _te.toString().c_str(), _BTof.toString().c_str());
                for (vector<EndId>::iterator it = ties.begin(); it != ties.end(); ++it) {
                    if (*it == _te) {
		      //                        DBG2N("and its matching BEST_TIED_BY %s\t", it->toString().c_str());
                        ties.erase(it);
                        break;
                    }
                }
                bestTies.erase(_te);
            }
            // if other ends have this end as their bestTie, delete those, plus my end's bestTiedBy record
            auto teInBestTiedBy = bestTiedBy.find(_te);
            if (teInBestTiedBy != bestTiedBy.end()) {
	      //	      DBG2N("[%s]: Erasing BEST_TIED_BY of %s\t", __func__, _te.toString().c_str());
                vector<EndId> &BTBy = teInBestTiedBy->second;
                for (auto & _btb : BTBy) {
		  //                    DBG2N("and BEST_TIE of %s\t", _btb.toString().c_str());
		    bestTies.erase(_btb);
                }
                bestTiedBy.erase(_te);
            }

            // delete form suspended list if found
            ObjectId objID = _te.getID();
	    if (suspended.erase(objID)) {
	      //	      DBG2N("[%s]: Erased SUSPENSION of %lld\t",__func__, (lld)objID);
	    }
        }

        bestTies[_teA] = _teB;
        bestTiedBy[_teB].push_back(_teA);

        bestTies[_teB] = _teA;
        bestTiedBy[_teA].push_back(_teB);

	//        DBG2N("[%s]: Mutual best ties: %s and %s\n", __func__, _teA.toString().c_str(), _teB.toString().c_str());

    }
    DBG2N("[%s]: Done with path.\n",__func__)
}

  
  void walk_from_end(End &_end, vector<path_info_t> &allWalks, unordered_set<EndId> &walkTerminals, unordered_set<EndId> &walkNodesLookup)
{     
    DBG2N("end %d:\n", _end.getPrime());

    //        if (!walkTerminals.count(_end.endId)) {
    if (!walkNodesLookup.count(_end.endId)) { //don't initiate walk if end is already part of a walk
    
      objectSeenCnt_t seen;
      int gap = 0;
      int level = 0;
      int distanceTraversed = 0;
      bool limitsReached = false;
	
      DBG2N("\nTry DFS from end %s ...\n", _end.toString().c_str());
      path_t bestPath;
      bestPath.reserve(200);

      bestPath.push_back(_end.endId); // prime the path with the first "out" node
      traverse_graph(bestPath, _end, seen, distanceTraversed, level, limitsReached);

      // If the path terminates with an end that's already another path's terminal, clip that entire object off
	while (bestPath.size() > 2 && walkTerminals.count(bestPath.back())) {
	  DBG2N("Clipped terminal object of walk since it is already a terminal of another finalized path.\n");
	  bestPath.resize(bestPath.size() - 2);
	}

      bestPath.pop_back(); // paths must end with a sink end, so clip the last "out" node of the path

      if (bestPath.size()) {
	path_info_t pathInfo;
	pathInfo.path = bestPath;
	pathInfo.distanceTraversed = distanceTraversed;
	  
	allWalks.push_back(pathInfo);
	walkTerminals.insert(bestPath.back());
	for (auto eid : bestPath) {
	  walkNodesLookup.insert(eid);
	}	
      }
    } else {
      DBGN(" - ineligible since already part of walk\n");
    }       
}

  
  void find_bestTies(const sortedBy_t& sortedByLen, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyObjTracker, bool existsSrfFile)
{
    // Super-function to evaluate all size-sorted objects' ends and initiate bestTie search


    unordered_set<EndId> walkTerminals;  // Safeguards against a misassembly-prone situation where two paths terminate at the same object from opposite ends which would implicitly join the paths into one.
    unordered_set<EndId> walksLookup; //keep track of the nodes seen in walks
    vector<path_info_t> allWalks;  //the complete set of initial walks   
    allWalks.reserve(1000); // optimization; we could end up with a lot more or a lot fewer

    
    LOGF("Finding walks (%lld objects)...\n", (lld)sortedByLen.size());

    SET_HIPMER_VERBOSITY(LL_DBG2);
    int64_t count = 0, progressCt = sortedByLen.size() / 100;
    for (auto tmp : sortedByLen) {
        ObjectId id = tmp.id;

        if (progressCt && ++count % progressCt == 0) {
            LOGF("find_bestTies: %lld of %lld (%0.3f %%) object %lld\n", (lld)count, (lld)sortedByLen.size(), 100.0 * count / sortedByLen.size(), (lld)id);
        }
        DBG2(" === object %lld\t", (lld)id);

        if (suspended.count(id)) {
            DBG2N(" -ineligible (suspended)\n");
            continue;
        }

        Obj *piece; // find_bestTies_end may change onoObjectMap, so ensure piece is correct for each call
        piece = &onoObjectsMap.at(id);
	walk_from_end(piece->end5, allWalks, walkTerminals, walksLookup);
        piece = &onoObjectsMap.at(id);
	walk_from_end(piece->end3, allWalks, walkTerminals, walksLookup);
        DBG2N("\n");
    }
    walksLookup = unordered_set<EndId>(); //release mem
    walkTerminals = unordered_set<EndId>(); //release mem

    
    //evaluate and finalize paths, then make bestTies
    LOGF("Evaluating and filtering walks (%lld walks)...\n", (lld)allWalks.size());

    tiedEndsHash_t uPathEdgesALL;  // stores edges (end pairs) that are part of final confirmed paths
    
    std::sort(allWalks.begin(), allWalks.end(), by_path_len());
    int idx = 0;
    for (auto &walkInfo : allWalks) {
      DBG2N("\tEvaluating walk (%d bp)...\n", walkInfo.distanceTraversed);
      path_t &myPath = walkInfo.path;
      path_t uniqPath = doctor_path(myPath, copyObjTracker, allWalks, idx, uPathEdgesALL, existsSrfFile);      
      if (uniqPath.size()) {
	convert_path_to_bestTies(uniqPath, bestTies, bestTiedBy, suspended);
      }
      idx++; 
    }

    //  Insert suspended pieces where possible
    place_suspended(suspended, bestTies, bestTiedBy, copyObjTracker, existsSrfFile);

    SET_HIPMER_VERBOSITY(LL_DBG);
    
    // update ties for any newly created copy-object ends
    LOGF("Updating ties for uPathEdgesALL=%lld\n", (lld)uPathEdgesALL.size());
    count = 0, progressCt = uPathEdgesALL.size() / 100;
    for (auto & element : uPathEdgesALL) {
        if (progressCt && ++count % progressCt == 0) {
            LOGF("updating ties %lld of %lld %0.1f %%\n", (lld)count, (lld)uPathEdgesALL.size(), 100.0 * count / uPathEdgesALL.size());
        }
        EndId _e1 = element.first;
        EndId _e2 = element.second;
        if (_e1.getID() < 0) { // a copy-end
            ObjectId e1_parentId = onoObjectsMap.at(_e1.getID()).parentId;
            assert(e1_parentId != LLONG_MIN);
            Obj &e1_parent = onoObjectsMap.at(e1_parentId);
            End *sourceEnd = (_e1.getPrime() == 5) ? &e1_parent.end5 : &e1_parent.end3;
            assert(sourceEnd);

            End *__e1 = &getEnd(_e1);
            End *__e2 = &getEnd(_e2);
            if (_e2.getID() < 0 && !__e2->ties.size()) { //a copy-end that hasn't had ties assigned;  use parent end instead
                ObjectId e2_parentId = onoObjectsMap.at(_e2.getID()).parentId;
                assert(e2_parentId != LLONG_MIN);
                Obj &e2_parent = onoObjectsMap.at(e2_parentId);
                _e2 = (_e2.getPrime() == 5) ? e2_parent.end5.endId : e2_parent.end3.endId;
                __e2 = &getEnd(_e2);
            }
            copy_tie_info(sourceEnd, __e1, __e2);
        }
    }
}
}  // end of namespace onoCCS





int oNo_CCS_main(int argc, char **argv)
{
    using namespace std;
    using namespace onoCCS;
    
    LOG_VERSION;
    initCommonStatics();
    
    if (argc < 2) {
    SWARN("Usage: ./ono_CCS <-m merSize> <-l linkDataFile> (<-s scaffoldReportFile> || <-c contigReportFile>)  <-o scaffOutFile> <<-p supportThreshold>> <<-O IDoffset>> <<-V(erbose)>>\n");
    return 1;
    }

    option_t *optList, *_optList, *thisOpt;
    _optList = GetOptList(argc, argv, "m:l:p:s:c:o:O:V");
    optList = _optList;

    char *linkDataFile = NULL;
    char *srfFile = NULL;
    char *crfFile = NULL;
    char *srfOutFile = NULL;
    int supportThreshold = 2;
    ObjectId srfOffset = 0;
    bool verbose = false;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'm':
            merSize = atoi(thisOpt->argument);
            break;
        case 'l':
            linkDataFile = thisOpt->argument;
            break;
        case 'p':
            supportThreshold = atoi(thisOpt->argument);
            break;
        case 's':
            srfFile = thisOpt->argument;
            break;
        case 'c':
            crfFile = thisOpt->argument;
            break;
        case 'o':
            srfOutFile = thisOpt->argument;
            break;
        case 'O':
            srfOffset = atol(thisOpt->argument);
            break;
        case 'V':
	    verbose = true;
	    SET_HIPMER_VERBOSITY(LL_DBG2);
	    break;
	}
    }

    if (!merSize) {
        SWARN("You must specify a -m merSize!\n");
        return 1;
    }
    if (linkDataFile == NULL) {
        SWARN("You must specify a -l linkDataFile!\n");
        return 1;
    }
    if (srfFile == NULL && crfFile == NULL) {
        SWARN("You must specify either -c crfFile or -s srfFile  inputs!\n");
        return 1;
    }
    if (srfOutFile == NULL) {
        SWARN("You must specify an output file!\n");
        return 1;
    }

    unordered_map<string, libInfo_t> libInfoHash;

    vector<string> libMetaData;

    ifstream infile(linkDataFile);
    string line;
    while (getline(infile, line)) {
        libInfo_t libInfo;
        istringstream stm(line);
        string word;
        while (stm >> word) { // read white-space delimited tokens one by one
            libMetaData.push_back(word);
        }
        string libName = libMetaData[0];
        libInfo.insSize = stoi(libMetaData[1]);
        libInfo.sdev = stoi(libMetaData[2]);
        libInfo.linkFile = libMetaData[3];

	if (!libInfoHash.count(libName))
	  libInfoHash.insert({ libName, libInfo });

        if (libInfo.insSize > maxAvgInsSize) {
            maxAvgInsSize = libInfo.insSize;
            maxAvgInsSDev = libInfo.sdev;
        }
	libMetaData.clear();
    }


    // load contig/scaffold info

    if (crfFile != NULL) {
        parse_crfFile(string(crfFile));
    } else if (srfFile != NULL) {
        parse_srfFile(string(srfFile));
    }


    if (!onoObjectsMap.size()) { // Not a fatal error since parCC is not guaranteed to produce a CC for every thread;  Write empty srf file and exit;
        FreeOptList(_optList);
        LOGF("No assembly objects found in the input;  \n");
        ofstream srfOut_empty(srfOutFile);
        //  FLUSH_MY_LOG;
        return 0;
    }


    
    LOGF("There are %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);

    int maxUncertainty = 1000000; // no spans are allowed here, so set to arbitrary large value
    
    //Build the linkage hash
    int64_t numLinks = 0;
    unordered_map<string,bool> seen;
    for (std::pair<string, libInfo_t> element : libInfoHash) {
        string libName = element.first;
        string linkFile = libInfoHash[libName].linkFile;
	if (!seen.count(linkFile)) { //multiple libs -  the same link file
	  numLinks += parse_linkFile_and_build_ties(linkFile, supportThreshold, maxUncertainty, NULL != srfFile);
	  seen.insert({linkFile, true});
	}
    }

    LOGF("There are %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);

    // build a vector of pointers to objects, sorted by sequence length
    LOGF("Reserving %lld entries for sortedByLen (%0.3f MB)\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj *) / ONE_MB);
    sortedBy_t sortedByLen;
    sortedByLen.reserve(onoObjectsMap.size());


    for (auto kv = onoObjectsMap.begin(); kv != onoObjectsMap.end(); kv++) {
        Obj &obj = kv->second;
        sortedByElement_t tmp(obj);
        sortedByLen.push_back(tmp);
    }

    std::sort(sortedByLen.begin(), sortedByLen.end(), by_length_depth());

    // mark object ends

    int nObjects = sortedByLen.size();
    EndMarkCounts endMarkCounts = {};
    LOGF("Marking ends... (%d objects)\n", nObjects);

    for (auto _i : sortedByLen) {
        Obj *i = &onoObjectsMap.at(_i.id);
        mark_end(i->end5, endMarkCounts);
	DBG2N(" %s  marked [%d]\n", i->end5.toString().c_str(), i->end5.getEndMark());
        mark_end(i->end3, endMarkCounts);
	DBG2N(" %s  marked [%d]\n", i->end3.toString().c_str(), i->end3.getEndMark());
    }


    // # find a "best tie" for every end.
    tiedEndsHash_t bestTie;     //stores EndId => this end's bestTie EndId
    tiedEndListsH_t bestTiedBy; // stores EndId => list of ends that claim this end as their bestTie
    suspendedHash_t suspended;

    copyTracker_t copyObjTracker;
    find_bestTies(sortedByLen, bestTie, bestTiedBy, suspended, copyObjTracker, NULL != srfFile);
    LOGF("bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    LOGF("bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    LOGF("suspended has %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    LOGF("copyObjTracker has %lld entries (%0.3f MB)\n", (lld)copyObjTracker.size(), 1.0 * copyObjTracker.size() * sizeof(copyTracker_t::value_type) / ONE_MB);

    LOGF("Freeing memory from sorted objects %lld\n", (lld)sortedByLen.size());
    sortedBy_t().swap(sortedByLen);

    // # Lock ends together with no competing ties
    endLocks_t endLocks;
    lock_ends(endLocks, bestTie, bestTiedBy, suspended, onoObjectsMap);
    LOGF("bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    LOGF("bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    LOGF("endLocks has %lld entries (%0.3f MB)\n", (lld)endLocks.size(), 1.0 * endLocks.size() * sizeof(endLocks_t::value_type) / ONE_MB);
    LOGF("Freeing memory from suspended which %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    suspendedHash_t().swap(suspended);

    // # Traverse end locks to build scaffolds
    int64_t nScaffolds = build_scaffolds(endLocks, string(srfOutFile), NULL != srfFile, copyObjTracker, srfOffset);
    LOGF("Total of %lld scaffolds made\n", (lld)nScaffolds);


    for (int i = 0; i < EndLabel::MAX_END_LABEL; i++) {
        LOGF("Ends marked %s: %d\n", EndLabel::getName(i).c_str(), endMarkCounts[i]);
    }
    DBG("nr of best ties: %lld\n", (lld)bestTie.size());
    DBG("nr of copy-objects: %lld\n", (lld)copyObjTracker.size());
    DBG("nr of scaffolds: %lld\n", (lld)nScaffolds);

    // Cleanup

    LOGF("Cleaning bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    tiedEndsHash_t().swap(bestTie);
    LOGF("Cleaning bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    tiedEndListsH_t().swap(bestTiedBy);
    LOGF("Cleaning suspended has %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    suspendedHash_t().swap(suspended);
    LOGF("Cleaning endLocks has %lld entries (%0.3f MB)\n", (lld)endLocks.size(), 1.0 * endLocks.size() * sizeof(endLocks_t::value_type) / ONE_MB);
    endLocks_t().swap(endLocks);

    LOGF("Cleaning %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    objectsHash_t().swap(onoObjectsMap);

    LOGF("Cleaning %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    for (auto &sc : scaffInfo) {
        delete sc.second;
    }
    LOGF("Done deleting %lld scaffold objects\n", (lld)scaffInfo.size());
    scaffsHash_t().swap(scaffInfo);

    FreeOptList(_optList);
    LOGF("Done\n");
    
    FLUSH_MY_LOG;
    // END OF MAIN
    
    return 0;
}
