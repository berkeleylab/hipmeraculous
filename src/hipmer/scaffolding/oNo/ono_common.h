#ifndef _ONO_COMMON_H_
#define _ONO_COMMON_H_

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <memory>
#include <cstdint>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <functional>
#include <array>
#include <climits>

using namespace std;

#define CONTIG_ID_PREFIX "Contig"  // comment this out if input contig names are numeric.
#define SCAFF_ID_PREFIX "Scaffold" // comment this out if input scaffold names are always numeric.
#define USE_ULDP 1    // comment this out to turn off "depth-peer" based rRNA traversal assistance

extern int merSize;
extern int maxAvgInsSize;
extern int maxAvgInsSDev;

void initCommonStatics();

struct libInfo_t {
    int16_t insSize;
    int16_t sdev;
    string  linkFile;
};

struct ref_hit_t {
    int    start;
    int    end;
    string taxonomy;
};

struct link_info_t {
    int16_t support1, support2, support3; // 3|0|3
    int16_t gapEstimate;
    int16_t gapUncertainty = 0;
    int8_t  type; // SPLINT=1 SPAN=2
};


// forward declarations
class End;
class Obj;
class Scaff;

#define OBJECT_ID_BITS 56
typedef int64_t ObjectId;
typedef unordered_map<ObjectId, int> copyTracker_t;
typedef unordered_set<ObjectId> objectSeen_t;
typedef unordered_map<ObjectId, int> objectSeenCnt_t;

class EndLabel {
public:
    enum Label : uint8_t {
        UNMARKED = 0,
        SELF_TIE = 1,
        TIE_COLLISION = 2,
        DEPTH_DROP = 3,
        NO_TIES = 4,
        TIE_SPLIT_DD = 5,
        RRNA = 6,
        MAX_END_LABEL
    };
    static std::string getName(Label n)
    {
        std::string s = "INVALID";

        switch (n) {
        case UNMARKED:      s = "UNMARKED"; break;
        case SELF_TIE:      s = "SELF_TIE"; break;
        case DEPTH_DROP:    s = "DEPTH_DROP"; break;
        case TIE_COLLISION: s = "TIE_COLLISION"; break;
        case NO_TIES:       s = "NO_TIES"; break;
        case TIE_SPLIT_DD:  s = "TIE_SPLIT_DD"; break;
        case RRNA:          s = "RRNA"; break;
        default:            s = "UNKNOWN"; LOGF("What is label %d?\n", n); break;
        }
        ;
        return s;
    }
    static std::string getName(int n)
    {
        return getName((Label)n);
    }
};
typedef std::array<int64_t, EndLabel::MAX_END_LABEL> EndMarkCounts;

class EndId {
public:
    /* store the id and prime within 64-bits.
     *     Ids can be negative so use the 62nd bit to determine the prime
     *     Also store flags for EndIdAndFlag subclass
     */
    enum Flag : uint8_t {
        FLAG_NONE = 0,
        FLAG_DIVERGENCE = 1,
        FLAG_CONVERGENCE = 2,
        FLAG_INVALID = 4
    };

protected:
    uint8_t _endMark : 4;          // EndLabel is < 8... could even use 3 bits...
    uint8_t _prime : 1;            // 0 -> 3' 1 -> 5'
    uint8_t _divergence : 1;       // i.e. FLAG_DIVERGENCE
    uint8_t _convergence : 1;      // i.e. FLAG_CONVERGENCE
    uint8_t _dummy : 1;            // to fill all bits...
    ObjectId _id : OBJECT_ID_BITS; // the remaining bits, quadrillions is close enough to hold all ids.

public:

    const static ObjectId INVALID = (1LL << (OBJECT_ID_BITS - 1)) - 1; // the id that is invalid
    EndId() : _endMark(EndLabel::UNMARKED) {
        //DBGN("Called EndId()\t");
        set(INVALID, 5, FLAG_INVALID);
    }
    EndId(ObjectId id, int8_t prime) : _endMark(EndLabel::UNMARKED) {
        //DBGN("Called EndId(ObjectId %lld ,prime %d)\t", id, prime);
        set(id, prime, FLAG_NONE);
    }
    EndId(const string &endString, bool existsSrfFile = false) : _endMark(EndLabel::UNMARKED) {
        //DBGN("Called EndId(const string %s, %d)\t", endString.c_str(), existsSrfFile);
        char buf[32]; // more than enough for 2^64.[35]

        strcpy(buf, endString.c_str());
        set(buf, existsSrfFile);
    }

    EndId(char *buf, bool existsSrfFile = false) : _endMark(EndLabel::UNMARKED) { /* fast method, buf is modified! */
        //DBGN("Called EndId(%s, %d)\t", buf, existsSrfFile);
        set(buf, existsSrfFile);
    }

protected:
    void set(char *buf, bool existsSrfFile)   /* fast method, buf is modified!!! */
  {   //DBG("EndId::set(char *buf=%s, %d)\n", buf, existsSrfFile);
        char *numID = buf;
        char *endPos = strchr(buf, '.');

        if (!endPos) {
            DIE("Invalid EndId: '%s'\n", buf);
        }
        *endPos = '\0'; // replace '.' with end of string
        if (existsSrfFile) {
#ifdef SCAFF_ID_PREFIX
            assert(strncmp(SCAFF_ID_PREFIX, numID, strlen(SCAFF_ID_PREFIX)) == 0);
            numID = numID + strlen(SCAFF_ID_PREFIX);
#endif
        } else {
#ifdef CONTIG_ID_PREFIX
            assert(strncmp(CONTIG_ID_PREFIX, numID, strlen(CONTIG_ID_PREFIX)) == 0);
            numID = numID + strlen(CONTIG_ID_PREFIX);
#endif
        }

        //DBG("getting id: %s and %s\n", numID, endPos+1);
        set(atoll(numID), atoi(endPos + 1), FLAG_NONE);
    }

//	EndId(Flag flag): _endMark(EndLabel::UNMARKED) {
//		set(INVALID, 5, flag);
//	}
    EndId(const EndId &copy, Flag newFlag) : _endMark(EndLabel::UNMARKED) {
        //DBG("EndId(const EndId copy=%s, newFlag=%d)\n", copy.toString().c_str(), (int) newFlag);
        set(copy._id, copy.getPrime(), newFlag);
    }

public:
    EndId(const EndId &other) {
        *this = other;
    }
    EndId & operator = (const EndId &other) {
        memcpy(this, &other, sizeof(EndId));
        return *this;
    }

    bool operator == (const EndId &other) const {
        return _id == other._id && _prime == other._prime;
    }
    bool operator != (const EndId &other) const {
        return !(*this == other);
    }

    void flip_prime()
    {
        _prime = ~_prime;
    }
    bool isValid() const
    {
        return _id != INVALID;
    }
    ObjectId getID() const
    {
        return _id;
    }
    bool is5prime() const
    {
        return _prime == 1;
    }
    bool is3prime() const
    {
        return !is5prime();
    }
    int8_t getPrime() const
    {
        return is5prime() ? 5 : 3;
    }
    string toString() const
    {
        char buf[128];

        //sprintf(buf, "%lld.%d%s%s", (lld) getID(), getPrime(), getFlagName().c_str(), ((getEndMark() == EndLabel::UNMARKED) ? "" : EndLabel::getName(getEndMark()).c_str() ));
        sprintf(buf, "%lld.%d", (lld)getID(), getPrime());
        return string(buf);
    }

    size_t hash() const
    {
        std::hash<int64_t> hasher;
        return hasher(*((int64_t *)this));
    }

protected:
    EndId::Flag getFlag() const
    {
        if (_divergence) {
            return FLAG_DIVERGENCE;
        } else if (_convergence) {
            return FLAG_CONVERGENCE;
        } else if (_id == INVALID) {
            return FLAG_INVALID;
        } else { return FLAG_NONE; }
    }
    const string getFlagName() const
    {
        const char *ret = NULL;

        switch (getFlag()) {
        case FLAG_DIVERGENCE: ret = "DIVERGENCE"; break;
        case FLAG_CONVERGENCE: ret = "CONVERGENCE"; break;
        case FLAG_INVALID: ret = "INVALID"; break;
        default: ret = "";
        }
        return ret;
    }
    EndLabel::Label getEndMark() const
    {
        return (EndLabel::Label)_endMark;
    }
    void setEndMark(EndLabel::Label mark)
    {
        assert(mark < EndLabel::MAX_END_LABEL);
        assert(sizeof(EndId) == 8);
        _endMark = (uint8_t)mark;
    }
    
    void set(ObjectId id, int8_t prime, EndId::Flag flag)
    {
        //DBG("EndId::set(id=%lld, prime=%d, flag=%d)\n", (lld) id, prime, (int) flag);
        _dummy = 0;
        if (flag != FLAG_NONE) {
            if (flag == FLAG_INVALID && id != INVALID) {
                WARN("set( id=%lld, prime=%d, flag=%d ): FLAG_INVALID should have id==invalid %lld\n", (lld)id, prime, (int)flag, (lld)INVALID);
            }
            _id = id;
            _prime = 1;
            switch (flag) {
            case FLAG_DIVERGENCE: _divergence = 1; break;
            case FLAG_CONVERGENCE: _convergence = 1; break;
            case FLAG_INVALID: break;
            default: DIE("Invalid flag %d!\n", (int)flag);
            }
            return;
        } else {
            assert(id != INVALID);
            _divergence = 0;
            _convergence = 0;
        }
        if (id >= 0) {
            assert(id < INVALID);
        } else {
            assert(id > -INVALID);
        }
        _id = id;
        if (prime == 3) {
            _prime = 0;
        } else if (prime == 5) {
            _prime = 1;
        } else {
            DIE("Invalid prime in EndId::set(id=%lld, prime=%d, flag=%d)\n", (lld)id, (int)prime, (int)flag);
        }
        if (id != getID() || prime != getPrime()) {
            DIE("id %lld != %lld or prime %d != %d\n", (lld)id, (lld)getID(), prime, getPrime());
        }
        assert(id == getID());
        assert(prime == getPrime());
        //DBGN("assigned %s\n", this->toString().c_str());
    }
};

// just a handle to a real End... do not store flags with this protected type
class EndIdAndFlag : public EndId {
public:
    EndIdAndFlag() : EndId() {
    }
    EndIdAndFlag(ObjectId id, int8_t prime) : EndId(id, prime) {
    }
    EndIdAndFlag(const EndId &copy, Flag newFlag = FLAG_NONE) : EndId(copy, newFlag) {
    }
    EndIdAndFlag(char *buf, bool existsSrfFile) : EndId(buf, existsSrfFile) {
    }
    EndId::Flag getFlag() const
    {
        return this->EndId::getFlag();
    }
    EndLabel::Label getEndMark() const
    {
        return this->EndId::getEndMark();
    }
    void setEndMark(EndLabel::Label mark)
    {
        this->EndId::setEndMark(mark);
    }
    void set(char *buf, bool existsSrfFile)
    {
        this->EndId::set(buf, existsSrfFile);
    }
};

class EndIds {
public:
    /* Takes the string 'Contig21911.5<=>Contig53366903.3' and extracts the ID and primes */
    typedef std::array<EndIdAndFlag, 2> EndIds_t;
    EndIds_t end;
    EndIds() : end() {
    }
    EndIds(const EndId &endA, const EndId &endB) {
        end[0] = endA;
        end[1] = endB;
    }
    EndIds(const EndIdAndFlag &endA, const EndIdAndFlag &endB) {
        end[0] = endA;
        end[1] = endB;
    }
    EndIds(const Obj &);
    EndIds(const string &linkedEnds, bool existsSrfFile = false) {
        //DBG("EndIds(%s, %d)\n", linkedEnds.c_str(), existsSrfFile);
        char buf[64];

        strcpy(buf, linkedEnds.c_str());
        set(buf, existsSrfFile);
    }
    EndIds(char *linkedEnds, bool existsSrfFile = false) {
        set(linkedEnds, existsSrfFile);
    }
    bool operator == (const EndIds &other) const {
        return end[0] == other.end[0] && end[1] == other.end[1];
    }
    bool operator != (const EndIds &other) const {
        return !(*this == other);
    }
    EndIds_t &getEnds()
    {
        return end;
    }
    string toString() const
    {
        return end[0].toString() + "<=>" + end[1].toString();
    }
    size_t hash() const
    {
        // Compute individual hash values for first, second and third
        // http://stackoverflow.com/a/1646913/126995
        size_t res = 17;

        res = res * 31 + end[0].hash();
        res = res * 31 + end[1].hash();
        return res;
    }
protected:
    void set(char *buf, bool existsSrfFile)   /* fast method, buf is modified!! */
    {   //DBG("EndIds::set(%s, %d)\n", buf, existsSrfFile);
        char *linkPos = strstr(buf, "<=>");

        if (linkPos == NULL) {
            DIE("Invalid linkedEnds: '%s'\n", buf);
        }
        *linkPos = '\0';
        char *secondEnd = linkPos + 3;
        end[0].set(buf, existsSrfFile);
        end[1].set(secondEnd, existsSrfFile);
    }
};

namespace std
{
template <> struct hash<EndId> {
    size_t operator() (const EndId &k) const
    {
        return k.hash();
    }
};
template <> struct hash<EndIds> {
    size_t operator() (const EndIds &k) const
    {
        return k.hash();
    }
};
}

class EndTie
{
public:
    EndTie() : tiedEndId(), gapEstimate(), gapUncertainty(), __isSplinted(), nGapLinks() {
    }
    EndTie(EndId _tiedEndId, int _nGapLinks, int16_t _gapEstimate, int16_t _gapUncertainty, bool _isSplinted) :
        tiedEndId(_tiedEndId), gapEstimate(_gapEstimate), gapUncertainty(_gapUncertainty), __isSplinted(_isSplinted), nGapLinks(_nGapLinks) {
    }
    EndTie(const EndTie &other) {
        *this = other;
    }
    EndTie &operator = (const EndTie &other) {
        memcpy(this, &other, sizeof(EndTie));
        return *this;
    }

    int getNGapLinks() const
    {
        return nGapLinks;
    }
    bool isSplinted() const
    {
        return __isSplinted == 1;
    }

    // incomplete .. will lookup in the onoObjectsMap
    End &getTiedEnd();
    const End &getTiedEnd() const;
    EndId getEndId() const
    {
        return tiedEndId;
    }

    vector<EndId> getInverseTiedIds() const;
  
public:
    EndId tiedEndId; //i.e the identifier to find the target end vi objectMap.getEnd()
    int16_t gapEstimate;
    int16_t gapUncertainty;
    uint32_t __isSplinted : 1;
    uint32_t nGapLinks : 31;
};

class End
{
public:

    End(ObjectId, int8_t);
    End(const End &copy, EndId::Flag newFlag);
    End(const End &copy);

    End &operator = (const End &copy) {
        endId = copy.endId;
        ties = copy.ties;
        return *this;
    }
    ~End()
    {
    }


    // member data fields
    EndIdAndFlag endId;
    EndIdAndFlag getEndId() const
    {
        return endId;
    }
    
    // FIXME make ties a map for quicker searches..
    vector<EndTie> ties;

    // member functions
    void flip_prime();
    EndLabel::Label getEndMark() const;
    void setEndMark(EndLabel::Label mark);
    ObjectId getID() const;
    int8_t getPrime() const;

    void add_tie(EndId _te, int nLinks, int gapEst, int gapUncert, bool isSplint);
    void delete_tie(EndId _te);
    const EndTie *get_tie(EndId _te) const;
    EndTie *get_tie(EndId _te);
    const EndTie *get_tie(const End&) const;
    EndTie *get_tie(const End&);
    vector<EndTie> export_all_ties() const;
    void correct_gap_estimate(EndId _te, int newGapEst) const;
    void correct_gap_estimate(const End &_te, int newGapEst) const;
    void print_ties();
    void print_end();
    string toString();
};


class Obj
{
    // the main class for storing contig or scaffold data including two pointers to the 5' and 3' End objects which get constructed automatically when and Obj instance is created.

public:

    Obj(ObjectId, int, float); //default constructor
    ~Obj()
    {
    }

    enum obj_label_t : uint8_t { UNLABELED, DD_TIG, SD_TIG, COPY_TIG, TRUE_SCAFFOLD, DIPLOTIG };

    End &getEnd(int8_t prime);
    End &getEnd(EndId endId);

    int16_t incrementCopy();

    ObjectId id;
    ObjectId parentId; // LLONG_MIN unless this object is a copy of another object
    End end5;
    End end3;
    int len;
    float depth;
    int16_t nCopiesMade;
    obj_label_t label;
};

class objectsHash_t : public unordered_map<ObjectId, Obj> {
public:
    const End &getEnd(EndId endId) const
    {
        auto endIt = this->find(endId.getID());

        if (endIt == this->end()) {
            DIE("Attempt to getEnd from missing object endId %s\n", endId.toString().c_str());
        }
        const Obj &obj = endIt->second;
        if (endId.is5prime()) {
            return obj.end5;
        } else {
            return obj.end3;
        }
    }
    End &getEnd(EndId endId)
    {
        return const_cast<End&>(static_cast<const objectsHash_t&>(*this).getEnd(endId));
    }
};

End &getEnd(EndId endId);
End &getEnd(EndTie endTie);


typedef unordered_map<ObjectId, bool> suspendedHash_t;


class Scaf
{
public:
    Scaf(ObjectId); //default constructor

    typedef struct contig_t {
        string contName; // keep original contig names ,e.g. Contig99.cp2
        int    startCoord;
        int    endCoord;
        float  depth;
        char   ori;
    } contig_t;

    typedef struct gap_t {
        int16_t gapSize;
        int16_t gapUncertainty;
    } gap_t;


    ObjectId scafId;
    vector<contig_t> Contigs;
    vector<gap_t> Gaps;


    // member functions:

    void push_contig(string name, char ori, int start, int end, float dep);
    void push_gap(int16_t size, int16_t uncert);
    void reverse_order_ori();
    string form_contig_string(int idx);
    string form_copyTig_string(int idx, int ver);
    string form_gap_string(int idx);
};


//typedef unordered_map<string, string> srf_t;  // stores input scaffold contents from srf file
typedef unordered_map<ObjectId, Scaf *> scaffsHash_t;             // stores scaffold info

typedef unordered_map<ObjectId, ref_hit_t> refHitsHash_t;         // stores contig ids and coordinates corresponding to a reference db/model hit

typedef unordered_map< EndIds, vector<link_info_t> > linksHash_t; // stores linkage info for a single pair of endIds (could be multple links - SPLINT,SPAN,etc)

typedef unordered_map<EndId, EndId> tiedEndsHash_t;               // simple hash of tied endIds
typedef unordered_map<EndId, EndIdAndFlag> endLocks_t;
typedef unordered_map<EndId, vector<EndId> > tiedEndListsH_t;     // hash of endIds and a list of endIds tied to it
typedef unordered_map<EndId, float> scoredEndsHash_t;               
typedef vector<EndId> path_t;                                     // a path through the graph with EndIds as nodes
typedef vector<ObjectId> objectsVec_t;

typedef struct {
  path_t path;
  int distanceTraversed = 0; // doesn't include lengths of terminal objects  
} path_info_t;


// Comparator declarations

class sortedByElement_t {
public:
    int len;
    float depth;
    ObjectId id;
    sortedByElement_t(const Obj &obj) : len(obj.len), depth(obj.depth), id(obj.id) {
    }
};

typedef std::vector<sortedByElement_t> sortedBy_t;

struct by_length_depth {
    bool operator() (const sortedByElement_t a, const sortedByElement_t b) const;
};

struct by_dist_len {
    // compares gap estimates and returns the closest;  if equal returns the SHORTER;  if equal, returns the least id
    bool operator() (const EndTie &a, const EndTie &b) const;
};

struct by_dist_depth {
    // compares gap estimates and returns the closest;  if equal, looks at the depths and returns the HIGHEST; if equal, returns the least id
    bool operator() (const EndTie &a, const EndTie &b) const;
};


struct by_end_name {
    bool operator() (const EndId &a, const EndId &b) const;
};

struct by_path_len {
  bool operator() (const path_info_t &a, const path_info_t &b) const; 
};



// utility functions

ObjectId create_copy_object(Obj *_object, copyTracker_t& copyTracker, bool existsSrfFile);
void copy_tie_info(const End *_tieSourceEnd, End *_myEnd, End *_tiedToEnd);
void reassign_tie_info(End *_tieSourceEnd, End *_myEnd, End *_tiedToEnd);
bool mutual_unique_best(const End *_e1, const End *_e2, suspendedHash_t& suspended, tiedEndsHash_t& bestTie, tiedEndListsH_t& bestTiedBy);

End &other_end(ObjectId objID, int prime);
End &other_end(const End &_end);
End &other_end(EndId endId);

bool is_splinted(const End &_e1, const End &_e2);
bool is_splinted(const End &_e1, EndId endId2);
bool is_splinted(const End *_e1, const End *&_e2);
bool is_splinted(const End *_e1, EndId endId2);

vector<string> tokenize(const string& in, string sep);

int64_t parse_linkFile_and_build_ties(string linksFile, int pairThreshold, int minCombinedObjSize, bool existsSrfFile);
void place_suspended(suspendedHash_t&, tiedEndsHash_t&, tiedEndListsH_t&, copyTracker_t&, bool);
void lock_ends(endLocks_t &endLocks, tiedEndsHash_t &bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, const objectsHash_t &objects);
int64_t build_scaffolds(endLocks_t& endLocks, const string srfOutFilename, bool existsSrfFile, copyTracker_t& copyTracker, int srfOffset);
float find_peak_depth(const unordered_map<int, int>& depthHist);
int path_suffix_overlap(path_t&, path_t&);

bool ifsIsEmpty(ifstream&);
bool fileIsEmpty(FILE*);

  


// Externs
extern objectsHash_t onoObjectsMap; // global hash table to store contig/scaffold metadata
extern scaffsHash_t scaffInfo;      // global hash table to store scaffold contents (contigs+gaps)


#endif // _ONO_COMMON_H_
