// FILENAME:  ono_meta.cpp
// PROGRAMMER: Eugene Goltsman
// DATE:12/11/2017
// REQUIRED:
// PURPOSE:   scaffolder for metagenomic assembly in Meraculous/Hipmer framework

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>

extern "C" {
#include "optlist.h"
}

#include "poc_common.h"
#include "common.h"
#include "log.h"

#include "ono_common.h"
#include "ono_meta.h"


using namespace std;


#define MAX_STRAIN 1        //determines how much two links can overlap before they're deemed a tie collision
int *LARGE = &maxAvgInsSize; // or make LARGE an arbitrary cutoff here
  
namespace onoMeta {
// funcitons with same names/args as those below exist in other versions of ono, thus the separate namespace

  
// up-front declarations

void parse_crfFile(string);
void parse_srfFile(string);
void parse_rRNAHits(string, refHitsHash_t &, bool);

void mark_end(End&, EndMarkCounts&, const refHitsHash_t&);
bool resolve_tie_collision(const End &, EndId, EndId, int, int);
  void find_bestTies(const sortedBy_t&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, copyTracker_t&, bool, refHitsHash_t&);
End *best_tie(const End&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&);
  void traverse_rRNA(path_t&,  const End&, objectSeenCnt_t&, int&, int&, float, bool&);
bool end_supports_all(EndId, const scoredEndsHash_t&);
float ext_score(const path_t&, const vector<EndId>&, const EndId);
  bool build_contender_list(const path_t&, scoredEndsHash_t&, const vector<EndId>&, const vector<EndTie>&, const objectSeenCnt_t&, const float, float*);
void find_frontier_long(vector<EndId>*, EndIdAndFlag, objectSeen_t&, int);
  void find_all_long(vector<EndId>*, EndIdAndFlag, objectSeen_t&, int, int);
void convert_path_to_bestTies(path_t&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&);
path_t doctor_path(const path_t&, copyTracker_t&, tiedEndsHash_t&, bool);
  void inspect_ties(End&);
  void trash_wonky_spans(End&);
  void trash_lonely_spans(End&, int);
int taxonomy_divergence_level(const string&, const string&);
string taxonomy_common(const string&, const string&);






#define AVG_CRF_LINE_SIZE 26
void parse_crfFile(string crfFile)
{
    // For every contig, create a struct and populate it with contig info, including creation of two new "End" instances
    // Then add this structure to the hash of assembly objects
    LOGF("Parsing CRF %s\n", crfFile.c_str());

    ifstream infile(crfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", crfFile.c_str());
    }

    int64_t estimatedEntries = (get_file_size(crfFile.c_str()) / AVG_CRF_LINE_SIZE) * 105 / 100 + 1000; // add 5% + 1000
    onoObjectsMap.reserve(estimatedEntries);

    static const int maxLine = 180;
    char l[maxLine];
    vector<char *> words;
    while (infile.getline(l, maxLine - 1)) {
        words.clear();
        char *savePtr, *token;
        token = strtok_r(l, "\t", &savePtr);
        while (token != NULL) { // read tab deliminated tokens
            words.push_back(token);
            token = strtok_r(NULL, "\t", &savePtr);
        }
        assert(words.size() >= 3);

        // convert contig names like 'Contig123' to int64
        char *cName = words[0];
        ObjectId id;
#ifdef CONTIG_ID_PREFIX
        assert(strncmp(cName, CONTIG_ID_PREFIX, strlen(CONTIG_ID_PREFIX)) == 0);
        cName += strlen(CONTIG_ID_PREFIX);
#endif
        id = atol(cName);
        int len = stoi(words[1]);
        float depth = atof(words[2]);
        Obj newObj(id, len, depth);
        onoObjectsMap.insert(std::make_pair(id, newObj));
    }
    LOGF("Found %lld objects\n", (lld)onoObjectsMap.size());
}


// scaffInfo: key = "Scaffold$objectId"
void parse_srfFile(string srfFile)
{
    LOGF("Parsing SRF %s\n", srfFile.c_str());
    ifstream infile(srfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", srfFile.c_str());
    }

    if( ifsIsEmpty(infile)) {
      DBG("Empty srf file");
      return;
    }
    
    static const int maxLine = 180;
    char l[maxLine];
    ObjectId currId = -1;
    Scaf *_currScaf = NULL;
    Obj __tmpObj(0, 0, 0.0);
    Obj *_currObj = NULL;
    int scaffRealBases = -1;
    float scaffDepSum = 0;

    vector<char *> words;
    while (infile.getline(l, maxLine - 1)) {
        words.clear();
        char *token, *savePtr = NULL;
        token = strtok_r(l, "\t", &savePtr);
        while (token != NULL) {
            words.push_back(token);
            token = strtok_r(NULL, "\t", &savePtr);
        }
        assert(words.size() >= 4);
        int n = words.size();
        if (n > 6 || n < 4) {
            DIE("Error:  srf file not in expected format!\n");
        }


        //Scaffold459     CONTIG4 -Contig2948227  11      109     3.000000
        //Scaffold459     GAP4    -97     1
        char *scaffName = words[0];

#ifdef SCAFF_ID_PREFIX
        assert(strncmp(scaffName, SCAFF_ID_PREFIX, strlen(SCAFF_ID_PREFIX)) == 0);
        scaffName += strlen(SCAFF_ID_PREFIX);
#endif
        ObjectId id = atoll(scaffName);

        if (id != currId) {
            if (_currObj != NULL) {
                // add record to the corresponding hashes and reset totals
                _currObj->depth = scaffDepSum / scaffRealBases;
                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));
                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));

                scaffDepSum = 0;
                scaffRealBases = 0;
            }

            // initialize new objects
            _currScaf = new Scaf(id);
            _currObj = &__tmpObj;
            *_currObj = Obj(id, 0, 0.0);
        }

        char *indexStr = words[1];
        char *found = strstr(indexStr, "CONTIG");
        if (found != NULL) {
            assert(n >= 6);
            char ori = words[2][0];
            assert(ori == '+' || ori == '-');
            string cName = words[2] + 1;
            int start = atoi(words[3]);
            int end = atoi(words[4]);
            float dep = atof(words[5]);
            int cLen = end - start + 1;
            scaffRealBases += cLen;
            scaffDepSum += float(cLen * dep);

            _currObj->len = end;
            _currScaf->push_contig(cName, ori, start, end, dep);
        } else { // GAP
            int size = atoi(words[2]);
            int uncert = atoi(words[3]);
            _currScaf->push_gap(size, uncert);
        }

        currId = id;
    }

    _currObj->depth = scaffDepSum / scaffRealBases;
    scaffInfo.insert(std::make_pair(currId, _currScaf));
    onoObjectsMap.insert(std::make_pair(currId, *_currObj));

    LOGF("Found %lld objects\n", (lld)onoObjectsMap.size());
}

void inspect_ties(End &_end)
{

    int maxGap = maxAvgInsSize + (3*maxAvgInsSDev);
    int minGap = -(merSize -2); 
    vector<EndTie> ties = _end.export_all_ties(); // a copy of the vector of end ties since we'll be manipulating it, sorting, etc
    if (ties.size() < 2) {
        return;
    }
    End &_my_other_end = other_end(_end);
    unsigned int nTies = ties.size();
    unordered_set<EndId> beenCorrected;
    
    bool orderConfirmed = false;
    while (! orderConfirmed) {

      
      std::sort(ties.begin(), ties.end(), by_dist_len());     // sort ties by distance
      DBG2N("Ties from end %s sorted by distance:\n", _end.toString().c_str());

      orderConfirmed = true;  //assume confirmed unless proven otherwise
      
      for (size_t i = 0; i < nTies; i++) {
	int newGapEstimate_i = ties[i].gapEstimate;  	
	EndId tiedEnd_i = ties[i].tiedEndId;
	ObjectId ID_i = tiedEnd_i.getID();
	End &_otherEnd_i = other_end(tiedEnd_i);
	DBG2N("\tTied end %s:",tiedEnd_i.toString().c_str());
	
	if (beenCorrected.count(tiedEnd_i)){ // avoid infinite corrections
	  DBG2N("\tConfirmed (previously corrected).\n");
	  continue;
	}
	if (is_splinted(_end, tiedEnd_i)) {
	  DBG2N("\tConfirmed (splint).\n");
	  continue;
	}

	if (_otherEnd_i.get_tie(_my_other_end) != NULL) {  // opposite end loops back to the starting object; most likely a tandem repeat.
	  DBG2N("\tTandem repeat signature - stop here.\n");
	  return;
	  //	TODO:  handle tandem reps with copy-tig + tie reassignment
	}
	
	// A tie to i can get resized UP only if the opposite end of i doesn't tie to the next downstream tie.  e.g.   e.5 <...> 3.i.5 <=> 3.j.5 <...>   (can't move i fwd) 
	// Likewise, a tie to i can get resized DOWN only if i doesn't tie to the opposite end of the previous tie of _end. e.g.   e.5 <...> 3.h.5 <=> 3.i.5 <...>   (can't move i backward
	// In such cases, consider i confirmed for now;  it can still get resized later if j or h get moved around.
	// This prevents a case where a multi-node loop gets resized indefinitely bc nodes keep invalidating each other

	bool canMoveFwd = true;
	bool canMoveBkwd = true;
	if (i==nTies || (i < nTies - 1 && _otherEnd_i.get_tie(ties[i+1].tiedEndId) != NULL) ) {
	  DBG2N("\tCan't move fwd.\n");
	  canMoveFwd = false;
	}
	if (i==0 || (i > 0 && other_end(ties[i-1].tiedEndId).get_tie(tiedEnd_i) != NULL) ) {
	  DBG2N("\tCan't move backward.\n");
	  canMoveBkwd = false;
	}

	int voteToResize_i = 0; // a +1 vote means make gap to i larger;  -1 means make smaller;  zero - leave unchanged
	// If evidence is found for both up and down-sizing, votes will cancel each other!
	
	// First, check this tied end (i) for ties to OPPOSITE ends of downstream ties of _end (j), indicating that i should be placed AFTER j.
	if (canMoveFwd) {
	  for (size_t j = i+1; j < nTies; j++) {
	    EndId tiedEnd_j = ties[j].tiedEndId;
	    End &_otherEnd_j = other_end(tiedEnd_j);
	    DBG2N("\t\tCheck fwd vs %s; ",tiedEnd_j.toString().c_str());
	    
	    if (ties[j].gapEstimate == ties[i].gapEstimate) {
	      continue; 
	    }
	    if (_otherEnd_i.get_tie(tiedEnd_j) != NULL) {
	      // e.g. x.5 -> i.3 && i.5 -> j.3   confirms x-i-j order but not necessarily gap size
	      continue;
	    }

	    //  ties between end i and OTHER end of j tells us that the gap estimates need to be corrected
	    if (_otherEnd_j.get_tie(tiedEnd_i) != NULL) {
	      int newGapPrelim;
	      ObjectId ID_j = tiedEnd_j.getID();	      
	      if (is_splinted(_otherEnd_j, tiedEnd_i)) { 
		// make new gap estimate be the distance _end<=>j, plus j.len plus the distance j<=>i 
		newGapPrelim = ties[j].gapEstimate + onoObjectsMap.at(ID_j).len + _otherEnd_j.get_tie(tiedEnd_i)->gapEstimate;
	      } else {
		// don't rely on the j-> gap estimate and just make i immediate follow j
		newGapPrelim = ties[j].gapEstimate + onoObjectsMap.at(ID_j).len - (merSize - 2);
	      }
	      if (newGapPrelim > maxGap) {
		DBG2N(" [resized gap would be illegal: newGap=%d,  max:%d]", newGapPrelim, maxGap);
		continue;
	      } else {
		newGapEstimate_i = newGapPrelim;
	      }

	      
	      DBG2N(" [newGapEst = %d]", newGapEstimate_i);
	    }
	  }
	  if (newGapEstimate_i > ties[i].gapEstimate) {
	    voteToResize_i++;
	  }
	}
	// Now, check OPPOSITE end of this tied end (i) for ties to upstream ties (h), indicating that end i should be placed BEGORE h.
	if (canMoveBkwd) {
	  for (int h = i-1; h >= 0; h--) {
	    EndId tiedEnd_h = ties[h].tiedEndId;
	    End &_otherEnd_h = other_end(tiedEnd_h);
	    DBG2N("\t\tCheck backwd vs %s; ",tiedEnd_h.toString().c_str());
	    
	    if (ties[h].gapEstimate == ties[i].gapEstimate) {
	      continue; 
	    }
	    if (_otherEnd_h.get_tie(tiedEnd_i) != NULL) {
	      // e.g. x.5 -> h.3 && h.5 -> i.3   confirms x-h-i order but not necessarily gap size
	      continue;
	    }
	    if (_otherEnd_i.get_tie(tiedEnd_h) != NULL) {
	      int newGapPrelim;
	      if (is_splinted(_otherEnd_i, tiedEnd_h)) {
		// make new gap estimate be the distance _end<=>h, less combinded distance i<=>h and i.len
		newGapPrelim = ties[h].gapEstimate -  (_otherEnd_i.get_tie(tiedEnd_h)->gapEstimate + onoObjectsMap.at(ID_i).len) ;
	      } else {
		// don't rely on the h<=>i gap estimate and just make i immediate precede h
		newGapPrelim = ties[h].gapEstimate - ( -(merSize - 2) + onoObjectsMap.at(ID_i).len) ;
	      }
	      
	      if (newGapPrelim < minGap) {
		DBG2N(" [resized gap would be illegal: newGapEst=%d", newGapPrelim);
		continue;
	      } else {
		newGapEstimate_i = newGapPrelim;
	      }
	      
	      DBG2N(" [newGapEst = %d]", newGapEstimate_i);
	    }
	  }
	  if (newGapEstimate_i < ties[i].gapEstimate ) {
	    voteToResize_i--;
	  }
	}

	
	if (voteToResize_i != 0) {
	  DBG2N("\n\t gap will be resized, order will change!\n");	  
	  // correct gap estimate for tie_i 
	  ties[i].gapEstimate = newGapEstimate_i;
	  // since ties[] is a copy, correct original tie and its reciprocal
	  _end.correct_gap_estimate(tiedEnd_i, newGapEstimate_i);
	  getEnd(tiedEnd_i).correct_gap_estimate(_end, newGapEstimate_i);
	  
	  beenCorrected.insert(tiedEnd_i);
	  orderConfirmed = false;
	  break; // we've made gap/order corrections, so start all over
	}
	DBG2N("\tConfirmed (no conflicts).\n");	
      }
      if (orderConfirmed) {
	DBG2N("\n\t Order confirmed.\n");
      }
    }
}

void trash_wonky_spans(End &_end)
{
  // if a span-only tie is followed by a splinted tie, the first one is likely to do more harm (due to gap estimate uncertainty) and little good 
  // so we eliminate it from the end's tie list

  vector<EndTie> ties = _end.export_all_ties();  // a copy of the vector of end ties 
  if (ties.size() < 2) {
    return;
  }
  unsigned int nTies = ties.size();

  std::sort(ties.begin(), ties.end(), by_dist_len());     // sort ties by distance

  // int farthestSplintIdx = 0;
  // for (int i = nTies-1; i >= 0; i--) {
  //   if (is_splinted(_end, ties[i].tiedEndId)) {
  //     farthestSplintIdx = i;
  //     break;
  //   }
  // }

  
  //  for (size_t i = 0; i < farthestSplintIdx; i++) {
  for (size_t i = 0; i < nTies-1; i++) {
    EndId tiedEnd_i = ties[i].tiedEndId;
    EndId tiedEnd_next = ties[i+1].tiedEndId;
    if ( !is_splinted(_end, tiedEnd_i)  && is_splinted(_end, tiedEnd_next)) {
      //trash i and its reciprocal tie
      _end.delete_tie(tiedEnd_i);
      getEnd(tiedEnd_i).delete_tie(_end.getEndId());
      
      DBG2N("Troubleshome span-only tie %s<=>%s deleted.\n", _end.endId.toString().c_str(), tiedEnd_i.toString().c_str());
    }
  }  
}

void trash_lonely_spans(End &_end, int maxUncertainty)
{
  // if there's only one tie and it's a span, trash it
    assert(_end.ties.size() == 1);
    if (_end.ties[0].gapUncertainty < maxUncertainty ) {
      return;
    }
    End &_tiedEnd = getEnd(_end.ties[0].tiedEndId);
    if (! is_splinted(_end, _end.ties[0].tiedEndId)
	&&
	_tiedEnd.ties.size() == 1) {  // check if tied end has other ties

      
      _end.delete_tie(_end.ties[0].tiedEndId);
      _tiedEnd.delete_tie(_end.getEndId());
      assert(!_end.ties.size());
      DBG2N("Lonely span tie of high uncertainty %s<=>%s deleted.\n", _end.endId.toString().c_str(), _tiedEnd.toString().c_str());
    }
}

bool resolve_tie_collision(const End &_end, EndId _tiedEnd_i, EndId _tiedEnd_j, int start_i, int start_j)
{
    End &_my_other_end = other_end(_end);
    End &_otherEnd_i = other_end(_tiedEnd_i);
    End &_otherEnd_j = other_end(_tiedEnd_j);

    ObjectId ID_i = _tiedEnd_i.getID();
    ObjectId ID_j = _tiedEnd_j.getID();

    int len_i = onoObjectsMap.at(ID_i).len;
    int len_j = onoObjectsMap.at(ID_j).len;

    if (_otherEnd_i.get_tie(_my_other_end) != NULL
	||
	_otherEnd_j.get_tie(_my_other_end) != NULL ) {  // tandem repeat signatures
      
      return false;
    }
    
    //use splint info to detect false strains
    if (is_splinted(_otherEnd_i, _tiedEnd_j)) {
        return true;
    }

    //  any ties between i and j suggest that there is no collision and that the gap estimates need to be adjusted 
    
    if (_otherEnd_i.get_tie(_tiedEnd_j) != NULL) {
          // decide whether to make i closer or push j farther
      if ( (_end.get_tie(_tiedEnd_i)->gapUncertainty > _end.get_tie(_tiedEnd_j)->gapUncertainty)
           && (start_j - len_i - 1  + (merSize - 2) >= -(merSize - 2)) ) { //new gap to i would be legal
        int newGapEstimate_i = start_j - len_i - 1 + (merSize - 2); // place i before j
        _end.correct_gap_estimate(_tiedEnd_i, newGapEstimate_i);
        getEnd(_tiedEnd_i).correct_gap_estimate(_end.endId, newGapEstimate_i);
        return true;
      }
      else if (! is_splinted( _end, _tiedEnd_j)) {
        int newGapEstimate_j = start_i + len_i - (merSize - 2); // push back j to follow i
        _end.correct_gap_estimate(_tiedEnd_j, newGapEstimate_j);
        getEnd(_tiedEnd_j).correct_gap_estimate(_end.endId, newGapEstimate_j);
        return true;
      }
      
      
    }

    //******** logic below already performed in  inspect_ties() ******//
    
    // if (_otherEnd_j.get_tie(_tiedEnd_i) != NULL) {
    //   // decide whether to bring j closer or push i farther

    //   if ( (_end.get_tie(_tiedEnd_j)->gapUncertainty > _end.get_tie(_tiedEnd_i)->gapUncertainty)
    //        && (start_i - len_j - 1  + (merSize - 2) >= -(merSize - 2)) ) { //new gap to j would be legal
    //     int newGapEstimate_j = start_i - len_j - 1 + (merSize - 2); // place j before i
    //     _end.correct_gap_estimate(_tiedEnd_j, newGapEstimate_j);
    //     getEnd(_tiedEnd_j).correct_gap_estimate(_end.endId, newGapEstimate_j);
    //     return true;
    //   }
    //   else if (! is_splinted( _end, _tiedEnd_i)) {
    //     int newGapEstimate_i = start_j + len_j - (merSize - 2); // push back i to follow j 
    //     _end.correct_gap_estimate(_tiedEnd_i, newGapEstimate_i);
    //     getEnd(_tiedEnd_i).correct_gap_estimate(_end.endId, newGapEstimate_i);
    //     return true;
    //   }
    // }
    
    return false;
}


void mark_end(End &_end, EndMarkCounts& endMarkCounts, const refHitsHash_t& rRNAHits)
{

  _end.setEndMark(EndLabel::UNMARKED);  //default

  vector<EndTie> ties = _end.export_all_ties(); // a copy of the vector of end ties since we'll be manipulating it, sorting, etc
    if (!ties.size()) {
        _end.setEndMark(EndLabel::NO_TIES);
        endMarkCounts[EndLabel::NO_TIES]++;
        return;
    }

    for (auto tie : ties) {
        if (!tie.tiedEndId.isValid()) {
            continue;
        }
        End *_tiedEnd_i = &getEnd(tie.tiedEndId);
        if (_tiedEnd_i->getID() == _end.getID()) {
            _end.setEndMark(EndLabel::SELF_TIE);
            endMarkCounts[EndLabel::SELF_TIE]++;
            return;
        }
    }

    
    ObjectId objID = _end.getID();
    auto objInrRNAHits = rRNAHits.find(objID);
    if (objInrRNAHits != rRNAHits.end()) {
        auto & x = objInrRNAHits->second;
        int closestHit = (_end.getPrime() == 5) ? x.start : (onoObjectsMap.at(objID).len - x.end + 1);
        int hitLen = x.end - x.start + 1;
        if (closestHit <= merSize && hitLen >= merSize) { //these are pretty arbtitrary thresholds; the idea is to make sure that rRNA hits extend close to the end we're looking at.
	  _end.setEndMark(EndLabel::RRNA);
	  endMarkCounts[EndLabel::RRNA]++;
	  return;
        }
    }

    unsigned int nTies = ties.size();
    if (nTies < 2) {
      return;
    }

    
    // if multiple ties, check for tie collisions (forks)

    std::sort(ties.begin(), ties.end(), by_dist_len());     // sort ties by distance
    
    for (size_t i = 0; i < nTies - 1; ++i) {
      ObjectId ID_i = ties[i].tiedEndId.getID();
      int start_i = ties[i].gapEstimate;
      int end_i = start_i + onoObjectsMap.at(ID_i).len - 1;
      int uncertainty_i = ties[i].gapUncertainty;

      int start_j = ties[i + 1].gapEstimate;
      int uncertainty_j = ties[i + 1].gapUncertainty;

      int overlap = end_i - start_j + 1;
      if (overlap > merSize - 2) {
	int excessOverlap = overlap - (merSize - 2);
	float strain = float(excessOverlap) / float(uncertainty_i + uncertainty_j - 1);
	if (strain > MAX_STRAIN) {
	  if (resolve_tie_collision(_end, ties[i].tiedEndId, ties[i + 1].tiedEndId, start_i, start_j)) {
	    continue;
	  } else {
	    _end.setEndMark(EndLabel::TIE_COLLISION);
	    endMarkCounts[EndLabel::TIE_COLLISION]++;
	    //	    cerr << "TIE_COLLISION: " << _end.toString() << " -> " << ties[i].tiedEndId->toString()<< " ? " << ties[i+1].tiedEndId->toString() << endl;
	  }
	  return;  // first collision encounteres is enough to label the end, so we retrun here
	}
      }
    }
}


int taxonomy_divergence_level(const string& txAstr, const string& txBstr)
{
    int level;
    string sep = ";";

    vector<string> txA = tokenize(txAstr, sep);
    vector<string> txB = tokenize(txBstr, sep);

    vector<string> *shortest = (txA.size() < txB.size()) ? &txA : &txB;
    for (size_t i = 0; i <= shortest->size(); i++) {
        level = i + 1; //we want it one-based
        if (txA[i] != txB[i]) {
            return level;
        }
    }

    return 0;
}


string taxonomy_common(const string& txAstr, const string& txBstr)
{
    string sep = ";";

    vector<string> txA = tokenize(txAstr, sep);
    vector<string> txB = tokenize(txBstr, sep);

    string common;

    vector<string> *shortest = (txA.size() < txB.size()) ? &txA : &txB;
    for (size_t i = 0; i <= shortest->size(); i++) {
        if (txA[i] != txB[i]) {
            return common;
        }
        common += (*shortest)[i] + ";";
    }
    return common;
}



void parse_rRNAHits(string refFile, refHitsHash_t& rRNAHits, bool existsSrfFile)
{
    LOGF("Parsing rRNAHits %s\n", refFile.c_str());

    ifstream infile(refFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", refFile.c_str());
    }

    string l;
    vector<string>words;
    while (getline(infile, l)) {
        if (l.empty()) {
            continue;
        }
        istringstream stm(l);
        string word;
        int n = 0;
        while (stm >> word) { // read white-space delimited tokens one by one
            words.push_back(word);
            n++;
        }

        if (n < 4) {
            DIE("Error:  reference file not in expected format!  Should be at least four fields. \n");
        }

        string cName = words[0];
        ObjectId id;
        if (existsSrfFile) {
#ifdef SCAFF_ID_PREFIX
            string prefix = SCAFF_ID_PREFIX;
            cName = cName.substr(prefix.size());
#endif
        } else {
#ifdef CONTIG_ID_PREFIX
            string prefix = CONTIG_ID_PREFIX;
            cName = cName.substr(prefix.size());
#endif
        }

        id = atol(cName.c_str());
        int start = stoi(words[1]);
        int end = stoi(words[2]);

        const char *strand = words[3].c_str();
        if (*strand == '-') {  //make coords relative to 5'
            int cLen = onoObjectsMap.at(id).len;
            start = cLen - start + 1;
            end = cLen - end + 1;
        }

        string taxonomy;
        if (n > 4) {
            taxonomy = words[4];
        }
        words.clear();

        auto idInrRNAHits = rRNAHits.find(id);
        if (idInrRNAHits != rRNAHits.end()) {   // A contig may have multiple hits (lsu,ssu,etc);

            auto & hit = idInrRNAHits->second;
	    
	    if (!taxonomy.empty()) {
	      string thisTaxonomy = taxonomy;
	      string storedTaxonomy = hit.taxonomy;
	      if (storedTaxonomy != thisTaxonomy) {
                if (thisTaxonomy.find(storedTaxonomy) != std::string::npos) {
		  // stored taxonomy is a substring of this one
		  hit.taxonomy = thisTaxonomy;
                } else {
		  // otherwise assume divergence and update record to the common substring
		  hit.taxonomy = taxonomy_common(hit.taxonomy, thisTaxonomy);
                }
	      }


	      size_t nLevels = std::count(thisTaxonomy.begin(), thisTaxonomy.end(), ';');
	      size_t nLevels_stored = std::count(storedTaxonomy.begin(), storedTaxonomy.end(), ';');
	      if (nLevels > nLevels_stored) {
                hit.taxonomy = thisTaxonomy;
	      }
	    }

            //update start/end positions to maximal range
            if (start < hit.start) {
                hit.start = start;
            }
            if (end > hit.end) {
                hit.end = end;
            }
        } else {
            ref_hit_t hit;
            hit.start = start;
            hit.end = end;
            hit.taxonomy = taxonomy;
            rRNAHits[id] = hit;
        }
    }
    LOGF("Stored rRNA hits: %lld\n", (lld)rRNAHits.size());
}



void gather_adjacent_splints(const End& sourceEnd, vector<EndTie>& adjacentSplints, vector<EndTie> ties)
{
    // Find closest splint(s)
    // If multiple splints overlap excessively with the closest one, report them also

  std::sort(ties.begin(), ties.end(), by_dist_depth());

  int closest_i = 0;
  while  (closest_i < ties.size() && !ties[closest_i].isSplinted()) {
    closest_i++;
  }
  if (closest_i >= ties.size())
    return;
  
  adjacentSplints.push_back(ties[closest_i]);

  int start_closest = ties[closest_i].gapEstimate;
  ObjectId ID0 = ties[closest_i].tiedEndId.getID();
  int end0 = start_closest + onoObjectsMap.at(ID0).len - 1;
  for (int i = closest_i+1; i < ties.size(); ++i) {
    if  (! ties[i].isSplinted()) 
      continue;
    
    int start_i = ties[i].gapEstimate;
    if (start_i == start_closest) {
      adjacentSplints.push_back(ties[i]);
      continue;
    } else if (sourceEnd.getEndId().getEndMark() == EndLabel::TIE_COLLISION && ties[i].isSplinted()) {
      // determine if this tie overlaps/collides with the first (closest) tie
      int overlap = end0 - start_i + 1;
      int excessOverlap = overlap - (merSize - 2); 
      if (excessOverlap > MAX_STRAIN) {
	DBG2N("[%s] Including non-adjacent splint w. excessive overlap of %d..\n", __func__, overlap);
    	adjacentSplints.push_back(ties[i]);
      } else {
    	break;  // no excessive overlap. no need to go further
      }
    } else {
      break;
    }
  }
}

void gather_farthest_splints(vector<EndTie>& farthestSplints, vector<EndTie> ties)
{
    // find farthest splint(s)
    //   If multiple equidistant, put highest depth first;  later we will want to traverse high-depth branches first

    // Note:  SPLINT links have gap estimate taken from highest frequency splint (bmaToLinks)

  std::sort(ties.begin(), ties.end(), by_dist_depth());
  int farthest = ties.back().gapEstimate;
  for (int i = ties.size()-1; i>=0; --i) {
    if ( ! ties[i].isSplinted()) {
      continue;
    }	  
    if (ties[i].gapEstimate < farthest) {
      break;
    }
    farthestSplints.push_back(ties[i]);
  }

}


void traverse_rRNA(path_t& currentPath,  const End &_myEnd, objectSeenCnt_t &seen, int &totDistTraversed, int &level, float refDepth, bool &limitsReached)
{
  /*  
    Recursive direct traversal function; Nodes are Ends, edges are ties.
    Inputs:  myEnd   - end we're trying to extend. Currently last node in myPath

    Losely based on the exSPAnder algorithm.  Relies on cumulative score of potential extension and comparing that against alternative extensions.
    Score is calculated using a scoring_function and winner-loser decision rules.  

    If no clear winner can be chosen by score, tries conservative depth matching to the lowest depth of the path.

  */

  // some tweakable params affecting decision rule  (per exSPAnder paper)
  float theta = 0.5;
  float C = 1.5;

  float lowDepthThr = 3.0;
  float peerDepthRatio = 0.8; 
  float maxDepthDropRatio = 0.5;
  
  float currentWinnerScore = 0.0;
  // parameter to limit traversal distance  
  int maxLevel = 100;
  int maxDist = 6000;

  DBG2N("\tDFS: myEnd: %s\n", _myEnd.endId.toString().c_str());
  ObjectId objID = _myEnd.getID();
  if (!seen.count(objID)) {
    seen[objID] = 1;
  }else {
    seen[objID]++;
  }
  
  if (++level == maxLevel) {    
    limitsReached=true;
    DBG2N("\tDFS: maxLevel reached\n");     
    return;
  }

#ifndef USE_ULDP  //Update reference depth for exSPAnder logic;  ULDP logic requires that refDepth stay fixed
  // Set as the lowest depth of the current path
  if (onoObjectsMap.at(objID).depth < refDepth) {
    DBG2N("\tDFS: refDepth updated: %f -> %f\n", refDepth, onoObjectsMap.at(objID).depth);
    refDepth = onoObjectsMap.at(objID).depth;
  }
 #endif
  
  vector<EndTie> ties = _myEnd.export_all_ties();
  if (ties.empty()) {
    DBG2N("\tDFS: No ties\n");
    return;
  }

  vector<EndTie> adjacentSplints;
  gather_adjacent_splints(_myEnd, adjacentSplints, ties);
  if (! adjacentSplints.size() ){
    DBG2N("\tDFS: No adjacent splint ties found!\n\n");
    return;
  }

  //  we are suspicious of cases where there's only one adjacent splint of low depth as we suspect there may be other lost low-depth neighbors;  DISABLE THIS FOR HIGHER AGGRESSIVENESS
  if (adjacentSplints.size() == 1) {
    ObjectId tiedObjID = adjacentSplints[0].getTiedEnd().endId.getID();
    float tiedDepth = onoObjectsMap.at(tiedObjID).depth;
    if (tiedDepth <= lowDepthThr  &&  tiedDepth / onoObjectsMap.at(objID).depth  < maxDepthDropRatio ) {
      DBG2N("\tDFS: Single neighbor candidate with low depth (%f) and high depth drop ratio - Reject!\n\n");
      return;
    }
  }
  
  
  
  // evaluate closest ties and pick as contenders those with score >= currentWinnerScore, i.e. they're worth considering as possible extensions

  scoredEndsHash_t contenderH;
  vector<EndId> repeat_nodes;  // will be populated AFTER the preliminalry contenders have been selected

      
  //get preliminary contenders and and the best score among them
  build_contender_list(currentPath, contenderH, repeat_nodes, adjacentSplints, seen, C, &currentWinnerScore);
  if ( !contenderH.size() ) {
    DBG2N("\tDFS: no suitable contender extensions found.\n");
    return;
  }

      
  //Find repeat-ends in current path  *relative to the contender collection* (ala exSPAnder) and prevent them from contributing to condender scoring;   then rebuild/rescore the contenders hash.  Single contenders are passed automatically.

  repeat_nodes.clear();
  while (contenderH.size() > 1) {   
    for (size_t i = 0; i < currentPath.size(); ++i) {
      EndId pid = currentPath[i];
      if (end_supports_all(pid, contenderH)) {
	repeat_nodes.push_back(pid);
      }
    }
    if (repeat_nodes.size()) {
      DBG2N("\t[DEBUG] repeat_nodes detected in path\n");
    }
    //get revised list of contenders
    int prevNContenders = contenderH.size();
    contenderH.clear();
    build_contender_list(currentPath, contenderH, repeat_nodes, adjacentSplints, seen, C, &currentWinnerScore); 
    // iterate until contender list is not changing
    if (contenderH.size() == prevNContenders) {
      break;
    }
  }
      

  // pick *strong* winner

  EndId currentWinningEnd;
  float best;
  float runnerUp;
  float winnerScore = 0.0;


  if (contenderH.size() == 1) {
    if (contenderH.begin()->second >= theta) {
      currentWinningEnd = contenderH.begin()->first;
      winnerScore = contenderH.begin()->second;
    }
  }
  else if (contenderH.size() > 1) {
    //sort by score (decreasing)
    vector<std::pair<EndId, float>> sortedByScore(contenderH.begin(), contenderH.end());

    auto comp = [] (std::pair<EndId, float> elem1, std::pair<EndId, float> elem2) -> bool
    {
      return elem1.second > elem2.second;
    };
    std::sort(sortedByScore.begin(), sortedByScore.end(), comp);

    best = sortedByScore[0].second;
    runnerUp = sortedByScore[1].second;
    if (best >= theta && best > C*runnerUp) {
      currentWinningEnd = sortedByScore[0].first;
      winnerScore = best;
    }
  } 
  else 
    DBG2N("\tDFS: No suitable contender extensions found.\n");



  if ( winnerScore ) {
    DBG2N("\tDFS: strong winner tied end %s, score=%f\n", currentWinningEnd.toString().c_str(), winnerScore);
  }
  else {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // No clear winner found, so we pick one based on the presense of a single "best" stp forward
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    DBG2N("\tDFS: no strong winning extension found.  Will try to pick based on compatible depth search (refDepth: %f)\n", refDepth);

    vector<int> hasLongPeerIdx; //idx of candidate edges leading to at AT LEAST ONE long depth-peer
    vector<int> hasPathFwdIdx;  //idx of candidate edges leading to AT LEAST ONE long object w sufficiently high dept

    objectSeen_t visited; // different from the 'seen' list in that this one is only used for the find_frontier search

    // using *adjacent spints as those are our extenstion candidates
    for (size_t i=0; i<adjacentSplints.size(); i++) {
      vector<EndId> frontier_i; 
      EndIdAndFlag testEnd_i = adjacentSplints[i].getTiedEnd().getEndId();
      // start with the "in" end
      // explore the frontier ala exSPAnder
      DBG2N("\n\tDFS: find_frontier_long from %s...\n", testEnd_i.toString().c_str());
      find_frontier_long(&frontier_i, testEnd_i, visited, 0);
      if (frontier_i.size()) {
	int depthPeerCnt = 0;
	int depthDropCnt = 0;
  	for (size_t j = 0; j < frontier_i.size(); ++j) {  
  	  ObjectId objID_j = frontier_i[j].getID();
  	  float depth_j = onoObjectsMap.at(objID_j).depth;
	  if (depth_j/refDepth < maxDepthDropRatio) {
	    depthDropCnt++;
	  }
#ifdef USE_ULDP
	  if (refDepth/depth_j >= peerDepthRatio &&
	      refDepth/depth_j <= 1/peerDepthRatio) {
	    depthPeerCnt++;
	    DBG2N("\t\tDFS: long peer %d: %s\n", depthPeerCnt, frontier_i[j].toString().c_str()); 
	  }
#endif
  	}
	if (depthDropCnt < frontier_i.size()) {
	  hasPathFwdIdx.push_back(i);
	  DBG2N("\t\tDFS: Frontier has a high-cov path fwd!\n");
	}
#ifdef USE_ULDP
	//		if (depthPeerCnt == 1) {
	if (depthPeerCnt) {
	  hasLongPeerIdx.push_back(i);
	  DBG2N("\t\tDFS: Frontier has ULDP!\n");
	}
#endif

      }

    }

    if (hasLongPeerIdx.size() == 1) {
      //First choise:  only ONE adjacent node leads to ONE long depth peer
      currentWinningEnd = adjacentSplints[hasLongPeerIdx[0]].tiedEndId;
      DBG2N("\n\tDFS: winning end based on ULDP: %s \n", currentWinningEnd.toString().c_str());
    }
    else if (hasPathFwdIdx.size() == 1) {
      //ExSPAnder logic -  only one candidate node leads to long nodes of "high-enough* depth
      currentWinningEnd = adjacentSplints[hasPathFwdIdx[0]].tiedEndId;
      DBG2N("\n\tDFS: winning end based on unique high-cov path logic: %s \n", currentWinningEnd.toString().c_str());
    }
    // else if (uniqLongPeerEnds.size() == adjacentSplints.size()
    // 	     &&
    // 	     std::equal(uniqLongPeerEnds.begin() + 1, uniqLongPeerEnds.end(), uniqLongPeerEnds.begin())) {
    //   // ALL adjacent nodes lead to the SAME long depth peer - pick first one
    //   currentWinningEnd = adjacentSplints[0].tiedEndId;
    // }
    else {
      DBG2N("\tDFS: failed to pick winning extension.\n\n");
      return;
    }
    

  }

    // add both ends of the winner object to current path and continue traversal off of the new tip
  
  ObjectId winnerObjId = currentWinningEnd.getID();
  int gapEst = _myEnd.get_tie(currentWinningEnd)->gapEstimate;

  if (gapEst + onoObjectsMap.at(winnerObjId).len  > maxDist) {
    limitsReached =true;
    DBG2N("\tDFS: Winning extension found, but path would exceed maxDist!\n");
    return;
  }
  End &_outEnd = other_end(currentWinningEnd);
  currentPath.push_back(currentWinningEnd);
  currentPath.push_back(_outEnd.endId);
  totDistTraversed += (gapEst + onoObjectsMap.at(winnerObjId).len);
  
  traverse_rRNA(currentPath, _outEnd, seen, totDistTraversed, level, refDepth, limitsReached);

  return;
}


bool end_supports_all(EndId refEndId, const scoredEndsHash_t& contenderH)
{
  for(auto kv : contenderH) {
    EndId testId = kv.first;
    if (getEnd(refEndId).get_tie(testId) == NULL) {
      return false;
    }
  }
  return true;
}

float ext_score(const path_t& path, const vector<EndId>& repeat_nodes, const EndId myEndId)
{
  /* 
     This function scores tne extension candidate based on cumulative support wrt nodes in currentPath.
     Only the *number* of expected vs present ties is considered, and all objects are treated the same (i.e. no 
     lower expectation for short obects vs long) 
  
  */
  int expected = 0;
  int support = 0;
  int dist = 0;
  float score = 0.0;

  //When two nodes in the path are not adjacent, their separation cam be measused by 
  //  i) direct splint gap estimate and ii) sum of individual gaps + object size for all the nodes inbetween
  //  This cutoff is the allowed discrepancy between the two
  int maxDistDeviation = merSize;   
  
  // path contains two ends for every object, but we're only interested in the "source" ends, so we skip the "sink" each time
  int prevObjSize = 0;
  EndId prevEndId = myEndId;
  for (int i = path.size() - 1; i >= 0; i -= 2) {
    
    EndId outEndId = path[i];
    DBG2N("\t\t\tDFS[ext_score] %d: testing myEnd w path outEndId %s ...\n", i, outEndId.toString().c_str()); 
    End &_outEnd = onoObjectsMap.getEnd(outEndId);
    dist += prevObjSize;
    dist += _outEnd.get_tie(prevEndId)->gapEstimate;    

    if (!repeat_nodes.size() || std::find(repeat_nodes.begin(), repeat_nodes.end(), outEndId) == repeat_nodes.end()){  // dont evaluate repeat nodes
      //update total distance traveled from myEnd
      if (dist <= *LARGE - (2*merSize)) {  //we expect a splint to seed here and reach myEnd
	expected++;
      }
      
      EndTie* tieToMyEnd = _outEnd.get_tie(myEndId);
      if (tieToMyEnd != NULL
	  && tieToMyEnd->isSplinted()
	  && (abs(tieToMyEnd->gapEstimate - dist) <= maxDistDeviation)
	  )   {
	// we indeed have a splint here with the gap estimate matchin the distance we've traveled
	support++; 
      }
    }

    DBG2N("\t\t\tDFS[ext_score] dist: %d, exp: %d, supp: %d \n", dist, expected, support);
    prevEndId = outEndId;
    prevEndId.flip_prime();

    prevObjSize = onoObjectsMap.at(prevEndId.getID()).len;
  }

  if (!expected) {
    return 0.0;
  }
  score = float(support)/float(expected);
  return score;
}

bool build_contender_list(const path_t& currentPath, scoredEndsHash_t& contenderH, const vector<EndId>& repeat_nodes, const vector<EndTie>& adjacentSplints, const objectSeenCnt_t& seen, const float C, float *_currentWinnerScore )
 {

   float runningBestScore = 0.0;
   for (size_t i = 0; i < adjacentSplints.size(); ++i) {
     const EndTie* _tie_i = &adjacentSplints[i];
     ObjectId tiedObjID_i = _tie_i->tiedEndId.getID();
     if (seen.count(tiedObjID_i)) {
       DBGN("\t\tDFS[build_contender_list]: LOOP! (obj: %d) - ", tiedObjID_i);
       if (seen.at(tiedObjID_i) > 1) { // allow going through a loop once
	 DBGN("Too many visits to object; Rejected.\n");
	 continue;
       }
       DBGN("Allowed\n");
     }
     EndIdAndFlag endId_i = adjacentSplints[i].getTiedEnd().getEndId();
     DBG2N("\t\tDFS[build_contender_list]: trying potential contender end %s..\n", endId_i.toString().c_str());
     if (onoObjectsMap.at(tiedObjID_i).len >= *LARGE && endId_i.getEndMark() != EndLabel::RRNA) {
       // // we've reached a non-rRNA long object
       DBG2N("\t\t\tDFS[build_contender_list]: long non-rRNA object (endMark: %d) - backpaddle\n", endId_i.getEndMark());
        continue;
     }
     
     float score_i;

     vector<EndId> reverseTies = _tie_i->getInverseTiedIds();
     if (reverseTies.size()  >= 2) {
       int nTiedToMyUniqPath = 0;
       for (auto &tiedBackTo : reverseTies) {
	 if (std::find(repeat_nodes.begin(), repeat_nodes.end(), tiedBackTo) == repeat_nodes.end()){
	   continue;
	 }
	 if (std::find(currentPath.begin(), currentPath.end(), tiedBackTo) == currentPath.end()) {
	   continue;
	 }
	 nTiedToMyUniqPath++;
       }
       if (reverseTies.size() == nTiedToMyUniqPath) {
	 score_i = 0.5;
	 contenderH[endId_i] = score_i;
	 *_currentWinnerScore = score_i;
	 DBGN("\t\t\tDFS[build_contender_list]: has ALL of its ties back to non-repeat nodes of current path - gets score of %f\n", score_i);
	 continue;
       }
     } 

     
     score_i = ext_score(currentPath, repeat_nodes, endId_i);
     DBG2N("\t\t\tDFS[build_contender_list]: score_i = %f (C*score = %f)\n", score_i, (C*score_i));
     //add to active set if C*score >= winner from *previous bulid_contenders attempt*
     if (C*score_i >= *_currentWinnerScore) {
       runningBestScore = score_i;
       contenderH[endId_i] = score_i;
     }
   }
   DBG2N("\t\tDFS[build_contender_list]: %d active contenders\n", contenderH.size());
   if (runningBestScore > *_currentWinnerScore) {
     *_currentWinnerScore = runningBestScore;
   }
   return true;
 }



void find_frontier_long(vector<EndId> *_longEnds,  const EndIdAndFlag myIn, objectSeen_t &visited, int level)
{
  /*
    Preforms DFS over short nodes (up to maxLevel) and returns any long nodes found 
    !!! Uses farthest splint(s) in the traversal !!!

   */

  int maxLevel = 50;

  ObjectId objID = myIn.getID(); 
  visited.insert(objID);
  level++;
  DBG2N(" %d: ",level);
  DBG2N("%s ",myIn.toString().c_str());
  
  if (level == maxLevel) {
    DBG2N("\t\t[maxLevel reached]\n");
    return;
  }

  if (onoObjectsMap.at(objID).len >= *LARGE  ) {

    if (myIn.getEndMark() != EndLabel::RRNA) {
      DBG2N("\t\t[non-rRNA long]\n");
      return;
    }

    _longEnds->push_back(myIn);
    DBG2N("\t\t[long rRNA!] - l: %d, d: %f\n\n", onoObjectsMap.at(objID).len, onoObjectsMap.at(objID).depth);
    return;
  }

  // continue traversal
  EndIdAndFlag myOut = myIn;
  myOut.flip_prime();
  DBG2N("->%s  ", myOut.toString().c_str());

  vector<EndTie> ties = onoObjectsMap.at(objID).getEnd(myOut).export_all_ties();
  if (!ties.size()) {
    DBG2N("\t\t[no ties]");
    return;
  }

  vector<EndTie> farthestSplints;
  gather_farthest_splints(farthestSplints, ties);
  //  vector<EndTie> adjacentSplints;
  //  gather_adjacent_splints(adjacentSplints, ties);
  for (int i = 0; i< farthestSplints.size(); i++) {
    const EndTie* _tie_i = &farthestSplints[i];
    ObjectId tiedObjID_i = _tie_i->tiedEndId.getID();
    if (visited.count(tiedObjID_i)) { 
      continue;
    }
    find_frontier_long(_longEnds, _tie_i->getTiedEnd().getEndId(), visited, level);
  }

  return;
}


void find_all_long(vector<EndId> *_longEnds,  const EndIdAndFlag myIn, objectSeen_t &visited, int level, int dist)
{
  /*
    preforms DFS up to maxLevel and returns *all* paths to long nodes 
   */

  int maxLevel = 50;
  int maxDist = 6000;
  
  ObjectId objID = myIn.getID(); 
  visited.insert(objID);
  level++;

  DBG2N(" %d [%d bp]: ",level,dist);
  DBG2N("%s ",myIn.toString().c_str());
  
  if (level == maxLevel) {
    DBG2N("\t\t[maxLevel]\n");
    return;
  }

  if (onoObjectsMap.at(objID).len >= *LARGE  ) {

    if (myIn.getEndMark() != EndLabel::RRNA) {
      DBG2N("\t\t[non-rRNA long]\n");
      return;
     }

    _longEnds->push_back(myIn);
    DBG2N("\t\t[long rRNA!] - l: %d, d: %f\n\n", onoObjectsMap.at(objID).len, onoObjectsMap.at(objID).depth);
    //    return;
  }

  // continue traversal
  EndIdAndFlag myOut = myIn;
  myOut.flip_prime();
  DBG2N("->%s  ", myOut.toString().c_str());


  if (myOut.getEndMark() == EndLabel::NO_TIES) {
    DBG2N("\t\t[No ties]\n");
    return;
  }
  
  vector<EndTie> ties = onoObjectsMap.at(objID).getEnd(myOut).export_all_ties();
  std::sort(ties.begin(), ties.end(), by_dist_len());     // sort ties by distance
  std::reverse(ties.begin(), ties.end());     //start with the farthest
  for (int i = 0; i< ties.size(); ++i) {
    if ( ! ties[i].isSplinted()) {
      continue;
    }	  
    const EndTie* _tie_i = &ties[i];
    ObjectId tiedObjID_i = _tie_i->tiedEndId.getID();
    if (visited.count(tiedObjID_i)) { 
      continue;
    }
    
    dist +=  (_tie_i->gapEstimate + onoObjectsMap.at(tiedObjID_i).len);
    if (dist > maxDist) {
      continue;
    }
    find_all_long(_longEnds, _tie_i->getTiedEnd().getEndId(), visited, level, dist);
  }
  return;
}




path_t doctor_path(const path_t& path, copyTracker_t& copyObjTracker, tiedEndsHash_t& rPathEdgesALL, bool existsSrfFile)
{
  //  If ANY of the nodes have already been incorporated into another r-path, make copies and build uniqPath

  //  Path elements are ends; Every TWO elements are edges/ties
  //  Two consecutive ends of same contig may or may not be a true edge (think opposite ends of same contig  vs. a true tandem repeat edge).
  //  Here we assume that ODD elements (0-based) are connected to the preceding element via a true edge and EVEN elements are the tail-end of objects
    
    bool even = false; 
    path_t uniqPath;
    ObjectId prevObjID = 0;
    ObjectId curCopyId = 0;
    unordered_set<EndId> seenHere; // if repeats are present, this is to keep track of object copies in this path only

    for (auto it = path.begin(); it != path.end(); ++it) {

        even = (std::distance(path.begin(), it) % 2 == 0) ? true : false;
	

        End &_myEnd = getEnd(*it);
        ObjectId myID = _myEnd.getID();
	DBG2N("[doctor_path]: %s\n", _myEnd.toString().c_str());
        if (rPathEdgesALL.count(_myEnd.endId)  // alredy seen in other paths
	    || 
	    seenHere.count(_myEnd.endId)  //already seen in this path
	    ||
            (rPathEdgesALL.count(other_end(_myEnd).endId) && (it != path.begin() && std::next(it) != path.end()))  ) {
	    //The last condition is for a case where not this end but the OTHER END of this object already exists in a different r-path,  and the two paths don't attach end-to-end.

	  
	    int endPrime = _myEnd.getPrime(); // do this before _myEnd may be invalidated by create_copy_object!
	    if (!curCopyId) {
                curCopyId = create_copy_object(&onoObjectsMap.at(myID), copyObjTracker, existsSrfFile);
            }
	    if (even && uniqPath.size() && uniqPath.back().getID() > 0 ) { // We're on the downstream end that we need to copy but the other (previous) end wasn't copied
	      // i.e., the current end is now part of a copy-object, but not the upstream end we just passed!  Replace last node of uniqPath with the relevant copy-end .
	    	uniqPath.pop_back();
	    	End *_copyOtherEnd = (endPrime == 3) ?  &onoObjectsMap.at(curCopyId).end5 : &onoObjectsMap.at(curCopyId).end3;
	    	uniqPath.push_back(_copyOtherEnd->endId);
	    }
	    End *_copyEnd = (endPrime == 5) ?  &onoObjectsMap.at(curCopyId).end5 : &onoObjectsMap.at(curCopyId).end3;
	    uniqPath.push_back(_copyEnd->endId);

	    DBG2N("[doctor_path]: \tobject %d already part of this or another r-path; created copy-object %d\n", myID, curCopyId);
        }
	else {
	  uniqPath.push_back(_myEnd.endId);
        }

	seenHere.insert(_myEnd.endId);
        prevObjID = myID;
	if (even) 
	  curCopyId = 0;
    }

    for (size_t i = 0; i < uniqPath.size(); i = i + 2) {
        //update hash of unique r-path edges
        rPathEdgesALL.insert(std::pair<EndId, EndId>(uniqPath[i], uniqPath[i + 1]));
        rPathEdgesALL.insert(std::pair<EndId, EndId>(uniqPath[i + 1], uniqPath[i]));
    }

    return uniqPath;
}


void convert_path_to_bestTies(path_t& path, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended)
{
    //# Aggressively turn the path into reciprocal bestTie entries, overwriting anything that's already present.
    //# This is meant to set in stone a particular path through the graph with no regard to any other linkage.

    // for ( End_sp _myEnd : path ) {
    //   // if exists, delete bestTie of this end, plus that end's bestTiedBy entry orresponding to my end.
    //   if (bestTies.count(_myEnd)) {
    //     End_sp _BTofA = bestTies.at(_myEnd);
    //     for (vector<End_sp>::iterator it = bestTiedBy.at(_BTofA).begin(); it != bestTiedBy.at(_BTofA).end(); ++it) {
    //  if (*it == _myEnd) {
    //    bestTiedBy.at(_BTofA).erase(it);
    //    break;
    //  }
    //     }
    //     bestTies.erase (_myEnd);
    //   }

    //   // if other ends have this end as their bestTie, delete those, plus my end's bestTiedBy record
    //   if (bestTiedBy.count(_myEnd)) {
    //     vector<End_sp> BTByA = bestTiedBy.at(_myEnd);

    //     for (auto & _btbA : BTByA) {
    //  bestTies.erase (_btbA);
    //     }
    //     bestTiedBy.erase (_myEnd);
    //   }

    //   // delete form suspended list if (if found)
    //   ObjectId objID = _myEnd->getID();
    //   suspended.erase (objID);
    // }



    //iterate over each *pair* of ends in the path and create RECIPROCAL bestTie & bestTiedBy records
    array<EndId, 2> tiedPair;
    for (size_t i = 0; i < path.size(); i += 2) {
        EndId _teA = path[i];
        EndId _teB = path[i + 1];

        tiedPair = { _teA, _teB };

	DBG2N("[convert_path_to_bestTies] Checking existing BT and BTB entries for %s & %s ...\n", _teA.toString().c_str(), _teB.toString().c_str());
        for (auto & _te : tiedPair) {
            auto teInBestTies = bestTies.find(_te);
            if (teInBestTies != bestTies.end()) {
                // if exists, delete bestTie of this end, plus that end's bestTiedBy entry corresponding to my end.
                EndId _BTof = teInBestTies->second;
                auto & ties = bestTiedBy.at(_BTof);
                DBG2N("[convert_path_to_bestTies]: Erasing BEST_TIE of %s: %s\t", _te.toString().c_str(), _BTof.toString().c_str());
                for (vector<EndId>::iterator it = ties.begin(); it != ties.end(); ++it) {
                    if (*it == _te) {
                        DBG2N("and its matching BEST_TIED_BY %s\t", it->toString().c_str());
                        ties.erase(it);
                        break;
                    }
                }
                bestTies.erase(_te);
            }
            // if other ends have this end as their bestTie, delete those, plus my end's bestTiedBy record
            auto teInBestTiedBy = bestTiedBy.find(_te);
            if (teInBestTiedBy != bestTiedBy.end()) {
                DBG2N("[convert_path_to_bestTies]: Erasing BEST_TIED_BY of %s\t", _te.toString().c_str());
                vector<EndId> &BTBy = teInBestTiedBy->second;
                for (auto & _btb : BTBy) {
                    DBG2N("and BEST_TIE of %s\t", _btb.toString().c_str());
		    bestTies.erase(_btb);
                }
                bestTiedBy.erase(_te);
            }

            // delete form suspended list if found
            ObjectId objID = _te.getID();
	    if (suspended.erase(objID)) {
	      DBG2N("[convert_path_to_bestTies]: Erased SUSPENSION of %lld\t", (lld)objID);
	    }
        }

        bestTies[_teA] = _teB;
        bestTiedBy[_teB].push_back(_teA);

        bestTies[_teB] = _teA;
        bestTiedBy[_teA].push_back(_teB);

        DBG2N("[convert_path_to_bestTies]: Mutual best ties: %s and %s\n", _teA.toString().c_str(), _teB.toString().c_str());

    }
    DBG2N("[convert_path_to_bestTies]: Done with path.\n")
}


End *best_tie(const End &_end, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended)
{
    Obj &obj = onoObjectsMap.at(_end.getID());
    bool largeObject = (obj.len > *LARGE) ? true : false;
    int nSuspended = 0;

    vector<EndTie> goodTies;
    for (EndTie tie : _end.ties) {
        EndLabel::Label endMark = getEnd(tie.tiedEndId).getEndMark();
        if (endMark == EndLabel::NO_TIES) { // no ties
            DBG2N("NO_TIES end=%lld %s\n", (lld)_end.getID(), tie.tiedEndId.toString().c_str());
            continue;
        }
        if (endMark == EndLabel::SELF_TIE) { // self-tie
            DBG2N("SELF_TIE end=%lld %s\n", (lld)_end.getID(), tie.tiedEndId.toString().c_str());
            continue;
        }
        auto tieInBestTies = bestTies.find(tie.tiedEndId);
        if (tieInBestTies != bestTies.end() && tieInBestTies->second != _end.endId) { // skip ends that already have a bestTie to a different end
            DBG2N("NOT_BEST_TIE end=%lld %s best=%s\n", (lld)_end.getID(), tie.tiedEndId.toString().c_str(), tieInBestTies->second.toString().c_str());
            continue;
        }
        goodTies.push_back(tie);

    }
    if (goodTies.empty()) {
        return NULL;
    }

    std::sort(goodTies.begin(), goodTies.end(), by_dist_len());

#ifdef DEBUG
    for (EndTie tie : goodTies) {
      DBG2N("GoodTie end=%lld %s; gap=%d\n", (lld)_end.getID(), tie.tiedEndId.toString().c_str(), tie.gapEstimate);
    }
#endif
    vector<ObjectId> toSuspend;

    // If a large object, return the closest large object if one exists.
    // Small objects may be suspended between large objects
    if (largeObject) {
        End *closestLarge = NULL;
        for (EndTie tie : goodTies) {
            EndId _te = tie.tiedEndId;
            Obj &obj_i = onoObjectsMap.at(_te.getID());
            if (obj_i.len > *LARGE) {
                closestLarge = &obj_i.getEnd(_te);
                break;
            } else {
                toSuspend.push_back(obj_i.id);
            }
        }
        if (closestLarge) {
            for (auto _objId : toSuspend) {
                suspended.insert(std::make_pair(_objId, true));
                DBG2N("BT: largeObject; suspended tied object: %lld\t", (lld)_objId);
                nSuspended++;
            }
            DBG2N("\nBT return: closestLarge (%lld suspended)\n", (lld)nSuspended);
            return closestLarge;
        }
    }

    //Return the closest extendable object if one exists.  Objects must be unmarked to qualify. Unextendable objects may be suspended
    End *closestExtendable = NULL;
    toSuspend.clear();
    for (EndTie tie : goodTies) {
        EndId _te = tie.tiedEndId;
        End *_otherEnd = &other_end(_te);
        Obj &obj_i = onoObjectsMap.at(_te.getID());
        if (obj_i.getEnd(_te).getEndMark() || _otherEnd->getEndMark()) {
            toSuspend.push_back(obj_i.id);
        } else {
            closestExtendable = &obj_i.getEnd(_te);
            break;
        }
    }
    if (closestExtendable) {
        for (auto _objId : toSuspend) {
            suspended.insert(std::make_pair(_objId, true));
            DBG2N("BT: suspended tied object %lld\t", (lld)_objId);
            nSuspended++;
        }
        DBG2N("BT return: closestExtendable (%lld suspended)\n", (lld)nSuspended);
        return closestExtendable;
    }

    toSuspend.clear();
    //Just return the closest object
    End *closestEnd = &getEnd(goodTies[0].tiedEndId);
    DBG2N("BT return: closest\n");
    return closestEnd;
}


void find_bestTies_end(tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyObjTracker, bool existsSrfFile, refHitsHash_t& rRNAhits, tiedEndsHash_t &rPathEdgesALL, tiedEndsHash_t &rPathTerminals, Obj *piece, End &_end)
{     
    DBGN("end %d:\n", _end.getPrime());
    
    if (_end.getEndMark() == EndLabel::UNMARKED && !bestTies.count(_end.endId)) {
        End *_bestTie = best_tie(_end, bestTies, bestTiedBy, suspended);
        if (_bestTie != NULL) {
            bestTies.insert(std::pair<EndId, EndId>(_end.endId, _bestTie->endId));
            bestTiedBy[_bestTie->endId].push_back(_end.endId);
            string bestTieStr = std::to_string(_bestTie->getID()) + "." + std::to_string(_bestTie->getPrime());
            DBGN("BEST_TIE: %s\n", bestTieStr.c_str());
        } else {
            DBGN("BEST_TIE: [none]\n");
        }
		
    } else if (piece->len >= *LARGE
    	       &&  _end.getEndMark() == EndLabel::RRNA
    	       &&  !rPathTerminals.count(_end.endId)) {
      // # Unless this is already a terminal of a another r-path, traverse rRNA aggressively using splints only!

// #ifdef DEBUG
//       SET_HIPMER_VERBOSITY(LL_DBG2);
// #endif
        objectSeenCnt_t seen;
        int rRNAcovered = (_end.getPrime() == 5) ? rRNAhits[piece->id].end : piece->len - rRNAhits[piece->id].start + 1;
        assert(rRNAcovered > 0);

        int gap = 0;
        int level = 0;
    	float depth = piece->depth;
    	int distanceTraversed = rRNAcovered;
    	bool limitsReached = false;
	
    	DBG2N("\nTry DFS from end %s ...\n", _end.toString().c_str());
    	path_t bestPath;
    	bestPath.reserve(200);

    	//// use SLU/SSU rRNA taxonomy if available)
        //string taxonomyStr = rRNAhits.at(piece->id).taxonomy;

	bestPath.push_back(_end.endId); // prime the path with the first "out" node
	traverse_rRNA(bestPath, _end, seen, distanceTraversed, level, depth, limitsReached);

        // If the walk terminates with an "out" end that's already another path's terminal, clip that entire object off to prevent end-to-end misjoins on repeats
	while (bestPath.size() > 2 && rPathTerminals.count(bestPath.back())) {
	  DBG2N("Clipped terminal of walk (%s) since it is already a terminal of another finalized path.\n", bestPath.back().toString().c_str());
	  bestPath.resize(bestPath.size() - 2);
	  DBG2N("Walk now ends with %s\n", bestPath.back().toString().c_str());
	}

// #ifdef DEBUG
// 	SET_HIPMER_VERBOSITY(LL_DBG);
// #endif

	bestPath.pop_back(); // paths must end with a sink end, so clip the last "out" node of the path

        if (bestPath.size()) {
    	    assert(bestPath.size() % 2 == 0); //must be even  
            string bestPathStr = "";
            for (auto it = bestPath.begin(); it != bestPath.end(); it++) {
                bestPathStr += std::to_string(it->getID()) + "." + std::to_string(it->getPrime()) + "   ";
            }
            DBGN("BEST_PATH: %s [%s]\n", _end.toString().c_str(), bestPathStr.c_str());
	    
            path_t uniqPath = doctor_path(bestPath, copyObjTracker, rPathEdgesALL, existsSrfFile);
            convert_path_to_bestTies(uniqPath, bestTies, bestTiedBy, suspended);

    	    rPathTerminals[uniqPath.back()] = uniqPath[0];
    	    rPathTerminals[uniqPath[0]] = uniqPath.back();

        } else {
	  DBGN("BEST_PATH: [none]\n");
        }

    } else {
        DBGN(" - ineligible [%d]\n", _end.getEndMark());
    }
}

  void find_bestTies(const sortedBy_t& sortedByLen, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyObjTracker, bool existsSrfFile, refHitsHash_t& rRNAhits)
{
    // Super-function to evaluate all size-sorted objects' ends and secure a "best tie"

    // Unmarked ends are subject to conserative bestTie() selection while those marked as RRNA
    // initiate graph traversal to find a path through the repeat structure to a long rRNA "peer" object.

    // Pieces can be inserted between earlier established bestTies (i.e. suspensions) and/or copied in the process.

    tiedEndsHash_t rPathEdgesALL;  // stores edges (end pairs) that are part of rRNA r-paths
    tiedEndsHash_t rPathTerminals; // this is of questionable utility;  Safeguards against starting with already existing terminal nodes of an r-path.

    LOGF("Finding \"best ties\" (%lld)...\n", (lld)sortedByLen.size());

    int64_t count = 0, progressCt = sortedByLen.size() / 100;
    for (auto tmp : sortedByLen) {
        ObjectId id = tmp.id;

        if (progressCt && ++count % progressCt == 0) {
            LOGF("find_bestTies: %lld of %lld (%0.3f %%) object %lld\n", (lld)count, (lld)sortedByLen.size(), 100.0 * count / sortedByLen.size(), (lld)id);
        }
        DBG2(" === object %lld\n", (lld)id);

        if (suspended.count(id)) {
            DBG2N(" -ineligible (suspended)\n");
            continue;
        }

        Obj *piece; // find_bestTies_end may change onoObjectMap, so ensure piece is correct for each call
        piece = &onoObjectsMap.at(id);
        find_bestTies_end(bestTies, bestTiedBy, suspended, copyObjTracker, existsSrfFile, rRNAhits, rPathEdgesALL, rPathTerminals, piece, piece->end5);
        piece = &onoObjectsMap.at(id);
        find_bestTies_end(bestTies, bestTiedBy, suspended, copyObjTracker, existsSrfFile, rRNAhits, rPathEdgesALL, rPathTerminals, piece, piece->end3);

        DBG2N("\n");
    }

    //  Insert suspended pieces where possible
    place_suspended(suspended, bestTies, bestTiedBy, copyObjTracker, existsSrfFile);

    // update ties for any newly created copy-object ends
    LOGF("Updating ties for rPathEdgesALL=%lld\n", (lld)rPathEdgesALL.size());
    count = 0, progressCt = rPathEdgesALL.size() / 100;
    for (auto & element : rPathEdgesALL) {
        if (progressCt && ++count % progressCt == 0) {
            LOGF("updating ties %lld of %lld %0.1f %%\n", (lld)count, (lld)rPathEdgesALL.size(), 100.0 * count / rPathEdgesALL.size());
        }
        EndId _e1 = element.first;
        EndId _e2 = element.second;
        if (_e1.getID() < 0) { // a copy-end
            ObjectId e1_parentId = onoObjectsMap.at(_e1.getID()).parentId;
            assert(e1_parentId != LLONG_MIN);
            Obj &e1_parent = onoObjectsMap.at(e1_parentId);
            End *sourceEnd = (_e1.getPrime() == 5) ? &e1_parent.end5 : &e1_parent.end3;
            assert(sourceEnd);

            End *__e1 = &getEnd(_e1);
            End *__e2 = &getEnd(_e2);
            if (_e2.getID() < 0 && !__e2->ties.size()) { //a copy-end that hasn't had ties assigned;  use parent end instead
                ObjectId e2_parentId = onoObjectsMap.at(_e2.getID()).parentId;
                assert(e2_parentId != LLONG_MIN);
                Obj &e2_parent = onoObjectsMap.at(e2_parentId);
                _e2 = (_e2.getPrime() == 5) ? e2_parent.end5.endId : e2_parent.end3.endId;
                __e2 = &getEnd(_e2);
            }
            copy_tie_info(sourceEnd, __e1, __e2);
        }
    }
}
}  // end of namespace onoMeta





int oNo_meta_main(int argc, char **argv)
{
    using namespace std;
    using namespace onoMeta;
    
    LOG_VERSION;
    initCommonStatics();

    if (argc < 2) {
    SWARN("Usage: ./oNo_meta <-m merSize> <-l linkDataFile> <-R rRNAHitsList> (<-s scaffoldReportFile> || <-c contigReportFile>)  <-o scaffOutFile> <<-p pairThreshold>> <<-O IDoffset>> <<-V(erbose)>>\n");
    return 1;
    }

    option_t *optList, *_optList, *thisOpt;
    _optList = GetOptList(argc, argv, "m:l:p:s:c:o:O:R:LV");
    optList = _optList;

    char *linkDataFile = NULL;
    char *srfFile = NULL;
    char *crfFile = NULL;
    char *srfOutFile = NULL;
    char *rRNAHitsFile = NULL;
    int pairThreshold = 2;
    ObjectId srfOffset = 0;
    bool verbose = false;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'm':
            merSize = atoi(thisOpt->argument);
            break;
        case 'l':
            linkDataFile = thisOpt->argument;
            break;
        case 'p':
            pairThreshold = atoi(thisOpt->argument);
            break;
        case 's':
            srfFile = thisOpt->argument;
            break;
        case 'c':
            crfFile = thisOpt->argument;
            break;
        case 'o':
            srfOutFile = thisOpt->argument;
            break;
        case 'R':
            rRNAHitsFile = thisOpt->argument;
            break;
        case 'O':
            srfOffset = atol(thisOpt->argument);
            break;
        case 'L':
	    break;
        case 'V':
	    verbose = true;
	    break;
	}
    }

    if (!merSize) {
        SWARN("You must specify a -m merSize!\n");
        return 1;
    }
    if (linkDataFile == NULL) {
        SWARN("You must specify a -l linkDataFile!\n");
        return 1;
    }
    if (srfFile == NULL && crfFile == NULL) {
        SWARN("You must specify either -c crfFile or -s srfFile  inputs!\n");
        return 1;
    }
    if (srfOutFile == NULL) {
        SWARN("You must specify an output file!\n");
        return 1;
    }

    unordered_map<string, libInfo_t> libInfoHash;

    vector<string> libMetaData;
    bool splintsPresent = false;
    bool spansPresent = false;
    
    ifstream infile(linkDataFile);
    string line;
    while (getline(infile, line)) {
        libInfo_t libInfo;
        istringstream stm(line);
        string word;
        while (stm >> word) { // read white-space delimited tokens one by one
            libMetaData.push_back(word);
        }
        string libName = libMetaData[0];
        libInfo.insSize = stoi(libMetaData[1]);
        libInfo.sdev = stoi(libMetaData[2]);
        libInfo.linkFile = libMetaData[3];

	if (libInfo.sdev == 0) {
	  splintsPresent = true;
	} else {
	  spansPresent = true;
	}
	
	if (!libInfoHash.count(libName))
	  libInfoHash.insert({ libName, libInfo });
  
        if (libInfo.insSize > maxAvgInsSize) {
            maxAvgInsSize = libInfo.insSize;
            maxAvgInsSDev = libInfo.sdev;
        }
	libMetaData.clear();
    }

    // load contig/scaffold info

    if (crfFile != NULL) {
        parse_crfFile(string(crfFile));
    } else if (srfFile != NULL) {
        parse_srfFile(string(srfFile));
    }


    if (!onoObjectsMap.size()) { // Not a fatal error since parCC is not guaranteed to produce a CC for every thread;  Write empty srf file and exit;
        FreeOptList(_optList);
        LOGF("No assembly objects found in the input;  \n");
        ofstream srfOut_empty(srfOutFile);
        //  FLUSH_MY_LOG;
        return 0;
    }


    
    LOGF("There are %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);


    //load rRNA db hits
    refHitsHash_t rRNAHits;
    if (rRNAHitsFile != NULL) {
        parse_rRNAHits(string(rRNAHitsFile), rRNAHits, NULL != srfFile);
    }


    int maxUncertainty = spansPresent ? maxAvgInsSDev : 1000000; // spans are prone to gap estimate bias, especially in metagenomes; limit span usage here
    if (spansPresent) {
      DBGN("maxUncertainty for SPANs: %d\n", maxUncertainty);
    }
    //Build the linkage hash
    int64_t numLinks = 0;
    unordered_map<string,bool> seen;
    for (std::pair<string, libInfo_t> element : libInfoHash) {
        string libName = element.first;
        string linkFile = libInfoHash[libName].linkFile;
	if (!seen.count(linkFile)) { //multiple libs -  same link file is OK
	  numLinks += parse_linkFile_and_build_ties(linkFile, pairThreshold, maxUncertainty, NULL != srfFile);
	  seen.insert({linkFile, true});
	}
    }

    LOGF("There are %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);

    // build a vector of pointers to objects, sorted by sequence length
    LOGF("Reserving %lld entries for sortedByLen (%0.3f MB)\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj *) / ONE_MB);
    sortedBy_t sortedByLen;
    sortedByLen.reserve(onoObjectsMap.size());


    for (auto kv = onoObjectsMap.begin(); kv != onoObjectsMap.end(); kv++) {
        Obj &obj = kv->second;
        sortedByElement_t tmp(obj);
        sortedByLen.push_back(tmp);
    }

    std::sort(sortedByLen.begin(), sortedByLen.end(), by_length_depth());

    if (spansPresent) {
      LOGF("SPANs are present - inspecting ties to minimize effects of span gap uncertainty ...\n");
      for (auto _i : sortedByLen) {
	Obj *i = &onoObjectsMap.at(_i.id);
	// if an end has only one tie and it's a span, don't trust it.
	if (i->end5.ties.size() == 1) 
	  trash_lonely_spans(i->end5, maxAvgInsSDev/2);
	if (i->end3.ties.size() == 1) 
	  trash_lonely_spans(i->end3, maxAvgInsSDev/2);

	if (!i->end5.ties.size() && !i->end3.ties.size()) {
	  continue;
	}
	// revisit spans and try to correct gap size estimates with the help of splints
	inspect_ties(i->end5);
	inspect_ties(i->end3);      
      }      
    }

    // delete spans that are followed by splints, i.e. we think those spans are not of much use and can do more harm than good.
    if (spansPresent && splintsPresent) { 
      for (auto _i : sortedByLen) {
    	Obj *i = &onoObjectsMap.at(_i.id);
    	trash_wonky_spans(i->end5);
    	trash_wonky_spans(i->end3);
      }
    }

    
    // mark object ends

    int nObjects = sortedByLen.size();
    EndMarkCounts endMarkCounts = {};
    LOGF("Marking ends... (%d objects)\n", nObjects);

    for (auto _i : sortedByLen) {
        Obj *i = &onoObjectsMap.at(_i.id);
        mark_end(i->end5, endMarkCounts, rRNAHits);
	DBG2N(" %s  marked [%d]\n", i->end5.toString().c_str(), i->end5.getEndMark());
        mark_end(i->end3, endMarkCounts, rRNAHits);
	DBG2N(" %s  marked [%d]\n", i->end3.toString().c_str(), i->end3.getEndMark());
    }


    // # find a "best tie" for every end.
    tiedEndsHash_t bestTie;     //stores EndId => this end's bestTie EndId
    tiedEndListsH_t bestTiedBy; // stores EndId => list of ends that claim this end as their bestTie
    suspendedHash_t suspended;

    copyTracker_t copyObjTracker;
    find_bestTies(sortedByLen, bestTie, bestTiedBy, suspended, copyObjTracker, NULL != srfFile, rRNAHits);
    LOGF("bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    LOGF("bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    LOGF("suspended has %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    LOGF("copyObjTracker has %lld entries (%0.3f MB)\n", (lld)copyObjTracker.size(), 1.0 * copyObjTracker.size() * sizeof(copyTracker_t::value_type) / ONE_MB);

    LOGF("Freeing memory from sorted objects %lld and rRNAHits %lld\n", (lld)sortedByLen.size(), (lld)rRNAHits.size());
    sortedBy_t().swap(sortedByLen);
    refHitsHash_t().swap(rRNAHits);

    // # Lock ends together with no competing ties
    endLocks_t endLocks;
    lock_ends(endLocks, bestTie, bestTiedBy, suspended, onoObjectsMap);
    LOGF("bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    LOGF("bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    LOGF("endLocks has %lld entries (%0.3f MB)\n", (lld)endLocks.size(), 1.0 * endLocks.size() * sizeof(endLocks_t::value_type) / ONE_MB);
    LOGF("Freeing memory from suspended which %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    suspendedHash_t().swap(suspended);

    // # Traverse end locks to build scaffolds
    int64_t nScaffolds = build_scaffolds(endLocks, string(srfOutFile), NULL != srfFile, copyObjTracker, srfOffset);
    LOGF("Total of %lld scaffolds made\n", (lld)nScaffolds);


    for (int i = 0; i < EndLabel::MAX_END_LABEL; i++) {
        LOGF("Ends marked %s: %d\n", EndLabel::getName(i).c_str(), endMarkCounts[i]);
    }
    DBG("nr of best ties: %lld\n", (lld)bestTie.size());
    DBG("nr of copy-objects: %lld\n", (lld)copyObjTracker.size());
    DBG("nr of scaffolds: %lld\n", (lld)nScaffolds);

    // Cleanup

    LOGF("Cleaning bestTie has %lld entries (%0.3f MB)\n", (lld)bestTie.size(), 1.0 * bestTie.size() * sizeof(tiedEndsHash_t::value_type) / ONE_MB);
    tiedEndsHash_t().swap(bestTie);
    LOGF("Cleaning bestTiedBy has %lld entries (%0.3f MB)\n", (lld)bestTiedBy.size(), 1.0 * bestTiedBy.size() * sizeof(tiedEndListsH_t::value_type) / ONE_MB);
    tiedEndListsH_t().swap(bestTiedBy);
    LOGF("Cleaning suspended has %lld entries (%0.3f MB)\n", (lld)suspended.size(), 1.0 * suspended.size() * sizeof(suspendedHash_t::value_type) / ONE_MB);
    suspendedHash_t().swap(suspended);
    LOGF("Cleaning endLocks has %lld entries (%0.3f MB)\n", (lld)endLocks.size(), 1.0 * endLocks.size() * sizeof(endLocks_t::value_type) / ONE_MB);
    endLocks_t().swap(endLocks);

    LOGF("Cleaning %lld objects loaded now %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    objectsHash_t().swap(onoObjectsMap);

    LOGF("Cleaning %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    for (auto &sc : scaffInfo) {
        delete sc.second;
    }
    LOGF("Done deleting %lld scaffold objects\n", (lld)scaffInfo.size());
    scaffsHash_t().swap(scaffInfo);

    FreeOptList(_optList);
    LOGF("Done\n");
    
    FLUSH_MY_LOG;
    // END OF MAIN

    return 0;
}
