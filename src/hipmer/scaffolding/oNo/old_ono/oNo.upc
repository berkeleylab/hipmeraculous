#include "oNo.h"
#include "log.h"

int64_t _peakDepth;
int _depthInfoAvailable;

shared int64_t _total_scaff_entries = 0;
shared int64_t _total_scaffs = 0;

// config params
int _merSize = 0;
int64_t _totalLinks = 0;
int64_t _totalContigs = 0;
int64_t _nTotalObjects = 0;
char *_base_dir;
libInfoType _libInfo[MAX_LIBRARIES];
int _nLinkFiles = 0;
char *_scaffoldReportFilePrefix = NULL;
char *_contigReportFilePrefix = NULL;
char *_outputPrefix = NULL;
FILE *_linkMetaFD = NULL;
int _nActiveThreads = 0;
int _minPairThreshold = 1;
int _maxPairThreshold = 10;
int _pairBlock = 0;

per_pair_threshold_data_t *_pp = NULL;

shared int *_n50s = NULL;
shared int64_t *_nGaps = NULL;
shared int64_t *_nScaffolds = NULL;

static int cmp_int(const void *a, const void *b)
{
    return *(int *)a - *(int *)b;
}

int calcN50(int *lens, int64_t num)
{
    int64_t tot_len = 0;

    for (int64_t i = 0; i < num; i++) {
        tot_len += lens[i];
    }
    qsort(lens, num, sizeof(int), cmp_int);
    // compute the N50
    int64_t running_tot = 0;
    double idx = 0.5;
    for (int64_t i = num - 1; i >= 0; i--) {
        running_tot += lens[i];
        if (running_tot >= idx * tot_len) {
            int64_t nx = (int64_t)(idx * 100);
            return lens[i];
        }
    }
    return 0;
}

double find_y(double chat, int ell, int L)
{
    return chat * ell / L;
}

double compute_log_likelihood(double c1, int ell1, double c2, int ell2,
                              int L, double c1hat, double c2hat)
{
    double y1hat = find_y(c1hat, ell1, L), y2hat = find_y(c2hat, ell2, L);
    double y1 = c1 * ell1 * 1.0 / L, y2 = c2 * ell2 * 1.0 / L;

    return -y1 - y2 + y1hat *log(y1) + y2hat *log(y2);
}

double find_MLE_fit(double c1hat, double c2hat, int ell1, int ell2, int L,
                    double eps, double interval_begin, double interval_end, int is_inside_interval)
{
    int check = 0;

    if (is_inside_interval && (interval_begin < c1hat / c2hat) && (c1hat / c2hat < interval_end)) {
        check = 1;
    } else if ((!is_inside_interval) && (interval_begin > c1hat / c2hat || interval_end < c1hat / c2hat)) {
        check = 1;
    }



    if (check) {
        return compute_log_likelihood(c1hat, ell1, c2hat, ell2, L, c1hat, c2hat);
    } else {
        double c1, c2, ml1, ml2;

        c2 = (c1hat * ell1 + c2hat * ell2) * 1.0 / ((1 - eps) * ell1 + ell2);
        c1 = c2 * (1 - eps);
        ml1 = compute_log_likelihood(c1, ell1, c2, ell2, L, c1hat, c2hat);

        c2 = (c1hat * ell1 + c2hat * ell2) * 1.0 / ((1 + eps) * ell1 + ell2);
        c1 = c2 * (1 + eps);
        ml2 = compute_log_likelihood(c1, ell1, c2, ell2, L, c1hat, c2hat);

        return ml1 > ml2 ? ml1 : ml2;
    }
}

double compute_log_ratio(int ell1, int ell2, double c1hat, double c2hat, int L)
{
    double eps = 0.05;
    double interval_begin = 1 - eps, interval_end = 1 + eps;

    return find_MLE_fit(c1hat, c2hat, ell1, ell2, L, eps, interval_begin, interval_end, 1)
           - find_MLE_fit(c1hat, c2hat, ell1, ell2, L, eps, interval_begin, interval_end, 0);
}

void compute_peak_depth(shared[BS] int64_t *depthHist, shared[BS] int64_t *maxDepth)
{
    // Thread 0 finds modal depth and propagates the info through shared array
    if (MYTHREAD == 0) {
        int64_t maxbases = 0;
        for (int64_t i = 0; i < MAX_HISTO_SIZE; i++) {
            int64_t agg_depth = depthHist[i];
//         if (agg_depth)
//            printf("%ld %ld\n", i, agg_depth);
            if (agg_depth > maxbases) {
                _peakDepth = i;
                maxbases = agg_depth;
            }
        }
        for (int64_t i = 0; i < THREADS; i++) {
            maxDepth[i] = _peakDepth;
        }
    }
    serial_printf("Depth information available. Modal depth is %ld.\n", _peakDepth);
    UPC_LOGGED_BARRIER;

    // ensure all threads have the depth context
    _peakDepth = maxDepth[MYTHREAD];
    _depthInfoAvailable = _peakDepth > 0 ? 1 : 0;
}


void run_serial(int pairThreshold)
{
//   printf("Thread %d is running serial region with pair threshold %d\n", MYTHREAD, pairThreshold);

    UPC_TICK_T tStart = UPC_TICKS_NOW();
    UPC_TICK_T t = UPC_TICKS_NOW();

    assert(MYTHREAD == MIN_ACTIVE_THREAD(pairThreshold));
    int minActiveThread = MYTHREAD;
    DBG("Starting serial on %d threshold\n", pairThreshold);

    float *localContigDepth = NULL;

    int64_t contigsPerActiveThread = 0;
    if (_depthInfoAvailable) {
        localContigDepth = malloc_chk(_totalContigs * sizeof(float));
        contigsPerActiveThread = (_totalContigs + _nActiveThreads) / _nActiveThreads;
        int64_t sizeEnt = contigsPerActiveThread * sizeof(float);
        float *tmpBuf = malloc_chk(sizeEnt);
        for (int heapNum = 0; heapNum < _nActiveThreads; heapNum++) {
            int heapThread = heapNum + minActiveThread;
            DBG2("getting data from active thread %d\n", heapThread);
            assert(heapThread < THREADS);
            assert(_pp[pairThreshold].sharingDepths[heapThread] != NULL);
            upc_memget(tmpBuf, _pp[pairThreshold].sharingDepths[heapThread], sizeEnt);
            int64_t pos = 0;
            for (int64_t i = heapNum; i < _totalContigs; i += _nActiveThreads) {
                CHECK_BOUNDS(pos, contigsPerActiveThread);
                localContigDepth[i] = tmpBuf[pos];
                pos++;
            }
        }
        free_chk(tmpBuf);
    }

    scaf_report_t *scaffReportLocal = NULL;

    int64_t scaffoldsPerActiveThread = 0;
    assert(_total_scaffs <= _nTotalObjects);
    if (_scaffoldReportFilePrefix) {
        int64_t localBound = _total_scaffs;
        scaffReportLocal = calloc_chk(localBound, sizeof(scaf_report_t));

        scaffoldsPerActiveThread = (localBound + _nActiveThreads) / _nActiveThreads;
        int64_t sizeEnt = scaffoldsPerActiveThread * sizeof(scaf_report_t);
        assert(sizeEnt >= sizeof(scaf_report_t));
        scaf_report_t *tmpBuf = malloc_chk(sizeEnt);
        for (int heapNum = 0; heapNum < _nActiveThreads; heapNum++) {
            int heapThread = minActiveThread + heapNum;
            assert(heapThread < THREADS);
            assert(_pp[pairThreshold].sharingScaffPtrs[heapThread] != NULL);
            assert(scaffoldsPerActiveThread <= _pp[pairThreshold].numObjects[heapThread]);
            upc_memget(tmpBuf, _pp[pairThreshold].sharingScaffPtrs[heapThread], sizeEnt);
            int64_t pos = 0;
            for (int64_t i = heapNum; i < localBound; i += _nActiveThreads) {
                CHECK_BOUNDS(i, localBound);
                CHECK_BOUNDS(pos, scaffoldsPerActiveThread);
                scaffReportLocal[i] = tmpBuf[pos];
                // this memory was being prematurely freed at one point in the code...
                UPC_VALIDATE_ALLOC(_pp[pairThreshold].sharingScaffPtrs[heapThread]->entries);
                pos++;
            }
        }
        DBG("Loaded %lld entries into scaffReportLocal\n", (lld)localBound);
        free_chk(tmpBuf);
    }

    int64_t statsBestTie[END_TYPES];
    int64_t nInsertedSuspensions = 0;
    double avg_depth = 0.0;
    double pairThresholdDouble = 0.0;
    double logPdiff;

    // Cache locally the directory of shared locations for scaffold report files
    ScaffoldReportFiles *cachedSharedScaffolds = calloc_chk(THREADS, sizeof(ScaffoldReportFiles));
    for (int q = 0; q < THREADS; q++) {
        cachedSharedScaffolds[q] = _pp[pairThreshold].sharedScaffolds[q];
    }

    // WARNING: My implementation assumes that the libs are sorted based on the insert sizes
    // This doesn't matter if there is only one input lib per oNo
    int maxInsertSize = _libInfo[_nLinkFiles - 1].insertSize;

    int suspendable = maxInsertSize;

    //int suspendable = maxInsertSize/4;

    //int suspendable = maxInsertSize + 3 * _libInfo[_nLinkFiles-1].stdDev;

    for (int64_t i = 0; i < END_TYPES; i++) {
        statsBestTie[i] = 0;
    }

    tie_t *localTieHeap = NULL;
    int64_t totalTiesData = 0;
    int64_t localTieHeapLimit = 0, localTieHeapIdx = 0;
    int remoteTieHeapThread = 0;

    if (_totalLinks) {
        for (int i = 0; i < THREADS; i++) {
            assert(_pp[pairThreshold].tieSharedHeap);
            int64_t remotePos = _pp[pairThreshold].tieSharedHeap[i].position;
            assert(remotePos % sizeof(tie_t) == 0);
            totalTiesData += remotePos / sizeof(tie_t);
        }

        if (totalTiesData > 0) {
            CHECK_BOUNDS(totalTiesData - 1, _totalLinks);
        }
    }
    assert(totalTiesData <= _totalLinks);

    // On position 2*i goes tie for Object_i.3 and on position 2*i+1 goes tie for Object_i.5
    int64_t maxEndTies = 2 * _nTotalObjects;
    assert(maxEndTies > 0);
// FIXME this may blow up memory
    endTies_t *endTies = calloc_chk(maxEndTies, sizeof(endTies_t));
    splintTracking_endTies_t *splintTracking_endTies =
        calloc_chk(maxEndTies, sizeof(splintTracking_endTies_t));

    dataTie_t *tiesDataHeap = NULL;
    splintDataTie_t *splintTiesDataHeap = NULL;

    int64_t maxTotalTies = 2 * totalTiesData + 1;
// FIXME this memory will blow up too
    if (totalTiesData) {
        tiesDataHeap = calloc_chk(maxTotalTies, sizeof(dataTie_t));
        splintTiesDataHeap = calloc_chk(maxTotalTies, sizeof(splintDataTie_t));
    }

    for (int64_t i = 0; i < maxEndTies; i++) {
        endTies[i].nTies = 0;
        splintTracking_endTies[i].nTies = 0;
    }

    DBG("initialized endTies\n");
    // First count the entries
    localTieHeapLimit = 0;
    localTieHeapIdx = 0;
    remoteTieHeapThread = 0;
    int64_t posInEndTies;
    for (int64_t i = 0; i < totalTiesData; i++) {
        if (i >= _totalLinks) {
            DIE("Index exceeds total links: %lld >= %lld\n"
                "Check parameters: is -L set correctly?\n", (lld)i, (lld)_totalLinks);
        }
        // localize remote heap when iterating
        while (i == localTieHeapIdx + localTieHeapLimit) {
            assert(remoteTieHeapThread < THREADS);
            localTieHeapIdx += localTieHeapLimit;
            LOCALIZE_HEAP(localTieHeap, _pp[pairThreshold].tieSharedHeap[remoteTieHeapThread], localTieHeapLimit);
            assert(localTieHeapLimit % sizeof(tie_t) == 0);
            localTieHeapLimit /= sizeof(tie_t);
            remoteTieHeapThread++;
        }
        int64_t lth_i = i - localTieHeapIdx;
        if (lth_i < 0 || lth_i > localTieHeapLimit) {
            DIE("Invalid local tie heap idx: lth_i=%lld localTieHeapLimit=%lld localTieHeapIdx=%lld totalTiesData=%lld _totalLinks=%lld remoteTieHeapThread=%d pairThreshold=%d sizeof(tie_t)=%lld\n", (lld)lth_i, (lld)localTieHeapLimit, (lld)localTieHeapIdx, (lld)totalTiesData, (lld)_totalLinks, (int)remoteTieHeapThread - 1, (int)pairThreshold, (lld)sizeof(tie_t));
        }
        if (localTieHeap[lth_i].ending1 == 3) {
            posInEndTies = 2 * localTieHeap[lth_i].end1_id;
        } else {
            posInEndTies = 2 * localTieHeap[lth_i].end1_id + 1;
        }
        CHECK_BOUNDS(posInEndTies, maxEndTies);
        endTies[posInEndTies].nTies++;
        splintTracking_endTies[posInEndTies].nTies++;

        if (localTieHeap[lth_i].ending2 == 3) {
            posInEndTies = 2 * localTieHeap[lth_i].end2_id;
        } else {
            posInEndTies = 2 * localTieHeap[lth_i].end2_id + 1;
        }
        CHECK_BOUNDS(posInEndTies, maxEndTies);
        endTies[posInEndTies].nTies++;
        splintTracking_endTies[posInEndTies].nTies++;
    }

    DBG("Done with counting endTies: %lld\n", (lld)posInEndTies);

    // Now do the allocation so that the data lies serially in each entry of endTies
    assert(maxEndTies > 0);
    int64_t runningPtr = 0;
    if (totalTiesData) {
        CHECK_BOUNDS(runningPtr, maxTotalTies);
        endTies[0].ties = &tiesDataHeap[runningPtr];
        splintTracking_endTies[0].ties = &splintTiesDataHeap[runningPtr];
    } else {
        endTies[0].ties = NULL;
        splintTracking_endTies[0].ties = NULL;
    }

    endTies[0].cur_pos = 0;
    splintTracking_endTies[0].cur_pos = 0;

    runningPtr += endTies[0].nTies;

    for (int64_t i = 1; i < maxEndTies; i++) {
        if (totalTiesData) {
            CHECK_BOUNDS(runningPtr, maxTotalTies);
            endTies[i].ties = &tiesDataHeap[runningPtr];
            splintTracking_endTies[i].ties = &splintTiesDataHeap[runningPtr];
        } else {
            endTies[i].ties = NULL;
            splintTracking_endTies[i].ties = NULL;
        }
        endTies[i].cur_pos = 0;

        splintTracking_endTies[i].cur_pos = 0;

        runningPtr += endTies[i].nTies;
    }

    // Privatization of remoteoNoObjectPtrs
    oNoObject *localONOObjectLengths = calloc_chk(_nTotalObjects, sizeof(oNoObject));
    oNoObject *sortedByLen = calloc_chk(_nTotalObjects, sizeof(oNoObject));

    int64_t objectsPerActiveThread = (_nTotalObjects + _nActiveThreads) / _nActiveThreads;
    int64_t sizeEnt = objectsPerActiveThread * sizeof(oNoObject);
    assert(sizeEnt >= sizeof(oNoObject));
    oNoObject *tmpBuf = calloc_chk(objectsPerActiveThread, sizeof(oNoObject));

    for (int heapNum = 0; heapNum < _nActiveThreads; heapNum++) {
        int heapThread = heapNum + minActiveThread;
        assert(heapThread < THREADS);
        assert(_pp[pairThreshold].sharingoNoObjectPtrs[heapThread] != NULL);
        upc_memget(tmpBuf, _pp[pairThreshold].sharingoNoObjectPtrs[heapThread], sizeEnt);
        int64_t pos = 0;
        for (int64_t i = heapNum; i < _nTotalObjects; i += _nActiveThreads) {
            CHECK_BOUNDS(pos, objectsPerActiveThread);
            if (tmpBuf[pos].my_id >= _nTotalObjects || tmpBuf[pos].length < 0) {
                WARN("Failure in bounds: i=%lld, pos=%lld.  my_id=%lld length=%lld\n",
                     (lld)i, (lld)pos, (lld)tmpBuf[pos].my_id, (lld)tmpBuf[pos].length);
            }
            CHECK_BOUNDS(tmpBuf[pos].my_id, _nTotalObjects);
            localONOObjectLengths[i] = tmpBuf[pos];
            sortedByLen[i] = localONOObjectLengths[i];
            pos++;
        }
    }

    free_chk(tmpBuf);

#ifdef DEBUG
    for (int64_t i = 0; i < _nTotalObjects; i++) {
        int64_t cur_end = sortedByLen[i].my_id;
        DBG2("sortByLen: %lld cur_end %lld\n", (lld)i, (lld)cur_end);
    }
#endif

    DBG("Ready to iterate\n");

    // Now iterate over the data and store ties appropriately
    localTieHeapLimit = 0;
    localTieHeapIdx = 0;
    remoteTieHeapThread = 0;
    for (int64_t i = 0; i < totalTiesData; i++) {
        // localize remote heap when iterating
        while (i == localTieHeapIdx + localTieHeapLimit) {
            assert(remoteTieHeapThread < THREADS);
            localTieHeapIdx += localTieHeapLimit;
            LOCALIZE_HEAP(localTieHeap, _pp[pairThreshold].tieSharedHeap[remoteTieHeapThread], localTieHeapLimit);
            assert(localTieHeapLimit % sizeof(tie_t) == 0);
            localTieHeapLimit /= sizeof(tie_t);
            remoteTieHeapThread++;
        }
        int64_t lth_i = i - localTieHeapIdx;
        int nGapL = localTieHeap[lth_i].nGapLinks;
        int gapE = localTieHeap[lth_i].gapEstimate;
        int gapU = localTieHeap[lth_i].gapUncertainty;

        if (localTieHeap[lth_i].ending1 == 3) {
            posInEndTies = 2 * localTieHeap[lth_i].end1_id;
        } else {
            posInEndTies = 2 * localTieHeap[lth_i].end1_id + 1;
        }

        CHECK_BOUNDS(posInEndTies, maxEndTies);
        int cPos = endTies[posInEndTies].cur_pos;

        dataTie_t *dataArray = endTies[posInEndTies].ties;

        splintDataTie_t *splintDataArray = splintTracking_endTies[posInEndTies].ties;

        dataArray[cPos].nGapLinks = nGapL;
        dataArray[cPos].gapEstimate = gapE;
        dataArray[cPos].gapUncertainty = gapU;
        dataArray[cPos].end2_id = localTieHeap[lth_i].end2_id;
        dataArray[cPos].ending2 = localTieHeap[lth_i].ending2;

        splintDataArray[cPos].end2_id = localTieHeap[lth_i].end2_id;
        splintDataArray[cPos].ending2 = localTieHeap[lth_i].ending2;
        splintDataArray[cPos].isSplinted = localTieHeap[lth_i].splintFlag;


        dataArray[cPos].endLength = localONOObjectLengths[localTieHeap[lth_i].end2_id].length;
        endTies[posInEndTies].cur_pos++;
        splintTracking_endTies[posInEndTies].cur_pos++;


        if (localTieHeap[lth_i].ending2 == 3) {
            posInEndTies = 2 * localTieHeap[lth_i].end2_id;
        } else {
            posInEndTies = 2 * localTieHeap[lth_i].end2_id + 1;
        }

        CHECK_BOUNDS(posInEndTies, maxEndTies);
        cPos = endTies[posInEndTies].cur_pos;
        dataArray = endTies[posInEndTies].ties;

        splintDataArray = splintTracking_endTies[posInEndTies].ties;

        dataArray[cPos].nGapLinks = nGapL;
        dataArray[cPos].gapEstimate = gapE;
        dataArray[cPos].gapUncertainty = gapU;
        dataArray[cPos].end2_id = localTieHeap[lth_i].end1_id;
        dataArray[cPos].ending2 = localTieHeap[lth_i].ending1;

        splintDataArray[cPos].end2_id = localTieHeap[lth_i].end1_id;
        splintDataArray[cPos].ending2 = localTieHeap[lth_i].ending1;
        splintDataArray[cPos].isSplinted = localTieHeap[lth_i].splintFlag;

        dataArray[cPos].endLength = localONOObjectLengths[localTieHeap[lth_i].end1_id].length;
        endTies[posInEndTies].cur_pos++;
        splintTracking_endTies[posInEndTies].cur_pos++;
    }

    double storeTiesTime = ELAPSED_SECS(t);

    LOGF("Done with iterations.. %lld\n", (lld)_nTotalObjects);

    // Now endTies array is ready to be used

    t = UPC_TICKS_NOW();
    qsort(sortedByLen, _nTotalObjects, sizeof(oNoObject), oNocmpfunc);
    double sortTime = ELAPSED_SECS(t);

    LOGF("Done with sort %lld\n", (lld)_nTotalObjects);

    t = UPC_TICKS_NOW();

    // Thread 0 serially marks ends
    // On position 2*i goes tie for Object_i.3 and on position 2*i+1 goes tie for Object_i.5
    int64_t maxEndMarks = 2 * _nTotalObjects;
    unsigned char *endMarks = (unsigned char *)calloc_chk(maxEndMarks, sizeof(unsigned char));

    int64_t n_averted_ties = 0;

    // Marking ends
    for (int64_t i = 0; i < _nTotalObjects; i++) {
        DBG2("Marking end %lld\n", (lld)i);

        int64_t cur_end = sortedByLen[i].my_id;
        CHECK_BOUNDS(cur_end, _nTotalObjects);

        char cur_ending = 5;
        unsigned char endMark = markEnd(cur_end, cur_ending, endTies, splintTracking_endTies, maxEndTies,
                                        localContigDepth, _peakDepth, _depthInfoAvailable, _merSize,
                                        _scaffoldReportFilePrefix ? 1 : 0, scaffReportLocal, &n_averted_ties);

        CHECK_BOUNDS(2 * cur_end + 1, maxEndMarks);
        endMarks[2 * cur_end + 1] = endMark;

        cur_ending = 3;
        endMark = markEnd(cur_end, cur_ending, endTies, splintTracking_endTies, maxEndTies,
                          localContigDepth, _peakDepth, _depthInfoAvailable, _merSize,
                          _scaffoldReportFilePrefix ? 1 : 0, scaffReportLocal, &n_averted_ties);

        CHECK_BOUNDS(2 * cur_end, maxEndMarks);
        endMarks[2 * cur_end] = endMark;
    }

    LOGF("Marked ends\n");
    double markendTime = ELAPSED_SECS(t);

    t = UPC_TICKS_NOW();

    // Finding best ties
    int piece;
    char bestTie;
    int closestRes;
    char closestResEnding;
    int bestTiePos, bestTiedArrayPos;
    endList_t *newEndObject;

    char *suspended = calloc_chk(_nTotalObjects, sizeof(char));
//   for (int64_t i = 0; i < _nTotalObjects; i++) {
//      suspended[i] = 0;
//   }

    int64_t maxTieArray = 2 * _nTotalObjects;
    end_t *bestTieArray = malloc_chk(maxTieArray * sizeof(end_t));
    endList_t *bestTiedByArray = malloc_chk(maxTieArray * sizeof(endList_t));
    for (int64_t i = 0; i < maxTieArray; i++) {
        bestTieArray[i].end_id = UNDEFINED;
        bestTiedByArray[i].end_id = UNDEFINED;
        bestTiedByArray[i].next = NULL;
    }

    LOGF("Starting totalObjects: %lld\n", (lld)_nTotalObjects);

    for (int64_t i = 0; i < _nTotalObjects; i++) {
        piece = sortedByLen[i].my_id;
        CHECK_BOUNDS(piece, _nTotalObjects);
        if (suspended[piece] == 0) {
            char cur_ending = 5;
            CHECK_BOUNDS(2 * piece + 1, maxEndMarks);
            unsigned char endMark = endMarks[2 * piece + 1];
            bestTie = endMark;

            CHECK_UPPER_BOUND(endMark, END_TYPES);
            statsBestTie[endMark]++;

            if (endMark == UNMARKED) {
                bestTie = bestTieFunction(piece, cur_ending, endTies, maxEndTies, localONOObjectLengths,
                                          suspendable, endMarks, suspended, &closestRes, &closestResEnding);
                bestTiePos = 2 * piece + 1;
                CHECK_BOUNDS(bestTiePos, maxTieArray);
                bestTieArray[bestTiePos].end_id = closestRes;
                bestTieArray[bestTiePos].ending = closestResEnding;

                if (bestTie != NO_GOOD_TIES) {
                    if (closestResEnding == 3) {
                        bestTiedArrayPos = 2 * closestRes;
                    } else {
                        bestTiedArrayPos = 2 * closestRes + 1;
                    }

                    CHECK_BOUNDS(bestTiedArrayPos, maxTieArray);
                    if (bestTiedByArray[bestTiedArrayPos].end_id == UNDEFINED) {
                        bestTiedByArray[bestTiedArrayPos].end_id = piece;
                        bestTiedByArray[bestTiedArrayPos].ending = cur_ending;
                    } else {
                        newEndObject = malloc_chk(sizeof(endList_t));
                        newEndObject->next = bestTiedByArray[bestTiedArrayPos].next;
                        bestTiedByArray[bestTiedArrayPos].next = newEndObject;
                        newEndObject->end_id = piece;
                        newEndObject->ending = cur_ending;
                    }
                }
            }

            cur_ending = 3;
            CHECK_BOUNDS(2 * piece, maxEndMarks);
            endMark = endMarks[2 * piece];
            bestTie = endMark;

            CHECK_UPPER_BOUND(endMark, END_TYPES);
            statsBestTie[endMark]++;

            if (endMark == UNMARKED) {
                bestTie = bestTieFunction(piece, cur_ending, endTies, maxEndTies, localONOObjectLengths,
                                          suspendable, endMarks, suspended, &closestRes, &closestResEnding);
                bestTiePos = 2 * piece;
                CHECK_BOUNDS(bestTiePos, maxTieArray);
                bestTieArray[bestTiePos].end_id = closestRes;
                bestTieArray[bestTiePos].ending = closestResEnding;


                if (bestTie != NO_GOOD_TIES) {
                    if (closestResEnding == 3) {
                        bestTiedArrayPos = 2 * closestRes;
                    } else {
                        bestTiedArrayPos = 2 * closestRes + 1;
                    }

                    CHECK_BOUNDS(bestTiedArrayPos, maxTieArray);
                    if (bestTiedByArray[bestTiedArrayPos].end_id == UNDEFINED) {
                        bestTiedByArray[bestTiedArrayPos].end_id = piece;
                        bestTiedByArray[bestTiedArrayPos].ending = cur_ending;
                    } else {
                        newEndObject = malloc_chk(sizeof(endList_t));
                        newEndObject->next = bestTiedByArray[bestTiedArrayPos].next;
                        bestTiedByArray[bestTiedArrayPos].next = newEndObject;
                        newEndObject->end_id = piece;
                        newEndObject->ending = cur_ending;
                    }
                }
            }
        }
    }

    LOGF("Placing suspended ties\n");
    double bestTiesTime = ELAPSED_SECS(t);

    t = UPC_TICKS_NOW();

    // Place suspended where possible
    dataTie_t *ties5, *ties3;
    end_t check_fast[TIE_FAST_SIZE];
    end_t *check_slow = NULL;
    end_t *check = NULL;
    int checkFlag;
    checkFlag = 1;
    unsigned char endMark3, endMark5;
    int nTies3, nTies5;
    int64_t j, k;
    int search_end5, search_pos5;
    char search_ending5;
    end_t bt5, t5, t3, tie5, tie3;
    int exists, nValidSuspensions, aux_pos;

    for (int64_t i = 0; i < _nTotalObjects; i++) {
        piece = sortedByLen[i].my_id;

        CHECK_BOUNDS(piece, _nTotalObjects);
        if (suspended[piece] == 0) {
            continue;
        }

        CHECK_BOUNDS(2 * piece + 1, maxEndMarks);
        endMark5 = endMarks[2 * piece + 1];
        endMark3 = endMarks[2 * piece];

        if ((endMark5 != UNMARKED) || (endMark3 != UNMARKED)) {
            continue;
        }

        CHECK_BOUNDS(2 * piece + 1, maxEndTies);
        nTies5 = endTies[2 * piece + 1].nTies;
        if (nTies5 > 0 && endTies[2 * piece + 1].ties) {
            ties5 = endTies[2 * piece + 1].ties;
        } else {
            continue;
        }

        CHECK_BOUNDS(2 * piece, maxEndTies);
        nTies3 = endTies[2 * piece].nTies;
        if (nTies3 > 0 && endTies[2 * piece].ties) {
            ties3 = endTies[2 * piece].ties;
        } else {
            continue;
        }

        // Decide to use preallocated array or allocate a new one
        if (nTies5 <= TIE_FAST_SIZE) {
            checkFlag = 1;
            check = &(check_fast[0]);
        } else {
            checkFlag = 0;
            check_slow = malloc_chk(nTies5 * sizeof(end_t));
            check = check_slow;
        }

        for (j = 0; j < nTies5; j++) {
            search_end5 = ties5[j].end2_id;
            search_ending5 = ties5[j].ending2;
            if (search_ending5 == 3) {
                search_pos5 = 2 * search_end5;
            } else {
                search_pos5 = 2 * search_end5 + 1;
            }

            CHECK_BOUNDS(search_pos5, maxTieArray);
            bt5 = bestTieArray[search_pos5];
            if (bt5.end_id == UNDEFINED) {
                check[j].end_id = UNDEFINED;
                continue;
            }
            exists = 0;
            k = 0;
            while ((k < nTies3) && (exists == 0)) {
                if ((ties3[k].end2_id == bt5.end_id) && (ties3[k].ending2 == bt5.ending)) {
                    exists = 1;
                }
                k++;
            }

            if (exists == 1) {
                check[j] = bt5;
            } else {
                check[j].end_id = UNDEFINED;
            }
        }

        nValidSuspensions = 0;
        for (j = 0; j < nTies5; j++) {
            if (check[j].end_id == UNDEFINED) {
                continue;
            }
            t5.end_id = ties5[j].end2_id;
            t5.ending = ties5[j].ending2;
            t3 = check[j];

            if (mutualUniqueBest(t5, t3, bestTieArray, bestTiedByArray, suspended) == 1) {
                if (nValidSuspensions == 0) {
                    tie5 = t5;
                    tie3 = t3;
                }
                nValidSuspensions++;
            }
        }

        if (nValidSuspensions == 1) {
            CHECK_BOUNDS(piece, _nTotalObjects);
            suspended[piece] = 0;

            if (tie5.ending == 3) {
                aux_pos = 2 * tie5.end_id;
            } else {
                aux_pos = 2 * tie5.end_id + 1;
            }
            CHECK_BOUNDS(aux_pos, maxTieArray);
            bestTieArray[aux_pos].end_id = piece;
            bestTieArray[aux_pos].ending = 5;
            bestTiedByArray[aux_pos].end_id = piece;
            bestTiedByArray[aux_pos].ending = 5;
            bestTiedByArray[aux_pos].next = NULL;
            aux_pos = 2 * piece + 1;
            CHECK_BOUNDS(aux_pos, maxTieArray);
            bestTieArray[aux_pos].end_id = tie5.end_id;
            bestTieArray[aux_pos].ending = tie5.ending;
            bestTiedByArray[aux_pos].end_id = tie5.end_id;
            bestTiedByArray[aux_pos].ending = tie5.ending;
            bestTiedByArray[aux_pos].next = NULL;

            if (tie3.ending == 3) {
                aux_pos = 2 * tie3.end_id;
            } else {
                aux_pos = 2 * tie3.end_id + 1;
            }

            CHECK_BOUNDS(aux_pos, maxTieArray);
            bestTieArray[aux_pos].end_id = piece;
            bestTieArray[aux_pos].ending = 3;
            bestTiedByArray[aux_pos].end_id = piece;
            bestTiedByArray[aux_pos].ending = 3;
            bestTiedByArray[aux_pos].next = NULL;
            aux_pos = 2 * piece;
            CHECK_BOUNDS(aux_pos, maxTieArray);
            bestTieArray[aux_pos].end_id = tie3.end_id;
            bestTieArray[aux_pos].ending = tie3.ending;
            bestTiedByArray[aux_pos].end_id = tie3.end_id;
            bestTiedByArray[aux_pos].ending = tie3.ending;
            bestTiedByArray[aux_pos].next = NULL;

            nInsertedSuspensions++;
        }

        if (checkFlag == 0) {
            if (check_slow) {
                free_chk(check_slow);
            }
        }
    }

    LOGF("Locking ends together\n");

    double suspensionsTime = ELAPSED_SECS(t);

    t = UPC_TICKS_NOW();

    //  Lock ends together with no competing ties
    int64_t maxEndLocks = 2 * _nTotalObjects;
    endLock_t *endLocks = malloc_chk(maxEndLocks * sizeof(endLock_t));
    char piece_ending;
    end_t packedEnd1, BestTie;
    endList_t btb;
    dataTie_t tieInfo;
    int getResult, posBestTie, nTies;

    for (int64_t i = 0; i < maxEndLocks; i++) {
        endLocks[i].end_id = UNDEFINED;
    }

    for (int64_t i = 0; i < maxTieArray; i++) {
        btb = bestTiedByArray[i];
        if (btb.end_id == UNDEFINED) {
            continue;
        }

        CHECK_BOUNDS(i, maxEndLocks);
        if (endLocks[i].end_id != UNDEFINED) {
            continue;
        }

        piece = i / 2;
        if ((i % 2) == 0) {
            piece_ending = 3;
        } else {
            piece_ending = 5;
        }

        CHECK_BOUNDS(piece, _nTotalObjects);
        if (suspended[piece] == 1) {
            continue;
        }

        BestTie = bestTieArray[i];
        if (BestTie.end_id == UNDEFINED) {
            continue;
        }

        nTies = 0;
        if (btb.next == NULL) {
            nTies = 1;
        }

        if ((nTies == 1) && (btb.end_id == BestTie.end_id) && (btb.ending == BestTie.ending)) {
            packedEnd1.end_id = piece;
            packedEnd1.ending = piece_ending;
            getResult = getTieInfo(packedEnd1, BestTie, endTies, maxEndTies, &tieInfo);
            if (getResult == 1) {
                endLocks[i].end_id = BestTie.end_id;
                endLocks[i].ending = BestTie.ending;
                endLocks[i].gap = tieInfo.gapEstimate;
                endLocks[i].gapUncertainty = tieInfo.gapUncertainty;
                if (BestTie.ending == 3) {
                    posBestTie = 2 * BestTie.end_id;
                } else {
                    posBestTie = 2 * BestTie.end_id + 1;
                }
                CHECK_BOUNDS(posBestTie, maxEndLocks);
                endLocks[posBestTie].end_id = piece;
                endLocks[posBestTie].ending = piece_ending;
                endLocks[posBestTie].gap = tieInfo.gapEstimate;
                endLocks[posBestTie].gapUncertainty = tieInfo.gapUncertainty;
            }
        } else {
            endLocks[i].end_id = DIVERGENCE;
        }
    }

    LOGF("Traverse ends locks\n");

    double lockEndsTime = ELAPSED_SECS(t);

    t = UPC_TICKS_NOW();

    int64_t scaffContigIndex, originalContigName;
    int scaffCoord, scaffReportPos, nextContigLen, nReportLines, visitReverse, r,
        actR, p0, p1, originalContigLength, contigStartCoord, contigEndCoord,
        originalGapSize, originalGapUncertainty;
    char contigOri;
    oNoObject curObject;
    int64_t scaffoldID = 0;
    char startEnd, preState, nextEnd, prevEnd;
    char *visitedContigs = calloc_chk(_nTotalObjects, sizeof(char));

    char *loopCheck = calloc_chk(_nTotalObjects, sizeof(char));
    //memset(loopCheck, 0, _nTotalObjects * sizeof(char));

    int64_t maxCleanupIndices = _nTotalObjects;
    int64_t *cleanupIndices = calloc_chk(maxCleanupIndices, sizeof(int64_t));
    int toCleanUp = 0;
    int z;

    end_t end;
    int contigLen, startContig, posAux, inScaffold, nextContig, nextGap, nextGapUncertainty, prevContig;
    endLock_t next, preStateCompact;
    char scaffReportLine[MAX_SCAFF_LINE_SIZE], originalContigOri;
    scaf_entry_t *reportLines;
    scaf_report_t oldReport;
    shared[] scaf_entry_t * AuxreportLines;

    int *scaff_lens = calloc_chk(_nTotalObjects, sizeof(int));

    //for (int64_t i=0; i<_nTotalObjects; i++) {
    //visitedContigs[i] = 0;
    //}

    scaf_entry_t **cachedEntries = calloc_chk(_nTotalObjects, sizeof(scaf_entry_t *));
#ifdef DEBUG
    for (int64_t i = 0; i < _nTotalObjects; i++) {
        assert(cachedEntries[i] == NULL);
    }
#endif
    LOGF("Allocated %lld objects %0.3f MB (private)\n", (lld)_nTotalObjects, 1.0 / ONE_MB * _nTotalObjects * sizeof(scaf_entry_t *));

    _nGaps[pairThreshold] = 0;
    int64_t scaffoldLineCount = 0;

    double while1Time = 0, while2Time = 0, writeTime = 0;

    for (int64_t localObjectIdx = 0; localObjectIdx < _nTotalObjects; localObjectIdx++) {
        curObject = localONOObjectLengths[localObjectIdx];
        contigLen = curObject.length;
        int64_t contig = curObject.my_id;
        if (contigLen == 0) {
            continue;
        }
        startContig = contig;
        startEnd = 5;

        CHECK_BOUNDS(contig, _nTotalObjects);
        if (visitedContigs[contig] == 1) {
            continue;
        }

        preState = TERMINATION;

        UPC_TICK_T while1S = UPC_TICKS_NOW();
        toCleanUp = 0;

        while (1) {
            CHECK_BOUNDS(startContig, _nTotalObjects);
            if (loopCheck[startContig] == 1) {
                break;
            }

            loopCheck[startContig] = 1;

            CHECK_BOUNDS(toCleanUp, maxCleanupIndices);
            cleanupIndices[toCleanUp] = startContig;
            toCleanUp++;

            end.end_id = startContig;
            end.ending = startEnd;
            if (startEnd == 3) {
                posAux = 2 * startContig;
            } else {
                posAux = 2 * startContig + 1;
            }
            CHECK_BOUNDS(posAux, maxEndLocks);
            next = endLocks[posAux];
            if (next.end_id != UNDEFINED) {
                if ((next.end_id == DIVERGENCE) || (next.end_id == CONVERGENCE)) {
                    preState = NEXT;
                    preStateCompact = next;
                    break;
                } else {
                    startContig = next.end_id;
                    if (next.ending == 3) {
                        startEnd = 5;
                    } else {
                        startEnd = 3;
                    }
                    preState = EXTENSION;
                }
            } else {
                preState = TERMINATION;
                break;
            }
        }

        DBG2("cleanup loop %lld: toCleanUp %d\n", (lld)localObjectIdx, toCleanUp);

        // Cleanup loopcheck without memset
        for (z = 0; z < toCleanUp; z++) {
            CHECK_BOUNDS(z, maxCleanupIndices);
            CHECK_BOUNDS(cleanupIndices[z], _nTotalObjects);
            loopCheck[cleanupIndices[z]] = 0;
        }
        toCleanUp = 0;

        while1Time += ELAPSED_SECS(while1S);

        inScaffold = 0;
        nextContig = startContig;
        if (startEnd == 3) {
            nextEnd = 5;
        } else {
            nextEnd = 3;
        }
        nextGap = 0;
        nextGapUncertainty = 0;
        prevContig = nextContig;
        prevEnd = nextEnd;
        scaffCoord = 1;
        scaffContigIndex = 1;
        scaffReportPos = 0;

        UPC_TICK_T while2S = UPC_TICKS_NOW();

        while (1) {
            CHECK_BOUNDS(nextContig, _nTotalObjects);
            if (loopCheck[nextContig] == 1) {
                break;
            }

            loopCheck[nextContig] = 1;

            CHECK_BOUNDS(toCleanUp, maxCleanupIndices);
            cleanupIndices[toCleanUp] = nextContig;
            toCleanUp++;

            CHECK_BOUNDS(nextContig, _nTotalObjects);
            visitedContigs[nextContig] = 1;
            nextContigLen = localONOObjectLengths[nextContig].length;
            contigOri = '+';

            if (nextEnd == 5) {
                contigOri = '-';
            }

            if (inScaffold == 1) {
                sprintf(scaffReportLine, "Scaffold%lld\tGAP%lld\t%d\t%d\n", (lld)scaffoldID, (lld)scaffContigIndex,
                        nextGap, nextGapUncertainty);
                UPC_TICK_T writeS = UPC_TICKS_NOW();
                append_scaffold(scaffoldID, scaffReportLine, cachedSharedScaffolds);
                scaffoldLineCount++;
                writeTime += ELAPSED_SECS(writeS);
                scaffCoord += nextGap;
                scaffContigIndex++;
                _nGaps[pairThreshold]++;
            }

            if (_scaffoldReportFilePrefix) {
                CHECK_BOUNDS(nextContig, _nTotalObjects);
                oldReport = scaffReportLocal[nextContig];
                nReportLines = oldReport.nEntries;
                visitReverse = 0;
                if (contigOri == '-') {
                    visitReverse = 1;
                }

                CHECK_BOUNDS(nextContig, _nTotalObjects);
                if (cachedEntries[nextContig] == NULL) {
                    reportLines = malloc_chk(nReportLines * sizeof(scaf_entry_t));
                    upc_memget(reportLines, oldReport.entries, nReportLines * sizeof(scaf_entry_t));
                    cachedEntries[nextContig] = reportLines;
                } else {
                    reportLines = cachedEntries[nextContig];
                }

                for (r = 0; r < nReportLines; r++) {
                    if (visitReverse == 0) {
                        actR = r;
                    } else {
                        actR = nReportLines - r - 1;
                    }

                    CHECK_BOUNDS(actR, nReportLines);
                    DBG("Processing reportLine r=%lld actR=%lld entry_id=%lld\n", (lld)r, (lld)actR, (lld)reportLines[actR].entry_id);
                    if (reportLines[actR].type == CONTIG) {
                        originalContigName = reportLines[actR].entry_id;
                        CHECK_BOUNDS(originalContigName, _totalContigs);
                        p0 = reportLines[actR].f1;
                        p1 = reportLines[actR].f2;
                        originalContigLength = p1 - p0 + 1;
                        if (reportLines[actR].sign == PLUS) {
                            originalContigOri = '+';
                        } else {
                            originalContigOri = '-';
                        }
                        if (contigOri == originalContigOri) {
                            originalContigOri = '+';
                        } else {
                            originalContigOri = '-';
                        }
                        contigStartCoord = scaffCoord;
                        contigEndCoord = scaffCoord + originalContigLength - 1;
                        if (_depthInfoAvailable) {
                            assert(localContigDepth != NULL);
                            float fdepth = localContigDepth[originalContigName];
                            sprintf(scaffReportLine, "Scaffold%lld\tCONTIG%lld\t%cContig%lld\t%d\t%d\t%f\n",
                                    (lld)scaffoldID, (lld)scaffContigIndex, originalContigOri, (lld)originalContigName,
                                    contigStartCoord, contigEndCoord, fdepth);
                        } else {
                            sprintf(scaffReportLine, "Scaffold%lld\tCONTIG%lld\t%cContig%lld\t%d\t%d\n",
                                    (lld)scaffoldID, (lld)scaffContigIndex, originalContigOri, (lld)originalContigName,
                                    contigStartCoord, contigEndCoord);
                        }

                        CHECK_BOUNDS(scaffoldID, _nTotalObjects);
                        scaff_lens[scaffoldID] = contigEndCoord;

                        UPC_TICK_T writeS = UPC_TICKS_NOW();
                        append_scaffold(scaffoldID, scaffReportLine, cachedSharedScaffolds);
                        scaffoldLineCount++;
                        writeTime += ELAPSED_SECS(writeS);
                        scaffCoord += originalContigLength;
                    } else {
                        originalGapSize = reportLines[actR].f1;
                        originalGapUncertainty = reportLines[actR].f2;
                        sprintf(scaffReportLine, "Scaffold%lld\tGAP%lld\t%d\t%d\n", (lld)scaffoldID,
                                (lld)scaffContigIndex, originalGapSize, originalGapUncertainty);
                        _nGaps[pairThreshold]++;
                        UPC_TICK_T writeS = UPC_TICKS_NOW();
                        append_scaffold(scaffoldID, scaffReportLine, cachedSharedScaffolds);
                        scaffoldLineCount++;
                        writeTime += ELAPSED_SECS(writeS);

                        scaffCoord += originalGapSize;
                        scaffContigIndex++;
                    }
                }
            } else {
                contigStartCoord = scaffCoord;
                contigEndCoord = scaffCoord + nextContigLen - 1;

                if (_depthInfoAvailable) {
                    assert(localContigDepth != NULL);
                    CHECK_BOUNDS(nextContig, _totalContigs);
                    float fdepth = localContigDepth[nextContig];
                    sprintf(scaffReportLine, "Scaffold%lld\tCONTIG%lld\t%cContig%lld\t%d\t%d\t%f\n",
                            (lld)scaffoldID, (lld)scaffContigIndex, contigOri, (lld)nextContig, contigStartCoord,
                            contigEndCoord, fdepth);
                } else {
                    sprintf(scaffReportLine, "Scaffold%lld\tCONTIG%lld\t%cContig%lld\t%d\t%d\n", (lld)scaffoldID,
                            (lld)scaffContigIndex, contigOri, (lld)nextContig, contigStartCoord, contigEndCoord);
                }

                CHECK_BOUNDS(scaffoldID, _nTotalObjects);
                scaff_lens[scaffoldID] = contigEndCoord;

                UPC_TICK_T writeS = UPC_TICKS_NOW();
                append_scaffold(scaffoldID, scaffReportLine, cachedSharedScaffolds);
                scaffoldLineCount++;
                writeTime += ELAPSED_SECS(writeS);
                scaffCoord += nextContigLen;
            }

            inScaffold = 1;

            end.end_id = nextContig;
            end.ending = nextEnd;
            if (nextEnd == 3) {
                posAux = 2 * nextContig;
            } else {
                posAux = 2 * nextContig + 1;
            }
            CHECK_BOUNDS(posAux, maxEndLocks);
            next = endLocks[posAux];
            if (next.end_id != UNDEFINED) {
                if ((next.end_id == DIVERGENCE) || (next.end_id == CONVERGENCE)) {
                    break;
                } else {
                    prevContig = nextContig;
                    prevEnd = nextEnd;
                    nextContig = next.end_id;
                    if (next.ending == 3) {
                        nextEnd = 5;
                    } else {
                        nextEnd = 3;
                    }
                    nextGap = next.gap;
                    nextGapUncertainty = next.gapUncertainty;
                }
            } else {
                break;
            }
        }
        assert(scaffoldID <= localObjectIdx);
        DBG2("done with scaffold: %lld (local object %lld)\n", (lld)scaffoldID, (lld)localObjectIdx);
        scaffoldID++;

        // Cleanup loopcheck without memset
        for (z = 0; z < toCleanUp; z++) {
            CHECK_BOUNDS(z, maxCleanupIndices);
            CHECK_BOUNDS(cleanupIndices[z], _nTotalObjects);
            loopCheck[cleanupIndices[z]] = 0;
        }

        while2Time += ELAPSED_SECS(while2S);
    }
    LOGF("done iterating over scaffolds %lld numLines %lld\n", _nTotalObjects, scaffoldLineCount);
    assert(scaffoldID > 0);
    int64_t nScaffs = scaffoldID;

    double traverseLocksTime = ELAPSED_SECS(t);

    CHECK_BOUNDS(nScaffs, _nTotalObjects + 1);
    _n50s[pairThreshold] = calcN50(scaff_lens, nScaffs);

    if (_scaffoldReportFilePrefix) {
        if (scaffReportLocal) {
            free_chk(scaffReportLocal);
        }
        for (int64_t i = 0; i < _nTotalObjects; i++) {
            if (cachedEntries[i] != NULL) {
                free_chk(cachedEntries[i]);
                cachedEntries[i] = NULL;
            }
        }
    }

    free_chk(cachedEntries);

    free_chk(scaff_lens);
    free_chk(sortedByLen);
    if ((MYTHREAD == MIN_ACTIVE_THREAD(pairThreshold)) && localContigDepth) {
        free_chk(localContigDepth);
    }
    if (localTieHeap) {
        free_chk(localTieHeap);
    }
    free_chk(endTies);
    free_chk(splintTracking_endTies);
    if (totalTiesData) {
        free_chk(tiesDataHeap);
        free_chk(splintTiesDataHeap);
    }
    free_chk(localONOObjectLengths);
    free_chk(endMarks);
    free_chk(suspended);
    free_chk(bestTieArray);

    for (int64_t i = 0; i < 2 * _nTotalObjects; i++) {
        endList_t *curr = bestTiedByArray[i].next;
        while (curr) {
            endList_t *prev = curr;
            curr = curr->next;
            free_chk(prev);
        }
    }
    free_chk(bestTiedByArray);
    free_chk(endLocks);
    free_chk(visitedContigs);
    free_chk(loopCheck);
    free_chk(cleanupIndices);

    for (int q = 0; q < THREADS; q++) {
        _pp[pairThreshold].sharedScaffolds[q] = cachedSharedScaffolds[q];
    }
    free_chk(cachedSharedScaffolds);

    FILE *f = NULL;
    {
        char fname[MAX_FILE_PATH];
        sprintf(fname, "ono-serial-p%d", pairThreshold);

        //   printf("Thread %d finished serial section with pair threshold %d, N50 %d in %.3f s\n",
        //          MYTHREAD, pairThreshold, _n50s[pairThreshold], ELAPSED_SECS(tStart));

        f = fopen_rank_path(fname, "w", MYTHREAD);
    }

    fprintf(f, "In total we found %lld scaffolds\n", (lld)scaffoldID);
    fprintf(f, "Inserted %lld suspensions\n", (lld)nInsertedSuspensions);
    fprintf(f, "N50 is %d\n", _n50s[pairThreshold]);

    fprintf(f, "BEST TIE SUMMARY\n");
    fprintf(f, "======================================\n");
    fprintf(f, "Accepted :\t %lld\n", (lld)statsBestTie[UNMARKED]);
    fprintf(f, "======================================\n");
    fprintf(f, "Rejected SELF_TIE :\t %lld\n", (lld)statsBestTie[SELF_TIE]);
    fprintf(f, "Rejected TIE_COLLISION :\t %lld and \t %lld averted ties\n", (lld)statsBestTie[TIE_COLLISION], (lld)n_averted_ties);
    fprintf(f, "Rejected DEPTH_DROP:\t %lld\n", (lld)statsBestTie[DEPTH_DROP]);
    fprintf(f, "Rejected NO_TIES :\t %lld\n", (lld)statsBestTie[NO_TIES]);
    fprintf(f, "Rejected MULTIPLE_BEST_TIE :\t %lld\n", (lld)statsBestTie[MULTIPLE_BEST_TIE]);
    fprintf(f, "======================================\n");
    fprintf(f, "======================================\n\n\n");

    fprintf(f, "Sorting objects time is: %f seconds \n", sortTime);
    fprintf(f, "======================================\n");
    fprintf(f, "Storing ties locally time is : %f\n", storeTiesTime);
    fprintf(f, "Marking end time is : %f\n", markendTime);
    fprintf(f, "Adding suspension time is : %f\n", suspensionsTime);
    fprintf(f, "Finding best ties time is : %f\n", bestTiesTime);
    fprintf(f, "Locking end ties time is : %f\n", lockEndsTime);
    fprintf(f, "Traversing locks time is : %f\n", traverseLocksTime);
    fprintf(f, "======================================\n\n");

    fprintf(f, "While loop 1 time is %f\n", while1Time);
    fprintf(f, "While loop 2 time is %f\n", while2Time);
    fprintf(f, "Writing output time is %f\n", writeTime);

    fprintf(f, "======================================\n\n");

    fclose_track(f);

    _nScaffolds[pairThreshold] = scaffoldID;

    {
        FILE *fdforNmetaScaffs = NULL;
        char filenameForNscaffolds[100];

        char fullPrefix[MAX_FILE_PATH];
        sprintf(fullPrefix, "%s-%d", _outputPrefix, pairThreshold);

        sprintf(filenameForNscaffolds, "nScaffolds_%s.txt", fullPrefix);
        get_rank_path(filenameForNscaffolds, -1);
        fdforNmetaScaffs = fopen_chk(filenameForNscaffolds, "w");
        fprintf(fdforNmetaScaffs, "%lld\n", (lld)_nScaffolds[pairThreshold]);
        fclose_track(fdforNmetaScaffs);

        sprintf(filenameForNscaffolds, "nGaps_%s.txt", fullPrefix);
        get_rank_path(filenameForNscaffolds, -1);
        fdforNmetaScaffs = fopen_chk(filenameForNscaffolds, "w");
        fprintf(fdforNmetaScaffs, "%lld\n", (lld)_nGaps[pairThreshold]);
        fclose_track(fdforNmetaScaffs);

        char fnameForN50[MAX_FILE_PATH];
        sprintf(fnameForN50, "n50_%s.txt", fullPrefix);
        get_rank_path(fnameForN50, -1);
        FILE *fdForN50 = fopen_chk(fnameForN50, "w");
        fprintf(fdForN50, "%d %d\n", pairThreshold, _n50s[pairThreshold]);
        fclose_track(fdForN50);
    }
}  // end run_serial


void run_parallel(void)
{
    char *lineBuffers = calloc_chk(MAX_LINE_SIZE * 5, 1);
    char *line = lineBuffers;
    char *aux_line = line + MAX_LINE_SIZE;
    char *entries1 = aux_line + MAX_LINE_SIZE;
    char *entries2 = entries1 + MAX_LINE_SIZE;
    char *entries3 = entries2 + MAX_LINE_SIZE;
    int64_t totalScaffoldReportLines = 0;

    // TODO: Add support for blessed Link file
    // Read linkData file and store library information
    while (fgets(line, MAX_LINE_SIZE, _linkMetaFD) != NULL) {
        strcpy(aux_line, line);
        splitLinkMetaLine(aux_line, &_libInfo[_nLinkFiles]);
        serial_printf("Read lib %d: insert size %d, stddev %d, link file %s\n", _nLinkFiles,
                      _libInfo[_nLinkFiles].insertSize, _libInfo[_nLinkFiles].stdDev,
                      _libInfo[_nLinkFiles].linkFileNamePrefix);
        _nLinkFiles++;
    }
    fclose_track(_linkMetaFD);
    if (!_nLinkFiles) {
        DIE("No link files found\n");
    }

    UPC_TICK_T startTimer = UPC_TICKS_NOW();

    char *contigReportFileName = NULL;
    GZIP_FILE scaffReportFD = NULL, contigReportFD = NULL;
    {
        char scaffoldReportFileName[MAX_FILE_PATH], tmp[MAX_FILE_PATH];

        if (_scaffoldReportFilePrefix) {
            sprintf(scaffoldReportFileName, "%s" GZIP_EXT, _scaffoldReportFilePrefix);
            scaffReportFD = openCheckpoint(scaffoldReportFileName, "r");
        }

        if (_contigReportFilePrefix) {
            sprintf(tmp, "%s.txt" GZIP_EXT, _contigReportFilePrefix);
            contigReportFD = openCheckpoint(tmp, "r");
            contigReportFileName = strdup_chk(tmp);
        }
    }

    shared[BS] oNoObject * onoObjectLengths = NULL;
    UPC_ALL_ALLOC_CHK(onoObjectLengths, _nTotalObjects, sizeof(oNoObject));
    shared[BS] float *contigDepths = NULL;
    UPC_ALL_ALLOC_CHK(contigDepths, _totalContigs, sizeof(float));

    for (int64_t i = MYTHREAD; i < _totalContigs; i += THREADS) {
        contigDepths[i] = 0;
    }
    for (int64_t i = MYTHREAD; i < _nTotalObjects; i += THREADS) {
        onoObjectLengths[i].length = -1;
        onoObjectLengths[i].my_id = -1;
    }
    UPC_LOGGED_BARRIER;
    char *token, *aux, cur_type, cur_sign;

    oNoObject localONOObject;
    shared[BS] int64_t * depthHist = NULL;
    UPC_ALL_ALLOC_CHK(depthHist, MAX_HISTO_SIZE, sizeof(int64_t));

    shared[BS] int64_t * maxDepth = NULL;
    UPC_ALL_ALLOC_CHK(maxDepth, THREADS, sizeof(int64_t));

    for (int i = MYTHREAD; i < MAX_HISTO_SIZE; i += THREADS) {
        depthHist[i] = 0;
    }
    maxDepth[MYTHREAD] = 0;
    UPC_LOGGED_BARRIER;
    if (_contigReportFilePrefix) {
        assert(_nTotalObjects == _totalContigs);
        // All thread allocate onoObjectLengths shared array for contigs
        if (MYTHREAD == 0) {
            DBG("All_allocating %lld of %lld = %lld, plus locally %lld\n", (lld)_nTotalObjects,
                (lld)sizeof(oNoObject) + sizeof(float),
                (lld)_nTotalObjects * (sizeof(oNoObject) + sizeof(float)),
                (lld)_nTotalObjects * sizeof(oNoObject));
        }
        // Read the contig report file
        //int maxContigId = 0;
        char *buf = malloc_chk(1024);
        int line_num = 0;

        while (GZIP_GETS(line, MAX_LINE_SIZE, contigReportFD) != NULL) {
            strcpy(buf, line);
            token = strtok_r(buf, "\t", &aux);
            if (!token) {
                DIE("null token in file %s, line %d: %s\n", contigReportFileName, line_num, line);
            }
            int64_t contig = atol(token + 6);  // Since the string should be ContigXXXXXXXX
            CHECK_BOUNDS(contig, _totalContigs);

            token = strtok_r(NULL, "\t", &aux);
            if (!token) {
                DIE("null token in file at length field %s, line %d: %s\n", contigReportFileName, line_num, line);
            }
            int length = atoi(token);      // Extract contig's length
            if (length < 0) {
                DIE("Invalid length field '%s' %s, line %d: %s\n", token, contigReportFileName, line_num, line);
            }

            length = length + _merSize - 1; // Well, we store nKmers instead of the length in the merDepth file
            token = strtok_r(NULL, "\t", &aux);

            localONOObject.length = length;
            localONOObject.my_id = contig;

            onoObjectLengths[contig] = localONOObject;
            DBG2("Set onoObjectLengths[%lld] to %lld\n", (lld)contig, (lld)length);

            if (token != NULL) {
                float fdepth = atof(token);   // Extract contig's depth (should be stored as int)
                char strmodal[20];
                sprintf(strmodal, "%0.f", fdepth);
                int depth = (int)atof(strmodal);
                CHECK_BOUNDS(depth, MAX_HISTO_SIZE);
                // Increase appropriate entry in histogram
                UPC_ATOMIC_FADD_I64_RELAXED(NULL, &depthHist[depth], (int64_t)length);
                CHECK_BOUNDS(contig, _totalContigs);
                contigDepths[contig] = fdepth;
                _depthInfoAvailable = 1;
            }
            line_num++;
        }

        closeCheckpoint(contigReportFD);
        //if (!line_num)
        //   WARN("empty file for contigReport: %s\n", contigReportFileName);
        compute_peak_depth(depthHist, maxDepth);
        free_chk(buf);
    } // if (_contigReportFilePrefix)

    if (contigReportFileName) {
        free_chk(contigReportFileName);
    }
    shared[BS] scaf_report_t * scaffReport = NULL;
    if (_scaffoldReportFilePrefix) {
        // Allocate distributed data structure to store scafold reports
        if (MYTHREAD == 0) {
            DBG("Input is SRF file\nAll_allocating %ld, plus locally %ld\n",
                _nTotalObjects * (sizeof(scaf_report_t) + sizeof(oNoObject) + sizeof(float)) +
                _totalContigs * sizeof(float),
                _nTotalObjects * sizeof(oNoObject) + _nTotalObjects * sizeof(scaf_report_t));
        }
        UPC_ALL_ALLOC_CHK(scaffReport, _nTotalObjects, sizeof(scaf_report_t));
        for (int64_t i = MYTHREAD; i < _nTotalObjects; i += THREADS) {
            scaffReport[i].length = -1; // initialize to -1
            scaffReport[i].nEntries = 0;
            scaffReport[i].entries = NULL;
        }
        UPC_LOGGED_BARRIER;

        int64_t report_ind = 0;
        int64_t gap_id = 1;
        int64_t previous_scaff_id = UNDEFINED;
        int64_t contig_length = 0;
        float fcur_depth = 0, running_scaff_fdepth = 0.0;
        int64_t running_real_bases = 0;
        int64_t running_scaff_length = 0;
        int64_t cur_scaff_id = UNDEFINED, cur_id, cur_f1, cur_f2, cur_f3, cur_f4, cur_depth;

        scaf_entry_t *current_scaff_report = calloc_chk(MAX_SCAFF_REPORT_LENGTH, sizeof(scaf_entry_t));
        shared[] scaf_entry_t * new_ptr = NULL;
        scaf_report_t localScafReportObject;

        DBG("Done with allocation\n");
        int64_t line_num = 0;
        // Read the scaffold report file and store the info in the appropriate distributed table
        while (GZIP_GETS(line, MAX_SCAFF_LINE_SIZE, scaffReportFD) != NULL) {
            memset(&localScafReportObject, 0, sizeof(scaf_report_t));
            token = strtok_r(line, "\t", &aux);
            if (token == NULL) {
                DIE("Null token in scaffReportFD: %s\n", line);
            }
            ;
            cur_scaff_id = atol(token + 8); // Since the string should be ScaffoldXXXXXXX
            CHECK_BOUNDS(cur_scaff_id, _nTotalObjects);

            token = strtok_r(NULL, "\t", &aux);
            if (token == NULL) {
                DIE("Null token in scaffReportFD after scaffoldID: %s\n", line);
            }
            ;
            if ((*token) == 'C') {
                cur_type = CONTIG;
            } else {
                cur_type = GAP;
            }

            token = strtok_r(NULL, "\t", &aux);
            if (token == NULL) {
                DIE("Null token in scaffReportFD after cur_type: %s\n", line);
            }
            ;
            if (cur_type == CONTIG) {
                if ((*token) == '+') {
                    cur_sign = PLUS;
                } else {
                    cur_sign = MINUS;
                }
                cur_id = atol(token + 7);                 // Since column2 is +ContigXXXX or -ContigXXXX
                CHECK_BOUNDS(cur_id, _totalContigs);
                token = strtok_r(NULL, "\t", &aux);
                if (token == NULL) {
                    DIE("Null token in scaffReportFD after cur_id: %s\n", line);
                }
                ;
                cur_f1 = atol(token);
                token = strtok_r(NULL, "\t", &aux);
                if (token == NULL) {
                    DIE("Null token in scaffReportFD after cur_f1: %s\n", line);
                }
                ;
                cur_f2 = atol(token);
                token = strtok_r(NULL, "\t", &aux);
                // FIXME: Assumes that depth is always available
                if (token != NULL) {
                    fcur_depth = atof(token);
                    _depthInfoAvailable = 1;
                } else {
                    _depthInfoAvailable = 0;
                }
            } else if (cur_type == GAP) {
                cur_f1 = atol(token);
                token = strtok_r(NULL, "\t", &aux);
                if (token == NULL) {
                    DIE("Null token in scaffReportFD for gap after cur_f1: %s\n", line);
                }
                ;
                cur_f2 = atol(token);
            }

            if ((cur_scaff_id == previous_scaff_id) || (previous_scaff_id == UNDEFINED)) {
                // This is either the very first line of the scaffold report OR same line for PREVIOUS
                // scaffold
                if (cur_type == CONTIG) {
                    contig_length = cur_f2 - cur_f1 + 1;
                    if (cur_f2 > running_scaff_length) {
                        running_scaff_length = cur_f2;
                    }
                    if (_depthInfoAvailable) {
                        CHECK_BOUNDS(cur_id, _totalContigs);
                        contigDepths[cur_id] = fcur_depth;
                        running_real_bases += contig_length;
                        running_scaff_fdepth += contig_length * fcur_depth;
                        cur_depth = (int)trunc(fcur_depth);
                        char strmodal[20];
                        sprintf(strmodal, "%0.f", fcur_depth);
                        cur_depth = (int)atof(strmodal);
                        CHECK_BOUNDS(cur_depth, MAX_HISTO_SIZE);
                        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &depthHist[cur_depth], (int64_t)contig_length);
                    }
                }
                CHECK_BOUNDS(report_ind, MAX_SCAFF_REPORT_LENGTH);
                current_scaff_report[report_ind].type = cur_type;
                current_scaff_report[report_ind].f1 = cur_f1;
                current_scaff_report[report_ind].f2 = cur_f2;
                if (cur_type == CONTIG) {
                    current_scaff_report[report_ind].entry_id = cur_id;
                    if (_depthInfoAvailable) {
                        current_scaff_report[report_ind].fDepth = fcur_depth;
                    }
                    current_scaff_report[report_ind].sign = cur_sign;
                } else if (cur_type == GAP) {
                    current_scaff_report[report_ind].entry_id = gap_id;
                    gap_id++;
                }
                report_ind++;
                previous_scaff_id = cur_scaff_id;
            } else {
                // This is a line involving a NEW scaff report, store previous one in distributed data
                // structure
                assert(new_ptr == NULL);
                UPC_ALLOC_CHK0(new_ptr, report_ind * sizeof(scaf_entry_t)); // entries

                upc_memput(new_ptr, current_scaff_report, report_ind * sizeof(scaf_entry_t));
                UPC_ATOMIC_FADD_I64_RELAXED(NULL, &_total_scaff_entries, report_ind);
                UPC_ATOMIC_FADD_I64_RELAXED(NULL, &_total_scaffs, 1);

                localScafReportObject.entries = new_ptr;
                localScafReportObject.nEntries = report_ind;
                localScafReportObject.realBases = running_real_bases;
                new_ptr = NULL; // avoid any memory leaks
                if (_depthInfoAvailable) {
                    localScafReportObject.fdepth = running_scaff_fdepth;
                }
                localScafReportObject.length = running_scaff_length;
                CHECK_BOUNDS(previous_scaff_id, _nTotalObjects);
                assert(scaffReport[previous_scaff_id].nEntries == 0 && scaffReport[previous_scaff_id].entries == NULL); // this should be the first assignment
                scaffReport[previous_scaff_id] = localScafReportObject;

                localONOObject.my_id = previous_scaff_id;
                localONOObject.length = running_scaff_length;

                onoObjectLengths[previous_scaff_id] = localONOObject;
                DBG2("Set onoObjectLengths[%lld] to %lld\n", (lld)previous_scaff_id, (lld)running_scaff_length);

                // Initialize again a current scaff report
                running_real_bases = 0;
                int64_t running_scaff_depth = 0;
                running_scaff_fdepth = 0.0;
                running_scaff_length = 0;
                report_ind = 0;
                gap_id = 1;

                if (cur_type == CONTIG) {
                    contig_length = cur_f2 - cur_f1 + 1;
                    if (cur_f2 > running_scaff_length) {
                        running_scaff_length = cur_f2;
                    }

                    if (_depthInfoAvailable) {
                        CHECK_BOUNDS(cur_id, _totalContigs);
                        contigDepths[cur_id] = fcur_depth;
                        running_real_bases += contig_length;
                        running_scaff_fdepth += contig_length * fcur_depth;
                        cur_depth = (int)trunc(fcur_depth);
                        CHECK_BOUNDS(cur_depth, MAX_HISTO_SIZE);
                        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &depthHist[cur_depth], (int64_t)contig_length);
                    }
                }

                CHECK_BOUNDS(report_ind, MAX_SCAFF_REPORT_LENGTH);
                current_scaff_report[report_ind].type = cur_type;
                current_scaff_report[report_ind].f1 = cur_f1;
                current_scaff_report[report_ind].f2 = cur_f2;
                if (cur_type == CONTIG) {
                    current_scaff_report[report_ind].entry_id = cur_id;
                    if (_depthInfoAvailable) {
                        current_scaff_report[report_ind].fDepth = fcur_depth;
                    }

                    current_scaff_report[report_ind].sign = cur_sign;
                } else if (cur_type == GAP) {
                    current_scaff_report[report_ind].entry_id = gap_id;
                    gap_id++;
                }
                report_ind++;
                previous_scaff_id = cur_scaff_id;
            }
            line_num++;
        }
        closeCheckpoint(scaffReportFD);
        //if (!line_num)
        //   WARN("empty file for scaffReport %s\n", scaffoldReportFileName);
        totalScaffoldReportLines = reduce_long(line_num, UPC_ADD, ALL_DEST);

        DBG("Finished reading scaffoldReportFile. report_ind: %lld\n", (lld)report_ind);

        // Check if the last scaffold report has been stored in the distributed scaffold report table
        if (report_ind != 0) {
            assert(new_ptr == NULL);
            LOGF("Allocating last %lld scaff_entries %0.3f MB local shared\n", (lld)report_ind, 1.0 / ONE_MB * report_ind * sizeof(scaf_entry_t));
            UPC_ALLOC_CHK0(new_ptr, report_ind * sizeof(scaf_entry_t)); // entries

            upc_memput(new_ptr, current_scaff_report, report_ind * sizeof(scaf_entry_t));
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &_total_scaff_entries, report_ind);
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &_total_scaffs, 1);

            localScafReportObject.entries = new_ptr;
            localScafReportObject.nEntries = report_ind;
            localScafReportObject.realBases = running_real_bases;
            new_ptr = NULL; // avoid any memory leaks
            if (_depthInfoAvailable) {
                localScafReportObject.fdepth = running_scaff_fdepth;
            }
            localScafReportObject.length = running_scaff_length;
            CHECK_BOUNDS(previous_scaff_id, _nTotalObjects);
            assert(scaffReport[previous_scaff_id].nEntries == 0 && scaffReport[previous_scaff_id].entries == NULL); // this should be the first assignment
            scaffReport[previous_scaff_id] = localScafReportObject;
            localONOObject.my_id = previous_scaff_id;
            localONOObject.length = running_scaff_length;
            onoObjectLengths[previous_scaff_id] = localONOObject;
            DBG("Set onoObjectLengths[%lld] to %lld\n", (lld)previous_scaff_id, (lld)running_scaff_length);
        }

        // Thread 0 finds modal depth and propagates the info through shared array
        UPC_LOGGED_BARRIER;
        CHECK_BOUNDS(_total_scaffs, _nTotalObjects + 1);

        DBG("Done with SRF file: %lld Scaffolds read\n", (lld)(previous_scaff_id < 0 ? 0 : previous_scaff_id));
        /* Ask Jarrod about derived metric */
        /* Will compute this derived metric on the fly so no need to store it */
        /* Metric is contigDepths{} */
        compute_peak_depth(depthHist, maxDepth);

        free_chk(current_scaff_report);
    } // if (_scaffoldReportFilePrefix)

    //  Build up contig linkage table using linkFiles
    UPC_LOGGED_BARRIER;

    UPC_ALL_FREE_CHK(depthHist);
    UPC_ALL_FREE_CHK(maxDepth);

    double readInputTime = ELAPSED_SECS(startTimer);

    startTimer = UPC_TICKS_NOW();

    oNolink_hash_table_t *local_hashtable = NULL;
    oNoDatum_t *datum_heap = NULL;
    oNolinkList_t *linkList_heap = NULL;
    splints_stack_entry *splints = NULL;
    spans_stack_entry *spans = NULL;
    int64_t myProvision = 0, nSplints = 0, nSpans = 0;
    oNoLink_heap_t link_heap;
    oNolink_t *local_buffs = NULL;
    int64_t *local_index = NULL;

    if (_totalLinks) {
        // Allocate local buffers and local shared heaps required for the links hashtable
        int64_t total_entries = _totalLinks;
        // Over-allocating in heaps to avoid OOM errors in case of large load imbalance
        total_entries = (int64_t)total_entries * EXP_FACTOR;

        allocate_oNolocal_buffs(&local_buffs, &local_index);
        create_oNolink_heaps(total_entries, &link_heap);
        UPC_LOGGED_BARRIER;

        int64_t mySpans = 0;
        int64_t mySplints = 0;

        char *linkFileNames = calloc_chk(MAX_FILE_PATH * (MAX_LIBRARIES + 1), 1);
        char *curLinkFile = linkFileNames + (MAX_FILE_PATH * MAX_LIBRARIES);

        char cur_typeS[30], cur_key[300];
        oNolink_t new_entry;

        int num_lines = 0;
        for (int i = 0; i < _nLinkFiles; i++) {
            sprintf(curLinkFile, "%s" GZIP_EXT, _libInfo[i].linkFileNamePrefix);
            DBG("Reading linkFile: %s\n", curLinkFile);
            GZIP_FILE curLinkFileFD = openCheckpoint(curLinkFile, "r");
            while (GZIP_GETS(line, MAX_LINE_SIZE, curLinkFileFD) != NULL) {
                // Parse the current link and store it
                token = strtok_r(line, "\t", &aux);
                assert(token != NULL);
                strcpy(cur_typeS, token);
                token = strtok_r(NULL, "\t", &aux);
                assert(token != NULL);
                strcpy(cur_key, token);

                if (cur_typeS[2] == 'A') {         // SP'A'N
                    mySpans++;
                    new_entry.link_type = SPAN;
                } else if (cur_typeS[2] == 'L') {  // SP'L'INT
                    mySplints++;
                    new_entry.link_type = SPLINT;
                } else {
                    printf("Thread %d: All links should be either spLint or spAn: %s\n", MYTHREAD, line);
                    assert(0);
                }

                int64_t hashval = hashstr(THREADS, cur_key);
                new_entry.lib_id = i;

                token = strtok_r(NULL, "\t", &aux);
                assert(token != NULL);
                strcpy(entries1, token);
                token = strtok_r(NULL, "\t", &aux);
                assert(token != NULL);
                strcpy(entries2, token);

                if (new_entry.link_type == SPAN) {
                    token = strtok_r(NULL, "\t", &aux);
                    assert(token != NULL);
                    strcpy(entries3, token);
                }

                if (new_entry.link_type == SPAN) {
                    // Link is of the form: SPAN	Scaffold10809495.3<=>Scaffold7516640.5	0|1	-42	0.87
                    // Parse first the key
                    token = strtok_r(cur_key, "<", &aux);
                    assert(token != NULL);
                    if (strcmp(token + (strlen(token) - 1), "3") == 0) {
                        new_entry.end1_s = 3;
                    } else {
                        new_entry.end1_s = 5;
                    }
                    token[strlen(token) - 2] = '\0';
                    if ((*token) == 'S') {
                        new_entry.end1_id = atol(token + 8);
                    } else {
                        new_entry.end1_id = atol(token + 6);
                    }
                    token = strtok_r(NULL, "<", &aux);
                    assert(token != NULL);
                    token += 2; // to skip "=>"
                    if (strcmp(token + (strlen(token) - 1), "3") == 0) {
                        new_entry.end2_s = 3;
                    } else {
                        new_entry.end2_s = 5;
                    }
                    token[strlen(token) - 2] = '\0';
                    if ((*token) == 'S') {
                        new_entry.end2_id = atol(token + 8);
                    } else {
                        new_entry.end2_id = atol(token + 6);
                    }

                    // Parse then the info
                    token = strtok_r(entries1, "|", &aux);
                    assert(token != NULL);
                    new_entry.f1 = atol(token);
                    token = strtok_r(NULL, "|", &aux);
                    assert(token != NULL);
                    new_entry.f2 = atol(token);
                    new_entry.f3 = atol(entries2);
                    new_entry.f5 = atol(entries3);
                } else if (new_entry.link_type == SPLINT) {
                    // Link is of the form: SPLINT	Contig21203848.5<=>Contig33389213.3	19|0|19	-46
                    // Parse first the key
                    token = strtok_r(cur_key, "<", &aux);
                    assert(token != NULL);
                    if (strcmp(token + (strlen(token) - 1), "3") == 0) {
                        new_entry.end1_s = 3;
                    } else {
                        new_entry.end1_s = 5;
                    }

                    token[strlen(token) - 2] = '\0';
                    new_entry.end1_id = atol(token + 6);
                    token = strtok_r(NULL, "<", &aux);
                    assert(token != NULL);
                    token += 2; // to skip "=>"
                    if (strcmp(token + (strlen(token) - 1), "3") == 0) {
                        new_entry.end2_s = 3;
                    } else {
                        new_entry.end2_s = 5;
                    }

                    token[strlen(token) - 2] = '\0';
                    new_entry.end2_id = atol(token + 6);

                    /// Parse then the info
                    token = strtok_r(entries1, "|", &aux);
                    assert(token != NULL);
                    new_entry.f1 = atol(token);
                    token = strtok_r(NULL, "|", &aux);
                    assert(token != NULL);
                    new_entry.f2 = atol(token);
                    token = strtok_r(NULL, "|", &aux);
                    assert(token != NULL);
                    new_entry.f3 = atol(token);
                    new_entry.f4 = atol(entries2);
                }

#ifdef DEPTH_LINK_RESTRICT
                // Store link to shared heaps
                float depth_1;
                float depth_2;
                float min_depth_link;
                float max_depth_link;

                if (new_entry.link_type == SPAN) {
                    depth_1 = contigDepths[new_entry.end1_id];
                    depth_2 = contigDepths[new_entry.end2_id];

                    min_depth_link = depth_1;
                    max_depth_link = depth_2;

                    if (depth_2 < depth_1) {
                        min_depth_link = depth_2;
                        max_depth_link = depth_1;
                    }

                    if (max_depth_link < min_depth_link * 1.5) {
                        add_oNolink_to_shared_heaps(&new_entry, hashval, local_index, local_buffs, link_heap, total_entries);
                    }
                } else if (new_entry.link_type == SPLINT) {
                    add_oNolink_to_shared_heaps(&new_entry, hashval, local_index, local_buffs, link_heap, total_entries);
                }
#endif
#ifndef DEPTH_LINK_RESTRICT
                add_oNolink_to_shared_heaps(&new_entry, hashval, local_index, local_buffs, link_heap, total_entries);
#endif

                num_lines++;
            }
            closeCheckpoint(curLinkFileFD);
        }

        free_chk(linkFileNames);

        // Adds remaining links to the shared heaps
        add_rest_oNolinks_to_shared_heaps(local_index, local_buffs, link_heap, total_entries);

        // Wait until all heaps have been fixed
        UPC_LOGGED_BARRIER;

        double storingLinksTime = ELAPSED_SECS(startTimer);

        // Use another thread id, eg 480 to measure more accurately communication times
        if (MYTHREAD == THREADS / 2) {
            printf("Reading/processing inputs time is: %f seconds\n", readInputTime);
            printf("Storing links time is: %f seconds\n", storingLinksTime);
            fflush(stdout);
        }

        DBG("Added links to shared heaps\n");

        UPC_LOGGED_BARRIER;

        startTimer = UPC_TICKS_NOW();

        // Create local hashtable by iterating on local heap and storing link_data to local_hashtable
        int64_t entries_in_my_stack = link_heap.heap_indices[MYTHREAD];
        oNolink_t *my_stack = (oNolink_t *)link_heap.link_ptr[MYTHREAD];

        if (entries_in_my_stack) {
            assert(local_hashtable == NULL);
            assert(datum_heap == NULL);
            assert(linkList_heap == NULL);
            local_hashtable = create_oNolink_hash_table(entries_in_my_stack + 1);
            int64_t datum_heap_pos = 0;
            int64_t link_heap_pos = 0;
            create_oNolocal_heaps(entries_in_my_stack, &datum_heap, &linkList_heap);
            for (int64_t i = 0; i < entries_in_my_stack; i++) {
                add_oNolink_data(local_hashtable, &my_stack[i], &datum_heap_pos, datum_heap, &link_heap_pos, linkList_heap);
            }
        }

        DBG("Entries in my stack: %lld\n", (lld)entries_in_my_stack);

        double local_linktable_time = ELAPSED_SECS(startTimer);

        DBG("My splints: %lld, My spans: %lld\n", (lld)mySplints, (lld)mySpans);
        /* Allocate local stacks for splints and spans */
        nSpans = entries_in_my_stack + 1;
        nSplints = nSpans;
        splints = calloc_chk(nSplints, sizeof(splints_stack_entry));
        spans = calloc_chk(nSpans, sizeof(spans_stack_entry));
        free_oNolocal_buffs(&local_buffs, &local_index);
        LOGF("Done with totalLinks");
    } // if (_totalLinks)

    free_chk(lineBuffers);

    // Allocate a single local buffer for endTies(). All of them will go to processor 0
    tie_t *endTies_local = calloc_chk(MAX_TIES_LOCAL, sizeof(tie_t));

    // Now iterate over the hash table and build ties between contigs consolidating splint/span links
    startTimer = UPC_TICKS_NOW();
    double build_ties_communication_time = 0.0;

    // from here on we diverge according to the _pairThreshold

    int64_t total_scaffs = _total_scaffs; // cache the global variable
    for (int pairThreshold = _minPairThreshold; pairThreshold <= _maxPairThreshold; pairThreshold++) {
        int minActiveThread = MIN_ACTIVE_THREAD(pairThreshold);

        LOGF("Starting pairThreshold: %d (minActive %d)\n", pairThreshold, minActiveThread);

        int64_t localTiesPtr = 0;
        int64_t totTies = 0;

        if (local_hashtable) {
            for (int64_t i = 0; i < local_hashtable->size; i++) {
                oNolinkList_t *cur_bucket = local_hashtable->table[i];
                while (cur_bucket != NULL) {
                    //totTies++;

                    // Initialize splints and spans local stacks
                    int64_t splint_stack_ptr = 0;
                    int64_t span_stack_ptr = 0;

                    int64_t maxLikelihoodSplint = UNDEFINED;
                    int64_t maxSplintCount = 0;
                    int64_t minUncertaintySpan = UNDEFINED;
                    int64_t minSpanUncertainty = 100000;

                    // TODO: Add support for Blessed link files

                    // Process the data in the the current link
                    int cur_e1 = cur_bucket->end1_id;
                    int cur_e2 = cur_bucket->end2_id;
                    int cur_e1Ending = cur_bucket->end1_s;
                    int cur_e2Ending = cur_bucket->end2_s;
                    oNoDatum_t *cur_data = cur_bucket->data;
                    char thereIsASplint = 0;
                    double abs_diff = 0.0;
                    while (cur_data != NULL) {
                        cur_type = cur_data->link_type;
                        // Different process of datum depending on its type (SPLINT/SPAN)
                        if (cur_type == SPLINT) {
                            int64_t nUsedSplints = cur_data->f1;
                            int64_t nAnomalousSplints = cur_data->f2;
                            int gapEstimate = cur_data->f4;
                            thereIsASplint = 1;
                            if (nUsedSplints >= pairThreshold) {
                                CHECK_BOUNDS(splint_stack_ptr, nSplints);
                                assert(splint_stack_ptr >= 0);
                                splints[splint_stack_ptr].nUsedSplints = nUsedSplints;
                                splints[splint_stack_ptr].gapEstimate = gapEstimate;
                                splints[splint_stack_ptr].linkFileID = cur_data->lib_id;
                                if (maxLikelihoodSplint == UNDEFINED) {
                                    maxLikelihoodSplint = splint_stack_ptr;
                                    maxSplintCount = nUsedSplints;
                                }
                                if (nUsedSplints > maxSplintCount) {
                                    maxLikelihoodSplint = splint_stack_ptr;
                                    maxSplintCount = nUsedSplints;
                                }
                                splint_stack_ptr++;
                            }
                        } else if (cur_type == SPAN) {
                            int64_t nAnomalousSpans = cur_data->f1;
                            int64_t nUsedSpans = cur_data->f2;
                            int gapEstimate = cur_data->f3;
                            int gapUncertainty = cur_data->f5;
                            if (nUsedSpans >= pairThreshold) {
                                CHECK_BOUNDS(span_stack_ptr, nSpans);
                                spans[span_stack_ptr].nUsedSpans = nUsedSpans;
                                spans[span_stack_ptr].gapEstimate = gapEstimate;
                                spans[span_stack_ptr].gapUncertainty = gapUncertainty;
                                spans[span_stack_ptr].linkFileID = cur_data->lib_id;
                                if (minUncertaintySpan == UNDEFINED) {
                                    minUncertaintySpan = span_stack_ptr;
                                    minSpanUncertainty = gapUncertainty;
                                }
                                if (gapUncertainty < minSpanUncertainty) {
                                    minUncertaintySpan = span_stack_ptr;
                                    minSpanUncertainty = gapUncertainty;
                                }
                                span_stack_ptr++;
                            }
                        }
                        cur_data = cur_data->next;
                    }
                    // END: Process the data in the the current link
                    //assert(nSplints >= splint_stack_ptr);
                    //assert(nSpans >= span_stack_ptr);

                    int minimumGapSize = -(_merSize - 2);
                    int nSplintGap = UNDEFINED;
                    int splintGapEstimate = UNDEFINED;
                    int maxSplintError = 2;
                    int splintAnomaly = 0;
                    int n0, g0, u0, n, g, u;

                    if (maxLikelihoodSplint != UNDEFINED) {
                        CHECK_BOUNDS(maxLikelihoodSplint, nSplints);
                        n0 = splints[maxLikelihoodSplint].nUsedSplints;
                        g0 = splints[maxLikelihoodSplint].gapEstimate;
                        if (g0 < minimumGapSize) {
                            splintGapEstimate = minimumGapSize;
                        } else {
                            splintGapEstimate = g0;
                        }
                        nSplintGap = n0;
                        CHECK_BOUNDS(splint_stack_ptr, nSplints);
                        for (int64_t aux_ptr = 0; aux_ptr < splint_stack_ptr; aux_ptr++) {
                            n = splints[aux_ptr].nUsedSplints;
                            g = splints[aux_ptr].gapEstimate;
                            int splintGapDiff = abs(g - splintGapEstimate);
                            if (splintGapDiff > maxSplintError) {
                                splintAnomaly = 1;
                            }
                        }
                    }

                    int nSpanGap = UNDEFINED;
                    int spanGapEstimate = UNDEFINED;
                    int spanGapUncertainty = UNDEFINED;
                    int maxSpanZ = 3;

                    int spanAnomaly = 0;
                    if (minUncertaintySpan != UNDEFINED) {
                        CHECK_BOUNDS(minUncertaintySpan, nSpans);
                        n0 = spans[minUncertaintySpan].nUsedSpans;
                        g0 = spans[minUncertaintySpan].gapEstimate;
                        u0 = spans[minUncertaintySpan].gapUncertainty;

                        if (g0 < minimumGapSize) {
                            spanGapEstimate = minimumGapSize;
                        } else {
                            spanGapEstimate = g0;
                        }
                        if (u0 < 1) {
                            spanGapUncertainty = 1;
                        } else {
                            spanGapUncertainty = u0;
                        }
                        nSpanGap = n0;
                        CHECK_BOUNDS(span_stack_ptr, nSpans);
                        for (int64_t aux_ptr = 0; aux_ptr < span_stack_ptr; aux_ptr++) {
                            n = spans[aux_ptr].nUsedSpans;
                            g = spans[aux_ptr].gapEstimate;
                            u = spans[aux_ptr].gapUncertainty;
                            double spanGapZ = abs(g - spanGapEstimate) / (1.0 * (u + 1));
                            if (spanGapZ > (maxSpanZ * 1.0)) {
                                spanAnomaly = 1;
                            }
                        }
                    }

                    int gapEstimate = UNDEFINED;
                    int gapUncertainty = UNDEFINED;
                    int nGapLinks = UNDEFINED;
                    int valid_link = 1;

                    if (splintGapEstimate != UNDEFINED) {
                        gapEstimate = splintGapEstimate;
                        gapUncertainty = 1;
                        nGapLinks = nSplintGap;
                    } else if (spanGapEstimate != UNDEFINED) {
                        gapEstimate = spanGapEstimate;
                        gapUncertainty = spanGapUncertainty;
                        nGapLinks = nSpanGap;
                    } else {
                        valid_link = 0;
                    }

                    if (valid_link == 1) {
                        if ((spanGapEstimate != UNDEFINED) && (splintGapEstimate != UNDEFINED)) {
                            double gapZ = (1.0 * (splintGapEstimate - spanGapEstimate)) / (1.0 * spanGapUncertainty);
                            if (fabs(gapZ) > maxSpanZ) {
                                // TODO: add warning message
                            }
                        }

                        CHECK_BOUNDS(localTiesPtr, MAX_TIES_LOCAL);
                        endTies_local[localTiesPtr].end1_id = cur_e1;
                        endTies_local[localTiesPtr].end2_id = cur_e2;
                        endTies_local[localTiesPtr].ending1 = cur_e1Ending;
                        endTies_local[localTiesPtr].ending2 = cur_e2Ending;
                        endTies_local[localTiesPtr].nGapLinks = nGapLinks;
                        endTies_local[localTiesPtr].gapEstimate = gapEstimate;
                        endTies_local[localTiesPtr].gapUncertainty = gapUncertainty;
                        endTies_local[localTiesPtr].splintFlag = thereIsASplint;
                        localTiesPtr++;

                        if (localTiesPtr == MAX_TIES_LOCAL) {
                            UPC_TICK_T commStartTimer = UPC_TICKS_NOW();

                            appendHeap(_pp[pairThreshold].tieSharedHeap[MYTHREAD], endTies_local, MAX_TIES_LOCAL * sizeof(tie_t));

                            localTiesPtr = 0;

                            build_ties_communication_time += ELAPSED_SECS(commStartTimer);
                        }
                    }
                    cur_bucket = cur_bucket->next;
                }
            } // end for local_hashtable
        }

        // Store remaining ties to remote heap
        if (localTiesPtr != 0) {
            UPC_TICK_T commStartTimer = UPC_TICKS_NOW();

            appendHeap(_pp[pairThreshold].tieSharedHeap[MYTHREAD], endTies_local, localTiesPtr * sizeof(tie_t));
            localTiesPtr = 0;

            build_ties_communication_time += ELAPSED_SECS(commStartTimer);
        }

        double build_ties_time = ELAPSED_SECS(startTimer);
        int64_t objectsPerActiveThread = (_nTotalObjects + _nActiveThreads) / _nActiveThreads;
        int64_t scaffoldsPerActiveThread = _scaffoldReportFilePrefix ? ((total_scaffs + _nActiveThreads) / _nActiveThreads) : 0;
        int64_t contigsPerActiveThread = _depthInfoAvailable ? ((_totalContigs + _nActiveThreads) / _nActiveThreads) : 0;

        DBG("threshold %d, %lld ties, %lld totalContigs, "
            "%lld totalScaffolds, %lld totalObjects, %lld objectsPerActiveThread, %lld contigsPAT, %lld scaffoldPAT, %lld _nTotalObjects\n", pairThreshold,
            (lld)(_pp[pairThreshold].tieSharedHeap == NULL ? -1 : _pp[pairThreshold].tieSharedHeap[MYTHREAD].position / sizeof(tie_t)),
            (lld)_totalContigs, (lld)total_scaffs, (lld)_nTotalObjects, (lld)objectsPerActiveThread, (lld)contigsPerActiveThread, (lld)scaffoldsPerActiveThread, (lld)_nTotalObjects);

        startTimer = UPC_TICKS_NOW();

        UPC_LOGGED_BARRIER;

        // aggregate data from local cyclic memory to local continuous memory
        for (int64_t i = MYTHREAD; i < _nTotalObjects; i += THREADS) {
            int targetThread = i % _nActiveThreads + minActiveThread;
            assert(targetThread < THREADS);
            sharedoNoObjectPtr mySharingoNoObjectPtrs = NULL;
            mySharingoNoObjectPtrs = _pp[pairThreshold].sharingoNoObjectPtrs[targetThread];
            assert(mySharingoNoObjectPtrs != NULL);
            int new_i = i / _nActiveThreads;
            CHECK_BOUNDS(new_i, objectsPerActiveThread);
            if (onoObjectLengths[i].my_id < 0) {
                WARN("uninitialized value for onoObjectLengths[%ld] within _nTotalObjects %lld\n", i, (lld)_nTotalObjects);
            }
            CHECK_BOUNDS(onoObjectLengths[i].my_id, _nTotalObjects);
            assert(upc_threadof(&onoObjectLengths[i]) == MYTHREAD);
            mySharingoNoObjectPtrs[new_i] = onoObjectLengths[i];
        }
        DBG("Aggregated to mySharingoNoObjectPtrs\n");

        if (_depthInfoAvailable) {
            for (int64_t i = MYTHREAD; i < _totalContigs; i += THREADS) {
                int targetThread = i % _nActiveThreads + minActiveThread;
                assert(targetThread < THREADS);
                SharedFloatPtr mySharingDepths = NULL;
                mySharingDepths = _pp[pairThreshold].sharingDepths[targetThread];
                assert(mySharingDepths != NULL);
                int new_i = i / _nActiveThreads;
                CHECK_BOUNDS(new_i, contigsPerActiveThread);
                mySharingDepths[new_i] = contigDepths[i];
            }
            DBG("Aggregated to mySharingDepths\n");
        }

        if (_scaffoldReportFilePrefix) {
            assert(total_scaffs <= _nTotalObjects);
            int64_t localBound = total_scaffs;
            for (int64_t i = MYTHREAD; i < localBound; i += THREADS) {
                int targetThread = i % _nActiveThreads + minActiveThread;
                assert(targetThread < THREADS);
                sharedScaffPtr mySharingScaffPtrs = NULL;
                mySharingScaffPtrs = _pp[pairThreshold].sharingScaffPtrs[targetThread];
                assert(mySharingScaffPtrs != NULL);
                assert(scaffReport[i].length > 0);                            // scaffReport is initialized
                int64_t new_i = i / _nActiveThreads;
                assert(mySharingScaffPtrs[new_i].length == -1);               // sharingScaffPtrs is unintialized
                assert(new_i <= _pp[pairThreshold].numObjects[targetThread]); // new_i is within memory bounds
                CHECK_BOUNDS(new_i, scaffoldsPerActiveThread);
                mySharingScaffPtrs[new_i] = scaffReport[i];
            }
            DBG("Aggregated to mySharingScaffPtrs\n");
        }


        DBG("Aggregated memory\n");
        UPC_LOGGED_BARRIER;

        double propagating_lengths_time = ELAPSED_SECS(startTimer);

        // allocate sharedScaffolds now
        if (_nTotalObjects > totalScaffoldReportLines) {
            totalScaffoldReportLines = _nTotalObjects;
        }
        int64_t lineSize = MAX_SCAFF_LINE_SIZE + 1;
        // Over-estimation of output size for each thread
        int64_t outputSize = ((totalScaffoldReportLines * 3 / 2 + THREADS - 1) / THREADS + 128) * lineSize;
        LOGF("Provisioning for pair %d: %0.3f MB local shared memory for output\n", pairThreshold, 1.0 / ONE_MB * outputSize * sizeof(char));
        initHeap(&(_pp[pairThreshold].sharedScaffolds[MYTHREAD]), outputSize * sizeof(char));
        DBG2("Allocated sharedScaffolds: outputSize=%lld\n", (lld)outputSize);

        DBG("Done with pair threshold %d\n", pairThreshold);
    } // end pair threshold loop

    DBG("Freeing parallel memory\n");
    UPC_TIMED_BARRIER;
    if (splints) {
        free_chk(splints);
    }
    if (spans) {
        free_chk(spans);
    }

    if (endTies_local) {
        free_chk(endTies_local);
    }

    if (_totalLinks) {
        destroy_oNolink_heaps(&link_heap);
    }

    if (scaffReport) {
        /* Note: entries within scaffReport are needed in the serial section, but not the global array */
        UPC_ALL_FREE_CHK(scaffReport);
    }

    if (onoObjectLengths) {
        UPC_ALL_FREE_CHK(onoObjectLengths);
    }
    if (contigDepths) {
        UPC_ALL_FREE_CHK(contigDepths);
    }
    if (datum_heap) {
        free_oNolocal_heaps(&datum_heap, &linkList_heap);
    }
    if (local_hashtable) {
        free_oNohash_table(&local_hashtable);
    }
}   // end run_parallel

int oNo_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    _peakDepth = 0;
    _depthInfoAvailable = 0;
    _merSize = 0;
    _totalLinks = 0;
    _totalContigs = 0;
    _nTotalObjects = 0;
    _nLinkFiles = 0;
    _scaffoldReportFilePrefix = NULL;
    _contigReportFilePrefix = NULL;
    _outputPrefix = NULL;
    _linkMetaFD = NULL;
    _nActiveThreads = 0;
    _minPairThreshold = 1;
    _maxPairThreshold = 11;
    _pairBlock = 0;

    if (!MYTHREAD) {
        _total_scaff_entries = 0;
        _total_scaffs = 0;
    }
    UPC_LOGGED_BARRIER;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "m:l:p:s:c:L:n:C:o:B:N:A:");
    print_args(optList, __func__);
    char *pairThresholdStr = NULL;

    int coresPerNode = (_sv != NULL) ? MYSV.cores_per_node : 1;

    // Process the input arguments
    // TODO: Add support for blessed Link file
    while (optList != NULL) {
        char filename[MAX_FILE_PATH];
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'l':
            strcpy(filename, thisOpt->argument);
            _linkMetaFD = fopen_rank_path(filename, "r", -1);
            break;
        case 'm':
            _merSize = atoi(thisOpt->argument);
            break;
        case 'L':
            _totalLinks = atol(thisOpt->argument);
            break;
        case 'n':
            _nTotalObjects = atol(thisOpt->argument);
            break;
        case 'C':
            _totalContigs = atol(thisOpt->argument);
            break;
        case 's':
            _scaffoldReportFilePrefix = thisOpt->argument;
            break;
        case 'c':
            _contigReportFilePrefix = thisOpt->argument;
            break;
        case 'p':
            pairThresholdStr = thisOpt->argument;
            break;
        case 'o':
            _outputPrefix = thisOpt->argument;
            break;
        case 'B':
            _base_dir = thisOpt->argument;
            break;
        case 'A':
            _nActiveThreads = atoi(thisOpt->argument);
            break;
        case 'N':
            coresPerNode = atoi(thisOpt->argument);
            SET_CORES_PER_NODE(coresPerNode);
            break;
        default:
            break;
        }
    }

    //if (_totalLinks == 1) {
    //_totalLinks = 0;
    if (!MYTHREAD && !_totalLinks) {
        LOGF("No links found for oNo\n");
    }
    //}
    if (_scaffoldReportFilePrefix && _contigReportFilePrefix) {
        SDIE("Please one and only one of either: -c merDepthReport or -s scaffoldReportFile\n");
    }
    if (_scaffoldReportFilePrefix) {
        if (!_nTotalObjects) {
            SDIE("For scaffold report file must specify total objects\n");
        }
        serial_printf("Processing scaffold report file with %lld objects\n", (lld)_nTotalObjects);
    } else {
        if (_nTotalObjects) {
            SDIE("For merDepth processing, must not specify total objects\n");
        }
        serial_printf("Processing merDepth file with %lld contigs\n", (lld)_totalContigs);
        _nTotalObjects = _totalContigs;
    }
    if (!_totalContigs) {
        SDIE("Must specify total contigs\n");
    }
    if (!_merSize) {
        SDIE("Please specify merSize: -m merSize\n");
    }
    if (pairThresholdStr) {
        char *maxPairStr = strchr(pairThresholdStr, ',');
        if (!maxPairStr) {
            _maxPairThreshold = atoi(pairThresholdStr);
        } else {
            _maxPairThreshold = atoi(maxPairStr + 1);
        }
        maxPairStr[0] = 0;
        _minPairThreshold = atoi(pairThresholdStr);
        if (_minPairThreshold > _maxPairThreshold) {
            SDIE("Min pair threshold %d must be less than max pair threshold %d\n",
                 _minPairThreshold, _maxPairThreshold);
        }
    }
    int rangePairThresholds = _maxPairThreshold - _minPairThreshold + 1;

    int maxCores = THREADS;
    int nodes = THREADS / coresPerNode;
    if (rangePairThresholds > THREADS) {
        _maxPairThreshold = THREADS - _minPairThreshold + 1;
        SWARN("rangePairsThresholds=%d are greater than the number of therads=%d, reducing maxPairThreshold to %d\n",
              rangePairThresholds, THREADS, _maxPairThreshold);
        rangePairThresholds = _maxPairThreshold - _minPairThreshold + 1;
        _pairBlock = 1;
        _nActiveThreads = 1;
    } else if (nodes >= rangePairThresholds) {
        // we have enough nodes to have one pair threshold calculation per node
        _pairBlock = coresPerNode * (int)(nodes / rangePairThresholds);
        if (!_nActiveThreads) {
            // don't use offnode threads
            _nActiveThreads = coresPerNode;
        }
    } else {
        // we need more than one pair per node
        _pairBlock = coresPerNode / (int)INT_CEIL(rangePairThresholds, nodes);
        if (!_nActiveThreads) {
            // use all available threads
            _nActiveThreads = _pairBlock;
        }
    }
    if (_nActiveThreads % 2 && _nActiveThreads > 3) {
        _nActiveThreads--;
    }

    if (_nActiveThreads > _pairBlock) {
        SLOG("NOTE: Too many active threads to distribute fully: %d > %d. Reducing active threads\n", _nActiveThreads, _pairBlock);
        _nActiveThreads = _pairBlock;
    }
    if (_nActiveThreads > coresPerNode) {
        serial_printf("Warning: there are more active threads (%d) per serial section than cores "
                      "per node (%d); performance could suffer\n", _nActiveThreads, coresPerNode);
    }

    if (!MYTHREAD) {
        serial_printf("Configuration:\n");
        serial_printf("    %d cores per node on %d nodes\n", coresPerNode, nodes);
        serial_printf("    %d pair thresholds (%d to %d)\n",
                      rangePairThresholds, _minPairThreshold, _maxPairThreshold);
        serial_printf("    %d active threads per serial section, pair block %d\n",
                      _nActiveThreads, _pairBlock);
        serial_printf("    serial sections running on threads: ");
        int i;
        for (i = _minPairThreshold; i <= _maxPairThreshold; i++) {
            serial_printf("%d, ", MIN_ACTIVE_THREAD(i));
        }
        serial_printf("%d\n", MIN_ACTIVE_THREAD(i));
    }

    LOGF("All allocating %lld entries _pp, for ngaps, n50s and nScaffolds: %0.3f MB globally %0.3f MB per thread and %0.3f MB private\n", (lld)_maxPairThreshold, 1.0 / ONE_MB * _maxPairThreshold * (sizeof(int) + sizeof(int64_t) * 2), 1.0 / ONE_MB * _maxPairThreshold * (sizeof(int) + sizeof(int64_t) * 2) / THREADS, 1.0 / ONE_MB * sizeof(per_pair_threshold_data_t) * _maxPairThreshold);
    _pp = calloc_chk(_maxPairThreshold + 1, sizeof(per_pair_threshold_data_t));
    UPC_ALL_ALLOC_CHK(_n50s, _maxPairThreshold + 1, sizeof(int));
    UPC_ALL_ALLOC_CHK(_nGaps, _maxPairThreshold + 1, sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(_nScaffolds, _maxPairThreshold + 1, sizeof(int64_t));

    // Over-estimation of output size for each thread
    int64_t myProvision = (2 * _totalLinks + THREADS - 1) / THREADS + 1;

    for (int pairThreshold = _minPairThreshold; pairThreshold <= _maxPairThreshold; pairThreshold++) {
        if (_totalLinks) {
            LOGF("Provisioning for pair %d: %0.3f MB local shared memory for myProvision %lld tie heaps\n", pairThreshold, 1.0 / ONE_MB * myProvision * sizeof(tie_t), (lld)myProvision);
            UPC_ALL_ALLOC_CHK(_pp[pairThreshold].tieSharedHeap, THREADS, sizeof(TieHeap));
            initHeap(&(_pp[pairThreshold].tieSharedHeap[MYTHREAD]), myProvision * sizeof(tie_t));
        } else {
            _pp[pairThreshold].tieSharedHeap = NULL;
        }
        DBG2("Allocated tieSharedHeap: _totalLinks=%lld myProvision=%lld\n", (lld)_totalLinks, (lld)myProvision);
        // Allocate shared buffers for storing the scaffold report files
        UPC_ALL_ALLOC_CHK(_pp[pairThreshold].sharedScaffolds, THREADS, sizeof(ScaffoldReportFiles));
        // Actual initiation of heap happens in run_parallel() now, after scaffold report is read (if > round 1)

        int minActiveThread = MIN_ACTIVE_THREAD(pairThreshold);
        int64_t myTotalObjects = (_nTotalObjects + _nActiveThreads) / _nActiveThreads;
        UPC_ALL_ALLOC_CHK(_pp[pairThreshold].sharingoNoObjectPtrs, THREADS, sizeof(sharedoNoObjectPtr));
        if (MYTHREAD >= minActiveThread && MYTHREAD < minActiveThread + _nActiveThreads) {
            LOGF("Provisioning for pair %d: %0.3f MB local shared memory for %lld oNoObjects\n", pairThreshold, 1.0 / ONE_MB * myTotalObjects * sizeof(oNoObject), (lld)myTotalObjects);
            UPC_ALLOC_CHK(_pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD], myTotalObjects * sizeof(oNoObject));
            // set the original to -1 to catch errors
            for (int j = 0; j < myTotalObjects; j++) {
                _pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD][j].my_id = -1;
            }
        } else {
            _pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD] = NULL;
        }
        DBG2("Allocated sharingoNoObjectPtrs: myTotalObjects=%lld _pp[%d].sharingoNoObjectPtrs[MYTHREAD]=%p\n",
             (lld)myTotalObjects, pairThreshold, (oNoObject *)_pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD]);

        UPC_ALL_ALLOC_CHK(_pp[pairThreshold].numObjects, THREADS, sizeof(int64_t));
        UPC_ALL_ALLOC_CHK(_pp[pairThreshold].sharingScaffPtrs, THREADS, sizeof(sharedScaffPtr));
        if (MYTHREAD >= minActiveThread && MYTHREAD < minActiveThread + _nActiveThreads) {
            LOGF("allocating %lld scaf_reports as %0.3f MB local shared\n", (lld)myTotalObjects, 1.0 / ONE_MB * myTotalObjects * sizeof(scaf_report_t));
            _pp[pairThreshold].numObjects[MYTHREAD] = myTotalObjects;
            UPC_ALLOC_CHK(_pp[pairThreshold].sharingScaffPtrs[MYTHREAD], myTotalObjects * sizeof(scaf_report_t));
            for (int j = 0; j < myTotalObjects; j++) {
                _pp[pairThreshold].sharingScaffPtrs[MYTHREAD][j].length = -1;
                _pp[pairThreshold].sharingScaffPtrs[MYTHREAD][j].nEntries = 0;
                _pp[pairThreshold].sharingScaffPtrs[MYTHREAD][j].entries = NULL;
            }
        } else {
            _pp[pairThreshold].numObjects[MYTHREAD] = 0;
            _pp[pairThreshold].sharingScaffPtrs[MYTHREAD] = NULL;
        }
        DBG2("Allocated sharingScaffPtrs: myTotalObjects=%lld  _pp[%d].sharingScaffPtrs[MYTHREAD]=%p\n",
             (lld)myTotalObjects, pairThreshold, (scaf_report_t *)_pp[pairThreshold].sharingScaffPtrs[MYTHREAD]);

        UPC_ALL_ALLOC_CHK(_pp[pairThreshold].sharingDepths, THREADS, sizeof(SharedFloatPtr));
        if (MYTHREAD >= minActiveThread && MYTHREAD < minActiveThread + _nActiveThreads) {
            myTotalObjects = (_totalContigs + _nActiveThreads) / _nActiveThreads;
            LOGF("allocating sharingDepths for %lld objects as %0.3f MB local shared\n", (lld)myTotalObjects, 1.0 / ONE_MB * myTotalObjects * sizeof(float));
            //WARN("pairThreshold %d: allocating sharingDepths for %ld objects, interval >= %d and < %d \n",
            //     pairThreshold, myTotalObjects, minActiveThread, minActiveThread + _nActiveThreads);
            UPC_ALLOC_CHK(_pp[pairThreshold].sharingDepths[MYTHREAD], myTotalObjects * sizeof(float));
            for (int j = 0; j < myTotalObjects; j++) {
                _pp[pairThreshold].sharingDepths[MYTHREAD][j] = -1;
            }
        } else {
            _pp[pairThreshold].sharingDepths[MYTHREAD] = NULL;
        }
        DBG2("Allocated sharingDepths: myTotalObjects=%lld _pp[%d].sharingDepths[MYTHREAD]=%p\n",
             //WARN("Allocated sharingDepths: myTotalObjects=%lld _pp[%d].sharingDepths[MYTHREAD]=%p\n",
             (lld)myTotalObjects, pairThreshold, (float *)_pp[pairThreshold].sharingDepths[MYTHREAD]);
        UPC_LOGGED_BARRIER;
    }
    UPC_LOGGED_BARRIER;

    UPC_TICK_T t = upc_ticks_now();
    run_parallel();

    LOGF("I completed Parallel in %.3f s\n", ELAPSED_SECS(t));
    UPC_LOGGED_BARRIER;
    serial_printf("Parallel section done in %.3f s\n", ELAPSED_SECS(t));

    UPC_LOGGED_BARRIER;
    t = upc_ticks_now();
    for (int i = _minPairThreshold; i <= _maxPairThreshold; i++) {
        if (MYTHREAD == MIN_ACTIVE_THREAD(i) % THREADS) {
            run_serial(i);
            LOG("Completed serial for threshold %d in %.3f s\n", i, ELAPSED_SECS(t));
            break;
        }
    }
    UPC_LOGGED_BARRIER;
    serial_printf("Serial sections done in %.3f s\n", ELAPSED_SECS(t));

    //   int chr14_checks[] = {0, 7807, 8322, 7082, 5810, 4832, 4059, 3454, 3011, 2622, 2309, 2047, 1836,
    //                         1658, 1500, 1361, 1248, 1152, 1071, 1002, 947, 899, 862, 833, 809, 790, 774};
    //   int chr14_num_fails = 0;
    int bestN50 = 0;
    int bestPairThreshold = 0;
    serial_printf("Pair threshold N50s\n");
    for (int i = _minPairThreshold; i <= _maxPairThreshold; i++) {
        //      char chr14_fail = ' ';
        //      if (chr14_checks[i] != _n50s[i]) {
        //         chr14_fail = '*';
        //         chr14_num_fails++;
        //      }
        //      serial_printf("%d\t%d %c\n", i, _n50s[i], chr14_fail);
        serial_printf("%d\t%d\n", i, _n50s[i]);
        if (_n50s[i] > bestN50) {
            bestN50 = _n50s[i];
            bestPairThreshold = i;
        }
    }

    //   serial_printf("CHR14 failures: %d out of %d\n",
    //                 chr14_num_fails, _maxPairThreshold - _minPairThreshold + 1);

    serial_printf("Best N50 is %d from pair threshold %d\n", bestN50, bestPairThreshold);
    ADD_DIAG("%d", "best_N50", bestN50);

    DBG("ready to write files\n");

    t = upc_ticks_now();

    // for shared memory, each thread needs to write its own data, to ensure it is in the local
    // shared memory. So it needs to do a remote fetch to ensure this.

    // write out only the results for the best pair threshold

    // sharedScaffolds may have been re-allocated to a different thread (0)
    // so the owning thread must get the data if necessary

    {   // new block to reduce stack size
        char scaffName[MAX_FILE_PATH];
        sprintf(scaffName, "%s" GZIP_EXT, _outputPrefix);

        Heap bestSharedScaffolds = _pp[bestPairThreshold].sharedScaffolds[MYTHREAD];
        int64_t mySize = bestSharedScaffolds.position;
        CHECK_BOUNDS(mySize, bestSharedScaffolds.size);

        char *myOutput = malloc_chk(mySize + 1);
        upc_memget(myOutput, (SharedCharPtr)bestSharedScaffolds.ptr, mySize);
        myOutput[mySize] = '\0';

        GZIP_FILE myOutFD = openCheckpoint(scaffName, "w");
        if (mySize) {
            int res = GZIP_FWRITE(myOutput, 1, mySize, myOutFD);
            if (res < 0) {
                DIE("Could not write %lld bytes to file %s: error %d, %s\n",
                    (lld)mySize, scaffName, res, strerror(errno));
            }
            if (res < mySize) {
                WARN("Output was truncated: should have written %lld, but wrote %lld\n", (lld)mySize, (lld)res);
            }
        }
        free_chk(myOutput);
        closeCheckpoint(myOutFD);
        DBG("wrote file %s\n", scaffName);
    }

    UPC_LOGGED_BARRIER;
    for (int i = _minPairThreshold; i <= _maxPairThreshold; i++) {
        if (_pp[i].sharedScaffolds) {
            freeHeap(&(_pp[i].sharedScaffolds[MYTHREAD]));
        }
    }
    serial_printf("Freed all my sharedScaffolds (Heaps)\n");

    UPC_LOGGED_BARRIER;
    for (int i = _minPairThreshold; i <= _maxPairThreshold; i++) {
        if (_pp[i].sharedScaffolds) {
            UPC_ALL_FREE_CHK(_pp[i].sharedScaffolds);
        }
    }
    serial_printf("Freed sharedScaffolds\n");

    if (!MYTHREAD) {
        // now cp the txt files for the best N50
        char fname[MAX_FILE_PATH];

        sprintf(fname, "nScaffolds_%s.txt", _outputPrefix);
        get_rank_path(fname, -1);
        FILE *fd = fopen_chk(fname, "w");
        fprintf(fd, "%lld\n", (lld)_nScaffolds[bestPairThreshold]);
        fclose_track(fd);

        sprintf(fname, "nGaps_%s.txt", _outputPrefix);
        get_rank_path(fname, -1);
        fd = fopen_chk(fname, "w");
        fprintf(fd, "%lld\n", (lld)_nGaps[bestPairThreshold]);
        fclose_track(fd);

        sprintf(fname, "n50_%s.txt", _outputPrefix);
        get_rank_path(fname, -1);
        fd = fopen_chk(fname, "w");
        fprintf(fd, "%d %d\n", bestPairThreshold, _n50s[bestPairThreshold]);
        fclose_track(fd);
    }
    UPC_LOGGED_BARRIER;
    if (_n50s) {
        UPC_ALL_FREE_CHK(_n50s);
    }
    if (_nGaps) {
        UPC_ALL_FREE_CHK(_nGaps);
    }
    if (_nScaffolds) {
        UPC_ALL_FREE_CHK(_nScaffolds);
    }
    for (int pairThreshold = _minPairThreshold; pairThreshold <= _maxPairThreshold; pairThreshold++) {
        DBG("Freeing _pp[%d] sharing data arrays\n", (int)pairThreshold);
        if (_pp[pairThreshold].sharingoNoObjectPtrs) {
            if (_pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD]) {
                UPC_FREE_CHK(_pp[pairThreshold].sharingoNoObjectPtrs[MYTHREAD]);
            }
            UPC_ALL_FREE_CHK(_pp[pairThreshold].sharingoNoObjectPtrs);
        }
        if (_pp[pairThreshold].sharingScaffPtrs) {
            sharedScaffPtr mySharingScaffPtrs = _pp[pairThreshold].sharingScaffPtrs[MYTHREAD];
            if (mySharingScaffPtrs) {
                if (MYTHREAD < _nActiveThreads) { // only one active block needs to free the entries (as they are shared across all active blocks)
                    DBG("Freeing _pp[%d].sharingScaffPtrs[MYTHREAD] with myTotalObjects=%lld\n", (int)pairThreshold, (lld)_pp[pairThreshold].numObjects[MYTHREAD]);
                    int64_t myTotalObjects = _pp[pairThreshold].numObjects[MYTHREAD];
                    for (int64_t i = 0; i < myTotalObjects; i++) {
                        if (mySharingScaffPtrs[i].nEntries > 0) {
                            UPC_FREE_CHK0(mySharingScaffPtrs[i].entries);
                        }
                    }
                }
                UPC_FREE_CHK(mySharingScaffPtrs);
            }
            UPC_ALL_FREE_CHK(_pp[pairThreshold].sharingScaffPtrs);
            UPC_ALL_FREE_CHK(_pp[pairThreshold].numObjects);
        }
        if (_pp[pairThreshold].sharingDepths) {
            if (_pp[pairThreshold].sharingDepths[MYTHREAD]) {
                UPC_FREE_CHK(_pp[pairThreshold].sharingDepths[MYTHREAD]);
            }
            UPC_ALL_FREE_CHK(_pp[pairThreshold].sharingDepths);
        }
        if (_pp[pairThreshold].tieSharedHeap) {
            freeHeap(&(_pp[pairThreshold].tieSharedHeap[MYTHREAD]));
            UPC_ALL_FREE_CHK(_pp[pairThreshold].tieSharedHeap);
        }
    }
    free_chk(_pp);
    UPC_LOGGED_BARRIER;
    serial_printf("Writing output took %.3f s\n", ELAPSED_SECS(t));
    serial_printf("Output written to %s\n", _outputPrefix);
    serial_printf("Overall time for %s is %.2f s\n", basename(argv[0]),
                  ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    return 0;
}
