#ifndef __ONO_UTILS_H
#define __ONO_UTILS_H

#include <assert.h>
#include <math.h>
#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "defines.h"

#define MAX_LINESIZE 2000
#define MAX_HISTO_SIZE 100000
#define MAX_SCAFF_REPORT_LENGTH 20000
#define N_SCAFF_LINES 1000
#define BS 1
#define UNDEFINED (-999999)
#define DIVERGENCE (-1)
#define CONVERGENCE (-2)
#define TERMINATION (-3)
#define NEXT (-4)
#define EXTENSION (-5)

#define CONTIG 0
#define GAP 1
#define PLUS 0
#define MINUS 1
#define MAX_TIES_LOCAL 1000

#define END_TYPES 6
#define UNMARKED 0
#define SELF_TIE 1
#define TIE_COLLISION 2
#define DEPTH_DROP 3
#define NO_TIES 4
#define MULTIPLE_BEST_TIE 5

#define SPLINT 0
#define SPAN 1

#define EXP_FACTOR 2
#define MAX_NAME_SIZE 255

#define NO_GOOD_TIES 0
#define CLOSEST_LARGE 1
#define CLOSEST_EXTENDABLE 2
#define CLOSEST 3

extern int64_t _totalLinks;
extern int64_t _totalContigs;
extern int64_t _nTotalObjects;

typedef struct libInfoType {
    int  insertSize;
    int  stdDev;
    char libName[LIB_NAME_LEN];
    char linkFileNamePrefix[MAX_NAME_SIZE];
} libInfoType;

int splitLinkMetaLine(char *line, libInfoType *libInfo);

typedef struct tie_t {
    int64_t end1_id;
    int64_t nGapLinks;
    int     gapEstimate;
    float   gapUncertainty;
    int64_t end2_id;
    char    ending1;
    char    ending2;
    char    splintFlag;
} tie_t;

typedef struct dataTie_t {
    int64_t end2_id;
    int64_t nGapLinks;
    int     gapEstimate;
    float   gapUncertainty;
    int     endLength;
    int     is_dropoff;
    char    ending2;
} dataTie_t;

typedef struct endTies_t {
    int64_t    nTies;
    dataTie_t *ties;
    int        cur_pos;
} endTies_t;

typedef struct splintDataTie_t {
    int64_t end2_id;
    char    ending2;
    char    isSplinted;
} splintDataTie_t;

typedef struct splintTracking_endTies_t {
    int64_t          nTies;
    splintDataTie_t *ties;
    int              cur_pos;
} splintTracking_endTies_t;

typedef struct end_t {
    int64_t end_id;
    char    ending;
} end_t;

typedef struct endLock_t {
    int64_t gap;
    int64_t end_id;
    float   gapUncertainty;
    char    ending;
} endLock_t;

struct endList_t {
    int64_t           end_id;
    struct endList_t *next;
    char              ending;
};

typedef struct endList_t endList_t;

typedef shared[] tie_t *SharedTiePtr;

typedef struct splints_stack_entry {
    int64_t nUsedSplints;
    int     gapEstimate;
    char    linkFileID;
} splints_stack_entry;

typedef struct spans_stack_entry {
    int64_t nUsedSpans;
    int     gapEstimate;
    float   gapUncertainty;
    char    linkFileID;
} spans_stack_entry;

typedef struct lib_info {
    int  insertSize;
    int  stdDev;
    char lib_id;
    char linkFile[MAX_LINESIZE];
} lib_info;

typedef struct scaf_entry_t {
    int64_t entry_id;
    int64_t f1;
    int64_t f2;
    float   fDepth;
    char    type;
    char    sign;
} scaf_entry_t;

typedef struct scaf_report_t {
    int     realBases;
    int     depth;
    float   fdepth;
    int     length;
    int64_t nEntries;
    shared[] scaf_entry_t * entries;
} scaf_report_t;

typedef struct oNoObject {
    int     length;
    int64_t my_id;
} oNoObject;

typedef Heap TieHeap;
typedef Heap ScaffoldReportFiles;

typedef shared[] oNoObject *sharedoNoObjectPtr;
typedef shared[] scaf_report_t *sharedScaffPtr;
typedef shared[] scaf_entry_t *sharedScaffEntriesPtr;

void append_scaffold(int64_t scaffoldID, char *scaffReportLine, ScaffoldReportFiles *cachedSharedScaffolds);


int oNocmpfunc(const void *a, const void *b);

/* Split a linkData entry */
int split_linkData(char *line, lib_info *libInfo, int *libs_no);


int cmpFuncTie(const void *a, const void *b);

unsigned char markEnd(int64_t end, char ending, endTies_t *endTies, splintTracking_endTies_t *splintTracking_endTies, int64_t maxEndTies, float *contigDepths, float peakDepth, int depthInfoAvailable, int merSize, int srfFlag, scaf_report_t *scaffReport, int64_t *averted_ties);

char bestTieFunction(int end, char ending, endTies_t *endTies, int64_t maxEndTies, oNoObject *oNoObjectlengths, int suspendable, unsigned char *endMarks, char *suspended, int *closestLargeRes, char *closestLargeEndingRes);

int mutualUniqueBest(end_t end1, end_t end2, end_t *bestTieArray, endList_t *bestTiedByArray, char *suspended);

int getTieInfo(end_t end1, end_t end2, endTies_t *endTies, int64_t maxEndTies, dataTie_t *result);


#endif
