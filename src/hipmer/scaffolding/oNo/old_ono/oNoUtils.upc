#include "oNoUtils.h"

int splitLinkMetaLine(char *line, libInfoType *libInfo)
{
    char *token;
    char *aux;

    token = strtok_r(line, "\t", &aux);
    strncpy(libInfo->libName, token, LIB_NAME_LEN - 1);
    token = strtok_r(NULL, "\t", &aux);
    libInfo->insertSize = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    libInfo->stdDev = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    int len = strlen(token);
    // make sure to drop the \n at the end
    strncpy(libInfo->linkFileNamePrefix, token, len - 1);
    libInfo->linkFileNamePrefix[len - 1] = '\0';
    return 1;
}

void append_scaffold(int64_t scaffoldID, char *scaffReportLine, ScaffoldReportFiles *cachedSharedScaffolds)
{
    assert(scaffReportLine != NULL);
    assert(cachedSharedScaffolds != NULL);
    int curLen = strlen(scaffReportLine);
    assert(curLen > 0);
    int64_t destThread = scaffoldID % THREADS;
    int64_t appendSize = curLen * sizeof(char);
#if USE_NEW_HEAP
    appendHeap(cachedSharedScaffolds[destThread], scaffReportLine, curLen);
#else
    //fprintf(stderr, "thread %d: destThread %d: %s", MYTHREAD, destThread, scaffReportLine);
    if (cachedSharedScaffolds[destThread].position >= cachedSharedScaffolds[destThread].size) {
        WARN("append_scaffold already exceeded size! position=%lld size=%lld\n", (lld)cachedSharedScaffolds[destThread].position, (lld)cachedSharedScaffolds[destThread].size);
    }
    if (cachedSharedScaffolds[destThread].position + appendSize >= cachedSharedScaffolds[destThread].size) {
        //printf("Thread %lld needs more memory: %lld >= %lld\n",
        //       (lld) destThread, (lld) cachedSharedScaffolds[destThread].position + appendSize, (lld) cachedSharedScaffolds[destThread].size);
        // Unlucky thread needs more memory.
        int64_t newSize = (cachedSharedScaffolds[destThread].size + appendSize) * 3 / 2;
        DBG("Reallocating (locally) sharedScaffolds for thread %lld to %lld\n", (lld)destThread, (lld)newSize);
        SharedCharPtr newPtr;
        UPC_ALLOC_CHK(newPtr, newSize);
        assert(upc_threadof(newPtr) == MYTHREAD);
        if (newPtr == NULL) {
            DIE("append_scaffold could not re-allocate %lld bytes for unlucky thread %lld\n", (lld)newSize, (lld)destThread);
        }
        char *localPtr = (char *)newPtr;
        memset(localPtr, 0, newSize);
        upc_memget(localPtr, cachedSharedScaffolds[destThread].ptr, cachedSharedScaffolds[destThread].position);
        SharedCharPtr oldPtr = cachedSharedScaffolds[destThread].ptr;
        cachedSharedScaffolds[destThread].ptr = newPtr;
        cachedSharedScaffolds[destThread].size = newSize;
        UPC_FREE_CHK(oldPtr);
    }
    assert(cachedSharedScaffolds[destThread].position + appendSize < cachedSharedScaffolds[destThread].size);
    upc_memput(cachedSharedScaffolds[destThread].ptr + cachedSharedScaffolds[destThread].position, scaffReportLine, appendSize);
    cachedSharedScaffolds[destThread].position += curLen;
#endif
    DBG2("Appended '%.*s' to %ld sharedScaffolds\n", curLen, scaffReportLine, destThread);
}


int oNocmpfunc(const void *a, const void *b)
{
    if (((oNoObject *)b)->length == ((oNoObject *)a)->length) {
        return ((oNoObject *)b)->my_id - ((oNoObject *)a)->my_id;
    }

    return ((oNoObject *)b)->length - ((oNoObject *)a)->length;
}

/* Split a linkData entry */
int split_linkData(char *line, lib_info *libInfo, int *libs_no)
{
    char *token, *aux;
    int libnum = 0;
    char libs_string[MAX_LINESIZE];
    char inserts_string[MAX_LINESIZE];
    char std_string[MAX_LINESIZE];
    char linkFile[MAX_LINESIZE];

    token = strtok_r(line, "\t", &aux);
    strcpy(libs_string, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(inserts_string, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(std_string, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(linkFile, token);
    //linkFile[strlen(linkFile)-3] = '\0';

    token = strtok_r(libs_string, ",", &aux);
    while (token != NULL) {
        libInfo[libnum].lib_id = *(token + 3);
        strcpy(libInfo[libnum].linkFile, linkFile);
        libnum++;
        token = strtok_r(NULL, ",", &aux);
    }

    token = strtok_r(inserts_string, ",", &aux);
    libnum = 0;
    while (token != NULL) {
        libInfo[libnum].insertSize = atoi(token);
        libnum++;
        token = strtok_r(NULL, ",", &aux);
    }

    token = strtok_r(std_string, ",", &aux);
    libnum = 0;
    while (token != NULL) {
        libInfo[libnum].stdDev = atoi(token);
        libnum++;
        token = strtok_r(NULL, ",", &aux);
    }

    (*libs_no) = libnum;

    return 1;
}

int cmpFuncTie(const void *a, const void *b)
{
    const dataTie_t *p1 = (dataTie_t *)a;
    const dataTie_t *p2 = (dataTie_t *)b;

    if (p1->gapEstimate > p2->gapEstimate) {
        return 1;
    } else if (p1->gapEstimate < p2->gapEstimate) {
        return -1;
    } else if (p1->endLength > p2->endLength) {
        return 1;
    } else if (p1->endLength < p2->endLength) {
        return -1;
    }

    return 0;
}

unsigned char markEnd(int64_t end, char ending, endTies_t *endTies, splintTracking_endTies_t *splintTracking_endTies, int64_t maxEndTies, float *contigDepths, float peakDepth, int depthInfoAvailable, int merSize, int srfFlag, scaf_report_t *scaffReport, int64_t *averted_ties)
{
    int64_t testContig, i, nLinks_i, gapEstimate_i, gapUncertainty_i, contig_i, contig_j, contigLen_i, start_i, start_j, end_i, uncertainty_i, uncertainty_j, end_j, overlap, excessOverlap;
    //int posInEndTies = (ending == 3) ? 2*end : 2*end+1 ;
    int64_t posInEndTies;

    if (ending == 3) {
        posInEndTies = 2 * end;
    } else {
        posInEndTies = 2 * end + 1;
    }
    CHECK_BOUNDS(posInEndTies, maxEndTies);
    float contigDepth_i, testContigDepth, strain;

    int64_t nTies = endTies[posInEndTies].nTies;

    if (nTies == 0) {
        return NO_TIES;
    }

    testContig = end;
    testContigDepth = UNDEFINED;
    CHECK_BOUNDS(testContig, _totalContigs);

    if (depthInfoAvailable == 1) {
        if (srfFlag) {
            testContigDepth = scaffReport[testContig].fdepth / scaffReport[testContig].realBases;
        } else {
            testContigDepth = contigDepths[testContig];
        }
    }

    int nCollisions = 0;
    int maxStrain = 3;
    float maxDepthDropoff = 3.0;
    dataTie_t *ties = endTies[posInEndTies].ties;
    int64_t nDepthDropoffs = 0;

    for (i = 0; i < nTies; i++) {
        ties[i].is_dropoff = 0;
        nLinks_i = ties[i].nGapLinks;
        gapEstimate_i = ties[i].gapEstimate;
        gapUncertainty_i = ties[i].gapUncertainty;
        contig_i = ties[i].end2_id;
        CHECK_BOUNDS(contig_i, _totalContigs);

        if (contig_i == testContig) {
            return SELF_TIE;
        }


	// check for the dropoffs
        if (depthInfoAvailable == 1) {
            if (srfFlag) {
                contigDepth_i = scaffReport[contig_i].fdepth / scaffReport[contig_i].realBases;
            } else {
                contigDepth_i = contigDepths[contig_i];
            }


	    
#ifdef PEAK_DEPTH_UNIFORM
            if (((1.0 * (testContigDepth - contigDepth_i)) / (1.0 * peakDepth)) > maxDepthDropoff) {
                nDepthDropoffs++;
                ties[i].is_dropoff = 1;
            }
#endif
#ifndef PEAK_DEPTH_UNIFORM
            // this is the default - peak depth cannot be assumed to be uniform
            if (fabs(testContigDepth - contigDepth_i) / (fmin(testContigDepth, contigDepth_i)) > maxDepthDropoff) {
                nDepthDropoffs++;
                ties[i].is_dropoff = 1;
//            serial_printf("dropoff: testContigDepth %f contigDepth %f\n", testContigDepth, contigDepth_i);
            }
#endif
        }

    }
    // only return depth drop if all ties are dropoffs
    if (nDepthDropoffs == nTies) {
        return DEPTH_DROP;
    }

    
    qsort(ties, nTies, sizeof(dataTie_t), cmpFuncTie);
    char contigEnd_i, contigEnd_j;
    int64_t posToCheck = 0, nTiesToCheck = 0, z, isSplinted = 0;
    splintDataTie_t *splintDataArray;

    for (i = 0; i < nTies - 1; i++) {
        // ignore all ties that are dropoffs
        if (ties[i].is_dropoff) {
            continue;
        }
        start_i = ties[i].gapEstimate;
        end_i = start_i + ties[i].endLength - 1;
        uncertainty_i = ties[i].gapUncertainty;

        start_j = ties[i + 1].gapEstimate;
        end_j = start_j + ties[i + 1].endLength - 1;
        uncertainty_j = ties[i + 1].gapUncertainty;

        overlap = end_i - start_j + 1;
        if (overlap > merSize - 2) {
            excessOverlap = overlap - (merSize - 2);
            strain = (1.0 * excessOverlap) / (1.0 * (uncertainty_i + uncertainty_j));
            if (strain > (1.0 * maxStrain)) {
                contig_i = ties[i].end2_id;
                contigEnd_i = ties[i].ending2;
                if (contigEnd_i == 3) {
                    contigEnd_i = 5;
                } else {
                    contigEnd_i = 3;
                }

                contig_j = ties[i + 1].end2_id;
                contigEnd_j = ties[i + 1].ending2;

                if (contigEnd_i == 3) {
                    posToCheck = 2 * contig_i;
                } else {
                    posToCheck = 2 * contig_i + 1;
                }
                CHECK_BOUNDS(posToCheck, maxEndTies);

                nTiesToCheck = splintTracking_endTies[posToCheck].nTies;
                splintDataArray = splintTracking_endTies[posToCheck].ties;
                isSplinted = 0;

                for (z = 0; z < nTiesToCheck; z++) {
                    if ((splintDataArray[z].end2_id == contig_j) && (splintDataArray[z].ending2 == contigEnd_j) && (splintDataArray[z].isSplinted == 1)) {
                        isSplinted = 1;
                        (*averted_ties)++;
                    }
                }

                if (isSplinted == 0) {
                    return TIE_COLLISION;
                }
            }
        }
    }
    return UNMARKED;
}

char bestTieFunction(int end, char ending, endTies_t *endTies, int64_t maxEndTies, oNoObject *oNoObjectlengths, int suspendable, unsigned char *endMarks, char *suspended, int *closestLargeRes, char *closestLargeEndingRes)
{
    //int posInEndTies = (ending == 3) ? 2*end : 2*end+1 ;
    int posInEndTies;

    if (ending == 3) {
        posInEndTies = 2 * end;
    } else {
        posInEndTies = 2 * end + 1;
    }
    CHECK_BOUNDS(posInEndTies, maxEndTies);
    int64_t nTies = endTies[posInEndTies].nTies;

    if (nTies == 0) {
        (*closestLargeRes) = UNDEFINED;
        return NO_GOOD_TIES;
    }

    dataTie_t *ties = endTies[posInEndTies].ties;
    int testContig = end;
    CHECK_BOUNDS(testContig, _nTotalObjects);
    int testContigLength = oNoObjectlengths[testContig].length;
    int largeObject = 0;
    if (testContigLength > suspendable) {
        largeObject = 1;
    }

    DBGN("\n[bestTieFunction] testContig: %d, testContigLength: %d\n", testContig, testContigLength);
    int64_t nGoodTies, nLinks_i, gapEstimate_i, gapUncertainty_i, contig_i;
    char ending_i, endMark_i;
    int posTiedEnd_i;
    int64_t i;
    nGoodTies = 0;
    dataTie_t *goodTies = (dataTie_t *)calloc_chk(nTies, sizeof(dataTie_t));

    for (i = 0; i < nTies; i++) {
        nLinks_i = ties[i].nGapLinks;
        gapEstimate_i = ties[i].gapEstimate;
        gapUncertainty_i = ties[i].gapUncertainty;
        contig_i = ties[i].end2_id;
        ending_i = ties[i].ending2;

        //posTiedEnd_i = (ending_i == 3) ? 2 * contig_i  : 2 * contig_i + 1;
        if (ending_i == 3) {
            posTiedEnd_i = 2 * contig_i;
        } else {
            posTiedEnd_i = 2 * contig_i + 1;
        }
        endMark_i = endMarks[posTiedEnd_i];

        if (endMark_i == UNMARKED) {
            goodTies[nGoodTies] = ties[i];
            nGoodTies++;
        }
    }

    if (nGoodTies == 0) {
        free_chk(goodTies);
        (*closestLargeRes) = UNDEFINED;
	DBGN("[bestTieFunction]  NO_GOOD_TIES \n");
        return NO_GOOD_TIES;
    }

    qsort(goodTies, nGoodTies, sizeof(dataTie_t), cmpFuncTie);
    int *localSuspended = (int *)calloc_chk(nGoodTies, sizeof(int));
    int nSus, closestLarge, closestExtendable, sContig, contigLen_i, testEnd_i;
    char closestLargeEnding, closestExtendableEnding, sEnding, end_i, otherEnd_i;
    nSus = 0;

    /* If a large object, return the closest large object (if one exists) small objects may be suspended between large objects */

    if (largeObject == 1) {
        closestLarge = UNDEFINED;
        for (i = 0; i < nGoodTies; i++) {
            sContig = goodTies[i].end2_id;
            sEnding = goodTies[i].ending2;
            contigLen_i = goodTies[i].endLength;
	    DBGN("[bestTieFunction]\t tied end %d.%d, len:%d",sContig, sEnding, contigLen_i);
            if (contigLen_i > suspendable) {
                closestLarge = sContig;
                closestLargeEnding = sEnding;
		DBGN("\t - closestLarge\n");
                break;
            } else {
	        DBGN("\t - suspendable\n");
                localSuspended[nSus] = sContig;
                nSus++;
            }
        }

        if (closestLarge != UNDEFINED) {
            for (i = 0; i < nSus; i++) {
                suspended[localSuspended[i]] = 1;
            }

            (*closestLargeRes) = closestLarge;
            (*closestLargeEndingRes) = closestLargeEnding;
            free_chk(goodTies);
            free_chk(localSuspended);
	    DBGN("return closestLarge %d.%d\n", closestLarge, closestLargeEnding);
            return CLOSEST_LARGE;
        }
    }

    /* Return the closest extendable object (if one exists) unextendable objects may be suspended */

    nSus = 0;
    closestExtendable = UNDEFINED;
    	    
    DBGN("No closestLarge found... \n\n");
    for (i = 0; i < nGoodTies; i++) {
        contig_i = goodTies[i].end2_id;
        end_i = goodTies[i].ending2;
        if (end_i == 3) {
            otherEnd_i = 5;
        } else {
            otherEnd_i = 3;
        }

        if (otherEnd_i == 3) {
            testEnd_i = 2 * contig_i;
        } else {
            testEnd_i = 2 * contig_i + 1;
        }
        endMark_i = endMarks[testEnd_i];
        if (endMark_i == UNMARKED) {
            closestExtendable = contig_i;
            closestExtendableEnding = end_i;
            break;
        } else {
            localSuspended[nSus] = contig_i;
            nSus++;
        }
    }

    if (closestExtendable != UNDEFINED) {
        for (i = 0; i < nSus; i++) {
            suspended[localSuspended[i]] = 1;
        }

        (*closestLargeRes) = closestExtendable;
        (*closestLargeEndingRes) = closestExtendableEnding;
        free_chk(goodTies);
        free_chk(localSuspended);
	DBGN("return closestExtendable:  %d.%d\n", closestExtendable, closestExtendableEnding);
        return CLOSEST_EXTENDABLE;
    }

    /* Just return the closest object */
    DBGN("No closestExtendable found... \n\n");
    
    (*closestLargeRes) = goodTies[0].end2_id;
    (*closestLargeEndingRes) = goodTies[0].ending2;
    DBGN("return closest: %d.%d\n", goodTies[0].end2_id, goodTies[0].ending2);
    free_chk(goodTies);
    free_chk(localSuspended);
    return CLOSEST;
}

int mutualUniqueBest(end_t end1, end_t end2, end_t *bestTieArray, endList_t *bestTiedByArray, char *suspended)
{
    int64_t piece1 = end1.end_id;
    int64_t piece2 = end2.end_id;

    if ((suspended[piece1] == 1) || (suspended[piece2] == 1)) {
        return 0;
    }

    //int pos1 = ( end1.ending == 3 ) ? 2*piece1 : 2*piece1 + 1;
    //int pos2 = ( end2.ending == 3 ) ? 2*piece2 : 2*piece2 + 1;
    int pos1, pos2;
    if (end1.ending == 3) {
        pos1 = 2 * piece1;
    } else {
        pos1 = 2 * piece1 + 1;
    }

    if (end2.ending == 3) {
        pos2 = 2 * piece2;
    } else {
        pos2 = 2 * piece2 + 1;
    }

    int exists1 = 0;
    int exists2 = 0;
    end_t bestTie1, bestTie2;
    endList_t bestTiedBy1, bestTiedBy2;

    bestTie1 = bestTieArray[pos1];

    if (bestTie1.end_id == UNDEFINED) {
        return 0;
    }

    bestTie2 = bestTieArray[pos2];
    if (bestTie2.end_id == UNDEFINED) {
        return 0;
    }

    if ((bestTie1.end_id != end2.end_id) || (bestTie1.ending != end2.ending)) {
        return 0;
    }

    if ((bestTie2.end_id != end1.end_id) || (bestTie2.ending != end1.ending)) {
        return 0;
    }

    bestTiedBy1 = bestTiedByArray[pos1];
    if (bestTiedBy1.end_id == UNDEFINED) {
        return 0;
    }

    bestTiedBy2 = bestTiedByArray[pos2];
    if (bestTiedBy2.end_id == UNDEFINED) {
        return 0;
    }

    if (bestTiedBy1.next != NULL) {
        return 0;
    }

    if ((bestTiedBy1.end_id != end2.end_id) || (bestTiedBy1.ending != end2.ending)) {
        return 0;
    }

    if (bestTiedBy2.next != NULL) {
        return 0;
    }

    if ((bestTiedBy2.end_id != end1.end_id) || (bestTiedBy2.ending != end1.ending)) {
        return 0;
    }

    return 1;
}

int getTieInfo(end_t end1, end_t end2, endTies_t *endTies, int64_t maxEndTies, dataTie_t *result)
{
    //int pos1 = (end1.ending == 3) ? 2*end1.end_id : 2*end1.end_id+1 ;
    int64_t pos1;

    if (end1.ending == 3) {
        pos1 = 2 * end1.end_id;
    } else {
        pos1 = 2 * end1.end_id + 1;
    }
    CHECK_BOUNDS(pos1, maxEndTies);
    int64_t nTies = endTies[pos1].nTies;
    if (nTies == 0) {
        return 0;
    }

    dataTie_t *ties;
    dataTie_t tie_i;
    ties = endTies[pos1].ties;
    int64_t i;

    for (i = 0; i < nTies; i++) {
        tie_i = ties[i];
        if ((tie_i.end2_id == end2.end_id) && (tie_i.ending2 == end2.ending)) {
            (*result) = tie_i;
            return 1;
        }
    }

    return 0;
}
