#ifndef ONO_HASH_H_
#define ONO_HASH_H_

#define SPLINT 0
#define SPAN 1
#define LINK_CHUNK_SIZE 100
#define BS 1
#define PLUS 0
#define MINUS 1
#include <upc.h>
#include <upc_nb.h>

#include "StaticVars.h"
#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"


char *myitoa2(int value, char *result, int base);

typedef struct oNolink_t oNolink_t;
struct oNolink_t {
    int  end1_id;                // ID of link's endpoint 1
    int  end2_id;                // ID of link's endpoint 2
    char end1_s;
    char end2_s;
    char link_type;     // Type of link: SPLINT or SPAN
    char lib_id;        // Library id
    int  f1;
    int  f2;
    int  f3;
    int  f4;
    int  f5;
};

typedef shared[] oNolink_t *oNoShared_link_ptr;

// over allocate per thread, especially for high concurrency / low per-thread counts
#define LINK_HEAPS_PER_THREAD(size) (512 + (5 * size / 2 + THREADS) / THREADS)

/* Link heap data structure */
typedef struct oNoLink_heap_t oNoLink_heap_t;
struct oNoLink_heap_t {
    shared[BS] oNoShared_link_ptr * link_ptr;                // Pointers to shared memory heaps
    shared[BS] int64_t * heap_indices;                       // Indices of remote heaps
    int64_t maxLinkHeapsPerThread;
};

/* Creates shared heaps that required for links hash table - IMPORTANT: This is a collective function */
int create_oNolink_heaps(int64_t size, oNoLink_heap_t *link_heap);

void destroy_oNolink_heaps(oNoLink_heap_t *link_heap);

/* Allocate local arrays used for book-keeping when using aggregated upc_memputs */
int allocate_oNolocal_buffs(oNolink_t **local_buffs, int64_t **local_index);

void free_oNolocal_buffs(oNolink_t **local_buffs, int64_t **local_index);

/* Adds a link to the shared link heap */
int add_oNolink_to_shared_heaps(oNolink_t *new_entry, int64_t hashval, int64_t *local_index, oNolink_t *local_buffs, oNoLink_heap_t link_heap, int64_t size);

/* Adds remaining links to the shared heaps. Should be called when all calls "add_oNolink_to_shared_heaps()" have been done */
int add_rest_oNolinks_to_shared_heaps(int64_t *local_index, oNolink_t *local_buffs, oNoLink_heap_t link_heap, int64_t size);

/* Data structure to store data regarding a link */
typedef struct oNoDatum_t oNoDatum_t;
struct oNoDatum_t {
    char        lib_id;
    int         f1;
    int         f2;
    int         f3;
    int         f4;
    int         f5;
    char        link_type; // Type of link: SPLINT or SPAN
    oNoDatum_t *next;      // Pointer to next entry in the same datum list
};

/* Essentially the key now is the combination: end1_id | end2_id | linktype */
typedef struct oNolinkList_t oNolinkList_t;
struct oNolinkList_t {
    int            end1_id;      // ID of link's endpoint 1
    int            end2_id;      // ID of link's endpoint 2
    char           end1_s;
    char           end2_s;
    oNoDatum_t *   data;            // Data list
    oNolinkList_t *next;            // Pointer to next entry in the same bucket
};

typedef struct oNolink_hash_table_t_ {
    int64_t         size;           /* the size of the table */
    oNolinkList_t **table;          /* the table elements */
} oNolink_hash_table_t;

/* Create a link_hash_table */
oNolink_hash_table_t *create_oNolink_hash_table(int64_t size);

int64_t oNolink_hash_val(int e1, int e2, char s1, char s2, int64_t linkhash_size);

/* Lookup function for link hash table */
oNolinkList_t *lookup_oNolink(oNolink_hash_table_t *hashtable, int e1, int e2, char s1, char s2, int64_t hashval);

/* Implement efficient memory allocator to avoid multiple malloc() calls */
int create_oNolocal_heaps(int64_t max_size, oNoDatum_t **datum_heap, oNolinkList_t **linkList_heap);

/* Add link data to the hashtable */
int add_oNolink_data(oNolink_hash_table_t *hashtable, oNolink_t *cur_link, int64_t *datum_heap_pos, oNoDatum_t *datum_heap, int64_t *link_heap_pos, oNolinkList_t *linkList_heap);

/* Free routines */
int free_oNolocal_heaps(oNoDatum_t **datum_heap, oNolinkList_t **linkList_heap);

int free_oNohash_table(oNolink_hash_table_t **hashtable);

#endif // ONO_HASH_H_
