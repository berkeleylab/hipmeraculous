#ifndef ONO_MAIN_H_
#define ONO_MAIN_H_

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <upc_nb.h>
#include <upc_tick.h>
#include <libgen.h>

//#define HIPMER_DEBUG_LEVEL 1

#include "optlist.h"
#include "upc_common.h"
#include "hash_funcs.h"
#include "timers.h"
#include "utils.h"
#include "upc_compatibility.h"
#include "oNoHash.h"
#include "oNoUtils.h"
#include "upc_output.h"

#define AUX_STRINGS_SIZE 100
#define MAX_LINE_SIZE 256
#define MAX_SCAFF_LINE_SIZE 128
#define TIE_FAST_SIZE 50


typedef struct {
    // set up in the parallel region, dependent on pair thres
    shared[BS] TieHeap * tieSharedHeap;
    // the results get put in the sharedScaffolds
    shared[BS] ScaffoldReportFiles * sharedScaffolds;
    // the sharing data structures are only allocated on the active threads (for shared memory heap)
    shared[BS] SharedFloatPtr * sharingDepths;
    shared[BS] sharedoNoObjectPtr * sharingoNoObjectPtrs;
    shared[BS] sharedScaffPtr * sharingScaffPtrs;
    shared[BS] int64_t * numObjects;
} per_pair_threshold_data_t;

#define MIN_ACTIVE_THREAD(p) (((p) - _minPairThreshold) * _pairBlock)

int calcN50(int *lens, int64_t num);

double find_y(double chat, int ell, int L);

double compute_log_likelihood(double c1, int ell1, double c2, int ell2, int L, double c1hat, double c2hat);

double find_MLE_fit(double c1hat, double c2hat, int ell1, int ell2, int L, double eps, double interval_begin, double interval_end, int is_inside_interval);
double compute_log_ratio(int ell1, int ell2, double c1hat, double c2hat, int L);

void compute_peak_depth(shared[BS] int64_t *depthHist, shared[BS] int64_t *maxDepth);

void run_serial(int pairThreshold);

static void run_parallel(void);

int oNo_main(int argc, char **argv);

#endif // ONO_MAIN_H_
