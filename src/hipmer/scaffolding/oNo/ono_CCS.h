#ifndef _ONO_CCS_H_
#define _ONO_CCS_H_

#if defined (__cplusplus)
extern "C" {
#endif

int oNo_CCS_main(int argc, char **argv);

#if defined (__cplusplus)
}
#endif

#endif // _ONO_CCS_H_
