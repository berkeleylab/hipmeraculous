#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <climits>

#include "../../common/poc_common.h"
#include "../../common/common.h"
#include "../../common/log.h"

#include "ono_common.h"


using namespace std;

int merSize;
int maxAvgInsSize;
int maxAvgInsSDev;
//bool CCS_reads_present;

objectsHash_t onoObjectsMap;
scaffsHash_t scaffInfo;

void initCommonStatics() {
    merSize = 0;
    maxAvgInsSize = 0;
    maxAvgInsSDev = 0;
    onoObjectsMap.clear();
    scaffInfo.clear();
}

///////////class End /////////

//def constructor
//def constructor
End::End(ObjectId _obj, int8_t _prime) :  endId(_obj, _prime), ties()
{
}

// use to construct a special flag object only
End::End(const End &copy, EndId::Flag newFlag) : endId(copy.endId, newFlag), ties(copy.ties)
{
    assert(newFlag != (int8_t) EndId::FLAG_NONE);
}
End::End(const End &copy) : endId(copy.endId), ties(copy.ties)
{
}

void End::flip_prime()
{
    endId.flip_prime();
}
EndLabel::Label End::getEndMark() const
{
    return endId.getEndMark();
}
void End::setEndMark(EndLabel::Label mark)
{
    endId.setEndMark(mark);
}
ObjectId End::getID() const
{
    return endId.getID();
}
int8_t End::getPrime() const
{
    return endId.getPrime();
}
void End::add_tie(EndId _te, int nLinks, int gapEst, int gapUncert, bool isSplint)
{
    EndTie tie(_te, nLinks, gapEst, gapUncert, isSplint);

    ties.push_back(tie);
    if (ties.back().tiedEndId != _te) {
        DIE("tiedEndId %s != EndId %s\n", ties.back().tiedEndId.toString().c_str(), _te.toString().c_str());
    }
}

void End::delete_tie(EndId _te)
{
    /* for (vector<EndTie>::iterator it = ties.begin() ; it != ties.end(); ++it) { */
    /*   if (it->tiedEndId == _te) { */
    /*  ties.erase(it); */
    /*  break;  // this is safe since a given tie will only appear once  */
    /*   } */
    /* } */

    ties.erase(
        std::remove_if(this->ties.begin(), this->ties.end(), [&](EndTie const & tie) {
                           return tie.tiedEndId == _te;
                       }),
        this->ties.end());
}

const EndTie *End::get_tie(EndId _te) const
{
    for (auto it = ties.begin(); it != ties.end(); ++it) {
        if (it->tiedEndId == _te) {
            return &(*it);
        }
    }
    return NULL;
}
EndTie *End::get_tie(EndId _te)
{
    return const_cast<EndTie *>(static_cast<const End&>(*this).get_tie(_te));
}
const EndTie *End::get_tie(const End& _te) const
{
    return get_tie(_te.endId);
}
EndTie *End::get_tie(const End& _te)
{
    return get_tie(_te.getEndId());
}

vector<EndTie> End::export_all_ties() const
{
    return ties;
}


void End::correct_gap_estimate(EndId teId, int newGapEst) const
{
    EndTie *tie = const_cast<EndTie *>(get_tie(teId)); // const cast okay, changing the gap estimate doesn't change the linkage

    assert(tie);
    int oldGapEst = tie->gapEstimate;
    tie->gapEstimate = newGapEst;
    DBG2N("In tie %s->%s, corrected gap estimate from  %d to %d\n", this->endId.toString().c_str(), tie->tiedEndId.toString().c_str(), (int)oldGapEst, (int)newGapEst);
}
void End::correct_gap_estimate(const End &_te, int newGapEst) const
{
    correct_gap_estimate(_te.getEndId(), newGapEst);
}

void End::print_ties()
{
    for (vector<EndTie>::iterator it = ties.begin(); it != ties.end(); ++it) {
        DBGN("\ttiedEndId: %s", it->getTiedEnd().toString().c_str());
        DBGN(" nGapLinks: %d gapEst|uncert: %d|%d\n", it->getNGapLinks(), it->gapEstimate, it->gapUncertainty);
    }
}

void End::print_end()
{
    cerr << "end: " << this->endId.getID() << "." << this->endId.getPrime() << " mark: " << this->endId.getEndMark() << " nTies: " << this->ties.size() << endl;
}

string End::toString()
{
    return this->endId.toString();
}



//////////// class Obj ///////////

// def constructor with parameterized Ends initilization
Obj::Obj(ObjectId _id, int _l, float _d) :    id(_id), len(_l), depth(_d), end5(id, 5), end3(id, 3)
{
    label = UNLABELED;
    parentId = LLONG_MIN;
    nCopiesMade = 0;
}
End &Obj::getEnd(int8_t prime)
{
    assert(prime == 5 || prime == 3);
    return prime == 5 ? end5 : end3;
}
End &Obj::getEnd(EndId endId)
{
    if (endId.getID() != id) {
        DIE("Improper use of Obj.getEnd(EndId)... Ids must match this=%lld vs endID=%s\n", (lld)id, endId.toString().c_str());
    }
    return getEnd(endId.getPrime());
}
int16_t Obj::incrementCopy()
{
    return ++nCopiesMade;
}



/////////// class Scaff ///////////

Scaf::Scaf(int64_t id) :  scafId(id)
{
}

void Scaf::push_contig(string name, char ori, int start, int end, float dep)
{
    Scaf::contig_t c;

    c.contName = name;
    c.ori = ori;
    c.startCoord = start;
    c.endCoord = end;
    c.depth = dep;
    Contigs.push_back(c);
}

void Scaf::push_gap(int16_t size, int16_t uncert)
{
    assert(Gaps.size() == Contigs.size() - 1); // a gap can only be added following a contig
    Scaf::gap_t g;
    g.gapSize = size;
    g.gapUncertainty = uncert;
    Gaps.push_back(g);
}

void Scaf::reverse_order_ori()
{
    std::reverse(this->Contigs.begin(), this->Contigs.end());
    std::reverse(this->Gaps.begin(), this->Gaps.end());
    for (std::vector<contig_t>::iterator it = Contigs.begin(); it != Contigs.end(); ++it) {
        it->ori = (it->ori == '+') ? '-' : '+';
    }
}

string Scaf::form_contig_string(int idx)
{
    // -Contig4783871  1       105     2.000000
    return Contigs[idx].ori + Contigs[idx].contName + "\t"
           + std::to_string(Contigs[idx].startCoord) + "\t"
           + std::to_string(Contigs[idx].endCoord) + "\t"
           + std::to_string(Contigs[idx].depth) + "\n";
}

string Scaf::form_copyTig_string(int idx, int ver)
{
    // -Contig4783871.cp2  1       105     2.000000
    string copyVersion = std::to_string(ver);

    return Contigs[idx].ori + Contigs[idx].contName + ".cp" + copyVersion + "\t"
           + std::to_string(Contigs[idx].startCoord) + "\t"
           + std::to_string(Contigs[idx].endCoord) + "\t"
           + std::to_string(Contigs[idx].depth) + "\n";
}

string Scaf::form_gap_string(int idx)
{
    return std::to_string(Gaps[idx].gapSize) + "\t" + std::to_string(Gaps[idx].gapUncertainty) + "\n";
}


// class EndIds

EndIds::EndIds(const Obj& obj)
{
    end[0] = obj.end3.getEndId();
    end[1] = obj.end5.getEndId();
}


// comparators

bool by_length_depth::operator() (const sortedByElement_t a, const sortedByElement_t b) const
{
    if (a.len != b.len) {
        return a.len > b.len;
    } else if (a.depth != b.depth) {
        return a.depth > b.depth;
    } else {
        return a.id < b.id;
    }
}


// compares gap estimates and returns the closest;  if equal returns the SHORTER;  if equal, returns the least id
bool by_dist_len::operator() (const EndTie& a, const EndTie& b) const
{
    if (a.gapEstimate != b.gapEstimate) {
        return a.gapEstimate < b.gapEstimate;
    } else {
        ObjectId id_a = a.getTiedEnd().endId.getID();
        ObjectId id_b = b.getTiedEnd().endId.getID();
        Obj &obja = onoObjectsMap.at(id_a), &objb = onoObjectsMap.at(id_b);
        if (obja.len != objb.len) {
            return obja.len < objb.len;
        } else {
            return id_a < id_b;
        }
    }
}

// compares gap estimates and returns the closest;  if equal, looks at the depths and returns the HIGHEST; if equal, returns the least id
bool by_dist_depth::operator() (const EndTie& a, const EndTie& b) const
{
    if (a.gapEstimate != b.gapEstimate) {
        return a.gapEstimate < b.gapEstimate;
    } else {
        ObjectId id_a = a.getTiedEnd().endId.getID();
        ObjectId id_b = b.getTiedEnd().endId.getID();
        Obj &obja = onoObjectsMap.at(id_a), &objb = onoObjectsMap.at(id_b);
        if (obja.depth != objb.depth) {
            return obja.depth > objb.depth;
        } else {
            return id_a < id_b;
        }
    }
};

bool by_end_name::operator() (const EndId &a, const EndId &b) const
{
    return a.getID() < b.getID();
}

bool by_path_len::operator() (const path_info_t &a, const path_info_t &b) const
{
  return a.distanceTraversed < b.distanceTraversed;
}

// common ono functions


bool ifsIsEmpty(ifstream& in){
  in.seekg(0,ios::end);
  if( in.tellg() == 0)
    return true;
  in.clear();
  in.seekg(0,ios::beg);
  return false;
}

bool fileIsEmpty(FILE* fp){
  fseek(fp, 0, SEEK_END);  // goto end of file
  if (ftell(fp) == 0)
    return true;
  fseek(fp, 0, SEEK_SET); // goto begin of file
  return false;
}



ObjectId create_copy_object(Obj *_object, copyTracker_t& copyTracker, bool existsSrfFile)
{
    // Creates a new object on the bases of the original input contig/scaffold.
    // The copy-object gets assigned a negative id and is added to the main onoObjectsMap container.

    ObjectId id = -1 * (copyTracker.size() + 1);
    Obj _newObj(id, _object->len, _object->depth);

    _newObj.parentId = _object->id;
    _newObj.label = Obj::COPY_TIG;
    onoObjectsMap.insert(std::make_pair(id, _newObj));

    copyTracker[id] = _object->incrementCopy(); // records that a) this copy-object is an n'th copy of its parent, and b) that the parent object has n copies made from it


    if (existsSrfFile) {
        //# add a new Scaff object to reflect the copy

        Scaf *_newScaff = new Scaf(id);
        _newScaff->Contigs = scaffInfo.at(_object->id)->Contigs; // original contig names get copied here; when printing out the scaffold we will label them as copies
        _newScaff->Gaps = scaffInfo.at(_object->id)->Gaps;

        scaffInfo.insert(std::make_pair(id, _newScaff));

        DBG2N("copied %lld contigs into new copy-scaffold:\n", (lld)_newScaff->Contigs.size());
    }

    DBG2N("id=%lld copy #%d of parent object %lld)\n", (lld)id, (int)copyTracker[id], (lld)onoObjectsMap.at(id).parentId);
    return id;
}


void copy_tie_info(const End *_sourceEndA, End *_sourceEndB, End *_tiedToEnd)
{
    //In _sourceEndA, finds a tie pointing to _tiedToEnd and copies this tie to _sourceEndB
    assert(_sourceEndB);
    assert(_tiedToEnd);

    const EndTie tieToCopy = *(_sourceEndA->get_tie(_tiedToEnd->endId));
    // assign to  myEnd
    _sourceEndB->ties.push_back(tieToCopy);
    //add to tiedToEnd a new tie to _sourceEndB
    EndTie tieBackToB = *(_tiedToEnd->get_tie(_sourceEndA->endId));
    tieBackToB.tiedEndId = _sourceEndB->endId;
    _tiedToEnd->ties.push_back(tieBackToB);

    assert(_sourceEndB->get_tie(_tiedToEnd->endId));
    assert(_tiedToEnd->get_tie(_sourceEndB->endId));
}


void reassign_tie_info(End *_sourceEndA, End *_sourceEndB, End *_tiedToEnd)
{
    //In _sourceEndA, finds a tie pointing to _tiedToEnd and reassigns this tie to _sourceEndB, revoving it from _sourceEndA

    EndTie *_tieToReassign = _sourceEndA->get_tie(_tiedToEnd->endId);

    // assign to  myEnd
    _sourceEndB->ties.push_back(*_tieToReassign);

    //remove from sourceEnd
    _sourceEndA->delete_tie(_tiedToEnd->endId);

    //correct tiedToEnd so that it's tie now points to myEnd
    EndTie *_tieToCorrect = _tiedToEnd->get_tie(_sourceEndA->endId);
    _tieToCorrect->tiedEndId = _sourceEndB->endId;
}




bool mutual_unique_best(const End *_e1, const End *_e2, suspendedHash_t& suspended, tiedEndsHash_t& bestTie, tiedEndListsH_t& bestTiedBy)
{
    Obj &_o1 = onoObjectsMap.at(_e1->endId.getID());
    Obj &_o2 = onoObjectsMap.at(_e2->endId.getID());

    if (suspended.count(_o1.id) || suspended.count(_o2.id)) {
        return false;
    }

    if (!bestTie.count(_e1->endId) || !bestTie.count(_e2->endId)) {
        return false;
    }

    if (bestTie.at(_e1->endId) != _e2->endId || bestTie.at(_e2->endId) != _e1->endId) {
        return false;
    }

    if (!bestTiedBy.count(_e1->endId) || !bestTiedBy.count(_e2->endId)) {
        return false;
    }


    size_t nBTBs1 = bestTiedBy.at(_e1->endId).size();
    size_t nBTBs2 = bestTiedBy.at(_e2->endId).size();
    if (nBTBs1 == 1 && nBTBs2 == 1 &&
        bestTiedBy.at(_e1->endId)[0] == _e2->endId && bestTiedBy.at(_e2->endId)[0] == _e1->endId) {
        //TODO:  test if this is a reasonable requirement on complex/large data sets;
        //      A less stringent alternative is to require that the end is *present* in the other end's btb vector
        return true;
    } else {
        //    cerr << "WARNING:  end serves as best-tie to ends other than it's own best-tie ! " << endl;
        return false;
    }
}


End &other_end(ObjectId objID, int prime)
{
    if (prime == 3) {
        return onoObjectsMap.at(objID).end5;
    } else {
        return onoObjectsMap.at(objID).end3;
    }
}
End &other_end(const End &_end)
{
    return other_end(_end.getID(), _end.getPrime());
}
End &other_end(EndId endId)
{
    return other_end(endId.getID(), endId.getPrime());
}

bool is_splinted(const End &_e1, const End &_e2)
{
    const EndTie *tie = _e1.get_tie(_e2.endId);

    if (tie == NULL) {
        return false;
    }
    if (tie->isSplinted()) {
        return true;
    }
    return false;
}
bool is_splinted(const End &_e1, EndId endId2)
{
    const EndTie *tie = _e1.get_tie(endId2);

    if (tie == NULL) {
        return false;
    }
    if (tie->isSplinted()) {
        return true;
    }
    return false;
}
bool is_splinted(const End *_e1, const End *_e2)
{
    return is_splinted(*_e1, *_e2);
}
bool is_splinted(const End *_e1, EndId endId2)
{
    return is_splinted(*_e1, endId2);
}

vector<string> tokenize(const string& in, string sep)
{
    string::size_type b = 0;

    vector<string> result;

    while ((b = in.find_first_not_of(sep, b)) != string::npos) {
        auto e = in.find_first_of(sep, b);
        result.push_back(in.substr(b, e - b));
        b = e;
    }
    return result;
}


// forward def for parse_linkFile_and_build_ties
void build_ties_i(char *linkedEndsStr, vector<link_info_t> &linksV, int pairThreshold, int maxUncertainty, bool existsSrfFile);

#define AVG_LINKS_LINE_SIZE 53
int64_t parse_linkFile_and_build_ties(string linksFile, int pairThreshold, int maxUncertainty, bool existsSrfFile)
{
    LOGF("Parsing linkFile %s\n", linksFile.c_str());

    ifstream infile(linksFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", linksFile.c_str());
    }

    if( ifsIsEmpty(infile)) {
      DBG("Empty LINKS file");
      return 0;
    }

    int64_t numLinks = 0;
    int64_t estimatedLinks = get_file_size(linksFile.c_str()) / AVG_LINKS_LINE_SIZE;
    //links.reserve( estimatedLinks );
    int64_t linksCount = 0;
    char lastEnd[80];
    lastEnd[0] = '\0';
    const int maxLine = 180;
    char l[maxLine];
    vector<link_info_t> linksV;
    vector<char *> words;
    EndIds linkedEnds, lastLinkedEnds;
    bool isFirst = true;

    vector<char *> supportV;

    while (infile.getline(l, maxLine - 1)) {
        words.clear();
        char *savePtr = NULL;
        char *token = strtok_r(l, "\t", &savePtr);
        while (token != NULL) { // read tab delimited tokens one by one
            words.push_back(token);
            token = strtok_r(NULL, "\t", &savePtr);
        }
        assert(words.size() > 3);
        link_info_t linkInfo;
        linkInfo.type = (strcmp(words[0], "SPLINT") == 0) ? 1 : 2;
        if (linkInfo.type == 2) {
            assert(strcmp(words[0], "SPAN") == 0);
        }

        char *ends = words[1];

        supportV.clear();
        savePtr = NULL;
        token = strtok_r(words[2], "|", &savePtr);
        while (token != NULL) { // read '|' deliminated tokens
            supportV.push_back(token);
            token = strtok_r(NULL, "|", &savePtr);
        }

        if (supportV.size() > 0) {
            linkInfo.support1 = atoi(supportV[0]);
        }
        if (supportV.size() > 1) {
            linkInfo.support2 = atoi(supportV[1]);
        }
        if (supportV.size() > 2) {
            linkInfo.support3 = atoi(supportV[2]);
        }
        linkInfo.gapEstimate = atoi(words[3]);

        if (words.size() > 4) {
            linkInfo.gapUncertainty = atoi(words[4]);
        }
//    links[ends].push_back(linkInfo);
        if (lastEnd[0] != '\0' && strcmp(ends, lastEnd) != 0) {
            // new end, process the last one
            if (!linksV.empty()) {
                linksCount++;
                build_ties_i(lastEnd, linksV, pairThreshold, maxUncertainty, existsSrfFile);
            }
            linksV.clear();
        }
        // append to linksV
        linksV.push_back(linkInfo);
        strcpy(lastEnd, ends);
    }
    if (lastEnd[0] != '\0' && !linksV.empty()) {
        linksCount++;
        build_ties_i(lastEnd, linksV, pairThreshold, maxUncertainty, existsSrfFile);
    }
    DBG("end-pairs linked: %lld\n", (lld)linksCount);
    return linksCount;
}


void build_ties_i(char *linkedEndsStr, vector<link_info_t> &linksV, int pairThreshold, int maxUncertainty, bool existsSrfFile)
{
    link_info_t *maxLikelihoodSplint = NULL;
    int maxSplintCount = 0;

    const link_info_t *minUncertaintySpan = NULL;
    int minSpanUncertainty = maxUncertainty;
    int minUncertaintySpanSupport = 0;


    for (auto it = linksV.begin(); it != linksV.end(); ++it) {
        if (it->type == 1) {                 // SPLINT
            int nUsedSplints = it->support1; // x in "x|y|z"

            //	  if (nUsedSplints > maxSupport)
            //	    maxSupport = nUsedSplints;

            if (nUsedSplints >= pairThreshold) {
                if (nUsedSplints > maxSplintCount) {
                    maxLikelihoodSplint = &(*it);
                    maxSplintCount = nUsedSplints;
                }
            }
        } else if (it->type == 2) { // SPAN
            int nUsedSpans = it->support2; // i.e. y in "x|y"

            //	  if (nUsedSpans > maxSupport)
            //	    maxSupport = nUsedSpans;

            if (nUsedSpans >= pairThreshold) {
                if (it->gapUncertainty <= minSpanUncertainty) {
                    minUncertaintySpan = &(*it);
                    minSpanUncertainty = it->gapUncertainty;
                    minUncertaintySpanSupport = nUsedSpans;
                }
            }
        } else {
            DIE("Invalid linkType in link: %s", linkedEndsStr);
        }
    }

    int minimumGapSize = -(merSize - 2);
    int nSplintGap;
    int splintGapEstimate;
    bool splintGapEst_defined = false;
    //      int maxSplintError = 2;
    //      bool splintAnomaly = false;
    if (maxLikelihoodSplint) {
        //	splintGapEstimate = (maxLikelihoodSplint->gapEstimate < minimumGapSize) ? minimumGapSize : maxLikelihoodSplint->gapEstimate;
        if (maxLikelihoodSplint->gapEstimate < minimumGapSize) {
            DBGN("Rejected SPLINT link %s: maxLikelihoodSplint gap size (%d) < minimumGapSize\n", linkedEndsStr, maxLikelihoodSplint->gapEstimate);
        } else {
            splintGapEstimate = maxLikelihoodSplint->gapEstimate;
            splintGapEst_defined = true;
            nSplintGap = maxSplintCount;
        }
    }

    int nSpanGap;
    int spanGapEstimate;
    int spanGapUncertainty;
    bool spanGapEst_defined = false;
    //      int maxSpanZ = 3;
    //      boot spanAnomaly = false;
    if (minUncertaintySpan) {
      if (minUncertaintySpan->gapEstimate + minSpanUncertainty  < minimumGapSize ) {
	DBGN("Rejected SPAN link %s: minUncertaintySpan gap size (%d) + minSpanUncertainty < minimumGapSize\n", linkedEndsStr, minUncertaintySpan->gapEstimate);
      } else {
	spanGapEstimate = (minUncertaintySpan->gapEstimate < minimumGapSize) ? minimumGapSize : minUncertaintySpan->gapEstimate;
	spanGapEst_defined = true;
	spanGapUncertainty = (minSpanUncertainty < 1) ? 1 : minSpanUncertainty;
	nSpanGap = minUncertaintySpanSupport;
      }
    }

    EndIds linkedEnds(linkedEndsStr, existsSrfFile);
    Obj &obj0 = onoObjectsMap.at(linkedEnds.end[0].getID());
    Obj &obj1 = onoObjectsMap.at(linkedEnds.end[1].getID());
    End& _end1 = (linkedEnds.end[0].getPrime() == 3) ? obj0.end3 : obj0.end5;
    End& _end2 = (linkedEnds.end[1].getPrime() == 3) ? obj1.end3 : obj1.end5;

    // based on the collected splint/span info, determine the overall gap size estimate & uncertainty

    int16_t gapEstimate;
    int16_t gapUncertainty;
    int nGapLinks;
    bool isSplinted = false;

    if (splintGapEst_defined) {
        gapEstimate = splintGapEstimate;
        gapUncertainty = 1;
        nGapLinks = nSplintGap;
        isSplinted = true;
    } else if (spanGapEst_defined) {
        gapEstimate = spanGapEstimate;
        gapUncertainty = spanGapUncertainty;
        nGapLinks = nSpanGap;
    } else {
        DBG2N("Rejected tie: gap estimate not defined for %s\n", linkedEndsStr);
        return;
    }

    _end1.add_tie(_end2.endId, nGapLinks, gapEstimate, gapUncertainty, isSplinted);
    _end2.add_tie(_end1.endId, nGapLinks, gapEstimate, gapUncertainty, isSplinted);
}


void place_suspended(suspendedHash_t& suspended, tiedEndsHash_t& bestTie, tiedEndListsH_t& bestTiedBy, copyTracker_t& copyObjTracker, bool existsSrfFile)
{
    // insert suspenced pieces between mutually uniquely best-tied objects.  Existing bestTie and bestTiedBy entries are overwritten  (this is different from ono7!)

    int nSusp = suspended.size();
    int nInserted = 0;

    LOGF("Inserting %lld suspended pieces..\n", (lld)nSusp);

    // onoObjectsMap may change soon, so sortedByLen may become invalidated soon, track the Ids for those that will be needed
    vector<ObjectId> ids;
    for (auto &kv : onoObjectsMap) {
        ObjectId id = kv.first;
        if (suspended.find(id) == suspended.end()) {
	  continue;
        }
        ids.push_back(id);
    }
    std::sort(ids.rbegin(), ids.rend());    // sort ids for reproducibility  (reverse order to ensure copy-tigs are attempted last)
    
    for (ObjectId id : ids) {
        Obj *piece = &onoObjectsMap.at(id);

        int nInsertedThisPiece = 0;
        End &_end5 = piece->end5;
        End &_end3 = piece->end3;

        if (_end3.getEndMark() == EndLabel::NO_TIES || _end5.getEndMark() == EndLabel::NO_TIES) {  // NO_TIES
            DBG2N("UNINSERTED: %lld (no ties at one or both ends)\n", (lld)piece->id);
            continue;
        }

        vector<EndId> tiedEnds5;
        for (EndTie t : _end5.ties) {
            tiedEnds5.push_back(t.tiedEndId);
        }

        unordered_map<EndId, bool> tiedEnds3;
        for (EndTie t : _end3.ties) {
            tiedEnds3.insert(std::make_pair(t.tiedEndId, true));
        }

        if (bestTie.count(_end5.endId) || bestTie.count(_end3.endId)) { // best ties exist
            DBG2N("UNINSERTED: %lld (best ties exist)\n", (lld)piece->id);
            continue;
        }

        map<EndId, EndId, by_end_name> check;              // ordered map to ensure reproducibility
        for (auto _te5 : tiedEnds5) {
            if (tiedEnds3.find(_te5) != tiedEnds3.end()) { // same end tied to both 5' and 3';
                continue;
            }

            auto te5InBestTie = bestTie.find(_te5);
            if (te5InBestTie != bestTie.end() && tiedEnds3.count(bestTie.at(_te5))) {
                check.insert(std::make_pair(_te5, te5InBestTie->second));
                DBG2N("check: %s %s\n", _te5.toString().c_str(), te5InBestTie->second.toString().c_str());
            }
        }

        vector<EndId> v5;
        vector<EndId> v3;
        unsigned int nValidSuspensions = 0;

        for (std::pair<EndId, EndId>element : check) {
            End &_te5 = getEnd(element.first);
            End &_te3 = getEnd(element.second);
            if (mutual_unique_best(&_te5, &_te3, suspended, bestTie, bestTiedBy)) {
                // this is a valid pair of ends between which this piece can be inserted
                v5.push_back(_te5.endId);
                v3.push_back(_te3.endId);
                nValidSuspensions++;
                DBG2N("mub: %s %s\n", _te5.toString().c_str(), _te3.toString().c_str());
            }
        }

        if (!nValidSuspensions) {
            DBG2N("UNINSERTED: %lld (no valid suspensions)\n", (lld)piece->id);
            continue;
        }
        for (size_t i = 0; i < nValidSuspensions; i++) {
            //          # for suspended collapsed repeats, multiple valid insertions are possible, and we handle this by inserting copy-objects of the suspended candidate

            EndId _le5 = v5[i];
            EndId _le3 = v3[i];
            End *_te5 = NULL, *_te3 = NULL; // assign later, after possible create_copy_object that would invalidate it
            Obj *pieceToInsert = piece;

            if (nInsertedThisPiece > 0) {
                //Create a copy object to insert and reassign ties
                ObjectId copyID = create_copy_object(piece, copyObjTracker, existsSrfFile);
                pieceToInsert = &onoObjectsMap.at(copyID);
                piece = &onoObjectsMap.at(id); // need to get again in case objects map was resized

                //To avoid creating new tie conflicts, the reassignment involves only ties
                // to/from the two "suspender" ends.  This means that other "suspendEE" pieces
                // that could potentially be inserted here may end up uninserted due to not meeting
                // valid suspension criteria. In other words, if an copy-object must be inserted, it will be the only insertion overall.

                _te5 = &getEnd(_le5);
                _te3 = &getEnd(_le3);
                reassign_tie_info(&(piece->end5), &(pieceToInsert->end5), _te5);
                reassign_tie_info(&(piece->end3), &(pieceToInsert->end3), _te3);
		piece->label = Obj::COPY_TIG;
		piece->depth /= 2;
		pieceToInsert->depth = piece->depth;
		
            } else {
                _te5 = &getEnd(_le5);
                _te3 = &getEnd(_le3);
            }


            bestTie[pieceToInsert->end5.endId] = _te5->endId;
            auto & te5InBestTiedBy = bestTiedBy[_te5->endId];
            te5InBestTiedBy.clear();
            te5InBestTiedBy.push_back(pieceToInsert->end5.endId);

            bestTie[_te5->endId] = pieceToInsert->end5.endId;
            auto & end5InBestTiedBy = bestTiedBy[pieceToInsert->end5.endId];
            end5InBestTiedBy.clear();
            end5InBestTiedBy.push_back(_te5->endId);

            bestTie[pieceToInsert->end3.endId] = _te3->endId;
            auto & te3InBestTiedBy = bestTiedBy[_te3->endId];
            te3InBestTiedBy.clear();
            te3InBestTiedBy.push_back(pieceToInsert->end3.endId);

            bestTie[_te3->endId] = pieceToInsert->end3.endId;
            auto & end3InBestTiedBy = bestTiedBy[pieceToInsert->end3.endId];
            end3InBestTiedBy.clear();
            end3InBestTiedBy.push_back(_te3->endId);

            DBG2N("SUSPENDED-INSERTED: %lld\n", (lld)pieceToInsert->id);
            nInserted++; nInsertedThisPiece++;
        }
        suspended.erase(piece->id);
    }
    LOGF("Done. Inserted %lld suspensions. (%lld total suspended pieces)\n", (lld)nInserted, (lld)nSusp);
}


void lock_ends(endLocks_t &endLocks, tiedEndsHash_t &bestTies,
               tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, const objectsHash_t &objects)
{
    //	cerr << "Locking ends with no competing ties.." << endl;

    End *end = NULL;

    LOGF("lock_ends %lld...\n", (lld)bestTiedBy.size());
    int64_t count = 0, progressCt = bestTiedBy.size() / 100;
    for (auto & endTiePair : bestTiedBy) {
        if (progressCt && ++count % progressCt == 0) {
            LOGF("lock_ends %lld of %lld (%0.1f %%)\n", (lld)count, (lld)bestTiedBy.size(), 100.0 * count / bestTiedBy.size());
        }
        DBG2("endTie=%s\n", endTiePair.first.toString().c_str());
        end = &getEnd(endTiePair.first);

        DBG2("endTie=%s\n", end->endId.toString().c_str());
        auto endInEndLocks = endLocks.find(end->endId);
        if (endInEndLocks != endLocks.end()) {
            continue;
        }
        //        auto ties = bestTiedBy[end];
        auto ties = endTiePair.second;

        const Obj &obj = objects.find(end->getID())->second;
        if (suspended.find(obj.id) != suspended.end()) {
            DBG2("%lld is suspended... skipping\n", (lld)end->getID());
            continue;
        }

        End *bestTie = NULL;
        auto endInBestTies = bestTies.find(end->endId);
        if (endInBestTies != bestTies.end()) {
            DBG2("bestTie[%s]=%s\n", end->endId.toString().c_str(), endInBestTies->second.toString().c_str());
            bestTie = &getEnd(endInBestTies->second);
        } else {
            DBG2("%lld has no best ties... skipping\n", (lld)end->getID());
            continue;
        }

        bool existsReciprocalBestTie = false;

        if (ties.size() > 1) {
            DBG2("WARNINIG: %d best_tied_by's for end %lld.%d \n", ties.size(), (lld)end->getID(), end->getPrime());
        }
        for (auto & tie : ties) {
            if (tie == bestTie->endId) {
                existsReciprocalBestTie = true;
                break;
            }
        }
        if (existsReciprocalBestTie) {
            endLocks[end->endId] = EndIdAndFlag(bestTie->endId);
            DBG2("locked ends %lld.%d  <--> %lld.%d \n", (lld)end->getID(), (int)end->getPrime(), (lld)bestTie->getID(), (int)bestTie->getPrime());
            endLocks[bestTie->endId] = EndIdAndFlag(end->endId);
            DBG2("locked ends %lld.%d  <--> %lld.%d \n", (lld)bestTie->getID(), (int)bestTie->getPrime(), (lld)end->getID(), (int)end->getPrime());
        } else {
            DBG2("DIVERGENCE: %lld.%d\n", (lld)end->getID(), end->getPrime());
            endLocks[end->endId] = EndIdAndFlag(end->endId, EndId::FLAG_DIVERGENCE); // std::shared_ptr<End>(new End(End::FLAG_DIVERGENCE));
        }
    }
    LOGF("Finished lock_ends\n");
}


int64_t build_scaffolds(endLocks_t& endLocks, const string srfOutFilename, bool existsSrfFile, copyTracker_t& copyTracker, int srfOffset)
{
    enum PathFlag { PATH_CONVERGENCE, PATH_DIVERGENCE, PATH_EXTENSION, PATH_TERMINATION };

    // we use the objectId as the key for local hashes where an Object is the key
    std::unordered_set<ObjectId> visitedObjects;
    visitedObjects.reserve(onoObjectsMap.size());

    LOGF("build_scaffolds %lld\n", (lld)onoObjectsMap.size());
    int64_t count = 0, progressCt = onoObjectsMap.size() / 100;
    ofstream srfOut;
    srfOut.open(srfOutFilename + ".tmp"); // support checkpointing and only create the output file when successful
    assert(srfOut.is_open());
    int initBuffer = 8192;
    string newScaffReport;
    newScaffReport.reserve(initBuffer);

    long unsigned int newScaffoldId = srfOffset;
    for (auto objPair : onoObjectsMap) {
        if (progressCt && ++count % progressCt == 0) {
            LOGF("building %lld of %lld %0.1f %%\n", (lld)count, (lld)onoObjectsMap.size(), 100.0 * count / onoObjectsMap.size());
        }
        auto obj = &objPair.second;
        if (visitedObjects.find(obj->id) != visitedObjects.end()) { // TODO shouldn't this be an insert().second == false? ... why not insert obj->id?  -RE
            continue;
        }
        //		DBGN("Seeding scaffold with obj %lld\n", obj->id);

        End *end = &obj->end5;
        PathFlag preState = PATH_TERMINATION;

        std::unordered_set<ObjectId> loopCheck;
        while (true) {
            if (loopCheck.insert(end->getID()).second == false) {
                break;
            }

            if (endLocks.find(end->endId) != endLocks.end()) {
                auto next = endLocks[end->endId];
                EndId::Flag flag = next.getFlag();
                if (flag == EndId::FLAG_CONVERGENCE || flag == EndId::FLAG_DIVERGENCE) {
                    preState = flag == EndId::FLAG_CONVERGENCE ? PATH_CONVERGENCE : PATH_DIVERGENCE;
                } else {
                    assert(flag == EndId::FLAG_NONE); // End should be a valid obj with no special flags

                    End *tmpend = &getEnd(next);
                    end = &other_end(*tmpend);

                    preState = PATH_EXTENSION;
                }
            } else {
                preState = PATH_TERMINATION;
            }
        }

        // at this point:
        // preState = TERMINATION / DIVERGENCE / CONVERGENCE
        // end->getID() points to the last valid End visited
        auto nextObject = &onoObjectsMap.at(end->getID());

        loopCheck.clear();

        DBG2("Scaffold%lld :: preState %d; Contig%lld (%d) ", (lld)newScaffoldId, preState, (lld)end->getID(), end->getPrime());

        int inScaffold = 0;
        int nextEndPrime = end->getPrime() == 3 ? 5 : 3;
        int nextGap = 0;
        int nextGapUncertainty = 0;
        auto prevObject = nextObject;
        auto prevEndPrime = nextEndPrime;

        long scaffCoord = 1;
        long scaffContigIndex = 1;

        while (true) {
            if (loopCheck.insert(nextObject->id).second == false) {
                DBG2("LOOP\n");
                break;
            }
            //			DBG2("\tContig%lld\n", (lld) nextObject->id);
            visitedObjects.insert(nextObject->id);
            unsigned long nextObjectLen = nextObject->len;

            char objectOri = '+';
            if (nextEndPrime == 5) {
                objectOri = '-';
            }

            if (inScaffold) {
                newScaffReport += "Scaffold";
                newScaffReport += std::to_string(newScaffoldId);
                newScaffReport += "\tGAP";
                newScaffReport += std::to_string(scaffContigIndex);
                newScaffReport += "\t";
                newScaffReport += std::to_string(nextGap);
                newScaffReport += "\t";
                newScaffReport += std::to_string(nextGapUncertainty);
                newScaffReport += "\n";

                scaffCoord += nextGap;
                scaffContigIndex++;
            }

            if (existsSrfFile) {
                // objects are scaffolds from previous srf
                Scaf nextObjectScaffInfo = *scaffInfo.at(nextObject->id); //shallow cp
                if (objectOri == '-') {
                    nextObjectScaffInfo.reverse_order_ori();
                }
                bool is_a_copy = false;
                int cp_ver;
                if (nextObject->id < 0) { // a copy-object: print all contigs as 'parent_id.cp[version]'
                    is_a_copy = true;
		    assert( copyTracker.count(nextObject->id));
                    cp_ver = copyTracker[nextObject->id];
                }

                // correct coordinates according to new scaffold layout
                for (size_t idx = 0; idx < nextObjectScaffInfo.Contigs.size(); ++idx) {
                    Scaf::contig_t *c = &nextObjectScaffInfo.Contigs[idx];
                    int cLen = c->endCoord - c->startCoord + 1;
                    c->startCoord = scaffCoord;
                    c->endCoord = scaffCoord + cLen - 1;
                    scaffCoord += cLen;

                    // print CONTIG line
                    newScaffReport += "Scaffold" + std::to_string(newScaffoldId) + "\tCONTIG" + std::to_string(scaffContigIndex) + "\t";
                    if (is_a_copy) {
                        newScaffReport += nextObjectScaffInfo.form_copyTig_string(idx, cp_ver);
                    } else {
                        newScaffReport += nextObjectScaffInfo.form_contig_string(idx);
                    }
                    // print GAP line
                    if (&nextObjectScaffInfo.Contigs[idx] != &nextObjectScaffInfo.Contigs.back()) {
                        newScaffReport += "Scaffold" + std::to_string(newScaffoldId) + "\tGAP" + std::to_string(scaffContigIndex) + "\t";
                        Scaf::gap_t *g = &nextObjectScaffInfo.Gaps[idx];
                        scaffCoord += g->gapSize;
                        newScaffReport += nextObjectScaffInfo.form_gap_string(idx);

                        scaffContigIndex++;
                    }
                }
            } else { // objects are contigs
                auto contigOri = objectOri;
                auto contigStartCoord = scaffCoord;
                auto contigEndCoord = scaffCoord + nextObjectLen - 1;
                auto depth = nextObject->depth;

                char contigOutName[32];
                //string contigOutName = "";
                const char *prefix = "";
#ifdef CONTIG_ID_PREFIX
                prefix = CONTIG_ID_PREFIX;
                //contigOutName = CONTIG_ID_PREFIX;
#endif
                if (nextObject->id < 0) { //copy-object - print out as 'parent_id.cp[version]'
                    sprintf(contigOutName, "%s%lld.cp%d", prefix, (lld)nextObject->parentId, copyTracker[nextObject->id]);
                    //string copyVersion = std::to_string(copyTracker[nextObject->id]);
                    //contigOutName += std::to_string(nextObject->parentId) + ".cp" + copyVersion;
                } else {
                    sprintf(contigOutName, "%s%lld", prefix, (lld)nextObject->id);
                    //contigOutName += std::to_string(nextObject->id);
                }

                newScaffReport += "Scaffold";
                newScaffReport += std::to_string(newScaffoldId);
                newScaffReport += "\t";
                newScaffReport += "CONTIG";
                newScaffReport += std::to_string(scaffContigIndex);
                newScaffReport += "\t";
                newScaffReport += contigOri;
                newScaffReport += contigOutName;
                newScaffReport += "\t";
                newScaffReport += std::to_string(contigStartCoord);
                newScaffReport += "\t";
                newScaffReport += std::to_string(contigEndCoord);
                newScaffReport += "\t";
                newScaffReport += std::to_string(depth);
                newScaffReport += "\n";

                scaffCoord += nextObjectLen;
            }

            inScaffold = true;
            auto _end = nextObject->getEnd(nextEndPrime);

            if (endLocks.find(_end.endId) != endLocks.end()) {
                auto next = endLocks[_end.endId];
                EndId::Flag flag = next.getFlag();
                if (flag == EndId::FLAG_CONVERGENCE || flag == EndId::FLAG_DIVERGENCE) {
                    break;
                } else {
                    prevObject = nextObject;
                    prevEndPrime = nextEndPrime;

                    nextObject = &onoObjectsMap.at(next.getID());
                    auto nextTie = nextObject->getEnd(next.getPrime()).get_tie(_end.endId);
                    nextGap = nextTie->gapEstimate;
                    nextGapUncertainty = nextTie->gapUncertainty;
                    nextEndPrime = next.getPrime();
                    nextEndPrime = (5 == nextEndPrime) ? 3 : 5;
                }
            } else {
                DBG2N("Scaffold%lld :: TERMINATION; Contig%lld (%d) \n", (lld)newScaffoldId, (lld)nextObject->id, nextEndPrime);
                break;
            }
        }

        if (newScaffReport.size() > initBuffer / 2) {
            srfOut << newScaffReport;
            newScaffReport.clear();
        }
        newScaffoldId++;
    }

    if (newScaffReport.size() > 0) {
        srfOut << newScaffReport;
        newScaffReport.clear();
    }
    srfOut.close();
    if ( rename(string(srfOutFilename + ".tmp").c_str(), srfOutFilename.c_str()) ) {
        DIE("Could not rename srfOutput file %s to %s! %s\n", string(srfOutFilename + ".tmp").c_str(), srfOutFilename.c_str(), strerror(errno));
    } else {
        LOGF("Finished building %s\n", srfOutFilename.c_str());
    }

    return newScaffoldId - 1;
}

float find_peak_depth(const unordered_map<int, int>& depthHist)
{
    float peakD = 0.0;
    int maxBases = 0;

    for (auto it : depthHist) {
        int d = it.first;
        int b = it.second;
        if (b > maxBases) {
            peakD = d;
            maxBases = b;
        }
    }
    return peakD;
}

End &EndTie::getTiedEnd()
{
    return onoObjectsMap.getEnd(this->tiedEndId);
}
const End &EndTie::getTiedEnd() const
{
    return onoObjectsMap.getEnd(this->tiedEndId);
}

vector<EndId> EndTie::getInverseTiedIds() const  // all ends that this tied end ties back to.
{
  vector<EndId> inverseTiedIds;
  const End& tiedEnd = this->getTiedEnd();
  vector<EndTie> inverseTies = tiedEnd.export_all_ties();
  for (size_t j = 0; j < inverseTies.size(); j++) {
    inverseTiedIds.push_back(inverseTies[j].getTiedEnd().getEndId());
  }
  return inverseTiedIds;
}

End &getEnd(EndId endId)
{
    return onoObjectsMap.getEnd(endId);
}

End &getEnd(EndTie endTie)
{
    return getEnd(endTie.getEndId());
}



int path_suffix_overlap(path_t &queryPath, path_t &subjectPath)
{
  // return the position on queryPath where redundant suffix begins
  int i1 = queryPath.size()-1;
  int i2;
  int k=0;
  
  auto it = std::find(subjectPath.begin(), subjectPath.end(), queryPath.back());
  if(it==subjectPath.end()) {
    return 0;
  } else {
    i2 = std::distance(subjectPath.begin(), it);  //start scanning the subject path from here
  }

  //even-indexed ends are "sources" and odds are "sinks";
  //the query end is a sink by definition, so we determine walk direction from this
  bool flipDirection = (i2 % 2 == 0) ? true : false;
  
  while(i1 >= 0 && i2 >= 0 && i2 < subjectPath.size()  && queryPath[i1] == subjectPath[i2]) {
    i1--;
    if (flipDirection) {
      i2++;
    } else {
      i2--;
    }
    k++;
  }
  return k;
}
