#ifndef _ONO_META_H_
#define _ONO_META_H_

#if defined (__cplusplus)
extern "C" {
#endif

int oNo_meta_main(int argc, char **argv);

#if defined (__cplusplus)
}
#endif

#endif // _ONO_META_H_
