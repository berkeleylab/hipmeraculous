#include "parCC.h"

#define UPC_PARALLEL_SORT_TYPE link_t
#include "upc_parallel_sort.upc"

#define thread0LinksCheckpoint "Temp-LinksForThread0"
#define thread0HmmCheckpoint "Temp-HmmForThread0"
#define thread0ObjCheckpoint "Temp-ObjForThread0"

#define PARCC_CONVERGE_FASTER

#ifdef DEBUG
#define MIN_MAX_CC_SIZE 1 /* debug build always tests the largest CC pathway */
#else
#define MIN_MAX_CC_SIZE 1000 /* minimum size for the 'largest' CC to be offloaded to thread 0 - avoids races when # CC is small */
#endif

char **split_commandline(const char *cmdline, int *argc)
{
    int i;
    char **argv = NULL;
    wordexp_t p;

    wordexp(cmdline, &p, 0);
    *argc = p.we_wordc;
    argv = calloc_chk(*argc, sizeof(char *));
    for (i = 0; i < p.we_wordc; i++) {
        argv[i] = strdup_chk(p.we_wordv[i]);
    }
    wordfree(&p);
    return argv;
}



void free_split_commandline(int *argc, char **argv)
{
    int i;

    for (i = 0; i < *argc; i++) {
        free_chk(argv[i]);
    }
    free_chk(argv);
}

shared char max_changes;
shared int64_t max_cc;
shared int64_t n_cc = 0;
shared int64_t largest_CC_ID;
shared int64_t offset_in_largsest_CC_compact_list = 0;

int parse_hmm_line(char *hmm_line, int64_t *hmm_contig, int64_t *hmm_start, int64_t *hmm_end, char *strand)
{
    char *token, *aux;

    token = strtok_r(hmm_line, " \t", &aux);
    if (token[0] != 'C') {
        return 0;
    }

    (*hmm_contig) = atol(token + 7);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    (*hmm_start) = atol(token);
    token = strtok_r(NULL, " \t", &aux);
    (*hmm_end) = atol(token);
    token = strtok_r(NULL, " \t", &aux);
    token = strtok_r(NULL, " \t", &aux);
    (*strand) = *(token);

    return 1;
}

int splitLinkMetaLine(char *line, libInfoType *libInfo)
{
    char *token;
    char *aux;

    token = strtok_r(line, "\t", &aux);
    strncpy(libInfo->libName, token, LIB_NAME_LEN - 1);
    token = strtok_r(NULL, "\t", &aux);
    libInfo->insertSize = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    libInfo->stdDev = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    int len = strlen(token);
    // make sure to drop the \n at the end
    if (len > MAX_NAME_SIZE) {
        DIE("Invalid line in splitLinkMetaLine.  linkFileNamePrefix is > %d chars for libName=%s: %s\n", MAX_NAME_SIZE, libInfo->libName,  token);
        return 0;
    } else {
        memcpy(libInfo->linkFileNamePrefix, token, len - 1);
        libInfo->linkFileNamePrefix[len - 1] = '\0';
        return 1;
    }
}


int parse_link_line(char *line, int64_t *vertex1, int64_t *vertex2, char *end1_s, char *end2_s)
{
    char *token, *aux;
    char cur_typeS[1000], cur_key[2000];
    char link_type;
    char entries1[2000], entries2[2000], entries3[2000];
    int64_t f1, f2, f3, f4, f5;
    int support_count;

    token = strtok_r(line, "\t", &aux);
    strcpy(cur_typeS, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(cur_key, token);
    if (cur_typeS[2] == 'A') {           // SP'A'N
        link_type = SPAN;
    } else if (cur_typeS[2] == 'L') {    // SP'L'INT
        link_type = SPLINT;
    }

    token = strtok_r(NULL, "\t", &aux);
    strcpy(entries1, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(entries2, token);
    if (link_type == SPAN) {
        token = strtok_r(NULL, "\t", &aux);
        strcpy(entries3, token);
    }

    if (link_type == SPAN) {
        token = strtok_r(cur_key, "<", &aux);
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            *end1_s = 3;
        } else {
            assert(strcmp(token + (strlen(token) - 1), "5") == 0);
            *end1_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        if ((*token) == 'S') {
            (*vertex1) = atol(token + 8);
        } else {
            (*vertex1) = atol(token + 6);
        }
        token = strtok_r(NULL, "<", &aux);
        token += 2; // to skip "=>"
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            *end2_s = 3;
        } else {
            assert(strcmp(token + (strlen(token) - 1), "5") == 0);
            *end2_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        if ((*token) == 'S') {
            (*vertex2) = atol(token + 8);
        } else {
            (*vertex2) = atol(token + 6);
        }
        //Parse then the info
        token = strtok_r(entries1, "|", &aux);
        f1 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f2 = atol(token);
        f3 = atol(entries2);
        f5 = atol(entries3);
        support_count = f2;
    }

    if (link_type == SPLINT) {
        token = strtok_r(cur_key, "<", &aux);
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            *end1_s = 3;
        } else {
            assert(strcmp(token + (strlen(token) - 1), "5") == 0);
            *end1_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        (*vertex1) = atol(token + 6);
        token = strtok_r(NULL, "<", &aux);
        token += 2; // to skip "=>"
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            *end2_s = 3;
        } else {
            assert(strcmp(token + (strlen(token) - 1), "5") == 0);
            *end2_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        (*vertex2) = atol(token + 6);
        token = strtok_r(entries1, "|", &aux);
        f1 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f2 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f3 = atol(token);
        f4 = atol(entries2);
        support_count = f1;
    }

    return support_count;
}

int parse_full_link_line(char *line, int64_t *vertex1, int64_t *vertex2, link_t *result_link)
{
    char *token, *aux;
    char cur_typeS[1000], cur_key[2000];
    char link_type;
    char entries1[2000], entries2[2000], entries3[2000];
    char end1_s, end2_s;
    int64_t f1, f2, f3, f4, f5;
    int support_count;

    token = strtok_r(line, "\t", &aux);
    strcpy(cur_typeS, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(cur_key, token);
    if (cur_typeS[2] == 'A') {           // SP'A'N
        link_type = SPAN;
    } else if (cur_typeS[2] == 'L') {    // SP'L'INT
        link_type = SPLINT;
    }

    token = strtok_r(NULL, "\t", &aux);
    strcpy(entries1, token);
    token = strtok_r(NULL, "\t", &aux);
    strcpy(entries2, token);
    if (link_type == SPAN) {
        token = strtok_r(NULL, "\t", &aux);
        strcpy(entries3, token);
    }

    if (link_type == SPAN) {
        token = strtok_r(cur_key, "<", &aux);
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            end1_s = 3;
        } else {
            end1_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        if ((*token) == 'S') {
            (*vertex1) = atol(token + 8);
        } else {
            (*vertex1) = atol(token + 6);
        }
        token = strtok_r(NULL, "<", &aux);
        token += 2; // to skip "=>"
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            end2_s = 3;
        } else {
            end2_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        if ((*token) == 'S') {
            (*vertex2) = atol(token + 8);
        } else {
            (*vertex2) = atol(token + 6);
        }
        //Parse then the info
        token = strtok_r(entries1, "|", &aux);
        f1 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f2 = atol(token);
        f3 = atol(entries2);
        f5 = atol(entries3);
        support_count = f2;
    }

    if (link_type == SPLINT) {
        token = strtok_r(cur_key, "<", &aux);
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            end1_s = 3;
        } else {
            end1_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        (*vertex1) = atol(token + 6);
        token = strtok_r(NULL, "<", &aux);
        token += 2; // to skip "=>"
        if (strcmp(token + (strlen(token) - 1), "3") == 0) {
            end2_s = 3;
        } else {
            end2_s = 5;
        }
        token[strlen(token) - 2] = '\0';
        (*vertex2) = atol(token + 6);
        token = strtok_r(entries1, "|", &aux);
        f1 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f2 = atol(token);
        token = strtok_r(NULL, "|", &aux);
        f3 = atol(token);
        f4 = atol(entries2);
        support_count = f1;
    }

    /* Propagate results to link destination */
    result_link->v1 = (*vertex1);
    result_link->v2 = (*vertex2);
    result_link->type = link_type;
    result_link->end1 = end1_s;
    result_link->end2 = end2_s;
    result_link->f1 = f1;
    result_link->f2 = f2;
    result_link->f3 = f3;
    result_link->f4 = f4;
    result_link->f5 = f5;

    return support_count;
}

int find_connected_components(GZIP_FILE curLinkFileFD, int minimum_link_support, int64_t n_vertices, reverse_maps_t *reverse_maps, shared[1] int64_t **ccLabels_result, int64_t **list_of_assigned_cc_ids_result, int64_t *n_assigned_result, shared[1] asmObj_t *object_db, int filter_length, double filter_depth, int filter_connections, shared[1] hmm_db_t *hmm_db)
{
    int64_t i, j, e, v1, v2;
    char line[1000], e1, e2;

    shared[1] int64_t * ccLabels = NULL;
    shared[1] int64_t * nMembers = NULL;
    shared[1] char *sharedChanges = NULL;
    int64_t myChanges = 0;
    int totalChanges = 0;
    int64_t *private_list = NULL;
    int64_t remote_val, ccId;
    upc_tick_t start_tick, end_tick, start_tick2, end_tick2;
    uint64_t time_in_ns;
    int support_count;
    asmObj_t cached_obj;

    /* Some memory management auxiliary data structures...  */
    start_tick = upc_ticks_now();
    int64_t *mem_req = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    shared[1] int64_t * heap_sizes = NULL;
    UPC_ALL_ALLOC_CHK(heap_sizes, THREADS, sizeof(int64_t));
    heap_sizes[MYTHREAD] = 0;
    for (i = 0; i < THREADS; i++) {
        mem_req[i] = 0;
    }
    UPC_LOGGED_BARRIER;

    if (filter_length > 0 && filter_connections > 0) {
        /* Optionally pass over the link files to calculate connections per contig */
        while (GZIP_GETS(line, 1000, curLinkFileFD) != NULL) {
            support_count = parse_link_line(line, &v1, &v2, &e1, &e2);
            if (support_count >= minimum_link_support) {
                if (e1 == 3) {
                    UPC_ATOMIC_FADD_I32_RELAXED(NULL, &object_db[v1].n_links3, 1);
                } else {
                    assert(e1 == 5);
                    UPC_ATOMIC_FADD_I32_RELAXED(NULL, &object_db[v1].n_links5, 1);
                }
                if (e2 == 3) {
                    UPC_ATOMIC_FADD_I32_RELAXED(NULL, &object_db[v2].n_links3, 1);
                } else {
                    assert(e2 == 5);
                    UPC_ATOMIC_FADD_I32_RELAXED(NULL, &object_db[v2].n_links5, 1);
                }
            }
        }
        GZIP_REWIND(curLinkFileFD);
        UPC_LOGGED_BARRIER;
        serial_printf("Calculated contig connections in %0.3f sec\n", TICKS_TO_S(upc_ticks_now() - start_tick));
    }

    /* First pass over the link files to calculate memory requirements */
    while (GZIP_GETS(line, 1000, curLinkFileFD) != NULL) {
        support_count = parse_link_line(line, &v1, &v2, &e1, &e2);
        if (support_count >= minimum_link_support) {
            if (filter_length > 0 && (filter_depth > 0 || filter_connections > 0)) {
                if (hmm_db == NULL || ((hmm_db[v1].n_entries == 0) && (hmm_db[v2].n_entries == 0))) {
                    cached_obj = object_db[v1];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                    cached_obj = object_db[v2];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                }
            }
            if (v1 != v2) {
                mem_req[v1 % THREADS]++;
                mem_req[v2 % THREADS]++;
            }
        }
    }
    GZIP_REWIND(curLinkFileFD);

    for (i = 0; i < THREADS; i++) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &heap_sizes[i], mem_req[i]);
    }
    UPC_LOGGED_BARRIER;

    int64_t my_n_entries = heap_sizes[MYTHREAD] + (n_vertices + THREADS - 1) / THREADS;
    shared[1] sharedTuplePtr * heap_ptrs = NULL;
    UPC_ALL_ALLOC_CHK(heap_ptrs, THREADS, sizeof(sharedTuplePtr));
    UPC_ALLOC_CHK(heap_ptrs[MYTHREAD], my_n_entries * sizeof(tuple_t));
    sharedTuplePtr *cached_heap_ptrs = (sharedTuplePtr *)malloc_chk(THREADS * sizeof(sharedTuplePtr));
    UPC_ALL_ALLOC_CHK(ccLabels, n_vertices, sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(nMembers, n_vertices, sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(sharedChanges, THREADS, sizeof(char));
    UPC_LOGGED_BARRIER;

    for (i = 0; i < THREADS; i++) {
        cached_heap_ptrs[i] = heap_ptrs[i];
    }
    heap_sizes[MYTHREAD] = 0;
    UPC_LOGGED_BARRIER;

    end_tick = upc_ticks_now();
    serial_printf("Done with memory management logistics in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);
    start_tick = upc_ticks_now();

    tuple_t *store_bufs = (tuple_t *)malloc_chk(THREADS * CHUNK_SIZE * sizeof(tuple_t));
    int64_t *store_bufs_pos = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    for (i = 0; i < THREADS; i++) {
        store_bufs_pos[i] = 0;
    }

    /* Store the link information into the heaps */
    int remote_thread;
    int64_t store_pos = 0;
    tuple_t cur_tuple;

    while (GZIP_GETS(line, 199, curLinkFileFD) != NULL) {
        support_count = parse_link_line(line, &v1, &v2, &e1, &e2);
        if (support_count >= minimum_link_support) {
            if (filter_length > 0 && (filter_depth > 0 || filter_connections > 0)) {
                if (hmm_db == NULL || ((hmm_db[v1].n_entries == 0) && (hmm_db[v2].n_entries == 0))) {
                    cached_obj = object_db[v1];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                    cached_obj = object_db[v2];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                }
            }
            cur_tuple.x = v1;
            cur_tuple.y = v2;
            if (v1 != v2) {
                remote_thread = v1 % THREADS;
                store_pos = store_bufs_pos[remote_thread];
                if (store_pos <= CHUNK_SIZE - 1) {
                    store_bufs[remote_thread * CHUNK_SIZE + store_pos] = cur_tuple;
                    store_bufs_pos[remote_thread]++;
                }
                store_pos = store_bufs_pos[remote_thread];
                if (store_pos == CHUNK_SIZE) {
                    UPC_ATOMIC_FADD_I64(&store_pos, &heap_sizes[remote_thread], CHUNK_SIZE);
                    upc_memput((shared[] tuple_t *)(cached_heap_ptrs[remote_thread] + store_pos), &store_bufs[remote_thread * CHUNK_SIZE], CHUNK_SIZE * sizeof(tuple_t));
                    store_bufs_pos[remote_thread] = 0;
                }

                cur_tuple.y = v1;
                cur_tuple.x = v2;
                remote_thread = v2 % THREADS;
                store_pos = store_bufs_pos[remote_thread];
                if (store_pos <= CHUNK_SIZE - 1) {
                    store_bufs[remote_thread * CHUNK_SIZE + store_pos] = cur_tuple;
                    store_bufs_pos[remote_thread]++;
                }
                store_pos = store_bufs_pos[remote_thread];
                if (store_pos == CHUNK_SIZE) {
                    UPC_ATOMIC_FADD_I64(&store_pos, &heap_sizes[remote_thread], CHUNK_SIZE);
                    upc_memput((shared[] tuple_t *)(cached_heap_ptrs[remote_thread] + store_pos), &store_bufs[remote_thread * CHUNK_SIZE], CHUNK_SIZE * sizeof(tuple_t));
                    store_bufs_pos[remote_thread] = 0;
                }
            }
        }
    }
    GZIP_REWIND(curLinkFileFD);

    for (remote_thread = 0; remote_thread < THREADS; remote_thread++) {
        if (store_bufs_pos[remote_thread] != 0) {
            UPC_ATOMIC_FADD_I64(&store_pos, &heap_sizes[remote_thread], store_bufs_pos[remote_thread]);
            upc_memput((shared[] tuple_t *)(cached_heap_ptrs[remote_thread] + store_pos), &store_bufs[remote_thread * CHUNK_SIZE], store_bufs_pos[remote_thread] * sizeof(tuple_t));
        }
    }
    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Done with communicating the graph in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);
    start_tick = upc_ticks_now();

    /* Each thread iterates over the received data and calculates cardinality for each edge list */
    shared[1] int64_t * edgeList_sizes = NULL;
    UPC_ALL_ALLOC_CHK(edgeList_sizes, n_vertices, sizeof(int64_t));
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        edgeList_sizes[i] = 1;
    }

    tuple_t *my_heap = (tuple_t *)cached_heap_ptrs[MYTHREAD];
    int64_t my_watermark = heap_sizes[MYTHREAD];
    for (i = 0; i < my_watermark; i++) {
        cur_tuple = my_heap[i];
        assert(cur_tuple.x % THREADS == MYTHREAD); // atomic add not needed as only incrementing local counters...
        edgeList_sizes[cur_tuple.x]++;
    }

    /* Each thread iterates over the received data and packs/stores them in the corresponding edge lists */
    int64_t *edgeListBuffer = (int64_t *)calloc_chk(my_n_entries, sizeof(int64_t));
    int64_t running_offset = 0;
    shared[1] LongPtr * edgeLists = NULL;
    UPC_ALL_ALLOC_CHK(edgeLists, n_vertices, sizeof(LongPtr));
    shared[1] int64_t * edgeListsOffsets = NULL;
    UPC_ALL_ALLOC_CHK(edgeListsOffsets, n_vertices, sizeof(int64_t));
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        edgeLists[i] = (int64_t *)&edgeListBuffer[running_offset];
        running_offset += edgeList_sizes[i];
        assert(edgeListBuffer[running_offset - 1] == 0);
        edgeListBuffer[running_offset - 1] = END_OF_LIST;
        edgeListsOffsets[i] = 0;
    }

    for (i = 0; i < my_watermark; i++) {
        cur_tuple = my_heap[i];
        assert(cur_tuple.x % THREADS == MYTHREAD); // only local counters are used and incremented
        int64_t *cur_tuple_array = (int64_t *)edgeLists[cur_tuple.x];
        assert(cur_tuple_array[edgeListsOffsets[cur_tuple.x]] == 0);
        cur_tuple_array[edgeListsOffsets[cur_tuple.x]] = cur_tuple.y;
        edgeListsOffsets[cur_tuple.x]++;
    }

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Done with packing the local edge lists in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);

#ifdef DUMP_CONN_STATS
    char dumpFile[MAX_FILE_PATH];
    sprintf(dumpFile, "./parcc_conn.txt" GZIP_EXT);
    GZIP_FILE dump = GZIP_OPEN_RANK_PATH(dumpFile, "w", MYTHREAD);
    GZIP_PRINTF(dump, "#ContigID\tLength\tdepth\tCCElements\tCCCount3\tCCCount5\n");
#endif

    /* Time here the CC part... */
    start_tick = upc_ticks_now();
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        nMembers[i] = 0;
    }
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
#ifdef DUMP_CONN_STATS
        cached_obj = object_db[i];
        GZIP_PRINTF(dump, "%lld\t%d\t%4.1f\t%d\t%d\t%d", (lld)i, cached_obj.length, cached_obj.depth, (int)edgeListsOffsets[i], (int)cached_obj.n_links3, (int)cached_obj.n_links5);
        private_list = (int64_t *)edgeLists[i];
        e = 0;
        while (private_list[e] != END_OF_LIST) {
            GZIP_PRINTF(dump, "%c%lld", (e == 0 ? '\t' : ','), (lld)private_list[e]);
            e++;
        }
        assert(e == edgeListsOffsets[i]);
        GZIP_PRINTF(dump, "\n");
#endif
        // initialize ccLabels
        int64_t myVal = i;
        // apply the first round without remote accesses
        private_list = (int64_t *)edgeLists[i];
        e = 0;
        while (private_list[e] != END_OF_LIST) {
            if (myVal > private_list[e]) {
                myVal = private_list[e];
            }
            e++;
        }
        ccLabels[i] = myVal;
    }
#ifdef DUMP_CONN_STATS
    GZIP_CLOSE(dump);
#endif

    if (!MYTHREAD) {
        // initialize global variables
        max_changes = 0;
        max_cc = 0;
        n_cc = 0;
        largest_CC_ID = -1;
        offset_in_largsest_CC_compact_list = 0;
    }
    UPC_LOGGED_BARRIER;

    totalChanges = 1;
    int64_t rounds = 0, remoteAccesses = 0, totalReductions = 0;
    int check_period = 10, check_times = 0;
    double reductionTime = 0.0;

    while (totalChanges) {
        myChanges = 0;
        for (i = MYTHREAD; i < n_vertices; i += THREADS) {
            int64_t myVal = ccLabels[i];
            int64_t localMinIdx = i, localMaxIdx = i;
#ifdef PARCC_CONVERGE_FASTER
            if (myVal == 0) continue; // no need to check edges for a lower value
#endif
            private_list = (int64_t *)edgeLists[i];
            e = 0;
            char changed = 0;
            while (private_list[e] != END_OF_LIST) {
                assert(private_list[e] >= 0);
                int64_t privateIdx = private_list[e];
                if (privateIdx < localMinIdx) localMinIdx = privateIdx;
                if (privateIdx > localMaxIdx) localMaxIdx = privateIdx;
                remote_val = ccLabels[privateIdx]; /* Potentially atomic read */
                remoteAccesses++;
                if (remote_val > privateIdx) DIE("Invalid assumption - ccLabels[ %lld ] > %lld\n", (lld) remote_val, (lld) privateIdx);
                if (myVal > remote_val) {
                    myVal = remote_val;
                    changed = 1;
                }
                e++;
            }

#ifdef PARCC_CONVERGE_FASTER_BUT_WITH_EXTRA_REMOTE_ACCESSES_IN_AN_IMBALANCED_WAY
            // have one local neighbor verify that the node owning the CC has not itself changed
            if (changed == 0 && i == localMaxIdx && myVal < localMinIdx && myVal != 0) {
                // This is the maximum link of immediate neighbors and these neighbors are not just the first order cc
                // check myVal for a new value
                remote_val = ccLabels[myVal];
                remoteAccesses++;
                if (remote_val > myVal) DIE("Invalid assumption - ccLabels[ %lld ] > %lld\n", (lld) remote_val, (lld) myVal);
                if (myVal > remote_val) {
                    myVal = remote_val;
                    changed = 1;
                }
            }
#endif
            if (changed) {
#ifdef PARCC_CONVERGE_FASTER
                int64_t testCC;
                do { // traverse graph until the ccLabel equals the index value
                    testCC = myVal;
                    if (testCC == 0) break; // no need to test the label of the lowest possible CC
                    myVal = ccLabels[testCC];
                    if (myVal > testCC) DIE("Invalid assumption - ccLabels[ %lld ] > %lld\n", (lld) myVal, (lld) testCC);
                    remoteAccesses++;
                } while ( testCC != myVal );
#endif
                ccLabels[i] = myVal; /* Potentially atomic write */
                myChanges++;
            }
        }
        upc_fence;
        if ((++rounds) % check_period == 0) {
            sharedChanges[MYTHREAD] = myChanges > 0 ? 1 : 0;
            start_tick2 = upc_ticks_now();
            upc_all_reduceC(&max_changes, sharedChanges, UPC_MAX, THREADS, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
            end_tick2 = upc_ticks_now();
            reductionTime += TICKS_TO_S(end_tick2 - start_tick2);
            totalReductions++;
            totalChanges = max_changes;
            if (totalChanges == 0) {
                if (check_period > 1 && check_times == 0) {
                    // at least one more round to verify all threads have no changes
                    totalChanges = 1;
                }
                check_period = 1; // check every round now
                check_times = 1;
            } else {
                check_times = 0;
            }
            LOGF("rounds=%lld myChanges=%lld remoteAccesses=%lld %0.4fs\n", (lld)rounds, (lld)myChanges, (lld)remoteAccesses, TICKS_TO_S(end_tick2 - start_tick2));
        } else {
            DBG("rounds=%lld myChanges=%lld remoteAccesses=%lld\n", (lld)rounds, (lld)myChanges, (lld)remoteAccesses);
        }
    }

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("ParCC time is %.5f seconds (DONE in %lld rounds...) %0.5f seconds in %lld reductions\n", TICKS_TO_S(end_tick - start_tick), (lld)rounds, reductionTime, (lld)totalReductions);
    start_tick = upc_ticks_now();

    /* Count the memebrship for each connected component  */
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        ccId = ccLabels[i];
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &nMembers[ccId], 1);
    }

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Counted CC membership in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);
    start_tick = upc_ticks_now();

    int64_t max_connected = 0;
    int64_t my_nCC = 0;
    shared[1] int64_t * shared_n_cc = NULL;
    UPC_ALL_ALLOC_CHK(shared_n_cc, THREADS, sizeof(int64_t));
    shared[1] int64_t * shared_max_cc = NULL;
    UPC_ALL_ALLOC_CHK(shared_max_cc, THREADS, sizeof(int64_t));

    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (nMembers[i] > 0) {
            my_nCC++;
            if (nMembers[i] > max_connected) {
                max_connected = nMembers[i];
            }
        }
    }

    shared_n_cc[MYTHREAD] = my_nCC;
    shared_max_cc[MYTHREAD] = max_connected;
    UPC_LOGGED_BARRIER;
    upc_all_reduceL(&max_cc, shared_max_cc, UPC_MAX, THREADS, 1, NULL, 0);
    UPC_LOGGED_BARRIER;
    upc_all_reduceL(&n_cc, shared_n_cc, UPC_ADD, THREADS, 1, NULL, 0);
    UPC_LOGGED_BARRIER;

    /* Create an array for each connected component. Each entry stores the "label", the size, and the "assigned processor ID" */
    /* For now, just do a naive Load balancing */
    shared[1] int64_t * n_assigned_cc = NULL;
    UPC_ALL_ALLOC_CHK(n_assigned_cc, THREADS + 1, sizeof(int64_t));
    shared[1] int64_t * assigned_cc = NULL;
    UPC_ALL_ALLOC_CHK(assigned_cc, n_vertices, sizeof(int64_t));
    n_assigned_cc[MYTHREAD] = 0;
    if (MYTHREAD==0) {
        n_assigned_cc[ALL_LABEL] = 0;
    }
    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Reductions for CC max sizes in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);
    start_tick = upc_ticks_now();

    srand(MYTHREAD);
    int assigned_processor;
    int64_t local_max_cc = max_cc;
    // additional inactive threads up to 1% of the job up to 1 node (0 is okay)
    int inactive_threads = (_sv) ? (MYSV.cores_per_node - 1) : 15;
    if (inactive_threads > (THREADS - 1) / 100) {
        inactive_threads = (THREADS - 1) / 100;
    }
    int mod = THREADS - 1 - inactive_threads;
    if (mod <= 0) {
        mod = 1;
    }
    int mustRerun = 0;
    serial_printf("Threads 0 - %d will hold memory for the largest CC (%lld)\n", inactive_threads, (lld) local_max_cc);
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (nMembers[i] > 0) {
            assigned_processor = THREADS > 1 ? (rand() % mod) + 1 + inactive_threads : 0;
            if (local_max_cc >= MIN_MAX_CC_SIZE && nMembers[i] == local_max_cc) {
                // all processors will handle this component
                int64_t oldId;
                UPC_ATOMIC_CSWAP_I64(&oldId, &largest_CC_ID, -1, i);
                if (oldId == -1) {
                    assigned_processor = ALL_LABEL;
                    LOG("largest_CC_ID=%lld\n", (lld) i);
                } else {
                    LOGF("NOTICE: There is more than one CC with the local_max_CC of %lld entries - %lld already chosen, sending %lld to a different thread - %d\n", (lld) local_max_cc, (lld) oldId, (lld) i, assigned_processor);
                    mustRerun = 1;
                }
            }
            assigned_cc[i] = assigned_processor;
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &n_assigned_cc[assigned_processor], 1);
        } else {
            assigned_cc[i] = NON_LABEL;
        }
    }

    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0 && n_assigned_cc[MYTHREAD]) DIE("n_assigned_cc should be 0 for Thread 0: %lld\n", (lld) n_assigned_cc[MYTHREAD]);
    reverse_maps->largest_CC_ID = broadcast_long(largest_CC_ID, 0);
    reverse_maps->largest_CC_size = local_max_cc;
    shared[] int64_t * my_assigned_ccs = NULL;
    UPC_ALLOC_CHK(my_assigned_ccs, (n_assigned_cc[MYTHREAD]+1) * sizeof(int64_t));
    shared[1] sharedLongPtr * cc_heaps = NULL;
    UPC_ALL_ALLOC_CHK(cc_heaps, THREADS, sizeof(sharedLongPtr));
    cc_heaps[MYTHREAD] = my_assigned_ccs;
    int64_t n_assigned = n_assigned_cc[MYTHREAD];
    n_assigned_cc[MYTHREAD] = 0;
    LOGF("n_assigned=%lld largest_CC_ID=%lld local_max=%lld\n", (lld) n_assigned, (lld) reverse_maps->largest_CC_ID, (lld) local_max_cc);
    UPC_LOGGED_BARRIER;

    /* Cache locally remote heap pointers  */
    sharedLongPtr *local_cc_heaps = (sharedLongPtr *)malloc_chk(THREADS * sizeof(sharedLongPtr));
    for (i = 0; i < THREADS; i++) {
        local_cc_heaps[i] = cc_heaps[i];
    }

    int64_t remote_pos;
    shared[] int64_t * remote_heap = NULL;

    Buffer myLargestCCMembers = initBuffer( (local_max_cc * 2 / THREADS + 128) * sizeof(int64_t) );
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        ccId = ccLabels[i];
        if (ccId == reverse_maps->largest_CC_ID) {
            int64_t *nextMember = (int64_t*) allocFromBuffer(myLargestCCMembers, sizeof(int64_t));
            *nextMember = i;
            DBG("Found %lld as member of LARGEST cc: %lld with %lld members\n", (lld) i, (lld) ccId, (lld) nMembers[i]);
        } else if (nMembers[i] > 0) {
            assigned_processor = assigned_cc[i];
            assert(assigned_processor != NON_LABEL);
            UPC_ATOMIC_FADD_I64_RELAXED(&remote_pos, &n_assigned_cc[assigned_processor], 1);
            remote_heap = local_cc_heaps[assigned_processor];
            remote_heap[remote_pos] = i;
            DBG("Found %lld as leader of cc: %lld with %lld members. assigned_processor=%d\n", (lld) i, (lld) ccId, (lld) nMembers[i], assigned_processor);
        }
    }

    /* Allocate/initialize reverse map */
    int myLargestCCMembersCount = getLengthBuffer(myLargestCCMembers) / sizeof(int64_t);
    LOGF("Generating largest_assigned_cc_ids. myLargestCount=%d. upc_alloc of %0.3f MB\n", myLargestCCMembersCount, 1.0 / ONE_MB * myLargestCCMembersCount * sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(reverse_maps->largest_CC_reverse_map, THREADS, sizeof(reverse_map_t));
    reverse_maps->largest_CC_reverse_map[MYTHREAD].nEntries = myLargestCCMembersCount;
    reverse_maps->largest_CC_reverse_map[MYTHREAD].cur_pos = 0;
    UPC_ALLOC_CHK(reverse_maps->largest_CC_reverse_map[MYTHREAD].members_list, myLargestCCMembersCount * sizeof(int64_t));
    memcpy( (int64_t*) reverse_maps->largest_CC_reverse_map[MYTHREAD].members_list, getStartBuffer(myLargestCCMembers), myLargestCCMembersCount * sizeof(int64_t));
    freeBuffer(myLargestCCMembers);
    
    LOGF("Generating list_of_assigned_cc_ids. n_assigned=%lld. Mallocing %0.3f MB\n", (lld) n_assigned, 1.0 / ONE_MB * n_assigned * sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(reverse_maps->reverse_map, n_vertices, sizeof(reverse_map_t));
    shared[1] reverse_map_t *reverse_map = reverse_maps->reverse_map;
    shared[] int64_t * local_heap = local_cc_heaps[MYTHREAD];
    reverse_map_t cur_reverse_map_entry;
    int64_t cur_cc;
    int64_t cur_cc_size;
    int64_t *list_of_assigned_cc_ids = (int64_t *)malloc_chk(n_assigned * sizeof(int64_t));

    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        reverse_map[i].cur_pos = 0;
        reverse_map[i].nEntries = 0;
    }
    UPC_LOGGED_BARRIER;

    for (i = 0; i < n_assigned; i++) {
        cur_cc = local_heap[i];
        list_of_assigned_cc_ids[i] = cur_cc;
        cur_cc_size = nMembers[cur_cc];
        cur_reverse_map_entry.members_list = NULL;
        if (cur_cc != reverse_maps->largest_CC_ID) {
            UPC_ALLOC_CHK(cur_reverse_map_entry.members_list, cur_cc_size * sizeof(int64_t));
            cur_reverse_map_entry.cur_pos = 0;
            cur_reverse_map_entry.nEntries = cur_cc_size;
            reverse_map[cur_cc] = cur_reverse_map_entry;
        } else {
            DIE("This code should not be reached even on thread 0 where n_assigned should be 0 (%lld)!\n", (lld) n_assigned);
        }
    }

    UPC_LOGGED_BARRIER;

    /* Build a reverse index: Each connected component has list of assigned vertices */
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        ccId = ccLabels[i];
        if (ccId != reverse_maps->largest_CC_ID) {
            UPC_ATOMIC_FADD_I64_RELAXED(&remote_pos, &(reverse_map[ccId].cur_pos), 1);
            remote_heap = reverse_map[ccId].members_list;
            remote_heap[remote_pos] = i;
        } else {
            // largest CC, ignore here -- largest_CC_reverse_map[*].members_list is already constructed
        }
    }
    LOGF("Assigned member entries to remote_heap member_lists\n");
    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Build reverse vertex index in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);

    *ccLabels_result = ccLabels;
    *list_of_assigned_cc_ids_result = list_of_assigned_cc_ids;
    *n_assigned_result = n_assigned;

    serial_printf("In total %lld CCs, maximum with size %lld\n", (lld) n_cc, (lld) max_cc);

#if 0
    int64_t *local_members;
    if (MYTHREAD == 0) {
        serial_printf("In total %lld CCs, maximum with size %lld\n", (lld) n_cc, (lld) max_cc);
        local_members = (int64_t *)malloc_chk(n_vertices * sizeof(int64_t));
        for (i = 0; i < n_vertices; i++) {
            local_members[i] = nMembers[i];
        }
        qsort(local_members, n_vertices, sizeof(int64_t), cmpfunc);
        for (i = 0; i < 10; i++) {
            if (local_members[i] > 1) {
                LOGF("Found CC with %lld members\n", (lld) local_members[i]);
            }
        }
        free_chk(local_members);
    }
#endif

    UPC_LOGGED_BARRIER;

    free_chk(cached_heap_ptrs);
    free_chk(mem_req);
    free_chk(store_bufs);
    free_chk(store_bufs_pos);
    free_chk(edgeListBuffer);
    free_chk(local_cc_heaps);

    UPC_FREE_CHK(heap_ptrs[MYTHREAD]);
    UPC_FREE_CHK(my_assigned_ccs);

    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(heap_sizes);
    UPC_ALL_FREE_CHK(heap_ptrs);
    UPC_ALL_FREE_CHK(nMembers);
    UPC_ALL_FREE_CHK(sharedChanges);
    UPC_ALL_FREE_CHK(edgeList_sizes);
    UPC_ALL_FREE_CHK(edgeLists);
    UPC_ALL_FREE_CHK(edgeListsOffsets);
    UPC_ALL_FREE_CHK(shared_n_cc);
    UPC_ALL_FREE_CHK(n_assigned_cc);
    UPC_ALL_FREE_CHK(shared_max_cc);
    UPC_ALL_FREE_CHK(assigned_cc);
    UPC_ALL_FREE_CHK(cc_heaps);

    return mustRerun;
}


void redistribute_links(GZIP_FILE curLinkFileFD, int minimum_link_support, int64_t n_vertices, shared[1] int64_t *ccLabels, shared[1] link_cc_t **links_in_cc_result, shared[] link_t **compact_buffer_cc_result, shared[1] link_t **the_largest_cc_compact_buffer_result, shared[1] asmObj_t *object_db, int filter_length, double filter_depth, int filter_connections, shared[1] hmm_db_t *hmm_db)
{
    int64_t i, j, k, e, v1, v2, ccId, current_tip;
    char line[1000], e1, e2;
    upc_tick_t start_tick, end_tick;
    uint64_t time_in_ns;
    int64_t my_n_entries;
    int64_t my_watermark;
    int support_count;

    int64_t *mem_req = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    int64_t min_req = -1, max_req = 0, tot_req = 0;
    int max_mem_thread = 0;

    shared[1] int64_t * heap_sizes = NULL;
    UPC_ALL_ALLOC_CHK(heap_sizes, THREADS, sizeof(int64_t));
    int64_t *store_bufs_pos = (int64_t *)malloc_chk(THREADS * sizeof(int64_t));
    int remote_thread;
    int64_t store_pos = 0;
    asmObj_t cached_obj;

    /* Store the links in a distributed table (similar to the graph) but with "all" associated info */
    heap_sizes[MYTHREAD] = 0;
    for (i = 0; i < THREADS; i++) {
        mem_req[i] = 0;
    }
    UPC_LOGGED_BARRIER;
    start_tick = upc_ticks_now();

    /* First pass over the link files to calculate memory requirements */
    while (GZIP_GETS(line, 1000, curLinkFileFD) != NULL) {
        support_count = parse_link_line(line, &v1, &v2, &e1, &e2);
        if (support_count >= minimum_link_support) {
            if (filter_length > 0 && (filter_depth > 0 || filter_connections > 0)) {
                if (hmm_db == NULL || ((hmm_db[v1].n_entries == 0) && (hmm_db[v2].n_entries == 0))) {
                    cached_obj = object_db[v1];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                    cached_obj = object_db[v2];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                }
            }
            mem_req[v1 % THREADS]++;
        }
    }
    GZIP_REWIND(curLinkFileFD);

    for (i = 0; i < THREADS; i++) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &heap_sizes[i], mem_req[i]);
        if (min_req < 0 || min_req > mem_req[i]) {
            min_req = mem_req[i];
        }
        if (max_req < mem_req[i]) {
            max_req = mem_req[i]; max_mem_thread = i;
        }
        tot_req += mem_req[i];
    }
    UPC_LOGGED_BARRIER;
    my_n_entries = heap_sizes[MYTHREAD];

    LOGF("required per-thread memory: my_n_entries=%lld (%0.1f MB), min=%0.1f MB, max=%0.1f MB (Th%d), avg=%0.1f MB, tot=%0.1f MB\n", (lld)my_n_entries, 1.0 * my_n_entries * sizeof(link_t) / ONE_MB, 1.0 * min_req * sizeof(link_t) / ONE_MB, 1.0 * max_req * sizeof(link_t) / ONE_MB, max_mem_thread, 1.0 * tot_req * sizeof(link_t) / ONE_MB / THREADS, 1.0 * tot_req * sizeof(link_t) / ONE_MB);

    shared[1] sharedLinkPtr * link_heap_ptrs = NULL;
    UPC_ALL_ALLOC_CHK(link_heap_ptrs, THREADS, sizeof(sharedLinkPtr));
    link_heap_ptrs[MYTHREAD] = NULL;
    UPC_ALLOC_CHK(link_heap_ptrs[MYTHREAD], my_n_entries * sizeof(link_t));
    sharedLinkPtr *cached_link_heap_ptrs = (sharedLinkPtr *)malloc_chk(THREADS * sizeof(sharedLinkPtr));
    UPC_LOGGED_BARRIER;

    for (i = 0; i < THREADS; i++) {
        cached_link_heap_ptrs[i] = link_heap_ptrs[i];
    }
    heap_sizes[MYTHREAD] = 0;
    UPC_LOGGED_BARRIER;

    link_t *link_store_bufs = (link_t *)malloc_chk(THREADS * CHUNK_SIZE * sizeof(link_t));
    for (i = 0; i < THREADS; i++) {
        store_bufs_pos[i] = 0;
    }

    /* Store the link information into the heaps */
    store_pos = 0;
    link_t cur_link;

    while (GZIP_GETS(line, 199, curLinkFileFD) != NULL) {
        support_count = parse_full_link_line(line, &v1, &v2, &cur_link);
        if (support_count >= minimum_link_support) {
            if (filter_length > 0 && (filter_depth > 0 || filter_connections > 0)) {
                if (hmm_db == NULL || ((hmm_db[v1].n_entries == 0) && (hmm_db[v2].n_entries == 0))) {
                    cached_obj = object_db[v1];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                    cached_obj = object_db[v2];
                    if (cached_obj.length < filter_length && ((filter_depth > 0 && cached_obj.depth > filter_depth) || (filter_connections > 0 && cached_obj.n_links3 > filter_connections && cached_obj.n_links5 > filter_connections))) {
                        continue;
                    }
                }
            }
            remote_thread = v1 % THREADS;
            store_pos = store_bufs_pos[remote_thread];
            if (store_pos <= CHUNK_SIZE - 1) {
                link_store_bufs[remote_thread * CHUNK_SIZE + store_pos] = cur_link;
                store_bufs_pos[remote_thread]++;
            }
            store_pos = store_bufs_pos[remote_thread];
            if (store_pos == CHUNK_SIZE) {
                UPC_ATOMIC_FADD_I64_RELAXED(&store_pos, &heap_sizes[remote_thread], CHUNK_SIZE);
                upc_memput((shared[] link_t *)(cached_link_heap_ptrs[remote_thread] + store_pos), &link_store_bufs[remote_thread * CHUNK_SIZE], CHUNK_SIZE * sizeof(link_t));
                store_bufs_pos[remote_thread] = 0;
            }
        }
    }

    for (remote_thread = 0; remote_thread < THREADS; remote_thread++) {
        if (store_bufs_pos[remote_thread] != 0) {
            UPC_ATOMIC_FADD_I64_RELAXED(&store_pos, &heap_sizes[remote_thread], store_bufs_pos[remote_thread]);
            upc_memput((shared[] link_t *)(cached_link_heap_ptrs[remote_thread] + store_pos), &link_store_bufs[remote_thread * CHUNK_SIZE], store_bufs_pos[remote_thread] * sizeof(link_t));
        }
    }
    UPC_LOGGED_BARRIER;

    /* Each thread iterates over the received data and calculates link cardinality for each vertex */
    LOGF("Allocating shared_links_db: n_vertices=%lld (%0.1f MB globally %0.1f local)\n", (lld)n_vertices, 1.0 * n_vertices * sizeof(link_db_t) * THREADS / ONE_MB, 1.0 * n_vertices * sizeof(link_db_t) / ONE_MB);
    shared[1] link_db_t * shared_links_db = NULL;
    UPC_ALL_ALLOC_CHK(shared_links_db, n_vertices, sizeof(link_db_t));
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        shared_links_db[i].nEntries = 0;
        shared_links_db[i].cur_pos = 0;
    }

    link_t *my_link_heap = (link_t *)cached_link_heap_ptrs[MYTHREAD];
    my_watermark = heap_sizes[MYTHREAD];
    for (i = 0; i < my_watermark; i++) {
        cur_link = my_link_heap[i];
        shared_links_db[cur_link.v1].nEntries++;
    }

    /* Allocate contiguous shared space for compacted links */
    LOGF("Allocating compact_buffer: my_watermark=%lld (%0.1f MB)\n", (lld)my_watermark, 1.0 * my_watermark * sizeof(link_t) / ONE_MB);
    shared[] link_t * compact_buffer = NULL;
    UPC_ALLOC_CHK(compact_buffer, my_watermark * sizeof(link_t));
    current_tip = 0;

    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (shared_links_db[i].nEntries > 0) {
            shared_links_db[i].link_list = (shared[] link_t *) & compact_buffer[current_tip];
            current_tip += shared_links_db[i].nEntries;
        }
    }

    /* Now compact the links lists */
    shared[] link_t * cur_list_ptr = NULL;
    for (i = 0; i < my_watermark; i++) {
        cur_link = my_link_heap[i];
        cur_list_ptr = shared_links_db[cur_link.v1].link_list;
        cur_list_ptr[shared_links_db[cur_link.v1].cur_pos] = cur_link;
        shared_links_db[cur_link.v1].cur_pos++;
    }
    UPC_LOGGED_BARRIER;

    /* Iterate over the link DB and find no of links per CC */
    LOGF("Allocating links_in_cc: n_vertices=%lld (%0.1f MB global %0.1f MB local)\n", (lld)n_vertices, 1.0 * n_vertices * sizeof(link_cc_t) * THREADS / ONE_MB, 1.0 * n_vertices * sizeof(link_cc_t) / ONE_MB);
    shared[1] link_cc_t * links_in_cc = NULL;
    UPC_ALL_ALLOC_CHK(links_in_cc, n_vertices, sizeof(link_cc_t));
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        links_in_cc[i].n_links = 0;
        links_in_cc[i].cur_pos = 0;
    }
    UPC_LOGGED_BARRIER;

    int64_t n_links_check;
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        n_links_check = shared_links_db[i].nEntries;
        if (n_links_check > 0) {
            ccId = ccLabels[i];
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(links_in_cc[ccId].n_links), n_links_check);
        }
    }
    UPC_LOGGED_BARRIER;

    /* Allocate a list of link per CC  */
    /* First each thread finds the total requirements for the links to be received */
    int64_t my_total_cc_links = 1; // protect against zero byte allocations
    int64_t the_largest_cc_id = broadcast_long(largest_CC_ID, 0);

    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (i != the_largest_cc_id) {
            my_total_cc_links += links_in_cc[i].n_links;
        }
    }

    /* Make an allocation and distributed over the CC entries */
    LOGF("Allocating compact_buffer_cc: my_total_cc_links=%lld (%0.1f MB)\n", (lld)my_total_cc_links, 1.0 * my_total_cc_links * sizeof(link_t) / ONE_MB);
    shared[] link_t * compact_buffer_cc = NULL;
    UPC_ALLOC_CHK(compact_buffer_cc, my_total_cc_links * sizeof(link_t));
    shared[1] link_t * the_largest_cc_compact_buffer = NULL;
    if (the_largest_cc_id >= 0) {
        UPC_ALL_ALLOC_CHK(the_largest_cc_compact_buffer, links_in_cc[the_largest_cc_id].n_links + 1, sizeof(link_t));
    } // else there is no largest and it will remain NULL

    current_tip = 0;
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if ((links_in_cc[i].n_links > 0) && (i != the_largest_cc_id)) {
            links_in_cc[i].link_list = (shared[] link_t *) & compact_buffer_cc[current_tip];
            current_tip += links_in_cc[i].n_links;
        }
    }
    UPC_LOGGED_BARRIER;

    /* Store list of links to CC data structure */
    int64_t link_pos;
    shared[] link_t * remote_buf;
    int64_t aux_offset;
    Buffer largestLinksBuffer = initBuffer(32*ONE_KB);
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        n_links_check = shared_links_db[i].nEntries;
        if (n_links_check > 0) {
            ccId = ccLabels[i];
            if (ccId != the_largest_cc_id) {
                UPC_ATOMIC_FADD_I64_RELAXED(&link_pos, &(links_in_cc[ccId].cur_pos), n_links_check);
                remote_buf = links_in_cc[ccId].link_list;
                upc_memput((shared[] link_t *) & remote_buf[link_pos], (link_t *)shared_links_db[i].link_list, n_links_check * sizeof(link_t));
            } else {
                // aggregate all links for largest before sending
                link_t *the_local_list = (link_t *)shared_links_db[i].link_list;
                reserveBuffer(largestLinksBuffer, sizeof(link_t)*n_links_check);
                for (aux_offset = 0; aux_offset < n_links_check; aux_offset++) {
                    *((link_t*) allocFromBuffer(largestLinksBuffer, sizeof(link_t))) = the_local_list[aux_offset];
                }
            }
        }
    }

    // send links to offset positions
    if (!isEmptyBuffer(largestLinksBuffer)) {
        int64_t linksForLargest = getLengthBuffer(largestLinksBuffer) / sizeof(link_t);
        assert(linksForLargest == 0 || the_largest_cc_compact_buffer != NULL);
        assert(getLengthBuffer(largestLinksBuffer) == linksForLargest * sizeof(link_t)); // i.e. no rounding error
        UPC_ATOMIC_FADD_I64_RELAXED(&link_pos, &offset_in_largsest_CC_compact_list, linksForLargest);
        LOGF("Sending %lld links for largest starting at %lld\n", (lld) linksForLargest, (lld) link_pos);

        // FIXME can this transfer be quasi-transposed with more bulk memputs and less individual? -- the strict ordering does not matter I believe - RSE

        link_t *the_local_list = (link_t *)getStartBuffer(largestLinksBuffer);
        for (aux_offset = 0; aux_offset < linksForLargest; aux_offset++) {
           the_largest_cc_compact_buffer[aux_offset + link_pos] = the_local_list[aux_offset];
        }
    }
    freeBuffer(largestLinksBuffer);

    UPC_LOGGED_BARRIER;

    end_tick = upc_ticks_now();
    serial_printf("Redistributed Links in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);

    *links_in_cc_result = links_in_cc;
    *compact_buffer_cc_result = compact_buffer_cc;
    *the_largest_cc_compact_buffer_result = the_largest_cc_compact_buffer;

    free_chk(mem_req);
    free_chk(store_bufs_pos);
    free_chk(cached_link_heap_ptrs);
    free_chk(link_store_bufs);
    UPC_FREE_CHK(compact_buffer);
    UPC_FREE_CHK(link_heap_ptrs[MYTHREAD]);

    UPC_LOGGED_BARRIER;

    UPC_ALL_FREE_CHK(heap_sizes);
    UPC_ALL_FREE_CHK(link_heap_ptrs);
    UPC_ALL_FREE_CHK(shared_links_db);

    return;
}


void build_object_db(GZIP_FILE curReportFileFD, int64_t n_vertices, int merSize, shared[1] asmObj_t **object_db_result, shared [1] SharedString **_srf_arr)
{
    int is_srf_input = (_srf_arr == NULL) ? 0 : 1;

    upc_tick_t start_tick, end_tick;
    uint64_t time_in_ns;

    char *token, *aux;
    char buf[1024];
    char line[1024];

    asmObj_t cur_obj_entry;

    start_tick = upc_ticks_now();
    shared[1] asmObj_t * object_db = NULL;

    UPC_ALL_ALLOC_CHK(object_db, n_vertices, sizeof(asmObj_t));

    if (!is_srf_input) {
        while (GZIP_GETS(line, 999, curReportFileFD) != NULL) {
            strcpy(buf, line);
            token = strtok_r(buf, "\t", &aux);
            int64_t objId = atol(token + 6); /* Since the string should be objectXXXXXXXX */
            token = strtok_r(NULL, "\t", &aux);
            int length = atoi(token);        /* Extract object's length */
            cur_obj_entry.length = length;
            token = strtok_r(NULL, "\t", &aux);
            if (token != NULL) {
                float fdepth = atof(token); /* Extract object's depth (should be stored as int) */
                cur_obj_entry.depth = fdepth;
            }
            token = strtok_r(NULL, "\t", &aux);
            if (token != NULL) {
                int64_t diplotigId = atol(token); /* In diploid mode diplotig id is stored in the input report file;  Extract it here*/
                cur_obj_entry.diplotig = diplotigId;
            } else {
                cur_obj_entry.diplotig = -1;
            }
            cur_obj_entry.n_links3 = 0; /* 0 initialize count when loading links file */
            cur_obj_entry.n_links5 = 0; /* 0 initialize count when loading links file */

            object_db[objId] = cur_obj_entry;
        }
    } else { // srf file input
        /*  ..srf example:
         *
         * Scaffold459     CONTIG4 -Contig2948227  11      109     3.000000
         * Scaffold459     GAP4    -97     1
         */

        UPC_ALL_ALLOC_CHK(*_srf_arr, n_vertices, sizeof(SharedString));

        Buffer srfBuffer = initBuffer(1024 * 1024);
        int64_t curId = -1;
        int64_t scaffTotLen = 0;
        int64_t scaffRealBases = -1;
        float scaffDepSum = 0;
        int64_t objId;
        while (GZIP_GETS(line, 999, curReportFileFD) != NULL) {
            strcpy(buf, line);
            token = strtok_r(buf, "\t", &aux);
            objId = atol(token + 8); /* Since every srf entry begins with ScaffoldXXXXXXXX */

            if (objId != curId) {
                // populate local cur_obj struct, allocate and copy to shared objects_db and reset/clear current structs

                if (curId > -1) {
                    int mySrfStrLen = getLengthBuffer(srfBuffer);
                    UPC_ALLOC_CHK((*_srf_arr)[curId].str, mySrfStrLen);
                    upc_memput((*_srf_arr)[curId].str, getStartBuffer(srfBuffer), mySrfStrLen);
                    (*_srf_arr)[curId].len = mySrfStrLen;

                    cur_obj_entry.length = scaffTotLen;
                    cur_obj_entry.depth = scaffDepSum / scaffRealBases;
                    cur_obj_entry.diplotig = 0;
                    cur_obj_entry.n_links3 = 0; /* 0 initialize count when loading links file */
                    cur_obj_entry.n_links5 = 0; /* 0 initialize count when loading links file */

                    object_db[curId] = cur_obj_entry;
                }

                scaffDepSum = 0;
                scaffRealBases = 0;
                resetBuffer(srfBuffer);
            }

            // parse the rest of the line and tally size/depth and add to the srf buffer
            printfBuffer(srfBuffer, line);
            token = strtok_r(buf, "\t", &aux);
            if (strstr(token, "CONTIG")) {
                token = strtok_r(buf, "\t", &aux); //skip contig id
                token = strtok_r(buf, "\t", &aux);
                int start = atoi(token);
                token = strtok_r(buf, "\t", &aux);
                int end = atoi(token);
                token = strtok_r(buf, "\t", &aux);
                float depth = atof(token);
                int cLen = end - start + 1;
                scaffRealBases += cLen; //doesn't count gaps
                scaffTotLen = end;      //includes gaps
                scaffDepSum += (float)(cLen * depth);
            }

            curId = objId;
        }

        if (curId > -1) {
            int mySrfStrLen = getLengthBuffer(srfBuffer);
            UPC_ALLOC_CHK((*_srf_arr)[curId].str, mySrfStrLen);
            upc_memput((*_srf_arr)[curId].str, getStartBuffer(srfBuffer), mySrfStrLen);
            (*_srf_arr)[curId].len = mySrfStrLen;

            cur_obj_entry.length = scaffTotLen;
            cur_obj_entry.depth = scaffDepSum / scaffRealBases;
            cur_obj_entry.diplotig = 0;
            cur_obj_entry.n_links3 = 0; /* 0 initialize count when loading links file */
            cur_obj_entry.n_links5 = 0; /* 0 initialize count when loading links file */

            object_db[curId] = cur_obj_entry;
        }
    }

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Built assembly objects DB in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);
    *object_db_result = object_db;

    return;
}


#ifdef HIPMER_EMBED_HMMER
void build_hmm_db(int64_t n_vertices, char *_base_dir, char *s16s23_file_path, char *contig_file_path, shared[1] hmm_db_t **hmm_db_result, shared[] hmm_t **compact_buffer_hmm_result)
{
    int64_t i, current_tip = 0, store_pos;
    upc_tick_t start_tick, end_tick;
    uint64_t time_in_ns;

    start_tick = upc_ticks_now();
    /* Initialize over the hmm DB*/
    shared[1] hmm_db_t * hmm_db = NULL;
    UPC_ALL_ALLOC_CHK(hmm_db, n_vertices, sizeof(hmm_db_t));
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        hmm_db[i].n_entries = 0;
        hmm_db[i].cur_pos = 0;
    }
    UPC_LOGGED_BARRIER;

    FILE *hmm_out_fd;
    char curHMM_out_report[MAX_FILE_PATH];
    char curHMM_out_report_ckpt[MAX_FILE_PATH];
    snprintf(curHMM_out_report_ckpt, MAX_FILE_PATH, "hmm_out.txt");
    checkpointToLocal(curHMM_out_report_ckpt, curHMM_out_report);
    
    char curHMM_tbl_report[MAX_FILE_PATH];
    char curHMM_tbl_report_ckpt[MAX_FILE_PATH];
    snprintf(curHMM_tbl_report_ckpt, MAX_FILE_PATH, "hmm_tbl.txt");
    checkpointToLocal(curHMM_tbl_report_ckpt, curHMM_tbl_report);

    if (doesCheckpointExist(curHMM_tbl_report_ckpt)) {
        serial_printf("Skipping hmm as the report and table are already checkpointed.\n");
    } else {

        /* The contig file is gzipped which will not work with nhmmer_driver.  Uncompress it and abort if it is zero bytes */
        LOGF("Uncompressing contig file %s\n", contig_file_path);
        upc_tick_t start_uncompress = upc_ticks_now();
        const int maxBuf = 8192;
        char *buf = (char *)malloc_chk(maxBuf);
        GZIP_FILE gzr = GZIP_OPEN(contig_file_path, "r");
        strcat(contig_file_path, ".ungzip"); // just change the file to the new one.  will delete later
        FILE *fw = fopen_chk(contig_file_path, "w");
        int bytesRead = 0, bytesWrite = 0, bytesWritten = 0;
        while ((bytesRead = gzread_chk(gzr, buf, maxBuf)) > 0) {
            bytesWrite = fwrite_chk(buf, 1, bytesRead, fw);
            if (bytesWrite != bytesRead) {
                DIE("Could not read %d and write %d bytes to %s! %s\n", bytesRead, bytesWrite, contig_file_path, strerror(errno));
            }
            bytesWritten += bytesWrite;
        }
        if (!gzeof(gzr)) {
            int err;
            gzerror(gzr, &err);
            DIE("Error reading/writing %s! %d %s!\n", contig_file_path, err, strerror(err));
        }
        GZIP_CLOSE(gzr);
        fclose(fw);
        free_chk(buf);
        LOGF("Uncompressed contigs of %lld bytes in %0.3fs\n", (lld)bytesWritten, ELAPSED_TIME(start_uncompress));

        upc_tick_t start_hmm = upc_ticks_now();
        size_t fileSize = get_file_size(contig_file_path);
        if (fileSize > 0) {
            char cmd_line[4*MAX_FILE_PATH + 200];
            sprintf(cmd_line, "nhmmer_driver -o %s --tblout %s --noali --notextw --F1 0.35 --F2 0.15 --F3 0.003 --nobias --nonull2 %s %s ", curHMM_out_report, curHMM_tbl_report, s16s23_file_path, contig_file_path);
            LOGF("Executing hmm: %s\n", cmd_line);
            int my_argc;
            char **my_argv;
            my_argv = split_commandline(cmd_line, &my_argc);
            // call out to nhmmer
            upc_tick_t start_hmm = upc_ticks_now();
            nhmmer_main(my_argc, my_argv);
            free_split_commandline(&my_argc, my_argv);
        } else {
            // create empty output files that never got generated
            LOGF("Skipping hmm, creating empty output files: %s and %s\n", curHMM_out_report, curHMM_tbl_report);
            fclose_track( fopen_chk(curHMM_out_report, "w") );
            fclose_track( fopen_chk(curHMM_tbl_report, "w") );
        }

        LOGF("Deleting uncompressed contig file %s\n", contig_file_path);
        unlink(contig_file_path);
#ifdef DEBUG
        restoreCheckpoint( curHMM_out_report_ckpt ); // force global checkpoint to be created
#endif
        LOGF("Finished hmm scan in %0.3fs\n", ELAPSED_TIME(start_hmm));
    }
    upc_tick_t process_hmm_start = upc_ticks_now();

    char hmm_line[1000];

    hmm_out_fd = openCheckpoint0(curHMM_tbl_report_ckpt, "r");
    int64_t hmm_contig, hmm_start, hmm_end;
    char strand;
    int hit_line;
    while (fgets(hmm_line, 999, hmm_out_fd) != NULL) {
        hit_line = parse_hmm_line(hmm_line, &hmm_contig, &hmm_start, &hmm_end, &strand);
        if (hit_line) {
            if (hmm_contig >= n_vertices) {
                DIE("hmm_contig=%lld > n_vertices=%lld\n", (lld)hmm_contig, (lld)n_vertices);
            }
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(hmm_db[hmm_contig].n_entries), 1);
        }
    }
    rewind(hmm_out_fd);

    UPC_LOGGED_BARRIER;

    int64_t my_n_hmm_entries = 0;
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (hmm_db[i].n_entries > 0) {
            my_n_hmm_entries += hmm_db[i].n_entries;
        }
    }

    shared[] hmm_t * compact_buffer_hmm = NULL;
    UPC_ALLOC_CHK(compact_buffer_hmm, my_n_hmm_entries * sizeof(hmm_t));
    current_tip = 0;
    for (i = MYTHREAD; i < n_vertices; i += THREADS) {
        if (hmm_db[i].n_entries > 0) {
            hmm_db[i].hmm_list = (shared[] hmm_t *) & compact_buffer_hmm[current_tip];
            current_tip += hmm_db[i].n_entries;
        }
    }
    assert(current_tip == my_n_hmm_entries);

    UPC_LOGGED_BARRIER;

    hmm_t cur_hmm_entry;
    shared[] hmm_t * cur_hmm_buf = NULL;
    while (fgets(hmm_line, 999, hmm_out_fd) != NULL) {
        hit_line = parse_hmm_line(hmm_line, &hmm_contig, &hmm_start, &hmm_end, &strand);
        if (hit_line) {
            assert(hmm_contig < n_vertices);
            cur_hmm_entry.contig_id = hmm_contig;
            cur_hmm_entry.start = hmm_start;
            cur_hmm_entry.end = hmm_end;
            cur_hmm_entry.strand = strand;
            UPC_ATOMIC_FADD_I64_RELAXED(&store_pos, &(hmm_db[hmm_contig].cur_pos), 1);
            cur_hmm_buf = hmm_db[hmm_contig].hmm_list;
            if (cur_hmm_buf == NULL) {
                DIE("Could not find the buffer for hmm_contig=%lld\n", (lld)hmm_contig);
            }
            cur_hmm_buf[store_pos] = cur_hmm_entry;
        }
    }

    closeCheckpoint0(hmm_out_fd);
    syncCheckpoint(curHMM_tbl_report_ckpt); // ensure global is written to disk

    LOGF("Processed hmm files in %0.3fs\n", ELAPSED_TIME(process_hmm_start));

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Running HMM and storing in HMM DB in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);

    *hmm_db_result = hmm_db;
    *compact_buffer_hmm_result = compact_buffer_hmm;

    return;
}
#endif

int compareLinks(const void *_a, const void *_b)
{
    const link_t *a = (const link_t *)_a, *b = (const link_t *)_b;

    if (a->v1 < b->v1) {
        return -1;
    } else if (a->v1 > b->v1) {
        return 1;
    } else if (a->v2 < b->v2) {
        return -1;
    } else if (a->v2 > b->v2) {
        return 1;
    } else if (a->end1 < b->end1) {
        return -1;
    } else if (a->end1 > b->end1) {
        return 1;
    } else if (a->end2 < b->end2) {
        return -1;
    } else if (a->end2 > b->end2) {
        return 1;
    } else if (a->type < b->type) {
        return -1;
    } else if (a->type > b->type) {
        return 1;
    } else { return 0; }
}

void prepare_oNo_inputs(int64_t n_assigned, int merSize, int64_t *list_of_assigned_cc_ids, shared[1] link_cc_t *links_in_cc, shared[1] link_t *the_largest_cc_compact_buffer, reverse_maps_t *reverse_maps, shared[1] hmm_db_t *hmm_db, shared[1] asmObj_t *object_db, FILE *gatheredLinkReportFD, FILE *gatheredObjReportFD, FILE *gatheredHmmReportFD, shared [1] SharedString *srf_arr)
{
    const char *objectPrefix = (srf_arr != NULL) ? "Scaffold" : "Contig";
    upc_tick_t start_tick, end_tick;
    uint64_t time_in_ns;
    int64_t cc_id_to_process;
    int64_t current_max_size_link_list = 1000;
    int64_t current_max_size_hmm_list = 1000;
    int64_t current_max_size_obj_list = 1000;
    link_t *local_link_list = (link_t *)malloc_chk(current_max_size_link_list * sizeof(link_t));
    hmm_t *local_hmm_list = (hmm_t *)malloc_chk(current_max_size_hmm_list * sizeof(hmm_t));
    asmObj_t *local_obj_list = (asmObj_t *)malloc_chk(current_max_size_obj_list * sizeof(asmObj_t));
    shared [1] reverse_map_t *reverse_map = reverse_maps->reverse_map;
    reverse_map_t cur_reverse_map_entry;
    int64_t size_link_list;

    shared[] link_t * src_buf = NULL;
    link_cc_t cur_link_cc;
    int64_t n_hmm_entries, i, j, cur_vertex;
    int64_t hmm_pos_buf, obj_pos_buf, n_members_in_cc;
    shared[] int64_t * local_heap;
    double sortTime = 0.0, maxSortTime = 0.0;
    int64_t totalLinks = 0, maxLinks = 0, mySplints = 0, mySpans = 0, totalObjects = 0;

    start_tick = upc_ticks_now();
    /* For each assigned connected component: a) Visit member_list and gather the required links info, contig info, HMM-output info */
    int64_t the_largest_cc_id = reverse_maps->largest_CC_ID;

    // write to Buffers, then to files collectively in the first round for largest CC, or at the end / or when buffers fill
    // use checkpoints for each of the largest CC  and thread 0 rename the checkpoints after this function completes

    Buffer linkReportBuffer = initBuffer(ONE_MB-1);
    Buffer objReportBuffer = initBuffer(ONE_MB-1);
    Buffer hmmReportBuffer = initBuffer(ONE_MB-1);

    if (!MYTHREAD && n_assigned > 0) {
        DIE("Expected that no CCs would be assigned to thread 0 as it handles the largest CC collectively: %lld\n", (lld) n_assigned);
    }

    // here if i == -1, then all threads will process the largest_CC collectively
    for (i = -1; i < n_assigned; i++) {
        if (i > 0 && i % ((n_assigned+99)/100) == 0) LOGF("Processing component %lld of %lld (%0.2f %)\n", (lld) i, (lld) n_assigned, 100.0 * i / n_assigned);

        int isLargest = i < 0;
        cc_id_to_process = isLargest ? the_largest_cc_id : list_of_assigned_cc_ids[i];

        /* 1)  Gather first the involved links */

        if (! isLargest ) {
            cur_link_cc = links_in_cc[cc_id_to_process];
            size_link_list = cur_link_cc.n_links;
            src_buf = cur_link_cc.link_list;
        } else {
            // collective on largest
            if (cc_id_to_process >= 0) {
                size_link_list = broadcast_long(links_in_cc[cc_id_to_process].n_links, upc_threadof(&links_in_cc[cc_id_to_process]));
            } else {
                size_link_list = 0;
            }
            src_buf = NULL;
        }

        if (size_link_list) {

            /* Realloc local buffer if need be */
            if ( ! isLargest ) {
                if (size_link_list > current_max_size_link_list) {
                    free_chk(local_link_list);
                    local_link_list = (link_t *)malloc_chk(size_link_list * sizeof(link_t));
                    current_max_size_link_list = size_link_list;
                    LOGF("(re-)Allocated buffer to %lld entries %0.3f MB\n", (lld) size_link_list, 1.0 / ONE_MB * size_link_list * sizeof(link_t *));
                }
    
                upc_memget(local_link_list, src_buf, size_link_list * sizeof(link_t));

                upc_tick_t start_sort = upc_ticks_now();
                // sort links so that they are grouped in the input file and thus can be streamed
                qsort(local_link_list, size_link_list, sizeof(link_t), compareLinks);
                double thisSortTime = UPC_TICKS_TO_SECS(upc_ticks_now() - start_sort);
                sortTime += thisSortTime;
                if (size_link_list > 1000) LOGF("qsort of %lld links in %0.3fs, max of %lld in %0.3fs, %0.3fs since start\n", (lld)size_link_list, sortTime, (lld)maxLinks, maxSortTime, ELAPSED_TIME(start_tick));

                if (size_link_list > maxLinks) {
                    maxLinks = size_link_list;
                    maxSortTime = thisSortTime;
                }

            } else {
                // collective on largest
                int64_t full_rows = (size_link_list + THREADS - 1) / THREADS;
                int64_t full_threads = size_link_list % THREADS;
                if (full_threads == 0) {
                    full_threads = THREADS;
                }

                LOGF("Processing largest CC with %lld links of which %lld is/are mine\n", (lld) size_link_list,  (lld) (MYTHREAD < full_threads) ? full_rows : full_rows - 1);
                size_link_list = (MYTHREAD < full_threads) ? full_rows : full_rows - 1;
                
                
                Buffer myLinks = initBuffer( sizeof(link_t) * size_link_list + 64);
                if (size_link_list > 0) {
                    assert(the_largest_cc_compact_buffer != NULL);
                    // first copy my links to a Buffer
                    upc_memget(allocFromBuffer(myLinks, sizeof(link_t)*size_link_list),  (shared[] link_t *) & the_largest_cc_compact_buffer[MYTHREAD], sizeof(link_t) * size_link_list);

                    // perform a parallel sort on all local entries
                    DBG("size_link_list=%lld getLengthBuffer(myLinks)=%lld sizeof(link_t)=%lld\n", (lld) size_link_list, (lld) getLengthBuffer(myLinks), (lld) sizeof(link_t));
                    assert( size_link_list == getLengthBuffer(myLinks) / sizeof(link_t) );

                }
                link_t_parallel_sort(myLinks, compareLinks);

                size_link_list = getLengthBuffer(myLinks) / sizeof(link_t);
                DBG("size_link_list=%lld getLengthBuffer(myLinks)=%lld sizeof(link_t)=%lld\n", (lld) size_link_list, (lld) getLengthBuffer(myLinks), (lld) sizeof(link_t));
                if (size_link_list > current_max_size_link_list) {
                    free_chk(local_link_list);
                    local_link_list = (link_t *)malloc_chk(size_link_list * sizeof(link_t));
                    current_max_size_link_list = size_link_list;
                    LOGF("(re-)Allocated buffer to %lld entries %0.3f MB\n", (lld) size_link_list, 1.0 / ONE_MB * size_link_list * sizeof(link_t *));
                }
                if (size_link_list) {
                    memcpy(local_link_list, getStartBuffer(myLinks), sizeof(link_t) * size_link_list);
                }
                freeBuffer(myLinks);

            }


            /* Transform buffer to LINK file entries*/
            for (j = 0; j < size_link_list; j++) {
                link_t *link = local_link_list + j;
                if (link->type == SPLINT) {
                    printfBuffer(linkReportBuffer, "SPLINT\t%s%ld.%d<=>%s%ld.%d\t%ld|%ld|%ld\t%ld\n", objectPrefix, link->v1, link->end1, objectPrefix, link->v2, link->end2, link->f1, link->f2, link->f3, link->f4);
                    mySplints++;
                } else {
                    printfBuffer(linkReportBuffer, "SPAN\t%s%ld.%d<=>%s%ld.%d\t%ld|%ld\t%ld\t%ld\n", objectPrefix, link->v1, link->end1, objectPrefix, link->v2, link->end2, link->f1, link->f2, link->f3, link->f5);
                    mySpans++;
                }
            }

            if ( !isLargest && getLengthBuffer(linkReportBuffer) > ONE_MB - 4*ONE_KB) {
                writeFileBuffer2(linkReportBuffer, gatheredLinkReportFD);
            }
            totalLinks += size_link_list;
        }

        if ( isLargest ) { // always write this file even if empty
            // collectively write linkReport for thread 0
            FILE *largestFD = openCheckpoint0(thread0LinksCheckpoint, "w");
            writeFileBuffer2(linkReportBuffer, largestFD);
            closeCheckpoint0(largestFD);
            if (MYSV.checkpoint_path == NULL) {
                // force a global sync as it will not happen automatically
                saveGlobalCheckpoint(thread0LinksCheckpoint);
            }
        }

        // find the proper reverse_map

        if (isLargest) {
            // local_heap and n_members_in_cc will be my counts within largest CC
            cur_reverse_map_entry = reverse_maps->largest_CC_reverse_map[MYTHREAD];
            LOGF("My members of largest CC: %lld\n", (lld) cur_reverse_map_entry.nEntries);
        } else {
            if (cc_id_to_process >= 0) {
                cur_reverse_map_entry = reverse_map[cc_id_to_process];
            } else {
                memset(&cur_reverse_map_entry, 0, sizeof(cur_reverse_map_entry));
            }
        }
        n_members_in_cc = cur_reverse_map_entry.nEntries;
        local_heap = cur_reverse_map_entry.members_list;

        /* 2)  Gather the involved HMM */
        if (hmm_db != NULL) {
            n_hmm_entries = 0;
            hmm_pos_buf = 0;
            for (j = 0; j < n_members_in_cc; j++) {
                cur_vertex = local_heap[j];
                n_hmm_entries += hmm_db[cur_vertex].n_entries;
            }

            /* Realloc local buffer if need be */
            if (n_hmm_entries > current_max_size_hmm_list) {
                free_chk(local_hmm_list);
                local_hmm_list = (hmm_t *)malloc_chk(n_hmm_entries * sizeof(hmm_t));
                current_max_size_hmm_list = n_hmm_entries;
            }

            for (j = 0; j < n_members_in_cc; j++) {
                cur_vertex = local_heap[j];
                n_hmm_entries = hmm_db[cur_vertex].n_entries;
                if (n_hmm_entries > 0) {
                    upc_memget(&local_hmm_list[hmm_pos_buf], hmm_db[cur_vertex].hmm_list, n_hmm_entries * sizeof(hmm_t));
                    hmm_pos_buf += n_hmm_entries;
                }
            }

            /* Transform buffer entries to HMM file entries */
            assert(gatheredHmmReportFD != NULL);
            for (j = 0; j < hmm_pos_buf; j++) {
                printfBuffer(hmmReportBuffer, "Contig%ld\t%ld\t%ld\t%c\n", local_hmm_list[j].contig_id, local_hmm_list[j].start, local_hmm_list[j].end, local_hmm_list[j].strand);
            }

            if ( isLargest ) {
                // collectively write hmmReport for thread 0
                FILE *largestFD = openCheckpoint0(thread0HmmCheckpoint, "w");
                writeFileBuffer2(hmmReportBuffer, largestFD);
                closeCheckpoint0(largestFD);
                if (MYSV.checkpoint_path == NULL) {
                    // force a global sync as it will not happen automatically
                    saveGlobalCheckpoint(thread0HmmCheckpoint);
                }
            } else if (getLengthBuffer(hmmReportBuffer) > ONE_MB - 4*ONE_KB) {
                writeFileBuffer2(hmmReportBuffer, gatheredHmmReportFD);
            }
            if (hmm_pos_buf) LOGF("Gathered hmm: %lld\n", hmm_pos_buf);
        }

        /* 3) Gather the involved obj entries  */
        /* Realloc local buffer if need be */
        if (n_members_in_cc > current_max_size_obj_list) {
            local_obj_list = (asmObj_t *)realloc_chk(local_obj_list, n_members_in_cc * sizeof(asmObj_t));
            current_max_size_obj_list = n_members_in_cc;
        }

        if (srf_arr != NULL) {
            for (j = 0; j < n_members_in_cc; j++) {
                cur_vertex = local_heap[j];
                int myLenRaw = srf_arr[cur_vertex].len;

                //	    DBG("cc %d, member %d : Scaffold%d, (strlen: %d)\n", i, j, cur_vertex, myLenRaw);
                char *srfString_tmp = malloc_chk((myLenRaw + 1) * sizeof(char));
                srfString_tmp[0] = '\0';
                upc_memget(srfString_tmp, srf_arr[cur_vertex].str, myLenRaw * sizeof(char));
                srfString_tmp[myLenRaw] = '\0';
                assert(strlen(srfString_tmp) == myLenRaw);
                //	    DBG("\tsrf record copied locally (strlen: %d)\n", strlen(srfString_tmp));

                printfBuffer(objReportBuffer, "%s", srfString_tmp);
                free_chk(srfString_tmp);
            }
        } else {
            obj_pos_buf = 0;
            for (j = 0; j < n_members_in_cc; j++) {
                cur_vertex = local_heap[j];
                local_obj_list[obj_pos_buf] = object_db[cur_vertex];
                /* Transform buffer entry to obj file entry*/
                printfBuffer(objReportBuffer, "Contig%ld\t%d\t%f", cur_vertex, local_obj_list[obj_pos_buf].length + merSize - 1, local_obj_list[obj_pos_buf].depth);
                if (local_obj_list[obj_pos_buf].diplotig > -1) {
                    printfBuffer(objReportBuffer, "\t%lld\n", (lld)local_obj_list[obj_pos_buf].diplotig);
                } else {
                    printfBuffer(objReportBuffer, "\n");
                }
                obj_pos_buf++;
            }
        }
        if ( isLargest ) {
            // collectively write objReport for thread 0
            FILE *largestFD = openCheckpoint0(thread0ObjCheckpoint, "w");
            writeFileBuffer2(objReportBuffer, largestFD);
            closeCheckpoint0(largestFD);
            if (MYSV.checkpoint_path == NULL) {
                // force a global sync as it will not happen automatically
                saveGlobalCheckpoint(thread0ObjCheckpoint);
            }
        } else if (getLengthBuffer(objReportBuffer) > ONE_MB - 4*ONE_KB) {
            writeFileBuffer2(objReportBuffer, gatheredObjReportFD);
        }

        totalObjects += n_members_in_cc;
    }

    // final flush of buffers

    if (gatheredHmmReportFD != NULL) {
        writeFileBuffer2(hmmReportBuffer, gatheredHmmReportFD);
        fclose_track(gatheredHmmReportFD);
    }
    freeBuffer(hmmReportBuffer);

    writeFileBuffer2(linkReportBuffer, gatheredLinkReportFD);
    fclose_track(gatheredLinkReportFD);
    freeBuffer(linkReportBuffer);

    writeFileBuffer2(objReportBuffer, gatheredObjReportFD);
    fclose_track(gatheredObjReportFD);
    freeBuffer(objReportBuffer);

    LOGF("Prepared mySplints=%lld mySpans=%lld objects=%lld totalLinks=%lld in %0.3f s\n", (lld) mySplints, (lld) mySpans, (lld) totalObjects, (lld) totalLinks, upc_ticks_to_ns(upc_ticks_now() - start_tick) / 1000000000.0);

    UPC_LOGGED_BARRIER;
    end_tick = upc_ticks_now();
    serial_printf("Prepared oNo inputs in %.5f seconds\n", upc_ticks_to_ns(end_tick - start_tick) / 1000000000.0);

    free_chk(local_link_list);
    free_chk(local_hmm_list);
    free_chk(local_obj_list);

    return;
}

int64_t get_nScaf_from_srfFile(GZIP_FILE srfReportFD)
{
  int64_t total_scaffolds = 0;
  char scaff_line[1000];
  char scaff_line_save[1000];
  int64_t new_id;
  char *token, *aux;
  int64_t additional = 0;
  while (GZIP_GETS(scaff_line, 1000, srfReportFD) != NULL) {
        additional = 1;
        token = strtok_r(scaff_line, "\t", &aux);
        new_id = atol(token + 8);
        if (new_id > total_scaffolds) {
            total_scaffolds = new_id;
        }
    }
  total_scaffolds += additional;
  return total_scaffolds;
}


static int64_t renumberScaffolds(char *parCC_srfFileOut, char *finalSrfFile, char *_outScaffoldReportFilePrefix);

int parCC_main(int argc, char **argv)
{
    char *_contigReportFilePrefix = NULL;
    char *_inScaffoldReportFilePrefix = NULL;
    char *_outScaffoldReportFilePrefix = NULL;
    char *_base_dir;
    char *_contig_filename;
    FILE *_linkMetaFD = NULL;
    libInfoType _libInfo[MAX_LIBRARIES];
    int64_t n_vertices;
    int merSize = -1;
    char *merSizeStr = NULL;
    int _nLinkFiles = 0;
    char _s16s23_filename[MAX_FILE_PATH];

    _s16s23_filename[0] = '\0';
    char link_meta_name[MAX_FILE_PATH];
    link_meta_name[0] = '\0';
    char parCC_link_meta_name[MAX_FILE_PATH+50];
    parCC_link_meta_name[0] = '\0';
    int pairThresholdMin = -1;
    char *pairThresholdsStr = NULL;
    int minimum_link_support = 1;
    char *diploidMode = NULL;
    char *diploidDepthThld = NULL;
    int metagenome = 0;
    int long_reads_present = 0;
    
    //filters to control CC size (avoid size explosion due to mega-repeat nodes);
    //cutoff are arbitrary for now; need to replace w data-sensitive functions
    int filter_length = 100;
    double filter_depth = 500.0;
    int filter_connections = 0;
    int onoRound = 0; //needed only for checkpointing

    option_t *optList, *thisOpt;
    optList = NULL;

    optList = GetOptList(argc, argv, "l:m:n:c:s:o:d:r:B:C:F:H:P:L:D:X:MR");

    print_args(optList, __func__);

    while (optList != NULL) {
        char filename[MAX_FILE_PATH];
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'l':
            strcpy(filename, thisOpt->argument);
            strcpy(link_meta_name, thisOpt->argument);
            snprintf(parCC_link_meta_name, MAX_FILE_PATH+10, "parCC_%s", filename);
            _linkMetaFD = fopen_rank_path(filename, "r", -1);
            break;
        case 'H':
            strcpy(_s16s23_filename, thisOpt->argument);
            break;
        case 'm':
            merSize = atoi(thisOpt->argument);
            merSizeStr = thisOpt->argument;
            break;
        case 'n':
            n_vertices = atol(thisOpt->argument);
            break;
        case 'c':
            _contigReportFilePrefix = thisOpt->argument;
            break;
        case 's':
            _inScaffoldReportFilePrefix = thisOpt->argument;
            break;
        case 'o':
            _outScaffoldReportFilePrefix = thisOpt->argument;
            break;
        case 'r':
            onoRound = atoi(thisOpt->argument);
            break;
        case 'C':
            _contig_filename = thisOpt->argument;
            break;
        case 'B':
            _base_dir = thisOpt->argument;
            break;
        case 'P':
            pairThresholdsStr = thisOpt->argument;
            pairThresholdMin = atoi(thisOpt->argument);
	    minimum_link_support = pairThresholdMin;
            //            serial_printf("Using pair threshold %d\n", _pairThreshold);
            break;
        case 'L':
            filter_length = atol(thisOpt->argument);
            break;
        case 'F':
            filter_depth = atof(thisOpt->argument);
            break;
        case 'D':
            diploidMode = thisOpt->argument; 
            break;
        case 'M':
            metagenome = 1;
            break;	    
        case 'd':
            diploidDepthThld = thisOpt->argument;
            break;
        case 'R':
	  /*ToDo: replace this with library-specific flag in link_meta_file */
	    long_reads_present = 1;
            break;
        case 'X':
            filter_connections = atoi(thisOpt->argument);
            break;
	    
        default:
            break;
        }
    }
    char aux_line[1000];
    char line[1000];
    char new_links_full_path[1000];

    if (merSize < 0) {
        SDIE("please specify -m mer_size\n");
    }
    if (pairThresholdsStr == NULL) {
        SDIE("please specify -P pair_threshold(s)\n");
    }
    if (parCC_link_meta_name[0] == '\0') {
        SDIE("No link meta option was provided: -l\n");
    }

    if (_contigReportFilePrefix == NULL && _inScaffoldReportFilePrefix == NULL) {
        SDIE("please specify either -c crf or -s srf as input");
    }


    LOG_VERSION;

    upc_tick_t start_tick, end_tick;
    uint64_t time_in_ns;

    /* The distributed data structures that are built in order to prepare inputs for embarassing parallel oNo executions */
    shared[1] int64_t * ccLabels = NULL;
    reverse_maps_t reverse_maps;
    memset(&reverse_maps, 0, sizeof(reverse_maps_t));
    shared[1] reverse_map_t * reverse_map = NULL;
    shared[1] link_cc_t * links_in_cc = NULL;
    shared[1] link_t * the_largest_cc_compact_buffer = NULL;
    int64_t *list_of_assigned_cc_ids = NULL;
    int64_t n_assigned = 0;
    shared[1] asmObj_t * object_db = NULL;
    shared [1] SharedString * srf_arr = NULL;

    shared[1] hmm_db_t * hmm_db = NULL;
    int hasHmm = 0;
    shared[] link_t * compact_buffer_cc = NULL;
    shared[] hmm_t * compact_buffer_hmm = NULL;
    int64_t scaffolds_found_in_call;
    int64_t i;

    upc_tick_t start_parcc = upc_ticks_now();

    if (!MYTHREAD) {
        // MYTHREAD == 0 allways writes to disk to support manual restart
        _base_dir = ".";
    }

    get_rank_path(link_meta_name, -1);
    {
        char filename[MAX_FILE_PATH+50];
        strcpy(filename, parCC_link_meta_name);
        sprintf(parCC_link_meta_name, "%s/%.*s", _base_dir, MAX_FILE_PATH, filename);
    }
    FILE *new_link_metaFD = NULL;
    new_link_metaFD = fopen_rank_path(parCC_link_meta_name, "w+", MYTHREAD);

    if (filter_length > 0) {
        serial_printf("Filtering contigs with length < %d and (%s depth > %0.1f or %s connections > %d)\n", filter_length, (filter_depth > 0 ? "" : "(ignoring)"), filter_depth, (filter_connections > 0 ? "" : "(ignoring)"), filter_connections);
    }

    while (fgets(line, 1000, _linkMetaFD) != NULL) {
        strcpy(aux_line, line);
        splitLinkMetaLine(aux_line, &_libInfo[_nLinkFiles]);

        serial_printf("Read lib %d (%s): insert size %d, stddev %d, link file %s\n",
                      _nLinkFiles, _libInfo[_nLinkFiles].libName,
                      _libInfo[_nLinkFiles].insertSize, _libInfo[_nLinkFiles].stdDev,
                      _libInfo[_nLinkFiles].linkFileNamePrefix);

        sprintf(new_links_full_path, "%s/parCC_%s", _base_dir, _libInfo[_nLinkFiles].linkFileNamePrefix);
        get_rank_path(new_links_full_path, MYTHREAD);
        fprintf(new_link_metaFD, "%s\t%d\t%d\t%s\n", _libInfo[_nLinkFiles].libName, _libInfo[_nLinkFiles].insertSize, _libInfo[_nLinkFiles].stdDev, new_links_full_path);

        _nLinkFiles++;
    }
    fclose_track(_linkMetaFD);
    fclose_track(new_link_metaFD);

    /* Form proper filename paths and open proper files  */
    char curLinkFile[MAX_FILE_PATH * 2 + 20];
    sprintf(curLinkFile, "%s" GZIP_EXT, _libInfo[0].linkFileNamePrefix);
    LOGF("curLinkFile=%s\n", curLinkFile);



    char curCrfFileCkpt[MAX_FILE_PATH * 2 + 20];
    char parCC_crfFile[MAX_FILE_PATH * 2 + 20];
    char curSrfFileCkpt[MAX_FILE_PATH * 2 + 20];
    char parCC_srfFileIn[MAX_FILE_PATH * 2 + 20];

    if (_contigReportFilePrefix != NULL) {
        sprintf(curCrfFileCkpt, "%s.txt" GZIP_EXT, _contigReportFilePrefix);
        sprintf(parCC_crfFile, "%s/parCC_%s" GZIP_EXT, _base_dir,  _contigReportFilePrefix);
        get_rank_path(parCC_crfFile, MYTHREAD);
    } else {
        sprintf(curSrfFileCkpt, "%s" GZIP_EXT, _inScaffoldReportFilePrefix);
        snprintf(parCC_srfFileIn, MAX_FILE_PATH*2+20, "%s/parCC-in_%s", _base_dir, _inScaffoldReportFilePrefix);
        get_rank_path(parCC_srfFileIn, MYTHREAD);
    }

    char myContigFileName[MAX_FILE_PATH + 20];
    snprintf(myContigFileName, MAX_FILE_PATH + 20, "%s.fasta" GZIP_EXT, _contig_filename);
    restoreCheckpoint( myContigFileName ); // restore if necessary
    checkpointToLocal(myContigFileName, myContigFileName);

    char parCC_LinkFile[MAX_FILE_PATH + 50];
    sprintf(parCC_LinkFile, "%s/parCC_%s", _base_dir, _libInfo[0].linkFileNamePrefix);
    get_rank_path(parCC_LinkFile, MYTHREAD);

    char parCC_hmmFile[MAX_FILE_PATH + 50];
    sprintf(parCC_hmmFile, "%s/parCC_hmmReport", _base_dir);
    get_rank_path(parCC_hmmFile, MYTHREAD);

    // Mutliple parCC ono outputs (different pair-thresholds) may be produced; The best one will be picked prior to building the final srf file

    char parCC_srfFileOut[MAX_FILE_PATH + 50]; 
    sprintf(parCC_srfFileOut,  "%s/parCC-out_%s-best", _base_dir, _outScaffoldReportFilePrefix);
    get_rank_path(parCC_srfFileOut, MYTHREAD); 
      
    char finalSrfFile[MAX_FILE_PATH + 50];
    sprintf(finalSrfFile, "%s" GZIP_EXT, _outScaffoldReportFilePrefix);


    char readyFile[MAX_FILE_PATH];
    sprintf(readyFile, "parCCFinished_oNo-%d_is_READY", onoRound);
    get_rank_path(readyFile, -1);

    const char *manualOnoCmd = ".manualOnoRestartCommand";
    const char *manualOnoCmdTmp = ".manualOnoRestartCommand.tmp";
    if (!MYTHREAD) {
        unlink(manualOnoCmdTmp); // okay to fail
    }

    int pThresholds[6];
    int pidx = -1;
    char *token, *aux;
    token = strtok_r(pairThresholdsStr, ",", &aux);
    for (pidx = 0; token != NULL; pidx++) {
      if (pidx >=6 )
        DIE("Too many p-thresholds passed! (limit is 6)");
      pThresholds[pidx] = atoi(token);
      token = strtok_r(NULL, ",", &aux);
    }
    assert(pidx > -1);

    int isManualRestartAndReady = 0;
    if (!MYTHREAD) {
        // check for manualRestart and ready files
        if (does_file_exist(manualOnoCmd)) {
            serial_printf("Checking for checkpoint file %s\n", readyFile);
            if (does_file_exist(readyFile)) {
                isManualRestartAndReady = 1; 
            }
        } else {
            if (!MYTHREAD && does_file_exist(readyFile)) {
                serial_printf("There is no manual restart command file, so there should be no checkpoint...\n");
                unlink(readyFile);
            }
        }
    }
    isManualRestartAndReady = broadcast_int(isManualRestartAndReady, 0);
    UPC_LOGGED_BARRIER;
    int allAreReady = 0;
    if (isManualRestartAndReady) {
        // Now every thread will verify that all output scaffold files exist.  if not parCC needs to be rerun, some calls to ono may be skipped.
        allAreReady = 1;

        for (int pi = 0; pi < pidx; pi++) {
          int pTld = pThresholds[pi];
          char parCC_srfFile_pi[MAX_FILE_PATH * 2 + 20];
          snprintf(parCC_srfFile_pi, MAX_FILE_PATH, "%s/parCC-out_%s-p%d", _base_dir, _outScaffoldReportFilePrefix, pTld);
          get_rank_path(parCC_srfFile_pi, MYTHREAD);
          if (!does_file_exist(parCC_srfFile_pi)) {
              allAreReady = 0;
          }
        }
        allAreReady = reduce_int(allAreReady, UPC_ADD, ALL_DEST);
        LOGF("Detected a manual restart scenario. allAreReady = %d\n", allAreReady);
    }

    if (allAreReady != THREADS) {

        int nargsUsed;
        int maxargs = 17;
        char **all_args = (char**) calloc_chk((maxargs+1)*pidx, sizeof(char*));

        GZIP_FILE curLinkFileFD = openCheckpoint(curLinkFile, "r");

        GZIP_FILE in_reportFD = (_contigReportFilePrefix != NULL) ? openCheckpoint(curCrfFileCkpt, "r") : openCheckpoint(curSrfFileCkpt, "r");

        FILE *gatheredLinkReportFD = fopen_chk(parCC_LinkFile, "w");

        FILE *gatheredObjReportFD = (_contigReportFilePrefix != NULL) ? fopen_chk(parCC_crfFile, "w") : fopen_chk(parCC_srfFileIn, "w");

        FILE *gatheredHmmReportFD = NULL;

        UPC_LOGGED_BARRIER;

        build_object_db(in_reportFD, n_vertices, merSize, &object_db, (_inScaffoldReportFilePrefix != NULL) ? &srf_arr : NULL);

        closeCheckpoint(in_reportFD);

        LOG_SET_FLUSH(1); // FIXME once scaling issues are solved on knl for thread 0

        UPC_LOGGED_BARRIER;
        if (_s16s23_filename[0] != '\0') {
#ifdef HIPMER_EMBED_HMMER
            // perform the hmm search against contigs
            build_hmm_db(n_vertices, _base_dir, _s16s23_filename, myContigFileName, &hmm_db, &compact_buffer_hmm);
            hasHmm = 1;
 
            gatheredHmmReportFD = fopen_chk(parCC_hmmFile, "w");
#else
            SWARN("hmm file is provided, but HipMer was not compiled with embedded hmmer support.  Please recompile with HIPMER_EMBED_HMMER=1\n");
#endif
        } else {
            serial_printf("No hmm file provided, so no search for rDNA will be performed\n");
            assert(!hasHmm);
            assert(hmm_db == NULL);
            assert(compact_buffer_hmm == NULL);
        }

        serial_printf("Starting connected components calculations\n");
        UPC_LOGGED_BARRIER;
        int mustRerun = find_connected_components(curLinkFileFD, minimum_link_support, n_vertices, &reverse_maps, &ccLabels, &list_of_assigned_cc_ids, &n_assigned, object_db, filter_length, filter_depth, filter_connections, hmm_db);

        UPC_LOGGED_BARRIER;
        redistribute_links(curLinkFileFD, minimum_link_support, n_vertices, ccLabels, &links_in_cc, &compact_buffer_cc, &the_largest_cc_compact_buffer, object_db, filter_length, filter_depth, filter_connections, hmm_db);

        closeCheckpoint(curLinkFileFD);

        UPC_LOGGED_BARRIER;
        prepare_oNo_inputs(n_assigned, merSize, list_of_assigned_cc_ids, links_in_cc, the_largest_cc_compact_buffer, &reverse_maps, hmm_db, object_db, gatheredLinkReportFD, gatheredObjReportFD, gatheredHmmReportFD, (_inScaffoldReportFilePrefix != NULL) ? srf_arr : NULL);

        UPC_LOGGED_BARRIER;

        // prepare thread 0 files that were generated collectively

        syncCheckpoint(thread0LinksCheckpoint);
        unlinkLocalCheckpoint(thread0LinksCheckpoint);
        if (hmm_db) {
            syncCheckpoint(thread0HmmCheckpoint);
            unlinkLocalCheckpoint(thread0HmmCheckpoint);
        }
        syncCheckpoint(thread0ObjCheckpoint);
        unlinkLocalCheckpoint(thread0ObjCheckpoint);
        UPC_LOGGED_BARRIER;

        if (MYTHREAD == 0) {
            // Thread 0 will rename the global checkpoints to its parCC file (the local filesystem)
            char global[MAX_FILE_PATH];

            checkpointToGlobal(thread0LinksCheckpoint, global);
            if (get_file_size(parCC_LinkFile) > 0) DIE("Expected my parCC_LinkFile %s to be 0 bytes not %lld!\n", parCC_LinkFile, (lld) get_file_size(parCC_LinkFile));
            if (unlink(parCC_LinkFile) != 0) DIE("Could not unlink my parCC_LinkFile %s! (%s)\n", parCC_LinkFile, strerror(errno));
            if (rename(global, parCC_LinkFile)) DIE("Could not rename %s to %s! (%s)\n", global, parCC_LinkFile, strerror(errno));

            if (hmm_db) {
                checkpointToGlobal(thread0HmmCheckpoint, global);
                if (get_file_size(parCC_hmmFile) > 0) DIE("Expected my parCC_hmmFile %s to be 0 bytes not %lld!\n", parCC_hmmFile, (lld) get_file_size(parCC_hmmFile));
                if (unlink(parCC_hmmFile) != 0) DIE("Could not unlink my parCC_hmmFile %s! (%s)\n", parCC_hmmFile, strerror(errno));
                if (rename(global, parCC_hmmFile)) DIE("Could not rename %s to %s! (%s)\n", global, parCC_hmmFile, strerror(errno));
            }

            checkpointToGlobal(thread0ObjCheckpoint, global);
            const char *parCC_objFile =  (_contigReportFilePrefix != NULL) ? parCC_crfFile : parCC_srfFileIn;
            if (get_file_size(parCC_objFile) > 0) DIE("Expected my parCC_objFile %s to be 0 bytes not %lld!\n", parCC_objFile, (lld) get_file_size(parCC_objFile));
            if (unlink(parCC_objFile) != 0) DIE("Could not unlink my parCC_objFile %s! (%s)\n", parCC_objFile, strerror(errno));
            if (rename(global, parCC_objFile)) DIE("Could not rename %s to %s! (%s)\n", global, parCC_objFile, strerror(errno));

            LOGF("Renamed temporay global checkpoints to my parCC files\n");
        } 
      
        /* Free remaining distributed data structures */
        upc_tick_t start_clean = upc_ticks_now();
        serial_printf("Releasing some memory\n");
        if (hmm_db) {
            UPC_ALL_FREE_CHK(hmm_db);
        }
        if (compact_buffer_cc) {
            UPC_FREE_CHK(compact_buffer_cc);
        }
        if (compact_buffer_hmm) {
            UPC_FREE_CHK(compact_buffer_hmm);
        }
        UPC_LOGGED_BARRIER;
        if (the_largest_cc_compact_buffer) {
            UPC_ALL_FREE_CHK(the_largest_cc_compact_buffer);
        }
        if (ccLabels) {
            UPC_ALL_FREE_CHK(ccLabels);
        }
        if (links_in_cc) {
            UPC_ALL_FREE_CHK(links_in_cc);
        }
        if (object_db) {
            UPC_ALL_FREE_CHK(object_db);
        }
        if (_inScaffoldReportFilePrefix != NULL) {
            for (int v = MYTHREAD; v < n_vertices; v += THREADS) {
                if (srf_arr[v].str != NULL) {
                    UPC_FREE_CHK(srf_arr[v].str);
                }
            }
            UPC_ALL_FREE_CHK(srf_arr);
        }

        int64_t cc_id_to_process;
        if (reverse_maps.reverse_map) {
            reverse_map = reverse_maps.reverse_map;
            reverse_map_t cur_reverse_map_entry;
            for (i = 0; i < n_assigned; i++) {
                assert(list_of_assigned_cc_ids);
                cc_id_to_process = list_of_assigned_cc_ids[i];
                cur_reverse_map_entry = reverse_map[cc_id_to_process];
                UPC_FREE_CHK(cur_reverse_map_entry.members_list);
            }
        }
        if (reverse_maps.largest_CC_reverse_map) {
            UPC_FREE_CHK(reverse_maps.largest_CC_reverse_map[MYTHREAD].members_list);
            UPC_ALL_FREE_CHK(reverse_maps.largest_CC_reverse_map); 
        }
        if (list_of_assigned_cc_ids) {
            free_chk(list_of_assigned_cc_ids);
        }

        // set a counter that is not owned by thread 0!
        shared [1] int64_t *_onoCompleteCount = NULL;
        UPC_ALL_ALLOC_CHK(_onoCompleteCount, THREADS, sizeof(int64_t));
        _onoCompleteCount[MYTHREAD] = 0;
        shared [] int64_t *onoCompleteCount = (shared [] int64_t *) &_onoCompleteCount[THREADS-1];

        UPC_LOGGED_BARRIER;
        if (reverse_maps.reverse_map) {
            UPC_ALL_FREE_CHK(reverse_maps.reverse_map);
        }
        serial_printf("Cleaned up in %0.6f s, %0.6f s in total\n", ELAPSED_TIME(start_clean), ELAPSED_TIME(start_parcc));

        /* Run the serial oNo algorithm on the current connected component with inputs: CRF/SRF file, LINK file, HMM file, my_total_scaffolds, proper_links_meta file, proper Scaffold report filename  */
        //UPC_LOGGED_BARRIER;

        // parse and translate ono_main's argv to embedded ono arguments of
        // thread-based filepath names
        // preparing necessary inputs and consoidaing outputs
        // ** all work should be in /dev/shm, unless overridden by a future config argment **

        // Usage: ./oNo_meta.pl <-m merSize> <-l linkDataFile> <-R rRNAHitsList> (<-s scaffoldReportFile> || <-c contigReportFile>)  <<-p pairThreshold     >> <<-B blessedLinkFile>> <<-O IDoffset>> <<-o srfOutputFileName>> <<-V(erbose)>> <<-L(ogfile) logPath>>

        upc_tick_t start_ono = upc_ticks_now();
        // prepare global cc id offsets for soon-to-be duplicated contigs
        int64_t idOffset = 0; // reduce_prefix_long(n_assigned, UPC_ADD, NULL) - n_assigned;
        char idOffsetStr[16];
        sprintf(idOffsetStr, "%lld", (lld)idOffset);
        char binname[32];
        char *cmd = calloc_chk(8192 * pidx,1);

	if (long_reads_present) {
	  serial_printf("Long reads present; will run ono_CCS\n");   
	}else if (diploidMode) {
          serial_printf("Diploid mode detected; will run oNo_2D\n");
        }else if (metagenome){
          serial_printf("metagenome mode detected; will run oNo_meta\n");
        } else {
	  serial_printf("Haploid mode; will run oNo4\n");
	}
        
        double startOno = now();

        /* Iterate ono over p-thresholds and pick the best output at the end */
        for (int pi = 0; pi < pidx; pi++) {
          
          int pTld = pThresholds[pi];
          //          int pTld = pairThresholdMin + pi;

          LOGF("\t ono iteration %d (p=%d)...\n", pi, pTld);

          if (strlen(cmd)) {
             strcat(cmd, " && ");
          }
          
          char parCC_srfFile_pi[MAX_FILE_PATH * 2 + 20];
          snprintf(parCC_srfFile_pi, MAX_FILE_PATH, "%s/parCC-out_%s-p%d", _base_dir, _outScaffoldReportFilePrefix, pTld);
          get_rank_path(parCC_srfFile_pi, MYTHREAD);
          
          char pairThreshold_i[16];
          sprintf(pairThreshold_i, "%d", pTld);


          int onoRet = -1;

          int cmdOffset = strlen(cmd);
	  if (long_reads_present) { //ono_CCS args
            sprintf(binname, "oNo_CCS");
            const int nargs = 13;
            nargsUsed = nargs;
            char **ono_argv = all_args + (nargs+1)*pi;
            char *tmp[nargs + 1] =      { does_file_exist(parCC_srfFile_pi) && !is_file_newer_than(link_meta_name, parCC_srfFile_pi) ? strdup("true") : strdup(binname), // only execute if necessary on restart
                                          strdup("-m"),                                                                        strdup(merSizeStr),
                                          strdup("-l"),                                                                        strdup(parCC_link_meta_name),
                                          (_contigReportFilePrefix != NULL) ?  strdup("-c") : strdup("-s"),
                                          (_contigReportFilePrefix != NULL) ?  strdup(parCC_crfFile) : strdup(parCC_srfFileIn),
                                          strdup("-p"),                                                                        strdup(pairThreshold_i),
                                          strdup("-O"),                                                                        strdup(idOffsetStr),
                                          strdup("-o"),                                                                        strdup(parCC_srfFile_pi),
                                          NULL };
            for (int i = 0; tmp[i] != NULL; i++) {
                strcat(cmd, tmp[i]);
                strcat(cmd, " ");
                ono_argv[i] = tmp[i];
            }
            serial_printf("preparing oNo_CCS: %s\n", cmd + cmdOffset);
	    
	  } else if (diploidMode) { //ono_2D args
            sprintf(binname, "oNo_2D");
            const int nargs = 17;
            nargsUsed = nargs;
            char **ono_argv = all_args + (nargs+1)*pi;
            char *tmp[nargs + 1] =      { mustRerun == 0 && does_file_exist(parCC_srfFile_pi) && !is_file_newer_than(link_meta_name, parCC_srfFile_pi) ? strdup("true") : strdup(binname), // only execute if necessary on restart
                                          strdup("-m"),                                                                        strdup(merSizeStr),
                                          strdup("-l"),                                                                        strdup(parCC_link_meta_name),
                                          (_contigReportFilePrefix != NULL) ?  strdup("-c") : strdup("-s"),
                                          (_contigReportFilePrefix != NULL) ?  strdup(parCC_crfFile) : strdup(parCC_srfFileIn),
                                          strdup("-p"),                                                                        strdup(pairThreshold_i),
                                          strdup("-O"),                                                                        strdup(idOffsetStr),
                                          strdup("-o"),                                                                        strdup(parCC_srfFile_pi),
                                          strdup("-M"),                                                                        strdup(diploidMode),
                                          strdup("-D"),                                                                        strdup(diploidDepthThld),
                                          NULL };
            for (int i = 0; tmp[i] != NULL; i++) {
                strcat(cmd, tmp[i]);
                strcat(cmd, " ");
                ono_argv[i] = tmp[i];
            }
            serial_printf("preparing oNo_2D: %s\n", cmd + cmdOffset);

          } else if (metagenome) { //ono_meta args

            sprintf(binname, "oNo_meta");
	    //            const int nargs = 16;
	                const int nargs = 15;
            nargsUsed = nargs;
            char **ono_argv = all_args + (nargs+1)*pi;
            char *tmp[nargs + 1] =      { mustRerun == 0 && does_file_exist(parCC_srfFile_pi) && !is_file_newer_than(link_meta_name, parCC_srfFile_pi) ? strdup("true") : strdup(binname), // only execute if necessary on restart
                                          strdup("-m"),                                                                        strdup(merSizeStr),
                                          strdup("-l"),                                                                        strdup(parCC_link_meta_name),
                                          strdup("-R"),                                                                        strdup(hasHmm == 0 ? "/dev/null" : parCC_hmmFile),
                                          (_contigReportFilePrefix != NULL) ?  strdup("-c") : strdup("-s"),
                                          (_contigReportFilePrefix != NULL) ?  strdup(parCC_crfFile) : strdup(parCC_srfFileIn),
                                          strdup("-p"),                                                                        strdup(pairThreshold_i),
                                          strdup("-O"),                                                                        strdup(idOffsetStr),
                                          strdup("-o"),                                                                        strdup(parCC_srfFile_pi),
                                          NULL };
            for (int i = 0; tmp[i] != NULL; i++) {
                strcat(cmd, tmp[i]);
                strcat(cmd, " ");
                ono_argv[i] = tmp[i];
            }
            serial_printf("preparing oNo_meta: %s\n", cmd + cmdOffset);

          } else { // ono4 args
            sprintf(binname, "oNo4");
            const int nargs = 13;
            nargsUsed = nargs;
            char **ono_argv = all_args + (nargs+1)*pi;
            char *tmp[nargs + 1] =      { does_file_exist(parCC_srfFile_pi) && !is_file_newer_than(link_meta_name, parCC_srfFile_pi) ? strdup("true") : strdup(binname), // only execute if necessary on restart
                                          strdup("-m"),                                                                        strdup(merSizeStr),
                                          strdup("-l"),                                                                        strdup(parCC_link_meta_name),
                                          (_contigReportFilePrefix != NULL) ?  strdup("-c") : strdup("-s"),
                                          (_contigReportFilePrefix != NULL) ?  strdup(parCC_crfFile) : strdup(parCC_srfFileIn),
                                          strdup("-p"),                                                                        strdup(pairThreshold_i),
                                          strdup("-O"),                                                                        strdup(idOffsetStr),
                                          strdup("-o"),                                                                        strdup(parCC_srfFile_pi),
                                          NULL };
            for (int i = 0; tmp[i] != NULL; i++) {
                strcat(cmd, tmp[i]);
                strcat(cmd, " ");
                ono_argv[i] = tmp[i];
            }
            serial_printf("preparing oNo4: %s\n", cmd + cmdOffset);
	    
	  }
	  
        } // for pi

        if (!MYTHREAD) {
           sprintf(cmd + strlen(cmd), " && touch %s\n", readyFile);
           put_str_in_file(manualOnoCmdTmp, cmd);
        }

        // now run them
        for (int pi = 0; pi < pidx; pi++) {
          int pTld = pThresholds[pi];
          int onoRet = -1;

          char ** ono_argv = all_args + (nargsUsed+1)*pi;
          serial_printf("Running %s for pi=%d\n", ono_argv[0], pi);
          
          if (ono_argv[0] && strcmp(ono_argv[0], "true") == 0) {
             serial_printf("Skipping already completed ono execution\n");
             onoRet = 0;
          } else {
	    if (long_reads_present) {
	      onoRet = oNo_CCS_main(nargsUsed, ono_argv);
	    } else if (diploidMode) { 
              onoRet = oNo_2D_main(nargsUsed, ono_argv);
            } else if (metagenome) {
              onoRet = oNo_meta_main(nargsUsed, ono_argv);
            } else {
	      onoRet = oNo4_main(nargsUsed, ono_argv);
	    }
	    
          }

          if (onoRet != 0) {
            DIE("%s returned non-zero status: %d\n", binname, onoRet);
          }

          LOGF("Completed oNo with paired-end threshold %d  in %0.6f s\n", pTld, ELAPSED_TIME(start_ono));
          
        }  /* end of ono iterations */

        for (int i = 0; i < maxargs*pidx; i++) {
            if (all_args[i]) free(all_args[i]);
        }
        free_chk(all_args);
        free_chk(cmd);

        LOGF("Thread done with ono iterations in %0.3f s\n", now() - startOno);
        
        int64_t testCount = 0;
        if (MYTHREAD) {
          UPC_ATOMIC_FADD_I64(&testCount, onoCompleteCount, 1);
          LOGF("I am %d out of %d (+1) THREADS to finish\n", testCount+1, THREADS-1);
        }
        if (testCount+1 == THREADS-1) {
            if (rename(manualOnoCmdTmp, manualOnoCmd)) {
                if (does_file_exist(manualOnoCmdTmp)) {
                    WARN("Could not rename %s to %s! (%s)\n", manualOnoCmdTmp, manualOnoCmd, strerror(errno));
                }
            } else {
                printf("\n\nAll threads but thread 0 have completed %s in %0.3f s. Detected by thread %d\n", binname, ELAPSED_TIME(start_ono), MYTHREAD);
                printf("If thread 0 runs out of memory you can restart by sourcing:\n\t%s\n\n\n", manualOnoCmd);
                fflush(stdout);
            }
            fflush(stdout);
        }

        UPC_LOGGED_BARRIER;
        UPC_ALL_FREE_CHK(_onoCompleteCount);
        serial_printf("finished running oNo in %0.6f s\n",  ELAPSED_TIME(start_ono));

        if (!MYTHREAD) {
            unlink(manualOnoCmd);
        }
        serial_printf("No need for the manual restart, Thread 0 completed fine.\n");
        
    } else { /* readyFile existed */
      serial_printf("Skipping CC calculations and oNo, as this looks like a manual ono restart\n");
    }


    // now pick the best
    int64_t nScafMin = n_vertices;  //initialize to most possible scaffolds we can have
    char best_parCC_srfFile[MAX_FILE_PATH + 20];
    best_parCC_srfFile[0] = '\0';
    char best_maskFile[MAX_FILE_PATH + 40];
    best_maskFile[0] = '\0'; 
    for (int pi = 0; pi < pidx; pi++) {
          /* Pick best current srf file based on the number of scaffolds */
          int pTld = pThresholds[pi];
          
          char parCC_srfFile_pi[MAX_FILE_PATH * 2 + 20];
          snprintf(parCC_srfFile_pi, MAX_FILE_PATH * 2 + 20, "%s/parCC-out_%s-p%d", _base_dir, _outScaffoldReportFilePrefix, pTld);
          get_rank_path(parCC_srfFile_pi, MYTHREAD);

          GZIP_FILE srf_pi_FD = GZIP_OPEN(parCC_srfFile_pi, "r"); 
          int64_t nScaf_i = get_nScaf_from_srfFile(srf_pi_FD);
          assert(nScaf_i <= n_vertices);
          
          GZIP_CLOSE(srf_pi_FD);

          LOGF("%d scaffolds in srf file %s\n", nScaf_i, parCC_srfFile_pi);

          if (nScaf_i <= nScafMin) {
            strcpy(best_parCC_srfFile, parCC_srfFile_pi);
            if (diploidMode && atoi(diploidMode) == 2) { 
              snprintf(best_maskFile, MAX_FILE_PATH + 40, "%s.contigs_mask", best_parCC_srfFile);
            }

            nScafMin = nScaf_i;
          }
    }

    // and hard-link the best to the -best name
    if (unlink(parCC_srfFileOut)) {
        LOGF("Could not unlink %s. (%s)... okay to fail\n", parCC_srfFileOut, strerror(errno));
    }
    link_chk(best_parCC_srfFile, parCC_srfFileOut);
    assert( access( parCC_srfFileOut, F_OK ) != -1 );

    if (diploidMode && atoi(diploidMode) == 2) {
        char best_maskOut[MAX_FILE_PATH*2 + 40];
        snprintf(best_maskOut, MAX_FILE_PATH*2 + 40, "%s.contigs_mask", parCC_srfFileOut);
        unlink(best_maskOut);
        if (does_file_exist(best_maskFile)) {
           link_chk(best_maskFile, best_maskOut);
        }
    }

    renumberScaffolds(parCC_srfFileOut, finalSrfFile, _outScaffoldReportFilePrefix);

    LOG_SET_FLUSH(0); // FIXME
    
    if (!MYTHREAD) {
        serial_printf("Successful ono, removing the manual command file.\n");
        if (does_file_exist(manualOnoCmd)) {
            unlink(manualOnoCmd);
        }
        if (does_file_exist(manualOnoCmdTmp)) {
            unlink(manualOnoCmdTmp);
        }
        if (does_file_exist(readyFile)) {
            unlink(readyFile);
        }
    }

    return 0;
}

int64_t renumberScaffolds(char *parCC_srfFileOut, char *finalSrfFile, char *_outScaffoldReportFilePrefix)
{
    /* Revisit scaffold result and add to each scaffold ID the scaffold_id_block_start */
    upc_tick_t start_renumber = upc_ticks_now();
    char scaff_line[1000];
    char scaff_line_save[1000];
    int64_t new_id;
    char *token, *aux;
    int64_t my_total_scaffolds = 0;
    GZIP_FILE srfReportFD = GZIP_OPEN(parCC_srfFileOut, "r");
    my_total_scaffolds = get_nScaf_from_srfFile(srfReportFD);
    LOGF("Found %lld scaffolds\n", (lld)my_total_scaffolds);

    UPC_LOGGED_BARRIER;
    int64_t n_scaffolds_globally = 0;
    int64_t scaffold_id_block_start = reduce_prefix_long(my_total_scaffolds, UPC_ADD, &n_scaffolds_globally) - my_total_scaffolds;
    UPC_LOGGED_BARRIER;
    int64_t my_max_renumbered_id = -1;
    serial_printf("Found %lld total scaffolds\n", (lld)n_scaffolds_globally);
    LOGF("Renumbering my %lld scaffolds from %lld\n", (lld)my_total_scaffolds, (lld)scaffold_id_block_start);


    // Thread0 will scatter its scaffolds here
    GlobalFinished gf = initGlobalFinished(MYSV.cores_per_node);

    int bufferCapacity = 32*1024; // 32kb should be overkill for any given scaffold
    AllSharedBufferPtr remoteBuffers = NULL;
    UPC_ALL_ALLOC_CHK(remoteBuffers, THREADS, sizeof(SharedBuffer));
    remoteBuffers[MYTHREAD] = allocSharedBuffer(bufferCapacity);
    UPC_LOGGED_BARRIER;
    int64_t remoteBatchesWritten = 0;
    int threadRoundRobin = 0;

    Buffer myBuffer = initBuffer(bufferCapacity);
    GZIP_FILE renumbered_srfReportFD = openCheckpoint(finalSrfFile, "w");
    GZIP_REWIND(srfReportFD);
    int64_t old_id = -1;
    while (GZIP_GETS(scaff_line, 1000, srfReportFD) != NULL) {
        strcpy(scaff_line_save, scaff_line);
        token = strtok_r(scaff_line, "\t", &aux);
        new_id = atol(token + 8) + scaffold_id_block_start;
        if (my_max_renumbered_id < new_id) {
            my_max_renumbered_id = new_id;
        }
        if (new_id != old_id) {
            int64_t readLength = getReadLengthBuffer(myBuffer);
            if (readLength > 4*1024) { // flush if more than 4KB are ready
                if (MYTHREAD == 0 && readLength < bufferCapacity) {
                    // send to remote
                    SharedBuffer sb;
                    if ( (sb = remoteBuffers[ threadRoundRobin % THREADS ]).len == 0) { 
                        // this thread is ready to receive
                        assert(sb.len == 0);
                        if (sb.size <= readLength) DIE("readLength=%lld sb.size=%lld sb.len=%ld thread=%d sb.str=%d,%lld\n", (lld) readLength, (lld) sb.size, (lld) sb.len, upc_threadof(sb.str), upc_addrfield(sb.str));
                        assert(sb.size > readLength);
                        // two steps, memput, fence, then update the length
                        upc_memput(sb.str, getCurBuffer(myBuffer), readLength); // send the data
                        upc_fence;
                        remoteBuffers[ threadRoundRobin % THREADS ].len = readLength; // update the length, no need to wait for fence here
                        resetBuffer(myBuffer); // sent the data, reset it now
                    } // else write it to my file
                    threadRoundRobin++; // next thread gets next batch
                }
                if (getLengthBuffer(myBuffer) > 0) { // every other thread or if the Thread0 buffer is too big to send or remote buffer is full
                    GZIP_FWRITE(getCurBuffer(myBuffer), 1, readLength, renumbered_srfReportFD);
                    resetBuffer(myBuffer);
                }
            }
            // always check for any delivered data to process and write to my file
            if (remoteBuffers[MYTHREAD].len) { // If I have a scaffold from thread0 to write, then write it now.
                if ( upc_threadof(remoteBuffers[MYTHREAD].str) != MYTHREAD) DIE("Invalid shared buffer!  My buffer is owned by thread %d\n", upc_threadof( remoteBuffers[MYTHREAD].str ) );
                GZIP_FWRITE((char*) remoteBuffers[MYTHREAD].str, 1, remoteBuffers[MYTHREAD].len, renumbered_srfReportFD);
                remoteBuffers[MYTHREAD].len = 0;
                remoteBatchesWritten++;
            }
        }
        old_id = new_id;
        printfBuffer(myBuffer, "Scaffold%ld\t%s", new_id, scaff_line_save + 1 + strlen(token));
    }
    upc_fence;
    // final write
    if (getReadLengthBuffer(myBuffer) > 0) {
        GZIP_FWRITE(getCurBuffer(myBuffer), 1, getReadLengthBuffer(myBuffer), renumbered_srfReportFD);
    } 
    LOGF("I finished writing my %lld scaffolds in %0.3f s\n",  (lld) my_total_scaffolds, ELAPSED_TIME(start_renumber));
    incrementGlobalFinished(gf);
    while (!isGlobalFinished(gf)) {
        // tight loop okay -- only accessing local variables
        if (remoteBuffers[MYTHREAD].len) { // If I have a scaffold from thread0 to write, then write it.
            GZIP_FWRITE((char*) remoteBuffers[MYTHREAD].str, 1, remoteBuffers[MYTHREAD].len, renumbered_srfReportFD);
            remoteBuffers[MYTHREAD].len = 0;
            remoteBatchesWritten++;
        }
        upc_fence;
        UPC_POLL;
    }
    UPC_LOGGED_BARRIER;
    // last check in case there was something between GlobalFinished and the barrier
    if (remoteBuffers[MYTHREAD].len) { // If I have a scaffold from thread0 to write, then write it.
        GZIP_FWRITE((char*) remoteBuffers[MYTHREAD].str, 1, remoteBuffers[MYTHREAD].len, renumbered_srfReportFD);
        remoteBuffers[MYTHREAD].len = 0;
        remoteBatchesWritten++;
    }
    freeGlobalFinished(&gf);
    freeSharedBuffer(remoteBuffers[MYTHREAD]);
    UPC_ALL_FREE_CHK(remoteBuffers);

    int expected_renumbering = ((my_total_scaffolds == 0) & (my_max_renumbered_id == -1)) \
                               | ((my_total_scaffolds > 0) & (my_max_renumbered_id == my_total_scaffolds + scaffold_id_block_start - 1));
    if (!expected_renumbering) {
        WARN("my_total_scaffolds==%lld scaffold_id_block_start==%lld but my_max_renumbered_id==%lld\n", (lld)my_total_scaffolds, (lld)scaffold_id_block_start, (lld)my_max_renumbered_id);
    }
    LOGF("Renumbered up to %lld which %s expected. remoteBatchesWritten=%lld\n", (lld)my_max_renumbered_id, expected_renumbering ? "is" : "IS NOT", (lld) remoteBatchesWritten);

    if (!MYTHREAD) {
        char scaff_fname[255];
        sprintf(scaff_fname, "Scaffolds_%s", _outScaffoldReportFilePrefix);
        put_num_in_file("", scaff_fname, n_scaffolds_globally);
    }

    GZIP_CLOSE(srfReportFD);
    closeCheckpoint(renumbered_srfReportFD);
    serial_printf("Renumbered scaffolds and report files in %0.6f s\n", ELAPSED_TIME(start_renumber));
    double startSync = now();
    syncCheckpoint(finalSrfFile); // ensure all writes are finished before exiting
    serial_printf("Syncing all changes to disk in %0.3f s\n", now() - startSync);

    return my_total_scaffolds;
}
