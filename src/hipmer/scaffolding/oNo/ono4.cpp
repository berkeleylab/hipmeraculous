// FILENAME:  ono4.cpp
// PROGRAMMER: Eugene Goltsman
// DATE:3/31/2019
// REQUIRED:
// PURPOSE:   scaffolder for haploid assembly in Meraculous/Hipmer framework

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>
#include <new>

extern "C" {
#include "optlist.h"
}

#include "poc_common.h"
#include "common.h"
#include "log.h"

#include "ono_common.h"
#include "ono4.h"





using namespace std;

#define MAX_STRAIN 3               //determines how much two links can overlap before they're deemed a tie collision
#define MAX_DEPTH_DROPOFF 0.8     // the maximum allowed depth drop *as fraction of peak depth*
#define SUSPENDABLE  maxAvgInsSize


typedef unordered_map<int32_t, int32_t> depthHist_t;

// up-front declarations
namespace ono4   // funcitons with same names/args as those below exist in other versions of ono, thus the separate namespace
{
float peakDepth;
void parse_crfFile(string, depthHist_t&);
void parse_srfFile(string, depthHist_t&);
void mark_end(End&, EndMarkCounts&);

bool resolve_tie_collision(const End&, const End&, const End&, int, int);

  void find_bestTies(const vector<Obj *>&, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, copyTracker_t&, bool);
  
  End* best_tie(const End*, tiedEndsHash_t&, tiedEndListsH_t&, suspendedHash_t&, bool);



void parse_crfFile(string crfFile, depthHist_t& depthHist)
{
    // For every contig, create a struct and populate it with contig info, including creation of two new "End" instances
    // Then add this structure to the hash of assembly objects

    bool calculatePeakDepth = (!peakDepth) ? true : false;

    LOGF("Parsing CRF %s\n", crfFile.c_str());

    ifstream infile(crfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", crfFile.c_str());
    }

    string l;
    while (getline(infile, l)) {
        string words[3];
        istringstream stm(l);
        string word;
        int n = 0;
        while (stm >> word && n < 3) { // read white-space delimited tokens one by one
            words[n] = word;
            ++n;
        }
	assert( ! words[2].empty());


        // convert contig names like 'Contig123' to int64
        string cName = words[0];
        ObjectId id;
#ifdef CONTIG_ID_PREFIX
        string prefix = CONTIG_ID_PREFIX;
        cName = cName.substr(prefix.size());
#endif
        id = atol(cName.c_str());
        int len = stoi(words[1].c_str());
        float depth = stof(words[2].c_str());
        Obj newObj(id, len, depth);
        onoObjectsMap.insert(std::make_pair(id, newObj));
        if (calculatePeakDepth) {
	  int roundedDepth = int(depth + 0.5);
	  if (depthHist.find(roundedDepth) != depthHist.end()) {
	    depthHist[roundedDepth] += len;
	  } else {
	    depthHist[roundedDepth] = len;
	  }
        }
    }

    LOGF("Found %lld onoObjectsMap\n", (lld)onoObjectsMap.size());
}

// scaffs: key = "Scaffold$objectId"
void parse_srfFile(string srfFile, depthHist_t& depthHist)
{
    bool calculatePeakDepth = (!peakDepth) ? true : false;

    LOGF("Parsing SRF %s\n", srfFile.c_str());
    ifstream infile(srfFile);
    if (!infile.is_open()) {
        DIE("Can't open %s\n", srfFile.c_str());
    }

    if( ifsIsEmpty(infile)) {
      DBG("Empty srf file");
      return;
    }
    
    
    string l;
    ObjectId currId = -1;
    Obj __tmpObj(0, 0, 0.0);
    Scaf *_currScaf = NULL;
    Obj *_currObj = NULL;
    int scaffRealBases = -1;
    float scaffDepSum = 0;

    while (getline(infile, l)) {
        string words[6];
        istringstream stm(l);
        string word;
        int n = 0;
        while (stm >> word) { // read white-space delimited tokens one by one
            words[n] = word;
            ++n;
        }
        if (n > 6 || n < 4) {
            DIE("Error:  srf file not in expected format!\n");
        }


        //Scaffold459     CONTIG4 -Contig2948227  11      109     3.000000
        //Scaffold459     GAP4    -97     1
        string scaffName = words[0];

#ifdef SCAFF_ID_PREFIX
        string prefix = SCAFF_ID_PREFIX;
        scaffName = scaffName.substr(prefix.size());
#endif
        ObjectId id = atol(scaffName.c_str());

        if (id != currId) {
            if (_currObj != NULL) {
                // add record to the corresponding hashes and reset totals
                _currObj->depth = scaffDepSum / scaffRealBases;

                scaffInfo.insert(std::make_pair(currId, _currScaf));
                onoObjectsMap.insert(std::make_pair(currId, *_currObj));

                scaffDepSum = 0;
                scaffRealBases = 0;
            }

            // initialize new objects
            _currScaf = new Scaf(id);
            _currObj = &__tmpObj;
            *_currObj = Obj(id, 0, 0.0);
        }

        string indexStr = words[1];
        size_t found = indexStr.find("CONTIG");
        if (found != std::string::npos) {
            char ori = words[2][0];
            string cName = words[2].substr(1);
            int start = atoi(words[3].c_str());
            int end = atoi(words[4].c_str());
            float dep = atof(words[5].c_str());
            int cLen = end - start + 1;
            scaffRealBases += cLen;
            scaffDepSum += float(cLen * dep);

            _currObj->len = end;
            _currScaf->push_contig(cName, ori, start, end, dep);

            if (calculatePeakDepth) {
                //add to the the depth histogram
	      int roundedDepth = int(dep + 0.5);
	      if (depthHist.find(roundedDepth) != depthHist.end()) {
		depthHist[roundedDepth] += cLen;
	      } else {
		depthHist[roundedDepth] = cLen;
	      }
            }
        } else { // GAP
            int size = atoi(words[2].c_str());
            int uncert = atoi(words[3].c_str());
            _currScaf->push_gap(size, uncert);
        }

        currId = id;
    }
    _currObj->depth = scaffDepSum / scaffRealBases;

    scaffInfo.insert(std::make_pair(currId, _currScaf));
    onoObjectsMap.insert(std::make_pair(currId, *_currObj));
}
  
void mark_end(End& __end, EndMarkCounts& endMarkCounts)
{
    End *_end = &__end;
    
    vector<EndTie> ties = _end->export_all_ties();  // a copy of the vector of end ties since we'll be manipulating it, sorting, etc
    if (ties.empty()) {
        _end->setEndMark(EndLabel::NO_TIES);
        endMarkCounts[EndLabel::NO_TIES]++;
	DBG2N("%lld.%d\tNO_TIES\n", _end->getID(), _end->getPrime());
        return;
    }

    ObjectId objId = _end->getID();
    float myDepth = onoObjectsMap.at(objId).depth;

    for (auto tie : ties) {
        if (!tie.tiedEndId.isValid()) {
            continue;
        }
        End *_tiedEnd_i = &getEnd(tie.tiedEndId);
        ObjectId tiedId = _tiedEnd_i->getID();
        float tiedDepth = onoObjectsMap.at(tiedId).depth;
        if (tiedId == objId) {
            _end->setEndMark(EndLabel::SELF_TIE);
            endMarkCounts[EndLabel::SELF_TIE]++;
	    DBG2N("%lld.%d\tSELF_TIE\n", _end->getID(), _end->getPrime());
            return;
        }
        if ((myDepth - tiedDepth) / peakDepth > MAX_DEPTH_DROPOFF) {
            _end->setEndMark(EndLabel::DEPTH_DROP);
            endMarkCounts[EndLabel::DEPTH_DROP]++;
	    DBG2N("%lld.%d\tDEPTH_DROP\n", _end->getID(), _end->getPrime());
            return;
        }
    }

    // sort ties by distance
    std::sort(ties.begin(), ties.end(), by_dist_len());

    unsigned int nTies = ties.size();
    if (nTies >= 2) {
        for (size_t i = 0; i < nTies - 1; ++i) {
            int64_t ID_i = ties[i].tiedEndId.getID();
            int start_i = ties[i].gapEstimate;
            int end_i = start_i + onoObjectsMap.at(ID_i).len - 1;
            int uncertainty_i = ties[i].gapUncertainty;

            int64_t ID_j = ties[i + 1].tiedEndId.getID();
            int start_j = ties[i + 1].gapEstimate;
            int uncertainty_j = ties[i + 1].gapUncertainty;

            int overlap = end_i - start_j + 1;

            if (overlap > merSize - 2) {
                DBG2N("End %lld.%d --< excessive overlap between ties %s (%d - %d) & %s (%d) ...\n", _end->getID(), _end->getPrime(), ties[i].tiedEndId.toString().c_str(), start_i, end_i, ties[i + 1].tiedEndId.toString().c_str(), start_j);
                int excessOverlap = overlap - (merSize - 2);
                float strain = float(excessOverlap) / float(uncertainty_i + uncertainty_j);
                if (strain > MAX_STRAIN) {
                    DBG2N("\tExcessive strain of %f ...", strain);
                    Obj *_testObj = &onoObjectsMap.at(objId);
                    Obj *_obj_i = &onoObjectsMap.at(ID_i);
                    Obj *_obj_j = &onoObjectsMap.at(ID_j);

                    if (resolve_tie_collision(*_end, getEnd(ties[i].tiedEndId), getEnd(ties[i + 1].tiedEndId), start_i, start_j)) {
                        DBG2N("\tTie collision resolved!\n");
                        _end->setEndMark(EndLabel::UNMARKED);
                        endMarkCounts[EndLabel::UNMARKED]++;
			//			DBG2N("\tUNMARKED\n");
                        return;
                    } else {
                        _end->setEndMark(EndLabel::TIE_COLLISION);
                        endMarkCounts[EndLabel::TIE_COLLISION]++;
                        DBG2N("\tTIE_COLLISION: %lld.%d --< %s ? %s\n", _end->getID(), _end->getPrime(), ties[i].tiedEndId.toString().c_str(), ties[i + 1].tiedEndId.toString().c_str());
                        return;
                    }
                }
            }
        }
    }
    _end->setEndMark(EndLabel::UNMARKED);
    endMarkCounts[EndLabel::UNMARKED]++;
    //    DBG2N("\tUNMARKED\n");
    return;
}


bool resolve_tie_collision(const End& _end, const End& _tiedEnd_i, const End& _tiedEnd_j, int start_i, int start_j)
{
    End *_otherEnd_i = &other_end(_tiedEnd_i);
    End *_otherEnd_j = &other_end(_tiedEnd_j);

    //  int64_t ID_i = _tiedEnd_i->objectId;
    ObjectId ID_j = _tiedEnd_j.getID();

    //  int len_i = onoObjectsMap.at(ID_i)->len;
    int len_j = onoObjectsMap.at(ID_j).len;

    // test if the coliding objects are themselves strongly linked, i.e., the strain is false
    int i_j_gapUncert = 0;

    if (_otherEnd_i->get_tie(_tiedEnd_j) != NULL) {
        i_j_gapUncert = _otherEnd_i->get_tie(_tiedEnd_j)->gapUncertainty;
    }
    if (is_splinted(*_otherEnd_i, _tiedEnd_j) || i_j_gapUncert == 1) {      // splint or span with highest certainty
        return true;
    } else if (is_splinted(*_otherEnd_j, _tiedEnd_i)) { // splint suggests that current relative order is wrong (most likely due to bad span-based gap estimate)
        int newGapEstimate = start_j + len_j - (merSize - 2) + 1; // make new distance to i larger than dist to j
        _end.correct_gap_estimate(_tiedEnd_i, newGapEstimate);
        _tiedEnd_i.correct_gap_estimate(_end, newGapEstimate);
        return true;
    }

    // //   if the colision involves a SPAN, and the SPLINTed object is small,  make new distance to  SPAN'ed object larger as we suspect the gap estimate to be negatively biased
    //   if ( is_splinted( _end, _tiedEnd_j)   && ( ! is_splinted( _end, _tiedEnd_i))
    //        && len_j <= (maxAvgInsSize + (2*maxAvgInsSDev)) ) {
    //     int newGapEstimate = start_j + len_j - (merSize-2);
    //     _end->correct_gap_estimate(_tiedEnd_i, newGapEstimate);
    //     _tiedEnd_i->correct_gap_estimate(_end, newGapEstimate);
    //     return true;
    //   }
    //   else if ( is_splinted( _end, _tiedEnd_i)   && ( ! is_splinted( _end, _tiedEnd_j))
    //        && len_i <= (maxAvgInsSize + (2*maxAvgInsSDev)) ) {
    //     int newGapEstimate = start_i + len_i - (merSize-2);
    //     _end->correct_gap_estimate(_tiedEnd_j, newGapEstimate);
    //     _tiedEnd_j->correct_gap_estimate(_end, newGapEstimate);
    //     return true;
    //   }

    return false;
}


  void find_bestTies(const sortedBy_t& sortedByLen, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, copyTracker_t& copyObjTracker, bool existsSrfFile)
{
    // Super-function to evaluate all size-sorted objects' ends and secure a "best tie"
    // Pieces can be inserted between earlier established bestTies (i.e. suspensions) and/or copied in the process.

    LOGF("Finding \"best ties\" (%lld)...\n", (lld)sortedByLen.size());

    int64_t count = 0, progressCt = sortedByLen.size() / 100;
    for (auto tmp : sortedByLen) {
        ObjectId id = tmp.id;

        if (progressCt && ++count % progressCt == 0) {
            LOGF("find_bestTies: %lld of %lld (%0.3f %%) object %lld\n", (lld)count, (lld)sortedByLen.size(), 100.0 * count / sortedByLen.size(), (lld)id);
        }
        DBG2N(" === object %lld\n\n", (lld)id);

        if (suspended.count(id)) {
            DBG2N(" -ineligible (suspended)\n");
            continue;
        }

        Obj *piece = &onoObjectsMap.at(id);

        EndIds ends(*piece);
        for (auto _endId : ends.getEnds()) {
            DBG2N("end %d:\n", _endId.getPrime());

            // Initially only unmarked ends are allowed to have best ties.
            if (_endId.getEndMark() == EndLabel::UNMARKED) {
                End *_bestTie = best_tie(&getEnd(_endId), bestTies, bestTiedBy, suspended, existsSrfFile);
                if (_bestTie != NULL) {
                    bestTies.insert(std::pair<EndId, EndId>(_endId, _bestTie->getEndId()));
                    bestTiedBy[_bestTie->getEndId()].push_back(_endId);
                    DBG2N("BEST_TIE: [%s]\t%s\n\n", _bestTie->toString().c_str(), _endId.toString().c_str());
                } else {
                    DBG2N("BEST_TIE: [%d]\t%s\n\n", _endId.getEndMark(), _endId.toString().c_str());
                }
            } else {
                DBG2N("BEST_TIE: [%d]\t%s\n\n", _endId.getEndMark(), _endId.toString().c_str());
            }
        }
    }

    //  Insert suspended pieces where possible
    place_suspended(suspended, bestTies, bestTiedBy, copyObjTracker, existsSrfFile);
}

End *best_tie(const End *_end, tiedEndsHash_t& bestTies, tiedEndListsH_t& bestTiedBy, suspendedHash_t& suspended, bool existsSrfFile)
{
    Obj *obj = &onoObjectsMap.at(_end->getID());
    bool largeObject = (obj->len > SUSPENDABLE) ? true : false;
    if (largeObject)
      DBG2N("\tLarge object (%d > %d).\n ", obj->len, (SUSPENDABLE));
    int nSuspended = 0;

    vector<EndTie> goodTies;
    for (auto tie : _end->ties) {
        EndLabel::Label tieMark = getEnd(tie.tiedEndId).getEndMark();
        if (tieMark != EndLabel::UNMARKED) {
            // Skip ties to previously marked ends  unless they're split DD-SD type ends - these we will try to resolve
	    DBG2N("\ttied end  %s marked [%d]. SKIP..\n", tie.tiedEndId.toString().c_str(), tieMark);  
            continue;
        }
        DBG2N("\tgood tie: %s\n", tie.tiedEndId.toString().c_str());
        goodTies.push_back(tie);
    }
    if (goodTies.empty()) {
        DBG2N("noGoodTies\n");
        return NULL;
    }

    std::sort(goodTies.begin(), goodTies.end(), by_dist_len());

    objectsVec_t toSuspend;

    // If a large object, return the closest large object if one exists.
    // Small objects may be suspended between large objects
    if (largeObject) {
        End *_closestLarge = NULL;
        for (auto tie : goodTies) {
            EndId _te = tie.getEndId();
            End *_te_end = &getEnd(_te);
            ObjectId obj_id = _te.getID();
            Obj *obj_i = &onoObjectsMap.at(obj_id);
            if (obj_i->len > SUSPENDABLE && _te_end->getEndMark() == EndLabel::UNMARKED) {
                _closestLarge = _te_end;
                break;
            } else {
                toSuspend.push_back(obj_id);
            }
        }
        if (_closestLarge) {
            if (toSuspend.size()) {
	      for (auto _objId : toSuspend) {
		Obj *_obj = &onoObjectsMap.at(_objId);
		suspended.insert(std::make_pair(_objId, true));
		DBG2N("BT: suspended object %lld\n", (lld)_objId);
		nSuspended++;
	      }
	      toSuspend.clear();
            }
            DBG2N("BT return: closestLarge (%lld suspended)\n", (lld)nSuspended);
            return _closestLarge;
        }
        DBG2N("BT: no closest large found\n");
    }

    //Return the closest extendable object if one exists.  Objects must be unmarked to qualify. Unextendable objects may be suspended
    End *_closestExtendable = NULL;
    toSuspend.clear();
    for (EndTie tie : goodTies) {
        End *_te = &tie.getTiedEnd();
        End *_otherEnd = &other_end(*_te);
        ObjectId obj_id = _te->getID();
        Obj *obj_i = &onoObjectsMap.at(obj_id);
        if (_otherEnd->getEndMark() != EndLabel::UNMARKED) {
            toSuspend.push_back(obj_id);
        } else {
            _closestExtendable = _te;
            break;
        }
    }
    if (_closestExtendable) {
      if (toSuspend.size()) {
	for (auto _objId : toSuspend) {
	  Obj *_obj = &onoObjectsMap.at(_objId);
	  suspended.insert(std::make_pair(_objId, true));
	  DBG2N("BT: suspended object %lld\n", (lld)_objId);
	  nSuspended++;
	}
	toSuspend.clear();
      }
      
      DBG2N("BT return: closestExtendable (%lld suspended)\n", (lld)nSuspended);
      return _closestExtendable;
    }
    DBG2N("BT: no closest extendable found\n");

    //Just return the closest object
    toSuspend.clear();
    End *_closestEnd = &getEnd(goodTies[0]);
    DBG2N("\t%s.. is closest\n", _closestEnd->toString().c_str());
    DBG2N("BT return: closest\n");
    return _closestEnd;
}

} // namespace ono4



int oNo4_main(int argc, char **argv)
{
    using namespace std;
    using namespace ono4;

    LOG_VERSION;
    
    initCommonStatics();
    ono4::peakDepth = 0.0;

    if (argc < 2) {
      SWARN("Usage: ./ono <-m merSize> <-l linkDataFile> (<-s scaffoldReportFile> || <-c contigReportFile>)  <-o scaffOutFile> <<-p pairThreshold>> <<-d peakDepth>> <<-O IDoffset>> \n");
      return 1;
    }

    option_t *optList, *_optList, *thisOpt;
    _optList = GetOptList(argc, argv, "m:l:p:d:s:c:o:O:");
    optList = _optList;
    
    char *linkDataFile = NULL;
    char *srfFile = NULL;
    char *crfFile = NULL;
    char *srfOutFile = NULL;
    int pairThreshold = 2;
    ObjectId srfOffset = 0;
    bool existsSrfFile = false;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'm':
            merSize = atoi(thisOpt->argument);
            break;
        case 'l':
            linkDataFile = thisOpt->argument;
            break;
        case 'p':
            pairThreshold = atoi(thisOpt->argument);
            break;
        case 'd':
            peakDepth = atof(thisOpt->argument);
            break;
        case 's':
            srfFile = thisOpt->argument;
	    existsSrfFile = true;
            break;
        case 'c':
            crfFile = thisOpt->argument;
            break;
        case 'o':
            srfOutFile = thisOpt->argument;
            break;
        case 'O':
            srfOffset = atol(thisOpt->argument);
            break;
        }
    }

    if (!merSize) {
      SWARN("You must specify a -m merSize!\n");
        return 1;
    }
    if (linkDataFile == NULL) {
      SWARN( "You must specify a -l linkDataFile!\n");
        return 1;
    }
    if (srfFile == NULL && crfFile == NULL) {
      SWARN("You must specify either -c crfFile or -s srfFile  inputs!\n");
        return 1;
    }
    if (srfOutFile == NULL) {
      SWARN("You must specify an output file!\n");
        return 1;
    }

    unordered_map<string, libInfo_t> libInfoHash;

    vector<string> libMetaData;

    ifstream infile(linkDataFile);
    string line;
    while (getline(infile, line)) {
      // WARNING: a single library can be listed twice - for spans and for splints, in which case it will have different insSize values!  (we use readLength as insert size for splints).  That's ok, since the same combined link file is given for both, and we don't use insert sizes beyond this point.

        libInfo_t libInfo;
        istringstream stm(line);
        string word;
        while (stm >> word) { // read white-space delimited tokens one by one
            libMetaData.push_back(word);
        }
        string libName = libMetaData[0];
        libInfo.insSize = stoi(libMetaData[1]);
        libInfo.sdev = stoi(libMetaData[2]);
        libInfo.linkFile = libMetaData[3];
	
	if (!libInfoHash.count(libName))
	  libInfoHash.insert({ libName, libInfo });

        if (libInfo.insSize > maxAvgInsSize) {
            maxAvgInsSize = libInfo.insSize;
            maxAvgInsSDev = libInfo.sdev;
        }
	libMetaData.clear();
    }

    LOGF("maxAvgInsSize picked as %d\n", maxAvgInsSize);

    // load contig/scaffold info

    depthHist_t depthHist;

    if (crfFile != NULL) {
        parse_crfFile(string(crfFile), depthHist);
    } else if (srfFile != NULL) {
        parse_srfFile(string(srfFile), depthHist);
    }


    if (!onoObjectsMap.size()) { // Not a fatal error since parCC is not guaranteed to produce a CC for every thread;  Write empty srf file and exit;
        FreeOptList(optList);
        LOGF("No assembly objects found in the input;  \n");
        ofstream srfOut_empty(srfOutFile);
        //  FLUSH_MY_LOG;
        return 0;
    }


    if (!peakDepth) {
        peakDepth = find_peak_depth(depthHist);
    }
    assert(peakDepth >= 1);
    LOGF("Modal depth: %f\n", peakDepth);

    
    //Build the linkage and then ties streaming
    int64_t numLinks = 0;
    int maxUncertainty = 1000000;
    unordered_map<string,bool> seen;
    for (std::pair<string, libInfo_t> element : libInfoHash) {
        string libName = element.first;
        string linkFile = libInfoHash[libName].linkFile;
	if (!seen.count(linkFile)) { //multiple libs, same link file
	  numLinks += parse_linkFile_and_build_ties(linkFile, pairThreshold, maxUncertainty, existsSrfFile);
	  seen.insert({linkFile, true});
	}
    }

    LOGF("There are %lld ojbects loaded now %0.3f MB and %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj) / ONE_MB, 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    if (scaffInfo.size()) {
      LOGF("There are %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    }




    
    // build a vector of pointers to objects, sorted by sequence length
    sortedBy_t sortedByLen;
    sortedByLen.reserve(onoObjectsMap.size());

    for (auto kv : onoObjectsMap) {
      sortedByLen.push_back(kv.second);
    }
        
    std::sort(sortedByLen.begin(), sortedByLen.end(), by_length_depth());

    //    SET_HIPMER_VERBOSITY(LL_DBG2);
    
    // mark object ends
    int nObjects = sortedByLen.size();
    EndMarkCounts endMarkCounts = {};
    LOGF("Marking ends... (%d objects)\n", nObjects);

    for (auto & i : sortedByLen) {
      Obj *obj = &onoObjectsMap.at(i.id);
      mark_end(obj->end5, endMarkCounts);
      mark_end(obj->end3, endMarkCounts);
    }

    // # find a "best tie" for every end.
    tiedEndsHash_t bestTie;     //stores End_sp => this end's bestTie ptr
    tiedEndListsH_t bestTiedBy; // stores End_sp => list of ends that claim this end as their bestTie ptr
    suspendedHash_t suspended;
    copyTracker_t copyObjTracker; //dummy here
    find_bestTies(sortedByLen, bestTie, bestTiedBy, suspended, copyObjTracker,  NULL != srfFile);

    //    SET_HIPMER_VERBOSITY(LL_DBG);

    
    // # Lock ends together with no competing ties
    endLocks_t endLocks;
    lock_ends(endLocks, bestTie, bestTiedBy, suspended, onoObjectsMap);

    // # Traverse end locks to build scaffolds
    int64_t nScaffolds = build_scaffolds(endLocks, string(srfOutFile), NULL != srfFile, copyObjTracker, srfOffset);
    LOGF("Total of %lld scaffolds made\n", (lld)nScaffolds);



    for (int i = 0; i < EndLabel::MAX_END_LABEL; i++) {
        LOGF("Ends marked %s: %d\n", EndLabel::getName(i).c_str(), endMarkCounts[i]);
    }
    DBG("nr of best ties: %lld\n", (lld)bestTie.size());
    DBG("nr of scaffolds: %lld\n", (lld)nScaffolds);


    // Cleanup

    LOGF("Cleaning up data structures\n");
    tiedEndsHash_t().swap(bestTie);
    tiedEndListsH_t().swap(bestTiedBy);
    suspendedHash_t().swap(suspended);
    endLocks_t().swap(endLocks);

    LOGF("Cleaning %lld ojbects loaded now %0.3f MB and %0.3f MB\n", (lld)onoObjectsMap.size(), 1.0 * onoObjectsMap.size() * sizeof(Obj) / ONE_MB, 1.0 * onoObjectsMap.size() * sizeof(objectsHash_t::value_type) / ONE_MB);
    objectsHash_t().swap(onoObjectsMap);

    LOGF("Cleaning %lld scaffInfo loaded now %0.3f MB and %0.3f MB\n", (lld)scaffInfo.size(), 1.0 * scaffInfo.size() * sizeof(Scaf) / ONE_MB, 1.0 * scaffInfo.size() * sizeof(scaffsHash_t::value_type) / ONE_MB);
    for (auto &sc : scaffInfo) {
        delete sc.second;
    }
    scaffsHash_t().swap(scaffInfo);

    FreeOptList(optList);
    LOGF("Done\n");
    FLUSH_MY_LOG;
    
    // END OF MAIN

    return 0;
}

