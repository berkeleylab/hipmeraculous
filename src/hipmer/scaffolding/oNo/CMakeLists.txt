MESSAGE("Building oNo and parCC ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

SET(CMAKE_CXX_STANDARD 14)

SET_UPC_PROPS(*.upc n50fromSRF.c)



add_library(oNo4_objs OBJECT ono4.cpp)

add_library(oNo_common_objs OBJECT ono_common.cpp)

add_library(oNo_meta_objs OBJECT ono_meta.cpp)

add_library(oNo_CCS_objs OBJECT ono_CCS.cpp)

add_library(parCC_objs OBJECT parCC.upc)
if (HIPMER_EMBED_HMMER)
  add_dependencies(parCC_objs project_libhmmer)
endif()

add_library(oNo_2D_objs OBJECT ono_2D.cpp)

SET(_ono_link_libs ${ZLIB_LIBRARIES} ${RT_LIBRARIES})

IF (HIPMER_FULL_BUILD)
  ADD_EXECUTABLE(n50fromSRF n50fromSRF.c
                          $<TARGET_OBJECTS:upc_common>
                          $<TARGET_OBJECTS:COMMON>
                          $<TARGET_OBJECTS:HIPMER_VERSION>
                          $<TARGET_OBJECTS:HASH_FUNCS>
                          $<TARGET_OBJECTS:Buffer>
                          $<TARGET_OBJECTS:MemoryChk>
                          $<TARGET_OBJECTS:OptList>
                          $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
              ) 
  set_target_properties(n50fromSRF PROPERTIES LINKER_LANGUAGE "UPC")
  target_link_libraries(n50fromSRF ${ZLIB_LIBRARIES} ${RT_LIBRARIES})
  INSTALL(TARGETS n50fromSRF DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
ENDIF()

# Always build stand alone oNo binaries

  ADD_EXECUTABLE(oNo4 ono4_exec.cpp 
                     $<TARGET_OBJECTS:oNo4_objs>
                     $<TARGET_OBJECTS:oNo_common_objs>
                     $<TARGET_OBJECTS:poc_common>
                     $<TARGET_OBJECTS:COMMON>
                     $<TARGET_OBJECTS:HIPMER_VERSION>
                     $<TARGET_OBJECTS:Buffer>
                     $<TARGET_OBJECTS:MemoryChk>
                     $<TARGET_OBJECTS:OptList>
  )
  target_link_libraries(oNo4 ${_ono_link_libs})
  INSTALL(TARGETS oNo4 DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

  
  ADD_EXECUTABLE(oNo_meta ono_meta_exec.cpp
                          $<TARGET_OBJECTS:oNo_meta_objs>
                          $<TARGET_OBJECTS:oNo_common_objs>
                          $<TARGET_OBJECTS:poc_common>
                          $<TARGET_OBJECTS:COMMON>
                          $<TARGET_OBJECTS:HIPMER_VERSION>
                          $<TARGET_OBJECTS:Buffer>
                          $<TARGET_OBJECTS:MemoryChk>
                          $<TARGET_OBJECTS:OptList>
                )
  target_link_libraries(oNo_meta ${_ono_link_libs})
  INSTALL(TARGETS oNo_meta DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

  ADD_EXECUTABLE(oNo_CCS ono_CCS_exec.cpp
                          $<TARGET_OBJECTS:oNo_CCS_objs>
                          $<TARGET_OBJECTS:oNo_common_objs>
                          $<TARGET_OBJECTS:poc_common>
                          $<TARGET_OBJECTS:COMMON>
                          $<TARGET_OBJECTS:HIPMER_VERSION>
                          $<TARGET_OBJECTS:Buffer>
                          $<TARGET_OBJECTS:MemoryChk>
                          $<TARGET_OBJECTS:OptList>
                )
  target_link_libraries(oNo_CCS ${_ono_link_libs})
  INSTALL(TARGETS oNo_CCS DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

  

  ADD_EXECUTABLE(oNo_2D ono_2D_exec.cpp
                          $<TARGET_OBJECTS:oNo_2D_objs>
                          $<TARGET_OBJECTS:oNo_common_objs>
                          $<TARGET_OBJECTS:poc_common>
                          $<TARGET_OBJECTS:COMMON>
                          $<TARGET_OBJECTS:HIPMER_VERSION>
                          $<TARGET_OBJECTS:Buffer>
                          $<TARGET_OBJECTS:MemoryChk>
                          $<TARGET_OBJECTS:OptList>
                )
  target_link_libraries(oNo_2D ${_ono_link_libs})
  INSTALL(TARGETS oNo_2D DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )


IF (HIPMER_FULL_BUILD)
  SET(_parcc_link_libs ${_ono_link_libs})
  if (HIPMER_EMBED_HMMER) 
    set(_parcc_link_libs ${_parcc_link_libs} ${HMMER_LIBS})
  endif()
  ADD_EXECUTABLE(parCC parCC_exec.upc
                     $<TARGET_OBJECTS:parCC_objs>
                     $<TARGET_OBJECTS:upc_common>
                     $<TARGET_OBJECTS:COMMON>
                     $<TARGET_OBJECTS:HIPMER_VERSION>
                     $<TARGET_OBJECTS:EXEC_UTILS>
                     $<TARGET_OBJECTS:HASH_FUNCS>
                     $<TARGET_OBJECTS:oNo_common_objs>
                     $<TARGET_OBJECTS:oNo_meta_objs>
		             $<TARGET_OBJECTS:oNo_CCS_objs>
                     $<TARGET_OBJECTS:oNo_2D_objs>
		             $<TARGET_OBJECTS:oNo4_objs>
                     $<TARGET_OBJECTS:Buffer>
                     $<TARGET_OBJECTS:MemoryChk>
                     $<TARGET_OBJECTS:OptList>
                     $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
  )
  set_target_properties(parCC PROPERTIES LINKER_LANGUAGE "UPC")
  target_link_libraries(parCC ${_parcc_link_libs})
  INSTALL(TARGETS parCC DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
 
ENDIF()

