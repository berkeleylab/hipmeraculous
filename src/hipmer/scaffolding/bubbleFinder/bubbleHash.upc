#include "bubbleHash.h"

/* Creates and initializes a distributed hastable - collective function */
hash_table_t *create_hash_table(int64_t size, memory_heap_t *memory_heap, int64_t my_heap_size)
{
    hash_table_t *result;
    int64_t i;
    int64_t n_buckets = size;

    result = (hash_table_t *)calloc_chk(1, sizeof(hash_table_t));
    result->size = n_buckets;
    UPC_ALL_ALLOC_CHK(result->table, n_buckets, sizeof(bucket_t));

    /* Initialization of every bucket */
    for (i = MYTHREAD; i < n_buckets; i += THREADS) {
        result->table[i].head = NULL;
    }

    memset(memory_heap, 0, sizeof(memory_heap_t));
    UPC_ALLOC_CHK(memory_heap->my_heap_ptr, my_heap_size * sizeof(list_t));
    UPC_ALLOC_CHK(memory_heap->my_data_ptr, my_heap_size * sizeof(data_t));
    memory_heap->my_heap_pos = 0;
    memory_heap->my_data_pos = 0;
    memory_heap->max_pos = my_heap_size;

    LOGF("Initialized hash table with %lld size and my_heap_size=%lld\n", (lld) size, (lld) my_heap_size);
    upc_barrier;

    return result;
}

void destroy_hash_table(hash_table_t **_ht, memory_heap_t *memory_heap)
{
    if (_ht == NULL) {
        DIE("Invalid call to destroy_hash_table\n");
    }
    hash_table_t *ht = *_ht;
    if (ht != NULL) {
        UPC_FREE_CHK(memory_heap->my_heap_ptr);
        UPC_FREE_CHK(memory_heap->my_data_ptr);
        memory_heap->max_pos = 0;
        UPC_ALL_FREE_CHK(ht->table);
        ht->size = 0;
        LOGF("destroy_hash_table stats: lookups=%lld %0.3fs, nNexts=%lld addTips=%lld %0.3fs\n", (lld) ht->ct.count, ht->ct.time, ht->nNexts, (lld) ht->addTips.count, ht->addTips.time);
        free_chk(ht);
    }
    *_ht = NULL;
}

/* Creates pointsTo and initializes a distributed hastable - collective function */
phash_table_t *create_phash_table(int64_t size, pmemory_heap_t *memory_heap, int64_t my_heap_size)
{
    phash_table_t *result;
    int64_t i;
    int64_t n_buckets = size;

    result = (phash_table_t *)calloc_chk(1, sizeof(phash_table_t));
    result->size = n_buckets;
    UPC_ALL_ALLOC_CHK(result->table, n_buckets, sizeof(pbucket_t));

    /* Initialization of every bucket */
    for (i = MYTHREAD; i < n_buckets; i += THREADS) {
        result->table[i].head = NULL;
    }

    memset(memory_heap, 0, sizeof(pmemory_heap_t));
    UPC_ALLOC_CHK(memory_heap->my_heap_ptr, my_heap_size * sizeof(plist_t));
    UPC_ALLOC_CHK(memory_heap->my_data_ptr, my_heap_size * sizeof(pdata_t));
    memory_heap->my_heap_pos = 0;
    memory_heap->my_data_pos = 0;
    memory_heap->max_pos = my_heap_size;

    LOGF("Initialized phash table with %lld size and my_heap_size=%lld\n", (lld) size, (lld) my_heap_size);
    upc_barrier;

    return result;
}

void destroy_phash_table(phash_table_t **_ht, pmemory_heap_t *memory_heap)
{
    if (_ht == NULL) {
        DIE("Invalid call to destroy_hash_table\n");
    }
    phash_table_t *ht = *_ht;
    if (ht != NULL) {
        UPC_FREE_CHK(memory_heap->my_heap_ptr);
        UPC_FREE_CHK(memory_heap->my_data_ptr);
        UPC_ALL_FREE_CHK(ht->table);
        ht->size = 0;
        LOGF("destroy_phash_table stats: lookups %lld %0.3fs, nNexts=%lld, addEdges=%lld %0.3fs\n", (lld) ht->ct.count, ht->ct.time, ht->nNexts, (lld) ht->addEdges.count, ht->addEdges.time);
        free_chk(ht);
    }
    *_ht = NULL;
}

shared[] list_t *find_in_ll(shared [] list_t *head, const char *tipKey, list_t *copy, int kmer_len, int64_t *nNexts)
{
    shared [] list_t *result = head, *next = NULL;
    int64_t nexts = 0;
    for (; result != NULL; ) {
        (*copy) = *result;
        if (memcmp(copy->tipKey, tipKey, 2 * kmer_len * sizeof(char)) == 0) {
            break;
        }
        nexts++;
        next = copy->next;
        /* RSE - atomic Accessors are not necessary here. 
         * Only the head is modified, never the next member field of a node
         * UPC_ATOMIC_GET_SHPTR(&next, &(result->next));
         */
        result = next;
    }
    if (nNexts) 
        *nNexts += nexts;
    DBG2("find_in_ll(%.*s) found %d:%x %d deep\n", 2*kmer_len, tipKey, upc_threadof(result), upc_addrfield(result), nexts);
    return result;
}

/* Add entry to the hash table for tipInfo */
int add_tip(hash_table_t *hashtable, const char *tip, int64_t contig, memory_heap_t *memory_heap, int kmer_len)
{
    double start = now();
    assert(kmer_len < MAX_KMER_SIZE);
    char rcTip[2 * MAX_KMER_SIZE];
    char rcFlag = PLUS;
    const char *tipKey = tip;
    if (!reverseComplementSeqBubble(tip, rcTip, 2 * kmer_len)) {
        WARN("Unexpected base in reverseComplementSeqBubble for add_tip\n");
    }
    if (memcmp(rcTip, tip, 2 * kmer_len) < 0) {
        tipKey = rcTip;
        rcFlag = MINUS;
    }

    int64_t hashval = hashkey(hashtable->size, tipKey, 2 * kmer_len);
    list_t cached_copy;
    shared[] list_t *result = NULL, *head = NULL, *next = NULL;

    // implement with atomic pointers, cswap and no locks!

    assert(hashval >= 0);
    assert(hashval < hashtable->size);

    int64_t cur_data_pos = memory_heap->my_data_pos;
    if (cur_data_pos >= memory_heap->max_pos) {
        WARN("hash_table ran out of data heap space! .. continuing\n");
        addCountTime(&(hashtable->addTips), now() - start);
        return 0;
    }
    if (memory_heap->my_heap_pos >= memory_heap->max_pos) {
        WARN("hash_table ran out of heap space! .. continuing\n");
        addCountTime(&(hashtable->addTips), now() - start);
        return 0;
    }

    shared[] data_t * my_data_ptr = memory_heap->my_data_ptr;
    (memory_heap->my_data_pos)++; // we will add an entry
    int64_t cur_heap_pos = memory_heap->my_heap_pos;
    shared[] list_t * my_heap_ptr = NULL;

    UPC_ATOMIC_GET_SHPTR(&head, &(hashtable->table[hashval].head));
    int64_t attempts = 0;
    int found;
    while (result == NULL) {
        if (++attempts % 10000 == 0) { WARN("Possible infinite loop here %lld attempts\n", (lld) attempts); }
        result = find_in_ll(head, tipKey, &cached_copy, kmer_len, &(hashtable->nNexts));
        found = (result == NULL) ? 0 : 1;

        if (found == 1) {
            /* There is such a key, therefore just add the data in the list */
            assert(result != NULL);
            my_data_ptr[cur_data_pos].next = cached_copy.data;
            my_data_ptr[cur_data_pos].contig = contig;
            my_data_ptr[cur_data_pos].rcFlag = rcFlag;
            upc_fence;
            //result->data = &(my_data_ptr[cur_data_pos]);
            shared [] data_t *test = NULL;
            UPC_ATOMIC_CSWAP_SHPTR(&test, &(result->data), cached_copy.data, &(my_data_ptr[cur_data_pos]));
            if (test != cached_copy.data) {
               // result->data changed while prepping, start over
               result = NULL;
               UPC_POLL;
               continue;
            }
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(result->nContigs), 1);
        } else {
            /* There is not such a key, therefore add both the entry and data */
            assert(result == NULL);
            cur_heap_pos = memory_heap->my_heap_pos;
            my_heap_ptr = memory_heap->my_heap_ptr;
            my_data_ptr[cur_data_pos].next = NULL;
            my_data_ptr[cur_data_pos].contig = contig;
            my_data_ptr[cur_data_pos].rcFlag = rcFlag;
            my_heap_ptr[cur_heap_pos].next = head;
            memcpy((char *)my_heap_ptr[cur_heap_pos].tipKey, tipKey, 2 * kmer_len);
            my_heap_ptr[cur_heap_pos].nContigs = 1;
            my_heap_ptr[cur_heap_pos].data = &(my_data_ptr[cur_data_pos]);
            result = &(my_heap_ptr[cur_heap_pos]);
            upc_fence;
            //hashtable->table[hashval].head = &(my_heap_ptr[cur_heap_pos]);
            UPC_ATOMIC_CSWAP_SHPTR(&next, &(hashtable->table[hashval].head), head, result);
            if (next != head) {
                // head changed while prepping, start over
                result = NULL;
                head = next; // use the latest value for head
                UPC_POLL;
                continue;
            }
            (memory_heap->my_heap_pos)++;
        }
    }

    DBG2("add_tip found=%d added contig=%lld rcFlag=%d my_heap_pos=%d my_data_pos=%d %.*s\n", found, (lld) contig, rcFlag, memory_heap->my_heap_pos, memory_heap->my_data_pos, 2*kmer_len, tipKey);
    addCountTime(&(hashtable->addTips), now() - start);
    return 1;
}

shared[] list_t *lookup_and_copy_tipInfo(hash_table_t *hashtable, const char *tipKey, list_t *copy, int kmer_len)
{
    double start = now();
    assert(hashtable != NULL);
    assert(tipKey != NULL);
    assert(copy != NULL);
    assert(hashtable->size > 0);
    assert(hashtable->table != NULL);
    assert(kmer_len < MAX_KMER_SIZE);

    int64_t hashval = hashkey(hashtable->size, tipKey, 2 * kmer_len);
    CHECK_BOUNDS(hashval, hashtable->size);

    shared[] list_t *result = NULL, *head = NULL;

    UPC_ATOMIC_GET_SHPTR(&head, &(hashtable->table[hashval].head));
    result = find_in_ll(head, tipKey, copy, kmer_len, &(hashtable->nNexts));

    addCountTime(&(hashtable->ct), now() - start);
    return NULL;
}

shared[] plist_t *find_in_ll_pt(shared [] plist_t *head, const char *tipKey, plist_t *copy, int kmer_len, int64_t *nNexts)
{
    shared [] plist_t *result = head, *next = NULL;
    int64_t nexts = 0;
    for (; result != NULL; ) {
        (*copy) = *result;
        if (memcmp(copy->tipKey, tipKey, kmer_len * sizeof(char)) == 0) {
            break;
        }
        nexts++;
        next = copy->next;
        /* RSE - Atomic accessors are not necessary here.
         * only head of a bucket is modified, never the next member field of a node
         * UPC_ATOMIC_GET_SHPTR(&next, &(result->next));
         */
        result = next;
    }
    if (nNexts) 
        *nNexts += nexts;
    DBG2("find_in_ll_pt(%.*s) found %d:%x %d deep\n", kmer_len, tipKey, upc_threadof(result), upc_addrfield(result), nexts);
    return result;
}

shared[] plist_t *lookup_and_copy_pt(phash_table_t *hashtable, const char *tipKey, plist_t *copy, int kmer_len)
{
    double start = now();
    assert(hashtable != NULL);
    assert(tipKey != NULL);
    assert(copy != NULL);
    assert(hashtable->size > 0);
    assert(hashtable->table != NULL);
    assert(kmer_len < MAX_KMER_SIZE);

    int64_t hashval = hashkey(hashtable->size, tipKey, kmer_len);
    CHECK_BOUNDS(hashval, hashtable->size);

    shared[] plist_t *head = NULL, *result = NULL, *next = NULL;

    UPC_ATOMIC_GET_SHPTR(&head, &(hashtable->table[hashval].head));

    result = find_in_ll_pt(head, tipKey, copy, kmer_len, &(hashtable->nNexts));

    addCountTime(&(hashtable->ct), now() - start);
    return result;
}

/* Add entry to the hash table for pointsTo */
int add_edge(phash_table_t *hashtable, const char *tipKey, int64_t objectID, char type, pmemory_heap_t *memory_heap, int kmer_len)
{
    double start = now();
    int64_t hashval = hashkey(hashtable->size, tipKey, kmer_len);
    plist_t cached_copy;

    shared[] plist_t *result = NULL, *head = NULL, *next = NULL;

    // implement with atomic accessors, cswap and no locks!

    int64_t cur_data_pos = memory_heap->my_data_pos;
    if (cur_data_pos >= memory_heap->max_pos) {
        WARN("phash_table ran out of data heap space in add_edge! continuing...\n");
        addCountTime(&(hashtable->addEdges), now() - start);
        return 0;
    }
    if (memory_heap->my_heap_pos >= memory_heap->max_pos) {
        WARN("phash_table ran out of heap space in add_edge! continuing...\n");
        addCountTime(&(hashtable->addEdges), now() - start);
        return 0;
    }

    (memory_heap->my_data_pos)++; // we will add an entry

    UPC_ATOMIC_GET_SHPTR(&head, &(hashtable->table[hashval].head));
    /* CSWAP head until it works */
    int64_t attempts = 0;
    while (result == NULL) {
        if (++attempts % 10000 == 0) { WARN("Possible infinite loop here %lld attempts\n", (lld) attempts); }

        result = find_in_ll_pt(head, tipKey, &cached_copy, kmer_len, &(hashtable->nNexts));
        int found = (result == NULL) ? 0 : 1;

        shared[] pdata_t * my_data_ptr = memory_heap->my_data_ptr;
        int64_t cur_heap_pos;
        shared[] plist_t * my_heap_ptr;

        if (found == 1) {
            /* There is such a key, therefore just add the data in the list */
            assert(result != NULL);
            my_data_ptr[cur_data_pos].next = cached_copy.data;
            my_data_ptr[cur_data_pos].object = objectID;
            my_data_ptr[cur_data_pos].type = type;
            upc_fence;
            shared [] pdata_t *test = NULL;
            UPC_ATOMIC_CSWAP_SHPTR(&test, &(result->data), cached_copy.data, &(my_data_ptr[cur_data_pos])); // result->data = &(my_data_ptr[cur_data_pos]);
            if (test != cached_copy.data) { 
                // result->data changed while prepping, start over
                result = NULL;
                UPC_POLL;
                continue;
            }
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(result->nContigs), 1);
        } else {
            /* There is not such a key, therefore add both the entry and data */
            assert(result == NULL);
            cur_heap_pos = memory_heap->my_heap_pos;
            my_heap_ptr = memory_heap->my_heap_ptr;
            my_data_ptr[cur_data_pos].next = NULL;
            my_data_ptr[cur_data_pos].object = objectID;
            my_data_ptr[cur_data_pos].type = type;
            my_heap_ptr[cur_heap_pos].next = head;
            memcpy((char *)my_heap_ptr[cur_heap_pos].tipKey, tipKey, kmer_len * sizeof(char));
            my_heap_ptr[cur_heap_pos].nContigs = 1;
            my_heap_ptr[cur_heap_pos].data = &(my_data_ptr[cur_data_pos]);
            result = &(my_heap_ptr[cur_heap_pos]);
            upc_fence;
            UPC_ATOMIC_CSWAP_SHPTR(&next, &(hashtable->table[hashval].head), head, result); // hashtable->table[hashval].head = &(my_heap_ptr[cur_heap_pos]);
            if (next != head) {
                // bucket.head changed while prepping, start over
                result = NULL;
                head = next; // use the latest value for head
                UPC_POLL;
                continue;
            }
            (memory_heap->my_heap_pos)++;
        }
    }

    DBG2("add_edge(tip=%.*s, objectID=%lld, type=%c, hashval=%lld, head was %d:%x, result=%d:%x)\n", kmer_len, tipKey, (lld) objectID, type, (lld) hashval, upc_threadof(head), upc_addrfield(head), upc_threadof(result), upc_addrfield(result));
    addCountTime(&(hashtable->addEdges), now() - start);
    return 1;
}

int getNext(stream_t input, shared[1] extensions_t *linkertips, shared[1] bubbletip_t *bubbletips, phash_table_t *pointsTo, stream_t *output, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    char type = input.type;
    char strand = input.strand;
    int64_t ID = input.objectId;
    char links[2 * MAX_KMER_SIZE];
    char in[MAX_KMER_SIZE];
    char rcin[MAX_KMER_SIZE];
    char out[MAX_KMER_SIZE];
    char rcout[MAX_KMER_SIZE];
    in[kmer_len] = '\0';
    rcin[kmer_len] = '\0';
    out[kmer_len] = '\0';
    rcout[kmer_len] = '\0';

    bubbletip_t cur_bubbletip;
    extensions_t cur_linkertip;
    plist_t local_pt;
    shared[] plist_t * res_ptr;
    pdata_t res_pdata;
    char pdata_type;

    if (type == 'C') {
        cur_linkertip = linkertips[ID];
        if (cur_linkertip.used_flag != USED_EXT) {
            DIE("FATAL ERROR - LINKERTIGS NOT DEFINED for [%lld]\n", (lld)ID);
        }
        memcpy(in, &(cur_linkertip.data[0]), kmer_len * sizeof(char));
        memcpy(out, &(cur_linkertip.data[kmer_len]), kmer_len * sizeof(char));
    } else if (type == 'B') {
        cur_bubbletip = bubbletips[ID];
        memcpy(in, &(cur_bubbletip.tipKey[0]), kmer_len * sizeof(char));
        memcpy(out, &(cur_bubbletip.tipKey[kmer_len]), kmer_len * sizeof(char));
    } else {
        WARN("UNHANDLED CASE BUUUUUUUUUUUUUUUUUG: type ('%c') != 'C' or 'B'\n", (char)type);
    }

    if (!reverseComplementSeqBubble(in, rcout, kmer_len)) {
        WARN("Unexpected base in reverseComplementSeqBubble for links index %lld, type %c\n", (lld)ID, type);
    }
    if (!reverseComplementSeqBubble(out, rcin, kmer_len)) {
        WARN("Unexpected base in reverseComplementSeqBubble for links index %lld, type %c\n", (lld)ID, type);
    }
    res_ptr = NULL;

    if (strand == '-') {
        if ((rcout[0] != '0')) {
            res_ptr = lookup_and_copy_pt(pointsTo, rcout, &local_pt, kmer_len);
        } else {
            return 0;
        }
    } else {
        if ((out[0] != '0')) {
            res_ptr = lookup_and_copy_pt(pointsTo, out, &local_pt, kmer_len);
        } else {
            return 0;
        }
    }

    if (res_ptr == NULL) {
        return 0;
    }
    if (local_pt.nContigs > 1) {
        return 0;
    }

    res_pdata = *(res_ptr->data);
    pdata_type = res_pdata.type;
    output->objectId = res_pdata.object;
    if (pdata_type == B_PLUS) {
        output->type = 'B';
        output->strand = '+';
    } else if (pdata_type == B_MINUS) {
        output->type = 'B';
        output->strand = '-';
    } else if (pdata_type == C_PLUS) {
        output->type = 'C';
        output->strand = '+';
    } else if (pdata_type == C_MINUS) {
        output->type = 'C';
        output->strand = '-';
    } else {
        WARN("UNHANDLED CASE BUUUUUUUUUUUUUUUUUG: pdata_type (%d) != B+ %d or B- %d or C+ %d or C- %d\n", pdata_type, B_PLUS, B_MINUS, C_PLUS, C_MINUS);
    }

    return 1;
}
