#include <upc.h>
#include "StaticVars.h"
#include "log.h"
#include "upc_common.h"
#include "common.h"

int bubbleFinder_main(int argc, char **argv);

int main(int argc, char **argv)
{
    OPEN_MY_LOG("bubbleFinder");
    return bubbleFinder_main(argc, argv);
}
