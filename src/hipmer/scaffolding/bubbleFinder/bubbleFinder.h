#ifndef BUBBLE_FINDER_H
#define BUBBLE_FINDER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"
#include "Buffer.h"
#include "timers.h"
#include "StaticVars.h"
#include "defines.h"
#include "upc_output.h"

#include "bubbleHash.h"

#define PLUS '+'
#define MINUS '-'

#define USED_EXT 42
#define UNUSED_EXT 0


#define B_PLUS '0'
#define B_MINUS '1'
#define C_PLUS '2'
#define C_MINUS '3'

#define MAX_LINE_SIZE 1000
#define MAX_CONTIG_SIZE 1000000

#define UNVISITED 0
#define VISITED 1

#define MAX_DEPTH_MODE 1
#define DUAL_PATH_MODE 2

#define IS_DIPLOTIG 0
#define IS_ISOTIG 1
#define HAIR_MIN 150



/* cea data structure */
typedef struct cea_t cea_t;
struct cea_t {
    int64_t contigID;
    int     contigLen;
    int     nMers;
    float   meanDepth;
    char    firstMer[MAX_KMER_SIZE];
    char    lastMer[MAX_KMER_SIZE];
    char    prevCodeL;
    char    prevCodeR;
    char    prevBase;
    char    nextBase;
    char    nextCodeL;
    char    nextCodeR;
    char    statusField;
};


typedef struct sequence_t sequence_t;
struct sequence_t {
    shared[] char *data;
    int seqLength;
};

typedef struct local_sequence_t local_sequence_t;
struct local_sequence_t {
    char *data;
    int   seqLength;
};

// data type for storing output contig info
typedef struct {
    int64_t contigId;
    int     depth;
    int     contigType;
    size_t  recordLen;
    int     diplotigId;
} IdLengthDepth;


int reverseComplementSeqBubble(const char *seq, char *rc_seq, size_t seqLen);

int splitDEPTH(char *inputLine, int64_t *contigID, int *nMers, float *depth);


int bubbleFinder_main(int argc, char **argv);

void recordIdLengthDepth(int64_t contigId, char *finalSequence, int depth, int contigType, Buffer haplotigsFDBuffer, int64_t diplotigId);



#endif //BUBBLE_FINDER_H
