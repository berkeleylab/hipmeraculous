#include "bubbleFinder2D.h"

shared int64_t McontigID;


/* Parses a single alignment */
/* Returns 1 if the alignemnt is valid. Returns 0 if the alignment is a guard alignment */
int parseAlignment(char *inputAln, char *readName, int64_t *subjectId)
{
    char *token;
    char *aux;
    int result = 0;

    token = strtok_r(inputAln, "\t", &aux); //!!truncates original string

    if (strcmp(token, "MERALIGNER-F") == 0) {
        return -1;
    }

    if (strcmp(token, "MERALIGNER-0") == 0) {
        result = 1;
    }

    if (strcmp(token, "MERALIGNER-1") == 0) {
        result = 2;
    }

    if (strcmp(token, "MERALIGNER-2") == 0) {
        result = 3;
    }

    token = strtok_r(NULL, "\t", &aux);
    if (readName) {
        assert(sizeof(token) <= MAX_READ_NAME_LEN);
        strcpy(readName, token);
        assert(strlen(readName) < MAX_READ_NAME_LEN); // ensure it's null-terminated
    }

    token = strtok_r(NULL, "\t", &aux);
    //(*qStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    //(*qStop) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    //(*qLength) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*subjectId) = atoi(token + 6);
    //token = strtok_r(NULL, "\t", &aux);
    //(*sStart) = atoi(token);
    //token = strtok_r(NULL, "\t", &aux);
    //(*sStop) = atoi(token);
    //token = strtok_r(NULL, "\t", &aux);
    //(*sLength) = atoi(token);
    //token = strtok_r(NULL, "\t", &aux);
    //(*strand) = ( strcmp(token, "Plus") == 0) ? PLUS : MINUS;

    return result;
}


/* Creates shared heaps that required for alignment heaps - IMPORTANT: This is a collective function */
int create_alignment_heaps(int64_t nContigs, alignment_heap_t *alignment_heap, shared int64_t *cloneCnts)
{
    UPC_ALL_ALLOC_CHK(alignment_heap->alignment_ptr, nContigs, sizeof(shared_alignment_ptr));
    UPC_ALL_ALLOC_CHK(alignment_heap->heap_indices, nContigs, sizeof(int64_t));

    for (int64_t i = MYTHREAD; i < nContigs; i += THREADS) {
        UPC_ALLOC_CHK0(alignment_heap->alignment_ptr[i], (cloneCnts[i] + 1) * sizeof(alignment_t));
        alignment_heap->heap_indices[i] = 0;
    }
    upc_barrier;
    return 0;
}

void destroy_alignment_heaps(int64_t nContigs, alignment_heap_t *alignment_heap)
{
    for (int64_t i=MYTHREAD; i < nContigs; i+=THREADS) {
        UPC_FREE_CHK0(alignment_heap->alignment_ptr[i]);
    }
    upc_barrier;
    UPC_ALL_FREE_CHK(alignment_heap->alignment_ptr);
    UPC_ALL_FREE_CHK(alignment_heap->heap_indices);

}

int isPairedRead(const char *readName) 
{
    int len = strlen(readName);
    if (len >= 3 && readName[len-2] == '/') {
        char pair = readName[len-1];
        if (pair == '1') {
            return 1;
        } else if (pair == '2') {
            return 2;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

/* truncates string from the right end at the point encountering the specifiec character */
void rtrim_at(const char *string, char rtrimMark, char *out)
{
    char *pos = strrchr(string, rtrimMark);
    if (pos && pos != string) {
        memcpy(out, string, pos-string);
        out[pos-string] = '\0';
    } else {
        strcpy(out, string);
    }
}

int rb_tree_cmp_clone(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b)
{
    char *a = (char *) node_a->value;
    char *b = (char *) node_b->value;
    return strcmp(a, b);
}

void rb_tree_delete_clone_cb(struct rb_tree *self, struct rb_node *node)
{
    free_chk0(node->value);
    rb_tree_node_dealloc_cb(self, node);
}

int add_clone(struct rb_tree *clones_hash, char *cloneName)
{
    char *c;

    c = (char *) rb_tree_find(clones_hash, cloneName);
    if (c == NULL) {
        c = strdup_chk0(cloneName);
        return rb_tree_insert(clones_hash, c);
    } else {
        return 0;
    }
}

bool clone_exists(struct rb_tree *clones_hash, char *cloneName)
{
    return (rb_tree_find(clones_hash, cloneName) != NULL);
}

void delete_all_clones(struct rb_tree *clones_hash)
{
    rb_tree_clear(clones_hash, rb_tree_delete_clone_cb);
}

int link_clones_to_path(h_path *path_ptr, fixed_read_name *btigClones, int minSupport)
{
    int nPathLinks = 0;
    int i = 0;

    if (btigClones == NULL) {
        return 0;
    }
    assert(path_ptr->path_clones);
    for (i = 0; btigClones[i][0] != 0; i++) {
        char *clone = btigClones[i];
        if (clone_exists(path_ptr->path_clones, clone)) {
            nPathLinks++;
        }
	if (nPathLinks == minSupport) {
	  return nPathLinks;
	}
    }
    return 0;
}


int assign_btig_to_hpath(enhanced_data_t *btig, h_path *hPaths)
{
    int toPathIdx = -1;
    int assigned = 0;
    int i;
    int minSupport = 2;

    for (i = 0; i < NUM_HPATHS; i++) {
        int64_t nSupportLinkage = link_clones_to_path(&hPaths[i], btig->clones, minSupport);
        if (nSupportLinkage) {
	  DBG2("Contig%ld is linked to halo-path %d by at least %ld mapped clones\n", btig->contig, i, nSupportLinkage);
            toPathIdx = i;
            assigned++;
        }
    }

    if (assigned == 1) {
        return toPathIdx;
    } else if (assigned == 0) {
        DBG2("Couldn't link Contig %ld  to either path!\n", btig->contig);
        return -1;
    } else {
        DBG2("Contig %ld fits both paths..\n", btig->contig);
        return -1;
    }
}

// copy tig structure into haplopath
void add_tig_to_hpath(h_path *hp, enhanced_data_t *tig)
{
    int last = hp->n;

    hp->n++;

    memcpy(&hp->tigs[last], tig, sizeof(*tig)); // shallow copy!
    hp->tigs[last].clones = NULL;               //we don't store individual tigs' clones within the hpath

    // FIXME there is a memory leak here, but we can neither free the existing nor assert it is NULL!
    //if(hp->tigs[last].seq != NULL) free_chk(hp->tigs[last].seq);
    //assert(hp->tigs[last].seq == NULL);
    hp->tigs[last].seq = malloc_chk((tig->length + 1) * sizeof(char));

    assert(tig->seq[tig->length] == '\0');

    strcpy(hp->tigs[last].seq, tig->seq);

    DBG("Added tig %ld (%d bp) to path\n", hp->tigs[last].contig, hp->tigs[last].length);
    assert(hp->path_clones);

    if (tig->type == 'B' && tig->clones != NULL) { //we have alignments to this bubbletip
        int i = 0;
        for (i = 0; tig->clones[i][0] != '\0'; i++) {
            add_clone(hp->path_clones, tig->clones[i]);
        }
        DBG("%u clones currently assigned to path\n", rb_tree_size(hp->path_clones));
    }
}

// Complete the haplopaths with seq and depth info
void bless_haplopaths(h_path *haploPaths, shared[1] sequence_t *contigSequences, int maskMode, int kmer_len)
{
    int i;
    int c;
    int j;

    int64_t cid;
    double start = now();

    /* We soft-mask all C-tig sequences to avoid ambiguous read mapping down the line.
     * Because of redundancy clipping of btigs, we leave the first and last (k-1)/2 bases of the C-tig unmasked.
     * This ensures that there will be at least one variant kmer left unmasked*/
    int defOffset = (kmer_len - 1) / 2; //default masking offset

    for (i = 0; i < NUM_HPATHS; i++) {
        double wContigDepthSum = 0;
        int btigLenSum = 1;
        for (c = 0; c < haploPaths[i].n; c++) {
            DBG2("\tBlessing HP%d: adding %lu bp from %c-tig %ld \n", i, strlen(haploPaths[i].tigs[c].seq), haploPaths[i].tigs[c].type, haploPaths[i].tigs[c].contig);

            /* mean depth is calculated using B-tigs only */
            if (haploPaths[i].tigs[c].type == 'B') {
                cid = haploPaths[i].tigs[c].contig;
                /* here we use original untrimmed length since that reflects the kmer depth more accurately */
                btigLenSum += contigSequences[cid].seqLength;
                wContigDepthSum += haploPaths[i].tigs[c].depth * contigSequences[cid].seqLength;
                assert(wContigDepthSum > 0);
            }


            if (haploPaths[i].tigs[c].type == 'C' && maskMode) {
                int maskStart;
                int maskEnd;

                /* If the C-tigs surrounding a bubble actually overlap, this indicates that the bubble consits of
                 * low complexity sequence which is redundant with the Ctigs, i.e. the b-tigs get trimmed completely and redundant
                 * sequence had to be clipped from the upstream C-tig itself.
                 * To make sure we have the variant sequence capturable by a unique k-seed, we keep (k-2 - overlap) bases unmasked on both sizes of
                 * the expected bubble.  */


                if (c == 0) {                                             // first Ctig - mask from the beginning
                    maskStart = 1;
                } else if (c >= 2 && haploPaths[i].tigs[c - 2].endClip) { // we're downstream of a low-complexity Btig --  the previous C-tig has been clipped
                    maskStart = kmer_len - 2;
                } else {                                                  // Ctig follows a normal btig
                    maskStart = defOffset + 1;
                }
                DBG2("maskStart: %d\n", maskStart);

                if (c + 1 == haploPaths[i].n) {             // final Ctig - mask to the end
                    maskEnd = haploPaths[i].tigs[c].length;
                } else if (haploPaths[i].tigs[c].endClip) { // we're upsream of a low-complexity Btig -- this Ctig has been clipped
                    maskEnd = haploPaths[i].tigs[c].length + haploPaths[i].tigs[c].endClip - (kmer_len - 2);
                } else {
                    maskEnd = haploPaths[i].tigs[c].length - defOffset;
                }
                DBG2("maskEnd: %d\n", maskEnd);

                if (maskEnd < maskStart) { // handle cases where there's too little of Ctig left to mask; ensure at least one bp is masked
                    if (haploPaths[i].tigs[c].endClip) {
                        maskStart = maskEnd;
                    } else {
                        maskEnd = maskStart;
                    }
                    DBG2("fixed maskEnd: %d\n", maskEnd);
                }

                //apply lowercase as mask
                for (j = maskStart; j <= maskEnd; j++) {
                    haploPaths[i].tigs[c].seq[j - 1] = tolower(haploPaths[i].tigs[c].seq[j - 1]);
                }
            }

            haploPaths[i].totLength += haploPaths[i].tigs[c].length;
            strcat(haploPaths[i].finalSeq, haploPaths[i].tigs[c].seq);
            assert(haploPaths[i].finalSeq[haploPaths[i].totLength] == '\0');
        }
        haploPaths[i].depth = wContigDepthSum / btigLenSum;
        DBG2("\tHP depth: %f\n", haploPaths[i].depth);
        DBG2("\tHP length: %d\n", haploPaths[i].totLength);
    }
    DBG("bless_haplopaths tool %0.3fs NUM_HPATHS=%lld\n", now() - start, (lld) NUM_HPATHS);
}

// Record haploPath sequence into buffer for later use (filtering, writing, etc)
void store_haplopaths(h_path *haploPaths, int64_t diplotigId, int64_t *curMetaConIdPtr, int64_t nBubbles, int64_t nCons, Buffer haplotigsFDBuffer)
{
    int64_t cid = 0; //don't store global contig ids unless in DEBUG
    double start = now();
    assert(haploPaths[0].n == haploPaths[1].n);

#ifdef DEBUG
    UPC_ATOMIC_FADD_I64(curMetaConIdPtr, &McontigID, NUM_HPATHS);
#endif
    for (int i = 0; i < NUM_HPATHS; i++) {
#ifdef DEBUG
        cid = (*curMetaConIdPtr) + i;
#endif
        recordIdLengthDepth(cid, haploPaths[i].finalSeq, haploPaths[i].depth, IS_DIPLOTIG, haplotigsFDBuffer, diplotigId);

        DBG2("H-path %d (diplotig %ld): %s\n", i, diplotigId, haploPaths[i].finalSeq);

        // dump unfiltered
        //    GZIP_PRINTF(haplotigsFD_raw, ">Contig_%ld\n", cid);
        //    GZIP_PRINTF(haplotigsFD_raw, "%s\n", haploPaths[i].finalSeq);
    }
#ifdef DEBUG
    *curMetaConIdPtr += NUM_HPATHS - 1;
#endif
    DBG("store_haplopaths took %0.3fs. nBubbles=%lld nCons=%lld\n", now()-start, (lld) nBubbles, (lld) nCons);
}
