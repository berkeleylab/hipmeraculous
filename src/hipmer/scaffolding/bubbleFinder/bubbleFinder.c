#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <upc_tick.h>
#include <libgen.h>
#include <unistd.h>
#include <glob.h>
#include "optlist.h"
#include "upc_common.h"
#include "common.h"

#include "Buffer.h"
#include "upc_compatibility.h"
#include "kseq.h"
#include "utils.h"
#include "upc_output.h"
#include "timers.h"
#include "findDMin.h"
#include "../../meraligner/ssw.h"
#include "../../contigs/kmer_handling.h"

#include "bubbleFinder.h"
#include "bubbleFinder2D.h"
#include "bubbleHash.h"


static int kmer_len = MAX_KMER_SIZE - 1;

/* Define segment length for FASTA sequences */
#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

#define CHECK_NCONTIG_BOUND(i) if ((i) >= nContigs || (i) < 0) { \
        DIE("out of bounds %lld > %lld\n", (lld)(i), (lld)nContigs); }


#define EXPAND_ARRAY(array, index, maxSize) do { \
        if (index >= maxSize) { \
            LOGF("Expanding %s from %lld to %lld (%0.3f MB)\n", # array, (lld)maxSize, (lld)maxSize * 2, (maxSize * 2) * sizeof(*array) / (double)ONE_MB); \
            maxSize *= 2; \
            array = realloc_chk(array, maxSize * sizeof(*array)); \
        } \
} while (0)

shared int64_t _bubbleID;

shared int64_t diplotigID;
shared int64_t totDiplotigLength;
shared int64_t indTravs;
shared int64_t _totConflicts;
shared int64_t McontigID;
shared int64_t _bubbleContigMinDepth;

// global histogram defaults
int bin_size = 4;
int histogram_size = 1000;

alignment_heap_t alignment_heap;



//#define MAX_HAIR_DEPTH 20.0
//typedef shared[] char* shared_sequence_ptr;





int64_t writeHaplotigs(const char *base_dir, const char *newFileName, Buffer haplotigsFDBuffer, const char diploidMode, size_t *unfilteredCount)
{
    int64_t written = 0;
    char *bufPos;
    IdLengthDepth ild;

    //fprintf(stderr, "Thread %d: reading from haplotigsFDBuffer length: %lld\n", MYTHREAD, (lld) getLengthBuffer(haplotigsFDBuffer));
    assert(getPosBuffer(haplotigsFDBuffer) == 0);
    int64_t len;
    size_t old_pos = haplotigsFDBuffer->pos;
    int64_t my_num_haplotigs_passed = 0;
    int64_t bufLen = getLengthBuffer(haplotigsFDBuffer);

    //LOGF("writeHaplotigs() starting with buffer sized %lld of %lld\n", (lld) bufLen, (lld) getSizeBuffer(haplotigsFDBuffer));
    int64_t bubbleContigMinDepth = broadcast_long(_bubbleContigMinDepth, 0);
    UPC_TICK_T start = UPC_TICKS_NOW();

    if (bufLen > 0) {
        while ((len = readBuffer(haplotigsFDBuffer, &ild, sizeof(IdLengthDepth))) > 0) {
            //        CHECK_NCONTIG_BOUND(ild.contigId);
            if (diploidMode == DUAL_PATH_MODE ||
                (ild.contigType == IS_DIPLOTIG || ild.depth >= bubbleContigMinDepth)) {
                my_num_haplotigs_passed++;
            }
            assert(ild.recordLen > sizeof(IdLengthDepth));
            haplotigsFDBuffer->pos += ild.recordLen - sizeof(IdLengthDepth);
            if (haplotigsFDBuffer->pos > haplotigsFDBuffer->len) {
                DIE("Corrupted haplotigsFDBuffer - request exceeds contents: pos=%lld recordLen=%lld len=%lld size=%lld haplotigs=%lld\n", (lld)haplotigsFDBuffer->pos, (lld)ild.recordLen, (lld)haplotigsFDBuffer->len, (lld)haplotigsFDBuffer->size, (lld)my_num_haplotigs_passed);
            }
            DBG2("my_num_haplotigs_passed=%lld, bufPos=%lld contigID=%lld recordLen=%lld\n", (lld)my_num_haplotigs_passed, (lld)getPosBuffer(haplotigsFDBuffer), (lld)ild.contigId, (lld)ild.recordLen);
            if (haplotigsFDBuffer->pos > bufLen) {
                DIE("haplotigsFDBuffer is corrupted! pos=%lld len=%lld\n", (lld)haplotigsFDBuffer->pos, (lld)bufLen);
            }
            DBG2("my_num_haplotigs_passed=%lld, bufPos=%lld contigID=%lld recordLen=%lld\n", (lld)my_num_haplotigs_passed, (lld)getPosBuffer(haplotigsFDBuffer), (lld)ild.contigId, (lld)ild.recordLen);
        }
    }
    rewindBuffer(haplotigsFDBuffer);
    UPC_TICK_T endRead = UPC_TICKS_NOW();
    LOGF("Found %lld haplotigs in %0.3f s\n", (lld)my_num_haplotigs_passed, UPC_TICKS_TO_SECS(endRead - start));

    UPC_LOGGED_BARRIER;
    SLOG("Starting reduction\n");

    int64_t cid = reduce_prefix_long(my_num_haplotigs_passed, UPC_ADD, NULL) - my_num_haplotigs_passed;

    UPC_TICK_T endReduce = UPC_TICKS_NOW();
    //LOGF("writeHaplotigs(): read haplotigsFDBuffer in %0.3f s. cid = %lld, my_num_haplotigs_passed = %lld\n", UPC_TICKS_TO_SECS(endReduce - start), (lld) cid, (lld) my_num_haplotigs_passed);

    char myHaplotigsFileName[MAX_FILE_PATH];
    snprintf(myHaplotigsFileName, MAX_FILE_PATH, "%s.fasta" GZIP_EXT, newFileName);
    GZIP_FILE haplotigsFD = openCheckpoint(myHaplotigsFileName, "w");

    char myOutputDepthFileName[MAX_FILE_PATH];
    snprintf(myOutputDepthFileName, MAX_FILE_PATH, "merDepth_%s.txt" GZIP_EXT, newFileName);
    GZIP_FILE depth_out_FD = openCheckpoint(myOutputDepthFileName, "w");
    if (bufLen > 0) {
        while ((len = readBuffer(haplotigsFDBuffer, &ild, sizeof(IdLengthDepth))) > 0) {
            assert(len == sizeof(IdLengthDepth));
            size_t outputLen = ild.recordLen - sizeof(IdLengthDepth);
            //fprintf(stderr, "Thread %d: read Contig %lld with %d depth and %lld size\n", MYTHREAD, (lld) ild.contigId, ild.depth, (lld) outputLen);

            // in DUAL_PATH_MODE, print depth info for all contigs
            // Otherwise print only for diplotigs and isotigs over the bubble depth cutoff
            if (diploidMode == DUAL_PATH_MODE ||
                (ild.contigType == IS_DIPLOTIG || ild.depth >= bubbleContigMinDepth)) {
                bufPos = getCurBuffer(haplotigsFDBuffer);
                assert(bufPos[0] == '>');
                char *id = strchr(bufPos, '\n');
                if (id == NULL) {
                    DIE("Invalid haplotigsFDBuffer id!: %.*s", (int)ild.recordLen, bufPos);
                }
                assert(*id == '\n');
                int idLen = id - bufPos + 1;

                DBG("Writing contig %lld. outputLen=%lld, idLen=%lld, kmer_len=%lld, ild.depth=%lld, strlen(id)=%lld,  diplotig_id? %lld, id=%.*s\n", (lld)cid, (lld)outputLen, (lld)idLen, (lld)kmer_len, (lld)ild.depth, (lld)strlen(id), ild.diplotigId, 5, (id + 1));

                /* if (mappingFD != NULL) { */
                /*   if (ild.diplotigId >=0) { */
                /*     GZIP_PRINTF(mappingFD, "(diplotig%lld) ==> Contig%lld\n", (lld) ild.diplotigId, (lld) cid); */
                /*   } */
                /* } */


                // TODO: Aggregate writes to improve further the performance!

                GZIP_PRINTF(depth_out_FD, "Contig%lld\t%lld\t%d\t%lld\n", (lld)cid, (lld)outputLen - idLen - kmer_len, ild.depth, (lld)ild.diplotigId);

                GZIP_PRINTF(haplotigsFD, ">Contig_%lld\n", (lld)cid);
                id++; // skip newline
                assert(*id == 'A' || *id == 'C' || *id == 'G' || *id == 'T' || *id == 'a' || *id == 'c' || *id == 'g' || *id == 't');
                size_t wrote = GZIP_FWRITE(id, 1, outputLen - idLen, haplotigsFD);
                if (wrote != outputLen - idLen) {
                    DIE("Could not write %lld bytes (%lld wrote) to %s! %s\n", (lld)outputLen - idLen, (lld)wrote, myHaplotigsFileName, strerror(errno));
                }

                written++;
                cid++;
            }
            (*unfilteredCount)++;
            haplotigsFDBuffer->pos += outputLen;
        }
    }
    closeCheckpoint(haplotigsFD);
    closeCheckpoint(depth_out_FD);
    UPC_TICK_T end = UPC_TICKS_NOW();
    LOGF("Wrote haplotigs in %0.3f s (total %0.3f s)\n", UPC_TICKS_TO_SECS(end - endReduce), UPC_TICKS_TO_SECS(end - start));
    return written;
}

/* Split a merDepth line */
int splitDEPTH(char *inputLine, int64_t *contigID, int *nMers, float *depth)
{
    char *token;
    char *aux;

    // merDepth lines are printed with the following command:
    // fprintf(myOutputFile, "Contig%lld\t%d\t%f\n", (lld) contigID, nMers, mean);

    token = strtok_r(inputLine, "\t", &aux);
    (*contigID) = atol(token + 6);
    token = strtok_r(NULL, "\t", &aux);
    (*nMers) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*depth) = atof(token);

    return 1;
}

/* Split a CEA line and store the contigs in a cea data structure */
int splitCEA(char *inputLine, cea_t *ceaEntry, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    char *token;
    char *aux;

    // CEA lines are printed with the following command:
    // fprintf(myOutputFile,"Contig%lld\t[%c%c]\t(%c)\t%s\t%d\t%s\t(%c)\t[%c%c]\n", (lld) contigID, prevCodeL, prevCodeR, prevBase, firstKmer, contigLen, lastKmer, nextBase, nextCodeL, nextCodeR );

    token = strtok_r(inputLine, "\t", &aux);
    ceaEntry->contigID = atol(token + 6);
    token = strtok_r(NULL, "\t", &aux);
    ceaEntry->prevCodeL = *(token + 1);
    ceaEntry->prevCodeR = *(token + 2);
    token = strtok_r(NULL, "\t", &aux);
    ceaEntry->prevBase = *(token + 1);
    token = strtok_r(NULL, "\t", &aux);
    memcpy(ceaEntry->firstMer, token, kmer_len * sizeof(char));
    token = strtok_r(NULL, "\t", &aux);
    ceaEntry->contigLen = atol(token);
    token = strtok_r(NULL, "\t", &aux);
    memcpy(ceaEntry->lastMer, token, kmer_len * sizeof(char));
    token = strtok_r(NULL, "\t", &aux);
    ceaEntry->nextBase = *(token + 1);
    token = strtok_r(NULL, "\t", &aux);
    ceaEntry->nextCodeL = *(token + 1);
    ceaEntry->nextCodeR = *(token + 2);

    return 1;
}

char reverseComplementBaseStrictBubble(const char base)
{
    char rc;

    switch (base) {
    case 'A':
        rc = 'T'; break;
    case 'C':
        rc = 'G'; break;
    case 'G':
        rc = 'C'; break;
    case 'T':
        rc = 'A'; break;
    case '0':
        rc = '0'; break;
    default:
        // unexpected base error
        rc = 'X';
    }
    return rc;
}

int reverseComplementSeqBubble(const char *seq, char *rc_seq, size_t seqLen)
{
    int end = seqLen - 1;
    int start = 0;
    char temp;

    int ok = 1;

    if (seqLen % 2 == 1) {
        /* has odd length, so handle middle */
        int pos = (start + end) / 2;
        rc_seq[pos] = reverseComplementBaseStrictBubble(seq[pos]);
        if (rc_seq[pos] == 'X') {
            ok = 0;
            rc_seq[pos] = '0';
        }
    }
    while (start < end) {
        temp = seq[end]; // in case this is inplace! (seq == rc_seq)
        rc_seq[end] = reverseComplementBaseStrictBubble(seq[start]);
        if (rc_seq[end] == 'X') {
            ok = 0;
            rc_seq[end] = '0';
        }
        rc_seq[start] = reverseComplementBaseStrictBubble(temp);
        if (rc_seq[start] == 'X') {
            ok = 0;
            rc_seq[start] = '0';
        }
        ++start;
        --end;
    }
    return ok;
}

int reverseComplementINPLACEBubble(char *subcontig, int64_t size)
{
    return reverseComplementSeqBubble(subcontig, subcontig, size);
}

/* qsort struct comparison function (based on sequence length) */
int struct_cmp_by_length(const void *a, const void *b)
{
    enhanced_data_t *ia = (enhanced_data_t *)a;
    enhanced_data_t *ib = (enhanced_data_t *)b;

    return (int)(ia->length - ib->length);
}

int int64_comp(const void *a, const void *b)
{
    int64_t *ia = (int64_t *)a;
    int64_t *ib = (int64_t *)b;

    return (int)((*ia) - (*ib));
}

int cmp_by_name(const void *_a, const void *_b) {
    char *a = (char*) _a, *b = (char*) _b;
    return strcmp(a,b);
}

void update_depth_histogram(int64_t *hist, float depth)
{
    int int_part = (int)depth / bin_size;

    if (int_part < histogram_size) {
        hist[int_part]++;
    } else {
        hist[histogram_size - 1]++;
    }
}




/* A mega-function for going down the C-B-C chain and building a single or a pair of diplotigs (depends on diploidMode)*/
int traverse_CBC_chain(
    const char diploidMode, int maskMode,
    BV_TYPE(stream_t) _upstream, int64_t seedId, BV_TYPE(stream_t) _downstream, int64_t upstr_pos,
    int64_t nTigs, int64_t nContigs, int64_t *myDiplotigs,
    shared[1] cea_t *contigInfo, shared[1] bubbletip_t *bubbletips, shared[1] sequence_t *contigSequences,
    hash_table_t *tipInfo,
    Buffer haplotigsFDBuffer,
    int64_t *local_histo, int64_t *local_histoDiplotigs
    )
{

    double start = now();
    BV_INIT2(stream_t, upstream, _upstream);
    BV_INIT2(stream_t, downstream, _downstream);

    int64_t curMetaConId = 0;
    list_t tiEntry;
    enhanced_data_t *btigs = NULL;
    int64_t nBtigs;
    int64_t nMers;
    int64_t g;
    shared[] data_t * data_t_ptr = NULL;
    char s;
    int64_t cid;
    float depth, maxDepth;
    sequence_t cur_seq;
    char *tig_seq = NULL;
    int cur_len, minLen, minClipLen, maxDepthIndex, maxDepthLength, btigLen, clipLen, diff, length_to_copy, cur_path_point_added, cur_entry_len;

    int64_t curDiplId = 0;
    int64_t overall_ptr = 0;
    int64_t ind;
    stream_t tig;
    char type;
    char strand;
    int64_t id;
    int64_t readID;
    alignment_t *localAlignsTMP_arr = NULL;
    int64_t nCtigMers = 0;
    float meanCtigDepth = 0;
    float meanBtigDepth = 0;
    int64_t nBubbles = 0;
    int64_t nCons = 0;
    h_path *haploPaths = NULL;
    enhanced_data_t *suspendedBtigs[NUM_HPATHS];
    int64_t nSuspended = 0;


    char *finalSequence = NULL, *tailSeq = NULL, *addBack = NULL, *paths = NULL, *clipSeq = NULL;
    if (diploidMode == DUAL_PATH_MODE) { // in this mode we don't store a single "final" sequence
        finalSequence = (char *)malloc_chk(sizeof(char));
    } else {
        finalSequence = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    }
    tailSeq = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    addBack = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    paths = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    clipSeq = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));


    if (diploidMode == DUAL_PATH_MODE) {
        /* Initialize haplo-paths */

        haploPaths = calloc_chk(NUM_HPATHS, sizeof(h_path));
        for (int p = 0; p < NUM_HPATHS; p++) {
            haploPaths[p].tigs = calloc_chk(nTigs, sizeof(*haploPaths[p].tigs));
            haploPaths[p].n = 0;
            haploPaths[p].totLength = 0;
            haploPaths[p].finalSeq = malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
            haploPaths[p].finalSeq[0] = '\0';
            haploPaths[p].path_clones = rb_tree_create(rb_tree_cmp_clone);
        }
    }
    UPC_ATOMIC_FADD_I64(&curDiplId, &diplotigID, 1);
    (*myDiplotigs)++;

    nCtigMers = 0;
    nBubbles = 0;
    nCons = 0;
    finalSequence[0] = '\0';

    /* Clever way to iterate over the array: upstream + "C+contigID" + downstream */
    CountTimer nTigsTime, Btime, Ctime, BtigsTime, Btigs2Time, cloneTime, alignTime, hpathTime;
    initCountTimer(&nTigsTime);
    initCountTimer(&Btime);
    initCountTimer(&Ctime);
    initCountTimer(&BtigsTime);
    initCountTimer(&Btigs2Time);
    initCountTimer(&cloneTime);
    initCountTimer(&alignTime);
    initCountTimer(&hpathTime);

    // For reproducibility, choose a direction of traversal based on the lesser of front and back objectIds
    // i.e. Front -> Back or Back -> Front
    
    int64_t front_id,back_id;
    if (0 < upstr_pos) {
        front_id = BV_GET(upstream, upstr_pos - 1).objectId;
    } else { 
        assert( 0 == upstr_pos );
        front_id = seedId;
    }
    if (nTigs-1 == upstr_pos) {
        back_id = seedId;
    } else {
        back_id = BV_GET(downstream, nTigs-1 - upstr_pos - 1).objectId;
    }

    DBG("traverse_CBC: front=%lld back=%lld nTigs=%lld:", front_id<back_id?front_id:back_id, front_id<back_id?back_id:front_id, nTigs);
    FOR_BY_DIR(overall_ptr, 0, nTigs-1, front_id < back_id ) {
        int64_t id = (overall_ptr < upstr_pos ? BV_GET(upstream, upstr_pos-overall_ptr-1).objectId : (overall_ptr == upstr_pos ? seedId : BV_GET(downstream, overall_ptr-upstr_pos-1).objectId));
        DBGN("\t%c:%lld", ((overall_ptr + 2*nTigs) - upstr_pos) % 2 == 0 ? 'C' : 'B', (lld) id);
    }
    DBGN("\n"); 
    
    int64_t num_more_than_two = 0;
    int64_t tigCntr =0;
    FOR_BY_DIR(overall_ptr, 0, nTigs-1, front_id < back_id ) {
        double nTigsStart = now();
	tigCntr++;
        if (overall_ptr < upstr_pos) {
            tig = BV_GET(upstream, upstr_pos - overall_ptr - 1);
        }
        if (overall_ptr == upstr_pos) {
            tig.type = 'C';
            tig.strand = '+';
            tig.objectId = seedId;
        }
        if (overall_ptr > upstr_pos) {
            tig = BV_GET(downstream, overall_ptr - upstr_pos - 1);
        }
	//reverse strand of each node if we've reversed the direction of traversal
	if ( ! (front_id < back_id) ) {
            tig.strand = tig.strand == '+' ? '-' : '+';
	}
	
        type = tig.type;
        strand = tig.strand;
        id = tig.objectId;
        CHECK_NCONTIG_BOUND(id);

        if (type == 'B') {
            double Bstart = now();
            nBubbles++;
            DBG2("BUBBLE %ld...\n", nBubbles);
            nSuspended = 0;

            if (diploidMode == DUAL_PATH_MODE) {
                for (int i = 0; i < NUM_HPATHS; i++) {
                    suspendedBtigs[i] = NULL;
                }
            }

            char curLinks[2 * MAX_KMER_SIZE];
            upc_memget(curLinks, bubbletips[id].tipKey, 2 * kmer_len * sizeof(char));
            lookup_and_copy_tipInfo(tipInfo, curLinks, &tiEntry, kmer_len);
            nBtigs = tiEntry.nContigs;
            if (nBtigs > 2) {
                num_more_than_two++;
                DBG("NOTICE: Bubble %ld consists of more than two bubble-tigs!  Only two-path bubbles are supported. Will use the first two and ignore the rest.\n", nBubbles);
                nBtigs = 2;
            }
            btigs = (enhanced_data_t *)calloc_chk(nBtigs, sizeof(enhanced_data_t));

            data_t_ptr = tiEntry.data;
            data_t tmp;
            for (g = 0; g < nBtigs; g++) {
                tmp = (*data_t_ptr);
                btigs[g].contig = tmp.contig;
                btigs[g].rcFlag = tmp.rcFlag;
                btigs[g].type = type;
                btigs[g].endClip = 0;
                data_t_ptr = data_t_ptr->next;
            }

            for (g = 0; g < nBtigs; g++) {
                double btigsStart = now();
                s = btigs[g].rcFlag;
                cid = btigs[g].contig;
                CHECK_NCONTIG_BOUND(cid);
                contigInfo[cid].statusField = 2;    //earmark as part of a future diplotig
                depth = contigInfo[cid].meanDepth;
                assert(depth > 0);
                btigs[g].depth = depth;

                cur_seq = contigSequences[cid];
                cur_len = cur_seq.seqLength;
                tig_seq = (char *)malloc_chk((cur_len + 1) * sizeof(char));
                assert(cur_seq.data != NULL);
                upc_memget(tig_seq, cur_seq.data, (cur_len) * sizeof(char));
                tig_seq[cur_len] = '\0';
                if (s != strand) {
                    if (!reverseComplementINPLACEBubble(tig_seq, cur_len)) {
                        WARN("unexpected base, id %lld\n", (lld)cid);
                    }
                }

                if (btigs[g].seq) DIE("Did not expect there to be a sequence already in btigs[%d]\n", g);
                btigs[g].seq = malloc_chk((cur_len + 1) * sizeof(char));
                strcpy(btigs[g].seq, tig_seq);
                assert(btigs[g].seq[cur_len] == '\0');

                free_chk(tig_seq);
                tig_seq = NULL;

                btigs[g].length = cur_len;

                DBG2("b-tig %d: Contig%d,  %d bp, %s\n", g, (int)cid, btigs[g].length, btigs[g].seq);
                if (diploidMode == DUAL_PATH_MODE) {
                    /* Record clones that aligned to the btig */
                    int64_t nAligns = alignment_heap.heap_indices[cid];
                    //		    DBG2("nAligns in heap for Contig%d: %ld\n", (int)cid, nAligns );
                    if (nAligns > 0) {
                        double cloneStart = now();
                        fixed_read_name cloneName;
                        alignment_t *localAlignsTMP_arr = malloc_chk(nAligns * sizeof(alignment_t));
                        upc_memget(localAlignsTMP_arr, alignment_heap.alignment_ptr[cid], (nAligns) * sizeof(alignment_t));

                        //trim /1 and /2 from read names to get clone name
                        char mark = '/';

                        // only use unique clone names
                        struct rb_tree *nameTree = rb_tree_create(rb_tree_cmp_clone);
                        for (int64_t i = 0; i < nAligns; i++) {
                            double alignStart = now();
                            if (isPairedRead(localAlignsTMP_arr[i].readName)) {
                                rtrim_at(localAlignsTMP_arr[i].readName, mark, cloneName);
                                assert(strlen(cloneName) < sizeof(fixed_read_name));
                            }
                            assert(cloneName[0] != '\0');
                            char *tmp = strdup_chk0(cloneName);
                            if (!rb_tree_insert(nameTree, tmp)) {
                                free_chk0(tmp);
                            }
                            addCountTime(&alignTime, now() - alignStart);
                        }

                        int64_t nClonesU = rb_tree_size(nameTree);
                        btigs[g].clones = malloc_chk((nClonesU + 1) * sizeof(fixed_read_name));

                        int64_t nUnique = 0;
                        struct rb_iter *iter = rb_iter_create();
                        for (char *name = rb_iter_first(iter, nameTree); name ; name= rb_iter_next(iter)) {
                            strcpy(btigs[g].clones[nUnique++], name);
                        }
                        DBG2("Found nUnique=%d out of nClonesU=%d from nAligns=%d\n", nUnique, nClonesU, nAligns);
                        assert(nUnique == nClonesU);
                        btigs[g].clones[nUnique][0] = '\0'; // terminate with empty string in the last entry
                        
                        free_chk(localAlignsTMP_arr);
                        rb_tree_dealloc(nameTree, rb_tree_delete_clone_cb);
			DBG2("Clones added to Contig%ld: %d\n",cid, nUnique);
                        addCountTime(&cloneTime, now() - cloneStart);
                    } else {
                        DBG2("No alignments for B-tig Contig%ld\n", cid);
                        btigs[g].clones = NULL;
                    }
                }
                addCountTime(&BtigsTime, now()-btigsStart);
            }
            /* sort array using qsort functions */
            qsort(btigs, nBtigs, sizeof(enhanced_data_t), struct_cmp_by_length);

            minLen = btigs[0].length;
            minClipLen = minLen - (2 * kmer_len - 4);  //this much would be left of the *smaller* btig after clippin
            tailSeq[0] = '\0';
            paths[0] = '\0';
            cur_path_point_added = 0;
            cur_entry_len = 0;

            if (minClipLen < 0) {
                /* The smaller btig is too short for redundant end clipping,
                 * so instead we clip minClipLen bp from the tail of the last C-tig */
                if (diploidMode == DUAL_PATH_MODE) {
                    // the C-tigs are the same in both paths
                    assert(haploPaths[0].n == haploPaths[1].n);
                    assert(haploPaths[0].n <= nTigs);
                    int lastIdx = haploPaths[0].n - 1;
                    assert(lastIdx >= 0 && lastIdx < nTigs);
                    assert(haploPaths[0].tigs[lastIdx].type == 'C');
                    enhanced_data_t lastCtig;
                    memcpy(&lastCtig, &haploPaths[0].tigs[lastIdx], sizeof(enhanced_data_t)); // shallow copy!
                    strcpy(tailSeq, &lastCtig.seq[(lastCtig.length + minClipLen)]);
                    DBG2("minClipLen: %d; tailSeq: %s\n", minClipLen, tailSeq);
                    /* update haplo-paths with the chopped contig info  (same for both since it's a C-tig)		    */
                    for (int p = 0; p < NUM_HPATHS; p++) {
                        haploPaths[p].tigs[lastIdx].seq[(lastCtig.length + minClipLen)] = '\0';
                        haploPaths[p].tigs[lastIdx].length += minClipLen;
                        haploPaths[p].tigs[lastIdx].endClip = abs(minClipLen);
                        assert(strlen(haploPaths[p].tigs[lastIdx].seq) > 0);
                    }
                    DBG2("Adjusted sequence and length of last C-tig %ld (%d:%s) \n", haploPaths[0].tigs[lastIdx].contig, haploPaths[0].tigs[lastIdx].length, haploPaths[0].tigs[lastIdx].seq);
                } else {
                    assert(strlen(finalSequence) + minClipLen > 0);
                    strcpy(tailSeq, &finalSequence[strlen(finalSequence) + minClipLen]);
                    finalSequence[strlen(finalSequence) + minClipLen] = '\0';
                }
                assert(tailSeq[abs(minClipLen)] == '\0');
            }

            maxDepthIndex = 0;
            maxDepthLength = 0;
            maxDepth = 0.0;

            for (g = 0; g < nBtigs; g++) {
                double btigs2Start = now();
                btigLen = btigs[g].length;
                clipLen = btigLen - (2 * kmer_len - 4);

                if (clipLen < 0) {
                    diff = clipLen - minClipLen;
                    memcpy(addBack, tailSeq, diff * sizeof(char));
                    addBack[diff] = '\0';
                    cur_entry_len = strlen(addBack);
                    if (diploidMode == DUAL_PATH_MODE) {
                        DBG2("Btig %ld too small for clipping: %s\n", btigs[g].contig, btigs[g].seq);
                        btigs[g].seq[0] = '\0';
                        strcpy(btigs[g].seq, addBack);
                        btigs[g].length = diff;
                        DBG2("After adding back tail difference of %d: %s\n", diff, btigs[g].seq);
                    } else {
                        cur_path_point_added = strlen(paths);
                        strcat(paths, addBack);
                    }
                } else {    // this btig is large enough, so we add any tail (if clipped from previous Ctig), and then the clipped sequence
                    memcpy(clipSeq, &(btigs[g].seq[kmer_len - 2]), clipLen * sizeof(char));
                    clipSeq[clipLen] = '\0';
                    cur_entry_len = strlen(tailSeq) + strlen(clipSeq);
                    if (diploidMode == DUAL_PATH_MODE) {
                        DBG2("Btig %ld large enough for clipping:  %s\n", btigs[g].contig, btigs[g].seq);
                        btigs[g].seq[0] = '\0';
                        strcpy(btigs[g].seq, tailSeq);
                        DBG2("After adding tailSeq (if any): %s\n", btigs[g].seq);
                        strcat(btigs[g].seq, clipSeq);
                        DBG2("After adding clipSeq: %s\n", btigs[g].seq);
                        btigs[g].length = cur_entry_len;
                    } else {
                        cur_path_point_added = strlen(paths);
                        strcat(paths, tailSeq);
                        strcat(paths, clipSeq);
                    }
                }

                depth = btigs[g].depth;
                if (depth >= maxDepth) {
                    maxDepth = depth;
                    maxDepthLength = cur_path_point_added;
                    maxDepthIndex = g;
                    length_to_copy = cur_entry_len;
                }

                if (diploidMode == DUAL_PATH_MODE) {
                    /* determine which of the two running haplo-paths this
                     * btig belongs to and update the haplo-path */
                    int pathID = -1;
                    if (nBubbles > 1) { // not the first bubble (we can't possibly link first bubble to anything)
                        pathID = assign_btig_to_hpath(&btigs[g], haploPaths);
                    }

                    if (pathID >= 0 && (haploPaths[pathID].n < (nBubbles + nCons))) {
                        assert(haploPaths[pathID].n <= nTigs);
                        add_tig_to_hpath(&haploPaths[pathID], &btigs[g]);
                        assert(haploPaths[pathID].n <= nTigs);
                    } else {
                        /* the btig couldn't be uniquely assigned to a path via common
                         * clones, OR we got a pathID we've already assigned a btig from
                         * this bubble to, so we pospone the assignment */
                        suspendedBtigs[nSuspended] = &btigs[g];
                        nSuspended++;
                    }
                }
                addCountTime(&Btigs2Time, now() - btigs2Start);
            }

            if (diploidMode == DUAL_PATH_MODE && nSuspended > 0) {
                if (nSuspended == 1) {
                    /*  one of the btigs wasn't mapped to a haplo-path, but the other
                     *  one was, so we assign the umapped btig to the still vacant path */
                    int pathID = (haploPaths[0].n > haploPaths[1].n ? 1 : 0);
                    DBG2("Assigned suspended btig to vacant path %d\n", pathID);
                    add_tig_to_hpath(&haploPaths[pathID], suspendedBtigs[0]);
                    assert(haploPaths[pathID].n <= nTigs);
                    //DBG2("[ Contig %ld, n: %d ]\n", haploPaths[pathID].tigs[lastIdx].contig, lastIdx);
                } else if (nSuspended == 2 && nBubbles > 1) {
                    /* Terminate this diplotig and tie up lose ends */
                    DBG2("Btigs couldn't be assigned to haplo-paths. Terminating haplo-paths.\n");

                    nBubbles--;
                    for (g = 0; g < 2; g++) {
                        cid = btigs[g].contig;
                        contigInfo[cid].statusField = 1;
                    }

                    assert(haploPaths[0].n <= nTigs);
                    int prev = haploPaths[0].n - 1;
                    assert(prev >= 0 && prev < nTigs);
                    if (minClipLen < 0) {
                        /* if we had to clip overlapping sequence from the upstream C-tig,
                         * add back the clipped sequence to the end of both haplopaths */
                        size_t addBackLen = strlen(tailSeq);
                        for (int p = 0; p < NUM_HPATHS; p++) {
                            assert(haploPaths[p].tigs[prev].seq);
                            strcat(haploPaths[p].tigs[prev].seq, tailSeq);
                            haploPaths[p].tigs[prev].length += addBackLen;
                        }
                    }
		    if (tigCntr < (nTigs - 2)) {
                        /* if this is not the end of the chain, print current diplotig sequences, then
                         * reset everything and start a new diplotig */

		      DBG2("This is not the end of the chain (%ld out of %ld)! Finalizing current haplo-paths into a diplotig pair\n", tigCntr, nTigs);
		      bless_haplopaths(haploPaths, contigSequences, maskMode, kmer_len);
		      store_haplopaths(haploPaths, curDiplId, &curMetaConId, nBubbles, nCons, haplotigsFDBuffer);
		      
		      DBG2("Resetting to start a new diplotig.\n");
                        UPC_ATOMIC_FADD_I64(&curDiplId, &diplotigID, 1);
                        (*myDiplotigs)++;

                        nCtigMers = 0;
                        meanCtigDepth = 0.0;
                        meanBtigDepth = 0.0;
                        nBubbles = 0;
                        nCons = 0;
                        for (int p = 0; p < NUM_HPATHS; p++) {
                            update_depth_histogram(local_histo, haploPaths[p].depth);
                            update_depth_histogram(local_histoDiplotigs, haploPaths[p].depth);
                            // empty the clones hash and reset the pointer
                            if (haploPaths[p].path_clones != NULL) {
                                delete_all_clones(haploPaths[p].path_clones);
                            }

                            // RE-INITIALIZE EXISTING HPATH
                            haploPaths[p].totLength = 0;
                            haploPaths[p].depth = 0;
                            haploPaths[p].finalSeq[0] = '\0';

                            for (int i = 0; i < haploPaths[p].n; i++) {
                                haploPaths[p].tigs[i].contig = 0;
                                haploPaths[p].tigs[i].length = 0;
                                haploPaths[p].tigs[i].depth = 0;
                                haploPaths[p].tigs[i].rcFlag = '\0';
                                haploPaths[p].tigs[i].type = '\0';
                                free_chk(haploPaths[p].tigs[i].seq);
                                haploPaths[p].tigs[i].seq = NULL;
                            }
                            haploPaths[p].n = 0;
                        }
                    } else { /* Last bubble in the chain - get out here, meaning this bubble AND the following C-tig won't be added to the hpath*/
                        DBG2("This is the last bubble in the chain. Ending the chain traversal here.\n");

                        for (g = 0; g < nBtigs; g++) {
                            free_chk(btigs[g].seq);
                            btigs[g].seq = NULL;
                            if (btigs[g].clones != NULL) {
                                free_chk(btigs[g].clones);
                            }
                        }
                        free_chk(btigs);
                        break; //  'for ( overall_ptr = 0; overall_ptr < nTigs; overall_ptr++ )'
                    }
                } else {       /* first bubble - assign based on depth */
                    double hpathStart = now();
                    DBG("First bubble - assigning based on depth..\n");
                    add_tig_to_hpath(&haploPaths[0], suspendedBtigs[maxDepthIndex]);
                    assert(haploPaths[0].n <= nTigs);
                    int minDepthIndex = abs(maxDepthIndex - 1);
                    add_tig_to_hpath(&haploPaths[1], suspendedBtigs[minDepthIndex]);
                    assert(haploPaths[1].n <= nTigs);
                    addCountTime(&hpathTime, now() - hpathStart);
                }
            } else if (diploidMode == MAX_DEPTH_MODE) {
                paths[maxDepthLength + length_to_copy] = '\0';
                strcat(finalSequence, &paths[maxDepthLength]);
            }

            for (g = 0; g < nBtigs; g++) {
                free_chk(btigs[g].seq);
                btigs[g].seq = NULL;
                if (diploidMode == DUAL_PATH_MODE && btigs[g].clones != NULL) {
                    free_chk(btigs[g].clones);
                }
            }
            free_chk(btigs);
            addCountTime(&Btime, now() - Bstart);
        } else if (type == 'C') {
            double Cstart = now();
            nCons++;
            DBG2("LINKER %ld...\n", nCons);
            contigInfo[id].statusField = 2;    //earmark as part of a future diplotig
            depth = contigInfo[id].meanDepth;
            assert(depth > 0);

            nMers = contigInfo[id].nMers;
            nCtigMers += nMers;
            meanCtigDepth += nMers * depth;

            cur_seq = contigSequences[id];
            cur_len = cur_seq.seqLength;
            tig_seq = (char *)malloc_chk((cur_len + 1) * sizeof(char));
            assert(cur_seq.data != NULL);

            upc_memget(tig_seq, cur_seq.data, (cur_len) * sizeof(char));
            tig_seq[cur_len] = '\0';

            if (strand == '-') {
                if (!reverseComplementINPLACEBubble(tig_seq, cur_len)) {
                    WARN("Unexpected base %lld\n", (lld)id);
                }
            }

            DBG2("C-tig: Contig%d,  %d bp, %s\n", (int)id, cur_len, tig_seq);
            if (diploidMode == DUAL_PATH_MODE) {
                enhanced_data_t Ctig;
                Ctig.contig = id;
                Ctig.depth = depth;
                Ctig.length = cur_len;
                Ctig.rcFlag = strand;
                Ctig.type = type;
                Ctig.seq = tig_seq;
                assert(Ctig.seq[cur_len] == '\0');
                Ctig.endClip = 0;
                Ctig.clones = NULL;
                for (int p = 0; p < NUM_HPATHS; p++) {
                    add_tig_to_hpath(&haploPaths[p], &Ctig);
                    assert(haploPaths[p].n <= nTigs);
                }
            } else { //MAX_DEPTH_MODE
                strcat(finalSequence, tig_seq);
            }
            free_chk(tig_seq);
            addCountTime(&Ctime, now() - Cstart);
        } else {
            DIE("Could not determine tig type (B|C): '%c' %d!\n", type, (int) type);
        }
        addCountTime(&nTigsTime, now() - nTigsStart);
    }

    if (num_more_than_two) {
        DBG("NOTICE: There were %lld bubbles with more than two bubble-tigs!  Only two-path bubbles are supported. Will use the first two and ignore the rest.\n", (lld) num_more_than_two);
    }

    free_chk(tailSeq);
    free_chk(addBack);
    free_chk(clipSeq);
    free_chk(paths);

    if (diploidMode == DUAL_PATH_MODE) {
        /* validate and store a PAIR of diplotigs  */


        if (nBubbles == 1) {
            /* test if the path meets the diploid criteria */

            bool validBubble = true;
            int64_t hairTigID;
            if (haploPaths[0].tigs[1].depth / haploPaths[1].tigs[1].depth < 0.25) {
                validBubble = false;
                hairTigID = haploPaths[0].tigs[1].contig;
            } else if (haploPaths[1].tigs[1].depth / haploPaths[0].tigs[1].depth < 0.25) {
                validBubble = false;
                hairTigID = haploPaths[1].tigs[1].contig;
            }

            if (!validBubble) {
                DBG2("Single-bubble haplopath pair (w btigs %lld [d:%.1f] & %lld [d:%.1f] )  likely not a true diploid variant! Undoing the haplopaths...\n", haploPaths[0].tigs[1].contig, haploPaths[0].tigs[1].depth, haploPaths[1].tigs[1].contig, haploPaths[1].tigs[1].depth);

                // reset flags appropriately and free

                for (g = 0; g < 2; g++) {
                    int t;
                    assert(haploPaths[g].n <= nTigs);
                    for (t = 0; t < haploPaths[g].n; t++) {
                        cid = haploPaths[g].tigs[t].contig;
                        if (cid == hairTigID) {
                            contigInfo[cid].statusField = 0;
                            DBG2("Contig%lld: Reset contig status to 0\n", cid);
                        } else {
                            contigInfo[cid].statusField = 1;
                            DBG2("Contig%lld: Reset contig status to 1\n", cid);
                        }
                    }
                    free_chk(haploPaths[g].tigs);
                    free_chk(haploPaths[g].finalSeq);
                }
                free_chk(haploPaths);
                free_chk(finalSequence);
                return num_more_than_two > 0 ? 1 : 0;
            }
        }


        bless_haplopaths(haploPaths, contigSequences, maskMode, kmer_len);
        store_haplopaths(haploPaths, curDiplId, &curMetaConId, nBubbles, nCons, haplotigsFDBuffer);

        for (int p = 0; p < NUM_HPATHS; p++) {
            update_depth_histogram(local_histo, haploPaths[p].depth);
            update_depth_histogram(local_histoDiplotigs, haploPaths[p].depth);

            if (haploPaths[p].path_clones != NULL) {
                delete_all_clones(haploPaths[p].path_clones);
                rb_tree_dealloc(haploPaths[p].path_clones, NULL);
                haploPaths[p].path_clones = NULL;
            }
#ifdef DEBUG
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &totDiplotigLength, haploPaths[p].totLength);

#endif
            assert(haploPaths[p].n <= nTigs);
            for (int i = 0; i < haploPaths[p].n; i++) {
                free_chk(haploPaths[p].tigs[i].seq);
                haploPaths[p].tigs[i].seq = NULL;
            }
            free_chk(haploPaths[p].tigs);
            free_chk(haploPaths[p].finalSeq);
        }
        free_chk(haploPaths);
    } else { //MAX_DEPTH_MODE
        /* print the single "mosaic" diplotig info  */
        meanCtigDepth /= nCtigMers;
        update_depth_histogram(local_histo, meanCtigDepth);
        update_depth_histogram(local_histoDiplotigs, meanCtigDepth);

#ifdef DEBUG
        UPC_ATOMIC_FADD_I64(&curMetaConId, &McontigID, 1);
#endif
        /* mean depth is calculated using C-tigs only */
        recordIdLengthDepth(curMetaConId, finalSequence, meanCtigDepth, IS_DIPLOTIG, haplotigsFDBuffer, 0);

        /* /\* Print all *tigs in 1-line FASTA format *\/ */
        /* GZIP_PRINTF(haplotigsFD_raw, ">Contig_%lld\n", (lld) curMetaConId); */
        /* GZIP_FWRITE(finalSequence, 1, strlen(finalSequence), haplotigsFD_raw); */
        /* GZIP_PRINTF(haplotigsFD_raw, "\n"); */

#ifdef DEBUG
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &totDiplotigLength, strlen(finalSequence));
#endif
    }
    free_chk(finalSequence);
    DBG("traverse_CBC_chain over %0.3fs -- nTigs=%lld %0.3fs Btime=%lld %0.3fs Ctime=%lld %0.3fs BtigsTime=%lld %0.3fs Btigs2Time=%lld %0.3fs cloneTime=%lld %0.3fs alignTime=%lld %0.3fs hpath=%lld %0.3fs\n",
         now() - start,
         (lld)nTigsTime.count, nTigsTime.time,
         (lld)Btime.count, Btime.time,
         (lld)Ctime.count, Ctime.time,
         (lld)BtigsTime.count, BtigsTime.time,
         (lld)Btigs2Time.count, Btigs2Time.time,
         (lld)cloneTime.count, cloneTime.time,
         (lld)alignTime.count, alignTime.time,
         (lld)hpathTime.count, hpathTime.time);

    return num_more_than_two > 0 ? 1 : 0;
}



void recordIdLengthDepth(int64_t contigId, char *finalSequence, int depth, int contigType, Buffer haplotigsFDBuffer, int64_t diplotigId)
{
    size_t pos = getLengthBuffer(haplotigsFDBuffer);
    IdLengthDepth ild, *ptr;

    ild.contigId = contigId;
    ild.depth = depth;
    ild.contigType = contigType;
    ild.recordLen = 0;
    ild.diplotigId = diplotigId;

    writeBuffer(haplotigsFDBuffer, &ild, sizeof(IdLengthDepth));

#ifdef DEBUG
    printfBuffer(haplotigsFDBuffer, ">Contig_%lld\n%s\n", (lld)contigId, finalSequence);
#else
    printfBuffer(haplotigsFDBuffer, ">\n%s\n", finalSequence);  //don't store contig id; new one will be assigned later
#endif

    size_t newPos = getLengthBuffer(haplotigsFDBuffer);
    ptr = (IdLengthDepth *)(getStartBuffer(haplotigsFDBuffer) + pos);
    ptr->recordLen = newPos - pos;
    assert(ptr->recordLen > strlen(finalSequence) + 10);
    DBG2("wrote haplotig %lld seqlen %lld recordLen %lld (%lld)\n", (lld)ptr->contigId, (lld)strlen(finalSequence), (lld)ptr->recordLen, (lld)contigId);
}



int bubbleFinder_main(int argc, char **argv)
{
    DBG("Starting bubbleFinder_main\n");
    upc_tick_t start_time = upc_ticks_now();

    kmer_len = MAX_KMER_SIZE - 1;
    int64_t myIsotigs = 0, myDiplotigs = 0;

    if (MYTHREAD == 0) {
        _bubbleID = 1;
        diplotigID = 1;
        indTravs = 0;
        _totConflicts = 0;
        totDiplotigLength = 0;
        McontigID = 0;
    }

    UPC_LOGGED_BARRIER;
    int64_t my_ind = 0;
    int64_t accumulator;
    option_t *thisOpt;
    option_t *optList = GetOptList(argc, argv, "a:c:d:N:M:o:B:b:s:H:D:k:K");
    print_args(optList, __func__);

    int kmerLength;
    int64_t my_nContigs;
    int64_t nContigs = -1;
    int64_t curMetaConId = 0;
    FILE *my_nC_fd, *nC_fd;
    char diploidMode;
    int maskMode = 0;

    const char *base_dir = ".";
    char *contigFileName, *newFileName, *depthFileName;
    char *merAlignerOutputNames = NULL;
    
    UPC_TICK_T start, end;
    UPC_TICK_T timerStart, timerEnd;
    if (MYTHREAD == 0) {
        _bubbleContigMinDepth = 0;
    }
    UPC_LOGGED_BARRIER;

    /*
     * only_short = 0 : Find all bubbles a la Meraculous
     * only_short = 1 : Find only bubbles <= 2k
     * only_short = 2 : Accept bubbles <= 2k AND longer bubbles with similar paths
     *
     */

    int only_short = 0;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'a':
	    merAlignerOutputNames = strdup_chk(thisOpt->argument); 
            break;
        case 'c':
            contigFileName = thisOpt->argument;
            break;
        case 'o':
            newFileName = thisOpt->argument;
            break;
        case 'd':
            depthFileName = thisOpt->argument;
            break;
        case 'k':
            kmer_len = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Please compile with a larger MAX_KMER_SIZE (%d) for kmer length %d", MAX_KMER_SIZE, kmer_len);
            }
            break;
        case 'N':
            nContigs = __atoi64(thisOpt->argument);
            break;
        case 'M':
            my_nContigs = __atoi64(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'b':
            bin_size = atoi(thisOpt->argument);
            break;
        case 's':
            only_short = atoi(thisOpt->argument);
            break;
        case 'H':
            histogram_size = atoi(thisOpt->argument);
            break;
        case 'D':
            if (MYTHREAD == 0) {
                _bubbleContigMinDepth = atoi(thisOpt->argument);
            }
            break;
        case 'K':
            maskMode = 1;
            break;
        default:
            break;
        }
    }

    if (merAlignerOutputNames != NULL) {
      diploidMode = DUAL_PATH_MODE;
    } else {
      diploidMode = MAX_DEPTH_MODE;
    }

    UPC_LOGGED_BARRIER;

    {
        char my_nContigsFile[MAX_FILE_PATH];
        snprintf(my_nContigsFile, MAX_FILE_PATH, "my%s.txt", contigFileName);
        my_nC_fd = openCheckpoint0(my_nContigsFile, "r");
        my_nContigs = 0;
        char buff[1024];
        while (fgets(buff, MAX_FILE_PATH, my_nC_fd)) {
            buff[strlen(buff) - 1] = '\0';
            my_nContigs += atol(buff);
        }
        closeCheckpoint0(my_nC_fd);
        LOGF("Found %lld contigs from %s\n", (lld) my_nContigs, my_nContigsFile);
    }

    if (nContigs == -1) {
        DIE("User did not specify the number of contigs with -N flag!\n");
    }

    UPC_LOGGED_BARRIER;

    char rstrand;

    int64_t *local_histo = (int64_t *)calloc_chk(histogram_size, sizeof(int64_t));
    int64_t *local_histoIsotigs = (int64_t *)calloc_chk(histogram_size, sizeof(int64_t));
    int64_t *local_histoDiplotigs = (int64_t *)calloc_chk(histogram_size, sizeof(int64_t));

    gzFile contigFD; // must be gzfile for kseq_init.. can be uncompressed
    {
        char myContigFileName[MAX_FILE_PATH];
        snprintf(myContigFileName, MAX_FILE_PATH, "%s.fasta" GZIP_EXT, contigFileName);
        contigFD = openCheckpoint1(myContigFileName, "r");
        if (!contigFD) {
            DIE("Could not open %s\n", myContigFileName);
        }
    }

    GZIP_FILE merDepthFD;
    {
        char myDepthFileName[MAX_FILE_PATH];
        snprintf(myDepthFileName, MAX_FILE_PATH, "merDepth_%s.txt" GZIP_EXT, depthFileName);
        merDepthFD = openCheckpoint(myDepthFileName, "r");
    }

    GZIP_FILE ceaFD;
    {
        char myCeaFileName[MAX_FILE_PATH];
        snprintf(myCeaFileName, MAX_FILE_PATH, "%s.cea" GZIP_EXT, contigFileName);
        ceaFD = openCheckpoint(myCeaFileName, "r");
    }



    /* GZIP_FILE haplotigsFD_raw = NULL; */
    /* { */
    /*     char myHaplotigsRawFileName[MAX_FILE_PATH]; */
    /*     snprintf(myHaplotigsRawFileName,MAX_FILE_PATH, "%s/unfiltered_%s_%d.fasta" GZIP_EXT, base_dir, newFileName, MYTHREAD); */
    /*     get_rank_path(myHaplotigsRawFileName, MYTHREAD); */
    /*     haplotigsFD_raw = GZIP_OPEN(myHaplotigsRawFileName, "w"); */

    /* } */

    /* GZIP_FILE mappingFD = NULL; */
    /* { */
    /* #ifdef PRINT_MAPPING */
    /*   char myMappingFileName[MAX_FILE_PATH]; */
    /*   snprintf(myMappingFileName,MAX_FILE_PATH, "%s/mapping_%s_%d.txt" GZIP_EXT, base_dir, newFileName, MYTHREAD); */
    /*   get_rank_path(myMappingFileName, MYTHREAD); */
    /*   mappingFD = GZIP_OPEN(myMappingFileName, "w"); */
    /* #endif */
    /* } */


    Buffer haplotigsFDBuffer = initBuffer(8192 * 1024);


    cea_t curCeaEntry;
    int hairLength = ((2 * kmer_len - 1) < HAIR_MIN) ? 2 * kmer_len - 1 : HAIR_MIN;
    char prevMer[MAX_KMER_SIZE];
    char nextMer[MAX_KMER_SIZE];
    char nullMer[MAX_KMER_SIZE];
    char statusField;
    int64_t contig;
    int contigLength;
    int j;

    for (j = 0; j < kmer_len; j++) {
        nullMer[j] = '0';
    }

    for (j = 0; j < histogram_size; j++) {
        local_histo[j] = 0;
        local_histoIsotigs[j] = 0;
        local_histoDiplotigs[j] = 0;
    }

    extensions_t extensions;

    /* First collectively allocate contigInfo, linkertips, bubbleMap and tipInfo arrays */
    shared[1] cea_t * contigInfo = NULL;
    UPC_ALL_ALLOC_CHK(contigInfo, nContigs, sizeof(cea_t));

    shared[1] extensions_t * linkertips = NULL;  //defines edges of linker nodes via kmer "tips"
    UPC_ALL_ALLOC_CHK(linkertips, nContigs, sizeof(extensions_t));

    upc_forall(int64_t j = 0; j < nContigs; j++; j) {
        linkertips[j].used_flag = UNUSED_EXT;
    }
    UPC_LOGGED_BARRIER;

    /* PRINT FASTA LOGISTICS */
    int64_t total_written = 0;
    int64_t towrite;
    char fastaSegment[SEGMENT_LENGTH];
    fastaSegment[SEGMENT_LENGTH - 1] = '\0';
    char *seqF;

    shared[1] int64_t * bubbleMap = NULL;
    UPC_ALL_ALLOC_CHK(bubbleMap, nContigs, sizeof(int64_t));

    memory_heap_t memory_heap;
    hash_table_t *tipInfo = create_hash_table(nContigs, &memory_heap, my_nContigs);

    char tips[2 * MAX_KMER_SIZE];

    /* Reading contig end data from .cea files */
    UPC_LOGGED_BARRIER;
    /* Store contig mean depth for later use */

    int curNmers;
    int64_t curContigId;
    float curDepth;

    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();

    Buffer depthBuffer = initBuffer(MAX_LINE_SIZE);
    resetBuffer(depthBuffer);

    while (gzgetsBuffer(depthBuffer, MAX_LINE_SIZE, merDepthFD) != NULL) {
        splitDEPTH(getStartBuffer(depthBuffer), &curContigId, &curNmers, &curDepth);
        CHECK_NCONTIG_BOUND(curContigId);
        contigInfo[curContigId].nMers = curNmers;
        contigInfo[curContigId].meanDepth = curDepth;
        resetBuffer(depthBuffer);
    }

    closeCheckpoint(merDepthFD);

    UPC_LOGGED_BARRIER;
    end = UPC_TICKS_NOW();
    double merdepthProcTime = UPC_TICKS_TO_SECS(end - start);

    /* Reading contig end data from .cea files */
    start = UPC_TICKS_NOW();

    Buffer ceaBuffer = depthBuffer; //reuse depth buffer
    depthBuffer = NULL;
    resetBuffer(ceaBuffer);
    while (gzgetsBuffer(ceaBuffer, MAX_LINE_SIZE, ceaFD) != NULL) {
        splitCEA(getStartBuffer(ceaBuffer), &curCeaEntry, kmer_len);
        resetBuffer1(ceaBuffer);
        prevMer[0] = curCeaEntry.prevBase;
        memcpy(&prevMer[1], curCeaEntry.firstMer, (kmer_len - 1) * sizeof(char));
        memcpy(nextMer, &(curCeaEntry.lastMer[1]), (kmer_len - 1) * sizeof(char));
        nextMer[kmer_len - 1] = curCeaEntry.nextBase;
        contigLength = curCeaEntry.contigLen;

        if (only_short == 0) {
            if (contigLength <= hairLength) {
                if (!((((curCeaEntry.prevCodeL == 'F') && isACGT(curCeaEntry.prevCodeR)) && ((curCeaEntry.nextCodeR == 'F') && isACGT(curCeaEntry.nextCodeL))) ||
                      (((curCeaEntry.prevCodeR == 'F') && isACGT(curCeaEntry.prevCodeL)) &&
                       ((curCeaEntry.nextCodeL == 'F') && isACGT(curCeaEntry.nextCodeR))))) {
                    contigInfo[curCeaEntry.contigID].statusField = 0;
                    continue;
                }
            }
            statusField = (contigLength > hairLength) ? 1 : 0;
        } else {
            statusField = 1;
        }
        contig = curCeaEntry.contigID;
        curCeaEntry.statusField = statusField;
        CHECK_NCONTIG_BOUND(contig);
        curCeaEntry.nMers = contigInfo[contig].nMers;
        curCeaEntry.meanDepth = contigInfo[contig].meanDepth;
        contigInfo[contig] = curCeaEntry;

        if (((curCeaEntry.prevCodeL == 'F') && isACGT(curCeaEntry.prevCodeR)) || ((curCeaEntry.nextCodeR == 'F') && isACGT(curCeaEntry.nextCodeL))) {
            if (((curCeaEntry.prevCodeL == 'F') && isACGT(curCeaEntry.prevCodeR))) {
                memcpy(&(extensions.data[0]), prevMer, kmer_len * sizeof(char));
            } else {
                memcpy(&(extensions.data[0]), nullMer, kmer_len * sizeof(char));
            }

            if (((curCeaEntry.nextCodeR == 'F') && isACGT(curCeaEntry.nextCodeL))) {
                memcpy(&(extensions.data[kmer_len]), nextMer, kmer_len * sizeof(char));
            } else {
                memcpy(&(extensions.data[kmer_len]), nullMer, kmer_len * sizeof(char));
            }

            extensions.used_flag = USED_EXT;
            CHECK_NCONTIG_BOUND(contig);
            linkertips[contig] = extensions;
        } else if (((curCeaEntry.prevCodeR == 'F') && isACGT(curCeaEntry.prevCodeL)) && ((curCeaEntry.nextCodeL == 'F') && isACGT(curCeaEntry.nextCodeR))) {
            memcpy(tips, prevMer, kmer_len * sizeof(char));
            memcpy(&(tips[kmer_len]), nextMer, kmer_len * sizeof(char));

            if (!add_tip(tipInfo, tips, contig, &memory_heap, kmer_len)) {
                WARN("Out of bounds on memory heap, max %lld, nContigs %lld\n", (lld)memory_heap.max_pos, (lld)nContigs);
            }

            bubbleMap[contig] = 0;
        }
    }

    closeCheckpoint(ceaFD);
    assert(ceaBuffer != NULL);
    freeBuffer(ceaBuffer);

    UPC_LOGGED_BARRIER;
    end = UPC_TICKS_NOW();
    double ceaProcTime = UPC_TICKS_TO_SECS(end - start);


    /* Store contig sequence for later use */
    shared[1] sequence_t * contigSequences = NULL;

    int64_t contigID;
    int contigLen;
    shared[] char *curSeq = NULL;

    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();

    UPC_ALL_ALLOC_CHK(contigSequences, nContigs, sizeof(sequence_t));

    Buffer myUPCAllocs = initBuffer(8192);
    kseq_t *ks = kseq_init(contigFD);
    while (kseq_read(ks) >= 0) {
        contigID = atol(ks->name.s + 7);

        contigLen = ks->seq.l;
        assert(contigLen > 0);
        DBG2("Loading %lld: name=%s len=%lld to thread %d\n", (lld)contigID, ks->name.s, (lld)contigLen, (int)upc_threadof(&(contigSequences[contigID])));
        CHECK_NCONTIG_BOUND(contigID);
        UPC_ALLOC_TRACK(curSeq, contigLen * sizeof(char), myUPCAllocs);
        memcpy((char *)curSeq, ks->seq.s, contigLen * sizeof(char));
        sequence_t tmp;
        tmp.data = curSeq;
        tmp.seqLength = contigLen;
        curSeq = NULL; // no chance of re-use

        // This is probably not on MYTHREAD
        upc_memput(&(contigSequences[contigID]), &tmp, sizeof(tmp));
        assert(contigSequences[contigID].seqLength == contigLen);
    }

    kseq_destroy(ks);
    closeCheckpoint1(contigFD);

    upc_fence;
    UPC_LOGGED_BARRIER;
    end = UPC_TICKS_NOW();
    double contigProcTime = UPC_TICKS_TO_SECS(end - start);

#if DEBUG
    /* verify all my contigs are non-zero */
    for (contigID = MYTHREAD; contigID < nContigs; contigID += THREADS) {
        if (contigSequences[contigID].seqLength == 0 || contigSequences[contigID].data == NULL) {
            WARN("Failed to see Contig_%lld! len=%lld\n", (lld)contigID, (lld)contigSequences[contigID].seqLength);
        }
    }
#endif

    /* Find bubbles from convergent tips */
    shared[] list_t * cur_tip_info;
    shared[] data_t * cur_data;
    data_t cur_data_struct;
    list_t local_tip_info;
    int64_t curBubbleId;

    shared[1] bubbletip_t * bubbletips = NULL;  //defines edges of bubble-nodes via kmer "tips"
    UPC_ALL_ALLOC_CHK(bubbletips, (nContigs + 1), sizeof(bubbletip_t));
    int64_t i;
    int cur_data_count;
    char rcFlag;

    // initialize all bytes of local data structures
    memset(&cur_data_struct, 0, sizeof(data_t));
    memset(&local_tip_info, 0, sizeof(list_t));

    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();

    assert(kmer_len < MAX_KMER_SIZE);
    char First[MAX_KMER_SIZE];
    char Last[MAX_KMER_SIZE];
    memset(First, 0, MAX_KMER_SIZE);
    memset(Last, 0, MAX_KMER_SIZE);

    First[kmer_len] = '\0';
    Last[kmer_len] = '\0';

    int lengthCheck, min_length, max_length;
    char *firstSeq, *secondSeq;
    int first_length, second_length;

    /* This table is used to transform nucleotide letters into numbers. */
    int8_t nt_table[128] = {
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 3, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 3, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
    };

    // initialize scoring matrix for genome sequences
    //  A  C  G  T	N (or other ambiguous code)
    //  2 -2 -2 -2  0	A
    // -2  2 -2 -2  0	C
    // -2 -2  2 -2  0	G
    // -2 -2 -2  2  0	T
    //	0  0  0  0  0	N (or other ambiguous code)

    int8_t *mat = (int8_t *)calloc_chk(25, sizeof(int8_t));
    int match = 1, mismatch = 0, gap_open = 3, gap_extension = 3, ambiguity_score = 2;

    int ll, kk, mm;
    for (ll = kk = 0; ll < 4; ++ll) {
        for (mm = 0; mm < 4; ++mm) {
            mat[kk++] = ll == mm ? match : -mismatch; /* weight_match : -weight_mismatch */
        }
        mat[kk++] = -ambiguity_score;                 // ambiguous base: no penalty
    }
    for (mm = 0; mm < 5; ++mm) {
        mat[kk++] = -ambiguity_score;
    }

    s_align *r = NULL;
    s_profile *profile = NULL;
    uint16_t best_score;
    double similarity = 0.0, similarity2 = 0.0, max_similarity;
    int8_t *num, *ref_num;
    int64_t contigIdx;

    for (contigIdx = MYTHREAD; contigIdx < nContigs; contigIdx += THREADS) {
        cur_tip_info = tipInfo->table[contigIdx].head;

        while (cur_tip_info != NULL) {
            local_tip_info = *cur_tip_info;

            if (local_tip_info.nContigs > 1) {
                cur_data = local_tip_info.data;

                if (only_short > 0) {
                    max_length = 0;
                    first_length = 0;
                    second_length = 0;
                    firstSeq = NULL;
                    secondSeq = NULL;

                    while (cur_data != NULL) {
                        cur_data_struct = *cur_data;
                        contig = cur_data_struct.contig;
                        lengthCheck = contigSequences[contig].seqLength;
                        if (lengthCheck == 0) {
                            WARN("Failed to find contig %lld!\n", (lld)contig);
                        }
                        assert(lengthCheck > 0);
                        assert(contigSequences[contig].data != NULL);

                        if (only_short == 2) {
                            if (max_length == 0) { // The first path in the bubble
                                firstSeq = (char *)malloc_chk(lengthCheck * sizeof(char));
                                first_length = lengthCheck;
                                upc_memget(firstSeq, contigSequences[contig].data, lengthCheck * sizeof(char));
                            }
                        }

                        if (lengthCheck > max_length) {
                            max_length = lengthCheck;
                        }

                        cur_data = cur_data_struct.next;

                        if (only_short == 2) {
                            if (cur_data == NULL) { // We just processed the last "path" in the bubble
                                secondSeq = (char *)malloc_chk(lengthCheck * sizeof(char));
                                second_length = lengthCheck;
                                upc_memget(secondSeq, contigSequences[contig].data, lengthCheck * sizeof(char));
                            }
                        }
                    }

                    if (only_short == 2) {
                        if ((first_length > 0) && (second_length > 0)) {
                            similarity = 0.0;
                            similarity2 = 0.0;

                            num = (int8_t *)malloc_chk(first_length * sizeof(int8_t));
                            ref_num = (int8_t *)malloc_chk(second_length * sizeof(int8_t));

                            for (mm = 0; mm < first_length; ++mm) {
                                num[mm] = (int8_t)nt_table[(int)firstSeq[mm]];
                            }
                            profile = ssw_init(num, first_length, mat, 5, 2);

                            for (mm = 0; mm < second_length; ++mm) {
                                ref_num[mm] = (int8_t)nt_table[(int)secondSeq[mm]];
                            }
                            int min_accpt = (first_length / 2 > 15) ? first_length / 2 : 15;

                            r = ssw_align(profile, ref_num, second_length, gap_open, gap_extension, 8, 0, 0, min_accpt);
                            best_score = r->score1;
                            similarity = (first_length > second_length) ? (1.0 * best_score) / (1.0 * first_length) : (1.0 * best_score) / (1.0 * second_length);
                            align_destroy(r);
                            r = NULL;

                            // Check similarity with reverse complement!
                            reverseComplementINPLACE(secondSeq, second_length);
                            for (mm = 0; mm < second_length; ++mm) {
                                ref_num[mm] = (int8_t)nt_table[(int)secondSeq[mm]];
                            }
                            min_accpt = (first_length / 2 > 15) ? first_length / 2 : 15;
                            r = ssw_align(profile, ref_num, second_length, gap_open, gap_extension, 8, 0, 0, min_accpt);
                            best_score = r->score1;
                            similarity2 = (first_length > second_length) ? (1.0 * best_score) / (1.0 * first_length) : (1.0 * best_score) / (1.0 * second_length);

                            if (similarity2 > similarity) {
                                max_similarity = similarity2;
                            } else {
                                max_similarity = similarity;
                            }

                            init_destroy(profile);
                            align_destroy(r);
                            r = NULL;
                            profile = NULL;
                            free_chk(ref_num);
                            free_chk(num);
                            free_chk(firstSeq);
                            firstSeq = NULL;
                            free_chk(secondSeq);
                            secondSeq = NULL;
                        }

                        if (firstSeq != NULL) {
                            free_chk(firstSeq);
                            firstSeq = NULL;
                        }

                        if (secondSeq != NULL) {
                            free_chk(secondSeq);
                            secondSeq = NULL;
                        }
                    }
                }

                /* Accept bubbles based on only_short flag */
                if ((only_short == 0) || ((only_short == 1) && (max_length <= 2 * kmer_len)) || ((only_short == 2) && ((max_length <= 2 * kmer_len) || (max_similarity > 0.8)))) {
                    /* A bubble */
                    UPC_ATOMIC_FADD_I64(&curBubbleId, &_bubbleID, 1);
                    CHECK_NCONTIG_BOUND(curBubbleId);
                    upc_memput(bubbletips[curBubbleId].tipKey, local_tip_info.tipKey, 2 * kmer_len * sizeof(char));

                    cur_data = local_tip_info.data;
                    cur_data_count = 0;

#ifdef DEBUG
                    memcpy(First, local_tip_info.tipKey, kmer_len * sizeof(char));
                    memcpy(Last, (local_tip_info.tipKey) + kmer_len, kmer_len * sizeof(char));
                    DBG2("BUBBLE %lld %s.%s [", (lld)curBubbleId, First, Last);
#endif

                    while (cur_data != NULL) {
                        cur_data_count++;
                        cur_data_struct = *cur_data;
                        contig = cur_data_struct.contig;
                        rcFlag = cur_data_struct.rcFlag;
                        DBG2("%cContig%lld (%f) ", rcFlag, (lld)contig, contigInfo[contig].meanDepth);
                        CHECK_NCONTIG_BOUND(contig);
                        bubbleMap[contig] = curBubbleId;
                        cur_data = cur_data_struct.next;
                    }

                    DBG2("]\n");

                    if (cur_data_count != local_tip_info.nContigs) {
                        SDIE("FATAL ERROR: Didn't find %lld nContigs\n", (lld)local_tip_info.nContigs);
                    }
                }
            }
            cur_tip_info = local_tip_info.next;
        }
    }

    free_chk(mat);

    UPC_LOGGED_BARRIER;
    end = UPC_TICKS_NOW();
    double bubbleMapTime = UPC_TICKS_TO_SECS(end - start);

    // it seems to me that for this hash table, we need to divide up the contigs uniformly among
    // the threads
    my_nContigs = INT_CEIL(nContigs, THREADS);

    /* Build the bubble-contig graph (pointsTo contains the edge info) */
    pmemory_heap_t memory_heap2;
    phash_table_t *pointsTo = create_phash_table(4 * nContigs + 256 * ((int64_t)THREADS), &memory_heap2, 2 * (my_nContigs + 8));
    assert(kmer_len < MAX_KMER_SIZE);
    char links[2 * MAX_KMER_SIZE];
    char rcLinks[2 * MAX_KMER_SIZE];
    char in[MAX_KMER_SIZE];
    char rcin[MAX_KMER_SIZE];
    char type;
    int64_t loopstart;
    loopstart = (MYTHREAD == 0) ? THREADS : MYTHREAD;

    // initialize local structs
    memset(links, 0, 2 * MAX_KMER_SIZE);
    memset(rcLinks, 0, 2 * MAX_KMER_SIZE);
    memset(in, 0, MAX_KMER_SIZE);
    memset(rcin, 0, MAX_KMER_SIZE);

    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();
    int64_t myEdges = 0;

    int64_t bubbleID = broadcast_long(_bubbleID, 0);
    for (i = loopstart; i < bubbleID; i += THREADS) {
        CHECK_NCONTIG_BOUND(i);
        upc_memget(links, bubbletips[i].tipKey, 2 * kmer_len * sizeof(char));
        if (!reverseComplementSeqBubble(links, rcLinks, 2 * kmer_len)) {
            WARN("Unexpected base in reverseComplementSeqBubble for links index %lld\n", (lld)i);
        }
        memcpy(in, links, kmer_len * sizeof(char));
        memcpy(rcin, rcLinks, kmer_len * sizeof(char));
        type = B_PLUS;
        if (!add_edge(pointsTo, in, i, type, &memory_heap2, kmer_len)) {
            WARN("Out of bounds on memory heap, max %lld, bubbleID %lld\n", (lld)memory_heap2.max_pos, (lld)bubbleID);
        }

        type = B_MINUS;
        if (!add_edge(pointsTo, rcin, i, type, &memory_heap2, kmer_len)) {
            WARN("Out of bounds on memory heap, max %lld, bubbleID %lld\n", (lld)memory_heap2.max_pos, (lld)bubbleID);
        }

        myEdges += 2;
    }

    DBG("Added edges from bubbletips: %lld\n", (lld)myEdges);

    for (i = MYTHREAD; i < nContigs; i += THREADS) {
        if (linkertips[i].used_flag != USED_EXT) {
            continue;
        }

        upc_memget(links, linkertips[i].data, 2 * kmer_len * sizeof(char));
        if (!reverseComplementSeqBubble(links, rcLinks, 2 * kmer_len)) {
            WARN("Unexpected base in reverseComplementSeqBubble for links index %lld\n", (lld)i);
        }
        memcpy(in, links, kmer_len * sizeof(char));
        memcpy(rcin, rcLinks, kmer_len * sizeof(char));
        if (in[0] != '0') {
            type = C_PLUS;
            if (!add_edge(pointsTo, in, i, type, &memory_heap2, kmer_len)) {
                WARN("Out of bounds on memory heap, max %lld, nContigs %lld\n", (lld)memory_heap2.max_pos, (lld)nContigs);
                myEdges += 1;
            }
        }

        if (rcin[0] != '0') {
            type = C_MINUS;
            if (!add_edge(pointsTo, rcin, i, type, &memory_heap2, kmer_len)) {
                WARN("Out of bounds on memory heap, max %lld, nContigs %lld\n", (lld)memory_heap2.max_pos, (lld)nContigs);
                myEdges += 1;
            }
        }
    }


    UPC_LOGGED_BARRIER;
#ifdef DEBUG
    int64_t _edges = 0;
    long gotEdges = reduce_prefix_long(myEdges, UPC_ADD, &_edges) - myEdges;
    DBG("total myEdges including linkertips: %lld (got offset %lld)\n", (lld)myEdges, (lld)gotEdges);
    SDBG("Total edges: %lld\n", (lld)_edges);
#endif

    end = UPC_TICKS_NOW();
    double buildGraphTime = UPC_TICKS_TO_SECS(end - start);

#ifdef DEBUG_DISABLED
    /*Print the pointsTo data structure for debugging purposes */
    int64_t ent;
    int64_t oid;
    char ty;
    char ch;
    char sign;
    int y;
    char dmpPath[MAX_FILE_PATH];
    snprintf(dmpPath, MAX_FILE_PATH, "bubbleFinder.dmp");
    FILE *dmp = fopen_rank_path(dmpPath, "w", MYTHREAD);

    shared[] plist_t * result;
    shared[] pdata_t * iter;

    assert(kmer_len < MAX_KMER_SIZE);
    char keyb[MAX_KMER_SIZE];
    memset(keyb, 0, MAX_KMER_SIZE);
    keyb[kmer_len] = '\0';
    for (i = 0; i < 2 * nContigs; i++) {
        result = pointsTo->table[i].head;
        while (result != NULL) {
            ent = result->nContigs;
            assert(result->tipKey != NULL);
            upc_memget(keyb, result->tipKey, kmer_len * sizeof(char));
            assert(result->data != NULL);
            iter = result->data;
            oid = iter->object;
            ty = iter->type;
            if (ty == C_PLUS) {
                ch = 'C'; sign = '+';
            }
            ;
            if (ty == C_MINUS) {
                ch = 'C'; sign = '-';
            }
            ;
            if (ty == B_MINUS) {
                ch = 'B'; sign = '-';
            }
            ;
            if (ty == B_PLUS) {
                ch = 'B'; sign = '+';
            }
            ;

            fprintf(dmp, "%s\t%c%c%lld", keyb, ch, sign, (lld) oid);
            iter = iter->next;
            while (iter != NULL) {
                oid = iter->object;
                ty = iter->type;
                fprintf(dmp, ",%c%c%lld", ch, sign, (lld) oid);
                iter = iter->next;
            }
            fprintf(dmp, "\n");
            result = result->next;
        }
    }
    fclose_track(dmp);
#endif

    double storeClonesTime;
    if (diploidMode == DUAL_PATH_MODE) {

        UPC_LOGGED_BARRIER;
        start = UPC_TICKS_NOW();

        /*** First pass:  count nr of clones aligned to each contig and allocate shared heaps accordingly*/
        char tmpBtigAlignmentsFile[MAX_FILE_PATH];
        sprintf(tmpBtigAlignmentsFile, "BtigAlignments");
        FILE *btigAlignsFD = openCheckpoint0(tmpBtigAlignmentsFile, "w");


        shared[1] int64_t * cloneCntsGlobal = NULL;
        UPC_ALL_ALLOC_CHK(cloneCntsGlobal, nContigs, sizeof(int64_t));
        for (int64_t i = MYTHREAD; i < nContigs; i += THREADS) {
            cloneCntsGlobal[i] = 0;
        }
        UPC_LOGGED_BARRIER;

        Buffer alignBuffer = initBuffer(MAX_LINE_SIZE);
        char alnTempHolder[MAX_LINE_SIZE];
        int parseRes;
        int64_t subjectId;

	LOGF("Processing alignment files..\n");


	char *next_name = NULL;
	char *cur_name = merAlignerOutputNames;
	while (1) {
	    GZIP_FILE alignsFD;
	    char myAlignFileName[MAX_FILE_PATH];

	    char *next_name = strchr(cur_name, ',');
	    if (next_name) {
	      next_name[0] = 0;
	    }

	    for (int readNumber = 1; readNumber <=2; readNumber++) {
	        sprintf(myAlignFileName, "%s_Read%d" GZIP_EXT, cur_name, readNumber);
		int exists = doesCheckpointExist(myAlignFileName);
		if (readNumber == 2 && !exists) {
		  LOGF("skippint read2: readNumber=%d exists=%d checkpointName=%s\n", readNumber, exists, myAlignFileName);
		  break;                                                                                    // do not process Read2 for unpaired files
		}

		alignsFD = openCheckpoint(myAlignFileName, "r"); 
		if (!alignsFD) {
		  DIE("Could not open %s\n", myAlignFileName);
		}

		LOGF("Parsing alignments from %s ...\n", myAlignFileName);

		while (gzgetsBuffer(alignBuffer, MAX_LINE_SIZE, alignsFD) != NULL) {
		  assert(getLengthBuffer(alignBuffer) <= sizeof(alnTempHolder));
		  strcpy(alnTempHolder, getStartBuffer(alignBuffer)); //parseAlignment() uses strtok which truncates the original string, so we need to copy it prior to passing
		  parseRes = parseAlignment(alnTempHolder, NULL, &subjectId);

		  if (!parseRes) {
		    DIE("Could not parse alignment entry %s\n", getStartBuffer(alignBuffer));
		  }
		  if (parseRes == -1) { // MERALIGNER-F
		    resetBuffer(alignBuffer);
		    continue;
		  }
		  
		  if (bubbleMap[subjectId]) { //we only store alignments to btgis
		    fprintf(btigAlignsFD, "%s", getStartBuffer(alignBuffer));
		    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(cloneCntsGlobal[subjectId]), 1);
		  }
		  resetBuffer(alignBuffer);
		}
		
		closeCheckpoint(alignsFD);

	    }
	    if (!next_name) {
	      break;
	    }
	    cur_name = next_name + 1;
	}
        closeCheckpoint0(btigAlignsFD);

        UPC_LOGGED_BARRIER;

        /* Allocate shared alignment heaps for each contig*/
        create_alignment_heaps(nContigs, &alignment_heap, cloneCntsGlobal);

        DBG("Done creating shared alignment heaps for %ld contigs\n", nContigs);
        UPC_LOGGED_BARRIER;

        LOGF("Done creating shared alignment heaps\n");
        FLUSH_MY_LOG;



        /*** Second pass: populate the shared heaps with clone lists */

        alignment_t new_entry;
        new_entry.readName[0] = '\0';
        int64_t store_pos;


        btigAlignsFD = openCheckpoint0(tmpBtigAlignmentsFile, "r"); // now only scan through the btig alignment we put aside in pass 1

        while (fgetsBuffer(alignBuffer, MAX_LINE_SIZE, btigAlignsFD) != NULL) {
            parseRes = parseAlignment(getStartBuffer(alignBuffer), new_entry.readName, &new_entry.subjectId);
            resetBuffer1(alignBuffer);
            if (parseRes == -1) { // MERALIGNER-F
                continue;
            }

            int64_t cid = new_entry.subjectId;
            // at this point the alignments read in must be to b-tigs
            assert(bubbleMap[cid]);
            assert(new_entry.readName[0] != '\0');
            UPC_ATOMIC_FADD_I64(&store_pos, &(alignment_heap.heap_indices[cid]), 1);
            //       DBG("storing read %s to shared heap for contig %ld, pos %ld",new_entry.readName, cid, store_pos);
            assert(store_pos < cloneCntsGlobal[cid]);
            upc_memput((shared[] alignment_t *)(alignment_heap.alignment_ptr[cid] + store_pos), &new_entry, sizeof(alignment_t));
        }

        closeCheckpoint0(btigAlignsFD);
        freeBuffer(alignBuffer);
        UPC_ALL_FREE_CHK(cloneCntsGlobal);

        UPC_LOGGED_BARRIER;

        LOGF("Done storing clones->btigs\n");
        FLUSH_MY_LOG;

        end = UPC_TICKS_NOW();
        storeClonesTime = UPC_TICKS_TO_SECS(end - start);
    }

    UPC_ALL_FREE_CHK(bubbleMap);



    /*** Traverse the bubble-contig graph, starting from each contig */


    /* Allocate array for TODO items when aborting contigs */

    start = UPC_TICKS_NOW();

    shared[1] int64_t * todo = NULL;
    UPC_ALL_ALLOC_CHK(todo, nContigs, sizeof(int64_t));


    /* Allocated shared visited arrays */
    shared[1] int64_t * visitedC = NULL;
    UPC_ALL_ALLOC_CHK(visitedC, nContigs, sizeof(int64_t));

    for (i = MYTHREAD; i < nContigs; i += THREADS) {
        visitedC[i] = UNVISITED;
    }

    shared[1] int64_t * visitedB = NULL;
    UPC_ALL_ALLOC_CHK(visitedB, bubbleID, sizeof(int64_t));

    for (i = MYTHREAD; i < bubbleID; i += THREADS) {
        visitedB[i] = UNVISITED;
    }

    UPC_LOGGED_BARRIER;

    int64_t flag;

    // use BufferVector macros to avoid potentially huge mallocs for every thread!
    BV_INIT(int64_t, local_log_listB);
    BV_INIT(int64_t, local_log_listC);
    //int64_t *local_log_listC = (int64_t *)malloc_chk(nContigs * sizeof(int64_t));
    //int64_t *local_log_listB = (int64_t *)malloc_chk(bubbleID * sizeof(int64_t));
    int64_t logC_ptr = 0;
    int64_t logB_ptr = 0;

    BV_INIT(stream_t, upstream);
    BV_INIT(stream_t, downstream);
    //stream_t *upstream = (stream_t *)malloc_chk((nContigs + bubbleID) * sizeof(stream_t));
    //stream_t *downstream = (stream_t *)malloc_chk((nContigs + bubbleID) * sizeof(stream_t));
    int64_t upstr_pos = 0;
    int64_t downstr_pos = 0;
    stream_t next;
    stream_t current;
    stream_t first, last;
    int getNextRes;
    int abort = 0;
    int64_t k, t;
    int64_t conflIndex;
    int64_t nTigs;

    /* If found visited status in an entry, then go and unmark the current "visited" entries, will handle those cases later, serially. This parallel section finds cases that are traversals ending to both endpoints with "multiple connections" and finds only unvisited vertices */
    int64_t conflicts = 0;
    int64_t count_num_more_than_two = 0;

    for (i = MYTHREAD; i < nContigs; i += THREADS) {
      /*For every contig id, start traversing the bubble graph building streams  of (C-B-C)n.  Upon encountering a visited node, roll back.  
	C-nodes have objectIds correponding to original contig ids, but B-nodes' objectIds are independent of the original contig ids. 
	A B-tig node represents TWO contigs that make up a bubble  */
      
        if (linkertips[i].used_flag != USED_EXT) {
            continue;
        }

        // CSWAP the first one (local to MYTHREAD) to avoid races, FADD others that might be backed out in a conflict
        UPC_ATOMIC_CSWAP_I64(&accumulator, &(visitedC[i]), UNVISITED, 1);
        flag = (accumulator == UNVISITED) ? UNVISITED : VISITED;

        if (flag == VISITED) {
            continue; // In this case we dont have to flush any visited entries from log
        }
        abort = 0;
        logC_ptr = 0;
        logB_ptr = 0;
        BV_GET(local_log_listC,logC_ptr) = i;
        logC_ptr++;

        /* Initialize downstream */
        downstr_pos = 0;

        current.objectId = i;
        current.type = 'C';
        current.strand = '+';
        getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);

        while (getNextRes == 1) {
            if (next.type == 'C') {
                assert(current.type == 'B');
                assert(linkertips[next.objectId].used_flag == USED_EXT);
                UPC_ATOMIC_FADD_I64(&accumulator, &(visitedC[next.objectId]), 1);
                flag = (accumulator == UNVISITED) ? UNVISITED : VISITED;

                BV_GET(local_log_listC,logC_ptr) = next.objectId;
                logC_ptr++;
            } else if (next.type == 'B') {
                assert(current.type == 'C');
                UPC_ATOMIC_FADD_I64(&accumulator, &(visitedB[next.objectId]), 1);
                flag = (accumulator == UNVISITED) ? UNVISITED : VISITED;

                BV_GET(local_log_listB, logB_ptr) = next.objectId;
                logB_ptr++;
            } else {
                DIE("Invalid type on node %lld: '%c' %d\n", (lld) next.objectId, next.type, next.type);
            }

            if (flag == VISITED) {

                /* We found a visited vertex... well... we have to cleanup what we visited so far */
                /* Cleanup the visitedC[] array based on the log */
                for (k = 0; k < logC_ptr; k++) {
                    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(visitedC[BV_GET(local_log_listC,k)]), -1);
                }
                logC_ptr = 0;

                /* Cleanup the visitedB[] array based on the log */
                for (k = 0; k < logB_ptr; k++) {
                    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(visitedB[BV_GET(local_log_listB,k)]), -1);
                }
                logB_ptr = 0;

                abort = 1;
                break;
            }

            /* Flag is UNVISITED, therefore we have claimed this vertex -- already added visited()=1 */
            /* Add to downstream */
            BV_GET(downstream, downstr_pos) = next;
            downstr_pos++;
            current = next;
            CHECK_NCONTIG_BOUND(current.objectId);
            getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);
        }

        /* We should abort this traversal... We have already cleaned up */
        if (abort == 1) {
            /* All the traversal from this seed have been cleaned up! */
            UPC_ATOMIC_FADD_I64(&conflIndex, &_totConflicts, 1);
            todo[conflIndex] = i;
            conflicts++;
            continue;
        }

        current.objectId = i;
        current.type = 'C';
        current.strand = '-';
        getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);

        /* Initialize upstream */
        upstr_pos = 0;

        while (getNextRes == 1) {
            if (next.type == 'C') {
                assert(current.type == 'B');
                assert(linkertips[next.objectId].used_flag == USED_EXT);
                UPC_ATOMIC_FADD_I64(&accumulator, &(visitedC[next.objectId]), 1);
                flag = (accumulator == UNVISITED) ? UNVISITED : VISITED;
            
                BV_GET(local_log_listC,logC_ptr) = next.objectId;
                logC_ptr++;
            } else if (next.type == 'B') {
                assert(current.type == 'C');
                UPC_ATOMIC_FADD_I64(&accumulator, &(visitedB[next.objectId]), 1);
                flag = (accumulator == UNVISITED) ? UNVISITED : VISITED;
            
                BV_GET(local_log_listB,logB_ptr) = next.objectId;
                logB_ptr++;
            } else {
                DIE("Invalid type on node %lld: '%c' %d\n", (lld) next.objectId, next.type, next.type);
            }

            if (flag == VISITED) {
                /* We found a visited vertex... well... we have to cleanup what we visited so far */
                /* Cleanup the visitedC[] array based on the log */
                for (k = 0; k < logC_ptr; k++) {
                    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(visitedC[BV_GET(local_log_listC,k)]), -1);
                }
                logC_ptr = 0;

                /* Cleanup the visitedB[] array based on the log */
                for (k = 0; k < logB_ptr; k++) {
                    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(visitedB[BV_GET(local_log_listB,k)]), -1);
                }
                logB_ptr = 0;

                abort = 1;
                break;
            }

            current = next;
            rstrand = '-';
            if (next.strand == '-') {
                rstrand = '+';
            }
            next.strand = rstrand;
            BV_GET(upstream, upstr_pos) = next;
            upstr_pos++;
            CHECK_NCONTIG_BOUND(current.objectId);
            getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);
        }

        /* We should abort this traversal... We have already cleaned up */
        if (abort == 1) {
            /* All the traversal from this seed have been cleaned up! */
            UPC_ATOMIC_FADD_I64(&conflIndex, &_totConflicts, 1);
            todo[conflIndex] = i;

            conflicts++;
            continue;
        }

        /* Remove leading and trailing bubbles */
        /* downstream has push semantics so head is at position 0 */
        /* upstream has unshift semantic sso head is at position (upstr_pos - 1) */

        if (upstr_pos > 0) {
            first = BV_GET(upstream, upstr_pos - 1);
            if (first.type == 'B') {
                upstr_pos--;
            }
        }

        if (downstr_pos > 0) {
            last = BV_GET(downstream, downstr_pos - 1);
            if (last.type == 'B') {
                downstr_pos--;
            }
        }

        nTigs = 1 + upstr_pos + downstr_pos;
        if (nTigs > 1) {
            count_num_more_than_two += traverse_CBC_chain(diploidMode, maskMode,
                               BV_VAR(upstream), i, BV_VAR(downstream), upstr_pos,
                               nTigs, nContigs, &myDiplotigs, contigInfo, bubbletips, contigSequences, tipInfo,
                               haplotigsFDBuffer,
                               local_histo, local_histoDiplotigs);
            my_ind++;
        }
    }
    LOGF("There were %lld traverse_CBC_chains that had more than 2 paths\n", count_num_more_than_two);

    assert(kmer_len < MAX_KMER_SIZE);

    int64_t skipped = 0;
    int64_t effLinkertigs = 0;
    BV_FREE(local_log_listB);
    BV_FREE(local_log_listC);

    UPC_LOGGED_BARRIER;  // ensure all conflicts are rolled back and fenced

    /* Now just THREAD 0 will initiate traversals from the unvisited seeds */
    if (MYTHREAD == 0) {
        double startSerial = now();
#ifdef DEBUG
        char linkdatacur[2 * MAX_KMER_SIZE];
        linkdatacur[2 * kmer_len] = '\0';
        char linktigsPath[MAX_FILE_PATH];
        sprintf(linktigsPath, "linktigs");
        FILE *linktigs = fopen_rank_path(linktigsPath, "w", MYTHREAD);
#endif

        int64_t totConflicts = _totConflicts;
        if (totConflicts) {
            DBG("\nFinal round of traversals using only Thread0 and only unvisited seeds..\n\n");
            /* Remove duplicates using sorting plus linear pass */
            //LOGF("Consolidating todo from remote to local: %lld\n", (lld) totConflicts);

            // TODO can this be a gather or all on thread 0 to start?
            int64_t *local_todo = (int64_t *)malloc_chk(totConflicts * sizeof(int64_t));
            for (t = 0; t < totConflicts; t++) {
                local_todo[t] = todo[t];
            }
            //LOGF("Done copying todo to local\n");

            /* Remove duplicates using sorting plus linear pass */
            qsort(local_todo, totConflicts, sizeof(int64_t), int64_comp);
            //LOGF("qsort of local_todo finished\n");
            int64_t aux1, aux2, dups = 0;
            for (aux1 = 0, aux2 = 1; aux2 < totConflicts; aux2++) {
                if (local_todo[aux1] != local_todo[aux2]) {
                    aux1++; // non-dup
                } else {
                    assert(local_todo[aux1] == local_todo[aux2]);
                    dups++;
                    totConflicts--;
                }
            }
            //LOGF("Found %lld dups.  totConflicts=%lld\n", (lld) dups, (lld) totConflicts);

            for (t = 0; t < totConflicts; t++) {
                i = local_todo[t];
                CHECK_NCONTIG_BOUND(i);
                if (linkertips[i].used_flag != USED_EXT) {
                    continue;
                }
                UPC_ATOMIC_FADD_I64(&accumulator, &(visitedC[i]), 1);
                flag = (accumulator == 0) ? UNVISITED : VISITED;
                if (flag == VISITED) {
                    DBG2("Skiped entry: %lld\n", (lld)i);
                    skipped++;
                    continue; // In this case we dont have to flush any visited entries from log
                }

                /* Initialize downstream */
                downstr_pos = 0;
                current.objectId = i;
                current.type = 'C';
                current.strand = '+';
                getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);

                while (getNextRes == 1) {
                    if (next.type == 'C') {
                        UPC_ATOMIC_FADD_I64(&accumulator, &(visitedC[next.objectId]), 1);
                        flag = (accumulator == 0) ? UNVISITED : VISITED;
                    } else if (next.type == 'B') {
                        UPC_ATOMIC_FADD_I64(&accumulator, &(visitedB[next.objectId]), 1);
                        flag = (accumulator == 0) ? UNVISITED : VISITED;
                    }

                    if (flag == VISITED) {
                        getNextRes = 0;
                    } else {
                        /* Add to downstream */
                        BV_GET(downstream, downstr_pos) = next;
                        downstr_pos++;
                        current = next;
                        CHECK_NCONTIG_BOUND(current.objectId);
                        getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);
                    }
                }

                /* Initialize upstream */
                upstr_pos = 0;
                current.objectId = i;
                current.type = 'C';
                current.strand = '-';
                getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);

                while (getNextRes == 1) {
                    rstrand = '-';
                    if (next.strand == '-') {
                        rstrand = '+';
                    }

                    if (next.type == 'C') {
                        UPC_ATOMIC_FADD_I64(&accumulator, &(visitedC[next.objectId]), 1);
                        flag = (accumulator == 0) ? UNVISITED : VISITED;
                    } else if (next.type == 'B') {
                        UPC_ATOMIC_FADD_I64(&accumulator, &(visitedB[next.objectId]), 1);
                        flag = (accumulator == 0) ? UNVISITED : VISITED;
                    }

                    if (flag == VISITED) {
                        getNextRes = 0;
                    } else {
                        current = next;
                        next.strand = rstrand;
                        BV_GET(upstream, upstr_pos) = next;
                        upstr_pos++;
                        CHECK_NCONTIG_BOUND(current.objectId);
                        getNextRes = getNext(current, linkertips, bubbletips, pointsTo, &next, kmer_len);
                    }
                }

                /* Remove leading and trailing bubbles */
                /* downstream has push semantics so head is at position 0 */
                /* upstream has unshift semantics so head is at position (upstr_pos - 1) */

                if (upstr_pos > 0) {
                    first = BV_GET(upstream, upstr_pos - 1);
                    if (first.type == 'B') {
                        upstr_pos--;
                    }
                }

                if (downstr_pos > 0) {
                    last = BV_GET(downstream, downstr_pos - 1);
                    if (last.type == 'B') {
                        downstr_pos--;
                    }
                }

                nTigs = 1 + upstr_pos + downstr_pos;

                if (nTigs > 1) {
                    count_num_more_than_two += traverse_CBC_chain(diploidMode, maskMode,
                                       BV_VAR(upstream), i, BV_VAR(downstream), upstr_pos,
                                       nTigs, nContigs, &myDiplotigs, contigInfo, bubbletips, contigSequences, tipInfo,
                                       haplotigsFDBuffer,
                                       local_histo, local_histoDiplotigs);
                    my_ind++;
                }
            }
            free_chk(local_todo);
        }
#ifdef DEBUG
        fclose_track(linktigs);
#endif
        LOGF("Final serial traversal of %lld conflicts (skipped=%lld orig=%lld) took %0.3fs\n", (lld) totConflicts, (lld) skipped, (lld) _totConflicts, now() - startSerial);
    }
    LOGF("After thread 0, final traversals, there were %lld traverse_CBC_chains that had more than 2 paths\n", count_num_more_than_two);

    long totalDiplotigs = reduce_long(myDiplotigs, UPC_ADD, SINGLE_DEST);


#ifdef DEBUG
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &indTravs, my_ind);
#endif

    SLOG("Finished all traversals\n");

    
    UPC_LOGGED_BARRIER;
    //   SDBG("totDiplotigLength = %lld, bases_from_independent = %lld, indTravs = %lld\n", (lld) totDiplotigLength, (lld) bases_from_independent, (lld) indTravs);

    UPC_ALL_FREE_CHK(bubbletips);
    UPC_ALL_FREE_CHK(todo);
    UPC_ALL_FREE_CHK(visitedC);
    UPC_ALL_FREE_CHK(visitedB);
    destroy_hash_table(&tipInfo, &memory_heap);
    destroy_phash_table(&pointsTo, &memory_heap2);
    BV_FREE(upstream);
    BV_FREE(downstream);

    if (diploidMode == DUAL_PATH_MODE) {
        UPC_LOGGED_BARRIER;
        destroy_alignment_heaps(nContigs, &alignment_heap);
    }

    end = UPC_TICKS_NOW();
    double SerialtravGraphTime = UPC_TICKS_TO_SECS(end - start);

    /*                                                       */
    /* Done with diplotigs; now print the remaining isotigs  */
    /*                                                       */

    start = UPC_TICKS_NOW();
    char stat;
    int curle;
    char *printSeq;
    char *bufferStatic = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    sequence_t cs;

    for (i = MYTHREAD; i < nContigs; i += THREADS) {
        stat = contigInfo[i].statusField;

        if (stat == 1) {
            myIsotigs++;
            cs = contigSequences[i];
            curle = cs.seqLength;

            if (curle > MAX_CONTIG_SIZE - 1) {
                printSeq = (char *)malloc_chk((curle + 1) * sizeof(char));
            } else {
                printSeq = bufferStatic;
            }

            upc_memget(printSeq, cs.data, curle * sizeof(char));
            printSeq[curle] = '\0';

#ifdef DEBUG
            UPC_ATOMIC_FADD_I64(&curMetaConId, &McontigID, 1);
#endif
            recordIdLengthDepth(curMetaConId, printSeq, contigInfo[i].meanDepth, IS_ISOTIG, haplotigsFDBuffer, 0);

            /* GZIP_PRINTF(haplotigsFD_raw, ">Contig_%lld\n", (lld) curMetaConId); */
            /* GZIP_FWRITE(printSeq, 1, strlen(printSeq), haplotigsFD_raw); */
            /* GZIP_PRINTF(haplotigsFD_raw, "\n"); */

            update_depth_histogram(local_histo, contigInfo[i].meanDepth);
            update_depth_histogram(local_histoIsotigs, contigInfo[i].meanDepth);

            if (curle > MAX_CONTIG_SIZE - 1) {
                free_chk(printSeq);
            }
        }
    }

    long totalIsotigs = reduce_long(myIsotigs, UPC_ADD, SINGLE_DEST);

    end = UPC_TICKS_NOW();
    double travGraphTime = UPC_TICKS_TO_SECS(end - start);

    start = UPC_TICKS_NOW();

    UPC_LOGGED_BARRIER;

    free_chk(bufferStatic);

    LOGF("Finished processing contigs.  haplotigsFDBuffer size %lld\n", (lld)getLengthBuffer(haplotigsFDBuffer));
    UPC_LOGGED_BARRIER;

    // This routine is problematic as the memory ownership is essentially random -- hangs on infiniband
    for (contigID = MYTHREAD; contigID < nContigs; contigID += THREADS) {
        assert(upc_threadof(&contigSequences[contigID]) == MYTHREAD);
        if (contigSequences[contigID].data) {
            UPC_FREE_TRACK(contigSequences[contigID].data);
        }
        contigSequences[contigID].data = NULL;
    }
    // Free the list of memory allocs that this thread originated
    UPC_FREE_ALL_TRACKED(myUPCAllocs);
    freeBuffer(myUPCAllocs);

    upc_fence;
    UPC_ALL_FREE_CHK(contigSequences);
    UPC_LOGGED_BARRIER;
    timerEnd = UPC_TICKS_NOW();
    double fetchingSequencesTime = UPC_TICKS_TO_SECS(timerEnd - start);
    timerStart = UPC_TICKS_NOW();


    /* Now all reduce the local histograms */
    int64_t padded_size = ((histogram_size + THREADS - 1) / THREADS) * ((int64_t)THREADS);
    shared[1] int64_t * dist_histograms = NULL;
    UPC_ALL_ALLOC_CHK(dist_histograms, ((int64_t)THREADS) * padded_size, sizeof(int64_t));

    shared[1] int64_t * shared_result = NULL;
    UPC_ALL_ALLOC_CHK(shared_result, histogram_size, sizeof(int64_t));

    for (j = 0; j < histogram_size; j++) {
        dist_histograms[MYTHREAD * padded_size + j] = local_histo[j];
    }

    UPC_LOGGED_BARRIER;

    int64_t cur_sum = 0;

    for (j = MYTHREAD; j < histogram_size; j += THREADS) {
        cur_sum = 0;
        for (i = 0; i < THREADS; i++) {
            cur_sum += dist_histograms[i * padded_size + j];
        }
        shared_result[j] = cur_sum;
    }
    LOGF("Generated histograms cur_sum: %lld\n", (lld)cur_sum);

    UPC_LOGGED_BARRIER;

    timerEnd = UPC_TICKS_NOW();
    double reducingHistogramTime = UPC_TICKS_TO_SECS(timerEnd - timerStart);
    timerStart = UPC_TICKS_NOW();

    GZIP_FILE fd_histo, fd_histoIsotigs, fd_histoDiplotigs;

    if (MYTHREAD == 0) {
        for (j = 0; j < histogram_size; j++) {
            local_histo[j] = shared_result[j];
        }

        char path[MAX_FILE_PATH];
        snprintf(path, MAX_FILE_PATH, "contig_histogram-%d.txt" GZIP_EXT, kmer_len);
        fd_histo = GZIP_OPEN_RANK_PATH(path, "w", -1);

        for (j = 0; j < histogram_size; j++) {
            GZIP_PRINTF(fd_histo, "%d\t%lld\n", j * bin_size, (lld)local_histo[j]);
        }

        GZIP_CLOSE(fd_histo);
        if (_bubbleContigMinDepth == 0) {
            _bubbleContigMinDepth = findDMin(local_histo, histogram_size, 0, bin_size, 1);
            if (_bubbleContigMinDepth < 0) {
                DIE("Could not automatically determine the BUBBLE_MIN_DEPTH_CUTOFF.  Please examine the contig_histogram.txt and set the bubble_min_depth_cutoff parameter in the config file,  then resume the run.\n");
            }
            upc_fence;

            //save to a file for future use
            char autocalibratedBubbleDepthFileName[MAX_FILE_PATH];
            sprintf(autocalibratedBubbleDepthFileName, "bubble_depth_threshold.txt");
            get_rank_path(autocalibratedBubbleDepthFileName, -1);
            FILE *FD = fopen_chk(autocalibratedBubbleDepthFileName, "w");
            fprintf(FD, "%lld\n", (lld) _bubbleContigMinDepth);
            fclose_track(FD);
        }
        SLOG("Using %lld as bubble_contig_min_depth to screen out heterozygous isotigs\n", (lld)_bubbleContigMinDepth);
    }

    SLOG("output histograms\n");
    UPC_LOGGED_BARRIER;

    timerEnd = UPC_TICKS_NOW();
    double findingDminTime = UPC_TICKS_TO_SECS(timerEnd - timerStart);
    timerStart = UPC_TICKS_NOW();

    upc_tick_t t_write_start = UPC_TICKS_NOW();



    // In MAX_DEPTH_MODE,  print the filtered isotigs  >= bubbleMinContigDepth + all diplotigs.
    // Otherwise print all



    size_t unfilteredHaplotigs = 0;
    size_t filteredHaplotigs = writeHaplotigs(base_dir, newFileName, haplotigsFDBuffer, diploidMode, &unfilteredHaplotigs);
    int64_t totalFilteredHaplotigs = 0;
    size_t gotFilteredHaplotigs = reduce_prefix_long(filteredHaplotigs, UPC_ADD, &totalFilteredHaplotigs) - filteredHaplotigs;
    LOGF("filteredHaplotigs = %lld, atomic totalFilteredHaplotigs = %lld\n", (lld)filteredHaplotigs, (lld)gotFilteredHaplotigs);
    size_t filteredOutIsotigs = unfilteredHaplotigs - filteredHaplotigs;
    LOGF("filtered out %lld isotigs\n", (lld)filteredOutIsotigs);

    freeBuffer(haplotigsFDBuffer);
    haplotigsFDBuffer = NULL;

    timerEnd = UPC_TICKS_NOW();
    double purePrintTime = UPC_TICKS_TO_SECS(timerEnd - timerStart);
    timerStart = UPC_TICKS_NOW();

    /*  Now all reduce the local histograms for diplotigs */

    for (j = 0; j < histogram_size; j++) {
        dist_histograms[MYTHREAD * padded_size + j] = local_histoDiplotigs[j];
    }

    UPC_LOGGED_BARRIER;

    if (MYTHREAD == 0) {
        serial_printf("Wrote %lld filteredHaplotigs in %.3f s\n", (lld)totalFilteredHaplotigs, UPC_TICKS_TO_SECS(UPC_TICKS_NOW() - start));
        ADD_DIAG("%lld", "total_filtered_haplotigs", (lld)totalFilteredHaplotigs);
    }

    for (j = MYTHREAD; j < histogram_size; j += THREADS) {
        cur_sum = 0;
        for (i = 0; i < THREADS; i++) {
            cur_sum += dist_histograms[i * padded_size + j];
        }
        shared_result[j] = cur_sum;
    }

    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(dist_histograms);

    timerEnd = UPC_TICKS_NOW();
    double reducingDiplotigHistogramTime = UPC_TICKS_TO_SECS(timerEnd - timerStart);
    timerStart = UPC_TICKS_NOW();

    if (MYTHREAD == 0) {
        for (j = 0; j < histogram_size; j++) {
            local_histoDiplotigs[j] = shared_result[j];
            local_histoIsotigs[j] = local_histo[j] - local_histoDiplotigs[j];
        }

        char path[MAX_FILE_PATH];
        snprintf(path, MAX_FILE_PATH, "contig_histogram_isotigs-%d.txt" GZIP_EXT, kmer_len);
        fd_histoIsotigs = GZIP_OPEN_RANK_PATH(path, "w", -1);
        snprintf(path, MAX_FILE_PATH, "contig_histogram_diplotigs-%d.txt" GZIP_EXT, kmer_len);
        fd_histoDiplotigs = GZIP_OPEN_RANK_PATH(path, "w", -1);


        for (j = 0; j < histogram_size; j++) {
            GZIP_PRINTF(fd_histoIsotigs, "%d\t%lld\n", j * bin_size, (lld)local_histoIsotigs[j]);
            GZIP_PRINTF(fd_histoDiplotigs, "%d\t%lld\n", j * bin_size, (lld)local_histoDiplotigs[j]);
        }

        GZIP_CLOSE(fd_histoIsotigs);
        GZIP_CLOSE(fd_histoDiplotigs);
    }
    SLOG("output contig histograms\n");

    free_chk(local_histo);
    free_chk(local_histoIsotigs);
    free_chk(local_histoDiplotigs);

    UPC_LOGGED_BARRIER;
    timerEnd = UPC_TICKS_NOW();
    double writingHistogramTime = UPC_TICKS_TO_SECS(timerEnd - timerStart);

    UPC_ALL_FREE_CHK(linkertips);
    UPC_ALL_FREE_CHK(contigInfo);
    UPC_ALL_FREE_CHK(shared_result);

    end = UPC_TICKS_NOW();
    double haplotigPrintingTime = UPC_TICKS_TO_SECS(end - start);


    /* Report performance breakdown */
    if (MYTHREAD == 0) {
        printf("Processing cea files %f seconds\n", ceaProcTime);
        printf("Processing contig files %f seconds\n", contigProcTime);
        printf("Processing merDepth files %f seconds\n", merdepthProcTime);
        printf("Filling bubbleMap %f seconds\n", bubbleMapTime);
	
        if (diploidMode == DUAL_PATH_MODE){
            printf("Storing mapped clones %f seconds\n", storeClonesTime);
        }
        printf("Building bubble-contig graph time %f seconds\n", buildGraphTime);
        printf("Parallel traversal bubble-contig graph time %f seconds\n", travGraphTime);
        printf("Serial traversal bubble-contig graph time %f seconds\n", SerialtravGraphTime);
        printf("Haplotig printing time %f seconds\n======\n", haplotigPrintingTime);
        printf("Fetching sequences time %f seconds\n", fetchingSequencesTime);
        printf("Reducing histogram time %f seconds\n", reducingHistogramTime);
        printf("Finding dmin time %f seconds\n", findingDminTime);
        printf("Pure printing time %f seconds\n", purePrintTime);
        printf("Reducing diplotigs histogram time %f seconds\n", reducingDiplotigHistogramTime);
        printf("Writing histograms time %f seconds\n====\n", writingHistogramTime);
        printf("\n=========================================\n");
        printf("Total time %f seconds\n", ceaProcTime + contigProcTime + merdepthProcTime + bubbleMapTime + buildGraphTime + travGraphTime + haplotigPrintingTime);
        printf("Total bubbles: %lld\n", (lld)bubbleID - 1);
#ifdef DEBUG
        printf("Total ind travs is %lld, total conflicts are %lld\n", (lld)indTravs, (lld)_totConflicts);
        printf("Total edges: %lld\n", (lld)_edges);
        printf("Total effective linkertips: %lld\n", (lld)effLinkertigs);
        printf("Total skipped: %lld\n", (lld)skipped);
        printf("Total length of diplotigs is %lld bases\n", (lld)totDiplotigLength);
#endif
        printf("Total diplotigs found %lld\n", (lld)totalDiplotigs);
        printf("Total isotigs found %lld\n", (lld)totalIsotigs);
        printf("Total filtered isotigs + diplotigs %lld\n", (lld)totalFilteredHaplotigs);

        ADD_DIAG("%lld", "total_bubbles", (lld)bubbleID - 1);
        ADD_DIAG("%lld", "total_diplotigs", (lld)totalDiplotigs);
        ADD_DIAG("%lld", "total_isotigs", (lld)totalIsotigs);

        char countFileName[MAX_FILE_PATH];
        snprintf(countFileName, MAX_FILE_PATH, "n%s.txt", newFileName);
        FILE *countFD = fopen_rank_path(countFileName, "w", -1);
        fprintf(countFD, "%lld\n", (lld)totalFilteredHaplotigs);
        fclose_track(countFD);
    }

/*    GZIP_CLOSE(haplotigsFD_raw); */
/* #ifdef PRINT_MAPPING */
/*    GZIP_CLOSE(mappingFD); */
/* #endif */

    UPC_LOGGED_BARRIER;

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }
    if (merAlignerOutputNames) free_chk(merAlignerOutputNames);
    return 0;
}
