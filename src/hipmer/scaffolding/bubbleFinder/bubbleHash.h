#ifndef BUBBLE_HASH_H
#define BUBBLE_HASH_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <assert.h>

#include "bubbleFinder.h"

typedef char fixed_read_name[MAX_READ_NAME_LEN];

typedef struct stream_t stream_t;
struct stream_t {
    int64_t objectId;
    char    type;
    char    strand;
};

/* Data_t data structure for tipInfo */
typedef struct data_t data_t;
struct data_t {
    shared[] data_t * next;
    int64_t     contig;
    char        rcFlag;
};

/* Enhanced data_t data structure for tipInfo and h_path member elements*/
typedef struct enhanced_data_t enhanced_data_t;
struct enhanced_data_t {
    int64_t          contig;
    int              length;
    int              endClip;
    float            depth;
    char *           seq;
    fixed_read_name *clones;
    char             type;
    char             rcFlag;
};

/* pdata_t data structure for pointsTo */
typedef struct pdata_t pdata_t;
struct pdata_t {
    shared[] pdata_t * next;
    int64_t object;
    char    type;
};

/* Hash-table entries data structure */
typedef struct list_t list_t;
struct list_t {
    shared[] list_t * next;
    shared[] data_t * data;
    char    tipKey[2 * MAX_KMER_SIZE];
    int64_t nContigs;
};

/* Hash-table entries data structure for points to */
typedef struct plist_t plist_t;
struct plist_t {
    shared[] plist_t * next;
    shared[] pdata_t * data;
    char    tipKey[MAX_KMER_SIZE];
    int64_t nContigs;
};
/* Bucket data structure */
typedef struct bucket_t bucket_t;
struct bucket_t {
    shared[] list_t * head;                     // Pointer to the first entry of the hashtable
};

/* Bucket data structure for pointsTo */
typedef struct pbucket_t pbucket_t;
struct pbucket_t {
    shared[] plist_t * head;                    // Pointer to the first entry of the hashtable
};

/* Hash-table data structure */
typedef struct hash_table_t hash_table_t;
struct hash_table_t {
    int64_t size;                               // Size of the hashtable
    shared[BS] bucket_t * table;                // Entries of the hashtable are pointers to buckets
    CountTimer ct, addTips;
    int64_t nNexts;
};

/* Hash-table data structure for pointsTo */
typedef struct phash_table_t phash_table_t;
struct phash_table_t {
    int64_t size;                               // Size of the hashtable
    shared[BS] pbucket_t * table;               // Entries of the hashtable are pointers to buckets
    CountTimer ct, addEdges;
    int64_t nNexts;
};

/* Memory heap data structure */
typedef struct memory_heap_t memory_heap_t;
struct memory_heap_t {
    shared[] list_t * my_heap_ptr;                  // Pointer to my heap
    shared[] data_t * my_data_ptr;                  // Pointer to my data
    int64_t my_heap_pos;
    int64_t my_data_pos;
    int64_t max_pos;
};

/* Memory heap data structure for pointsTo */
typedef struct pmemory_heap_t pmemory_heap_t;
struct pmemory_heap_t {
    shared[] plist_t * my_heap_ptr;                 // Pointer to my heap
    shared[] pdata_t * my_data_ptr;                 // Pointer to my data
    int64_t my_heap_pos;
    int64_t my_data_pos;
    int64_t max_pos;
};


/* extensions data structure */
typedef struct extensions_t extensions_t;
struct extensions_t {
    char data[2 * MAX_KMER_SIZE];
    char used_flag;
};

/* bubbletip data structure */
typedef struct bubbletip_t bubbletip_t;
struct bubbletip_t {
    char tipKey[2 * MAX_KMER_SIZE];
};

/* Creates and initializes a distributed hastable - collective function */
hash_table_t *create_hash_table(int64_t size, memory_heap_t *memory_heap, int64_t my_heap_size);

void destroy_hash_table(hash_table_t **_ht, memory_heap_t *memory_heap);

/* Creates pointsTo and initializes a distributed hastable - collective function */
phash_table_t *create_phash_table(int64_t size, pmemory_heap_t *memory_heap, int64_t my_heap_size);

void destroy_phash_table(phash_table_t **_ht, pmemory_heap_t *memory_heap);

/* Add entry to the hash table for tipInfo */
int add_tip(hash_table_t *hashtable, const char *tip, int64_t contig, memory_heap_t *memory_heap, int kmer_len);

shared[] list_t *lookup_and_copy_tipInfo(hash_table_t *hashtable, const char *tipKey, list_t *copy, int kmer_len);

shared[] plist_t *lookup_and_copy_pt(phash_table_t *hashtable, const char *tipKey, plist_t *copy, int kmer_len);

/* Add entry to the hash table for pointsTo */
int add_edge(phash_table_t *hashtable, const char *tipKey, int64_t objectID, char type, pmemory_heap_t *memory_heap, int kmer_len);

int getNext(stream_t input, shared[1] extensions_t *linkertips, shared[1] bubbletip_t *bubbletips, phash_table_t *pointsTo, stream_t *output, int kmer_len);


#endif // BUBBLE_HASH_H
