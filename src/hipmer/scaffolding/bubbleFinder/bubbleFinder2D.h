#ifndef BF2D_H
#define BF2D_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

#include <upc.h>
#include "upc_compatibility.h"
#include "bubbleFinder.h"
#include "rb_tree.h"

#define NUM_HPATHS 2

extern shared int64_t McontigID;




////* types and methods for handling alignment info *////
///
//



/* A bare minimum alignment struct used strictly for clone->contig assignments */
typedef struct alignment_t alignment_t;
struct alignment_t {
    fixed_read_name readName;
    int64_t         subjectId;
    char            type;
};


/* Parses a single alignment */
/* Returns 1 if the alignemnt is valid. Returns 0 if the alignment is a guard alignment */
int parseAlignment(char *inputAln, char *readName, int64_t *subjectId);

typedef shared[] alignment_t *shared_alignment_ptr;  // pointer to a shared array of alignments

/* Alignment heap data structure */
typedef struct alignment_heap_t alignment_heap_t;
struct alignment_heap_t {
    shared[1] int64_t * heap_indices;              // pointer to shared array of indices of remote heaps
    shared[1] shared_alignment_ptr * alignment_ptr; //  pointer to shared array of shared arrays of alignments
};

/* Creates shared heaps that required for alignment heaps - IMPORTANT: This is a collective function */
int create_alignment_heaps(int64_t nContigs, alignment_heap_t *alignment_heap, shared int64_t *cloneCnts);

void destroy_alignment_heaps(int64_t nContigs, alignment_heap_t *alignment_heap);

int isPairedRead(const char *readName);

int isPairedRead(const char *readName);
/* truncates string from the right end at the point encountering the specifiec character */
void rtrim_at(const char *string, char rtrimMark, char *out);


//////* types and methods for the dual path diplotigs */////
////                                                    ////
//                                                        //

typedef struct h_path {
    int              n;
    double           depth;
    int              totLength;
    //  char finalSeq[MAX_CONTIG_SIZE];
    char *           finalSeq;
    struct rb_tree  *path_clones;
    enhanced_data_t *tigs;
}h_path;

int rb_tree_cmp_clone(struct rb_tree *self, struct rb_node *node_a, struct rb_node *node_b);

void rb_tree_delete_clone_cb(struct rb_tree *self, struct rb_node *node);

int add_clone(struct rb_tree *clones_hash, char *cloneName);

bool clone_exists(struct rb_tree *clones_hash, fixed_read_name cloneName);

void delete_all_clones(struct rb_tree *clones_hash);

int link_clones_to_path(h_path *path_ptr, fixed_read_name *btigClones, int minSupport);


int assign_btig_to_hpath(enhanced_data_t *btig, h_path *hPaths);

// copy tig structure into haplopath
void add_tig_to_hpath(h_path *hp, enhanced_data_t *tig);

// Complete the haplopaths with seq and depth info
void bless_haplopaths(h_path *haploPaths, shared[1] sequence_t *contigSequences, int maskMode, int kmer_len);

// Record haploPath sequence into buffer for later use (filtering, writing, etc)
void store_haplopaths(h_path *haploPaths, int64_t diplotigId, int64_t *curMetaConIdPtr, int64_t nBubbles, int64_t nCons, Buffer haplotigsFDBuffer);

#endif //BF2D_H
