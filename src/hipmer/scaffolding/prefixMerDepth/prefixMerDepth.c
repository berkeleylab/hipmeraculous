#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>

#include "optlist.h"

#include "upc_common.h"
#include "common.h"
#include "meraculous.h"
#include "utils.h"
#include "upc_output.h"

#include "../merDepth/kmer_hash_merdepth.h"
#include "../../contigs/kmer_handling.h"
#include "../../kcount/readufx.h"

#include "../merDepth/buildUFXhashBinary_merdepth.h"

#define MAX_CONTIG_SIZE 9000000

int main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

    if (_sv != NULL) {
        DBG("Zeroing timers\n");
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:k:d:s:B:e:X");
    print_args(optList, __func__);

    char *input_UFX_name, *contig_file_name;
    int kmerLength, dmin, chunk_size;
    double errorRate = 1.0;
    int is_per_thread = 0;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    const char *base_dir = ".";

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = kmerLength = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Invalid kmer length %d for this binary compiled with KMER_MAX_SIZE=%d\n", kmer_len, MAX_KMER_SIZE);
            }
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'e':
            errorRate = atof(thisOpt->argument);
            break;
        case 's':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        default:
            break;
        }
    }

    HASH_TABLE_T *dist_hashtable;
    UPC_TICK_T start, end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
    }
    double con_time, depth_time;

#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    /* Build hash table using UFX file that also contains k-mer depths */

    int64_t myshare;
    int dsize;
    int64_t size;
    MEMORY_HEAP_T memory_heap;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Refreshing local checkpoints of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        // force a restore from previous checkpoint, if needed
        restoreCheckpoint(tmp);
        // same for the .entries file
        strcat(tmp, ".entries");
        restoreCheckpoint(tmp);
    }   

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) {
        DIE("Could not load UFX: %s is_per_thread: %d base_dir: %s\n", input_UFX_name, is_per_thread, base_dir);
    }
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more more memory / nodes\n", (lld)minMemory, (lld)size);
    }
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, errorRate, chunk_size, 1, kmer_len, UFX_f);


#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = UPC_TICKS_TO_SECS(end - start);
        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");
        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
        start = UPC_TICKS_NOW();
    }
#endif

    /* Calculating the depth of the contigs */

    /* Read contigs and find mean depth for each one */
    char my_contig_file_name[255];
    char my_output_file_name[255];
    //char contigBuffer[MAX_CONTIG_SIZE];
    char *contigBuffer = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));

    GZIP_FILE contigFile, myOutputFile;
    int64_t contigID;
    int is_least;
    char *curKmer;
    LIST_T copy;
    shared[] LIST_T * lookup_res = NULL;
    int64_t runningDepth = 0;
    int nMers = 0, contigLen, i;
    double mean = 0.0;

    curKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    curKmer[kmerLength] = '\0';

    sprintf(my_contig_file_name, "%s.fasta" GZIP_EXT, contig_file_name);
    sprintf(my_output_file_name, "prefixMerDepth_%s" GZIP_EXT, contig_file_name);


    contigFile = openCheckpoint(my_contig_file_name, "r");
    myOutputFile = openCheckpoint(my_output_file_name, "wb");

    int64_t nMersInContigs = 0;

    /* Calculate the number of nMers in the contig set */
    while (GZIP_GETS(contigBuffer, MAX_CONTIG_SIZE, contigFile) != NULL) {
        if (!GZIP_GETS(contigBuffer, MAX_CONTIG_SIZE, contigFile)) {
            break;
        }
        contigBuffer[strlen(contigBuffer) - 1] = '\0';
        contigLen = strlen(contigBuffer);
        nMers = contigLen - kmerLength + 1;
        nMersInContigs += nMers;
    }

    GZIP_REWIND(contigFile);

    int64_t *prefixDepthSumArray = (int64_t *)malloc_chk(nMersInContigs * sizeof(int64_t));
    int64_t posInArray = 0;

    /* Store the prefix arrays in a very compact way */
    while (GZIP_GETS(contigBuffer, MAX_CONTIG_SIZE, contigFile) != NULL) {
        /* Read a contig and its length */
        contigID = atol(contigBuffer + 8);
        if (!GZIP_GETS(contigBuffer, MAX_CONTIG_SIZE, contigFile)) {
            break;
        }
        contigBuffer[strlen(contigBuffer) - 1] = '\0';
        contigLen = strlen(contigBuffer);
        runningDepth = 0;
        /* For each kmer in the contig extract the depth and add to the running sum */
        for (i = 0; i <= contigLen - kmerLength; i++) {
            memcpy(curKmer, &contigBuffer[i], kmerLength * sizeof(char));
            lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, curKmer, &copy, &is_least, kmer_len);

            if (i == 0) {
                prefixDepthSumArray[posInArray] = copy.count;
            } else {
                prefixDepthSumArray[posInArray] = copy.count + prefixDepthSumArray[posInArray - 1];
            }
            posInArray++;
        }
    }

    GZIP_FWRITE(prefixDepthSumArray, 1, nMersInContigs * sizeof(int64_t), myOutputFile);

    closeCheckpoint(contigFile);
    closeCheckpoint(myOutputFile);

    UFXClose(UFX_f);


    upc_barrier;
    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);

    upc_barrier;
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        depth_time = UPC_TICKS_TO_SECS(end - start);
        printf("\nTime for calculating the prefix mer-depth arrays : %f seconds\n", depth_time);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
