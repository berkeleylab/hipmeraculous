#include "contigMerDepth_main.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("contigMerDepth");
    return contigMerDepth_main(argc, argv);
}
