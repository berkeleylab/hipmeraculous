#include "contigMerDepth_main.h"

int contigMerDepth_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

    if (_sv != NULL) {
        DBG("zeroing times\n");
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:k:d:D:s:B:X");
    print_args(optList, __func__);

    char *input_UFX_name, *contig_file_name;
    int kmerLength, dmin, chunk_size;
    double dynamic_dmin = 1.0;
    int is_per_thread = 0;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    const char *base_dir = ".";

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = kmerLength = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Please compile with a larger MAX_KMER_SIZE (%d) for kmer length %d", MAX_KMER_SIZE, kmer_len);
            }
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'D':
            dynamic_dmin = atof(thisOpt->argument);
            break;
        case 's':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        default:
            break;
        }
    }

    HASH_TABLE_T *dist_hashtable;
    UPC_TICK_T start, end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
    }
    double con_time, depth_time;


#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    /* Build hash table using UFX file that also contains k-mer depths */

    int64_t myshare;
    int dsize;
    int64_t size;
    MEMORY_HEAP_T memory_heap;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Refreshing local checkpoints of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        if (!doesLocalCheckpointExist(tmp)) {
            // force a restore from previous checkpoint, if needed
            restoreLocalCheckpoint(tmp);
            // same for the .entries file
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }   

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) {
        DIE("Could not load UFX: %s is_per_thread: %d base_dir: %s\n", input_UFX_name, is_per_thread, base_dir);
    }
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more total memory / nodes\n", (lld)minMemory, (lld)size);
    }
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, dynamic_dmin, chunk_size, 1, kmer_len, UFX_f);


#ifdef PROFILE
    UPC_LOGGED_BARRIER;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = UPC_TICKS_TO_SECS(end - start);
        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");
        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
        start = UPC_TICKS_NOW();
    }
#endif

    /* Calculating the depth of the contigs */

    /* Read contigs and find mean depth for each one */
    char my_contig_file_name[MAX_FILE_PATH];
    char my_output_file_name[MAX_FILE_PATH];

    gzFile contigFile; // must be gzFile, can be uncompressed
    GZIP_FILE myOutputFile;
    int64_t contigID;
    int is_least;
    char *curKmer;
    LIST_T copy;
    shared[] LIST_T * lookup_res = NULL;
    int64_t runningDepth = 0;
    int nMers = 0, contigLen, i;
    double mean = 0.0;

    curKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    curKmer[kmerLength] = '\0';

    sprintf(my_contig_file_name, "%s.fasta" GZIP_EXT, contig_file_name);
    contigFile = openCheckpoint1(my_contig_file_name, "r");
    if (!contigFile) {
        DIE("Could not open %s\n", my_contig_file_name);
    }
    kseq_t *ks = kseq_init(contigFile);

    sprintf(my_output_file_name, "merDepth_%s.txt" GZIP_EXT, contig_file_name);
    myOutputFile = openCheckpoint(my_output_file_name, "w");

#ifndef DEPTH_AS_MAX
    while (kseq_read(ks) >= 0) {
        DBG("Read %s\n", ks->name.s);
        /* Read a contig and its length */
        contigID = atol(ks->name.s + 7);
        contigLen = ks->seq.l;
        runningDepth = 0;
        /* For each kmer in the contig extract the depth and add to the running sum */
        for (i = 0; i <= contigLen - kmerLength; i++) {
            memcpy(curKmer, ks->seq.s + i, kmerLength * sizeof(char));
            lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, curKmer, &copy, &is_least, kmer_len);
            runningDepth += copy.count;
        }

        nMers = contigLen - kmerLength + 1;
        mean = (1.0 * runningDepth) / (1.0 * nMers);
        GZIP_PRINTF(myOutputFile, "Contig%lld\t%d\t%f\n", (lld)contigID, nMers, mean);
    }
#endif

#ifdef DEPTH_AS_MAX
    while (kseq_read(ks) >= 0) {
        DBG("Read %s\n", ks->name.s);
        /* Read a contig and its length */
        contigID = atol(ks->name.s + 7);
        contigLen = ks->seq.l;
        runningDepth = 0;
        /* For each kmer in the contig extract the depth and add to the running sum */
        for (i = 0; i <= contigLen - kmerLength; i++) {
            memcpy(curKmer, ks->seq.s + i, kmerLength * sizeof(char));
            lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, curKmer, &copy, &is_least, kmer_len);
            if (copy.count > runningDepth) {
                runningDepth = copy.count;
            }
        }

        nMers = contigLen - kmerLength + 1;
        mean = (1.0 * runningDepth) / (1.0 * nMers);
        GZIP_PRINTF(myOutputFile, "Contig%lld\t%d\t%f\n", (lld)contigID, nMers, 1.0 * runningDepth);
    }
#endif

    kseq_destroy(ks);
    closeCheckpoint1(contigFile);
    closeCheckpoint(myOutputFile);

    UFXClose(UFX_f);


    UPC_LOGGED_BARRIER;
    // this call frees the memory heap
    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);
    free_chk(curKmer);

    UPC_LOGGED_BARRIER;
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        depth_time = UPC_TICKS_TO_SECS(end - start);
        printf("\nTime for calculating the contig depths : %f seconds\n", depth_time);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
