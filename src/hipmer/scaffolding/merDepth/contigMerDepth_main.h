#ifndef CONTIG_MERDEPTH_MAIN_H_
#define CONTIG_MERDEPTH_MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>
#include <upc.h>

#include "optlist.h"
#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"
#include "Buffer.h"
#include "kseq.h"
#include "timers.h"
#include "utils.h"
#include "upc_output.h"

#include "meraculous.h"
#include "kmer_hash_merdepth.h"
#include "buildUFXhashBinary_merdepth.h"

#include "../../kcount/readufx.h"
//#define DEPTH_AS_MAX

#define MAX_CONTIG_SIZE 900000

int contigMerDepth_main(int argc, char **argv);

#endif // CONTIG_MERDEPTH_MAIN_H_
