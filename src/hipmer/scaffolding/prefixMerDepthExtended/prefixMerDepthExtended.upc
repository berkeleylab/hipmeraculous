#include "prefixMerDepthExtended_main.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("prefixMerDepthExtended");
    return prefixMerDepthExtended_main(argc, argv);
}
