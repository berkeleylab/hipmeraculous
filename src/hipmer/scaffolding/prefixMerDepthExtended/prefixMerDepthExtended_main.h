#ifndef PREFIX_MERDEPTH_EXTENDED_MAIN_H_
#define PREFIX_MERDEPTH_EXTENDED_MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>

#include "optlist.h"

#include "upc_common.h"
#include "common.h"
#include "Buffer.h"
#include "meraculous.h"
#include "utils.h"
#include "upc_output.h"

#include "../merDepth/merdepth_dds.h"

#include "../../contigs/kmer_handling.h"
#include "../../kcount/readufx.h"

#include "../merDepth/kmer_hash_merdepth.h"
#include "../merDepth/buildUFXhashBinary_merdepth.h"

#define MAX_CONTIG_SIZE 9000000

/* Define segment length for FASTA sequences */
#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

int prefixMerDepthExtended_main(int argc, char **argv);

#endif // PREFIX_MERDEPTH_EXTENDED_MAIN_H_
