#include "prefixMerDepthExtended_main.h"

int prefixMerDepthExtended_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    int kmer_len = MAX_KMER_SIZE - 1;

    if (_sv != NULL) {
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:c:k:d:s:B:e:X");
    print_args(optList, __func__);

    char *input_UFX_name, *contig_file_name;
    int kmerLength, dmin, chunk_size;
    double errorRate = 1.0;
    double depth_dropoff = 2.0;
    int use_left = 0;
    int use_right = 0;
    double current_depth = 0.0;
    int is_per_thread = 0;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    const char *base_dir = ".";

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = kmerLength = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Invalid kmer length %d for this binary compiled with KMER_MAX_SIZE=%d\n", kmer_len, MAX_KMER_SIZE);
            }
            break;
        case 'c':
            contig_file_name = thisOpt->argument;
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'e':
            errorRate = atof(thisOpt->argument);
            break;
        case 's':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        default:
            break;
        }
    }

    HASH_TABLE_T *dist_hashtable;
    UPC_TICK_T start, end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
               (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
    }
    double con_time, depth_time;

#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    /* Build hash table using UFX file that also contains k-mer depths */

    int64_t myshare;
    int dsize;
    int64_t size;
    MEMORY_HEAP_T memory_heap;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Refreshing local checkpoints of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        if (!doesLocalCheckpointExist(tmp)) {
            // force a restore from previous checkpoint, if needed
            restoreLocalCheckpoint(tmp);
            // same for the .entries file
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }   

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) DIE("Could not load UFX: %s is_per_thread: %d base_dir: %s\n", input_UFX_name, is_per_thread, base_dir);
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    if (MYTHREAD == 0) {
        int64_t minMemory = 12 * (sizeof(LIST_T) + sizeof(int64_t)) * size / 10 / 1024 / 1024 / THREADS;
        printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more total memory / nodes\n", (lld)minMemory, (lld)size);
    }
    LOGF("Opened UFX dsize=%lld myshare=%lld, size=%lld %s\n", (lld)dsize, (lld)myshare, (lld)size, input_UFX_name);
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, errorRate, chunk_size, 1, kmer_len, UFX_f);


#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        con_time = UPC_TICKS_TO_SECS(end - start);
        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");
        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
        start = UPC_TICKS_NOW();
    }
#endif

    /* Calculating the depth of the contigs */

    /* Read contigs and find mean depth for each one */
    char my_contig_file_name[MAX_FILE_PATH];
    char my_output_file_name[MAX_FILE_PATH];
    char my_extended_output_file_name[MAX_FILE_PATH];

    Buffer contigBuffer = initBuffer(MAX_CONTIG_SIZE);
    char *contigB = NULL;

    GZIP_FILE contigFile;
    GZIP_FILE myOutputFile;
    GZIP_FILE myExtendedOutputFile;
    int64_t contigID;
    int is_least;
    char *curKmer;
    LIST_T copy;
    shared[] LIST_T * lookup_res = NULL;
    int64_t runningDepth = 0, initOffset;
    int nMers = 0, contigLen, i;
    double mean = 0.0;

    curKmer = (char *)malloc_chk((kmerLength + 1) * sizeof(char));
    curKmer[kmerLength] = '\0';

    sprintf(my_contig_file_name, "%s.txt" GZIP_EXT, contig_file_name);
    sprintf(my_output_file_name, "merDepth_extended_%s.bin" GZIP_EXT, contig_file_name);
    sprintf(my_extended_output_file_name, "extended_%s.fasta" GZIP_EXT, contig_file_name);

    /* PRINT FASTA LOGISTICS */
    int64_t total_written = 0, cur_length;
    int64_t towrite;
    char fastaSegment[SEGMENT_LENGTH];
    fastaSegment[SEGMENT_LENGTH - 1] = '\0';
    char *seqF;

    contigFile = openCheckpoint(my_contig_file_name, "r");
    myOutputFile = openCheckpoint(my_output_file_name, "w");
    myExtendedOutputFile = openCheckpoint(my_extended_output_file_name, "w");
    //LOG("Opened files: %s %s %s\n", my_contig_file_name, my_output_file_name, my_extended_output_file_name);

    int64_t nMersInContigs = 0;
    int lengthLeft;
    int lengthOriginal;
    int lengthRight;
    char *token, *aux;
    int depthLeft, depthRight;

    /* Calculate the number of nMers in the contig set */
    resetBuffer(contigBuffer);
    while ((contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile)) != NULL) {
        //LOG("Read %s\n", contigB);
        /* Parse the contig ID and the extension-depths */
        token = strtok_r(contigB, "\t", &aux);
        contigID = atol(token + 8);
        token = strtok_r(NULL, "\t", &aux);
        depthLeft = atoi(token);
        token = strtok_r(NULL, "\t", &aux);
        depthRight = atoi(token);

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }
        chompBuffer(contigBuffer);

        lengthLeft = (depthLeft == 0) ? 0  : strlen(contigB);

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }

        chompBuffer(contigBuffer);
        lengthOriginal = strlen(contigB);

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }

        chompBuffer(contigBuffer);
        lengthRight = (depthRight == 0) ? 0  : strlen(contigB);

        contigLen = lengthLeft + lengthOriginal + lengthRight;
        nMers = contigLen - kmerLength + 1;
        nMersInContigs += nMers;

        resetBuffer(contigBuffer);
    }

    GZIP_REWIND(contigFile);
    //LOG("Read contigFile. nMersInContigs: %lld\n", (lld) nMersInContigs);

    int64_t *prefixDepthSumArray = (int64_t *)malloc_chk(nMersInContigs * sizeof(int64_t));
    int64_t posInArray = 0, posInArrayPrevious;
    char *extendedContig = (char *)malloc_chk(MAX_CONTIG_SIZE * sizeof(char));
    int posInExtended;

    /* Store the prefix arrays in a very compact way */
    resetBuffer(contigBuffer);
    while ((contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile)) != NULL) {
        /* Parse the contig ID and the extension-depths */
        token = strtok_r(contigB, "\t", &aux);
        contigID = atol(token + 8);
        token = strtok_r(NULL, "\t", &aux);
        depthLeft = atoi(token);
        token = strtok_r(NULL, "\t", &aux);
        depthRight = atoi(token);

        posInExtended = 0;
        use_left = 0;
        use_right = 0;
        current_depth = 0.0;

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }

        chompBuffer(contigBuffer);
        lengthLeft = (depthLeft == 0) ? 0  : strlen(contigB);

        if (lengthLeft > 0) {
            memcpy(extendedContig, contigB, lengthLeft * sizeof(char));
            posInExtended += lengthLeft;
        }

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }

        chompBuffer(contigBuffer);
        lengthOriginal = strlen(contigB);

        memcpy(&extendedContig[posInExtended], contigB, lengthOriginal * sizeof(char));
        posInExtended += lengthOriginal;

        resetBuffer(contigBuffer);
        if (!(contigB = gzgetsBuffer(contigBuffer, MAX_CONTIG_SIZE, contigFile))) {
            break;
        }

        chompBuffer(contigBuffer);
        lengthRight = (depthRight == 0) ? 0  : strlen(contigB);

        if (lengthRight > 0) {
            memcpy(&extendedContig[posInExtended], contigB, lengthRight * sizeof(char));
            posInExtended += lengthRight;
        }

        extendedContig[posInExtended] = '\0';
        contigLen = lengthLeft + lengthOriginal + lengthRight;
        runningDepth = 0;

        posInArrayPrevious = posInArray;
        initOffset = 0;

        /* For each kmer in the contig extract the depth and add to the running sum */
        for (i = 0; i <= contigLen - kmerLength; i++) {
            if (i < lengthLeft) {
                /* Assign depth of left extension */
                if (i == 0) {
                    prefixDepthSumArray[posInArray] = depthLeft;
                } else {
                    prefixDepthSumArray[posInArray] = depthLeft + prefixDepthSumArray[posInArray - 1];
                }
                posInArray++;
            } else if (i <= lengthLeft + lengthOriginal - kmerLength) {
                /* Lookup kmer depth in the hash table */
                memcpy(curKmer, &extendedContig[i], kmerLength * sizeof(char));
                lookup_res = LOOKUP_LEAST_KMER_AND_COPY(dist_hashtable, curKmer, &copy, &is_least, kmer_len);
                if (i == 0) {
                    prefixDepthSumArray[posInArray] = copy.count;
                } else {
                    prefixDepthSumArray[posInArray] = copy.count + prefixDepthSumArray[posInArray - 1];
                }
                posInArray++;
                current_depth += 1.0 * copy.count;
            } else {
                /* Assign depth of right extension */
                if (i == 0) {
                    prefixDepthSumArray[posInArray] = depthRight;
                } else {
                    prefixDepthSumArray[posInArray] = depthRight + prefixDepthSumArray[posInArray - 1];
                }
                posInArray++;
            }
        }

        /* Write the extended contig to the new output file */
        GZIP_PRINTF(myExtendedOutputFile, ">Contig_%lld\n", (lld)contigID);
        GZIP_FWRITE(extendedContig, 1, strlen(extendedContig), myExtendedOutputFile);
        GZIP_PRINTF(myExtendedOutputFile, "\n");
        resetBuffer(contigBuffer);
    }
    //LOG("Stored prefixArrays\n");

    if (nMersInContigs) {
        GZIP_FWRITE(prefixDepthSumArray, 1, nMersInContigs * sizeof(int64_t), myOutputFile);
    }

    closeCheckpoint(contigFile);
    closeCheckpoint(myOutputFile);
    closeCheckpoint(myExtendedOutputFile);
    freeBuffer(contigBuffer); contigBuffer = NULL; contigB = NULL;

    UFXClose(UFX_f);


    if (prefixDepthSumArray) {
        free_chk(prefixDepthSumArray);
    }

    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);
    free_chk(curKmer);
    free_chk(extendedContig);

    upc_barrier;
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();
        depth_time = UPC_TICKS_TO_SECS(end - start);
        printf("\nTime for calculating the prefix mer-depth arrays : %f seconds\n", depth_time);
    }

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
