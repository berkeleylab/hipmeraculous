#include "spannerUtils.h"

int assignType(int type, char *resultStr)
{
    if (type == ANCHOR) {
        sprintf(resultStr, "ANCHOR");
    } else if (type == OUTGAP) {
        sprintf(resultStr, "OUTGAP");
    } else if (type == INTGAP) {
        sprintf(resultStr, "INTGAP");
    }

    return 0;
}

int assignStat(int stat, char *resultStr)
{
    if (stat == GAP) {
        sprintf(resultStr, "GAP");
    } else if (stat == INC) {
        sprintf(resultStr, "INC");
    } else if (stat == TRUNC) {
        sprintf(resultStr, "TRUNC");
    } else if (stat == FULL) {
        sprintf(resultStr, "FULL");
    }

    return 0;
}

int assignDir(int dir, char *resultStr)
{
    if (dir == IN) {
        sprintf(resultStr, "IN");
    } else if (dir == OUT) {
        sprintf(resultStr, "OUT");
    } else if (dir == MID) {
        sprintf(resultStr, "MID");
    }

    return 0;
}

/* Process a pair */
int spanner_processPair(align_info *lane1, align_info *lane2, GZIP_FILE outFD, int innieRemoval, int minEndSeparation, int srfInput, int64_t *uninf, int64_t *sing, int minMatch)
{
    int aligns1 = lane1->subjects_matched;
    int aligns2 = lane2->subjects_matched;
    int sStart1, sStart2, sStop1, sStop2, sLength1, sLength2, s1Outie, s1Innie, s2Outie, s2Innie, innieSep, outieSep, type1, type2, contigEnd1, contigEnd2;
    int64_t i, iMin1 = 0, iMax1 = 0, iMin2 = 0, iMax2 = 0;
    int64_t contig1, contig2;
    int min, max, strand1, strand2, startStat1, startStat2, stopStat1, stopStat2, dirStat1, dirStat2, innieMaxSeparation, qStart1, qStop1, qStart2, qStop2, qLength1, qLength2;
    char type1str[8];
    char type2str[8];
    char typestring[17];
    char contig1str[50];
    char contig2str[50];
    char alignInfo1string[200];
    char alignInfo2string[200];
    char startStat1string[20];
    char startStat2string[20];
    char stopStat1string[20];
    char stopStat2string[20];
    char dirStat1string[20];
    char dirStat2string[20];
    char subjectType[20];
    char strandString1[7];
    char strandString2[7];

    UPC_TICK_T start_io, end_io;


    if ((aligns1 == 0) || (aligns2 == 0)) {
        /* This read is a singleton */
        (*sing)++;
        return 0;
    }

    
    /* Find the alignments with the "earlier starts" */
    min = lane1->aligned_query_locs[0];
    max = lane1->aligned_query_locs[0];
    for (i = 1; i < aligns1; i++) {
      if (lane1->aligned_query_locs[2 * i] < min) {
	min = lane1->aligned_query_locs[2 * i];
	iMin1 = i;
      }
      if (srfInput) {  // keep track of the farthest start too
	if (lane1->aligned_query_locs[2 * i] > max) {
	  max = lane1->aligned_query_locs[2 * i];
	  iMax1 = i;
	}  
      }
    }
      
    if (srfInput && max != min) {
	int earliestScaffId = lane1->aligned_subject_ids[iMin1];
	int earliestQStart = lane1->aligned_query_locs[2 * iMin1];
	int earliestSStart = lane1->aligned_subject_locs[2 * iMin1];  
	int earliestScafLen = lane1->aligned_subject_lengths[iMin1];
	int farScaffId =lane1->aligned_subject_ids[iMax1];
	int farQStart =lane1->aligned_query_locs[2 * iMax1];
	if (farScaffId != earliestScaffId &&
	    (farQStart - earliestQStart <=  (earliestScafLen-minMatch+1)-earliestSStart)) {
	      // farthest alignment is to different scaffold but within current scaffold's alignable range  -reject
	      (*uninf)++;
	      return 0;
	}
    }


    min = lane2->aligned_query_locs[0];
    max = lane2->aligned_query_locs[0];
    for (i = 1; i < aligns2; i++) {
        if (lane2->aligned_query_locs[2 * i] < min) {
            min = lane2->aligned_query_locs[2 * i];
            iMin2 = i;
        }
	if (srfInput) {  // keep track of the farthest start too
	  if (lane2->aligned_query_locs[2 * i] > max) {
	    max = lane2->aligned_query_locs[2 * i];
	    iMax2 = i;
	  }  
	}
    }

    if (srfInput && max != min) { 
	int earliestScaffId = lane2->aligned_subject_ids[iMin2];
	int earliestQStart = lane2->aligned_query_locs[2 * iMin2];
	int earliestSStart = lane2->aligned_subject_locs[2 * iMin2];  
	int earliestScafLen = lane2->aligned_subject_lengths[iMin2];
	int farScaffId =lane2->aligned_subject_ids[iMax2];
	int farQStart =lane2->aligned_query_locs[2 * iMax2];
	if (farScaffId != earliestScaffId &&
	    (farQStart - earliestQStart <=  (earliestScafLen-minMatch+1)-earliestSStart)) {
	      // downstream-most alignment is to a different scaffold but within the current scaffold's alignable range  -reject
	      (*uninf)++;
	      return 0;
	}
    }





    
    contig1 = lane1->aligned_subject_ids[iMin1];
    strand1 = lane1->aligned_strand[iMin1];
    qStart1 = lane1->aligned_query_locs[2 * iMin1];
    qStop1 = lane1->aligned_query_locs[2 * iMin1 + 1];
    qLength1 = lane1->query_length;
    contig2 = lane2->aligned_subject_ids[iMin2];
    strand2 = lane2->aligned_strand[iMin2];
    qStart2 = lane2->aligned_query_locs[2 * iMin2];
    qStop2 = lane2->aligned_query_locs[2 * iMin2 + 1];
    qLength2 = lane2->query_length;
    sStart1 = lane1->aligned_subject_locs[2 * iMin1];
    sStop1 = lane1->aligned_subject_locs[2 * iMin1 + 1];
    sLength1 = lane1->aligned_subject_lengths[iMin1];
    sStart2 = lane2->aligned_subject_locs[2 * iMin2];
    sStop2 = lane2->aligned_subject_locs[2 * iMin2 + 1];
    sLength2 = lane2->aligned_subject_lengths[iMin2];

    if (contig1 == contig2) {
        /* Uninformative */
        (*uninf)++;
        return 0;
    }

    if (innieRemoval) {
        innieMaxSeparation = 800;
        s1Innie = (strand1 == PLUS) ? sStop1 : sLength1 - sStart1 + 1;
        s2Innie = (strand2 == PLUS) ? sStop2 : sLength2 - sStart2 + 1;
        innieSep = s1Innie + s2Innie;
        if (innieSep < innieMaxSeparation) {
            /* Potential innie */
            return 0;
        }
    }

    if (minEndSeparation != 0) {
        s1Outie = (strand1 == PLUS) ? sLength1 - sStart1 + 1 : sStop1;
        s2Outie = (strand2 == PLUS) ? sLength2 - sStart2 + 1 : sStop2;
        outieSep = s1Outie + s2Outie;
        if (outieSep < minEndSeparation) {
            /* Potential shorty */
            return 0;
        }
    }

    startStat1 = lane1->startStatus[iMin1];
    stopStat1 = lane1->endStatus[iMin1];
    dirStat1 = lane1->location[iMin1];
    startStat2 = lane2->startStatus[iMin2];
    stopStat2 = lane2->endStatus[iMin2];
    dirStat2 = lane2->location[iMin2];

    type1 = ANCHOR;
    if (startStat1 == GAP) {
        type1 = OUTGAP;
    } else if (stopStat1 == GAP) {
        type1 = INTGAP;
    }

    type2 = ANCHOR;
    if (startStat2 == GAP) {
        type2 = OUTGAP;
    } else if (stopStat2 == GAP) {
        type2 = INTGAP;
    }

    contigEnd1 = (strand1 == PLUS) ? 3 : 5;
    contigEnd2 = (strand2 == PLUS) ? 3 : 5;

    /* Print PAIR string */
    if (srfInput) {
        sprintf(subjectType, "Scaffold");
    } else {
        sprintf(subjectType, "Contig");
    }
    assignType(type1, type1str);
    assignType(type2, type2str);
    sprintf(typestring, "%s.%s", type1str, type2str);
    sprintf(contig1str, "%s%lld.%d", subjectType, (lld)contig1, contigEnd1);
    sprintf(contig2str, "%s%lld.%d", subjectType, (lld)contig2, contigEnd2);
    assignStat(startStat1, startStat1string);
    assignStat(startStat2, startStat2string);
    assignStat(stopStat1, stopStat1string);
    assignStat(stopStat2, stopStat2string);
    assignDir(dirStat1, dirStat1string);
    assignDir(dirStat2, dirStat2string);
    if (strand1 == PLUS) {
        sprintf(strandString1, "Plus");
    } else {
        sprintf(strandString1, "Minus");
    }
    if (strand2 == PLUS) {
        sprintf(strandString2, "Plus");
    } else {
        sprintf(strandString2, "Minus");
    }
    sprintf(alignInfo1string, "[%s.%s.%s %s %d %d %d %s%lld %d %d %d %s]", startStat1string, stopStat1string, dirStat1string, lane1->query_name, qStart1, qStop1, qLength1, subjectType, (lld)contig1, sStart1, sStop1, sLength1, strandString1);
    sprintf(alignInfo2string, "[%s.%s.%s %s %d %d %d %s%lld %d %d %d %s]", startStat2string, stopStat2string, dirStat2string, lane2->query_name, qStart2, qStop2, qLength2, subjectType, (lld)contig2, sStart2, sStop2, sLength2, strandString2);


    start_io = UPC_TICKS_NOW();

    GZIP_PRINTF(outFD, "PAIR\t%s\t%s\t%s\t%s\t%s\n", typestring, contig1str, alignInfo1string, contig2str, alignInfo2string);

    end_io = UPC_TICKS_NOW();
    write_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

    return 1;
}

/* Convert to scaffold coordinate system */
int convertToScaffoldCoordinates(int qStart, int qStop, int qLength, int64_t *subject, int *strand, int *sStart, int *sStop, int *sLength, int *projectedStart, int *projectedEnd, shared[1] contigScaffoldMap_t *contigScaffoldMap, shared[1] int64_t *scaffLengths)
{
    int64_t scaffID;
    int scaffLen, contigScaffStart, contigScaffEnd, contigScaffStrand, scaffStrand, scaffStart, scaffStop;
    contigScaffoldMap_t contigScaffoldMapEntry = contigScaffoldMap[(*subject)];
    int unalignedStart = qStart - 1;
    int unalignedEnd = qLength - qStop;


    if (contigScaffoldMapEntry.scaffID == UNDEFINED) {
        return FAIL;
    } else {
        contigScaffStrand = contigScaffoldMapEntry.cStrand;
        scaffID = contigScaffoldMapEntry.scaffID;
        contigScaffStart = contigScaffoldMapEntry.sStart;
        contigScaffEnd = contigScaffoldMapEntry.sEnd;
        scaffLen = scaffLengths[scaffID];
        if (scaffLen == 0) {
            return FAIL;
        }
        scaffStrand = (contigScaffStrand == (*strand)) ? PLUS : MINUS;
        scaffStart = (contigScaffStrand == PLUS) ? contigScaffStart + (*sStart) - 1 : contigScaffEnd - (*sStop) + 1;
        scaffStop = (contigScaffStrand == PLUS) ? contigScaffStart + (*sStop) - 1 : contigScaffEnd - (*sStart) + 1;

        (*subject) = scaffID;
        (*sLength) = scaffLen;
        (*sStart) = scaffStart;
        (*strand) = scaffStrand;
        (*sStop) = scaffStop;
        (*projectedStart) = ((*strand) == PLUS) ? (*sStart) - unalignedStart : (*sStop) + unalignedStart;
        (*projectedEnd) = ((*strand) == PLUS) ? (*sStop) + unalignedEnd :  (*sStart) - unalignedEnd;

        return SUCCESS;
    }
}

/* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
int assessLocationAlignment(int strand, int projectedStart, int projectedEnd, int sLength, int endDistance, int *locationRes, int64_t *uninf)
{
    int location;

    if (strand == PLUS) {
        if (projectedStart > (sLength - endDistance)) {
            location = OUT;
        } else if (projectedEnd < endDistance) {
            location = IN;
        } else {
            location = MID;
        }
    } else {
        if (projectedStart < endDistance) {
            location = OUT;
        } else if (projectedEnd > (sLength - endDistance)) {
            location = IN;
        } else {
            location = MID;
        }
    }

    if (location != OUT) {
        (*uninf)++;
        return FAIL;
    } else {
        (*locationRes) = location;
        return SUCCESS;
    }
}

/* Assess alignment for completeness */
int assessAlignment(int strand, int qStart, int qStop, int qLength, int sStart, int sStop, int sLength, int fivePrimeWiggleRoomIn, int threePrimeWiggleRoomIn, int truncate, int *projectedStartRes, int *projectedEndRes, int *startStatusRes, int *endStatusRes, int64_t *five, int64_t *three)
{
    int fivePrimeWiggleRoom = fivePrimeWiggleRoomIn;
    int threePrimeWiggleRoom = threePrimeWiggleRoomIn;
    int missingStartBases, startStatus, unalignedEnd, projectedEnd, endStatus, missingEndBases;

    int unalignedStart = qStart - 1;
    int projectedStart = (strand == PLUS) ? sStart - unalignedStart : sStop + unalignedStart;
    int projectedOff = 0;

    if (projectedStart < 1) {
        projectedOff = 1 - projectedStart;
    } else if (projectedStart > sLength) {
        projectedOff = projectedStart - sLength;
    }
    missingStartBases = unalignedStart - projectedOff;

    if (unalignedStart == 0) {
        startStatus = FULL;
    } else if ((projectedOff > 0) && (missingStartBases < fivePrimeWiggleRoom)) {
        startStatus = GAP;
    } else if ((unalignedStart < fivePrimeWiggleRoom) || (truncate == 5)) {
        startStatus = INC;
    } else {
        (*five)++;
        return FAIL;
    }

    unalignedEnd = qLength - qStop;
    projectedEnd = (strand == PLUS) ? sStop + unalignedEnd : sStart - unalignedEnd;
    projectedOff = 0;
    if (projectedEnd < 1) {
        projectedOff = 1 - projectedEnd;
    } else if (projectedEnd > sLength) {
        projectedOff = projectedEnd - sLength;
    }
    missingEndBases = unalignedEnd - projectedOff;
    if (unalignedEnd == 0) {
        endStatus = FULL;
    } else if ((projectedOff > 0) && (missingEndBases < threePrimeWiggleRoom)) {
        endStatus = GAP;
    } else if ((unalignedEnd < threePrimeWiggleRoom) || (truncate == 3)) {
        endStatus = INC;
    } else {
        (*three)++;
        return FAIL;
    }

    (*startStatusRes) = startStatus;
    (*endStatusRes) = endStatus;
    (*projectedStartRes) = projectedStart;
    (*projectedEndRes) = projectedEnd;

    return SUCCESS;
}

int assignStrings(int startStatus, int endStatus, int location, char **startStatus_ptr, char **endStatus_ptr, char **location_ptr, char *inc, char *gap, char *trunc, char *ins, char *outs, char *mid, char *full)
{
    if (startStatus == FULL) {
        (*startStatus_ptr) = full;
    } else if (startStatus == INC) {
        (*startStatus_ptr) = inc;
    } else if (startStatus == GAP) {
        (*startStatus_ptr) = gap;
    } else if (startStatus == TRUNC) {
        (*startStatus_ptr) = trunc;
    }

    if (endStatus == FULL) {
        (*endStatus_ptr) = full;
    } else if (endStatus == INC) {
        (*endStatus_ptr) = inc;
    } else if (endStatus == GAP) {
        (*endStatus_ptr) = gap;
    } else if (endStatus == TRUNC) {
        (*endStatus_ptr) = trunc;
    }

    if (location == IN) {
        (*location_ptr) = ins;
    } else if (location == OUT) {
        (*location_ptr) = outs;
    } else if (location == MID) {
        (*location_ptr) = mid;
    }

    return 0;
}

static char *sgets(char *str, int num, char **input)
{
    char *next = *input;
    int numread = 0;
    int strpos = 0;

    while (numread + 1 < num && *next) {
        int isnewline = (*next == '\n');

        str[strpos] = *next++;
        strpos++;
        numread++;
        // newline terminates the line but is included
        if (isnewline) {
            break;
        }
    }

    if (numread == 0) {
        return NULL; // "eof"
    }
    // must have hit the null terminator or end of line
    str[strpos] = '\0'; // null terminate this tring
    // set up input for next call
    *input = next;
    return str;
}
