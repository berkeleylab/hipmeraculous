#include "spanner.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("spanner");
    return spanner_main(argc, argv);
}
