#include "optlist.h"
#include "spanner.h"

double write_io_time = 0.0;

#include "spannerUtils.h"


//#define FIND_DENSITY

static shared int64_t singletons = 0;
static shared int64_t fiveRejects = 0;
static shared int64_t threeRejects = 0;
static shared int64_t uninformatives = 0;
static shared int64_t tot_excessiveAlns = 0;
static shared int64_t tot_excludedOnDemand = 0;




int spanner_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    write_io_time = 0;

    if (!MYTHREAD) {
        singletons = 0;
        fiveRejects = 0;
        threeRejects = 0;
        uninformatives = 0;
        tot_excessiveAlns = 0;
        tot_excludedOnDemand = 0;
    }
    upc_barrier;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "l:i:s:r:m:S:ARF:T:U:D:C:Z:B:a:X:L:");
    print_args(optList, __func__);
    int reverseComplement = 0, innieRemoval = 0, minEndSeparation = 0, minFreqReported = 10, fivePrimeWiggleRoom = 5, threePrimeWiggleRoom = 5, truncate = 0, minMatch = 0, insertSize = 0, insertSigma = 0, readLength = 0, moreAlignmentsExist, scaffoldRound = 0;
    char outputMerAligner[MAX_FILE_PATH];
    char *libname = NULL;
    GZIP_FILE laneFD1;
    GZIP_FILE laneFD2;
    GZIP_FILE outFD;
    GZIP_FILE srfFD;
    int64_t i, j, k;
    align_info *result1, *result2;
    result1 = (align_info *)calloc_chk(sizeof(align_info), 1);
    result2 = (align_info *)calloc_chk(sizeof(align_info), 1);
    char *lineBuffers = (char *)calloc_chk(MAX_LINE_SIZE * 12, 1);
    char *alignmentBuffer1 = lineBuffers;
    char *copyAlignmentBuffer1 = alignmentBuffer1 + MAX_LINE_SIZE;
    char *firstAlignmentBuffer1 = copyAlignmentBuffer1 + MAX_LINE_SIZE;
    char *alignmentBuffer2 = firstAlignmentBuffer1 + MAX_LINE_SIZE;
    char *copyAlignmentBuffer2 = alignmentBuffer2 + MAX_LINE_SIZE;
    char *firstAlignmentBuffer2 = copyAlignmentBuffer2 + MAX_LINE_SIZE;
    char *readIdLane1 = firstAlignmentBuffer2 + MAX_LINE_SIZE;
    char *newReadIdLane1 = readIdLane1 + MAX_LINE_SIZE;
    char *readIdLane2 = newReadIdLane1 + MAX_LINE_SIZE;
    char *newReadIdLane2 = readIdLane2 + MAX_LINE_SIZE;
    char *srfLine = newReadIdLane2 + MAX_LINE_SIZE;
#ifdef MERGE
    char *aux = srfLine + MAX_LINE_SIZE;
    FILE *mergedFD = NULL;
#endif
    char *resRead1, *resRead2;
    int validAlign1, validAlign2;
    char *srfSuffixName = NULL;
    char srfFileName[MAX_FILE_PATH];
    UPC_TICK_T start, end;
    int shortPair = 600, trimOff;
    int subjectID;
    int count, endDistance;
    int64_t totalContigs = 0, totalScaffolds = 0, pairsProcessed = 0;
    shared[1] contigScaffoldMap_t * contigScaffoldMap = NULL;
    contigScaffoldMap_t contigScaffoldMapEntry;
    shared[1] int64_t * scaffLengths = NULL;
    int64_t scaffoldId, contigId;
    int splitRes, cStrand, sStart, sEnd;
    int srfInput = 0;
    int64_t subject1, subject2;
    int projectedEnd1, projectedStart1, startStatus1, endStatus1, location1, simplePos, evenPos, oddPos;
    int qStart1, qStop1, qLength1, sStart1, sStop1, sLength1, strand1;
    int qStart2, qStop2, qLength2, sStart2, sStop2, sLength2, strand2;
    int projectedEnd2, projectedStart2, startStatus2, endStatus2, location2;
    char outputSpanner[MAX_FILE_PATH];
    int64_t singleton = 0, uninformative = 0, threeReject = 0, fiveReject = 0, my_in_excludeList = 0;
    int swap_var;
    char *merAlignerOutput = NULL;
    char *excludeListPrefix = NULL;
    char excludeListFname[MAX_FILE_PATH];
    double read_io_time = 0.0;
    UPC_TICK_T start_io, end_io;

    const char *base_dir = ".";

#ifdef MERGE
    {
        char myname[MAX_FILE_PATH];
        sprintf(myname, "NEWmerged");
        mergedFD = openCheckpoint0(myname, "w");
    }
#endif


    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'l':
            libname = thisOpt->argument;
            break;
        case 'S':
            /* FIXME: Standardize srf file output name conventions */
            srfInput = 1;
            srfSuffixName = thisOpt->argument;
            break;
        case 'U':
            truncate = atoi(thisOpt->argument);
            break;
        case 'T':
            threePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 'D':
            minEndSeparation = atoi(thisOpt->argument);
            break;
        case 'F':
            fivePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 'm':
            minMatch = atoi(thisOpt->argument);
            break;
        case 'i':
            insertSize = atoi(thisOpt->argument);
            break;
        case 's':
            insertSigma = atoi(thisOpt->argument);
            break;
        case 'r':
            readLength = atoi(thisOpt->argument);
            break;
        case 'A':
            innieRemoval = 1;
            break;
        case 'C':
            totalContigs = atol(thisOpt->argument);
            break;
        case 'Z':
            totalScaffolds = atol(thisOpt->argument);
            break;
        case 'R':
            reverseComplement = 1;
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'a':
            merAlignerOutput = thisOpt->argument;
            break;
        case 'X':
            excludeListPrefix = thisOpt->argument;
            break;
        case 'L':
            scaffoldRound = atoi(thisOpt->argument);
            break;
        default:
            break;
        }
    }

    
    /* Auxiliary data structures to find density of mapped reads per contig */

    int64_t *arrayOfMappedPairCounts = NULL;
    int64_t *arrayOfContigLengths = NULL;
    int64_t commonContig = 0;

#ifdef FIND_DENSITY
    arrayOfMappedPairCounts = (int64_t *)calloc_chk(totalContigs, sizeof(int64_t));
    arrayOfContigLengths = (int64_t *)calloc_chk(totalContigs, sizeof(int64_t));

    shared[1] int64_t * sharedArrayOfMappedPairCounts = NULL;
    UPC_ALL_ALLOC_CHK(sharedArrayOfMappedPairCounts, totalContigs, sizeof(int64_t));

    shared[1] int64_t * sharedArrayOfContigLengths = NULL;
    UPC_ALL_ALLOC_CHK(sharedArrayOfContigLengths, totalContigs, sizeof(int64_t));

    for (int z = MYTHREAD; z < totalContigs; z += THREADS) {
        sharedArrayOfMappedPairCounts[z] = 0;
        sharedArrayOfContigLengths[z] = 0;
    }

    upc_barrier;
#endif

    if (libname == NULL) {
        SDIE("You must specify a -l libname!\n");
    }
    if (srfInput) {
        if (!srfSuffixName) {
            SDIE("You must specify a -S srfSuffixName\n");
        }
        sprintf(srfFileName, "%s" GZIP_EXT, srfSuffixName);
        srfFD = openCheckpoint(srfFileName, "r");
        if (!totalScaffolds || !totalContigs) {
            SDIE("You must specify  -C totalContigs  AND  -Z totalScaffolds\n");
        }
    }
    sprintf(outputMerAligner, "%s-%s_Read1" GZIP_EXT, libname, merAlignerOutput);
    laneFD1 = openCheckpoint(outputMerAligner, "r");

    sprintf(outputMerAligner, "%s-%s_Read2" GZIP_EXT, libname, merAlignerOutput);
    laneFD2 = openCheckpoint(outputMerAligner, "r");

    sprintf(outputSpanner, "%s-spans-%d" GZIP_EXT, libname, scaffoldRound);
    outFD = openCheckpoint(outputSpanner, "w");



    /* Some contigs may need to be excluded from consideration apriori */


    shared[1] int *excludeContigs = NULL;
    if (excludeListPrefix) {

      /* Allocating for  the global total num contigs is an overkill.  TODO: replace with dynamic lookup structure
	 containing just the contigs from a TOTAL combined input exclude list
       */
      UPC_ALL_ALLOC_CHK(excludeContigs, totalContigs, sizeof(int));
      if (excludeContigs == NULL)  {
	DIE("Could not allocate %lld contigs!\n", (lld)totalContigs);
      }

      for (i = MYTHREAD; i < totalContigs; i += THREADS) {
	excludeContigs[i]=0;
      }
      upc_barrier;
      sprintf(excludeListFname, "%s/%s", base_dir, excludeListPrefix);
      FILE *XFD = fopen_rank_path(excludeListFname, "r", MYTHREAD);
      char cid[100];
      while (fgets(cid, 100, XFD) != NULL) {
	int64_t id = atol(cid+6); 
	assert(id < totalContigs);  // expect renumbered contigs where max id == total contigs -1
	excludeContigs[id]=1;
      }
      fclose_track(XFD);
    }


    endDistance = insertSize + 3 * insertSigma;
    char *fgets_result;
    upc_barrier;
    start = UPC_TICKS_NOW();

    /******************************/
    /*  Read scaffold report file */
    /******************************/
    if (srfInput) {
        /* Build scaffLengths data structure */
        if (MYTHREAD == 0) {
            printf("all allocating %lld + %lld\n", (lld)totalScaffolds * sizeof(int64_t), (lld)totalContigs * sizeof(contigScaffoldMap_t));
        }
        UPC_ALL_ALLOC_CHK(scaffLengths, totalScaffolds + 1, sizeof(int64_t));
        if (scaffLengths == NULL) {
            DIE("Could not allocate %lld scaffolds!\n", (lld)totalScaffolds);
        }
        for (i = MYTHREAD; i < totalScaffolds; i += THREADS) {
            scaffLengths[i] = 0;
        }

        /* Build contigScaffoldMap data structure */
        UPC_ALL_ALLOC_CHK(contigScaffoldMap, totalContigs + 1, sizeof(contigScaffoldMap_t));
        if (contigScaffoldMap == NULL) {
            DIE("Could not allocate %lld totalContigs!\n", (lld)totalContigs);
        }
        for (i = MYTHREAD; i < totalContigs; i += THREADS) {
            contigScaffoldMap[i].scaffID = UNDEFINED;
        }

        upc_barrier;

        start_io = UPC_TICKS_NOW();
        fgets_result = GZIP_GETS(srfLine, MAX_LINE_SIZE, srfFD);
        end_io = UPC_TICKS_NOW();
        read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

        while (fgets_result != NULL) {
            assert(fgets_result[strlen(fgets_result) - 1] == '\n');
            splitRes = splitSrfLine(srfLine, &scaffoldId, &contigId, &sStart, &sEnd, &cStrand);
            if (splitRes == SUCCESS) {
                /* Update scaffsLength array */
                if (scaffLengths[scaffoldId] == 0) {
                    scaffLengths[scaffoldId] = sEnd;
                } else {
                    if (sEnd > scaffLengths[scaffoldId]) {
                        scaffLengths[scaffoldId] = sEnd;
                    }
                }

                /* Update contigScaffoldMap data structure */
                contigScaffoldMapEntry.cStrand = cStrand;
                contigScaffoldMapEntry.scaffID = scaffoldId;
                contigScaffoldMapEntry.sStart = sStart;
                contigScaffoldMapEntry.sEnd = sEnd;
                contigScaffoldMap[contigId] = contigScaffoldMapEntry;
            }


            start_io = UPC_TICKS_NOW();

            fgets_result = GZIP_GETS(srfLine, MAX_LINE_SIZE, srfFD);

            end_io = UPC_TICKS_NOW();
            read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);
        }

        closeCheckpoint(srfFD);
    }

    upc_barrier;

    /***************************/
    /* Read merAligner outputs */
    /***************************/
    start_io = UPC_TICKS_NOW();

    int64_t excessiveAlns = 0;

    resRead1 = GZIP_GETS(firstAlignmentBuffer1, MAX_LINE_SIZE, laneFD1);
    resRead2 = GZIP_GETS(firstAlignmentBuffer2, MAX_LINE_SIZE, laneFD2);

    end_io = UPC_TICKS_NOW();
    read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);
    moreAlignmentsExist = 1;

    while (moreAlignmentsExist) {
        if ((resRead1 == NULL) && (resRead2 == NULL)) {
            break;
        }

        /* Parse the first alignment of read (lane 1) */
        if (resRead1 != NULL) {
            assert(resRead1[strlen(resRead1) - 1] == '\n');
            /* Split alignment and check for guard values */
#ifdef MERGE
            fprintf(mergedFD, "%s", firstAlignmentBuffer1);
#endif

            validAlign1 = splitAlignment(firstAlignmentBuffer1, readIdLane1, &qStart1, &qStop1, &qLength1, &subject1, &sStart1, &sStop1, &sLength1, &strand1);

            if (validAlign1 == SUCCESS && excludeListPrefix) {
	      assert(subject1 < totalContigs);
	      assert(excludeContigs != NULL);
	      if (excludeContigs[subject1]) {
                    my_in_excludeList++;
                    validAlign1 = FAIL;
                }
            }

#ifdef FIND_DENSITY
            if (sLength1) {
                arrayOfContigLengths[subject1] = sLength1;
            }
#endif


            /* Assess alignment for completeness (do this before scaffold coordinate conversion!) */
            if (validAlign1 == SUCCESS) {
                validAlign1 = assessAlignment(strand1, qStart1, qStop1, qLength1, sStart1, sStop1, sLength1, fivePrimeWiggleRoom, threePrimeWiggleRoom, truncate, &projectedStart1, &projectedEnd1, &startStatus1, &endStatus1, &fiveReject, &threeReject);
            }

            /* Reorient alignment if requested */
            if (validAlign1 == SUCCESS) {
                if (reverseComplement) {
                    strand1 = (strand1 == PLUS) ? MINUS : PLUS;
                    swap_var = qStart1;
                    qStart1 = qLength1 - qStop1 + 1;
                    qStop1 = qLength1 - swap_var + 1;
                }
            }

            /* Convert to scaffold coordinate system if srfFile is specified */
            if (validAlign1 == SUCCESS) {
                if (srfInput) {
                    validAlign1 = convertToScaffoldCoordinates(qStart1, qStop1, qLength1, &subject1, &strand1, &sStart1, &sStop1, &sLength1, &projectedStart1, &projectedEnd1, contigScaffoldMap, scaffLengths);
                }
            }

            /* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
            if (validAlign1 == SUCCESS) {
                validAlign1 = assessLocationAlignment(strand1, projectedStart1, projectedEnd1, sLength1, endDistance, &location1, &uninformative);
            }

            /* Store alignment in data structure result1 */
            if (validAlign1 == SUCCESS) {
                result1->subjects_matched = 0;
                simplePos = result1->subjects_matched;
                if (simplePos < MAX_ALIGN) {
                    evenPos = 2 * simplePos;
                    oddPos = evenPos + 1;
                    result1->query_name = readIdLane1;
                    result1->query_length = qLength1;
                    result1->aligned_query_locs[evenPos] = qStart1;
                    result1->aligned_query_locs[oddPos] = qStop1;
                    result1->aligned_subject_ids[simplePos] = subject1;
                    result1->aligned_subject_locs[evenPos] = sStart1;
                    result1->aligned_subject_locs[oddPos] = sStop1;
                    result1->aligned_strand[simplePos] = strand1;
                    result1->startStatus[simplePos] = startStatus1;
                    result1->endStatus[simplePos] = endStatus1;
                    result1->location[simplePos] = location1;
                    result1->aligned_subject_lengths[simplePos] = sLength1;
                    result1->subjects_matched = 1;
                } else {
                    excessiveAlns++;
                }
            } else {
                result1->subjects_matched = 0;
            }
        }

        /* Read the the rest alignments of read (lane 1) */
        if (resRead1 != NULL) {
            start_io = UPC_TICKS_NOW();
            resRead1 = GZIP_GETS(alignmentBuffer1, MAX_LINE_SIZE, laneFD1);
            end_io = UPC_TICKS_NOW();
            read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

            memcpy(copyAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
            if (resRead1 != NULL) {
                assert(resRead1[strlen(resRead1) - 1] == '\n');

#ifdef MERGE
                memcpy(aux, copyAlignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
#endif
                /* Split alignment and check for guard values */
                validAlign1 = splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &qStart1, &qStop1, &qLength1, &subject1, &sStart1, &sStop1, &sLength1, &strand1);

#ifdef FIND_DENSITY
                if (sLength1) {
                    arrayOfContigLengths[subject1] = sLength1;
                }
#endif

                while (strcmp(readIdLane1, newReadIdLane1) == 0) {
#ifdef MERGE
                    fprintf(mergedFD, "%s", aux);
#endif

		    if (validAlign1 == SUCCESS && excludeListPrefix) {
		      assert(subject1 <= totalContigs);
		      if (excludeContigs[subject1]){
                            my_in_excludeList++;
                            validAlign1 = FAIL;
                        }
                    }


                    /* Assess alignment for completeness (do this before scaffold coordinate conversion!) */
                    if (validAlign1 == SUCCESS) {
                        validAlign1 = assessAlignment(strand1, qStart1, qStop1, qLength1, sStart1, sStop1, sLength1, fivePrimeWiggleRoom, threePrimeWiggleRoom, truncate, &projectedStart1, &projectedEnd1, &startStatus1, &endStatus1, &fiveReject, &threeReject);
                    }

                    /* Reorient alignment if requested */
                    if (validAlign1 == SUCCESS) {
                        if (reverseComplement) {
                            strand1 = (strand1 == PLUS) ? MINUS : PLUS;
                            swap_var = qStart1;
                            qStart1 = qLength1 - qStop1 + 1;
                            qStop1 = qLength1 - swap_var + 1;
                        }
                    }

                    /* Convert to scaffold coordinate system if srfFile is specified */
                    if (validAlign1 == SUCCESS) {
                        if (srfInput) {
                            validAlign1 = convertToScaffoldCoordinates(qStart1, qStop1, qLength1, &subject1, &strand1, &sStart1, &sStop1, &sLength1, &projectedStart1, &projectedEnd1, contigScaffoldMap, scaffLengths);
                        }
                    }

                    /* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
                    if (validAlign1 == SUCCESS) {
                        validAlign1 = assessLocationAlignment(strand1, projectedStart1, projectedEnd1, sLength1, endDistance, &location1, &uninformative);
                    }

                    /* Store alignment in data structure result1 */
                    if (validAlign1 == SUCCESS) {
                        simplePos = result1->subjects_matched;
                        if (simplePos < MAX_ALIGN) {
                            evenPos = 2 * simplePos;
                            oddPos = evenPos + 1;
                            result1->query_name = readIdLane1;
                            result1->query_length = qLength1;
                            result1->aligned_query_locs[evenPos] = qStart1;
                            result1->aligned_query_locs[oddPos] = qStop1;
                            result1->aligned_subject_ids[simplePos] = subject1;
                            result1->aligned_subject_locs[evenPos] = sStart1;
                            result1->aligned_subject_locs[oddPos] = sStop1;
                            result1->aligned_strand[simplePos] = strand1;
                            result1->aligned_subject_lengths[simplePos] = sLength1;
                            result1->startStatus[simplePos] = startStatus1;
                            result1->endStatus[simplePos] = endStatus1;
                            result1->location[simplePos] = location1;
                            result1->subjects_matched += 1;
                        } else {
                            excessiveAlns++;
                        }
                    }

                    start_io = UPC_TICKS_NOW();

                    resRead1 = GZIP_GETS(alignmentBuffer1, MAX_LINE_SIZE, laneFD1);

                    end_io = UPC_TICKS_NOW();
                    read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

                    memcpy(copyAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
                    if (resRead1 == NULL) {
                        break;
                    }
                    assert(resRead1[strlen(resRead1) - 1] == '\n');
                    validAlign1 = splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &qStart1, &qStop1, &qLength1, &subject1, &sStart1, &sStop1, &sLength1, &strand1);
#ifdef FIND_DENSITY
                    if (sLength1) {
                        arrayOfContigLengths[subject1] = sLength1;
                    }
#endif
                }

                if (resRead1 != NULL) {
                    memcpy(firstAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
                    //strcpy(readIdLane1, newReadIdLane1);
                }
            }
        }

        /* Parse the first alignment of read (lane 2) */
        if (resRead2 != NULL) {
            assert(resRead2[strlen(resRead2) - 1] == '\n');
            /* Split alignment and check for guard values */
#ifdef MERGE
            fprintf(mergedFD, "%s", firstAlignmentBuffer2);
#endif
            validAlign2 = splitAlignment(firstAlignmentBuffer2, readIdLane2, &qStart2, &qStop2, &qLength2, &subject2, &sStart2, &sStop2, &sLength2, &strand2);

            if (validAlign2 == SUCCESS && excludeListPrefix) {
	      assert(subject2 <= totalContigs);
	      if (excludeContigs[subject2])  {
                    my_in_excludeList++;
                    validAlign2 = FAIL;
                }
            }

#ifdef FIND_DENSITY
            if (sLength2) {
                arrayOfContigLengths[subject2] = sLength2;
            }
#endif

            /* Assess alignment for completeness (do this before scaffold coordinate conversion!) */
            if (validAlign2 == SUCCESS) {
                validAlign2 = assessAlignment(strand2, qStart2, qStop2, qLength2, sStart2, sStop2, sLength2, fivePrimeWiggleRoom, threePrimeWiggleRoom, truncate, &projectedStart2, &projectedEnd2, &startStatus2, &endStatus2, &fiveReject, &threeReject);
            }

            /* Reorient alignment if requested */
            if (validAlign2 == SUCCESS) {
                if (reverseComplement) {
                    strand2 = (strand2 == PLUS) ? MINUS : PLUS;
                    swap_var = qStart2;
                    qStart2 = qLength2 - qStop2 + 1;
                    qStop2 = qLength2 - swap_var + 1;
                }
            }

            /* Convert to scaffold coordinate system if srfFile is specified */
            if (validAlign2 == SUCCESS) {
                if (srfInput) {
                    validAlign2 = convertToScaffoldCoordinates(qStart2, qStop2, qLength2, &subject2, &strand2, &sStart2, &sStop2, &sLength2, &projectedStart2, &projectedEnd2, contigScaffoldMap, scaffLengths);
                }
            }

            /* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
            if (validAlign2 == SUCCESS) {
                validAlign2 = assessLocationAlignment(strand2, projectedStart2, projectedEnd2, sLength2, endDistance, &location2, &uninformative);
            }

            /* Store alignment in data structure result2 */
            if (validAlign2 == SUCCESS) {
                result2->subjects_matched = 0;
                simplePos = result2->subjects_matched;
                if (simplePos < MAX_ALIGN) {
                    evenPos = 2 * simplePos;
                    oddPos = evenPos + 1;
                    result2->query_name = readIdLane2;
                    result2->query_length = qLength2;
                    result2->aligned_query_locs[evenPos] = qStart2;
                    result2->aligned_query_locs[oddPos] = qStop2;
                    result2->aligned_subject_ids[simplePos] = subject2;
                    result2->aligned_subject_locs[evenPos] = sStart2;
                    result2->aligned_subject_locs[oddPos] = sStop2;
                    result2->aligned_strand[simplePos] = strand2;
                    result2->startStatus[simplePos] = startStatus2;
                    result2->endStatus[simplePos] = endStatus2;
                    result2->location[simplePos] = location2;
                    result2->aligned_subject_lengths[simplePos] = sLength2;
                    result2->subjects_matched = 1;
                } else {
                    excessiveAlns++;
                }
            } else {
                result2->subjects_matched = 0;
            }
        }

        /* Read the the rest alignments of read (lane 2) */
        if (resRead2 != NULL) {
            start_io = UPC_TICKS_NOW();

            resRead2 = GZIP_GETS(alignmentBuffer2, MAX_LINE_SIZE, laneFD2);

            end_io = UPC_TICKS_NOW();
            read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

            memcpy(copyAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
            if (resRead2 != NULL) {
                assert(resRead2[strlen(resRead2) - 1] == '\n');

#ifdef MERGE
                memcpy(aux, copyAlignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
#endif
                /* Split alignment and check for guard values */
                validAlign2 = splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &qStart2, &qStop2, &qLength2, &subject2, &sStart2, &sStop2, &sLength2, &strand2);
#ifdef FIND_DENSITY
                if (sLength2) {
                    arrayOfContigLengths[subject2] = sLength2;
                }
#endif

                while (strcmp(readIdLane2, newReadIdLane2) == 0) {
#ifdef MERGE
                    fprintf(mergedFD, "%s", aux);
#endif

                    if (validAlign2 == SUCCESS && excludeListPrefix) {
		      assert(subject2 <= totalContigs);
		      if (excludeContigs[subject2]) {
                            my_in_excludeList++;
                            validAlign2 = FAIL;
                        }
                    }


                    /* Assess alignment for completeness (do this before scaffold coordinate conversion!) */
                    if (validAlign2 == SUCCESS) {
                        validAlign2 = assessAlignment(strand2, qStart2, qStop2, qLength2, sStart2, sStop2, sLength2, fivePrimeWiggleRoom, threePrimeWiggleRoom, truncate, &projectedStart2, &projectedEnd2, &startStatus2, &endStatus2, &fiveReject, &threeReject);
                    }

                    /* Reorient alignment if requested */
                    if (validAlign2 == SUCCESS) {
                        if (reverseComplement) {
                            strand2 = (strand2 == PLUS) ? MINUS : PLUS;
                            swap_var = qStart2;
                            qStart2 = qLength2 - qStop2 + 1;
                            qStop2 = qLength2 - swap_var + 1;
                        }
                    }

                    /* Convert to scaffold coordinate system if srfFile is specified */
                    if (validAlign2 == SUCCESS) {
                        if (srfInput) {
                            validAlign2 = convertToScaffoldCoordinates(qStart2, qStop2, qLength2, &subject2, &strand2, &sStart2, &sStop2, &sLength2, &projectedStart2, &projectedEnd2, contigScaffoldMap, scaffLengths);
                        }
                    }

                    /* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
                    if (validAlign2 == SUCCESS) {
                        validAlign2 = assessLocationAlignment(strand2, projectedStart2, projectedEnd2, sLength2, endDistance, &location2, &uninformative);
                    }

                    /* Store alignment in data structure result2 */
                    if (validAlign2 == SUCCESS) {
                        simplePos = result2->subjects_matched;
                        if (simplePos < MAX_ALIGN) {
                            evenPos = 2 * simplePos;
                            oddPos = evenPos + 1;
                            result2->query_name = readIdLane2;
                            result2->query_length = qLength2;
                            result2->aligned_query_locs[evenPos] = qStart2;
                            result2->aligned_query_locs[oddPos] = qStop2;
                            result2->aligned_subject_ids[simplePos] = subject2;
                            result2->aligned_subject_locs[evenPos] = sStart2;
                            result2->aligned_subject_locs[oddPos] = sStop2;
                            result2->aligned_strand[simplePos] = strand2;
                            result2->startStatus[simplePos] = startStatus2;
                            result2->endStatus[simplePos] = endStatus2;
                            result2->location[simplePos] = location2;
                            result2->aligned_subject_lengths[simplePos] = sLength2;
                            result2->subjects_matched += 1;
                        } else {
                            excessiveAlns++;
                        }
                    }

                    start_io = UPC_TICKS_NOW();

                    resRead2 = GZIP_GETS(alignmentBuffer2, MAX_LINE_SIZE, laneFD2);

                    end_io = UPC_TICKS_NOW();
                    read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

                    memcpy(copyAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
                    if (resRead2 == NULL) {
                        break;
                    }
                    assert(resRead2[strlen(resRead2) - 1] == '\n');

                    validAlign2 = splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &qStart2, &qStop2, &qLength2, &subject2, &sStart2, &sStop2, &sLength2, &strand2);
#ifdef FIND_DENSITY
                    if (sLength2) {
                        arrayOfContigLengths[subject2] = sLength2;
                    }
#endif
                }

                if (resRead2 != NULL) {
                    memcpy(firstAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
                    //strcpy(readIdLane2, newReadIdLane2);
                }
            }
        }

        /* Process pair */
        //      if ((result2->subjects_matched == 1) && (result1->subjects_matched == 1)) {
        pairsProcessed += spanner_processPair(result1, result2, outFD, innieRemoval, minEndSeparation, srfInput, &uninformative, &singleton, minMatch);
        //      }


#ifdef FIND_DENSITY
        if (commonContig != -1) {
            arrayOfMappedPairCounts[commonContig]++;
        }
#endif

        strcpy(readIdLane1, newReadIdLane1);
        strcpy(readIdLane2, newReadIdLane2);
    }

    upc_fence;
    closeCheckpoint(outFD);
    closeCheckpoint(laneFD1);
    closeCheckpoint(laneFD2);
    free_chk(result1);
    free_chk(result2);

#ifdef MERGE
    closeCheckpoint0(mergedFD);
#endif
    // TODO replace with reduce
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &tot_excludedOnDemand, my_in_excludeList);  
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &singletons, singleton);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &fiveRejects, fiveReject);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &threeRejects, threeReject);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &uninformatives, uninformative);
    if (excessiveAlns) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &tot_excessiveAlns, excessiveAlns);
    }

    upc_barrier;
    if (scaffLengths != NULL) {
        UPC_ALL_FREE_CHK(scaffLengths);
    }
    if (contigScaffoldMap != NULL) {
        UPC_ALL_FREE_CHK(contigScaffoldMap);
    }
    if (excludeContigs != NULL) {
        UPC_ALL_FREE_CHK(excludeContigs);
    }

    end = UPC_TICKS_NOW();
    if (MYTHREAD == 0) {
        printf("Unused alignments:\n");
        printf("\tUNINFORMATIVE: %lld\n", (lld)uninformatives);
        printf("\t5-TRUNCATED: %lld\n", (lld)fiveRejects);
        printf("\t3-TRUNCATED: %lld\n", (lld)threeRejects);
        printf("\tSINGLETON: %lld\n", (lld)singletons);
        printf("\tExcessive: %lld\n", (lld)tot_excessiveAlns);
        printf("\tExcluded on demand: %lld\n", (lld)tot_excludedOnDemand);
        printf("\nTime for spanner : %d seconds\n", ((int)UPC_TICKS_TO_SECS(end - start)));

        ADD_DIAG("%lld", "unused_alns_uninformative", (lld)uninformatives);
        ADD_DIAG("%lld", "unused_alns_5_truncated", (lld)fiveRejects);
        ADD_DIAG("%lld", "unused_alns_3_truncated", (lld)threeRejects);
        ADD_DIAG("%lld", "unused_alns_singleton", (lld)singletons);

        char countFileName[MAX_FILE_PATH];
        sprintf(countFileName, "%s-bmaMeta-spans-%d", libname, scaffoldRound);
        get_rank_path(countFileName, -1);
        FILE *countFD = fopen_chk(countFileName, "w+");
        fprintf(countFD, "%d\t%d\t%d\t%d\t%s-spans-%d\n", insertSize, insertSigma, innieRemoval, readLength, libname, scaffoldRound);

        printf("\nRead I/O time is : %f seconds\n", read_io_time);
        printf("Write I/O time is : %f seconds\n", write_io_time);
        printf("Pure computation time is : %f seconds\n", UPC_TICKS_TO_SECS(end - start) - (write_io_time + read_io_time));


        fclose_track(countFD);
    }

    upc_barrier;

#ifdef FIND_DENSITY
    /* Perform aggregation for the partial counts */
    for (int z = 0; z < totalContigs; z++) {
        if (arrayOfMappedPairCounts[z] != 0) {
            /* Perform a fetch and add */
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &sharedArrayOfMappedPairCounts[z], arrayOfMappedPairCounts[z]);
        }
        if (arrayOfContigLengths[z] != 0) {
            int64_t dummy;
            UPC_ATOMIC_CSWAP_I64_RELAXED(dummy, &sharedArrayOfContigLengths[z], 0, arrayOfContigLengths[z]);
        }
    }

    upc_barrier;

    /* Write results for mapped erad-pair density on a file */
    char nMappedFilename[MAX_FILE_PATH];
    sprintf(nMappedFilename, "nMappedReadsPerContig");
    FILE *myCountFd = openCheckpoint0(nMappedFilename, "w+");
    for (int z = MYTHREAD; z < totalContigs; z += THREADS) {
        fprintf(myCountFd, "%d\t%lld\t%lld\n", z, (lld)sharedArrayOfMappedPairCounts[z], (lld)sharedArrayOfContigLengths[z]);
    }

    closeCheckpoint0(myCountFd);
    upc_barrier;

    if (arrayOfMappedPairCounts != NULL) {
        free_chk(arrayOfMappedPairCounts);
    }
    if (sharedArrayOfMappedPairCounts != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfMappedPairCounts);
    }
    if (arrayOfContigLengths != NULL) {
        free_chk(arrayOfContigLengths);
    }
    if (sharedArrayOfContigLengths != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfContigLengths);
    }
#endif

    free_chk(lineBuffers);
    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }
    return 0;
}
