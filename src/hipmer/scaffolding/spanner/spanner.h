#ifndef SPANNER_H_
#define SPANNER_H_

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#include "optlist.h"

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "upc_output.h"

extern double write_io_time;

#include "spannerUtils.h"

int spanner_main(int argc, char **argv);

#endif // SPANNER_H_
