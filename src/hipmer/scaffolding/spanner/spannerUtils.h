#ifndef SPANNER_UTILS_H_
#define SPANNER_UTILS_H_

#include <stdbool.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "spanner.h"

#include "../histogrammer/analyzerUtils.h"


/* contigScaffoldMap data structure */
typedef struct contigScaffoldMap_t contigScaffoldMap_t;
struct contigScaffoldMap_t {
    int     cStrand;
    int64_t scaffID;
    int     sStart;
    int     sEnd;
};

int assignType(int type, char *resultStr);

int assignStat(int stat, char *resultStr);

int assignDir(int dir, char *resultStr);

/* Process a pair */
int spanner_processPair(align_info *lane1, align_info *lane2, GZIP_FILE outFD, int innieRemoval, int minEndSeparation, int srfInput, int64_t *uninf, int64_t *sing, int minMatch);


/* Convert to scaffold coordinate system */
int convertToScaffoldCoordinates(int qStart, int qStop, int qLength, int64_t *subject, int *strand, int *sStart, int *sStop, int *sLength, int *projectedStart, int *projectedEnd, shared[1] contigScaffoldMap_t *contigScaffoldMap, shared[1] int64_t *scaffLengths);

/* Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle) */
int assessLocationAlignment(int strand, int projectedStart, int projectedEnd, int sLength, int endDistance, int *locationRes, int64_t *uninf);

/* Assess alignment for completeness */
int assessAlignment(int strand, int qStart, int qStop, int qLength, int sStart, int sStop, int sLength, int fivePrimeWiggleRoomIn, int threePrimeWiggleRoomIn, int truncate, int *projectedStartRes, int *projectedEndRes, int *startStatusRes, int *endStatusRes, int64_t *five, int64_t *three);

int assignStrings(int startStatus, int endStatus, int location, char **startStatus_ptr, char **endStatus_ptr, char **location_ptr, char *inc, char *gap, char *trunc, char *ins, char *outs, char *mid, char *full);

#endif // SPANNER_UTILS_H_
