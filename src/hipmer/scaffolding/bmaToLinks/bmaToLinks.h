#ifndef BMA_TO_LINKS_H_
#define BMA_TO_LINKS_H_

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>

#include "defines.h"
#include "optlist.h"
#include "upc_common.h"
#include "upc_compatibility.h"
#include "bma_meta.h"
#include "gapSizeEstimatesUtils.h"
#include "linkHash.h"
#include "utils.h"
#include "upc_output.h"

#define MAX_BMA_LIBRARIES (MAX_LIBRARIES*2) // splint + spans for each library

#include "bmaToLinksUtils.h"
#include "timers.h"

int bmaToLinks_main(int argc, char **argv);

#endif // BMA_TO_LINKS_H_
