#ifndef LINK_HASH_H_
#define LINK_HASH_H_

#define SPLINT 0
#define SPAN 1
#define LINK_CHUNK_SIZE 100
#define BS 1
#define PLUS 0
#define MINUS 1

#include <upc.h>
#include "bma_meta.h"
#include "upc_common.h"
#include "hash_funcs.h"

typedef struct link_t link_t;
struct link_t {
    int           end1_id;         // ID of link's endpoint 1
    int           end2_id;         // ID of link's endpoint 2
    char          compressedTails; // Code that repesents compressed info for (*.3 / *.5)
    char          nature;
    unsigned char link_type;       // Type of link: SPLINT or SPAN
    int           lib_id;          // Library id
    int           endSeparation;
    int           d1;             // distane from read1's 5' to end1  (legroom)
    int           d2;             // distane from read2's 5' to end2 (legroom)
    int           o1_length;
    int           o2_length;
};

typedef shared[] link_t *shared_link_ptr;

/* Link heap data structure */
typedef struct link_heap_t link_heap_t;
struct link_heap_t {
    shared[BS] shared_link_ptr * link_ptr;                   // Pointers to shared memory heaps
    shared[BS] int64_t * heap_indices;                       // Indices of remote heaps
    shared[BS] int64_t * heap_sizes;                         // Capacity of remote heaps
};

/* Creates shared heaps that required for links hash table - IMPORTANT: This is a collective function */
int create_link_heaps(int64_t size, link_heap_t *link_heap);

void destroy_link_heaps(link_heap_t *link_heap);

/* Allocate local arrays used for book-keeping when using aggregated upc_memputs */
int allocate_local_buffs(link_t **local_buffs, int64_t **local_index);

void free_local_buffs(link_t **local_buffs, int64_t **local_index);

/* Adds a link to the shared link heap */
int add_link_to_shared_heaps(link_t *new_entry, int64_t hashval, int64_t *local_index, link_t *local_buffs, link_heap_t link_heap, int count_only);

/* Adds remaining links to the shared heaps. Should be called when all calls "add_link_to_shared_heaps()" have been done */
int add_rest_links_to_shared_heaps(int64_t *local_index, link_t *local_buffs, link_heap_t link_heap);

/* Data structure to store data regarding a link */
typedef struct datum_t datum_t;
struct datum_t {
    int      endSeparation;      // End Separation field of dataum
    int      d1;                 // Span specific data
    int      d2;                 // Span specific data
    int      lib_id;             // Library identifier
    int      count;
    datum_t *next;               // Pointer to next entry in the same datum list
};

/* Essentially the key now is the combination: end1_id | end2_id | linktype */
typedef struct linkList_t linkList_t;
struct linkList_t {
    int           end1_id;       // ID of link's endpoint 1
    int           end2_id;       // ID of link's endpoint 2
    char          compressedTails;
    unsigned char link_type;     // Type of link: SPLINT or SPAN
    int           o1_length;
    int           o2_length;
    datum_t *     data;          // Data list
    linkList_t *  next;          // Pointer to next entry in the same bucket
};

typedef struct link_hash_table_t_ {
    int64_t      size;           /* the size of the table */
    linkList_t **table;          /* the table elements */
} link_hash_table_t;

/* Create a link_hash_table */
link_hash_table_t *create_link_hash_table(int64_t size);

void destroy_link_hash_table(link_hash_table_t **_ht);

char compressTails(char tail1, char tail2);

int64_t link_hash_val(int64_t e1, int64_t e2, char compressedTails, int64_t linkhash_size);

/* Lookup function for link hash table */
linkList_t *lookup_link(link_hash_table_t *hashtable, int e1, int e2, char compressedTails, unsigned char type, int64_t hashval);

/* Implement efficient memory allocator to avoid multiple malloc() calls */
int create_local_heaps(int64_t max_size, datum_t **datum_heap, linkList_t **linkList_heap);

/* Add link data to the hashtable */
int add_link_data(link_hash_table_t *hashtable, link_t *cur_link, int64_t *datum_heap_pos, datum_t *datum_heap, int64_t *link_heap_pos, linkList_t *linkList_heap, bma_info *bmas, int64_t *nLinks, GZIP_FILE myLog);

/* Free routines */
int free_local_heaps(datum_t **datum_heap, linkList_t **linkList_heap);

#endif // LINK_HASH_H_
