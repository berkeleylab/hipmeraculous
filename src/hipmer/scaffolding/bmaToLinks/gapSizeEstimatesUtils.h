#ifndef GAP_SIZE_ESTIMATES_UTILS_H_
#define GAP_SIZE_ESTIMATES_UTILS_H_

#include <math.h>

double meanSpanningClone(double g, int k, int l, int c1, int c2, double mu, double sigma);

double estimateGapSize(double meanAnchor, int k, int l, int c1, int c2, double mu, double sigma, int isMetagenome);

#endif // GAP_SIZE_ESTIMATES_UTILS_H_
