#include "bmaToLinks.h"

#ifndef M_2_SQRTPI
#define M_2_SQRTPI 1.12838
#endif

#ifndef M_SQRT1_2
#define M_SQRT1_2 0.707107
#endif

#define COL_SIZE 500
//#define FIND_DENSITY

double phi(double x)
{
    double result = 0.5 *M_2_SQRTPI *M_SQRT1_2 *exp(-0.5 *x *x);

    return result;
}


double estimateNumberOfPairs(int A, int B, int D, int mu, int sigma, double rho)
{
    double alpha = (1.0 * (D - mu)) / (1.0 * sigma);
    double beta = (1.0 * (D + B - mu)) / (1.0 * sigma);
    double gamma = (1.0 * (D + A - mu)) / (1.0 * sigma);
    double delta = (1.0 * (D + A + B - mu)) / (1.0 * sigma);
    double result;

    result = rho * sigma * (phi(alpha) - phi(beta) - 0.5 * alpha * (erf(M_SQRT1_2 * beta) - erf(M_SQRT1_2 * alpha)))
             + rho * B * 0.5 * (erf(M_SQRT1_2 * gamma) - erf(M_SQRT1_2 * beta))
             - rho * sigma * (phi(gamma) - phi(delta) - 0.5 * delta * (erf(M_SQRT1_2 * delta) - erf(M_SQRT1_2 * gamma)));

    return result;
}

shared int64_t spansTooLarge = 0;

int bmaToLinks_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    char objType[9]; // Contig or Scaffold
    
    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "b:m:D:P:n:L:l:B:C:M");
    print_args(optList, __func__);
    UPC_TICK_T start, end;
    char *cur_bmaFile;
    int linkUid = -1;
    char *bmaDataFileNames = NULL;
    bma_info* bmas;
    GZIP_FILE cur_bma_fd;
    int merSize, minNetEndDistance = 0, previousMaxInsertSize = 0;
    int isMetagenome = 0;
    int64_t i, j, total_entries = 0, nLibs = 0;
    char line[MY_LINESIZE];
    char link[MY_LINESIZE];
    char col0[COL_SIZE], col1[COL_SIZE], col2[COL_SIZE], col3[COL_SIZE], col4[COL_SIZE], col5[COL_SIZE];
    char personalBmaDataFile[MAX_FILE_PATH + 3];
    char personalBmaDataFileOut[MAX_FILE_PATH];
    GZIP_FILE outFD;
    int64_t *local_index;
    link_heap_t *link_heap;
    int insertSize = -1, insertStdDev = 0, my_count = 0;
    /*  Each line for a library: column0 --> spans_count | column1 --> endSeparation sum */
    int spans[MAX_BMA_LIBRARIES][4];
    int64_t *splints;
    int cpy_previousMaxInsertSize = 0;
    int64_t datum_heap_pos, link_heap_pos, entries_in_my_stack;
    datum_t *datum_heap, *cur_data;
    linkList_t *linkList_heap, *cur_bucket;
    link_hash_table_t *local_hashtable;
    link_t *local_buffs;
    link_t *my_stack;
    int64_t cur_e1, cur_e2;
    int minimumGap, splintMaxDev, spanMaxZ, cur_type, cur_lib, cur_endSep, cur_legroom, cur_headroom, splintDev, my_checksum;
    int64_t splintMaxFreq, nSplints, nSpans, anomalousSpans, anomalousSplints;
    int spanAnomaly, splintGapEstimate, nLibSpans, readLength, netSeparation, l1, l2, spanGapUncertainty;
    double spanZ;
    double meanGapEstimate, meanOffset, sumOfWeights, spanGapUncertaintyDouble, libWeight;
    char cur_compressedTails;
    int tail1, tail2, my_anomalies;
    double spansGE[MAX_BMA_LIBRARIES], spanGapEstimate, gapEstimate;
    int64_t total;
    int64_t myPRINTlinks = 0;
    int64_t totalContigs = 0;

    char my_nContigsFile[255];
    FILE *my_nC_fd;

    const char *base_dir = ".";

    UPC_TICK_T startT, endT;
    UPC_TICK_T start_io, end_io;
    double io_read_time = 0.0;

    char *libUid;

    serial_printf("MAX_LIBS %d\n", MAX_LIBRARIES);
    bmas = (bma_info *)malloc_chk(MAX_BMA_LIBRARIES * sizeof(bma_info));
    splints = (int64_t *)malloc_chk(MAX_END_SEPARATION * sizeof(int64_t));
    /* Process the input arguments */
    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'b':
            bmaDataFileNames = strdup_chk(thisOpt->argument);
            break;
        case 'm':
            merSize = atoi(thisOpt->argument);
            break;
        case 'D':
            minNetEndDistance = atoi(thisOpt->argument);
            break;
        case 'P':
            previousMaxInsertSize = atoi(thisOpt->argument);
            cpy_previousMaxInsertSize = previousMaxInsertSize;
            break;
        case 'n':
            /* Total number of entries in bma files */
            total_entries = atol(thisOpt->argument);
            break;
        case 'L':
            linkUid = atol(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'C':
            totalContigs = atol(thisOpt->argument);
            break;
        case 'M':
            isMetagenome = 1;
            break;
        default:
            break;
        }
    }

    if (linkUid < 0 || total_entries <= 0 || !bmaDataFileNames) {
        SDIE("Please enter -b bmaMetaFileNames -n (estimated)TotalEntries and"
             "a L linkUid(round number)\nwhere bmaMetaFileNames is a comma-separated list\n");
    }

    if (linkUid <= 1) { // for cgraph .. it names first round of two as Contig
      strcpy(objType, "Contig");
    } else {
      strcpy(objType, "Scaffold");
    }
    
    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();
    /* bmaToLinks code */


    
#ifdef FIND_DENSITY
    /* Code to assess density of maped reads per contig */
    char nMappedFilename[MAX_FILE_PATH];
    sprintf(nMappedFilename, "nMappedReadsPerContig");
    FILE *myCountFd = openCheckpoint0(nMappedFilename, "r");
    char lineMapped[256];
    char aux_lineMapped[256];
    char *token;
    char *aux;
    int64_t currentContig;
    int64_t currentDensity, currentLength;
    double currentDoubleDensity = 0.0;

    shared[1] double *sharedArrayOfDensities = NULL;
    spansTooLarge = 0;
    UPC_ALL_ALLOC_CHK(sharedArrayOfDensities, totalContigs, sizeof(double));

    shared[1] int64_t * sharedArrayOfLengths = NULL;
    UPC_ALL_ALLOC_CHK(sharedArrayOfLengths, totalContigs, sizeof(int64_t));

    while (fgets(lineMapped, 256, myCountFd) != NULL) {
        strcpy(aux_lineMapped, lineMapped);
        token = strtok_r(aux_lineMapped, "\t", &aux);
        currentContig = atol(token);
        token = strtok_r(NULL, "\t", &aux);
        currentDensity = atol(token);
        token = strtok_r(NULL, "\t", &aux);
        currentLength = atol(token);
        currentDoubleDensity = (1.0 * currentDensity) / (1.0 * currentLength);
        sharedArrayOfDensities[currentContig] = currentDoubleDensity;
        sharedArrayOfLengths[currentContig] = currentLength;
    }

    closeCheclpoint0(myCountFd);

    char linkEstimatesFileName[MAX_FILE_PATH];
    sprintf(linkEstimatesFileName, "nPairsEstimates");
    FILE *estimatesFD = openCheckpoint0(linkEstimatesFileName, "w");

    UPC_LOGGED_BARRIER;
    serial_printf("Threads done reading information for density of mapped reads per contig\n");
#endif

    char *next_name = NULL;
    char *cur_name = bmaDataFileNames;
    /* Count the number of libs and store info for each library in the mapInfo data structure */
    while (1) {
        if (nLibs >= MAX_BMA_LIBRARIES) DIE("nLibs=%d exceeds MAX_BMA_LIBRARIES==%d.  Please recomile with a larger number of possible libraries\n", nLibs, MAX_BMA_LIBRARIES);
        char *next_name = strchr(cur_name, ',');
        if (next_name) {
            next_name[0] = 0;
        }

        // one lib per file
        char name_path[MAX_FILE_PATH];
        strcpy(name_path, cur_name);
        FILE *bmaDataFile = fopen_rank_path(name_path, "r", -1);

        char lib_name_scratch[MAX_FILE_PATH];
        strncpy(lib_name_scratch, cur_name, MAX_FILE_PATH - 1);
        char *tmp;
        if ((tmp = strstr(lib_name_scratch, "-bmaMeta-spans")) != NULL) {
            *tmp = '\0';
        } else if ((tmp = strstr(lib_name_scratch, "-bmaMeta-splints")) != NULL) {
            *tmp = '\0';
        } else {
            DIE("Could not determine the library name from bmaDataFile: '%s'\n", cur_name);
        }
        if (strlen(lib_name_scratch) >= MAX_USER_LIB_NAME_LEN) {
            DIE("Invalid calculated name from bmaDataFile: '%s'\n", cur_name);
        }
        strcpy(bmas[nLibs].libName, lib_name_scratch);
        serial_printf("Found lib %s from %s\n", bmas[nLibs].libName, cur_name);


        if (!fgets(line, MY_LINESIZE - 1, bmaDataFile)) {
            DIE("Could not get the line in %s -> %s\n", cur_name, name_path);
        }
        char *aux_line = strdup_chk(line);
        split_bmaLine(aux_line, &bmas[nLibs], nLibs);
        free_chk(aux_line);
        serial_printf("read %s: %d %d %d %d %s\n", name_path, bmas[nLibs].insertSize, bmas[nLibs].stdDev,
                      bmas[nLibs].innieRemoval, bmas[nLibs].readLength, bmas[nLibs].bmaFile);

        for (i = 0; i < REASONS; i++) {
            bmas[nLibs].libPairSummary[i] = 0;
        }
        DBG("read file %s (%lld): %s %d %d\n", name_path, (lld)nLibs, bmas[nLibs].bmaFile, bmas[nLibs].insertSize, bmas[nLibs].stdDev);
        fclose_track(bmaDataFile);
        if (!MYTHREAD) {
            LOGF("Processing lib=%s insertSize=%d bmaFile=%s\n", cur_name, bmas[nLibs].insertSize, bmas[nLibs].bmaFile);
        }
        nLibs++;
        if (!next_name) {
            break;
        }
        cur_name = next_name + 1;
    }

    UPC_LOGGED_BARRIER;
    upc_barrier;
    if (bmaDataFileNames) {
        free_chk(bmaDataFileNames);
    }

    startT = UPC_TICKS_NOW();
    char *fgets_result;

    serial_printf("Threads done reading BMA Metafiles, found %lld libs\n", (lld)nLibs);

    /* Calculate the number of my heap entries required */

    /* Allocate local buffers and local shared heaps required for the links hashtable */
    allocate_local_buffs(&local_buffs, &local_index);
    link_heap = malloc_chk(sizeof(link_heap_t));
    create_link_heaps(1, link_heap); // just 1 entry for counting first

    /* Calculate the number of my heap entries required */
    for (i = 0; i < nLibs; i++) {
        cur_bmaFile = bmas[i].bmaFile;
        sprintf(personalBmaDataFile, "%s%s", cur_bmaFile, GZIP_EXT);
        LOGF("Counting entries in %s\n", personalBmaDataFile);
        cur_bma_fd = openCheckpoint(personalBmaDataFile, "r");

        start_io = UPC_TICKS_NOW();
        fgets_result = GZIP_GETS(line, MY_LINESIZE - 1, cur_bma_fd);
        end_io = UPC_TICKS_NOW();
        io_read_time += UPC_TICKS_TO_SECS(end_io - start_io);
        int line_count = 0;

        while (fgets_result != NULL) {
            line_count++;
            split_bma_entry(line, col0, col1, col2, col3, col4, col5);

            if ((strcmp(col0, "SINGLE") == 0) && (strcmp(col1, "SPLINT") == 0)) {
                processSplint(col2, col3, col4, col5, &bmas[i], local_index, local_buffs, *link_heap, 1);
            } else if (strcmp(col0, "PAIR") == 0) {
                processPair(col2, col3, col4, col5, &bmas[i], local_index, local_buffs, *link_heap, &cpy_previousMaxInsertSize, minNetEndDistance, 1);
            }
            start_io = UPC_TICKS_NOW();
            fgets_result = GZIP_GETS(line, MY_LINESIZE - 1, cur_bma_fd);
            end_io = UPC_TICKS_NOW();
            io_read_time += UPC_TICKS_TO_SECS(end_io - start_io);
        }

        closeCheckpoint(cur_bma_fd);
        LOGF("Finished %s (%d lines)\n", personalBmaDataFile, line_count);
    }
    UPC_LOGGED_BARRIER;
    for(int i = MYTHREAD ; i < THREADS + MYTHREAD; i++) {
         int remote_thread = i % THREADS;
         UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(link_heap->heap_indices[remote_thread]), local_index[remote_thread]);
         local_index[remote_thread] = 0;
    }
    UPC_LOGGED_BARRIER;
    int64_t my_entries = link_heap->heap_indices[MYTHREAD];
    link_heap->heap_indices[MYTHREAD] = 1; // to prevent warn and/or die on the following destroy_link_heaps
    LOGF("Found my_entries=%lld\n", (lld) my_entries);
    int64_t test_total = reduce_long(my_entries, UPC_ADD, SINGLE_DEST);
    if (MYTHREAD == 0 && test_total != total_entries) WARN("Inconsistent sum vs command line total_entries (%lld vs %lld)\n", (lld) test_total, (lld) total_entries);
    
    // recreate link heaps with actual counts
    destroy_link_heaps(link_heap);
    create_link_heaps(my_entries, link_heap);

    UPC_LOGGED_BARRIER;
    serial_printf("Threads done allocating link heaps: %lld\n", (lld)total_entries);

    for (i = 0; i < nLibs; i++) {
        cur_bmaFile = bmas[i].bmaFile;
        sprintf(personalBmaDataFile, "%s%s", cur_bmaFile, GZIP_EXT);
        LOGF("Processing %s\n", personalBmaDataFile);
        cur_bma_fd = openCheckpoint(personalBmaDataFile, "r");

        start_io = UPC_TICKS_NOW();
        fgets_result = GZIP_GETS(line, MY_LINESIZE - 1, cur_bma_fd);
        end_io = UPC_TICKS_NOW();
        io_read_time += UPC_TICKS_TO_SECS(end_io - start_io);
        int line_count = 0;

        while (fgets_result != NULL) {
            line_count++;
            split_bma_entry(line, col0, col1, col2, col3, col4, col5);

            if ((strcmp(col0, "SINGLE") == 0) && (strcmp(col1, "SPLINT") == 0)) {
                processSplint(col2, col3, col4, col5, &bmas[i], local_index, local_buffs, *link_heap, 0);
            } else if (strcmp(col0, "PAIR") == 0) {
                processPair(col2, col3, col4, col5, &bmas[i], local_index, local_buffs, *link_heap, &cpy_previousMaxInsertSize, minNetEndDistance, 0);
            }
            start_io = UPC_TICKS_NOW();
            fgets_result = GZIP_GETS(line, MY_LINESIZE - 1, cur_bma_fd);
            end_io = UPC_TICKS_NOW();
            io_read_time += UPC_TICKS_TO_SECS(end_io - start_io);
        }

        closeCheckpoint(cur_bma_fd);
        LOGF("Finished %s (%d lines)\n", personalBmaDataFile, line_count);
    }
    UPC_LOGGED_BARRIER;
    serial_printf("Threads done processing links\n");

    /* Adds remaining links to the shared heaps */
    add_rest_links_to_shared_heaps(local_index, local_buffs, *link_heap);

    /* Wait until all heaps have been fixed */
    UPC_LOGGED_BARRIER;
    serial_printf("Threads done fixing heaps\n");

    endT = UPC_TICKS_NOW();
    double links_storing = UPC_TICKS_TO_SECS(endT - startT);

    serial_printf("Threads done processing the input files\n");

    startT = UPC_TICKS_NOW();


    /* Create local hash table by iterating on local heap and storing link_data to local_hashtable */
    datum_heap_pos = 0;
    link_heap_pos = 0;
    entries_in_my_stack = link_heap->heap_indices[MYTHREAD];
    create_local_heaps(entries_in_my_stack, &datum_heap, &linkList_heap);
    local_hashtable = create_link_hash_table(entries_in_my_stack);

    if (local_hashtable == NULL) {
        DIE("Could not create_link_hashtable %lld\n", (lld)entries_in_my_stack);
    }
    assert(upc_threadof(link_heap->link_ptr + MYTHREAD) == MYTHREAD);
    my_stack = (link_t *)link_heap->link_ptr[MYTHREAD];
    LOGF("created link_hash_table: %lld\n", (lld)entries_in_my_stack);

    int64_t accepted_links = 0;
    int64_t nLinks = 0;
    GZIP_FILE myFD = NULL;
#ifdef DEBUG
    {
        char myLogName[MAX_FILE_PATH];
        sprintf(myLogName, "%s/linksLog_%d" GZIP_EXT, base_dir, MYTHREAD);
        get_rank_path(myLogName, MYTHREAD);
        myFD = GZIP_OPEN(myLogName, "w");
        if (myFD == NULL) {
            DIE("Thread %d: Could not open the my logfile %s!\n", MYTHREAD, myLogName);
        }
    } while (0) {
        ;
    }
#endif

    for (i = 0; i < entries_in_my_stack; i++) {
        assert(datum_heap_pos < entries_in_my_stack);
        accepted_links += (int64_t)add_link_data(local_hashtable, (link_t *)(my_stack + i), &datum_heap_pos, datum_heap, &link_heap_pos, linkList_heap, bmas, &nLinks, myFD);
    }

    DBG("added %lld links, accepted: %lld\n", (lld)nLinks, (lld)accepted_links);
    UPC_LOGGED_BARRIER;
    serial_printf("Threads created local splints hash tables\n");
    //UPC_LOGGED_BARRIER;

#ifdef DEBUG
    int64_t tot_links = reduce_long(accepted_links, UPC_ADD, SINGLE_DEST);
    int64_t tot_nLinks = reduce_long(nLinks, UPC_ADD, SINGLE_DEST);
    SLOG("Total accepted links are: %lld \n", (lld)tot_links);
    if (MYTHREAD == 0) {
        printf("Total different link keys are: %lld \n", (lld)tot_nLinks);
    }

#endif

    sprintf(personalBmaDataFileOut, "LINKS_OUTPUT_%d" GZIP_EXT, linkUid);
    LOGF("Writing links output: %s\n", personalBmaDataFileOut);
    outFD = openCheckpoint(personalBmaDataFileOut, "w");

    /*******************************************************/
    /* Now iterate over the hashtable and process the data */
    /*******************************************************/

    int64_t my_num_spans = 0;
    int64_t my_num_splints = 0;

    for (i = 0; i < local_hashtable->size; i++) {
        //if (MYTHREAD == 0) printf("Index is %lld\n", i);
        cur_bucket = local_hashtable->table[i];
        while (cur_bucket != NULL) {
            minimumGap = -(merSize - 2);
            splintMaxDev = 2;
            spanMaxZ = 3;

            /* Set SPLINT frequency table to zero */
            memset(splints, 0, MAX_END_SEPARATION * sizeof(int64_t));
            /* Set span data structure to zero */
            for (j = 0; j < nLibs; j++) {
              spans[j][0] = 0; //support
              spans[j][1] = 0; //netEndSeparation
              spans[j][2] = 0; //netLegroom: total distance-to-end (d1+d2) from all qualifying span-pairs
              spans[j][3] = 0; //netHeadroom: total distance-to-start
              
              spansGE[j] = 0.0;  // final gap estimate for lined ends using lib j
            }
            nSplints = 0;
            nSpans = 0;

            /* Process the data in the the current link */
            cur_e1 = cur_bucket->end1_id;
            cur_e2 = cur_bucket->end2_id;
            cur_compressedTails = cur_bucket->compressedTails;
            if (cur_compressedTails == 0) {
                tail1 = 3;
                tail2 = 3;
            }
            if (cur_compressedTails == 1) {
                tail1 = 3;
                tail2 = 5;
            }
            if (cur_compressedTails == 2) {
                tail1 = 5;
                tail2 = 3;
            }
            if (cur_compressedTails == 3) {
                tail1 = 5;
                tail2 = 5;
            }

#ifdef DEBUG
            sprintf(link, "%s%lld.%d<=>%s%lld.%d", objType, (lld)cur_e1, tail1, objType, (lld)cur_e2, tail2);
            GZIP_PRINTF(myFD, "%s\n", link);
            my_count = 0;
            my_checksum = 0;
            my_anomalies = 0;
#endif
            cur_type = cur_bucket->link_type;
            cur_data = cur_bucket->data;
            l1 = cur_bucket->o1_length;
            l2 = cur_bucket->o2_length;

            while (cur_data != NULL) {
                cur_lib = cur_data->lib_id;
                cur_endSep = cur_data->endSeparation;
                
                cur_legroom = cur_data->d1 + cur_data->d2;
                cur_legroom -= 2*merSize;
                cur_headroom = (l1 - cur_data->d1)  + (l2 - cur_data->d2);
                // Legroom (d) can be larger than contig length if the read "overhangs", thus leading to negative headroom.  To account for this, we add readLength - merSize. 
                cur_headroom += 2*(bmas[cur_lib].readLength - merSize);  
#ifdef DEBUG
                my_count++;
		GZIP_PRINTF(myFD, "%d, ", cur_endSep);
                my_checksum += cur_endSep;
#endif

                if (cur_type == SPLINT) {
                    if (cur_endSep >= (minimumGap - splintMaxDev)) {
                        nSplints++;
                        if (cur_endSep + OFFSET < MAX_END_SEPARATION) {
                            splints[cur_endSep + OFFSET]++;
                        } else {
                            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &spansTooLarge, 1);
                        }
                    }
                } else if (cur_type == SPAN) {
                    spanAnomaly = 0;
                    if (cur_endSep < minimumGap) {
                        insertStdDev = bmas[cur_lib].stdDev;
                        spanZ = abs(minimumGap - cur_endSep) / (1.0 * insertStdDev);
                        if (spanZ > spanMaxZ) {
#ifdef DEBUG
                            my_anomalies++;
#endif
                            spanAnomaly = 1;
                        }
                    }
                    if (spanAnomaly == 0) {
                        nSpans++;
                        spans[cur_lib][0] += 1;
                        spans[cur_lib][1] += cur_endSep;
                        spans[cur_lib][2] += cur_legroom;
                        spans[cur_lib][3] += cur_headroom;
                    }
                }
                cur_data = cur_data->next;
            }
#ifdef DEBUG
            GZIP_PRINTF(myFD, "COUNT: %d, CHECKSUM: %d, ANOMALIES: %d, SPANS: %lld\n\n", my_count, my_checksum, my_anomalies, (lld)nSpans);
#endif
            /* END: Process the data in the the current link */

            /* Gap size as estimated from splints is taken to be the maximum frequency splint */
            splintGapEstimate = SPLINT_UNDEFINED;
            splintMaxFreq = 0;
            for (j = 0; j < MAX_END_SEPARATION; j++) {
                if (splints[j] > splintMaxFreq) {
                    splintMaxFreq = splints[j];
                    /* FIXME: Deal with negative values of endSeparation */
                    splintGapEstimate = j - OFFSET;
                }
            }

            /* Gap size as estimated from spans is taken to be the weighted mean of spans */
            for (j = 0; j < nLibs; j++) {
                nLibSpans = spans[j][0];
                if (! nLibSpans) {
                  continue;
                }
                insertSize = bmas[j].insertSize;
                insertStdDev = bmas[j].stdDev;
                readLength = bmas[j].readLength;
                assert(readLength > 0);
                netSeparation = spans[j][1];
                meanGapEstimate = (1.0 * netSeparation / (1.0 * nLibSpans));
		meanOffset = (double)insertSize - (double)meanGapEstimate;

		gapEstimate = (l1 < l2) ? estimateGapSize(meanOffset, merSize, readLength, l1, l2, 1.0 * insertSize, 1.0 * insertStdDev, isMetagenome) : estimateGapSize(meanOffset, merSize, readLength, l2, l1, 1.0 * insertSize, 1.0 * insertStdDev, isMetagenome);
                spansGE[j] = gapEstimate;
            }


            /* Calculate overall gap estimate and uncertainty */
            spanGapEstimate = 0.0;
            sumOfWeights = 0.0;
            spanGapUncertainty = -1;
            for (j = 0; j < nLibs; j++) {
                nLibSpans = spans[j][0];
                if (! nLibSpans) {
                  continue;
                }
                insertSize = bmas[j].insertSize;
                insertStdDev = bmas[j].stdDev;
                assert(insertStdDev);
                
                if (isMetagenome) {
                 // Expect short contigs and variable depth. Use legroom and headroom to account for possible bias in gap size estimation and use it in overall spanGapUncertainty calculation 

                  DBG("[DEBUG] l1+l2: %d  (%d nLibSpans)\n", (l1 + l2), nLibSpans);

                  double meanCombinedLegRoom = 1+(spans[j][2] / nLibSpans);
                  DBG("[DEBUG] meanLegRoom: %f\n", meanCombinedLegRoom);
		  //int underestFactor = ceil(2*insertStdDev / meanCombinedLegRoom);
		  int underestCorr = 2*insertStdDev - meanCombinedLegRoom;   // to be confident that the sampling is unbiased by end proximity, we want this much leg room
		  if (underestCorr < 0)
		    underestCorr = 0;
                  /* the less room downstream, the less confident we are that the estimate is not drawm from a strong '+' outlier, thus UNDERestimating gap size */
                  DBG("[DEBUG] underestCorrection: %d\n", underestCorr);


		  //                  double meanHeadRoom = 1+(spans[j][3] / nLibSpans);
		  //                  DBG("[DEBUG] meanHeadRoom: %f\n", meanHeadRoom);
                  /* int overestFactor = ceil(2*insertStdDev / meanHeadRoom); */
                  /* /\*the less upstream room, the less confident we are that the estimate is not drawm from a stron '-' outlier, thus OVERestimating gap size    *\/ */
                  /* DBG("[DEBUG] overestFactor: %d\n", overestFactor); */


		  

		  // The correction means we effectively increase the stdDev factor and don't let depth affect our certainty.
		  // 
		  if (underestCorr) {  
		    libWeight = 1.0 / ((insertStdDev+underestCorr) * (insertStdDev+underestCorr));
		  }
		  else {
		    libWeight = (1.0 * spans[j][0]) / (insertStdDev * insertStdDev);
		  }
                  DBG("[DEBUG] libWeight: %f\n", libWeight);

                } else {
                  libWeight = (1.0 * spans[j][0]) / (1.0 * insertStdDev * 1.0 * insertStdDev);
                }
                sumOfWeights += 1.0 * libWeight;
                spanGapEstimate += (1.0 * spansGE[j]) * (1.0 * libWeight);
            }

            if (sumOfWeights > EPSILON) {
                spanGapEstimate /= sumOfWeights;
                spanGapUncertaintyDouble = sqrt(1 / sumOfWeights);
                DBG("[DEBUG] spanGapUncertaintyDouble: %f\n", spanGapUncertaintyDouble);
                spanGapUncertainty = 1;
            }

            anomalousSplints = 0;
            anomalousSpans = 0;

            /* Revisit data checking for consistency with estimate */
            cur_data = cur_bucket->data;
            while (cur_data != NULL) {
                cur_lib = cur_data->lib_id;
                cur_endSep = cur_data->endSeparation;
                if (cur_type == SPLINT) {
                    if (splintGapEstimate != SPLINT_UNDEFINED) {
                        splintDev = abs(cur_endSep - splintGapEstimate);
                        if (splintDev > splintMaxDev) {
                            anomalousSplints++;
                        }
                    }
                } else if (cur_type == SPAN) {
                    if (spanGapUncertainty != -1) {
                        insertStdDev = bmas[cur_lib].stdDev;
                        spanZ = fabs(cur_endSep - spanGapEstimate) / (1.0 * insertStdDev);
                        if (spanZ > spanMaxZ) {
                            anomalousSpans++;
                        }
                    }
                }
                cur_data = cur_data->next;
            }

            if (splintGapEstimate != SPLINT_UNDEFINED) {
                //strcpy(link, "Contig");
                //myitoa(cur_e1, temp_string, 10);
                //strcpy(link, temp_string);
                //strcat(link, "<=>");
                //strcpy(link, "Contig");
                //myitoa(cur_e2, temp_string, 10);
                //strcpy(link, temp_string);
                myPRINTlinks++;
		sprintf(link, "%s%lld.%d<=>%s%lld.%d", objType, (lld)cur_e1, tail1, objType, (lld)cur_e2, tail2);
                GZIP_PRINTF(outFD, "SPLINT\t%s\t%lld|%lld|%lld\t%d\n", link, (lld)splintMaxFreq, (lld)anomalousSplints, (lld)nSplints, splintGapEstimate);
                my_num_splints++;
            }

            if (spanGapUncertainty != -1) {
                //strcpy(link, "Scaffold");
                //myitoa(cur_e1, temp_string, 10);
                //strcpy(link, temp_string);
                //strcat(link, "<=>");
                //strcpy(link, "Scaffold");
                //myitoa(cur_e2, temp_string, 10);
                //strcpy(link, temp_string);
                myPRINTlinks++;
		sprintf(link, "%s%lld.%d<=>%s%lld.%d", objType, (lld)cur_e1, tail1, objType, (lld)cur_e2, tail2);


#ifndef FIND_DENSITY
                GZIP_PRINTF(outFD, "SPAN\t%s\t%lld|%lld\t%.0f\t%.0f\n", link, (lld)anomalousSpans, (lld)nSpans, spanGapEstimate, spanGapUncertaintyDouble);
                my_num_spans++;
#endif

#ifdef FIND_DENSITY
                int D = (int)spanGapEstimate;
                if (D > 0) {
                    int mu = bmas[nLibs - 1].insertSize;
                    int sigma = bmas[nLibs - 1].stdDev;
                    int A, B;
                    double rho = 0.0;
                    int64_t length1 = sharedArrayOfLengths[cur_e1];
                    int64_t length2 = sharedArrayOfLengths[cur_e2];
                    double estimatedPairs = 0.0;

                    if (length1 >= length2) {
                        A = length1;
                        B = length2;
                        rho = sharedArrayOfDensities[cur_e1];
                    } else {
                        A = length2;
                        B = length1;
                        rho = sharedArrayOfDensities[cur_e2];
                    }

                    estimatedPairs = estimateNumberOfPairs(A, B, D, mu, sigma, rho);
                    fprintf(estimatesFD, "SPAN %s with gap %.0f has %lld spans (%.3f estimated spans with params A=%d , B=%d , D=%d , mu=%d , sigma=%d , rho=%.3f ) \n", link, spanGapEstimate, (lld)nSpans, estimatedPairs, A, B, D, mu, sigma, rho);

                    if (linkUid == 1) {
                        if (1.0 * nSpans > 0.3 * estimatedPairs) {
                            GZIP_PRINTF(outFD, "SPAN\t%s\t%lld|%lld\t%.0f\t%.0f\n", link, (lld)anomalousSpans, (lld)nSpans, spanGapEstimate, spanGapUncertaintyDouble);
                            my_num_spans++;
                        }
                    } else {
                        GZIP_PRINTF(outFD, "SPAN\t%s\t%lld|%lld\t%.0f\t%.0f\n", link, (lld)anomalousSpans, (lld)nSpans, spanGapEstimate, spanGapUncertaintyDouble);
                        my_num_spans++;
                    }
#endif
            }

            cur_bucket = cur_bucket->next;
        }
    }
    closeCheckpoint(outFD);
    free_chk(splints);

#ifdef FIND_DENSITY
    closeCheckpoint0(estimatesFD);
#endif

#ifdef DEBUG
    GZIP_CLOSE(myFD);
#endif
    /* TODO:  Add functionality to add result at separate files  */

    UPC_LOGGED_BARRIER;
    end = UPC_TICKS_NOW();

    endT = UPC_TICKS_NOW();
    double processing_links = UPC_TICKS_TO_SECS(endT - startT);
    serial_printf("Threads done with output files\n");

    if (MYTHREAD == 0) {
        printf("\nTime for executing bmaToLinks : %d seconds\n", ((int)UPC_TICKS_TO_SECS(end - start)));
        printf("Time for storing links data (includes communication) is %f seconds\n", links_storing);
        printf("Time for processing links is %f seconds\n", processing_links);
        printf("I/O read time is %f seconds\n", io_read_time);
        printf("spansTooLarge: %lld (maximum end separation: %lld)\n", (lld)spansTooLarge, (lld)MAX_END_SEPARATION);
    }

    int64_t PRINTnLinks = reduce_long(myPRINTlinks, UPC_ADD, ALL_DEST);


    int64_t tot_spans = reduce_long(my_num_spans, UPC_ADD, SINGLE_DEST);
    int64_t tot_splints = reduce_long(my_num_splints, UPC_ADD, SINGLE_DEST);

    if (MYTHREAD == 0) {
        printf("Number of SPAN links: %lld\n", (lld)tot_spans);
    }
    if (MYTHREAD == 0) {
        printf("Number of SPLINT links: %lld\n", (lld)tot_splints);
    }

    /* Report summary statistics for each library */
    if (MYTHREAD == 0) {
        printf("\n************* Report summary statistics for each library ***********\n");
    }

    for (i = 0; i < nLibs; i++) {
        int64_t nTRUNC = reduce_long(bmas[i].libPairSummary[TRUNCA], UPC_ADD, SINGLE_DEST);
        int64_t nSMALL = reduce_long(bmas[i].libPairSummary[SMALL], UPC_ADD, SINGLE_DEST);
        int64_t nINORM = reduce_long(bmas[i].libPairSummary[INORM], UPC_ADD, SINGLE_DEST);
        int64_t nINNIE = reduce_long(bmas[i].libPairSummary[INNIE], UPC_ADD, SINGLE_DEST);
        int64_t nSELFL = reduce_long(bmas[i].libPairSummary[SELFL], UPC_ADD, SINGLE_DEST);
        int64_t nISHIRT = reduce_long(bmas[i].libPairSummary[ISHIRT], UPC_ADD, SINGLE_DEST);
        int64_t nEDIST = reduce_long(bmas[i].libPairSummary[EDIST], UPC_ADD, SINGLE_DEST);
        int64_t nREDUN = reduce_long(bmas[i].libPairSummary[REDUN], UPC_ADD, SINGLE_DEST);
        int64_t nACCPT = reduce_long(bmas[i].libPairSummary[ACCPT], UPC_ADD, SINGLE_DEST);

        int64_t tot_spans = reduce_long(my_num_spans / nLibs, UPC_ADD, SINGLE_DEST);

        UPC_LOGGED_BARRIER;

        if (MYTHREAD == 0) {
            total = nTRUNC + nSMALL + nINORM + nINNIE + nSELFL + nEDIST + nREDUN + nACCPT + nISHIRT;
            if (total != 0) {
                printf("LIB: %s\n", bmas[i].bmaFile);
                printf("Number of spans: %lld\n", (lld)tot_spans);
                printf("Percentage of TRUNC: %.3f %% \n", (1.0 * nTRUNC) / (1.0 * total) * 100.0);
                printf("Percentage of SMALL: %.3f %% \n", (1.0 * nSMALL) / (1.0 * total) * 100.0);
                printf("Percentage of INORM: %.3f %% \n", (1.0 * nINORM) / (1.0 * total) * 100.0);
                printf("Percentage of ISHIRT: %.3f %% \n", (1.0 * nISHIRT) / (1.0 * total) * 100.0);
                printf("Percentage of INNIE: %.3f %% \n", (1.0 * nINNIE) / (1.0 * total) * 100.0);
                printf("Percentage of SELFL: %.3f %% \n", (1.0 * nSELFL) / (1.0 * total) * 100.0);
                printf("Percentage of EDIST: %.3f %% \n", (1.0 * nEDIST) / (1.0 * total) * 100.0);
                printf("Percentage of REDUN: %.3f %% \n", (1.0 * nREDUN) / (1.0 * total) * 100.0);
                printf("Percentage of ACCPT: %.3f %% \n", (1.0 * nACCPT) / (1.0 * total) * 100.0);
                printf("Total links processed: %lld\n", (lld)total);
                printf("********************************************\n\n");

                ADD_DIAG("%lld", "total", (lld)total);
                ADD_DIAG("%lld", "trunc", (lld)nTRUNC);
                ADD_DIAG("%lld", "small", (lld)nSMALL);
                ADD_DIAG("%lld", "inorm", (lld)nINORM);
                ADD_DIAG("%lld", "ishirt", (lld)nISHIRT);
                ADD_DIAG("%lld", "innie", (lld)nINNIE);
                ADD_DIAG("%lld", "selfs", (lld)nSELFL);
                ADD_DIAG("%lld", "edist", (lld)nEDIST);
                ADD_DIAG("%lld", "redun", (lld)nREDUN);
                ADD_DIAG("%lld", "accpt", (lld)nACCPT);
            }
        }
        UPC_LOGGED_BARRIER;
    }

    if (MYTHREAD == 0) {
        char countFileName[MAX_FILE_PATH];
        sprintf(countFileName, "nLinks-%d.txt", linkUid);
        get_rank_path(countFileName, -1);
        FILE *countFD = fopen_chk(countFileName, "w+");

        fprintf(countFD, "%lld\n", (lld)PRINTnLinks);
        fclose_track(countFD);

        sprintf(countFileName, "linksMeta-%d", linkUid);
        get_rank_path(countFileName, -1);
        countFD = fopen_chk(countFileName, "w+");
	for (i=0; i<nLibs; i++) {
	  fprintf(countFD, "%s\t%d\t%d\tLINKS_OUTPUT_%d\n", bmas[i].libName, bmas[i].insertSize, bmas[i].stdDev, linkUid);
	}
	
        fclose_track(countFD);
    }

    free_chk(bmas);
    destroy_link_heaps(link_heap);
    free_chk(link_heap);
    free_local_buffs(&local_buffs, &local_index);
    free_local_heaps(&datum_heap, &linkList_heap);
    destroy_link_hash_table(&local_hashtable);
    UPC_LOGGED_BARRIER;

#ifdef FIND_DENSITY
    if (sharedArrayOfDensities != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfDensities);
    }
    if (sharedArrayOfLengths != NULL) {
        UPC_ALL_FREE_CHK(sharedArrayOfLengths);
    }
#endif

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
