#ifndef __BMA_TO_LINKS_UTILS_H
#define __BMA_TO_LINKS_UTILS_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <upc.h>

#include "defines.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "hash_funcs.h"
#include "bmaToLinks.h"
#include "bma_meta.h"


#define MAX_END_SEPARATION 64000        // Maximum end separation
#define OFFSET (MAX_END_SEPARATION / 2) // default offset to accomodate negative separation values
#define SPLINT_UNDEFINED (-999999999)
#define SPLINT 0
#define PAIR 1

#define EXP_FACTOR 2

#define EPSILON 1e-15

#define UNDEFINED 0
#define CONVERGENT 1
#define DIVERGENT 2


/* Split a bma entry */
int split_bma_entry(char *line, char *col0, char *col1, char *col2, char *col3, char *col4, char *col5);

/* Split blastmapped info existent in bma files */
int split_bmap_info(char *info, char *readStatus, char *readName, int *readStart, int *readEnd, int *readLength, char *contig_name, int *contigStart, int *contigEnd, int *contigLength, int *strand);

/* Splits a read status */
int split_read_status(char *readIn, char *leftStat, char *rightStat, char *locStat);

/* Splits a splint */
int processSplint(char *end1, char *align1, char *end2, char *align2, bma_info *bma, int64_t *local_index, link_t *local_buffs, link_heap_t link_heap, int countOnly);

/* Process a pair */
int processPair(char *end1, char *align1, char *end2, char *align2, bma_info *bma, int64_t *local_index, link_t *local_buffs, link_heap_t link_heap, int *previousMaxInsertSize, int minNetEndDistance, int countOnly);

#endif // __BMA_TO_LINKS_UTILS_H
