#include "bma_meta.h"

char *next_tok(char *line, char *s, char **aux)
{
    char *token = strtok_r(s, "\t", aux);

    if (!token) {
        DIE("null token for line %s\n", line);
    }
    if (!strlen(token)) {
        DIE("token is empty for line %s\n", line);
    }
    int len = strlen(token);
    if (token[len - 1] == '\n') {
        token[len - 1] = 0;
    }
    return token;
}

/* Split a bma line */
int split_bmaLine(char *line, struct bma_info *bma, int lib_id)
{
    char *aux;
    int len;

    char *orig_line = strdup_chk(line);

    bma->insertSize = atoi(next_tok(orig_line, line, &aux));
    bma->stdDev = atoi(next_tok(orig_line, NULL, &aux));
    bma->innieRemoval = atoi(next_tok(orig_line, NULL, &aux));
    bma->readLength = atoi(next_tok(orig_line, NULL, &aux));
    strcpy(bma->bmaFile, next_tok(orig_line, NULL, &aux));
    bma->lib_id = lib_id;
    free_chk(orig_line);
    return 1;
}
