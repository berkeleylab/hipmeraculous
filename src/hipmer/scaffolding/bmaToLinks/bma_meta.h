#ifndef BMA_META_H
#define BMA_META_H

#include <assert.h>

#include "upc_common.h"
#include "common.h"

/* Pair Categories */
#define REASONS 9
#define TRUNCA 0
#define SMALL 1
#define INORM 2
#define ISHIRT 3
#define INNIE 4
#define SELFL 5
#define EDIST 6
#define REDUN 7
#define ACCPT 8
#define MY_LINESIZE 4096             // linesize for bmaDataFile

typedef struct bma_info {
    char    bmaFile[MAX_FILE_PATH];
    char    libName[LIB_NAME_LEN];
    int     lib_id;
    int     insertSize;
    int     stdDev;
    int     readLength;
    int64_t libPairSummary[REASONS];
    int     innieRemoval;
} bma_info;

char *next_tok(char *line, char *s, char **aux);

/* Split a bma line */
int split_bmaLine(char *line, struct bma_info *bma, int lib_id);

#endif // BMA_META_H
