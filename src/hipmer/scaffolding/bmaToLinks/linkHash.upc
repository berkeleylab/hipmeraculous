#include "linkHash.h"

/* Creates shared heaps that required for links hash table - IMPORTANT: This is a collective function */
int create_link_heaps(int64_t my_size, link_heap_t *link_heap)
{
    DBG("Allocating %lld link heaps\n", (lld)my_size);
    memset(link_heap, 0, sizeof(*link_heap));
    UPC_ALL_ALLOC_CHK(link_heap->link_ptr, THREADS, sizeof(shared_link_ptr));
    UPC_ALL_ALLOC_CHK(link_heap->heap_indices, THREADS, sizeof(int64_t));
    UPC_ALL_ALLOC_CHK(link_heap->heap_sizes, THREADS, sizeof(int64_t));
    if (my_size > 0) {
        UPC_ALLOC_CHK(link_heap->link_ptr[MYTHREAD], my_size * sizeof(link_t));
    }
    link_heap->heap_indices[MYTHREAD] = 0;
    link_heap->heap_sizes[MYTHREAD] = my_size;

    upc_barrier;

    return 0;
}

void destroy_link_heaps(link_heap_t *link_heap)
{
    if (link_heap == NULL) {
        DIE("Invalid call to destroy_link_heaps\n");
    }
    assert(link_heap->link_ptr != NULL);
    assert(link_heap->heap_indices != NULL);
    assert(link_heap->heap_sizes != NULL);
    if (link_heap->heap_sizes[MYTHREAD] > 0) {
        assert(link_heap->link_ptr[MYTHREAD] != NULL);
    }
    if(link_heap->heap_sizes[MYTHREAD] < link_heap->heap_indices[MYTHREAD]) {
        DIE("Overflow in link_heaps index=%lld vs allocated mySize=%lld\n", (lld) link_heap->heap_indices[MYTHREAD], (lld) link_heap->heap_sizes[MYTHREAD] );
    }

    LOGF("destroy_link_heaps final count=%lld count=%lld\n", (lld) link_heap->heap_indices[MYTHREAD], (lld) link_heap->heap_sizes[MYTHREAD]);
    if ( link_heap->heap_indices[MYTHREAD] != link_heap->heap_sizes[MYTHREAD]) {
        WARN("Over allocated link heaps final count=%lld count=%lld\n", (lld) link_heap->heap_indices[MYTHREAD], (lld) link_heap->heap_sizes[MYTHREAD]);
    }
    if (link_heap->heap_sizes[MYTHREAD] > 0) {
        UPC_FREE_CHK(link_heap->link_ptr[MYTHREAD]);
    }
    UPC_ALL_FREE_CHK(link_heap->link_ptr);
    UPC_ALL_FREE_CHK(link_heap->heap_indices);
    UPC_ALL_FREE_CHK(link_heap->heap_sizes);
}

/* Allocate local arrays used for book-keeping when using aggregated upc_memputs */
int allocate_local_buffs(link_t **local_buffs, int64_t **local_index)
{
    (*local_buffs) = (link_t *)malloc_chk(((int64_t)THREADS) * LINK_CHUNK_SIZE * sizeof(link_t));
    (*local_index) = (int64_t *)malloc_chk(((int64_t)THREADS) * sizeof(int64_t));
    memset((*local_index), 0, ((int64_t)THREADS) * sizeof(int64_t));
    return 0;
}

void free_local_buffs(link_t **local_buffs, int64_t **local_index)
{
    for(int i = 0; i < THREADS; i++) {
        if ((*local_index)[i] > LINK_CHUNK_SIZE) DIE("Overflow in local_buffs! index=%lld vs size=%lld, thread=%d\n", (lld) (*local_index)[i], (lld) LINK_CHUNK_SIZE, i);
    }
    _free_chk(local_buffs);
    _free_chk(local_index);
}

/* Adds a link to the shared link heap */
int add_link_to_shared_heaps(link_t *new_entry, int64_t hashval, int64_t *local_index, link_t *local_buffs, link_heap_t link_heap, int count_only)
{
    int64_t store_pos;
    int remote_thread = hashval % THREADS;

    if (count_only) {
        local_index[remote_thread]++;
        return 0;
    }

    if (local_index[remote_thread] >= LINK_CHUNK_SIZE) {
        DIE("Overflow in local_buffs! index=%lld size=%lld thread=%d\n", (lld) local_index[remote_thread], (lld) LINK_CHUNK_SIZE, remote_thread);
    }

    /* Store link first to local buffer designated for remote thread */
    if (local_index[remote_thread] <= LINK_CHUNK_SIZE - 1) {
        memcpy(&(local_buffs[local_index[remote_thread] + remote_thread * LINK_CHUNK_SIZE]), new_entry, sizeof(link_t));
        local_index[remote_thread]++;
    }

    /* If buffer for that thread is full, do a remote upc_memput() */
    if (local_index[remote_thread] == LINK_CHUNK_SIZE) {
        UPC_ATOMIC_FADD_I64(&store_pos, &(link_heap.heap_indices[remote_thread]), LINK_CHUNK_SIZE);
#ifdef DEBUG
        assert(store_pos + LINK_CHUNK_SIZE <= link_heap.heap_sizes[remote_thread]);
#endif
        upc_memput((shared[] link_t *)((link_heap.link_ptr[remote_thread]) + store_pos), &(local_buffs[remote_thread * LINK_CHUNK_SIZE]), (LINK_CHUNK_SIZE)*sizeof(link_t));
        local_index[remote_thread] = 0;
    }

    return 0;
}

/* Adds remaining links to the shared heaps. Should be called when all calls "add_link_to_shared_heaps()" have been done */
int add_rest_links_to_shared_heaps(int64_t *local_index, link_t *local_buffs, link_heap_t link_heap)
{
    int i;
    int64_t store_pos;

    for (i = 0; i < THREADS; i++) {
        if (local_index[i] != 0) {
            if (local_index[i] > LINK_CHUNK_SIZE) {
                DIE("Overflow in local_buffs! index=%lld size=%lld thread=%d\n", (lld) local_index[i], (lld) LINK_CHUNK_SIZE, i);
            }
            UPC_ATOMIC_FADD_I64(&store_pos, &(link_heap.heap_indices[i]), local_index[i]);
#ifdef DEBUG
            assert(store_pos + local_index[i] <= link_heap.heap_sizes[i]);
#endif
            upc_memput((shared[] link_t *)((link_heap.link_ptr[i]) + store_pos), &(local_buffs[i * LINK_CHUNK_SIZE]), (local_index[i]) * sizeof(link_t));
        }
    }

    return 0;
}
/*
 * int64_t linkhash(int64_t linkhash_size, char *linkname)
 * {
 * unsigned long hashval;
 * hashval = 5381;
 * for(; *linkname != '\0'; linkname++) hashval = (*linkname) +  (hashval << 5) + hashval;
 * return hashval % linkhash_size;
 * }
 */

/* Create a link_hash_table */
link_hash_table_t *create_link_hash_table(int64_t size)
{
    link_hash_table_t *new_table;
    int64_t i;

    if (size < 1) {
        size = 1;
    }

    /* Attempt to allocate memory for the table structure */
    if ((new_table = (link_hash_table_t *)malloc_chk(sizeof(link_hash_table_t))) == NULL) {
        return NULL;
    }

    /* Attempt to allocate memory for the table itself */
    if ((new_table->table = malloc_chk(sizeof(linkList_t *) * size)) == NULL) {
        return NULL;
    }

    /* Initialize the elements of the table */
    for (i = 0; i < size; i++) {
        new_table->table[i] = NULL;
    }

    /* Set the table's size */
    new_table->size = size;

    return new_table;
}

void destroy_link_hash_table(link_hash_table_t **_ht)
{
    assert(_ht);
    link_hash_table_t *ht = *_ht;
    assert(ht);
    free_chk(ht->table);
    _free_chk(_ht);
}

char compressTails(char tail1, char tail2)
{
    char result = 0;

    if ((tail1 == '3') && (tail2 == '3')) {
        result = 0;
    }

    if ((tail1 == '3') && (tail2 == '5')) {
        result = 1;
    }

    if ((tail1 == '5') && (tail2 == '3')) {
        result = 2;
    }

    if ((tail1 == '5') && (tail2 == '5')) {
        result = 3;
    }

    return result;
}

int64_t link_hash_val(int64_t e1, int64_t e2, char compressedTails, int64_t linkhash_size)
{
    char linkname[LINK_CHUNK_SIZE];
    int tail1, tail2;

    if (compressedTails == 0) {
        tail1 = 3;
        tail2 = 3;
    }
    if (compressedTails == 1) {
        tail1 = 3;
        tail2 = 5;
    }
    if (compressedTails == 2) {
        tail1 = 5;
        tail2 = 3;
    }
    if (compressedTails == 3) {
        tail1 = 5;
        tail2 = 5;
    }

    char linkAux1[LINK_CHUNK_SIZE];
    char linkAux2[LINK_CHUNK_SIZE];

    sprintf_chk(linkAux1, LINK_CHUNK_SIZE, "%lld.%d", (lld)e1, tail1);
    sprintf_chk(linkAux2, LINK_CHUNK_SIZE, "%lld.%d", (lld)e2, tail2);

    if (strcmp(linkAux1, linkAux2) < 0) {
        sprintf_chk(linkname, LINK_CHUNK_SIZE, "%lld.%d<=>%lld.%d", (lld)e1, tail1, (lld)e2, tail2);
    } else {
        sprintf_chk(linkname, LINK_CHUNK_SIZE, "%lld.%d<=>%lld.%d", (lld)e2, tail2, (lld)e1, tail1);
    }
    //printf("Example linkname %s\n", linkname);


    return hashstr(linkhash_size, linkname);
}

/* Lookup function for link hash table */
linkList_t *lookup_link(link_hash_table_t *hashtable, int e1, int e2, char compressedTails, unsigned char type, int64_t hashval)
{
    linkList_t *list;

    for (list = hashtable->table[hashval]; list != NULL; list = list->next) {
        if ((e1 == list->end1_id) && (e2 == list->end2_id) && (type == list->link_type) && (compressedTails == list->compressedTails)) {
            return list;
        }
    }
    return NULL;
}

/* Implement efficient memory allocator to avoid multiple malloc() calls */
int create_local_heaps(int64_t max_size, datum_t **datum_heap, linkList_t **linkList_heap)
{
    if (max_size < 1) {
        max_size = 1;
    }
    (*datum_heap) = (datum_t *)malloc_chk(max_size * sizeof(datum_t));
    (*linkList_heap) = (linkList_t *)malloc_chk(max_size * sizeof(linkList_t));
    return 0;
}

/* Add link data to the hashtable */
int add_link_data(link_hash_table_t *hashtable, link_t *cur_link, int64_t *datum_heap_pos, datum_t *datum_heap, int64_t *link_heap_pos, linkList_t *linkList_heap, bma_info *bmas, int64_t *nLinks, GZIP_FILE myLog)
{
    linkList_t *new_bucket;
    datum_t *new_datum, *span_data;
    linkList_t *lookup_res;
    int64_t hashval;
    int d1, d2;
    int found;

    hashval = link_hash_val(cur_link->end1_id, cur_link->end2_id, cur_link->compressedTails, hashtable->size);
    lookup_res = lookup_link(hashtable, cur_link->end1_id, cur_link->end2_id, cur_link->compressedTails, cur_link->link_type, hashval);
    int cur_lib;

#ifdef DEBUG
    char linkAux1[LINK_CHUNK_SIZE];
    char linkAux2[LINK_CHUNK_SIZE];
    char tail2, tail1;

#endif

    if (lookup_res == NULL) {
        /* Should reserve a bucket from the appropriate heap and insert that to the table */
        if (myLog != 0) {
            long long ll_link_heap_pos_ptr = *link_heap_pos;
            GZIP_PRINTF(myLog, "Thread %d: Storing %lld at hash[%lld]\n",
                        MYTHREAD, ll_link_heap_pos_ptr, (lld)hashval);
        }
        new_bucket = (linkList_t *)(linkList_heap + (*link_heap_pos));
        (*link_heap_pos)++;
        new_bucket->end1_id = cur_link->end1_id;
        new_bucket->end2_id = cur_link->end2_id;
        new_bucket->compressedTails = cur_link->compressedTails;
        new_bucket->link_type = cur_link->link_type;
        new_bucket->o1_length = cur_link->o1_length;
        new_bucket->o2_length = cur_link->o2_length;
        new_bucket->data = NULL;
        new_bucket->next = hashtable->table[hashval];
        hashtable->table[hashval] = new_bucket;

        lookup_res = new_bucket;
#ifdef DEBUG
        if (new_bucket->compressedTails == 0) {
            tail1 = 3;
            tail2 = 3;
        }
        if (new_bucket->compressedTails == 1) {
            tail1 = 3;
            tail2 = 5;
        }
        if (new_bucket->compressedTails == 2) {
            tail1 = 5;
            tail2 = 3;
        }
        if (new_bucket->compressedTails == 3) {
            tail1 = 5;
            tail2 = 5;
        }

        sprintf_chk(linkAux1, LINK_CHUNK_SIZE, "%lld.%d", (lld)new_bucket->end1_id, tail1);
        sprintf_chk(linkAux2, LINK_CHUNK_SIZE, "%lld.%d", (lld)new_bucket->end2_id, tail2);
        if (myLog != NULL) {
            GZIP_PRINTF(myLog, "%s<=>%s\n", linkAux1, linkAux2);
        }
#endif
        (*nLinks)++;
    } else {
        /* For SPANs check if alreads data with d1.d2 exist -- Equivalent to redundancy check */
        if (cur_link->link_type == SPAN) {
            span_data = lookup_res->data;
            d1 = cur_link->d1;
            d2 = cur_link->d2;
            while (span_data != NULL) {
                /* Do not add the same datum, i.e. same d1 and d2 since it is redundant */
                if ((span_data->d1 == d1) && (span_data->d2 == d2)) {
                    cur_lib = cur_link->lib_id;
                    bmas[cur_lib].libPairSummary[REDUN] += 1;
                    (span_data->count)++;
                    return 0;
                }
                span_data = span_data->next;
            }
        }
    }

    /* Add new datum to bucket's chain */
    if (myLog != NULL) {
        long long ll_datum_heap_pos_ptr = *datum_heap_pos;
        GZIP_PRINTF(myLog, "Thread %d: adding new datum at %lld\n", MYTHREAD, ll_datum_heap_pos_ptr);
    }
    new_datum = (datum_t *)(datum_heap + (*datum_heap_pos));
    (*datum_heap_pos)++;
    new_datum->next = lookup_res->data;
    lookup_res->data = new_datum;
    new_datum->endSeparation = cur_link->endSeparation;
    new_datum->lib_id = cur_link->lib_id;
    if (cur_link->link_type == SPAN) {
        if (myLog != NULL) {
            GZIP_PRINTF(myLog, "Thread %d: adding SPAN link for lib %d\n", MYTHREAD, cur_link->lib_id);
        }
        new_datum->d1 = cur_link->d1;
        new_datum->d2 = cur_link->d2;
        new_datum->count = 1;
        cur_lib = cur_link->lib_id;
        bmas[cur_lib].libPairSummary[ACCPT] += 1;
    }
    if (myLog != NULL) {
        GZIP_PRINTF(myLog, "Thread %d: added_link_data\n", MYTHREAD);
    }

    return 1;
}

/* Free routines */
int free_local_heaps(datum_t **datum_heap, linkList_t **linkList_heap)
{
    _free_chk(datum_heap);
    _free_chk(linkList_heap);

    return 0;
}
