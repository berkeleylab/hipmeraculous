#include <stdbool.h>
#include "upc_compatibility.h"
#include "../histogrammer/analyzerUtils.h"



/* Checks if the read-to-contig alignment meets some criteria and returns appropriate characterization */
static inline int findReadStatus(int strand, int qStart, int qStop, int qLength, int sStart, int sStop, int sLength, int fivePrimeWiggleRoomIn, int threePrimeWiggleRoomIn, int *startStatusRes, int *endStatusRes, int *locationRes, int64_t *my_uninformative, int64_t *my_truncated)
{
    int endDistance, location;
    int fivePrimeWiggleRoom = fivePrimeWiggleRoomIn;
    int threePrimeWiggleRoom = threePrimeWiggleRoomIn;
    int missingStartBases, startStatus, unalignedEnd, projectedEnd, endStatus, missingEndBases;

    int unalignedStart = qStart - 1;
    int projectedStart = (strand == PLUS) ? sStart - unalignedStart : sStop + unalignedStart;
    int projectedOff = 0;

    if (projectedStart < 1) {
        projectedOff = 1 - projectedStart;
    } else if (projectedStart > sLength) {
        projectedOff = projectedStart - sLength;
    }
    missingStartBases = unalignedStart - projectedOff;

    if (unalignedStart == 0) {
        startStatus = FULL;
    } else if ((projectedOff > 0) && (missingStartBases < fivePrimeWiggleRoom)) {
        startStatus = GAP;
    } else if (unalignedStart < fivePrimeWiggleRoom) {
        startStatus = INC;
    } else {
        startStatus = TRUNC;
    }

    unalignedEnd = qLength - qStop;
    projectedEnd = (strand == PLUS) ? sStop + unalignedEnd : sStart - unalignedEnd;
    projectedOff = 0;
    if (projectedEnd < 1) {
        projectedOff = 1 - projectedEnd;
    } else if (projectedEnd > sLength) {
        projectedOff = projectedEnd - sLength;
    }
    missingEndBases = unalignedEnd - projectedOff;
    if (unalignedEnd == 0) {
        endStatus = FULL;
    } else if ((projectedOff > 0) && (missingEndBases < threePrimeWiggleRoom)) {
        endStatus = GAP;
    } else if (unalignedEnd < threePrimeWiggleRoom) {
        endStatus = INC;
    } else {
        endStatus = TRUNC;
    }

    endDistance = qLength;

    location = UNK;
    if (strand == PLUS) {
        if (projectedStart > (sLength - endDistance)) {
            location = OUT;
        } else if (projectedEnd < endDistance) {
            location = IN;
        } else {
            location = MID;
        }
    } else {
        if (projectedStart < endDistance) {
            location = OUT;
        } else if (projectedEnd > (sLength - endDistance)) {
            location = IN;
        } else {
            location = MID;
        }
    }

    (*startStatusRes) = startStatus;
    (*endStatusRes) = endStatus;
    (*locationRes) = location;

    if ((startStatus == TRUNC) || (endStatus == TRUNC)) {
        (*my_truncated)++;
        return FAIL;
    }

    if (!((startStatus == GAP) || (endStatus == GAP))) {
        (*my_uninformative)++;
        return FAIL;
    }

    if ((startStatus == GAP) || (endStatus == GAP)) {
        return SUCCESS;
    }

    return FAIL;
}

static inline int assignStrings(int startStatus, int endStatus, int location, char const **startStatus_ptr, char const **endStatus_ptr, char const **location_ptr, const char *inc, const char *gap, const char *trunc, const char *ins, const char *outs, const char *mid, const char *full)
{
    *startStatus_ptr = NULL;
    if (startStatus == FULL) {
        (*startStatus_ptr) = full;
    } else if (startStatus == INC) {
        (*startStatus_ptr) = inc;
    } else if (startStatus == GAP) {
        (*startStatus_ptr) = gap;
    } else if (startStatus == TRUNC) {
        (*startStatus_ptr) = trunc;
    } else { LOGF("assignStrings failed to assign startStatus %d!\n", startStatus); }

    *endStatus_ptr = NULL;
    if (endStatus == FULL) {
        (*endStatus_ptr) = full;
    } else if (endStatus == INC) {
        (*endStatus_ptr) = inc;
    } else if (endStatus == GAP) {
        (*endStatus_ptr) = gap;
    } else if (endStatus == TRUNC) {
        (*endStatus_ptr) = trunc;
    } else { LOGF("assignStrings failed to assign endStatus %d!\n", endStatus); }

    *location_ptr = NULL;
    if (location == IN) {
        (*location_ptr) = ins;
    } else if (location == OUT) {
        (*location_ptr) = outs;
    } else if (location == MID) {
        (*location_ptr) = mid;
    } else { LOGF("assignStrings failed to assign location %d!\n", location); }

    return 0;
}

/* Processes current splint and prints appropriate info */
static inline int processAlignment(align_info *cur_alignments, int nAligns, int64_t *nSplintsFound, GZIP_FILE outFD)
{
    int prev_pos, cur_pos, thisContig, nextContig, thisContigEnd, nextContigEnd, thisStrand, nextStrand, thisStopStat, nextStartStat;
    const char *inc = "INC", *gap = "GAP", *trunc = "TRUNC", *ins = "IN", *outs = "OUT", *mid = "MID", *full = "FULL";
    const char *pl = "Plus", *mi = "Minus";
    char const *prev_start_status, *cur_start_status, *prev_loc, *cur_loc, *prev_end_status, *cur_end_status;
    char const *prev_strand, *cur_strand;

    UPC_TICK_T start_io, end_io;


    /* IMPORTANT: According to merAligner, alignments should be already sorted by the qStart index */
#ifdef DEBUG
    for (prev_pos = 0; prev_pos < nAligns - 2; prev_pos++) {
      assert(cur_alignments->aligned_query_locs[2*prev_pos] <= cur_alignments->aligned_query_locs[2*(prev_pos+1)]);
    }
#endif

    int thisStop, nextStart, overlap;

    for (prev_pos = 0; prev_pos < nAligns - 1; prev_pos++) {

      for (int offset = 1; offset < nAligns - prev_pos; offset++) {   // check every alignment in cur_alignments (sorted by start pos)  against ALL the downstream alignments
        cur_pos = prev_pos + offset;
        thisContig = cur_alignments->aligned_subject_ids[prev_pos];
        nextContig = cur_alignments->aligned_subject_ids[cur_pos];

        if (thisContig != nextContig) {
            thisStopStat = cur_alignments->endStatus[prev_pos];
            nextStartStat = cur_alignments->startStatus[cur_pos];

            if ((thisStopStat == GAP) && (nextStartStat == GAP)) {
                thisStrand = cur_alignments->aligned_strand[prev_pos];
                nextStrand = cur_alignments->aligned_strand[cur_pos];
                thisContigEnd = (thisStrand == PLUS) ? 3 : 5;
                nextContigEnd = (nextStrand == PLUS) ? 5 : 3;

                assignStrings(cur_alignments->startStatus[prev_pos], cur_alignments->endStatus[prev_pos], cur_alignments->location[prev_pos], &prev_start_status, &prev_end_status, &prev_loc, inc, gap, trunc, ins, outs, mid, full);
                prev_strand = (cur_alignments->aligned_strand[prev_pos] == PLUS) ? pl : mi;

                assignStrings(cur_alignments->startStatus[cur_pos], cur_alignments->endStatus[cur_pos], cur_alignments->location[cur_pos], &cur_start_status, &cur_end_status, &cur_loc, inc, gap, trunc, ins, outs, mid, full);
                cur_strand = (cur_alignments->aligned_strand[cur_pos] == PLUS) ? pl : mi;

                thisStop = cur_alignments->aligned_query_locs[2 * prev_pos + 1];
                nextStart = cur_alignments->aligned_query_locs[2 * cur_pos];
                overlap = thisStop - nextStart + 1;

                {
                    start_io = UPC_TICKS_NOW();
                    GZIP_PRINTF(outFD, "SINGLE\tSPLINT\tContig%lld.%d\t[%s.%s.%s %s %d %d %d Contig%lld %d %d %d %s]\tContig%lld.%d\t[%s.%s.%s %s %d %d %d Contig%lld %d %d %d %s]\n", (lld)thisContig, thisContigEnd, prev_start_status, prev_end_status, prev_loc, cur_alignments->query_name, cur_alignments->aligned_query_locs[2 * prev_pos], cur_alignments->aligned_query_locs[2 * prev_pos + 1], cur_alignments->query_length, (lld)cur_alignments->aligned_subject_ids[prev_pos], cur_alignments->aligned_subject_locs[2 * prev_pos], cur_alignments->aligned_subject_locs[2 * prev_pos + 1], cur_alignments->aligned_subject_lengths[prev_pos], prev_strand, (lld)nextContig, nextContigEnd, cur_start_status, cur_end_status, cur_loc, cur_alignments->query_name, cur_alignments->aligned_query_locs[2 * cur_pos], cur_alignments->aligned_query_locs[2 * cur_pos + 1], cur_alignments->query_length, (lld)cur_alignments->aligned_subject_ids[cur_pos], cur_alignments->aligned_subject_locs[2 * cur_pos], cur_alignments->aligned_subject_locs[2 * cur_pos + 1], cur_alignments->aligned_subject_lengths[cur_pos], cur_strand);
                    (*nSplintsFound) += 1;
                    end_io = UPC_TICKS_NOW();
                    write_io_time += UPC_TICKS_TO_SECS(end_io - start_io);
                }
            }
        }
      }
    }

    return 0;
}

static inline char *sgets(char *str, int num, char **input)
{
    char *next = *input;
    int numread = 0;
    int strpos = 0;

    while (numread + 1 < num && *next) {
        int isnewline = (*next == '\n');

        str[strpos] = *next++;
        strpos++;
        numread++;
        // newline terminates the line but is included
        if (isnewline) {
            break;
        }
    }

    if (numread == 0) {
        return NULL; // "eof"
    }
    // must have hit the null terminator or end of line
    str[strpos] = '\0'; // null terminate this tring
    // set up input for next call
    *input = next;
    return str;
}
