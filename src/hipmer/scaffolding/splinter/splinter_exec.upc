#include <upc.h>

#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"

int splinter_main(int argc, char **argv);

int main(int argc, char **argv)
{
    OPEN_MY_LOG("splinter");
    return splinter_main(argc, argv);
}
