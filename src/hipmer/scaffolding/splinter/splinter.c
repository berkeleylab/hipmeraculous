#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

static shared int64_t tot_splints;
static shared int64_t singletons;
static shared int64_t uninformatives;
static shared int64_t truncated;
static shared int64_t tot_excessiveAlns;

static double read_io_time = 0.0;
static double write_io_time = 0.0;

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "upc_output.h"
#include "splinterUtils.h"




int splinter_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    read_io_time = 0;
    write_io_time = 0;

    if (!MYTHREAD) {
        tot_splints = 0;
        singletons = 0;
        uninformatives = 0;
        truncated = 0;
        tot_excessiveAlns = 0;
    }
    UPC_LOGGED_BARRIER;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "l:m:F:T:r:B:a:P:L:");
    print_args(optList, __func__);
    GZIP_FILE laneFD1;
    GZIP_FILE outFD;
    char *libname = NULL;
    int minMatch, fivePrimeWiggleRoom = 5, threePrimeWiggleRoom = 5;
    char merAlignerOutputFname[MAX_FILE_PATH];
    char outputSplinter[MAX_FILE_PATH];
    char *merAlignerSuffixName;
    int scaffoldRound = 0;
    align_info *cur_alignments = (align_info *)calloc_chk(sizeof(align_info), 1); // do not allocate this large struct on the stack
    int filesPerPair = -1;
    char *lineBuffers = (char *)calloc_chk(MAX_LINE_SIZE * 3, 1);
    char *cur_read = lineBuffers;
    char *prev_read = cur_read + MAX_LINE_SIZE;
    char *align = prev_read + MAX_LINE_SIZE;
    int64_t subject;
    int splitRes, qStart, qStop, qLength, sStart, sStop, sLength, strand, cur_pos;
    int64_t splints_found = 0;
    UPC_TICK_T start, end;
    int testResult, startStatusRes, endStatusRes, locationRes;
    int64_t my_sigleton = 0;
    int64_t my_uninformative = 0;
    int64_t my_truncated = 0;
    int64_t my_splints = 0;

    UPC_TICK_T start_io, end_io;

    const char *base_dir = ".";

    cur_read[0] = '\0';
    prev_read[0] = '\0';

    if (MYTHREAD == 0) {
        singletons = 0;
        uninformatives = 0;
        truncated = 0;
    }
    UPC_LOGGED_BARRIER;

    char my_nContigsFile[255];
    FILE *my_nC_fd;
    int readLength = 0;
    char *merAlignerOutput = NULL;

    /* Process the input arguments */
    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'l':
            libname = thisOpt->argument;
            break;
        case 'T':
            threePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 'F':
            fivePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 'm':
            minMatch = atoi(thisOpt->argument);
            break;
        case 'r':
            readLength = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'a':
            merAlignerOutput = thisOpt->argument;
            break;
        case 'P':
            filesPerPair = atoi(thisOpt->argument);
            break;
        case 'L':
            scaffoldRound = atoi(thisOpt->argument);
            break;
        default:
            break;
        }
    }
    if (libname == NULL) {
        DIE("Please specify a -l libname\n");
    }

    UPC_LOGGED_BARRIER;
    start = UPC_TICKS_NOW();

    int64_t excessiveAlns = 0;
    sprintf(outputSplinter, "%s-splints-%d" GZIP_EXT, libname, scaffoldRound);

    outFD = openCheckpoint(outputSplinter, "w");
    for (int readNumber = 1; readNumber <= 2; readNumber++) {
        sprintf(merAlignerOutputFname, "%s-%s_Read%d" GZIP_EXT, libname, merAlignerOutput, readNumber);
        int exists = doesCheckpointExist(merAlignerOutputFname);
        if (readNumber == 2 && (filesPerPair == 0 || !exists)) {
            LOGF("skippint read2: readNumber=%d filesPerPair=%d exists=%d checkpointName=%s\n", readNumber, filesPerPair, exists, merAlignerOutputFname);
            break;                                                                                    // do not process Read2 for unpaired files
        }
        laneFD1 = openCheckpoint(merAlignerOutputFname, "r");
        if (laneFD1 == NULL) {
            if (readNumber == 1) {
                DIE("Could not open %s!\n", merAlignerOutputFname);
            }
            break; // otherwise okay -- may be unpaired library
        }

        cur_pos = 0;
#ifdef DEBUG
        int report = 0;
#endif

        start_io = UPC_TICKS_NOW();

        char *fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, laneFD1);

        end_io = UPC_TICKS_NOW();
        read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);

        while (fgets_result != NULL) {
            splitRes = splitAlignment(align, cur_read, &qStart, &qStop, &qLength, &subject, &sStart, &sStop, &sLength, &strand);

            if (splitRes == 0) {
                /* Found a "guard" alignment */
	      
                /* Process the alignment iff current read alignments  >= 2 */
                if (cur_pos > 1) {
                    processAlignment(cur_alignments, cur_pos, &splints_found, outFD);
                } else if (cur_pos == 1) {
                    my_sigleton++;
                }
                /* Reset the ptr in the cur_alignments data structure */
                cur_pos = 0;
            } else {
                /* If the current read (just read) is different than the previous one -> process the previous alignments */
                if (strcmp(cur_read, prev_read) != 0) {
                    if (cur_pos > 1) {
                        processAlignment(cur_alignments, cur_pos, &splints_found, outFD);
                    } else if (cur_pos == 1) {
                        my_sigleton++;
                    }
                    /* Reset the ptr in the cur_alignments data structure */
                    cur_pos = 0;
                    strcpy(prev_read, cur_read);
                }

                /* Test the status of the current read */
                testResult = findReadStatus(strand, qStart, qStop, qLength, sStart, sStop, sLength, fivePrimeWiggleRoom, threePrimeWiggleRoom, &startStatusRes, &endStatusRes, &locationRes, &my_uninformative, &my_truncated);

                if (testResult == SUCCESS) {
                    /* Store alignment info in the data structure */
                    if (cur_pos == 0) {
                        if (cur_alignments->query_name) {
                            free_chk0(cur_alignments->query_name);
                        }
                        cur_alignments->query_name = strdup_chk0(cur_read);
                    }
                    if (cur_pos < MAX_ALIGN) {
                        cur_alignments->query_length = qLength;
                        cur_alignments->aligned_query_locs[2 * cur_pos] = qStart;
                        cur_alignments->aligned_query_locs[2 * cur_pos + 1] = qStop;
                        cur_alignments->aligned_subject_ids[cur_pos] = subject;
                        cur_alignments->aligned_subject_lengths[cur_pos] = sLength;
                        cur_alignments->aligned_subject_locs[2 * cur_pos] = sStart;
                        cur_alignments->aligned_subject_locs[2 * cur_pos + 1] = sStop;
                        cur_alignments->aligned_strand[cur_pos] = strand;
                        cur_alignments->location[cur_pos] = locationRes;
                        cur_alignments->startStatus[cur_pos] = startStatusRes;
                        cur_alignments->endStatus[cur_pos] = endStatusRes;
                        cur_pos++;
                    } else {
                        excessiveAlns++;
                    }
                }
            }

            start_io = UPC_TICKS_NOW();

            fgets_result = GZIP_GETS(align, MAX_LINE_SIZE, laneFD1);

            end_io = UPC_TICKS_NOW();
            read_io_time += UPC_TICKS_TO_SECS(end_io - start_io);
        }

        /* Process the last alignment left in the cur_alignment data structure (if any) */
        if (cur_pos > 1) {
            processAlignment(cur_alignments, cur_pos, &splints_found, outFD);
        } else if (cur_pos == 1) {
            my_sigleton++;
        }
        closeCheckpoint(laneFD1);
    } // iterate over files
    closeCheckpoint(outFD);
    if (cur_alignments->query_name) {
        free_chk0(cur_alignments->query_name);
    }
    free_chk(cur_alignments);

    // TODO replace with reduce
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &singletons, my_sigleton);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &truncated, my_truncated);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &uninformatives, my_uninformative);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &tot_splints, splints_found);
    if (excessiveAlns) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &tot_excessiveAlns, excessiveAlns);
    }

    UPC_LOGGED_BARRIER;

    end = UPC_TICKS_NOW();
    if (MYTHREAD == 0) {
        printf("Splints found: %lld\n", (lld)tot_splints);
        printf("Unused alignments:\n");
        printf("UNINFORMATIVE:\t%lld\n", (lld)uninformatives);
        printf("TRUNCATED:\t%lld\n", (lld)truncated);
        printf("SINGLETON:\t%lld\n", (lld)singletons);
        printf("Excessive:\t%lld\n", (lld)tot_excessiveAlns);
        printf("\nTime for computing SPLINTS : %d seconds\n", ((int)UPC_TICKS_TO_SECS(end - start)));
        printf("\nRead I/O time is : %f seconds\n", read_io_time);
        printf("Write I/O time is : %f seconds\n", write_io_time);
        printf("Pure computation time is : %f seconds\n", (((int)UPC_TICKS_TO_SECS(end - start))) - (write_io_time + read_io_time));

        ADD_DIAG("%lld", "splints_found", (lld)tot_splints);
        ADD_DIAG("%lld", "unused_alns_uninformative", (lld)uninformatives);
        ADD_DIAG("%lld", "unused_alns_truncated", (lld)truncated);
        ADD_DIAG("%lld", "unused_alns_singleton", (lld)singletons);

        char countFileName[MAX_FILE_PATH];
        sprintf(countFileName, "%s-bmaMeta-splints-%d", libname, scaffoldRound);
        get_rank_path(countFileName, -1);
        FILE *countFD = fopen_chk(countFileName, "w+");
        fprintf(countFD, "%d\t0\t0\t%d\t%s-splints-%d\n", readLength, readLength, libname, scaffoldRound);
        fclose_track(countFD);
    }

    UPC_LOGGED_BARRIER;
    free_chk(lineBuffers);

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }
    return 0;
}
