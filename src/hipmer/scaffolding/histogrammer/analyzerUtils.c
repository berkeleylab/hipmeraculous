#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "analyzerUtils.h"

/* Struct to store aligning info */

int splitSrfLine(char *srfLine, int64_t *scaffoldId, int64_t *contigId, int *sStart, int *sEnd, int *cStrand)
{
    char *token;
    char *aux;

    token = strtok_r(srfLine, "\t", &aux);
    (*scaffoldId) = atol(token + 8);
    token = strtok_r(NULL, "\t", &aux);
    assert(token != NULL);
    if ((*token) != 'C') {
        return FAIL;
    }
    token = strtok_r(NULL, "\t", &aux);
    (*cStrand) = ((*token) == '+') ? PLUS : MINUS;
    (*contigId) = atol(token + 7);
    token = strtok_r(NULL, "\t", &aux);
    (*sStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*sEnd) = atoi(token);

    return SUCCESS;
}

int cmpfunc(const void *a, const void *b)
{
    return *(int64_t *)a - *(int64_t *)b;
}

/* Parses a single alignment */
/* Returns 1 if the alignemnt is valid. Returns 0 if the alignment is a guard alignment */

int splitAlignment(char *input_map, char *read_id, int *qStart, int *qStop, int *qLength, int64_t *subject, int *sStart, int *sStop, int *sLength, int *strand)
{
    char *token;
    char *aux;
    int failed = 0;

    token = strtok_r(input_map, "\t", &aux);
    if (strcmp(token, "MERALIGNER-F") == 0) {
        failed = 1; /* no alignment, but read through read_length field */
    }
    token = strtok_r(NULL, "\t", &aux);
    strcpy(read_id, token);
    token = strtok_r(NULL, "\t", &aux);
    (*qStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*qStop) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*qLength) = atoi(token);
    if (failed) {
        return 0;
    }
    token = strtok_r(NULL, "\t", &aux);
    (*subject) = atol(token + 6);
    token = strtok_r(NULL, "\t", &aux);
    (*sStart) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*sStop) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*sLength) = atoi(token);
    token = strtok_r(NULL, "\t", &aux);
    (*strand) = (strcmp(token, "Plus") == 0) ? PLUS : MINUS;

    return 1;
}



/* Checks if the read-to-contig alignment meets some criteria and returns appropriate characterization */
int testAlignStatus(int qStart, int qStop, int qLength, int sStart, int sStop, int sLength, int strand, int fivePrimeWiggleRoom, int threePrimeWiggleRoom, int innieRemoval, int shortPair)
{
    int missingStartBases, startStatus, unalignedEnd, projectedEnd, endStatus, missingEndBases, minBackDistance, backDistance;
    int projectedOff = 0;

    int unalignedStart = qStart - 1;
    int projectedStart = (strand == PLUS) ? sStart - unalignedStart : sStop + unalignedStart;

    if (innieRemoval) {
        minBackDistance = shortPair;
        backDistance = projectedStart;
        if (strand == MINUS) {
            backDistance = sLength - projectedStart;
        }
        if (backDistance <= minBackDistance) {
            return FAIL;
        }
    }

    if (projectedStart < 1) {
        projectedOff = 1 - projectedStart;
    } else if (projectedStart > sLength) {
        projectedOff = projectedStart - sLength;
    }
    missingStartBases = unalignedStart - projectedOff;
    if (unalignedStart == 0) {
        startStatus = FULL;
    } else if ((projectedOff > 0) && (missingStartBases < fivePrimeWiggleRoom)) {
        startStatus = GAP;
    } else if (unalignedStart < fivePrimeWiggleRoom) {
        startStatus = INC;
    } else {
        startStatus = TRUNC;
    }

    unalignedEnd = qLength - qStop;
    projectedEnd = (strand == PLUS) ? sStop + unalignedEnd : sStart - unalignedEnd;
    projectedOff = 0;
    if (projectedEnd < 1) {
        projectedOff = 1 - projectedEnd;
    } else if (projectedEnd > sLength) {
        projectedOff = projectedEnd - sLength;
    }
    missingEndBases = unalignedEnd - projectedOff;
    if (unalignedEnd == 0) {
        endStatus = FULL;
    } else if ((projectedOff > 0) && (missingEndBases < threePrimeWiggleRoom)) {
        endStatus = GAP;
    } else if (unalignedEnd < threePrimeWiggleRoom) {
        endStatus = INC;
    } else {
        endStatus = TRUNC;
    }

    if (((startStatus == FULL) || (startStatus == INC)) && ((endStatus == FULL) || (endStatus == INC))) {
        return SUCCESS;
    } else {
        return FAIL;
    }
}
