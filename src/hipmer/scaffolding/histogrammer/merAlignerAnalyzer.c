#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <ctype.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>

#include "optlist.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "common.h"
#include "timers.h"
#include "analyzerUtils.h"
#include "locationHash.h"
#include "upc_output.h"

shared int64_t nLoci = 0;
shared int64_t redundancy = 0;

shared int64_t nFullPairs = 0;
shared int64_t nNonStandard = 0;

int64_t myNonStandard = 0;
int64_t meanRedundancy = 0;

shared int64_t num_found_fails = 0;
shared int64_t num_length_fails = 0;
shared int64_t num_tests = 0;
shared int64_t num_valid = 0;
shared int64_t totExcessiveAligns = 0;


#define CHECK_MER_ENTRY(buf, fname)                                     \
    do {                                                                 \
        if ((buf)[0] != 'M') {                                              \
            DIE("Invalid entry in meraligner output file %s/1: %s\n", fname, buf); } \
    } while (0)

int merAlignerAnalyzer_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    if (MYTHREAD == 0) {
        nLoci = 0;
        redundancy = 0;
        nFullPairs = 0;
        nNonStandard = 0;
        num_found_fails = 0;
        num_length_fails = 0;
        num_tests = 0;
        num_valid = 0;
        totExcessiveAligns = 0;
    }
    myNonStandard = 0;
    meanRedundancy = 0;
    UPC_LOGGED_BARRIER;

    int64_t myFullPairs = 0;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "l:i:s:m:M:RAE:L:F:T:U:t:c:B:N:Z:G:C:p:a:o:e");
    print_args(optList, __func__);
    int reverseComplement = 0, innieRemoval = 0, endAversion = 0, minTestLength = 0, minFreqReported = 10, fivePrimeWiggleRoom = 5, threePrimeWiggleRoom = 5, truncate = 0, minMatch = 0, estimatedInsertSize = 0, estimatedSigma = 0, testMode = 1, insertSize = 0, insertSigma = 0, coresPerNode = 1;
    char *libname = NULL;
    char merAlignerFname[MAX_FILE_PATH];
    GZIP_FILE laneFD1, laneFD2;
    int64_t i, j, k, pos1, pos2, found_flag, test1, test2, test3, strand1, strand2, sStart1, sStart2, sStop1, sStop2, sLength1, locs[4], min, max, extent, safeDistance, leftBoundary, rightBoundary, orientation, qStart1, qStart2, qStop1, qStop2, qLength2, qLength1, sLength2;
    align_info *result1 = (align_info *)calloc_chk(sizeof(align_info), 1);
    align_info *result2 = (align_info *)calloc_chk(sizeof(align_info), 1);
    char *lineBuffers = (char *)calloc_chk(MAX_LINE_SIZE * 10, 1);
    char *alignmentBuffer1 = lineBuffers;
    char *copyAlignmentBuffer1 = alignmentBuffer1 + MAX_LINE_SIZE;
    char *firstAlignmentBuffer1 = copyAlignmentBuffer1 + MAX_LINE_SIZE;
    char *alignmentBuffer2 = firstAlignmentBuffer1 + MAX_LINE_SIZE;
    char *copyAlignmentBuffer2 = alignmentBuffer2 + MAX_LINE_SIZE;
    char *firstAlignmentBuffer2 = copyAlignmentBuffer2 + MAX_LINE_SIZE;
    char *readIdLane1 = firstAlignmentBuffer2 + MAX_LINE_SIZE;
    char *newReadIdLane1 = readIdLane1 + MAX_LINE_SIZE;
    char *readIdLane2 = newReadIdLane1 + MAX_LINE_SIZE;
    char *newReadIdLane2 = readIdLane2 + MAX_LINE_SIZE;
    char *resRead1, *resRead2;
    char estimate_from_merged = 0;
    int validAlign1, validAlign2;
    UPC_TICK_T start, end;
    int coresToUsePerNode = 1;
    int shortPair = 600, trimOff;
    int threshold = 10, subjectID;
    int count;
    int64_t totalAlignments = 10;
    hash_table_t *locationHashTable;
    memory_heap_t memory_heap;
    bucket_t *cur_bucket;
    location_t *cur_location;
    int64_t hash_table_size, myLoci = 0, myRedundancy = 0, mean, meanSquare, n, size, freq;
    double meanDouble, stdDev;
    const char *base_dir = ".";

    int srfInput = 0;
    char *srfSuffixName;
    int64_t totalScaffolds, totalContigs;
    char *merAlignerOutput = NULL;
    char merAlignerOutputFname[255];
    char *outputExt;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'Z':
            /* FIXME: Standardize srf file output name conventions */
            srfInput = 1;
            srfSuffixName = thisOpt->argument;
            break;
        case 'G':
            totalScaffolds = atol(thisOpt->argument);
            break;
        case 'C':
            totalContigs = atol(thisOpt->argument);
            break;
        case 'R':
            reverseComplement = 1;
            break;
        case 'A':
            innieRemoval = 1;
            break;
        case 'l':
            libname = thisOpt->argument;
            break;
        case 'E':
            endAversion = atoi(thisOpt->argument);
            break;
        case 'L':
            minTestLength = atoi(thisOpt->argument);
            break;
        case 'U':
            truncate = atoi(thisOpt->argument);
            break;
        case 'T':
            threePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 't':
            threshold = atoi(thisOpt->argument);
            break;
        case 'M':
            minFreqReported = atoi(thisOpt->argument);
            break;
        case 'F':
            fivePrimeWiggleRoom = atoi(thisOpt->argument);
            break;
        case 'm':
            minMatch = atoi(thisOpt->argument);
            break;
        case 'i':
            estimatedInsertSize = atoi(thisOpt->argument);
            break;
        case 's':
            estimatedSigma = atoi(thisOpt->argument);
            break;
        case 'e':
            estimate_from_merged = 1; // i.e. not reliable, so do not WARN if they differ
            break;
        case 'p':
            coresToUsePerNode = atoi(thisOpt->argument);
            break;
        case 'c':
            totalAlignments = atol(thisOpt->argument);
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'N':
            coresPerNode = atoi(thisOpt->argument);
            SET_CORES_PER_NODE(coresPerNode);
            break;
        case 'a':
            merAlignerOutput = thisOpt->argument;
            break;
        case 'o':
            outputExt = thisOpt->argument;
            break;
        default:
            break;
        }
    }

    if (libname == NULL) {
        DIE("You must specify a -l libname\n");
    }

    start = UPC_TICKS_NOW();

    int64_t my_num_tests = 0, my_passed_truncate_1 = 0, my_passed_truncate_2 = 0, my_passed_srfInput = 0, my_passed_subject = 0, my_passed_endAversion = 0, my_passed_lengthCutoff = 0, my_passed_test1 = 0, my_passed_test2 = 0, my_passed_test3 = 0;
    int64_t my_num_length_fails = 0, my_num_found_fails = 0, my_num_valid = 0;
    int64_t *local_histo = (int64_t *)calloc_chk(HISTOGRAM_SIZE, sizeof(int64_t));

    /* Create Location hash table and relevant memory heap */
    UPC_LOGGED_BARRIER;
    locationHashTable = create_hash_table(totalAlignments + HISTOGRAM_SIZE * ((int64_t)THREADS), &memory_heap);
    UPC_LOGGED_BARRIER;

    shared[1] int64_t * scaffLengths = NULL;
    shared[1] contigScaffoldMap_t * contigScaffoldMap = NULL;
    contigScaffoldMap_t contigScaffoldMapEntry;
    char srfLine[MAX_LINE_SIZE];
    char srfFileName[MAX_FILE_PATH];
    GZIP_FILE srfFD;
    char *fgets_result;
    int64_t scaffoldId, contigId, subject1, subject2;
    int splitRes, cStrand, sStart, sEnd;
    upc_tick_t atomics_timer;

    /******************************/
    /*  Read scaffold report file */
    /******************************/

    if (srfInput) {
        if (!srfSuffixName) {
            SDIE("You must specify a -Z srfSuffixName\n");
        }
        sprintf(srfFileName, "%s" GZIP_EXT, srfSuffixName);
        srfFD = openCheckpoint(srfFileName, "r");
        if (!totalScaffolds || !totalContigs) {
            SDIE("You must specify a -C totalContigs and a -G totalScaffolds\n");
        }

        /* Build scaffLengths data structure */
        UPC_ALL_ALLOC_CHK(scaffLengths, totalScaffolds + 1, sizeof(int64_t));
        if (scaffLengths == NULL) {
            DIE("Could not allocate %lld scaffolds!\n", (lld)totalScaffolds);
        }
        for (i = MYTHREAD; i < totalScaffolds; i += THREADS) {
            scaffLengths[i] = 0;
        }

        /* Build contigScaffoldMap data structure */
        UPC_ALL_ALLOC_CHK(contigScaffoldMap, totalContigs + 1, sizeof(contigScaffoldMap_t));
        if (contigScaffoldMap == NULL) {
            DIE("Could not allocate %lld totalContigs!\n", (lld)totalContigs);
        }
        for (i = MYTHREAD; i < totalContigs; i += THREADS) {
            contigScaffoldMap[i].scaffID = UNDEFINED;
        }

        UPC_LOGGED_BARRIER;

        fgets_result = GZIP_GETS(srfLine, MAX_LINE_SIZE, srfFD);

        while (fgets_result != NULL) {
            splitRes = splitSrfLine(srfLine, &scaffoldId, &contigId, &sStart, &sEnd, &cStrand);
            if (splitRes == SUCCESS) {
                /* Update scaffsLength array */
                if (scaffLengths[scaffoldId] == 0) {
                    scaffLengths[scaffoldId] = sEnd;
                } else {
                    if (sEnd > scaffLengths[scaffoldId]) {
                        scaffLengths[scaffoldId] = sEnd;
                    }
                }

                /* Update contigScaffoldMap data structure */
                contigScaffoldMapEntry.cStrand = cStrand;
                contigScaffoldMapEntry.scaffID = scaffoldId;
                contigScaffoldMapEntry.sStart = sStart;
                contigScaffoldMapEntry.sEnd = sEnd;
                contigScaffoldMap[contigId] = contigScaffoldMapEntry;
            }

            fgets_result = GZIP_GETS(srfLine, MAX_LINE_SIZE, srfFD);
        }

        closeCheckpoint(srfFD);
    }

    UPC_LOGGED_BARRIER;

    /* Allocation of global histogram */
    shared[1] int64_t * shared_histo = NULL;
    UPC_ALL_ALLOC_CHK(shared_histo, HISTOGRAM_SIZE, sizeof(int64_t));

    for (i = MYTHREAD; i < HISTOGRAM_SIZE; i += THREADS) {
        shared_histo[i] = 0;
    }
    int64_t excessiveAligns = 0;

    UPC_LOGGED_BARRIER;

    while (coresToUsePerNode > 1 && coresPerNode % coresToUsePerNode != 0) {
        coresToUsePerNode--;                                                                   // Ensure coresToUse divides evenly in the node
    }
    int numFilesPerThread = coresPerNode / coresToUsePerNode;

    // force restore of checkpoints
    for (int fileIndex = MYTHREAD; fileIndex < MYTHREAD + numFilesPerThread; fileIndex++) {
        sprintf(merAlignerOutputFname, "%s-%s_Read1" GZIP_EXT, libname, merAlignerOutput);
        restoreCheckpoint( merAlignerOutputFname );
        sprintf(merAlignerOutputFname, "%s-%s_Read2" GZIP_EXT, libname, merAlignerOutput);
        restoreCheckpoint( merAlignerOutputFname );
    }
    
    if (MYTHREAD % numFilesPerThread == 0) {
        LOGF("Reading files from thread %d to %d\n", MYTHREAD, MYTHREAD + numFilesPerThread - 1);

        int scaffLength1, scaffLength2, contigScaffStrand1, contigScaffStrand2, scaffStrand1,
            scaffStrand2, scaffStart1, scaffStart2, scaffStop1, scaffStop2, sLengthCutoff;

        for (int fileIndex = MYTHREAD; fileIndex < MYTHREAD + numFilesPerThread; fileIndex++) {
            sprintf(merAlignerOutputFname, "%s/%s-%s_Read1" GZIP_EXT, base_dir, libname, merAlignerOutput);
            char *rp = get_rank_path(merAlignerOutputFname, fileIndex);
            laneFD1 = GZIP_OPEN(rp, "r");
            sprintf(merAlignerOutputFname, "%s/%s-%s_Read2" GZIP_EXT, base_dir, libname, merAlignerOutput);
            rp = get_rank_path(merAlignerOutputFname, fileIndex);
            laneFD2 = GZIP_OPEN(rp, "r");
            //printf("Thread %d is reading %s\n", MYTHREAD, merAlignerOutputFname);

            resRead1 = GZIP_GETS(firstAlignmentBuffer1, MAX_LINE_SIZE, laneFD1);
            resRead2 = GZIP_GETS(firstAlignmentBuffer2, MAX_LINE_SIZE, laneFD2);
            while (myFullPairs < totalAlignments) {
                if (resRead1 == NULL && resRead2 == NULL) {
                    break;
                }

                //  Parse the first alignment of read (lane 1)
                if (resRead1 != NULL) {
                    CHECK_MER_ENTRY(firstAlignmentBuffer1, merAlignerOutputFname);
                    validAlign1 = splitAlignment(firstAlignmentBuffer1, readIdLane1, &(result1->aligned_query_locs[0]), &(result1->aligned_query_locs[1]), &(result1->query_length), &(result1->aligned_subject_ids[0]), &(result1->aligned_subject_locs[0]), &(result1->aligned_subject_locs[1]), &(result1->aligned_subject_lengths[0]), &(result1->aligned_strand[0]));
                    if (validAlign1 == 0) {
                        result1->subjects_matched = 0;
                    } else {
                        result1->subjects_matched = 1;
                        my_num_valid++;
                    }
                }

                //  Read the the rest alignments of read (lane 1)
                if (resRead1 != NULL) {
                    resRead1 = GZIP_GETS(alignmentBuffer1, MAX_LINE_SIZE, laneFD1);
                    memcpy(copyAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
                    if (resRead1 != NULL) {
                        CHECK_MER_ENTRY(copyAlignmentBuffer1, merAlignerOutputFname);

                        if (result1->subjects_matched < MAX_ALIGN) {
                            validAlign1 = splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &(result1->aligned_query_locs[2 * result1->subjects_matched]), &(result1->aligned_query_locs[2 * result1->subjects_matched + 1]), &(result1->query_length), &(result1->aligned_subject_ids[result1->subjects_matched]), &(result1->aligned_subject_locs[2 * result1->subjects_matched]), &(result1->aligned_subject_locs[2 * result1->subjects_matched + 1]), &(result1->aligned_subject_lengths[result1->subjects_matched]), &(result1->aligned_strand[result1->subjects_matched]));
                        } else {
                            excessiveAligns++;
                        }

                        while (strcmp(readIdLane1, newReadIdLane1) == 0) {
                            result1->subjects_matched++;
                            resRead1 = GZIP_GETS(alignmentBuffer1, MAX_LINE_SIZE, laneFD1);
                            memcpy(copyAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
                            if (resRead1 == NULL) {
                                break;
                            }
                            CHECK_MER_ENTRY(copyAlignmentBuffer1, merAlignerOutputFname);
                            if (result1->subjects_matched < MAX_ALIGN) {
                                validAlign1 = splitAlignment(copyAlignmentBuffer1, newReadIdLane1, &(result1->aligned_query_locs[2 * result1->subjects_matched]), &(result1->aligned_query_locs[2 * result1->subjects_matched + 1]), &(result1->query_length), &(result1->aligned_subject_ids[result1->subjects_matched]), &(result1->aligned_subject_locs[2 * result1->subjects_matched]), &(result1->aligned_subject_locs[2 * result1->subjects_matched + 1]), &(result1->aligned_subject_lengths[result1->subjects_matched]), &(result1->aligned_strand[result1->subjects_matched]));
                            } else {
                                excessiveAligns++;
                            }
                        }

                        if (resRead1 != NULL) {
                            memcpy(firstAlignmentBuffer1, alignmentBuffer1, MAX_LINE_SIZE * sizeof(char));
                            strcpy(readIdLane1, newReadIdLane1);
                        }
                    }
                }

                //  Parse the first alignment of read (lane 2)

                if (resRead2 != NULL) {
                    CHECK_MER_ENTRY(firstAlignmentBuffer2, merAlignerOutputFname);
                    validAlign2 = splitAlignment(firstAlignmentBuffer2, readIdLane2, &(result2->aligned_query_locs[0]), &(result2->aligned_query_locs[1]), &(result2->query_length), &(result2->aligned_subject_ids[0]), &(result2->aligned_subject_locs[0]), &(result2->aligned_subject_locs[1]), &(result2->aligned_subject_lengths[0]), &(result2->aligned_strand[0]));

                    if (validAlign2 == 0) {
                        result2->subjects_matched = 0;
                    } else {
                        result2->subjects_matched = 1;
                    }
                }

                //  Read the the rest alignments of read (lane 2)
                if (resRead2 != NULL) {
                    resRead2 = GZIP_GETS(alignmentBuffer2, MAX_LINE_SIZE, laneFD2);
                    memcpy(copyAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));

                    if (resRead2 != NULL) {
                        CHECK_MER_ENTRY(copyAlignmentBuffer2, merAlignerOutputFname);
                        if (result2->subjects_matched < MAX_ALIGN) {
                            validAlign2 = splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &(result2->aligned_query_locs[2 * result2->subjects_matched]), &(result2->aligned_query_locs[2 * result2->subjects_matched + 1]), &(result2->query_length), &(result2->aligned_subject_ids[result2->subjects_matched]), &(result2->aligned_subject_locs[2 * result2->subjects_matched]), &(result2->aligned_subject_locs[2 * result2->subjects_matched + 1]), &(result2->aligned_subject_lengths[result2->subjects_matched]), &(result2->aligned_strand[result2->subjects_matched]));
                        } else {
                            excessiveAligns++;
                        }

                        while (strcmp(readIdLane2, newReadIdLane2) == 0) {
                            result2->subjects_matched++;
                            resRead2 = GZIP_GETS(alignmentBuffer2, MAX_LINE_SIZE, laneFD2);
                            memcpy(copyAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
                            if (resRead2 == NULL) {
                                break;
                            }
                            CHECK_MER_ENTRY(copyAlignmentBuffer2, merAlignerOutputFname);
                            if (result2->subjects_matched < MAX_ALIGN) {
                                validAlign2 = splitAlignment(copyAlignmentBuffer2, newReadIdLane2, &(result2->aligned_query_locs[2 * result2->subjects_matched]), &(result2->aligned_query_locs[2 * result2->subjects_matched + 1]), &(result2->query_length), &(result2->aligned_subject_ids[result2->subjects_matched]), &(result2->aligned_subject_locs[2 * result2->subjects_matched]), &(result2->aligned_subject_locs[2 * result2->subjects_matched + 1]), &(result2->aligned_subject_lengths[result2->subjects_matched]), &(result2->aligned_strand[result2->subjects_matched]));
                            } else {
                                excessiveAligns++;
                            }
                        }

                        if (resRead2 != NULL) {
                            memcpy(firstAlignmentBuffer2, alignmentBuffer2, MAX_LINE_SIZE * sizeof(char));
                            strcpy(readIdLane2, newReadIdLane2);
                        }
                    }
                }

                // ***************************
                //  Find insert size estimate
                // ***************************

                if ((result1->subjects_matched != 1) || (result2->subjects_matched != 1)) {
                    continue;
                }

                my_num_tests++;

                //  Extract parameters of the alignments
                strand1 = result1->aligned_strand[0];
                sStart1 = result1->aligned_subject_locs[0];
                qStart1 = result1->aligned_query_locs[0];
                sStop1 = result1->aligned_subject_locs[1];
                qStop1 = result1->aligned_query_locs[1];
                qLength1 = result1->query_length;
                sLength1 = result1->aligned_subject_lengths[0];
                strand2 = result2->aligned_strand[0];
                sStart2 = result2->aligned_subject_locs[0];
                qStart2 = result2->aligned_query_locs[0];
                sStop2 = result2->aligned_subject_locs[1];
                qStop2 = result2->aligned_query_locs[1];
                sLength2 = result2->aligned_subject_lengths[0];
                qLength2 = result2->query_length;
                subject1 = result1->aligned_subject_ids[0];
                subject2 = result2->aligned_subject_ids[0];

                //  Apply truncation conditions to lane 1 read
                if (truncate == 5) {
                    if ((qStart1 > 1) || (qStop1 < minMatch)) {
                        continue;
                    }
                    trimOff = qStop1 - minMatch;
                    qStop1 = minMatch;
                    qLength1 = minMatch;
                    if (strand1 == PLUS) {
                        sStop1 -= trimOff;
                    } else {
                        sStart1 += trimOff;
                    }
                } else if (truncate == 3) {
                    if ((qLength1 - qStart1 + 1 < minMatch) || (qStop1 < qLength1)) {
                        continue;
                    }
                    trimOff = (qLength1 - minMatch + 1) - qStart1;
                    qStart1 = 1;
                    qStop1 = minMatch;
                    qLength1 = minMatch;
                    if (strand1 == PLUS) {
                        sStart1 += trimOff;
                    } else {
                        sStop1 -= trimOff;
                    }
                }
                my_passed_truncate_1++;

                //  Apply truncation conditions to lane 2 read
                if (truncate == 5) {
                    if ((qStart2 > 1) || (qStop2 < minMatch)) {
                        continue;
                    }
                    trimOff = qStop2 - minMatch;
                    qStop2 = minMatch;
                    qLength2 = minMatch;
                    if (strand2 == PLUS) {
                        sStop2 -= trimOff;
                    } else {
                        sStart2 += trimOff;
                    }
                } else if (truncate == 3) {
                    if ((qLength2 - qStart2 + 1 < minMatch) || (qStop2 < qLength2)) {
                        continue;
                    }
                    trimOff = (qLength2 - minMatch + 1) - qStart2;
                    qStart2 = 1;
                    qStop2 = minMatch;
                    qLength2 = minMatch;
                    if (strand2 == PLUS) {
                        sStart2 += trimOff;
                    } else {
                        sStop2 -= trimOff;
                    }
                }
                my_passed_truncate_2++;

                //  Translate locations if there is an srf input file
                if (srfInput) {
                    contigScaffoldMapEntry = contigScaffoldMap[subject1];
                    if (contigScaffoldMapEntry.scaffID == UNDEFINED) {
                        continue;
                    }

                    scaffLength1 = scaffLengths[contigScaffoldMapEntry.scaffID];
                    contigScaffStrand1 = contigScaffoldMapEntry.cStrand;
                    scaffStrand1 = (contigScaffStrand1 == strand1) ? PLUS : MINUS;
                    scaffStart1 = (contigScaffStrand1 == PLUS) ? contigScaffoldMapEntry.sStart + sStart1 - 1 : contigScaffoldMapEntry.sEnd - sStop1 + 1;
                    scaffStop1 = (contigScaffStrand1 == PLUS) ? contigScaffoldMapEntry.sStart + sStop1 - 1 : contigScaffoldMapEntry.sEnd - sStart1 + 1;

                    subject1 = contigScaffoldMapEntry.scaffID;
                    sLength1 = scaffLength1;
                    strand1 = scaffStrand1;
                    sStart1 = scaffStart1;
                    sStop1 = scaffStop1;

                    contigScaffoldMapEntry = contigScaffoldMap[subject2];
                    if (contigScaffoldMapEntry.scaffID == UNDEFINED) {
                        continue;
                    }

                    scaffLength2 = scaffLengths[contigScaffoldMapEntry.scaffID];
                    contigScaffStrand2 = contigScaffoldMapEntry.cStrand;
                    scaffStrand2 = (contigScaffStrand2 == strand2) ? PLUS : MINUS;
                    scaffStart2 = (contigScaffStrand2 == PLUS) ? contigScaffoldMapEntry.sStart + sStart2 - 1 : contigScaffoldMapEntry.sEnd - sStop2 + 1;
                    scaffStop2 = (contigScaffStrand2 == PLUS) ? contigScaffoldMapEntry.sStart + sStop2 - 1 : contigScaffoldMapEntry.sEnd - sStart2 + 1;

                    subject2 = contigScaffoldMapEntry.scaffID;
                    sLength2 = scaffLength2;
                    strand2 = scaffStrand2;
                    sStart2 = scaffStart2;
                    sStop2 = scaffStop2;
                }
                my_passed_srfInput++;

                //  If the read does not align to the same pair, then reject it
                if (subject1 != subject2) {
                    continue;
                }
                my_passed_subject++;

                //  endAversion test
                if (endAversion) {
                    if ((sStop1 <= endAversion) || (sStart1 >= sLength1 - endAversion)) {
                        continue;
                    }
                    if ((sStop2 <= endAversion) || (sStart2 >= sLength2 - endAversion)) {
                        continue;
                    }
                }
                my_passed_endAversion++; 

                if (testMode) {
                    sLengthCutoff = 2 * estimatedInsertSize + 8 * estimatedSigma;
                    if (sLength1 <= sLengthCutoff) {
                        my_num_length_fails++;
                        continue;
                    }
                }
                my_passed_lengthCutoff++; 

                if (reverseComplement) {
                    strand1 = (strand1 == PLUS) ? MINUS : PLUS;
                    strand2 = (strand2 == PLUS) ? MINUS : PLUS;
                }

                test1 = testAlignStatus(qStart1, qStop1, qLength1, sStart1, sStop1, sLength1, strand1, fivePrimeWiggleRoom, threePrimeWiggleRoom, innieRemoval, shortPair);

                if (test1 == FAIL) {
                    continue;
                }

                test2 = testAlignStatus(qStart2, qStop2, qLength2, sStart2, sStop2, sLength2, strand2, fivePrimeWiggleRoom, threePrimeWiggleRoom, innieRemoval, shortPair);

                if (test2 == FAIL) {
                    continue;
                }

                if (strand1 == strand2) {
                    test3 = FAIL;
                    myNonStandard++;
                } else {
                    test3 = SUCCESS;
                }

                //  Test orientation

                orientation = UNDEFINED;
                if ((test1 == SUCCESS) && (test2 == SUCCESS) && (test3 == SUCCESS)) {
                    if (((strand1 == PLUS) && (sStart1 < sStart2) && (sStart1 < sStop2)) || ((strand2 == PLUS) && (sStart2 < sStart1) && (sStart2 < sStop1))) {
                        orientation = CONVERGENT;
                    } else if (((strand1 == PLUS) && (sStart1 > sStop2)) || ((strand2 == PLUS) && (sStart2 > sStop1))) {
                        orientation = DIVERGENT;
                    }

                    if (orientation != CONVERGENT) {
                        myNonStandard++;
                        test3 = FAIL;
                    }
                }

                //  Test boundaries
                if ((test1 == SUCCESS) && (test2 == SUCCESS) && (test3 == SUCCESS)) {
                    locs[0] = sStart1;
                    locs[1] = sStart2;
                    locs[2] = sStop1;
                    locs[3] = sStop2;
                    qsort(locs, 4, sizeof(int64_t), cmpfunc);
                    min = locs[0];
                    max = locs[3];
                    safeDistance = estimatedInsertSize + 4 * estimatedSigma;
                    leftBoundary = safeDistance;
                    rightBoundary = sLength1 - safeDistance;
                    if ((max < leftBoundary) || (min > rightBoundary)) {
                        test3 = FAIL;
                    }
                }

                //  Test location
                if ((test1 == SUCCESS) && (test2 == SUCCESS) && (test3 == SUCCESS)) {
                    subjectID = subject1;
                    count = lookupAndIncrementLocation(subjectID, min, max, locationHashTable, &memory_heap);
                    if (count != 0) {
                        test1 = FAIL;
                    }
                }

                //  Test innie removal
                if ((test1 == SUCCESS) && (test2 == SUCCESS) && (test3 == SUCCESS)) {
                    extent = max - min + 1;
                    if (innieRemoval) {
                        if (extent < shortPair) {
                            myNonStandard++;
                            test1 = FAIL;
                        }
                    }
                }

                if (test1 == SUCCESS) my_passed_test1++; 
                if (test2 == SUCCESS) my_passed_test2++; 
                if (test3 == SUCCESS) my_passed_test3++; 
                //  Have passed all the tests, so add this entry in the histogram
                if ((test1 == SUCCESS) && (test2 == SUCCESS) && (test3 == SUCCESS)) {
                    extent = max - min + 1;
                    myFullPairs++;
                    //  Check for bounds in histogram
                    if (extent < HISTOGRAM_SIZE) {
                        local_histo[extent]++;
                    }
                }
            } // while ( moreAlignmentsExist )
            GZIP_CLOSE(laneFD1);
            GZIP_CLOSE(laneFD2);

            if (myFullPairs == totalAlignments) {
                break;
            }
        } // file loop
          //if (myFullPairs == totalAlignments)
          //   printf("Thread %d: Found an adequate sample: %ld\n", MYTHREAD, myFullPairs);
          //else
          //   printf("WARNING: Thread %d did not find sufficient data ( %ld < %ld ) in files for a full sample\n",
          //          MYTHREAD, myFullPairs, totalAlignments);
    } //   if ((MYTHREAD % coresPerNode) < coresToUsePerNode)

    LOGF("TestStats: tests=%lld, truncate_1=%lld, truncate_2=%lld, srfInput=%lld, subject=%lld, endAversion=%lld, lengthCutoff=%lld, test1=%lld, test2=%lld, test3=%lld\n", (lld) my_num_tests, (lld) my_passed_truncate_1, (lld) my_passed_truncate_2, (lld) my_passed_srfInput, (lld) my_passed_subject, (lld) my_passed_endAversion, (lld) my_passed_lengthCutoff, (lld) my_passed_test1, (lld) my_passed_test2, (lld) my_passed_test3);
    atomics_timer = UPC_TICKS_NOW();
    DBG("Global Histgram entries:\n");
    for (i = 0; i < HISTOGRAM_SIZE; i++) {
        if (local_histo[i] != 0) {
            UPC_ATOMIC_FADD_I64_RELAXED(NULL, &shared_histo[i], local_histo[i]);
#ifdef DEBUG
            DBG("%d\t%lld\n", i, (lld) local_histo[i]);
#endif
        }
    }

    UPC_LOGGED_BARRIER;
    if (scaffLengths != NULL) {
        UPC_ALL_FREE_CHK(scaffLengths);
    }
    if (contigScaffoldMap != NULL) {
        UPC_ALL_FREE_CHK(contigScaffoldMap);
    }

    if (MYTHREAD == 0) {
        DBG("Global Histgram entries:\n");
        for (i = 0; i < HISTOGRAM_SIZE; i++) {
            local_histo[i] = shared_histo[i];
#ifdef DEBUG
            if (local_histo[i]) DBG("%d\t%lld\n", i, (lld) local_histo[i]);
#endif
        }
        end = UPC_TICKS_NOW();
        printf("\nTime for computing histogrammer : %.2f s, reduction %.2f s\n",
               (UPC_TICKS_TO_SECS(end - start)), (UPC_TICKS_TO_SECS(end - atomics_timer)));
    }

    UPC_LOGGED_BARRIER;
    UPC_ALL_FREE_CHK(shared_histo);
    free_chk(result1);
    free_chk(result2);

    /* Report results in a more formal way */
    /* Each thread iterates overs its entries of testLocations table to measure nLoci and redundancy */
    hash_table_size = locationHashTable->size;
    for (i = MYTHREAD; i < hash_table_size; i += THREADS) {
        cur_location = (location_t *)locationHashTable->table[i].head;
        while (cur_location != NULL) {
            myLoci++;
            myRedundancy += cur_location->count;
            cur_location = (location_t *)cur_location->next;
        }
    }


    // TODO replace with reduce
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &nLoci, myLoci);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &redundancy, myRedundancy);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &nFullPairs, myFullPairs);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &nNonStandard, myNonStandard);

    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &num_tests, my_num_tests);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &num_length_fails, my_num_length_fails);
    UPC_ATOMIC_FADD_I64_RELAXED(NULL, &num_valid, my_num_valid);
    if (excessiveAligns) {
        UPC_ATOMIC_FADD_I64_RELAXED(NULL, &totExcessiveAligns, excessiveAligns);
    }

    UPC_LOGGED_BARRIER;
    destroy_hash_table(&locationHashTable, &memory_heap);

    /* Thread 0 will report the statistics */

    if (MYTHREAD == 0) {
        printf("Carried out %lld tests; failures: too short %lld (%.3f), not found %lld (%.3f)\n",
               (lld)num_tests, (lld)num_length_fails, (double)num_length_fails / num_tests,
               (lld)num_found_fails, (double)num_found_fails / num_tests);
        printf("totalExcessiveAlignments: %lld\n", (lld)totExcessiveAligns);
        printf("number valid: %lld\n", (lld)num_valid);
        double mean_redundancy = (1.0 * redundancy) / (1.0 * nLoci);
        printf("%lld sampled canonical pairs (%lld non-standard pairs discarded); mean redundancy = %.1f\n",
               (lld)nFullPairs, (lld)nNonStandard, mean_redundancy);

        ADD_DIAG("%lld", "tests", (lld)num_tests);
        ADD_DIAG("%lld", "fail_too_short", (lld)num_length_fails);
        ADD_DIAG("%lld", "fail_not_found", (lld)num_found_fails);
        ADD_DIAG("%lld", "sampled_pairs", (lld)nFullPairs);
        // skip this as it can vary a lot
//      ADD_DIAG("%lld", "non_standard_discarded", (lld) nNonStandard);
        ADD_DIAG("%.1f", "mean_redundancy", mean_redundancy);
        ADD_DIAG("%lld", "excessive_alignments", (lld)totExcessiveAligns);

        mean = 0;
        meanSquare = 0;
        n = 0;
        for (i = 0; i < HISTOGRAM_SIZE; i++) {
            size = i;
            if (size >= minTestLength) {
                freq = local_histo[size];
                if (freq >= minFreqReported) { // FIXME this heuristic fails to find wide distributions like 35k +/ 3.5k in human dataset (LJMP)
                    mean += freq * size;
                    meanSquare += freq * size * size;
                    n += freq;
                }
            }
        }

        meanDouble = (1.0 * mean) / (1.0 * n);
        stdDev = sqrt((1.0 * meanSquare) / (1.0 * n) - meanDouble * meanDouble);
        if (isnan(meanDouble) || isnan(stdDev)) {
            if (estimatedInsertSize > 0 && estimatedSigma > 0) {
                meanDouble = estimatedInsertSize;
                stdDev = estimatedSigma;
                WARN("Could not calculate the insert size for lib %s (n=%lld), using estimated (%.0f +/- n=%.0f)\n", libname, (lld)n, meanDouble, stdDev);
            } else {
                serial_printf("Could not calculate the insert size for lib %s (n=%lld). A user provided estimate may be needed in later stages\n", libname, (lld)n);
            }
        } else {
            printf("Calculated insert size: %.0f +/- %.0f (estimated %d +/- %d)\n",
                   meanDouble, stdDev, estimatedInsertSize, estimatedSigma);

            if (estimatedInsertSize && (estimatedInsertSize < meanDouble * .7 || estimatedInsertSize > meanDouble * 1.3)) {
                // print warning if this exceeds 30% of estimated insert average, if that is set
                if (!estimate_from_merged) {
                    WARN("Calculated insert size %.0f for lib %s is more than 30%% different from estimated %d\n", libname, meanDouble, estimatedInsertSize);
                } else {
                    serial_printf("Fixing estimate (from merged_reads)\n");
                }
            }

            char countFileName[MAX_FILE_PATH];
            sprintf(countFileName, "%s-insert-%s.txt", libname, outputExt);
            get_rank_path(countFileName, -1);
            printf("Writing calculated insert average to %s\n", countFileName);
            FILE *countFD = fopen_chk(countFileName, "w");
            fprintf(countFD, "%.0f\n", meanDouble);
            fclose_track(countFD);

            sprintf(countFileName, "%s-std-%s.txt", libname, outputExt);
            get_rank_path(countFileName, -1);
            printf("Writing calculated insert standard deviation to %s\n", countFileName);
            countFD = fopen_chk(countFileName, "w");
            fprintf(countFD, "%.0f\n", stdDev);
            fclose_track(countFD);
        }
    }


    free_chk(lineBuffers);
    free_chk(local_histo);
    UPC_LOGGED_BARRIER;

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
