#ifndef _ANALYZER_UTILS_H_
#define _ANALYZER_UTILS_H_

#include <stdint.h>

#include "defines.h"

#define PLUS 0
#define MINUS 1
#define ON 1
#define OFF 0
#define SUCCESS 1
#define FAIL 0
#define FULL 0
#define GAP 1
#define INC 2
#define TRUNC 3
#define DIVERGENT 0
#define CONVERGENT 1
#define UNK 0
#define OUT 1
#define IN 2
#define MID 3
#define UNDEFINED (-1)
#define ANCHOR 0
#define OUTGAP 1
#define INTGAP 2

#define HISTOGRAM_SIZE 200000
#define MAX_LINE_SIZE 1000

/* Struct to store aligning info */

typedef struct align_info {
    char *  query_name;
    int     subjects_matched;
    int     query_length;
    int     aligned_query_locs[2 * MAX_ALIGN + 2];
    int64_t aligned_subject_ids[MAX_ALIGN + 1];
    int     aligned_subject_lengths[MAX_ALIGN + 1];
    int     aligned_subject_locs[2 * MAX_ALIGN + 2];
    int     aligned_strand[MAX_ALIGN + 1];
    int     startStatus[MAX_ALIGN + 1];
    int     endStatus[MAX_ALIGN + 1];
    int     location[MAX_ALIGN + 1];
} align_info;

int splitSrfLine(char *srfLine, int64_t *scaffoldId, int64_t *contigId, int *sStart, int *sEnd, int *cStrand);

int cmpfunc(const void *a, const void *b);

/* Parses a single alignment */
/* Returns 1 if the alignemnt is valid. Returns 0 if the alignment is a guard alignment */

int splitAlignment(char *input_map, char *read_id, int *qStart, int *qStop, int *qLength, int64_t *subject, int *sStart, int *sStop, int *sLength, int *strand);

/* Checks if the read-to-contig alignment meets some criteria and returns appropriate characterization */
int testAlignStatus(int qStart, int qStop, int qLength, int sStart, int sStop, int sLength, int strand, int fivePrimeWiggleRoom, int threePrimeWiggleRoom, int innieRemoval, int shortPair);

#endif // _ANALYZER_UTILS_H_
