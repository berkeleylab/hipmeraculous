#include "meraculous_main.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("meraculous");
    int ret = meraculous_main(argc, argv);
    CLOSE_MY_LOG;
    return ret;
}
