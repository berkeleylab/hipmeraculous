#ifndef UU_TRAVERSAL_CONTIG_H
#define UU_TRAVERSAL_CONTIG_H

#include "meraculous_dds.h"

#include "contigs_dds.h"
#include "kmer_hash_meraculous.h"

#include "UU_traversal_generic.h"

#endif // UU_TRAVERSAL_CONTIG_H
