#ifndef MERACULOUS_MAIN_H_
#define MERACULOUS_MAIN_H_

#ifdef USE_CRAY_UPC
#include <intrinsics.h>
#endif
#include <upc.h>
#include <upc_tick.h>

#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <libgen.h>

#include "optlist.h"

#include "StaticVars.h"
#include "upc_common.h"
#include "common.h"
#include "log.h"
#include "utils.h"

#include "meraculous.h"
#include "meraculous_dds.h"
#include "kmer_hash_meraculous.h"
#include "kmer_handling.h"

#include "../kcount/readufx.h"
#include "buildUFXhashBinary_meraculous.h"

extern shared int64_t contig_id;
extern int64_t myContigs;

int meraculous_main(int argc, char **argv);

#endif // MERACULOUS_MAIN_H_
