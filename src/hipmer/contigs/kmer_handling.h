#ifndef KMER_HANDLING_H
#define KMER_HANDLING_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "meraculous.h"
#include "hash_funcs.h"


void reverseComplementBase(char *base);

char reverseComplementBaseStrict(const char base);

char reverseComplementBaseExt(const char base);

void reverseComplementSeq(const char *seq, char *rc_seq, size_t seqLen);

void reverseComplementKmer(const char *kmer, char *rc_kmer, int kmer_len);

void reverseComplementExtensions(const char *ext, char *rc_ext);

void reverseComplementINPLACE(char *subcontig, int64_t size);

char *getLeastKmer(const char *kmer, char *temp, int kmer_len);

int isLeastKmer(const char *kmer, int kmer_len);

int isLeastOrientation(const char *seq, int64_t size);

uint64_t hashkmer(int64_t hashtable_size, char *seq, int kmer_len);

#endif // KMER_HANDLING_H
