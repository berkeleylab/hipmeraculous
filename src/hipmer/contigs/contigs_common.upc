#include "contigs_common.h"

#define CONTIGNO_BLOCK 10
static contig_ptr_t contig_block[CONTIGNO_BLOCK];
int64_t contig_offset = 0;

extern shared int64_t contig_id;
extern int64_t myContigs;

void reallocateContig(contig_ptr_t cur_contig, int64_t new_size, int64_t new_start)
{
    assert(upc_threadof(cur_contig) == MYTHREAD);
    assert(upc_threadof(cur_contig->sequence) == MYTHREAD);
    int64_t new_end, cur_length = cur_contig->end - cur_contig->start + 1;
    shared[] char *prev_buffer, *new_buffer;
    // ensure proper room for '\0' at end...
    if (new_size < new_start + cur_length + 1) {
        new_size = new_start + cur_length + 1;
    }

    new_end = new_start + cur_length - 1;
    assert(new_size > new_end + 1);

    UPC_ATOMIC_GET_SHPTR(&prev_buffer, &(cur_contig->sequence));
    assert(prev_buffer == cur_contig->sequence);

    new_buffer = NULL;
    UPC_ALLOC_CHK0(new_buffer, new_size * sizeof(char));

    DBG3("Thread %d: reallocating %lld from %lld (length %lld) to %lld (start %lld)\n", MYTHREAD, (lld)cur_contig->contig_id, (lld)cur_contig->size, (lld)cur_length, (lld)new_size, (lld)new_start);

    memcpy((char *)&(new_buffer[new_start]), (char *)&(prev_buffer[cur_contig->start]), cur_length * sizeof(char));
    UPC_ATOMIC_SET_SHPTR(NULL, &(cur_contig->sequence), new_buffer);
    assert(cur_contig->sequence == new_buffer);
    cur_contig->start = new_start;
    cur_contig->end = new_end;
    cur_contig->size = new_size;
    UPC_FREE_CHK0(prev_buffer);
    upc_fence;
}

void reallocateContigIfNeededLR(contig_ptr_t cur_contig, int leftBases, int rightBases, float growthFactor)
{
    assert(upc_threadof(cur_contig) == MYTHREAD);
    int64_t cur_length, new_size;
    int64_t new_start, new_end;


    if (cur_contig->start - leftBases <= 1 || cur_contig->end + rightBases >= (cur_contig->size - 2)) {
        /* Should reallocate space for contig buffer */
        new_size = ceil(growthFactor * cur_contig->size) + leftBases + rightBases + 2;
        cur_length = cur_contig->end - cur_contig->start + 1;

        new_start = new_size / 2 - cur_length / 2;
        if (new_start <= leftBases) {
            new_start = leftBases + 1;
        }
        new_end = new_start + cur_length - 1;
        if (new_size - new_end - 1 <= rightBases) {
            new_start -= (rightBases + 1 - (new_size - new_end - 1));
            new_end = new_start + cur_length - 1;
        }

        reallocateContig(cur_contig, new_size, new_start);
    }
    assert(cur_contig->start > leftBases && cur_contig->size - 1 - cur_contig->end > rightBases);
}

void reallocateContigIfNeeded(contig_ptr_t cur_contig)
{
    reallocateContigIfNeededLR(cur_contig, 1, 2, INCR_FACTOR);
}



/* unsafe in parallel; use only on local thread or when locked */
void add_contig_to_box_list(shared[] contig_ptr_box_list_t **contig_box_list, shared[] contig_ptr_box_list_t *new_box)
{
    assert(contig_box_list != NULL);
    assert(new_box != NULL);
    assert(new_box->next == NULL);

    // EGEOR: Atomic read of *contig_box_list
    // RE: No. contig_box_list is a local pointer to pointer-to-shared
    shared[] contig_ptr_box_list_t * tmp = *contig_box_list;

    // EGEOR: Atomic write of new_box->next
    UPC_ATOMIC_SET_SHPTR(NULL, &(new_box->next), tmp);
    assert(tmp == *contig_box_list);
    // EGEOR: Atomic write of *contig_box_list
    // RE: No. contig_box is a local pointer to pointer-to-shared
    *contig_box_list = new_box;

    DBG("Thread %d: add_contig_to_box_list added %lld (first %lld, last %lld)\n", MYTHREAD, (lld)new_box->contig->contig_id, (lld)((*contig_box_list)->contig->contig_id), (lld)(tmp == NULL ? -1 : ((tmp->contig == NULL) ? -2 : tmp->contig->contig_id)));
}

shared[] contig_ptr_box_list_t *alloc_contig_ptr_box_list(contig_ptr_t contig)
{
    shared[] contig_ptr_box_list_t * new_box = NULL;
    UPC_ALLOC_CHK0(new_box, sizeof(contig_ptr_box_list_t));
    new_box->next = NULL;
    new_box->contig = contig;
    new_box->original = contig;
    return new_box;
}

void printContigBox(shared[] contig_ptr_box_list_t *head)
{
    while (head != NULL) {
        DBG("Thread %d: printContigBox %lld (orig %lld) -> %lld\n", MYTHREAD, (lld)head->contig->contig_id, (lld)head->original->contig_id, (lld)(head->next == NULL ? -1 : head->next->contig->contig_id));
        head = head->next;
    }
}

void assign_contig_to_box_list(shared[] contig_ptr_box_list_t *head, contig_ptr_t contig)
{
    shared[] contig_ptr_box_list_t * contig_box_list, *last, *tmp, *contigMyself;
    int hasSelf, allAreClaimed;

    assert(head != NULL);
    assert(IS_VALID_UPC_PTR(head));
    assert(IS_VALID_UPC_PTR(contig));
    assert(head->contig != NULL && IS_VALID_UPC_PTR(head->contig));
    assert(head->original != NULL && IS_VALID_UPC_PTR(head->original));
    assert(head->contig == head->original);
    assert(head->contig->state == CLAIMED);
    assert(head->contig != contig);
    assert(contig != NULL);
    assert(upc_threadof(contig) == MYTHREAD);
    assert(contig->state == ACTIVE);

    contigMyself = contig->myself;
    assert(contigMyself != NULL);
    assert(contigMyself->contig == contig);

    // find the tail of the linked list of contig_box_ptrs, set to last.  Verify that all traversed contigs are in CLAIMED state
    do {
        allAreClaimed = 1;
        hasSelf = 0;
        // EGEOR: Atomic read of head
        // RE: NO. this is just a copy of the address in local vars
        contig_box_list = head;
        last = NULL;
        while (contig_box_list != NULL) {
            UPC_POLL;
            if (!IS_VALID_UPC_PTR(contig_box_list)) {
                DBG("Thread %d: Warning: assign_contig_to_box_list detected corrupted contig_box_list\n", MYTHREAD);
                allAreClaimed = 0;
                break;
            }
            // RE: no contig->myself does not change
            if (contig_box_list == contigMyself) {
                DBG("Thread %d: found target contig in box list. skipping this entry\n", MYTHREAD);
                hasSelf = 1;
            } else {
                contig_ptr_t originalContig = NULL, currentContig = NULL;
                originalContig = contig_box_list->original; // never changes for a given box
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                int originalState = originalContig->state;
#else
                int originalState;
                UPC_ATOMIC_GET_I32(&originalState, &originalContig->state);
#endif
                UPC_ATOMIC_GET_SHPTR(&currentContig, &(contig_box_list->contig));
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
                int currentState = currentContig->state;
#else
                int currentState;
                UPC_ATOMIC_GET_I32(&currentState, &currentContig->state);
#endif
                if (originalState != CLAIMED || currentState != CLAIMED) {
                    DBG("Thread %d: Warning: assign_contig_to_box_list detected unclaimed contig. retrying\n", MYTHREAD);
                    allAreClaimed = 0;
                    break;
                } else if (currentContig != contig) {
                    // EGEOR: Atomic write of contig_box_list->contig
                    UPC_ATOMIC_SET_SHPTR(NULL, &(contig_box_list->contig), contig);
                }
            }
            last = contig_box_list;
            // EGEOR: Use the atomically read next pointer
            UPC_ATOMIC_GET_SHPTR(&contig_box_list, &(last->next));
        }
        if (!allAreClaimed) {
            DBG("Thread %d: assign_contig_to_box_list found problem traversing contig list %lld to %lld (orig: %lld). repeating\n", MYTHREAD, (lld)head->contig->contig_id, (lld)contig->contig_id, (lld)head->original->contig_id);
        }
        upc_fence;
    } while (!allAreClaimed);

    assert(contig_box_list == NULL && last != NULL && allAreClaimed);
    // preserve head as the next of contig->myself list, append all existing
    if (!hasSelf) {
        assert(last->next == NULL);
        // EGEOR: Atomic reads/writes below
        UPC_ATOMIC_GET_SHPTR(&tmp, &(contigMyself->next));
        UPC_ATOMIC_SET_SHPTR(NULL, &(last->next), tmp);
        UPC_ATOMIC_SET_SHPTR(NULL, &(contigMyself->next), head);
    }
    upc_fence;
    assert(contigMyself == contig->myself);
    if (HIPMER_VERBOSITY >= LL_DBG) {
        printContigBox(head); printContigBox(contig->myself);
    }
}

void destroy_contig(contig_ptr_t cur_contig)
{
    /* free all the box pointers */
    remote_assert(cur_contig->state != COMPLETE && cur_contig->state != ACTIVE);

    // destroy my own box_ptr only
    if (cur_contig->myself != NULL && cur_contig->myself->original == cur_contig) {
        cur_contig->myself->contig = NULL;
        cur_contig->myself->next = NULL;
        UPC_FREE_CHK0(cur_contig->myself);
        cur_contig->myself = NULL;
    }

    cur_contig->myself = NULL;
    /* free the sequence */
    if (cur_contig->sequence) {
        UPC_FREE_CHK0(cur_contig->sequence);
    }
    cur_contig->sequence = NULL;
    /* free the structure */
    UPC_FREE_CHK0(cur_contig);
}

void destroy_contig_ptr_box_list(shared[] contig_ptr_box_list_t **cpbl, int destroyContigToo)
{
    if (cpbl == NULL) {
        return;
    }
    shared [] contig_ptr_box_list_t * boxList = *cpbl, *tmp = NULL;
    while (boxList != NULL) {
        tmp = boxList->next;
        if (destroyContigToo) {
            destroy_contig(boxList->contig);
        }
        boxList->contig = NULL;
        boxList->next = NULL;
        UPC_FREE_CHK0(boxList);
        boxList = tmp;
    }
    *cpbl = NULL;
}

/*
 * find the canonical orientation for linear contigs
 * for circular contigs, find the least kmer,
 * reverse complement if necessary and
 * rearrange to start with that least kmer
 */
void canonicalize_contig(contig_ptr_t cur_contig, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    assert(cur_contig != NULL);
    assert(upc_threadof(cur_contig) == MYTHREAD);
    assert(cur_contig->state == COMPLETE);
    assert(upc_threadof(cur_contig->sequence) == MYTHREAD);

    int64_t cur_length = cur_contig->end - cur_contig->start + 1;

    if (cur_contig->is_circular == 1) {
        /*
         * FIXME TODO detect & verify cyclic repeat kmer (must be an edge)
         * mer is kmer_len-1
         *
         * straight circle
         * mer ... mer
         *
         * reverse complement:
         * mer ... rev
         * mer .......   mer == rev
         * ....... mer   mer == rev
         *
         * TODO find canonical kmer and shift appropriately
         */
        char start[MAX_KMER_SIZE], end[MAX_KMER_SIZE], start_rev[MAX_KMER_SIZE], end_rev[MAX_KMER_SIZE];

        memcpy(start, (char *)(cur_contig->sequence + cur_contig->start), kmer_len - 1);
        memcpy(start_rev, start, kmer_len - 1);
        reverseComplementINPLACE(start_rev, kmer_len - 1);
        memcpy(end, (char *)(cur_contig->sequence + cur_contig->end - (kmer_len - 1) + 1), kmer_len - 1);
        memcpy(end_rev, end, kmer_len - 1);
        reverseComplementINPLACE(end_rev, kmer_len - 1);

        start[kmer_len - 1] = end[kmer_len - 1] = start_rev[kmer_len - 1] = end_rev[kmer_len - 1] = '\0';


        int revcomp = -1;
        if (strcmp(start, start_rev) == 0 || strcmp(end, end_rev) == 0 || strcmp(start, end_rev) == 0) {
            revcomp = 1;
        } else if (strcmp(start, end) == 0) {
            revcomp = 0;
        }

        if (revcomp < 0) {
            DBG("Thread %d: circular contig %lld %s: %s\nstart:    %s\nrc_start: %s\nend:      %s\nrc_end:   %s\n", MYTHREAD, (lld)cur_contig->contig_id, (revcomp == 1 ? "revcomp" : (revcomp == 0 ? "straight" : "unknown")), (char *)(cur_contig->sequence + cur_contig->start), start, start_rev, end, end_rev);
        }
        if (revcomp == 1) {
            // edge is only repeated kmer, just take the canonical orientation
            if (isLeastOrientation((char *)&(cur_contig->sequence[cur_contig->start]), cur_length) == 0) {
                reverseComplementINPLACE((char *)&(cur_contig->sequence[cur_contig->start]), cur_length);
            }
        } else if (revcomp == 0) { // straight
            if (1) {
                if (isLeastOrientation((char *)&(cur_contig->sequence[cur_contig->start]), cur_length) == 0) {
                    reverseComplementINPLACE((char *)&(cur_contig->sequence[cur_contig->start]), cur_length);
                }
            } else { // TODO test this code before rearranging contig.
                // find the least canonical kmer
                assert(kmer_len < MAX_KMER_SIZE);
                char *least_kmer, best[MAX_KMER_SIZE], tmp[MAX_KMER_SIZE];
                int64_t bestPos = 0;
                best[kmer_len] = tmp[kmer_len] = '\0';
                for (int64_t i = cur_contig->start; i < cur_contig->end - kmer_len + 1; i++) {
                    least_kmer = getLeastKmer((char *)(cur_contig->sequence + i), tmp, kmer_len);
                    if (i == cur_contig->start || strncmp(least_kmer, best, kmer_len) < 0) {
                        memcpy(best, least_kmer, kmer_len);
                        bestPos = i;
                    }
                }

                if (isLeastKmer((char *)(cur_contig->sequence + bestPos), kmer_len) == 0) {
                    reverseComplementINPLACE((char *)&(cur_contig->sequence[cur_contig->start]), cur_length);
                    bestPos = cur_length - bestPos - kmer_len;
                }

                if (bestPos != cur_contig->start) {
                    // shift
                    char *tmpBuffer = (char *)malloc_chk((cur_length + 1) * sizeof(char));
                    memcpy(tmpBuffer, (char *)(cur_contig->sequence + bestPos), cur_contig->end - bestPos + 1);
                    memcpy(tmpBuffer + bestPos - cur_contig->start, (char *)(cur_contig->sequence + cur_contig->start), bestPos - cur_contig->start);
                    memcpy((char *)(cur_contig->sequence + cur_contig->start), tmpBuffer, cur_length);
                }
            }
        }
    } else {
        // find canonical orientation
        if (isLeastOrientation((char *)&(cur_contig->sequence[cur_contig->start]), cur_length) == 0) {
            reverseComplementINPLACE((char *)&(cur_contig->sequence[cur_contig->start]), cur_length);
        }
    }
}


void write_final_contigs(GZIP_FILE output_file, int block_count)
{
    int64_t contig_base;

    UPC_ATOMIC_FADD_I64(&contig_base, &contig_id, block_count);
    // now write out all the remaining contigs
    for (int i = 0; i < block_count; i++) {
        contig_ptr_t ctg = contig_block[i];
        assert(upc_threadof(ctg) == MYTHREAD);
        assert(upc_threadof(ctg->sequence) == MYTHREAD);
        ctg->contig_id = contig_base + i;
        char *seq = (char *)&(ctg->sequence[ctg->start]);
        GZIP_PRINTF(output_file, ">Contig_%lld\n", (lld)ctg->contig_id);
        printFoldedSequence(output_file, (char *)seq, strlen(seq));
        //WARN("%d Writing out contig %ld\n", i, ctg->contig_id);
    }
    contig_offset = 0;
}


void finish_contig(contig_ptr_t cur_contig, GZIP_FILE output_file, int min_contig_length, shared[] contig_ptr_box_list_t **written, shared[] contig_ptr_box_list_t **notWritten, int kmer_len)
{
    // assumes cur_contig->state_lock has been acquired previously!
    assert(upc_threadof(cur_contig) == MYTHREAD);
    assert(cur_contig->state == ACTIVE);

    assert(cur_contig->end + 1 < cur_contig->size);
    DBG("Finishing contig %lld\n", (lld) cur_contig->contig_id);
    cur_contig->sequence[(cur_contig->end) + 1] = '\0';
#ifndef USE_ATOMIC_FOR_CONTIG_STATE
    cur_contig->state = COMPLETE;
#else
    int oldState;
    UPC_ATOMIC_SET_I32(&oldState, &cur_contig->state, COMPLETE);
    if (oldState != ACTIVE) DIE("in finish_contig, contig_id=%lld cur_contig->state must be ACTIVE before becoming COMPLETE. was %d\n", (lld) cur_contig->contig_id, oldState);
#endif

    canonicalize_contig(cur_contig, kmer_len);

    if ((cur_contig->end - cur_contig->start + 1) >= min_contig_length) {
        // temporary id
        cur_contig->contig_id = myContigs;
        myContigs++;
        if (written) {
            add_contig_to_box_list(written, alloc_contig_ptr_box_list(cur_contig));
        }
        contig_block[contig_offset] = cur_contig;
        contig_offset++;
        if (contig_offset == CONTIGNO_BLOCK) {
            write_final_contigs(output_file, CONTIGNO_BLOCK);
        }
    } else if (notWritten != NULL) {
        add_contig_to_box_list(notWritten, alloc_contig_ptr_box_list(cur_contig));
    }

    upc_fence;
}
