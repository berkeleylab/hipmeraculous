#include <stdio.h>
#include "upc.h"

#define MAX_PRINT 8

int main(int argc, char **argv)
{
    if (MYTHREAD == 0 || MYTHREAD == THREADS - 1 || MYTHREAD % ((THREADS + MAX_PRINT - 1) / MAX_PRINT) == 0) {
        printf("Hello, I am %d of %d\n", MYTHREAD, THREADS);
    }
    return 0;
}
