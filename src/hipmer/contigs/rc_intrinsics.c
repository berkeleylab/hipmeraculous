#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#include "rc_intrinsics.h"

#if defined(__AVX512F__) && !defined(NO_AVX512F)
#define CHUNK_SEQ 16
#include <immintrin.h>
#include <xmmintrin.h>
#include <emmintrin.h>


static inline char rc_base(const char base)
{
    char rc;

    switch (base) {
    case 'A':
        rc = 'T'; break;
    case 'C':
        rc = 'G'; break;
    case 'G':
        rc = 'C'; break;
    case 'T':
        rc = 'A'; break;
    default:
        break;
    }
    return rc;
}

static inline void rc_sequence(const char *seq, char *rc_seq, size_t seqLen)
{
    int end = seqLen - 1;
    int start = 0;
    char temp;

    if (seqLen % 2 == 1) {
        rc_seq[(start + end) / 2] = rc_base(seq[(start + end) / 2]);
    }
    while (start < end) {
        temp = seq[end];
        rc_seq[end] = rc_base(seq[start]);
        rc_seq[start] = rc_base(temp);
        ++start;
        --end;
    }
}

void reverseComplementSeq2(const char *seq, char *rc_seq, size_t seqLen)
{
    if (seqLen < CHUNK_SEQ) {
        rc_sequence(seq, rc_seq, seqLen);
    } else {
        int i;
        const int n_blocks = seqLen / (2 * CHUNK_SEQ);
        const int remaining = seqLen - (n_blocks * 2 * CHUNK_SEQ);
        int start_offset = 0;
        int end_offset = seqLen - CHUNK_SEQ;
        const __m128i reverse_mask = _mm_setr_epi8(15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0);
        const __m512i trans_mask = _mm512_set_epi32(7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7);
        const __m512i trans_table = _mm512_set_epi32(0, 0, 0, 0, 0, 0, 0, 0, (int32_t)'C', 0, 0, (int32_t)'A', (int32_t)'G', 0, (int32_t)'T', 0);

        __m128i front_block, back_block;
        __m512i extended_front_block, extended_back_block, translated_front_block, translated_back_block;

        for (i = 0; i < n_blocks; i++) {
            front_block = _mm_lddqu_si128((const __m128i *)&seq[start_offset]);
            front_block = _mm_shuffle_epi8(front_block, reverse_mask);
            extended_front_block = _mm512_cvtepi8_epi32(front_block);

            back_block = _mm_lddqu_si128((const __m128i *)&seq[end_offset]);
            back_block = _mm_shuffle_epi8(back_block, reverse_mask);
            extended_back_block = _mm512_cvtepi8_epi32(back_block);

            extended_front_block = _mm512_and_epi32(extended_front_block, trans_mask);
            translated_front_block = _mm512_permutexvar_epi32(extended_front_block, trans_table);
            front_block = _mm512_cvtepi32_epi8(translated_front_block);

            extended_back_block = _mm512_and_epi32(extended_back_block, trans_mask);
            translated_back_block = _mm512_permutexvar_epi32(extended_back_block, trans_table);
            back_block = _mm512_cvtepi32_epi8(translated_back_block);

            _mm_storeu_si128((__m128i *)&rc_seq[end_offset], front_block);
            _mm_storeu_si128((__m128i *)&rc_seq[start_offset], back_block);

            start_offset += CHUNK_SEQ;
            end_offset -= CHUNK_SEQ;
        }

        if (remaining >= CHUNK_SEQ) {
            /* Case where we have a (remaining) sequence with length >= CHUNK_SEQ && length <= 2*CHUNK_SEQ   */
            front_block = _mm_lddqu_si128((const __m128i *)&seq[start_offset]);
            front_block = _mm_shuffle_epi8(front_block, reverse_mask);
            extended_front_block = _mm512_cvtepi8_epi32(front_block);

            back_block = _mm_lddqu_si128((const __m128i *)&seq[end_offset]);
            back_block = _mm_shuffle_epi8(back_block, reverse_mask);
            extended_back_block = _mm512_cvtepi8_epi32(back_block);

            extended_front_block = _mm512_and_epi32(extended_front_block, trans_mask);
            translated_front_block = _mm512_permutexvar_epi32(extended_front_block, trans_table);
            front_block = _mm512_cvtepi32_epi8(translated_front_block);

            extended_back_block = _mm512_and_epi32(extended_back_block, trans_mask);
            translated_back_block = _mm512_permutexvar_epi32(extended_back_block, trans_table);
            back_block = _mm512_cvtepi32_epi8(translated_back_block);

            _mm_storeu_si128((__m128i *)&rc_seq[end_offset], front_block);
            _mm_storeu_si128((__m128i *)&rc_seq[start_offset], back_block);
        } else if (remaining > 0) {
            /* Case where the remaining sequence has length less than CHUNK_SEQ */
            front_block = _mm_lddqu_si128((const __m128i *)&seq[start_offset]);
            front_block = _mm_shuffle_epi8(front_block, reverse_mask);
            extended_front_block = _mm512_cvtepi8_epi32(front_block);
            extended_front_block = _mm512_and_epi32(extended_front_block, trans_mask);
            translated_front_block = _mm512_permutexvar_epi32(extended_front_block, trans_table);
            front_block = _mm512_cvtepi32_epi8(translated_front_block);
            back_block = _mm_lddqu_si128((const __m128i *)&rc_seq[start_offset - CHUNK_SEQ]);
            _mm_storeu_si128((__m128i *)&rc_seq[start_offset - CHUNK_SEQ + remaining], front_block);
            _mm_storeu_si128((__m128i *)&rc_seq[start_offset - CHUNK_SEQ], back_block);
        }
    }
}
#endif // __AVX512F__
