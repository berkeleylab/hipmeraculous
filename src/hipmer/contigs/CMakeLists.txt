MESSAGE("Building contigs ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

# fix linking when objects are from multiple languages
set(CMAKE_C_IMPLICIT_LINK_LIBRARIES "")
set(CMAKE_C_IMPLICIT_LINK_DIRECTORIES "")
set(CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++")
set(CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "")

SET(CMAKE_EXE_LINKER_FLAGS)
SET_UPC_PROPS(*.upc)

SET_SOURCE_FILES_PROPERTIES(kmer_handling.c packingDNAseq.c rc_intrinsics.c PROPERTIES LANGUAGE "C")

ADD_LIBRARY(rc_intrinsics OBJECT rc_intrinsics.c)

FOREACH(HIPMER_KMER_LEN ${HIPMER_KMER_LENGTHS})
    ADD_LIBRARY(ContigsCommon-${HIPMER_KMER_LEN} OBJECT contigs_common.upc)
    SET_TARGET_PROPERTIES(ContigsCommon-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    add_kmer_defs(ContigsCommon-${HIPMER_KMER_LEN})

    ADD_LIBRARY(UUtraversalContigs-${HIPMER_KMER_LEN} OBJECT UU_traversal_contigs.upc)
    SET_TARGET_PROPERTIES(UUtraversalContigs-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    add_kmer_defs(UUtraversalContigs-${HIPMER_KMER_LEN})

    ADD_LIBRARY(KmerHandling-${HIPMER_KMER_LEN} OBJECT kmer_handling.c)
    add_kmer_defs(KmerHandling-${HIPMER_KMER_LEN})

    ADD_LIBRARY(PackingDNASeq-${HIPMER_KMER_LEN} OBJECT packingDNAseq.c)
    add_kmer_defs(PackingDNASeq-${HIPMER_KMER_LEN})

    ADD_LIBRARY(KmerHashMeraculous-${HIPMER_KMER_LEN} OBJECT kmer_hash_meraculous.upc)
    SET_TARGET_PROPERTIES(KmerHashMeraculous-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    add_kmer_defs(KmerHashMeraculous-${HIPMER_KMER_LEN})

    ADD_LIBRARY(BuildUFXMeraculous-${HIPMER_KMER_LEN} OBJECT buildUFXhashBinary_meraculous.upc)
    SET_TARGET_PROPERTIES(BuildUFXMeraculous-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    add_kmer_defs(BuildUFXMeraculous-${HIPMER_KMER_LEN})

    add_library(meraculous_main-${HIPMER_KMER_LEN} OBJECT meraculous_main.upc)
    set_target_properties(meraculous_main-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
    add_kmer_defs(meraculous_main-${HIPMER_KMER_LEN})

    IF (HIPMER_FULL_BUILD)
      ADD_EXECUTABLE(meraculous-${HIPMER_KMER_LEN} meraculous.upc
        $<TARGET_OBJECTS:meraculous_main-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:UUtraversalContigs-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:rc_intrinsics> 
        $<TARGET_OBJECTS:ContigsCommon-${HIPMER_KMER_LEN}>
        #$<TARGET_OBJECTS:KmerObjects-${HIPMER_KMER_LEN}> 
        $<TARGET_OBJECTS:readufx-${HIPMER_KMER_LEN}>  
        $<TARGET_OBJECTS:PackingDNASeq-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:KmerHandling-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:KmerHashMeraculous-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:BuildUFXMeraculous-${HIPMER_KMER_LEN}>
        $<TARGET_OBJECTS:upc_common>
        $<TARGET_OBJECTS:COMMON>
        $<TARGET_OBJECTS:HIPMER_VERSION>
        $<TARGET_OBJECTS:HASH_FUNCS> 
        $<TARGET_OBJECTS:Buffer>
        $<TARGET_OBJECTS:MemoryChk>
        $<TARGET_OBJECTS:OptList>
        $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
      )
      SET_TARGET_PROPERTIES(meraculous-${HIPMER_KMER_LEN} PROPERTIES LINKER_LANGUAGE "UPC")
      TARGET_LINK_LIBRARIES(meraculous-${HIPMER_KMER_LEN} ${ZLIB_LIBRARIES} ${RT_LIBRARIES}) 
      INSTALL(TARGETS meraculous-${HIPMER_KMER_LEN} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
      add_kmer_defs(meraculous-${HIPMER_KMER_LEN})
    ENDIF()

ENDFOREACH()

set_source_files_properties(hello_world_scale.upc PROPERTIES LANGUAGE "UPC" )
add_executable(hello_world_scale hello_world_scale.upc)
install(TARGETS hello_world_scale DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
