#include "meraculous_main.h"
#include "upc_output.h"

#include "UU_traversal_contigs.h"

shared int64_t contig_id = 0;
int64_t myContigs = 0;

#ifdef PROFILE
int64_t bases_added;
#endif

#ifdef UU_TRAV_PROFILE
double walking_time;
double UU_time;
#endif


int meraculous_main(int argc, char **argv)
{
    upc_tick_t start_time = upc_ticks_now();

    LOG_VERSION;
    int kmer_len = MAX_KMER_SIZE - 1;

#ifdef PROFILE
    bases_added = 0;
#endif

#ifdef UU_TRAV_PROFILE
    walking_time = 0.0;
    UU_time = 0.0;
#endif

    if (_sv != NULL) {
        MYSV.fileIOTime = 0.0;
        MYSV.cardCalcTime = 0.0;
        MYSV.setupTime = 0.0;
        MYSV.storeTime = 0.0;
    }
    myContigs = 0;

    int fileNo = 0;
    int i;
    int64_t size;
    HASH_TABLE_T *dist_hashtable;
    MEMORY_HEAP_T memory_heap;
    GZIP_FILE my_out_file;
    char readFileName[MAX_FILE_PATH];
    char outputBlastmap[MAX_FILE_PATH];
    const char *base_dir = ".";
    shared[1] double *setupTimes = NULL;
    shared[1] double *calcTimes = NULL;
    shared[1] double *ioTimes = NULL;
    shared[1] double *storeTimes = NULL;
    double min_IO, min_calc, min_store, min_setup;
    double max_IO, max_store, max_calc, max_setup;
    double sum_ios = 0.0;
    double sum_calcs = 0.0;
    double sum_stores = 0.0;
    double sum_setups = 0.0;
    int cores_per_node = 0;

    int c;
    char *input_UFX_name, *output_name, *read_files_name;
    int minimum_contig_length = MINIMUM_CONTIG_SIZE, dmin = 10;
    double dynamic_dmin = 1.0;
    int chunk_size = 1;

    option_t *optList, *thisOpt;
    optList = NULL;
    optList = GetOptList(argc, argv, "i:o:m:d:D:c:s:l:f:F:C:B:N:k:X");
    print_args(optList, __func__);

    char *string_size;
    int load_factor = 1;
    char *oracle_file;
    int is_per_thread = 0;
#ifndef UFX_WRITE_SINGLE_FILE
    is_per_thread = 1;
#endif

    /* Sizes should be given in megabytes */
    int64_t outputSizeSimple;
    int64_t outputSizeFasta;

    while (optList != NULL) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'i':
            input_UFX_name = thisOpt->argument;
            break;
        case 'o':
            output_name = thisOpt->argument;
            break;
        case 'k':
            kmer_len = atoi(thisOpt->argument);
            if (kmer_len >= MAX_KMER_SIZE) {
                SDIE("Please compile with a higher MAX_KMER_SIZE (%d) for kmer length %d", MAX_KMER_SIZE, kmer_len);
            }
            if (GET_KMER_PACKED_LEN(kmer_len) > MAX_KMER_PACKED_LENGTH) {
                SDIE("Please compile with a higher MAX_KMER_PACKED_LENGTH (%d) for kmer length %d", MAX_KMER_PACKED_LENGTH, kmer_len);
            }
            break;
        case 'm':
            minimum_contig_length = atoi(thisOpt->argument);
            break;
        case 'd':
            dmin = atoi(thisOpt->argument);
            break;
        case 'D':
            dynamic_dmin = atof(thisOpt->argument);
            break;
        case 'c':
            chunk_size = atoi(thisOpt->argument);
            break;
        case 's':
            string_size = thisOpt->argument;
            break;
        case 'l':
            load_factor = atoi(thisOpt->argument);
            break;
        case 'f':
            oracle_file = thisOpt->argument;
            break;
        case 'F':
            outputSizeFasta = atoi(thisOpt->argument);
            break;
        case 'C':
            outputSizeSimple = atoi(thisOpt->argument);
            break;
        case 'B':
            base_dir = thisOpt->argument;
            break;
        case 'X':
            is_per_thread = 1;
            break;
        case 'N':
            cores_per_node = atoi(thisOpt->argument);
            SET_CORES_PER_NODE(cores_per_node);
            break;
        default:
            break;
        }
    }


    UPC_TICK_T start, end;

    if (MYTHREAD == 0) {
#ifndef NO_PAD
        serial_printf("Struct size is %lu, with %lu padding, %lu shared[] ptr\n",
                      (unsigned long)sizeof(LIST_T), (unsigned long)UPC_PADDING, (unsigned long)sizeof(shared void *));
#else
        serial_printf("Struct size is %lu, no padding, %lu shared[] ptr\n",
                      (unsigned long)sizeof(LIST_T), (unsigned long)sizeof(shared void *));
#endif
        serial_printf("Compiled with MAX_KMER_SIZE=%d MAX_KMER_PACKED_LENGTH=%d\n", MAX_KMER_SIZE, MAX_KMER_PACKED_LENGTH);
        serial_printf("Running with %d cores per node, kmer_len=%d\n", MYSV.cores_per_node, kmer_len);
    }
    if (strlen(output_name) > 200) {
        SDIE("Please choose a shorter output name max 200 chars\n");
    }
    double con_time, trav_time, blastmap_time;


    if (MYTHREAD == 0) {
        printf("\n\n*************** RUNNING VERSION WITH SYNCHRONIZATION PROTOCOL ENABLED ***************\n\n");
    }

#ifdef DEBUG
    if (MYTHREAD == 0) {
        printf("\n\n*************** RUNNING VERSION WITH DEBUG assertions and extra checks ***************\n\n");
    }
#endif

    if (MYTHREAD == 0) {
        printf("\n\n*************** RUNNING LOGGED VERBOSITY (%d) OUTPUT ***********************************\n\n", HIPMER_VERBOSITY);
    }


#ifdef PROFILE
    upc_barrier;
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif
    /* Build UFX hashtable */

    LOGF("Opening UFX with base_dir=%s as %s\n", base_dir, is_per_thread ? "per_rank" : "a single file");

    int64_t myshare;
    int dsize;

    if (is_per_thread && MYSV.checkpoint_path) {
        serial_printf("Forcing a global checkpoint of per thread ufx files\n");
        char tmp[MAX_FILE_PATH];
        sprintf(tmp, "%s%s", input_UFX_name, GZIP_EXT);
        if (!doesLocalCheckpointExist(tmp)) {
            // force a restore from previous checkpoint, if needed
            restoreLocalCheckpoint(tmp);
            // same for the .entries file
            strcat(tmp, ".entries");
            restoreLocalCheckpoint(tmp);
        }
    }

    struct ufx_file_t *UFX_f = UFXInitOpen(input_UFX_name, &myshare, MYTHREAD, base_dir, kmer_len);
    if (!UFX_f) DIE("Could not open %s%s: base_dir\n", input_UFX_name, base_dir);
    size = reduce_long(myshare, UPC_ADD, ALL_DEST);
    
    if (MYTHREAD == 0) {
        int64_t minMemory = (sizeof(LIST_T) + sizeof(int64_t) + load_factor * sizeof(BUCKET_T)) * size / 1024 / 1024 / THREADS;
        serial_printf("Minimum required shared memory: %lld MB. (%lld ufx kmers) If memory runs out re-run with more total memory / nodes\n", (lld)minMemory, (lld)size);
    }
    dist_hashtable = BUILD_UFX_HASH(size, &memory_heap, myshare, dsize, dmin, dynamic_dmin, chunk_size, load_factor, kmer_len, UFX_f);

#ifdef PROFILE
    /* Time the construction of the hashtable */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();

        con_time = UPC_TICKS_TO_SECS(end - start);

        printf("\n\n*********** OVERALL TIME BREAKDOWN ***************\n\n");

        printf("\nTime for constructing UFX hash table is : %f seconds\n", con_time);
    }
#endif

    /* UU-mer graph traversal */

    /* initialze global shared variables */
    if (MYTHREAD == 0) {
        contig_id = 0;
        upc_fence;
    }

    char *outputfile_name = malloc_chk(MAX_FILE_PATH);;
    snprintf(outputfile_name, MAX_FILE_PATH, "%s.fasta" GZIP_EXT, output_name);
    my_out_file = openCheckpoint(outputfile_name, "w");

#ifdef PROFILE
    upc_barrier;

    /* Time the UU-graph traversal */
    if (MYTHREAD == 0) {
        start = UPC_TICKS_NOW();
    }
#endif

    UU_GRAPH_TRAVERSAL(dist_hashtable, my_out_file, minimum_contig_length, kmer_len);

#ifdef PROFILE
    upc_barrier;
    /* Time the UU-graph traversal */
    if (MYTHREAD == 0) {
        end = UPC_TICKS_NOW();

        trav_time = UPC_TICKS_TO_SECS(end - start);

        printf("\nTime for UU-graph traversal is : %f seconds\n", trav_time);
        printf("\nTotal time is :  %f seconds\n", trav_time + con_time);
        printf("\nTotal contigs stored: %lld\n", (lld)contig_id);
        ADD_DIAG("%lld", "total_contigs", (lld)contig_id);
    }
#endif

#ifdef DETAILED_BUILD_PROFILE
    /* Measure load imbalance in blastmap */
    upc_barrier;
    UPC_ALL_ALLOC_CHK(ioTimes, THREADS, sizeof(double));
    UPC_ALL_ALLOC_CHK(storeTimes, THREADS, sizeof(double));
    UPC_ALL_ALLOC_CHK(calcTimes, THREADS, sizeof(double));
    UPC_ALL_ALLOC_CHK(setupTimes, THREADS, sizeof(double));
    upc_barrier;
    if (_sv != NULL) {
        ioTimes[MYTHREAD] = MYSV.fileIOTime;
        setupTimes[MYTHREAD] = MYSV.setupTime;
        storeTimes[MYTHREAD] = MYSV.storeTime;
        calcTimes[MYTHREAD] = MYSV.cardCalcTime;
    }
    upc_barrier;

    if (MYTHREAD == 0) {
        min_IO = ioTimes[0];
        max_IO = ioTimes[0];
        min_setup = setupTimes[0];
        max_setup = setupTimes[0];
        min_store = storeTimes[0];
        max_store = storeTimes[0];
        min_calc = calcTimes[0];
        max_calc = calcTimes[0];


        for (i = 0; i < THREADS; i++) {
            sum_ios += ioTimes[i];
            sum_stores += storeTimes[i];
            sum_setups += setupTimes[i];
            sum_calcs += calcTimes[i];

            if (ioTimes[i] < min_IO) {
                min_IO = ioTimes[i];
            }

            if (ioTimes[i] > max_IO) {
                max_IO = ioTimes[i];
            }

            if (storeTimes[i] < min_store) {
                min_store = storeTimes[i];
            }

            if (storeTimes[i] > max_store) {
                max_store = storeTimes[i];
            }

            if (calcTimes[i] < min_calc) {
                min_calc = calcTimes[i];
            }

            if (calcTimes[i] > max_calc) {
                max_calc = calcTimes[i];
            }

            if (setupTimes[i] < min_setup) {
                min_setup = setupTimes[i];
            }

            if (setupTimes[i] > max_setup) {
                max_setup = setupTimes[i];
            }
        }
        printf("\n******** CONSTRUCTION DETAILED STATISTICS ********\n");
        printf("IOs: Avg %f\tMin %f\tMax %f\n", sum_ios / THREADS, min_IO, max_IO);
        printf("Calculating cardinalities: Avg %f\tMin %f\tMax %f\n", sum_calcs / THREADS, min_calc, max_calc);
        printf("Seting up: Avg %f\tMin %f\tMax %f\n", sum_setups / THREADS, min_setup, max_setup);
        printf("Storing k-mers: Avg %f\tMin %f\tMax %f\n", sum_stores / THREADS, min_store, max_store);
#ifdef LHS_PERF
        printf("Percentage of node-local lookups: %f %%\n", (100.0 * success) / (1.0 * lookups));
#endif
    }

    upc_barrier;
    UPC_ALL_FREE_CHK(ioTimes);
    UPC_ALL_FREE_CHK(storeTimes);
    UPC_ALL_FREE_CHK(calcTimes);
    UPC_ALL_FREE_CHK(setupTimes);

#endif // DETAILED_BUILD_PROFILE

    upc_barrier;
    closeCheckpoint(my_out_file);

#ifdef PROFILE
#ifdef WORKLOAD_PROFILING
    if (MYTHREAD == 0) {
        printf("\n------------------------- STATS for UU graph traversal ----------------------------\n");
    }
    upc_barrier;
    closeCheckpoint(my_out_file);
    printf("Thread %d added %lld bases in total\n", MYTHREAD, bases_added);
#endif
#endif

    free_chk(outputfile_name);

#ifdef UU_TRAV_PROFILE
    printf("Thread %d spent %f seconds in UU-graph traversal, %f seconds in walking (%f %%)\n", MYTHREAD, UU_time, walking_time, walking_time / UU_time * 100.0);
#endif

    upc_barrier;

    DESTROY_HASH_TABLE(&dist_hashtable, &memory_heap);
    assert(dist_hashtable == NULL);

    /* Print some metafiles used in downstream steps */
    if (MYTHREAD == 0) {
        char countFileName[MAX_FILE_PATH];
        snprintf(countFileName, MAX_FILE_PATH, "n%s.txt", output_name);
        FILE *countFD = fopen_rank_path(countFileName, "w", -1);
        fprintf(countFD, "%lld\n", (lld)contig_id);
        fclose_track(countFD);
    }

    {
        char mycountFileName[MAX_FILE_PATH];
        snprintf(mycountFileName, MAX_FILE_PATH, "my%s.txt", output_name);
        FILE *mycountFD = openCheckpoint0(mycountFileName, "w");
        fprintf(mycountFD, "%lld\n", (lld)myContigs);
        closeCheckpoint0(mycountFD);
    }

    UFXClose(UFX_f);

    upc_barrier;

    if (!MYTHREAD) {
        printf("Overall time for %s is %.2f s\n", basename(argv[0]),
               ((double)upc_ticks_to_ns(upc_ticks_now() - start_time) / 1000000000.0));
    }

    return 0;
}
