#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "kmer_handling.h"
#include "common.h"

#include "rc_intrinsics.h"


void reverseComplementBase(char *base)
{
    switch (base[0]) {
    case 'A':
        base[0] = 'T';
        break;
    case 'C':
        base[0] = 'G';
        break;
    case 'G':
        base[0] = 'C';
        break;
    case 'T':
        base[0] = 'A';
        break;
    default:
        break;
    }
}

char reverseComplementBaseStrict(const char base)
{
    char rc;

    switch (base) {
    case 'A':
        rc = 'T'; break;
    case 'C':
        rc = 'G'; break;
    case 'G':
        rc = 'C'; break;
    case 'T':
        rc = 'A'; break;
    default:
#ifdef DEBUG
        WARN("Unexpected base in reverseComplementBaseStrict: %c %d\n", base, (int)base);
        assert(0);
#endif
        DIE("Unexpected base in reverseComplementBaseStrict: %c %d\n", base, (int)base);
    }
    return rc;
}

char reverseComplementBaseExt(const char base)
{
    char rc;

    switch (base) {
    case 'A':
        rc = 'T'; break;
    case 'C':
        rc = 'G'; break;
    case 'G':
        rc = 'C'; break;
    case 'T':
        rc = 'A'; break;
    case 'F':
        rc = 'F'; break;
    case 'X':
        rc = 'X'; break;
    default:
#ifdef DEBUG
        WARN("Unexpected base in reverseComplementBase: %c %d\n", base, (int)base);
        assert(0);
#endif
        DIE("Unexpected base in reverseComplementBaseExt: %c %d\n", base, (int)base);
    }
    return rc;
}

void reverseComplementSeq(const char *seq, char *rc_seq, size_t seqLen)
{
    int32_t end = seqLen, start = 0;
    char tmp;
    static const int8_t rc_table[128] = {
        4, 4,  4, 4,  4,  4,  4, 4,  4, 4, 4, 4, 4, 4, 4, 4,
        4, 4,  4, 4,  4,  4,  4, 4,  4, 4, 4, 4, 4, 4, 4, 4,
        4, 4,  4, 4,  4,  4,  4, 4,  4, 4, 4, 4, 4, 4, 4, 4,
        4, 4,  4, 4,  4,  4,  4, 4,  4, 4, 4, 4, 4, 4, 4, 4,
        4, 84, 4, 71, 4,  4,  4, 67, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4,  4, 4,  65, 65, 4, 4,  4, 4, 4, 4, 4, 4, 4, 4,
        4, 84, 4, 71, 4,  4,  4, 67, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4,  4, 4,  65, 65, 4, 4,  4, 4, 4, 4, 4, 4, 4, 4
    };

    --end;
    while (start < end) {
        tmp = seq[start];
        rc_seq[start] = (char)rc_table[(int8_t)seq[end]];
        rc_seq[end] = (char)rc_table[(int8_t)tmp];
        ++start;
        --end;
    }
    if (start == end) {
        rc_seq[start] = (char)rc_table[(int8_t)seq[start]];
    }
}

void reverseComplementKmer(const char *kmer, char *rc_kmer, int kmer_len)
{
    reverseComplementSeq2(kmer, rc_kmer, kmer_len);
}

void reverseComplementExtensions(const char *ext, char *rc_ext)
{
    rc_ext[1] = reverseComplementBaseExt(ext[0]);
    rc_ext[0] = reverseComplementBaseExt(ext[1]);
}

void reverseComplementINPLACE(char *subcontig, int64_t size)
{
    reverseComplementSeq2(subcontig, subcontig, size);
}

char *getLeastKmer(const char *kmer, char *temp, int kmer_len)
{
    /* Search for the canonical kmer */
    reverseComplementKmer(kmer, temp, kmer_len);
    int lex_ind = strncmp(kmer, temp, kmer_len);
    if (lex_ind < 0) {
        return (char *)kmer;
    } else {
        return temp;
    }
}

int isLeastKmer(const char *kmer, int kmer_len)
{
    assert(kmer_len < MAX_KMER_SIZE);
    char tempKmer[MAX_KMER_SIZE];
    memset(tempKmer, 0, MAX_KMER_SIZE);     // initialize bytes
    return getLeastKmer(kmer, tempKmer, kmer_len) == (char *)kmer;
}

int isLeastOrientation(const char *seq, int64_t size)
{
    int64_t start = 0, end = size - 1;

    while (start < end) {
        char rc = seq[end];
        reverseComplementBase(&rc);

        if (seq[start] < rc) {
            return 1;
        } else if (seq[start] > rc) {
            return 0;
        }
        start++; end--;
    }
    return 1;
}

uint64_t hashkmer(int64_t hashtable_size, char *seq, int kmer_len)
{
    return hashkey(hashtable_size, seq, kmer_len);
}
