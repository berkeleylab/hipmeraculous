#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#if defined(__AVX512F__) && !defined(NO_AVX512F)
void reverseComplementSeq2(const char *seq, char *rc_seq, size_t seqLen);
#else
#include "kmer_handling.h"
#define reverseComplementSeq2(seq, rc_seq, seqLen) reverseComplementSeq(seq, rc_seq, seqLen)
#endif
