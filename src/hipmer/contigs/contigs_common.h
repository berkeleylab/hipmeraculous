#ifndef CONTIGS_COMMON_H_
#define CONTIGS_COMMON_H_

#include <math.h>

#include "StaticVars.h"
#include "upc_common.h"
#include "common.h"
#include "log.h"
#include "timers.h"

#include "meraculous.h"
#include "kmer_handling.h"

/* Define constants for states etc */
#define USED 1
#define UNUSED 0
#define FINISHED 1
#define UNFINISHED 0
#define TRUE 1
#define FALSE 0
#define ACTIVE 0
#define ABORTED 1
#define CLAIMED 2
#define COMPLETE 3

#define LOCK_CONTIG(contig_ptr) do { \
    double start_contig_lock = now(); \
    DBG("Locking contig %lld oldState=%d...", (lld) contig_ptr->contig_id, contig_ptr->state); \
    upc_lock(contig_ptr->state_lock); \
    DBG(" currentState=%d\n", contig_ptr->state); \
    addCountTime(&MYSV.contigLocks, now() - start_contig_lock); \
} while (0)

#define UNLOCK_CONTIG(contig_ptr) do { \
    DBG("Un-Locking contig %lld with state=%d\n", (lld) contig_ptr->contig_id, contig_ptr->state); \
    upc_unlock(contig_ptr->state_lock); \
} while (0)

/* Contig data structure */
typedef struct contig_t contig_t;
typedef shared[] contig_t *contig_ptr_t;
typedef struct contig_ptr_box_list_t contig_ptr_box_list_t;

extern int64_t contig_offset; // defined in contigs_common.upc

/* contig "box" holding contig_t pointers in a linked list and list of merged contigs */
struct contig_ptr_box_list_t {
    shared[] contig_ptr_box_list_t * next;
    contig_ptr_t contig;
    contig_ptr_t original;     // never changes after creation of the box
};

struct contig_t {
    upc_lock_t *state_lock;                                             // Use this lock to synchronize threads when concatenating contigs
    int64_t     state;                                                  // State of the contig
    shared[] char *sequence;                                            // Actual content of the contig
    int64_t     size;                                                   // Current size of the contig
    int64_t     start;                                                  // position of the first base in the sequence
    int64_t     end;                                                    // position of the last base in the sequence
    int64_t     is_circular;                                            // 1 if the contig is circular or otherwise wraps onto itself
    int64_t     contig_id;                                              // The unique id of the contig
    shared[] contig_ptr_box_list_t * myself;                            // a linked list pointer of contigs.  This is the head ptr, which never changes after creation
};

/* contigScaffoldMap data structure */
typedef struct contigScaffoldMap_t contigScaffoldMap_t;
struct contigScaffoldMap_t {
    int     cStrand;
    int64_t scaffID;
    int     sStart;
    int     sEnd;
};

/* contigScaffoldMap data structure */
typedef struct contigNeighborhood_t contigNeighborhood_t;
struct contigNeighborhood_t {
    int64_t myID;
    shared[] char *seq;
    int64_t leftNeighborIDs[4];
    int64_t rightNeighborIDs[4];
    char    firstKmer[MAX_KMER_SIZE];
    char    lastKmer[MAX_KMER_SIZE];
    float   depth;
    int32_t length;
    char    leftExt;
    char    rightExt;
};

void reallocateContig(contig_ptr_t cur_contig, int64_t new_size, int64_t new_start);

void reallocateContigIfNeededLR(contig_ptr_t cur_contig, int leftBases, int rightBases, float growthFactor);

void reallocateContigIfNeeded(contig_ptr_t cur_contig);

/* unsafe in parallel; use only on local thread or when locked */
void add_contig_to_box_list(shared[] contig_ptr_box_list_t **contig_box_list, shared[] contig_ptr_box_list_t *new_box);

shared[] contig_ptr_box_list_t *alloc_contig_ptr_box_list(contig_ptr_t contig);

void printContigBox(shared[] contig_ptr_box_list_t *head);

void assign_contig_to_box_list(shared[] contig_ptr_box_list_t *head, contig_ptr_t contig);

void destroy_contig(contig_ptr_t cur_contig);

void destroy_contig_ptr_box_list(shared[] contig_ptr_box_list_t **cpbl, int destroyContigToo);

/*
 * find the canonical orientation for linear contigs
 * for circular contigs, find the least kmer,
 * reverse complement if necessary and
 * rearrange to start with that least kmer
 */
void canonicalize_contig(contig_ptr_t cur_contig, int kmer_len);

void write_final_contigs(GZIP_FILE output_file, int block_count);

void finish_contig(contig_ptr_t cur_contig, GZIP_FILE output_file, int min_contig_length, shared[] contig_ptr_box_list_t **written, shared[] contig_ptr_box_list_t **notWritten, int kmer_len);

#endif // CONTIGS_COMMON_H_
