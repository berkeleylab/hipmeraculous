#ifndef PACKING_DNA_SEQ_H
#define PACKING_DNA_SEQ_H

#include <math.h>
#include <assert.h>
#include <string.h>

unsigned int packedCodeToFourMer[256];

void init_LookupTable();

unsigned char convertFourMerToPackedCode(unsigned char *fourMer);

unsigned char convertExtensionsToPackedCode(unsigned char *ext);

void convertPackedCodeToExtension(unsigned char packed_ext, char *left_ext, char *right_ext);

void packSequence(const unsigned char *seq_to_pack, unsigned char *m_data, int m_len);

void unpackSequence(const unsigned char *seq_to_unpack, unsigned char *unpacked_seq, int kmer_len);

int comparePackedSeq(const unsigned char *seq1, const unsigned char *seq2, int seq_len);

typedef struct kmer_and_ext_t kmer_and_ext_t;
struct kmer_and_ext_t {
    char left_kmer_right[MAX_KMER_SIZE + 3];
};

char *getKmer(kmer_and_ext_t *kne);
char *getLeftKmer(kmer_and_ext_t *kne);
char *getLeftExt(kmer_and_ext_t *kne);

char *getRightKmer(kmer_and_ext_t *kne);
char *getRightExt(kmer_and_ext_t *kne, int kmer_length);
void setEnd(kmer_and_ext_t *kne, int kmer_length);

#endif // PACKING_DNA_SEQ_H
