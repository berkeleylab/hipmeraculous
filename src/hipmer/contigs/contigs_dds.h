#ifndef CONTIGS_DDS_H
#define CONTIGS_DDS_H

#include <upc.h>

#ifndef CONTIGS_DDS_TYPE
#error "You must specify CONTIGS_DDS_TYPE before including this file"
#endif

#include "upc_compatibility.h"
#include "common.h"
#include "contigs_common.h"

#define oracle_t unsigned char

/* Hash-table entries data structure (i.e. kmers and extensions) */
#define LIST_T JOIN(CONTIGS_DDS_TYPE, list_t)
typedef struct LIST_T LIST_T;

#ifdef MARK_POS_IN_CONTIG
#define LIST_POS_IN_CONTIG int posInContig; /* position of kmer in the UU contig */
#else
#define LIST_POS_IN_CONTIG                  /* no field */
#endif

#ifdef COUNT_INFO
#define COUNT_VAR int32_t myCount; /* count of the kmer */
#else
#define COUNT_VAR                  /* no field */
#endif

#ifdef FIND_NEW_KMERS
#define NEW_COUNTS_VAR \
    int32_t lefts[4], rights[4]; /* for findNewKmers */ \
    char hasContigConflicts;     /* for findNewKmers */
#else
#define NEW_COUNTS_VAR           /* no field */
#endif

#ifdef MERACULOUS

#    define LIST_STRUCT_CONTENTS \
    UPC_ATOMIC_MIN_T used_flag;                       /* State flag 32 or 64 bits depending on architecture */ \
    shared[] LIST_T * next;                           /* Pointer to next entry in the same bucket */ \
    shared[] contig_ptr_box_list_t * my_contig;       /* Pointer to the "box" of the contig ptr this k-mer has been added to */ \
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH]; /* The packed key of the k-mer */       \
    LIST_POS_IN_CONTIG              \
    COUNT_VAR \
    NEW_COUNTS_VAR \
    unsigned char packed_extensions;                /* The packed extensions of the k-mer */

#elif MERDEPTH

#  define LIST_STRUCT_CONTENTS \
    shared[] LIST_T * next;                           /* Pointer to next entry in the same bucket */ \
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH]; /* The packed key of the k-mer */ \
    int count;                                        /* State flag */ \
    char left_ext; \
    char right_ext; \

#elif CEA

#  define LIST_STRUCT_CONTENTS \
    shared[] LIST_T * next;                           /* Pointer to next entry in the same bucket */ \
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH]; /* The packed key of the k-mer */ \
    char left_ext;  \
    char right_ext; \



#elif NB_EXPLORE

#  define LIST_STRUCT_CONTENTS \
    shared[] LIST_T * next;                           /* Pointer to next entry in the same bucket */ \
    int64_t contID; \
    unsigned char packed_key[MAX_KMER_PACKED_LENGTH]; /* The packed key of the k-mer */ \
    int count; \
    char left_ext;  \
    char right_ext;

#else

#  error "Must specify MERACULOUS, MERDEPTH, NB_EXPLORE or CEA"

#endif

#define UNPADDED_LIST_T JOIN(CONTIGS_DDS_TYPE, unpadded_list_t)
typedef struct UNPADDED_LIST_T UNPADDED_LIST_T;
struct UNPADDED_LIST_T {
    LIST_STRUCT_CONTENTS
};

//#define NO_PAD
#ifndef UPC_PAD_TO
#define UPC_PAD_TO 64
#endif
#define UPC_PADDING (sizeof(UNPADDED_LIST_T) % UPC_PAD_TO == 0 ? 0 : UPC_PAD_TO - sizeof(UNPADDED_LIST_T) % UPC_PAD_TO)

struct LIST_T {
    LIST_STRUCT_CONTENTS
#ifndef NO_PAD
    unsigned char _padding_for_UPC_bug[UPC_PADDING];
#endif
};

#endif // CONTIGS_DDS_H
