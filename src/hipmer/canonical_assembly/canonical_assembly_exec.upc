#include "canonical_assembly.h"

int main(int argc, char **argv)
{
    OPEN_MY_LOG("canonical_assembly");
    int ret = canonical_assembly_main(argc, argv);
    CLOSE_MY_LOG;
    return ret;
}
