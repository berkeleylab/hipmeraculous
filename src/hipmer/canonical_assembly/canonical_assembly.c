#include "canonical_assembly.h"

#define TICKS_TO_S(t) ((double)upc_ticks_to_ns(t) / 1000000000.0)
#define ELAPSED_TIME(t) TICKS_TO_S(upc_ticks_now() - (t))

//#define CHECK_ALL

#define CHECK_SEQ(s, len) do {                                          \
        DBG2("Checking %s of len %d: %.*s\n", # s, len, len > 80 ? 80 : len, s); \
        for (int64_t si = 0; si < len; si++) {                                \
            if (((s)[si]) != 'A' && ((s)[si]) != 'C' && ((s)[si]) != 'G' && ((s)[si]) != 'T') { \
                DIE("incorrect sequence letter at pos %lld (len %d): %c (%d)\n", \
                    (lld)si, len, (s)[si], (int)((s)[si])); } }                  \
} while (0)

typedef struct {
    int32_t len;
    int32_t node_i;
    int64_t pos;
} seq_index_t;

typedef struct {
    // this is what gets sorted, and each index points to an offset in the seqs block
    shared [] seq_index_t * indexes;
    // the block of sequence data
    shared [] char *seqs;
    int64_t num;
    int64_t size;
    int32_t put_flag;
} seq_set_t;

typedef struct {
    seq_index_t *indexes;
    char *       seqs;
    int64_t      num;
    int64_t      size;
} local_seq_set_t;

typedef struct {
    seq_index_t *index;
    int64_t      array_i;
} pq_elem_t;

// priority queue for merging
typedef struct {
    pq_elem_t *      elems;
    int64_t          num;
    int64_t          capacity;
    local_seq_set_t *seq_sets;
} pq_t;

#define FASTA_LINE_WIDTH 100

static char _fa_fname[MAX_FILE_PATH] = "assembly.";
static char _fa_fname_suffix[MAX_FILE_PATH] = ".fa";
static char _output_fname[MAX_FILE_PATH] = "final_assembly.fa";
static int _dump_only = 0;
static int _min_length = 0;
static int _max_length = 0;
static int64_t _num_buffer_threads = 4;

static upc_tick_t _t_start;

// has to be global to be accessed from qsort
local_seq_set_t _my_seq_set;

static int cmp_my_seqs(const void *p1, const void *p2)
{
    seq_index_t *s1 = (seq_index_t *)p1;
    seq_index_t *s2 = (seq_index_t *)p2;

    if (s1->len != s2->len || (s1->len == 0 && s2->len == 0)) {
        return s1->len - s2->len;
    }
    return strncmp(_my_seq_set.seqs + s1->pos, _my_seq_set.seqs + s2->pos, s1->len);
}

static void revcomp(char *seq, int len)
{
    char *rev_seq = strndup(seq, len);

    switch_code(reverse(rev_seq));
    if (strcasecmp(rev_seq, seq) < 0) {
        memcpy(seq, rev_seq, len);
    }
    free(rev_seq);
}

static int cmp_seqs(char *s1, int len1, char *s2, int len2)
{
    if (len1 != len2) {
        return len1 - len2;
    }
    return strncmp(s1, s2, len1);
}

static int cmp_indexes(local_seq_set_t *seq_sets, seq_index_t *i1, seq_index_t *i2)
{
    return cmp_seqs(seq_sets[i1->node_i].seqs + i1->pos, i1->len,
                    seq_sets[i2->node_i].seqs + i2->pos, i2->len);
}

static void pq_init(pq_t *pq, int64_t capacity, local_seq_set_t *seq_sets)
{
    pq->num = 0;
    pq->capacity = capacity;
    pq->elems = malloc_chk(capacity * sizeof(pq_elem_t));
    pq->seq_sets = seq_sets;
}

static void pq_destroy(pq_t *pq)
{
    if (pq) { 
        if (pq->elems) {
            assert(pq->capacity);
            free_chk(pq->elems);
        } else {
            assert(pq->capacity == 0);
        }
        pq->capacity = 0;
    }
}

static void swap_pq_elems(pq_t *pq, int64_t i, int64_t j)
{
    pq_elem_t tmp = pq->elems[i];

    pq->elems[i] = pq->elems[j];
    pq->elems[j] = tmp;
}

//#define DEBUG

static void pq_insert(pq_t *pq, seq_index_t *index, int64_t array_i)
{
    if (pq->num == pq->capacity) {
        DIE("PQ is full - this shouldn't happen\n");
    }
    DBG2("inserting elem len %d\n", index->len);
    int64_t i;
    for (i = pq->num; i > 0 && cmp_indexes(pq->seq_sets, index, pq->elems[i / 2].index) < 0; i /= 2) {
        pq->elems[i] = pq->elems[i / 2];
        DBG2("new elem len %d < elem %lld len %d\n", index->len, (lld)i / 2, pq->elems[i / 2].index->len);
    }
    pq->elems[i].index = index;
    pq->elems[i].array_i = array_i;
    pq->num++;
}

static pq_elem_t *pq_delete(pq_t *pq)
{
    if (!pq->num) {
        return NULL;
    }
    pq_elem_t *elem = malloc_chk(sizeof(pq_elem_t));
    *elem = pq->elems[0];
    DBG2("getting elem len %d\n", elem->index->len);
    pq->elems[0] = pq->elems[--pq->num];
    int64_t i = 0;
    pq_elem_t tmp = pq->elems[i];
    int64_t child = i;
    for (; i * 2 < pq->num; i = child) {
        child = i * 2;
        if (child < pq->num - 1 && cmp_indexes(pq->seq_sets, pq->elems[child + 1].index, pq->elems[child].index) < 0) {
            child++;
        }
        if (cmp_indexes(pq->seq_sets, pq->elems[child].index, tmp.index) < 0) {
            pq->elems[i] = pq->elems[child];
        } else {
            break;
        }
    }
    pq->elems[i] = tmp;
    return elem;
}

static seq_index_t *heap_merge(local_seq_set_t *seq_sets, int64_t nodes, int64_t tot_num_seqs)
{
    upc_tick_t t_merge = upc_ticks_now();
    pq_t pq;

    pq_init(&pq, tot_num_seqs, seq_sets);
    seq_index_t *sorted_indexes = malloc_chk(sizeof(seq_index_t) * tot_num_seqs);

    for (int64_t i = 0; i < nodes; i++) {
        if (seq_sets[i].num) {
            pq_insert(&pq, &seq_sets[i].indexes[0], 0);
        }
    }

    for (int64_t i = 0, prev_len = 0; i < tot_num_seqs; i++) {
        pq_elem_t *current = pq_delete(&pq);
        if (!current) {
            DIE("Couldn't get elem from pq at pos %lld\n", (lld)i);
        }
        if (current->index->len < prev_len) {
            DIE("got out of order element from pq at position %lld: %d < %lld\n",
                (lld)i, current->index->len, (lld)prev_len);
        }
        prev_len = current->index->len;
        sorted_indexes[i] = *(current->index);
        local_seq_set_t *seq_set = &seq_sets[current->index->node_i];
        if (current->array_i < seq_set->num - 1) {
            int64_t next_array_i = current->array_i + 1;
            pq_insert(&pq, &(seq_set->indexes[next_array_i]), next_array_i);
        }
        free_chk(current);
    }

    pq_destroy(&pq);
    serial_printf("Merging took %.3f s at %.3f\n", ELAPSED_TIME(t_merge), ELAPSED_TIME(_t_start));
    return sorted_indexes;
}

static local_seq_set_t *fetch_seq_sets(shared seq_set_t *seq_sets, int64_t tot_num_seqs, int cores_per_node)
{
    // this is done only on thread 0
    assert(MYTHREAD == 0);
    int64_t nodes = ((int64_t)THREADS) / cores_per_node;
    upc_tick_t t_get = upc_ticks_now();
    int64_t nodes_done = 0;
    int64_t node_i = -1;
    int64_t found_tot_num = 0;
    local_seq_set_t *local_seq_sets = malloc_chk(sizeof(local_seq_set_t) * nodes);
    //seq_index_t *sorted_indexes = malloc_chk(sizeof(seq_index_t) * tot_num_seqs);
    // keep iterating through all the nodes until we have merged all sets
    do {
        node_i++;
        if (node_i == nodes) {
            node_i = 0;
        }
        int32_t put_flag;
        UPC_ATOMIC_GET_I32(&put_flag, &(seq_sets[node_i].put_flag));
        // skip it if already done or not yet received
        if (put_flag == 2 || put_flag == 0) {
            continue;
        }
        if (put_flag != 1) {
            DIE("wrong value for put flag: %d\n", put_flag);
        }
        // now put should be one, i.e. data is ready
        local_seq_sets[node_i].num = seq_sets[node_i].num;
        local_seq_sets[node_i].size = seq_sets[node_i].size;
        if (upc_threadof(seq_sets[node_i].indexes) == MYTHREAD) {
            // local to thread 0, just set the pointer
            local_seq_sets[node_i].indexes = (seq_index_t *)seq_sets[node_i].indexes;
            local_seq_sets[node_i].seqs = (char *)seq_sets[node_i].seqs;
        } else {
            local_seq_sets[node_i].indexes = malloc_chk(seq_sets[node_i].num * sizeof(seq_index_t));
            upc_memget(local_seq_sets[node_i].indexes, seq_sets[node_i].indexes, seq_sets[node_i].num * sizeof(seq_index_t));
            UPC_FREE_CHK0(seq_sets[node_i].indexes);
            local_seq_sets[node_i].seqs = malloc_chk(seq_sets[node_i].size);
            upc_memget(local_seq_sets[node_i].seqs, seq_sets[node_i].seqs, seq_sets[node_i].size);
            UPC_FREE_CHK0(seq_sets[node_i].seqs);
        }
        int64_t num = seq_sets[node_i].num;
        found_tot_num += num;
        //serial_printf("Merging in %d seqs from node %d\n", num, node_i);
        //serial_printf("Fetched %d seqs from node %d\n", num, node_i);
        //sorted_merge(sorted_indexes, local_seq_sets, node_i, found_tot_num);
        // this doesn't need to be atomic because it won't be touched by the remote thread again
        seq_sets[node_i].put_flag = 2;
        nodes_done++;
    } while (nodes_done < nodes);

    if (found_tot_num != tot_num_seqs) {
        DIE("Mismatch between number sequences found %lld and number counted %lld\n",
            (lld)found_tot_num, (lld)tot_num_seqs);
    }
    serial_printf("Fetching %lld sequences from %lld nodes took %.3f s at %.3f\n",
                  (lld)tot_num_seqs, (lld)nodes, ELAPSED_TIME(t_get), ELAPSED_TIME(_t_start));
    return local_seq_sets;
}

static void write_sequences(seq_index_t *sorted_indexes, local_seq_set_t *seq_sets, int64_t tot_num_seqs)
{
    upc_tick_t t_write = upc_ticks_now();
    char fname[MAX_FILE_PATH + 20];

    sprintf(fname, "results/%s", _output_fname);
    check_file_does_not_exist(fname);
    serial_printf("Writing canonical sequences to %s\n", fname);
    FILE *f = fopen_chk(fname, "w");
    for (int64_t i = 0; i < tot_num_seqs; i++) {
        if (sorted_indexes[i].len < _min_length || (_max_length > 0 && sorted_indexes[i].len >= _max_length)) {
            continue;
        }
        fprintf(f, ">%lld-%d\n%.*s\n", (lld)i, sorted_indexes[i].len, sorted_indexes[i].len,
                seq_sets[sorted_indexes[i].node_i].seqs + sorted_indexes[i].pos);
    }
    fclose_track(f);
    serial_printf("Writing took %.3f s at %.3f\n", ELAPSED_TIME(t_write), ELAPSED_TIME(_t_start));
}

static void compute_stats(seq_index_t *sorted_indexes, int64_t tot_num_seqs)
{
    upc_tick_t t_stats = upc_ticks_now();
    // now compute N50 and L50. It's almost free since the scaffolds are already sorted
    long tot_size = 0;
    int32_t *lens = malloc_chk(tot_num_seqs * sizeof(*lens));

    for (int64_t i = 0; i < tot_num_seqs; i++) {
        if (sorted_indexes[i].len < _min_length || (_max_length > 0 && sorted_indexes[i].len >= _max_length)) {
            continue;
        }
        tot_size += sorted_indexes[i].len;
        lens[i] = sorted_indexes[i].len;
    }
    long running_tot = 0;
    const int NUM_INDEXES = 5;
    double indexes[] = { 0.1, 0.25, 0.5, 0.75, 0.9 };
    int n_index = 0;
    serial_printf("Assembly stats:\n\tSize %lld\n", (lld)tot_size);
    ADD_DIAG("%lld", "assembly_size", (lld)tot_size);
    for (int64_t i = tot_num_seqs - 1; i >= 0 && n_index < NUM_INDEXES; i--) {
        running_tot += lens[i];
        if (running_tot >= indexes[n_index] * tot_size) {
            int64_t nx = (int)(indexes[n_index] * 100);
            serial_printf("\tN%lld %d L%lld %lld\n", (lld)nx, lens[i], (lld)nx, (lld)i + 1);
            if (nx == 50) {
                ADD_DIAG("%d", "N50", lens[i]);
                ADD_DIAG("%lld", "L50", (lld)i + 1);
            }
            n_index++;
        }
    }
    if (lens) free_chk(lens);
    serial_printf("Computed stats in %.3f s\n", ELAPSED_TIME(t_stats));
}

static void add_canonical_fasta(int64_t *num, Buffer seq, int len, int64_t pos)
{
    DBG2("adding canonical fasta(%lld, '%.*s', len=%lld, pos=%lld\n", (lld) * num, (int)(len > 80 ? 80 : len), getStartBuffer(seq), (lld)len, (lld)pos);
    if (len <= 0) {
        return;
    }
    _my_seq_set.indexes[*num].len = len;
    _my_seq_set.indexes[*num].pos = pos;
    revcomp(getStartBuffer(seq), len);
    strncpy(_my_seq_set.seqs + pos, getStartBuffer(seq), len);
#ifdef CHECK_ALL
    CHECK_SEQ(getStartBuffer(seq), len);
    CHECK_SEQ(_my_seq_set.seqs + pos, len);
#endif
    (*num)++;
    DBG("Added canonical_fasta: %lld\n", (lld) * num);
}

static int64_t dump_seqs(char *output_dir, const char *mode)
{
    char _fname[MAX_FILE_PATH+10], _input_fname[MAX_FILE_PATH*3+10];
    int64_t len = 0;

    snprintf(_input_fname, MAX_FILE_PATH*2+10, "%s%s" GZIP_EXT, _fa_fname, _fa_fname_suffix);
    restoreCheckpoint(_input_fname); // ensure fasta gets restored

    snprintf(_input_fname, MAX_FILE_PATH*3+10, "%s/%s%s" GZIP_EXT, output_dir, _fa_fname, _fa_fname_suffix);
    char *input_fname = get_rank_path(_input_fname, MYTHREAD);
    size_t myFileSize = get_file_size_if_exists(input_fname);
    Buffer output = initBuffer(myFileSize * 6 + 1 + WRITE_BLOCK_SIZE);
    DBG("dump_seqs(%s, %s): input_fname=%s myFileSize=%lld\n", output_dir, mode, input_fname, (lld)myFileSize);
    if (myFileSize > 0) {
        gzFile f = gzopen(input_fname, "r");
        kseq_t *ks = kseq_init(f);
        while (kseq_read(ks) >= 0) {
            if (ks->seq.l < _min_length || (_max_length > 0 && ks->seq.l >= _max_length)) {
                continue;
            }
            if (ks->comment.l) {
                printfBuffer(output, ">%s %s\n%s\n", ks->name.s, ks->comment.s, ks->seq.s);
            } else {
                printfBuffer(output, ">%s\n%s\n", ks->name.s, ks->seq.s);
            }
        }
        kseq_destroy(ks);
        gzclose(f);
    }
    len = getLengthBuffer(output);
    sprintf(_fname, "results/%s", _output_fname);
    LOGF("Writing unsorted sequences from %s (%lld bytes) to %s\n", input_fname, (lld)len, _fname);
    if (MYTHREAD == 0) {
        check_dir("results");
    }
    upc_barrier;
    allWriteFile(_fname, mode, output, 0);
    freeBuffer(output);
    return len;
}

static void count_my_seqs(char *output_dir, int cores_per_node, int64_t *num, long *size)
{
    char fname[MAX_FILE_PATH*3+10];
    upc_tick_t s = upc_ticks_now();
    Buffer buf = initBuffer(FASTA_LINE_WIDTH * 2);

    (*num) = 0;
    (*size) = 0;
    for (int64_t i = MYTHREAD; i < MYTHREAD + cores_per_node; i++) {
        snprintf(fname, MAX_FILE_PATH*3+10, "%s/%s%s" GZIP_EXT, output_dir, _fa_fname, _fa_fname_suffix);
        GZIP_FILE f = GZIP_OPEN_RANK_PATH(fname, "r", i);
        char *res = NULL;
        do {
            resetBuffer(buf);
            res = gzgetsBuffer(buf, FASTA_LINE_WIDTH * 2, f);
            if (res == NULL) {
                break;
            }
            if (res[0] == '>') {
                (*num)++;
            } else {
                chompBuffer(buf);
                (*size) += getLengthBuffer(buf);
            }
        } while (res != NULL);
        GZIP_CLOSE(f);
    }
    freeBuffer(buf);
    tprintf_flush("Counting Took %.3f s, found %lld seqs, %lld bytes\n", ELAPSED_TIME(s), (lld) * num, (lld) * size);
}


static void read_seq_set(char *output_dir, int cores_per_node)
{
    int64_t *ids = malloc_chk(_my_seq_set.num * sizeof(*ids));

    for (int64_t i = 0; i < _my_seq_set.num; i++) {
        ids[i] = -1;
    }
    char fname[MAX_FILE_PATH*3+10];
    Buffer seq = initBuffer(FASTA_LINE_WIDTH * 2);
    int word_len = 0;
    int64_t num = 0;
    long pos = 0;
    DBG("read_seq_set(%s, %d): my_seq_set.num=%lld\n", output_dir, cores_per_node, (lld)_my_seq_set.num);
    for (int64_t i = MYTHREAD; i < MYTHREAD + cores_per_node; i++) {
        snprintf(fname, MAX_FILE_PATH*3+10, "%s/%s%s" GZIP_EXT, output_dir, _fa_fname, _fa_fname_suffix);
        GZIP_FILE f = GZIP_OPEN_RANK_PATH(fname, "r", i);
        tprintf_flush("Thread %d: reading %s\n", MYTHREAD, fname);
        char *res = NULL;
        do {
            int prev_len = getLengthBuffer(seq);
            res = gzgetsBuffer(seq, FASTA_LINE_WIDTH * 2, f);
            if (res == NULL) {
                break;
            }
            assert(res == getStartBuffer(seq) + prev_len);
            DBG2("Read line: %.*s...\n", (int)(getLengthBuffer(seq) > 80 ? 80 : getLengthBuffer(seq)), res);
            if (num >= _my_seq_set.num) {
                DIE("anum %lld >= %lld\n", (lld)num, (lld)_my_seq_set.num);
            }
            if (res[0] == '>') {
                if (!word_len) {
                    word_len = (res[1] == 'C' ? strlen(">Contig_") : strlen(">Scaffold"));
                }
                if (res[1] == 'C') {
                    assert(res[2] == 'o' && res[3] == 'n' && res[4] == 't' && res[5] == 'i' &&
                           res[6] == 'g' && res[7] == '_');
                } else {
                    assert(res[1] == 'S' && res[2] == 'c' && res[3] == 'a' && res[4] == 'f' &&
                           res[5] == 'f' && res[6] == 'o' && res[7] == 'l' && res[8] == 'd');
                }
                if (ids[num] != -1) {
                    // header line is after previous sequence
                    add_canonical_fasta(&num, seq, prev_len, pos);
                    pos += prev_len;
                    if (num > _my_seq_set.num) {
                        DIE("anum %lld = %lld\n", (lld)num, (lld)_my_seq_set.num);
                    }
                }
                if (res) {
                    ids[num] = atoi(res + word_len);
                    resetBuffer(seq); // drop the header line
                }
            } else {
                if (ids[num] == -1) {
                    DIE("corrupted assembly file, %s, got sequence before id\n", fname);
                }
                chompBuffer(seq);
            }
        } while (res != NULL);
        DBG2("num=%lld _my_seq_set.num=%lld\n", (lld)num, (lld)_my_seq_set.num);
        if (num == _my_seq_set.num) {
            break;
        }
        if (ids[num] != -1) {
            int len = getLengthBuffer(seq);
            add_canonical_fasta(&num, seq, len, pos);
            if (num > _my_seq_set.num) {
                DIE("anum %lld > %lld\n", (lld)num, (lld)_my_seq_set.num);
            }
            pos += len;
        }
        GZIP_CLOSE(f);
        tprintf_flush("Thread %d: Done reading: %s\n", MYTHREAD, fname);
        res = NULL;
    }
    if (pos != _my_seq_set.size) {
        DIE("wrong final size %lld != %lld\n", (lld)pos, (lld)_my_seq_set.size);
    }
    freeBuffer(seq);
    free_chk(ids);
    tprintf_flush("Thread %d: read_seq_set finished %lld\n", MYTHREAD, (lld)num);
}

int canonical_assembly_main(int argc, char **argv)
{
    // only start counting once all threads are up
    DBG("Starting canonical_assembly_main");
    upc_barrier;
    _t_start = upc_ticks_now();

    strcpy(_fa_fname, "assembly");
    strcpy(_fa_fname_suffix, ".fa");
    strcpy(_output_fname, "final_assembly.fa");
    _dump_only = 0;
    _min_length = 0;
    _max_length = 0;
    _num_buffer_threads = 4;

    char *output_dir = NULL;
    int cores_per_node = (_sv == NULL ? 1 : MYSV.cores_per_node);
    option_t *thisOpt;
    option_t *optList = GetOptList(argc, argv, "o:O:n:f:F:dl:M:");
    print_args(optList, __func__);
    while (optList) {
        thisOpt = optList;
        optList = optList->next;
        switch (thisOpt->option) {
        case 'o':
            output_dir = thisOpt->argument;
            break;
        case 'O':
            sprintf(_output_fname, "%s", thisOpt->argument);
            break;
        case 'n':
            cores_per_node = atoi(thisOpt->argument);
            SET_CORES_PER_NODE(cores_per_node);
            break;
        case 'f':
            strcpy(_fa_fname, thisOpt->argument);
            break;
        case 'F':
            strcpy(_fa_fname_suffix, thisOpt->argument);
            break;
        case 'd':
            _dump_only = 1;
            serial_printf("Not sorting output into canonical form\n");
            break;
        case 'l':
            _min_length = atoi(thisOpt->argument);
            serial_printf("Output only contigs >= %d\n", _min_length);
            break;
        case 'L':
            _max_length = atoi(thisOpt->argument);
            serial_printf("Output only contigs < %d\n", _max_length);
            break;
        case 'M':
            _num_buffer_threads = atoi(thisOpt->argument);
            break;
        default:
            serial_printf("Invalid option: %c\n", thisOpt->option);
            upc_barrier;
            upc_global_exit(1);
        }
    }
    if (!output_dir || !cores_per_node) {
        serial_printf("Usage: %s -o outputdir -O output_name -n cores_per_node -f fa_file_prefix "
                      "-F fa_file_suffix -l min_length -L max_length -M num_buffer_threads\n", argv[0]);
        upc_barrier;
        upc_global_exit(1);
    }
    int64_t nodes = ((int64_t)THREADS) / cores_per_node;

    set_output_dir(output_dir);
    if (_dump_only == 1) {
        dump_seqs(output_dir, "w");
        upc_barrier;
        return 0;
    }

    char ckpt[MAX_FILE_PATH*2+10];
    snprintf(ckpt, MAX_FILE_PATH*2+10, "%s%s" GZIP_EXT, _fa_fname, _fa_fname_suffix);
    restoreCheckpoint(ckpt); // force restore from global if necessary

    serial_printf("Reading from %s/%s{0..%d}%s\n", output_dir, _fa_fname, THREADS - 1, _fa_fname_suffix);
    set_log_prefix(_output_fname);
    shared seq_set_t *seq_sets = NULL;
    UPC_ALL_ALLOC_CHK(seq_sets, nodes, sizeof(seq_set_t));
    upc_tick_t t_count = upc_ticks_now();
    // now each node counts up the size of its seq set and sets the variables in main array
    if (!(MYTHREAD % cores_per_node)) {
        int64_t node_i = ((int64_t)MYTHREAD) / cores_per_node;
        tprintf_flush("Counting for node %lld\n", (lld)node_i);
        fflush(stdout);
        int64_t num = 0;
        long size = 0;
        count_my_seqs(output_dir, cores_per_node, &num, &size);
        CHECK_BOUNDS(node_i, nodes);
        seq_sets[node_i].num = num;
        seq_sets[node_i].size = size;
        seq_sets[node_i].put_flag = 0;
        DBG("Done counting my sequences num=%lld, size=%lld\n", (lld)num, (lld)size);
    }
    upc_barrier;

    if (_num_buffer_threads > cores_per_node) {
        _num_buffer_threads = cores_per_node;
    }
    if (_num_buffer_threads > nodes) {
        _num_buffer_threads = nodes;
    }

    // checks and totals
    int64_t tot_seq_size = 0;
    int64_t tot_num_seqs = 0;

    for (int64_t i = 0; i < nodes; i++) {
        tot_seq_size += seq_sets[i].size;
        tot_num_seqs += seq_sets[i].num;
    }
    serial_printf("Done counting in %.3f s at %.3f: found %lld sequences, %lld bytes (%.3f GB)\n",
                  ELAPSED_TIME(t_count), ELAPSED_TIME(_t_start), (lld)tot_num_seqs, (lld)tot_seq_size,
                  (double)tot_seq_size / 1024 / 1024 / 1024);

    upc_tick_t t_alloc = upc_ticks_now();
    // now get the shared heap so we can estimate memory requirements
    int shared_heap_mb = get_shared_heap_mb();
    if (!shared_heap_mb) {
        serial_printf("Cannot determine UPC shared heap size: cannot check memory limits\n");
    } else {
        serial_printf("UPC shared heap is %lld MB per thread\n", (lld)shared_heap_mb);
        int64_t shared_heap_bytes = shared_heap_mb * ONE_MB;
        // need double the heap space per thread
        double mem_portion = 0.75;
        long bytes_avail = _num_buffer_threads * shared_heap_bytes * mem_portion;
        if (tot_seq_size > bytes_avail) {
            int prev_num_buffer_threads = _num_buffer_threads;
            _num_buffer_threads = INT_CEIL(tot_seq_size / mem_portion, shared_heap_bytes);
            if (_num_buffer_threads == 0) {
                _num_buffer_threads = 1;
            }
            if (_num_buffer_threads > nodes) {
                SDIE("Cannot allocate enough buffer threads: "
                     "will exceed the number of nodes %lld < %lld\n", (lld)nodes, (lld)_num_buffer_threads);
            }
            if (_num_buffer_threads > cores_per_node) {
                SDIE("Cannot allocate enough buffer threads: "
                     "will exceed the number of cores %lld < %lld\n", (lld)cores_per_node, (lld)_num_buffer_threads);
            }
            serial_printf("Not enough memory per thread for %lld seq_size > %lld bytes_avail: adjusted number of buffer threads from"
                          " %lld to %lld\n", (lld)tot_seq_size, (lld)bytes_avail, (lld)prev_num_buffer_threads, (lld)_num_buffer_threads);
        }
    }

    serial_printf("Using %lld threads for %lld nodes\n", (lld)_num_buffer_threads, (lld)nodes);
    upc_barrier;

    serial_printf("Done with shared heap calcs\n");
    upc_barrier;

    // we distribute the destination array across node 0, otherwise we could run out of memory
    // at least this way all the network traffic is done with puts, only gets are needed for
    // local memory
    if (MYTHREAD < _num_buffer_threads) {
        for (int64_t i = 0; i < nodes; i++) {
            if (MYTHREAD == i % _num_buffer_threads) {
                if (seq_sets[i].num > 0) {
                    UPC_ALLOC_CHK0(seq_sets[i].indexes, seq_sets[i].num * sizeof(seq_index_t));
                    UPC_ALLOC_CHK0(seq_sets[i].seqs, seq_sets[i].size);
                } else {
                    seq_sets[i].indexes = NULL;
                    seq_sets[i].seqs = NULL;
                }
            }
        }
    }
    //DBG("Done with allocation of memory heaps\n");
    serial_printf("Done with allocation of memory heaps\n");
    upc_barrier;
    serial_printf("Allocated space on %lld threads for %lld nodes on node 0 in %.3f s\n",
                  (lld)_num_buffer_threads, (lld)nodes, ELAPSED_TIME(t_alloc));
    if (!MYTHREAD) {
        for (int64_t i = 0; i < nodes; i++) {
            tprintf_flush("Node %lld is allocated to thread %lld\n",
                          (lld)i, (lld)upc_threadof(seq_sets[i].seqs));
        }
        fflush(stdout);
    }
    upc_barrier;
    int32_t myPutFlag = -1;
    if (!(MYTHREAD % cores_per_node)) {
        // one thread per node does this
        upc_tick_t t_read = upc_ticks_now();
        int64_t node_i = ((int64_t)MYTHREAD) / cores_per_node;
        // set up local space for this threads sequences
        shared seq_set_t *seq_set = &seq_sets[node_i];
        _my_seq_set.num = seq_set->num;
        _my_seq_set.size = seq_set->size;

        LOGF("Running per-node code: num=%lld size=%lld\n", (lld)_my_seq_set.num, (lld)_my_seq_set.size);
        if (_my_seq_set.num > 0) {
            // allocate local buffer and local indexes for all sequences
            _my_seq_set.indexes = malloc_chk(_my_seq_set.num * sizeof(seq_index_t));
            _my_seq_set.seqs = malloc_chk(_my_seq_set.size);
            // read sequences into local space
            read_seq_set(output_dir, cores_per_node);
            tprintf_flush("Reading took %.3f s\n", ELAPSED_TIME(t_read));
            serial_printf("Reading took %.3f s at %.3f\n", ELAPSED_TIME(t_read), ELAPSED_TIME(_t_start));
            // sort local sequences
            upc_tick_t t_qsort = upc_ticks_now();
            qsort(_my_seq_set.indexes, _my_seq_set.num, sizeof(seq_index_t), cmp_my_seqs);
            tprintf_flush("qsort took %.3f s\n", ELAPSED_TIME(t_qsort));
            serial_printf("qsort took %.3f s at %.3f\n", ELAPSED_TIME(t_qsort), ELAPSED_TIME(_t_start));
#ifdef CHECK_ALL
            DBG("Check all running\n");
            // checking only
            for (int64_t i = 0; i < _my_seq_set.num; i++) {
                CHECK_SEQ(_my_seq_set.seqs + _my_seq_set.indexes[i].pos, _my_seq_set.indexes[i].len);
            }
            CHECK_SEQ(_my_seq_set.seqs, (int)_my_seq_set.size);
            DBG("All checked out\n");
#endif
            // now put local sequences into shared memory and set atomic flag
            tprintf_flush("For node %lld putting %lld sequences, %lld bytes into global heap\n",
                          (lld)node_i, (lld)_my_seq_set.num, (lld)_my_seq_set.size);
            fflush(stdout);
            upc_tick_t t_put = upc_ticks_now();
            // set the node index before the put
            for (int64_t i = 0; i < _my_seq_set.num; i++) {
                _my_seq_set.indexes[i].node_i = node_i;
            }
            upc_memput(seq_set->seqs, _my_seq_set.seqs, _my_seq_set.size);
            upc_memput(seq_set->indexes, _my_seq_set.indexes, _my_seq_set.num * sizeof(seq_index_t));
            // now free the local memory
            free_chk(_my_seq_set.indexes);
            free_chk(_my_seq_set.seqs);
            tprintf_flush("puts took %.3f s\n", ELAPSED_TIME(t_put));
        }
        UPC_ATOMIC_FADD_I32(&myPutFlag, &(seq_set->put_flag), 1);
    }
    DBG2("Waiting for barrier after seq_set puts: (myPutFlag was %d for node %d).\n", myPutFlag, MYTHREAD / cores_per_node);
    upc_barrier;
    DBG("Done waiting after seq_set puts\n");
    // now thread 0 fetches and merges all sequence sets - note there is no barrier from the previous
    // round so that thread 0 can operate immediately even if all data is not yet received
    if (!MYTHREAD) {
        DBG("Fetching seq_sets.  tot_num_seqs=%lld cores_per_node=%d nodes=%lld\n", (lld)tot_num_seqs, cores_per_node, (lld)nodes);
        local_seq_set_t *local_seq_sets = fetch_seq_sets(seq_sets, tot_num_seqs, cores_per_node);
        seq_index_t *sorted_indexes = heap_merge(local_seq_sets, nodes, tot_num_seqs);
        check_dir("results");
        write_sequences(sorted_indexes, local_seq_sets, tot_num_seqs);
        compute_stats(sorted_indexes, tot_num_seqs);
        upc_tick_t t_free = upc_ticks_now();
        DBG("Freeing sorted_indexes\n");
        if (sorted_indexes) free_chk(sorted_indexes);
        for (int64_t i = 0; i < nodes; i++) {
            if (seq_sets[i].indexes != NULL && upc_threadof(seq_sets[i].indexes) == MYTHREAD) {
                UPC_FREE_CHK0(seq_sets[i].indexes);
                UPC_FREE_CHK0(seq_sets[i].seqs);
            } else if (local_seq_sets[i].indexes != NULL) {
                free_chk(local_seq_sets[i].indexes);
                free_chk(local_seq_sets[i].seqs);
            }
        }
        serial_printf("Freed all data in %.3f s\n", ELAPSED_TIME(t_free));
        free_chk(local_seq_sets);
    }
    DBG2("Waiting for barrier after fetch and merge\n");
    upc_barrier;
    DBG2("Done waiting for barrier after fetch and merge\n");
    UPC_ALL_FREE_CHK(seq_sets);
    char buf[MAX_FILE_PATH];
    serial_printf("Overall time for %s is %.2f s\n", get_basename(buf, argv[0]), ELAPSED_TIME(_t_start));
    upc_barrier;
    return 0;
}
