MESSAGE("Building canonical assembly ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

SET(CMAKE_EXE_LINKER_FLAGS)

set(CMAKE_C_IMPLICIT_LINK_LIBRARIES "")
set(CMAKE_C_IMPLICIT_LINK_DIRECTORIES "")
set(CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++")
set(CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "")

SET_UPC_PROPS(*.c *.upc)

add_library(canonical_assembly_objs OBJECT canonical_assembly.c)
set_target_properties(canonical_assembly_objs PROPERTIES LINKER_LANGUAGE "UPC")

IF (HIPMER_FULL_BUILD)
  ADD_EXECUTABLE(upc_canonical_assembly canonical_assembly_exec.upc
					$<TARGET_OBJECTS:canonical_assembly_objs>
                                        $<TARGET_OBJECTS:upc_common>
                                        $<TARGET_OBJECTS:COMMON>
                                        $<TARGET_OBJECTS:HIPMER_VERSION>
                                        $<TARGET_OBJECTS:HASH_FUNCS>
                                        $<TARGET_OBJECTS:Buffer>
                                        $<TARGET_OBJECTS:MemoryChk>
                                        $<TARGET_OBJECTS:OptList>
					$<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
                 )
  SET_TARGET_PROPERTIES(upc_canonical_assembly PROPERTIES LINKER_LANGUAGE "UPC")
  TARGET_LINK_LIBRARIES(upc_canonical_assembly ${ZLIB_LIBRARIES} ${RT_LIBRARIES})
  INSTALL(TARGETS upc_canonical_assembly DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )
  message("upc_canonical_assembly: zlib: ${ZLIB_LIBRARIES} rt: ${RT_LIBRARIES} cll: ${CMAKE_LINK_LIBRARIES} celf: ${CMAKE_EXE_LINKER_FLAGS} impl: ${CMAKE_UPC_IMPLICIT_LINK_LIBRARIES}")
ENDIF()


