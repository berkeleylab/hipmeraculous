#ifndef CANONICAL_ASSEMBLY_H_
#define CANONICAL_ASSEMBLY_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <upc.h>
#include <upc_tick.h>

#include "optlist.h"

#include "upc_common.h"
#include "common.h"
#include "utils.h"
#include "tracing.h"
#include "Buffer.h"
#include "upc_output.h"
#include "kseq.h"


int canonical_assembly_main(int argc, char **argv);

#endif // CANONICAL_ASSEMBLY_H_
