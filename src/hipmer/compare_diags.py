#!/usr/bin/python -u

import sys

def get_diags(fname):
    # note that each key can occur multiple times, so we have to store a list for each key
    diags = {}
    f = open(fname, "r")
    linenum = 0
    for line in f.readlines():
        (key, v) = line.split()
        if not key in diags:
            diags[key] = []
        diags[key].append((v, linenum))
        linenum += 1
    f.close()
    return diags

def get_mismatched_keys(diags1, diags2, fname1, fname2, check_counts):
    mismatches = 0
    msgs = []
    for key in diags1:
        if not key in diags2:
            mismatches += 1
            msgs.append("  [%s] %s%s%s not found in %s" % (fname1, KRED, key, KNORM, fname2))
            continue
        if check_counts:
            l1 = len(diags1[key])
            l2 = len(diags2[key])
            if l1 != l2:
                mismatches += 1
                msgs.append("  [%s] %s%s%s count mismatch in %s: %d != %d" \
                            % (fname1, KRED, key, KNORM, fname2, l1, l2))
                continue
    return mismatches, msgs

def get_diffs(diags1, diags2, fname1, fname2):
    diffcount = 0
    msgs = 1000*[""]
    for key in diags1:
        if not key in diags2:
            continue
        # the idea here is that the keys are ordered, so we go in order 
        count = min(len(diags1[key]), len(diags2[key]))
        for i in range(count):
            v1 = float(diags1[key][i][0])
            v2 = float(diags2[key][i][0])
            l1 = int(diags1[key][i][1])
            l2 = int(diags2[key][i][1])
            if v1 == 0 and v2 == 0:
                continue
            if v1 < min_thres or v2 < min_thres:
                continue
            diff = abs(v1 - v2) / (v1 + v2)
            if diff > thres:
                diffcount += 1
                msgs[l1] = "  Lines (%d %d), %s%s%s: %s != %s" \
                           % (l1, l2, KLRED, key, KNORM, diags1[key][i][0], diags2[key][i][0])
    return diffcount, msgs


if len(sys.argv) < 3:
    print "Usage:"
    print "%s:" % sys.argv[0], "fname1 fname2 <threshold>"
    sys.exit(1)
    
fname1 = sys.argv[1];
fname2 = sys.argv[2];
thres = 0.1;
if len(sys.argv) >= 4:
    thres = float(sys.argv[3])
min_thres = 15
if len(sys.argv) == 5:
    min_thres = int(sys.argv[4])

diags1 = get_diags(fname1)
diags2 = get_diags(fname2)

KRED = "\x1B[31m"
KLRED = "\x1B[91m"
KLGREEN = "\x1B[92m"
KNORM = "\x1B[0m"

print "Comparing", fname1, "with", fname2, "and looking for normalized differences >", thres,
if min_thres > 0:
    print "and ignoring values <", min_thres
else:
    print ""

(mismatches1, msgs1) = get_mismatched_keys(diags1, diags2, fname1, fname2, True)
(mismatches2, msgs2) = get_mismatched_keys(diags2, diags1, fname2, fname1, False)

mismatches = mismatches1 + mismatches2
if mismatches:
    print "%sFound" % KLRED, mismatches, "mismatched keys:", KNORM
    for i in msgs1:
        print i
    for i in msgs2:
        print i

(diffcount, msgs) = get_diffs(diags1, diags2, fname1, fname2)
if diffcount:
    print "%sFound" % KLRED, diffcount, "differences (%s to %s):" % (fname1, fname2), KNORM
    for i in msgs:
        if i != "":
            print i
else:
    print "%sNo mismatches found%s" % (KLGREEN, KNORM)
    
#sys.exit(diffcount)
