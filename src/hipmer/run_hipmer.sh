#!/bin/bash

# Resolve the base path and bootstrap the environment
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

echo "Discovered base path of $DIR"

# place this HipMer install path at the front of PATH
export PATH=${DIR}:${PATH/${DIR}/}

hipmer_install=${DIR%/*}
if [ -z "${HIPMER_ENV}" ] && [ -f "${hipmer_install}/env.sh" ]
then
    echo "Sourcing HipMer Environment: ${hipmer_install}/env.sh"
    source ${hipmer_install}/env.sh
fi

# now parse any ENV=val command line arguments
job_env_args=()
for arg in "$@"
do
  if [ "${arg/=}" != "${arg}" ]
  then
    job_env_args+=("export ${arg};")
    setenvlog="$setenvlog
Setenv $arg"
    eval "export ${arg%=*}='${arg##*=}'"
    shift
  else
    break
  fi
done

CONFIG=$1

get_config()
{
    TAB=$(printf "\t")
    field="$1"
    default="$2"
    vals=($(grep "^${field}\(${TAB}\| \)" ${CONFIG} || true))
    val="${vals[1]}"
    [ -n "${val}" ] || [ -z "${default}" ] || val="$default"
    echo "${val}"
}

LIBS=()
set_LIBS()
{
    LIBS=($(grep '^lib_seq' ${CONFIG} | awk '{print $3}'))
}

LIBSEQ=()
set_LIBSEQ()
{
    TAB=$(printf "\t")
    lib="$1"
    LIBSEQ=($(grep "^lib_seq[ ${TAB}].*[ ${TAB}]${lib}[ ${TAB}]" ${CONFIG}))
}

if [ "${CONFIG}" == "help" ]; then
    echo "USAGE: $0 meraculous.config"
    echo "Environment variables:"
    echo "    THREADS=X (number of UPC threads to use)"
    echo "    UPC_SHARED_HEAP_SIZE=X (size of UPC shared heap)"
    echo "    CACHED_IO=1 (cache intermediate files in memory) implies SAVE_INTERMEDIATES=1"
    echo "    SAVE_INTERMEDIATES=0 save intermediate (checkpoint) files after succesful run or stages"
    echo "    CORES_PER_NODE=X (use X cores per node)"
    echo "    KMER_CACHE_MB=X (set size of kmer cache in MB)"
    echo "    SW_CACHE_MB=X (set size of software cache in MB)"
    echo "    ILLUMINA_VERSION=X (set Illumina version for FASTQ files)"
    echo "    DRYRUN=1 (do a dryrun - show stages but don't execute)"
    echo "    DEBUG=1 (turn on debugging)"
    echo "    UPC_NODES='node1,node1,node2,node2,...noden,noden' (if using the udp conduit)"
    echo "    HYPERTHREADS=${HYPERTHREADS} - if >1 then there are more than 1 thread per core"
    echo "    AUTORESTART=1 (automatically restart if a stage fails)"
    echo "    RESTART_STAGE=${RESTART_STAGE} the stage to restart if possible"
    echo "    NORESTART= if set to 1, then a job will not attempt to restart if it is submitted again in the same directory"
    echo "    PE_PATH= The path to store the temporary but very large PossibleErrors kmer files default $TMPDIR ( /dev/shm is fastest )"
    echo "    UFX_HLL=1 run HLL and HH in ufx step"
    echo "    CANONICALIZE=0 if set, then output fasta will be canonicalized"
    echo "    MICROBENCHMARK=0 if set, then microbenchmarks will be executed before and after HipMer"
    echo "    MONITOR_NETWORK=0 the number of threads per node to monitor the network"
    echo "    CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB} require nodes to have this much space available in >2MB contiguous chunks"
    echo "    MIN_NODES=${MIN_NODES} if on SLURM, the minimum number of nodes to use that pass CHECK_FREE_HUGEPAGES_MB threshold"
    echo "    MAX_NODES=${MAX_NODES} if on SLURM, reduce the job to this number of nodes, keeping the ones with the most free hugepages"
    echo "    TEST_CHECKPOINTING=${TEST_CHECKPOINTING} die after every stage and purge per_rank files"
    echo "    BENCHMARK=${BENCHMARK} optimize output for benchmarking"
    exit 0;
fi

if [ ! -f "${CONFIG}" ]; then
    echo "USAGE: $0 meraculous.config"
    echo "Please specify a config file. '${CONFIG}' is invalid!" 
    echo "For help, call with:"
    echo "$0 help"
    exit 1
fi

short_config=${CONFIG##*/}
short_config=${short_config%.*}
export HIPMER_BOOTSTRAP_FUNC=HipMer-${short_config}
if [ -z "${HIPMER_BOOTSTRAPPED}" ]
then
    source ${DIR}/bootstrap_hipmer_env.sh
fi

set -e
set -o pipefail
module list 2>/dev/null || true

UPC_POLITE_SYNC=${UPC_POLITE_SYNC:=}
if [ ${THREADS} -lt ${CORES_PER_NODE} ]
then
  # support 1 thread or less than the cores in one node on a single node
  echo "Overriding CORES_PER_NODE from ${CORES_PER_NODE} to ${THREADS}"
  export CORES_PER_NODE=${THREADS}
elif [ $((THREADS % CORES_PER_NODE)) -ne 0 ]
then
  echo "Overriding THREADS from ${THREADS} to be a mulitple of ${CORES_PER_NODE}"
  export THREADS=$((THREADS - (THREADS % CORES_PER_NODE)))
fi

export CACHED_IO=${CACHED_IO:=$((THREADS!=CORES_PER_NODE))} # use CACHED_IO if multi-node

if [ ${HYPERTHREADS} -gt 1 ]
then
  export UPC_POLITE_SYNC=1
fi


if [ "${CACHED_IO}" == "1" ] && [ ! -w "/dev/shm/." ]
then
  echo "+ Disabling CACHED_IO as /dev/shm is not writable!"
  export CACHED_IO=0
fi

# set the shared heap size if not already set
if [ -z ${UPC_SHARED_HEAP_SIZE} ]; then
    PHYS_MEM_MB=${PHYS_MEM_MB:=$(awk '/MemTotal:/ { t=$2 ; print int(t / 1024)}' /proc/meminfo 2>/dev/null || sysctl -a | awk '/hw.memsize:/ {t=$2; print int(t/1024/1024)}' || echo 2000)}
    if [ "$CORES_PER_NODE" ]; then
        CPN=$CORES_PER_NODE
    else
        CPN=$(getcores)
    fi
    PERC=30
    [ "$CACHED_IO" == "1" ] && PERC=15
    export UPC_SHARED_HEAP_SIZE=$((PERC*PHYS_MEM_MB/CPN/100))
    echo "+ UPC_SHARED_HEAP_SIZE not set, using ${PERC}% of maximum memory: ${UPC_SHARED_HEAP_SIZE} MB per thread"
fi

if [ -n "${MIN_UPC_SHARED_HEAP_SIZE}" ] && [ ${MIN_UPC_SHARED_HEAP_SIZE} -lt ${UPC_SHARED_HEAP_SIZE} ]
then
  export UPC_SHARED_HEAP_SIZE=${MIN_UPC_SHARED_HEAP_SIZE}
fi

if [ -n "${DEBUG}" ]; then
    ulimit -c unlimited || true
    DEBUG_UPC="-backtrace -abort -verbose"
fi

if [ "$(get_config is_metagenome 0)" == "1" ]; then
    KMER_LENGTH=$(get_config mer_size_max)
else
    KMER_LENGTH=${KMER_LENGTH:=$(get_config mer_size 0)}
fi

if [ "$HIPMER_NO_CGRAPH" != "1" ] && ( [ "$(get_config cgraph_scaffolding 0)" == "1" ] || [ "$USE_CGRAPH" == "1" ] )
then
    USE_CGRAPH=1
else
    USE_CGRAPH=0
fi

KMER_SIZES=$(get_config mer_sizes)
if [ -z "$KMER_SIZES" ]; then
    echo "ERROR: config file is missing 'mer_sizes'. Are you sure that '$CONFIG' is a config file?"
    EXIT_VAL=1
    exit 1
fi

if [ -n "${KMER_SIZES}" ]; then
  IFS=',' read -ra ADDR <<< "$KMER_SIZES"
  for i in "${ADDR[@]}"; do
    if [ -z "${KMER_LENGTH}" ] || [ ${KMER_LENGTH} -lt $i ]; then
        KMER_LENGTH=$i
    fi
  done
fi

if [ -z "${KMER_LENGTH}" ]; then KMER_LENGTH=$(get_config mer_size 51) ; fi

SCAFF_MER_SIZE=$(get_config scaff_mer_size ${KMER_LENGTH})

if [ ${SCAFF_MER_SIZE} -gt ${KMER_LENGTH} ]; then
    MAX_KMER_SIZE=${MAX_KMER_SIZE:=$(( ( (SCAFF_MER_SIZE + 31)/ 32 ) * 32))}
else
    MAX_KMER_SIZE=${MAX_KMER_SIZE:=$(( ( (KMER_LENGTH + 31)/ 32 ) * 32))}
fi

echo "+ max kmer size is $MAX_KMER_SIZE"


SCAFF_MER_SIZE=$KMER_LENGTH

if [ -n "${VALGRIND}" ] && [ "${VALGRIND}" == "1" ]; then
    VALGRIND="valgrind --tool=memcheck --error-limit=no --leak-check=full"
    tmp=$(which upcrun)
    supp=${tmp%/*}/../dbg/lib/valgrind/gasnet.supp
    if [ -f ${supp} ]; then
        VALGRIND="${VALGRIND} --suppressions=${supp}"
    fi
fi

CANONICALIZE=${CANONICALIZE:=0}
set -x

if [ "$BENCHMARK" == "1" ]
then
  SAVE_INTERMEDIATES=0
fi
if [ "${CACHED_IO}" == "1" ] && [ -z "${SAVE_INTERMEDIATES}" ]
then
  SAVE_INTERMEDIATES=1
fi
[ "$CACHED_IO" == "1" ] && CACHED_IO_OPT="-B "
[ "$CORES_PER_NODE" ] && CORES_PER_NODE_OPT="-N ${CORES_PER_NODE}"
[ -n "$KMER_CACHE_MB" ] && KMER_CACHE_OPT="-Y ${KMER_CACHE_MB}"
[ -n "$SW_CACHE_MB" ] && SW_CACHE_OPT="-X ${SW_CACHE_MB}"
[ "$ILLUMINA_VERSION" ] && ILLM_VER_OPT="-I ${ILLUMINA_VERSION}"
[ "$PE_PATH" ] && [ -d "$PE_PATH" ] && PE_PATH_OPT="-P ${PE_PATH}"
[ -z "${UFX_HLL}" ] && UFX_HLL=1
[ -z "${AUTORESTART}" ] && AUTORESTART=1
[ "$CANONICALIZE" == "1" ] && CANONICAL_OPT="-C"
[ "$SAVE_INTERMEDIATES" == "1" ] && SAVE_INTERMEDIATES_OPT="-S"
[ "$SAVE_INTERMEDIATES" == "0" ] && SAVE_INTERMEDIATES_OPT="-x"
[ "$USE_CGRAPH" == "1" ] && USE_CGRAPH_OPT="-z ${USE_CGRAPH}"
[ "$TEST_CHECKPOINTING" == "1" ] && TEST_CHECKPOINTING_OPT="-T"
UFX_HLL_OPT=
[ ${UFX_HLL} -eq 1 ] && UFX_HLL_OPT="-H"
set +x

if [ -n "${HIPMER_UPCRUN_CONF}" ] && [ -f "${HIPMER_INSTALL}/${HIPMER_UPCRUN_CONF}" ]
then
   UPCCONF_OPT="-conf=${HIPMER_INSTALL}/${HIPMER_UPCRUN_CONF}"
fi

UPCRUN="${UPCRUN:=upcrun -q} ${UPCCONF_OPT} ${POLITE_SYNC}"

if [ -z "$(which main-${MAX_KMER_SIZE})" ]; then
    echo "ERROR: Cannot find main-${MAX_KMER_SIZE}"
    EXIT_VAL=1
    exit 1
fi

MPIRUN=${MPIRUN:=mpirun}

if [ -n "$UPC_NODES" ]
then
  export GASNET_SPAWNFN=C
  export GASNET_CSPAWN_CMD="${MPIRUN} -np %N %C"
fi

if [ -z "${RUNDIR}" ] || [ ! -d "${RUNDIR}" ] || [ ! -w "${RUNDIR}" ] || [ ! -d "${RUNDIR}/per_rank" ]
then
  # if RUNDIR is not defined and CONFIG is in the current directory with no path, this is the rundir
  if [ -z "${RUNDIR}" ] && [ -f "${CONFIG}" ] && [ "${CONFIG}" == "${CONFIG##*/}" -o "${CONFIG}" -ef "${CONFIG##*/}" ]
  then
      export RUNDIR=$(pwd -P)
      echo "+ Since the config is in the current directory, setting RUNDIR=$RUNDIR"
  fi
  setup_RUNDIR ${short_config}
else
  setup_RUNDIR
fi

# fork tee redirecting stdout & stderr to run.out now
echo "+ Appending STDOUT and STDERR to ${RUNDIR}/run.out"
exec > >(tee -i -a ${RUNDIR}/run.out)
exec 2>&1

# TODO investigate burst buffer at scale -- ftruncate is delayed indefiniately sometimes  
# [Th240  WARN  2018-12-04 21:11:13 upc_output.upc:222]: results/contigs-21.fa is not yet 43844091509 after ftruncate! (it is 42286972928)
if [ -n "${HIPMER_BB_PERSISTANT_NAME}" ] && [  "${HIPMER_NO_STAGE}" != "1" ]
then
  pers_name=DW_PERSISTENT_STRIPED_${HIPMER_BB_PERSISTANT_NAME}
  pers_dir=$(eval echo "\$${pers_name}")
  if [ -n "${pers_dir}" ] && [ -d "${pers_dir}" ]
  then
      stagein_RUNDIR $pers_dir
  fi
fi
if [ -z "${STAGE_RUNDIR}" ] && [ -n "$DW_JOB_STRIPED" ] && [ -d "${DW_JOB_STRIPED}" ] && [  "${HIPMER_NO_STAGE}" != "1" ]
then
  stagein_RUNDIR $DW_JOB_STRIPED
fi

# ensure the config file is in the rundir too
copyconfig=${RUNDIR}/${CONFIG##*/}
[ -f ${copyconfig} -a ${CONFIG} -ef ${copyconfig} ] || cp -p ${CONFIG} ${copyconfig}
cd ${RUNDIR}

export RUNDIR=$(pwd -P) # make sure downstream use of RUNDIR works regardless of cwd

echo "+ RUNDIR=${RUNDIR}"

MYEXEC=$(which main-${MAX_KMER_SIZE})
if [ ! -x "${MYEXEC}" ]
then
  echo "ERROR could not find ${MYEXEC} (main-${MAX_KMER_SIZE})"
  EXIT_VAL=1
  exit 1
fi

if [ -n "${SLURM_JOBID}" ] && [ "${USE_SBCAST}" == "1" ]
then
    newexec=/tmp/${MYEXEC##*/}-${SLURM_JOBID}
    export SLURM_BCAST=${SLURM_BCAST:=${newexec}}

#    if sbcast --force --preserve --verbose ${MYEXEC} ${newexec}
#    then
#      MYEXEC=${newexec}
#    else
#      echo "WARNING!! sbcast failed to distribute ${MYEXEC} to all nodes!"
#    fi

fi

if ( [ -n "${CHECK_FREE_HUGEPAGES_MB}" ] &&  [ "${CHECK_FREE_HUGEPAGES_MB}" != "0" ] ) || [ -n "$MIN_NODES" ] || [ -n "$MAX_NODES" ]
then
    # test the nodes for enough hugepage memory and kick out a few if necessary
    if [ -z "$MIN_NODES" ]
    then
        if [ $SLURM_JOB_NUM_NODES -gt ${MIN_NODES_FOR_HUGEPAGE_CHECK:=4} ]
        then
            MIN_NODES=$((3*SLURM_JOB_NUM_NODES/4))
        else
            MIN_NODES=${SLURM_JOB_NUM_NODES}
        fi  
    fi  
    if [ -n "${SLURM_JOB_ID}" ] && [ -f /proc/buddyinfo ]
    then
        if [ ! -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            check_hugepage_and_reduce_slurm_job.sh MIN_NODES=${MIN_NODES} || echo "Failed to check hugepages.  Continuing"
        fi  
        if [ -f $TMPDIR/new_slurm_env.$SLURM_JOB_ID ]
        then
            echo "Adjusting job environment: source $TMPDIR/new_slurm_env.SLURM_JOB_ID"
            set -x
            source $TMPDIR/new_slurm_env.$SLURM_JOB_ID
            set +x
        else
            echo "There is no file to adjust the environment: $TMPDIR/new_slurm_env.$SLURM_JOB_ID"
        fi  
    fi  
fi

runhello()
{
  echo "+ Attempting hello_world_scale on ${THREADS} threads at $(date)"
  set -x
  SBATCH_BCAST=/tmp/hello ${UPCRUN} -v -q -n ${THREADS} $(which hello_world_scale) || true
  set +x
  echo "+ Finished hello_world_scale at $(date)"
  echo "+ sleeping 15 s"
  sleep 15
}

runbenchmark()
{
  d=$1
  mkdir -p $d
  set -x
  cd ${d}
  ${UPCRUN} -n ${THREADS} $(which benchmarkConstr) -i 20000 -b 32768 -p 10000 > constr.log
  ${UPCRUN} -n ${THREADS} $(which benchmarkTrav) -i 20000 -b 32768 -p 10000 > trav.log
  ${UPCRUN} -n ${THREADS} $(which benchmarkGet) -i 20000 -b 32768 -p 10000 > get.log
  cd -
  set +x
  set -e
}

echo "+ Final environment:"
env | grep '\(UPC\|GASNET\|SLURM\|HIPMER\|THREAD\|CORES\)' | sort

if [ "${HELLO}" == "2" ]
then
  runhello
fi

if [ "${MICROBENCHMARKS}" == "1" ]
then
  runbenchmark prerun-microbenchmarks
fi

EXIT_VAL=1
SRUNPIDS=
cleanup()
{
  echo "Cleanup called -- $@"
  if [ -n "${SRUNPIDS}" ]
  then
    echo "+ Sending SIGKILL to ${SRUNPIDS}"
    kill ${SRUNPIDS} 2>/dev/null || true
  fi
  if [ -n "${STAGE_RUNDIR}" ]
  then
    # TODO resize the job to one node
    stageout_RUNDIR
  fi
  trap "" 0 1 2 3 15
  exit ${EXIT_VAL}
}
cleanup_signal()
{
  EXIT_VAL=1
  cleanup 1
  exit ${EXIT_VAL}
}
trap cleanup 0 
trap cleanup_signal 1 2 3 15

NODES=$((THREADS/CORES_PER_NODE))
CMD="${UPCRUN} ${DEBUG_UPC} -n ${THREADS} -c ${CORES_PER_NODE} -N ${NODES} ${VALGRIND} ${MYEXEC} "\
"-f ${CONFIG} ${CORES_PER_NODE_OPT} ${ILLM_VER_OPT} ${SW_CACHE_OPT} ${KMER_CACHE_OPT} "\
"${CACHED_IO_OPT} ${PE_PATH_OPT} ${UFX_HLL_OPT} ${CANONICAL_OPT} ${SAVE_INTERMEDIATES_OPT} ${USE_CGRAPH_OPT} ${TEST_CHECKPOINTING_OPT}"

RESTART_CMD=$CMD

old_stage="none"
if [ -n "${RESTART_STAGE}" ] || ( [ "${NORESTART}" != "1" ] && [ -f .hipmer_command ] && [ -f run.out ] && [ -d intermediates/ ] )
then
    rss=cat
    if [ -n "${RESTART_STAGE}" ]
    then
       rss="grep ${RESTART_STAGE}"
    fi

    stage=$(grep "# Starting stage " run.out| grep -v grep | grep -v loadfq | $rss | tail -n1 |cut -d' ' -f4 || true)
    if [ -z "$stage" ] && [ -n "${RESTART_STAGE}" ]
    then
      echo "Could not find ${RESTART_STAGE} in run.out.... trying anyway"
      stage=${RESTART_STAGE}
    fi

    if [ -n "$stage" ]
    then
        echo -e "\n\n\033[1;31m>>>>> RESTARTING at stage $stage (first try) <<<<<<\033[0;0m\n"
        if [ "$stage" == "cgraph" ]; then
            RESTART_CMD=""
        else
            RESTART_CMD="$CMD -s ${stage}-end"
            if [ "$TEST_CHECKPOINTING" == "1" ]
            then
                echo  -e "\n\n\033[1;31m>>>>> purging per_rank files for test of checkpointing <<<<<<\033[0;0m\n"
                if [ "$CACHED_IO" == "1" ]
                then
                    ls -lart /dev/shm/per_rank/00000000/00000000
                    rm -rf /dev/shm/per_rank/00000000
                else
                    ls -lart per_rank/00000000/00000000
                    rm -rf per_rank/00000000
                fi
            fi
        fi
    else
        echo -e "\n\n\033[1;31m>>>>> Failed to find a valid restart stage (${RESTART_STAGE}) <<<<<<\033[0;0m\n"
    fi
else
    echo "This run is not possible to be restarted RESTART_STAGE=${RESTART_STAGE} NORESTART=${NORESTART} "
fi

echo "+ Executing from $(hostname) as PID $$"
if [ -n "${MONITOR_NETWORK}" ]
then
  export SLURM_OVERCOMMIT=1
fi

# fork tee redirecting stdout & stderr to run.out now
echo "+ Appending STDOUT and STDERR to $(pwd)/run.out"
#exec > (tee -i >(egrep "# "|grep -iv "memory") > $(pwd)/run.out)
#exec > >(tee -i -a $(pwd)/run.out)
#exec 2>&1

echo "#### ${0##*/} Starting at $(date) on $(uname -n) from:"
echo "####   $0 $@"

if [ "${GASNET_BACKTRACE}" == "1" ] || [ "${RUN_STAT}" == "1" ]
then
  cp -p ${MYEXEC} .
fi

EXIT_VAL=0

while [ true ]; do
    SRUNPIDS=

    if [ "$RESTART_CMD" != "" ]; then
        echo "+" $RESTART_CMD

        echo "$RESTART_CMD" > .hipmer_command
        $RESTART_CMD &
        cmdpid=$!
        SRUNPIDS="${cmdpid}"
        echo "+ Executing srun as pid $cmdpid on $(hostname)"
        if [ "${RUN_STAT}" == "1" ] && [ -n "$(which stat-cl)" ]
        then
            timeout=$(which timeout || true)
            [ -z "${timeout}" ] || timeout="${timeout} 180 "
            while ps -p ${cmdpid} 2>/dev/null
            do
                sleep 60
                echo "+ Executing stat-cl to grab stack traces at $(date).  see stat-cl.log"
                $timeout stat-cl -a -c ${cmdpid} >> stat-cl.log 2>&1 || true 
            done
        fi
        err=0
        wait ${cmdpid} || err=$?
        echo "+ executed command pid ${cmdpid} finished with exit status ${err} at $(date)"
    else
        err=0
    fi
    SRUNPIDS=

    sync run.out
    if [ ${err} -ne 0 ]; then
        echo "+ Detected a non-zero exit: ${err}"
    else
        if [ "$USE_CGRAPH" == "1" ] && [ -n "$(tail -10000 run.out | grep -v srun: | tail | grep '# cgraph will now continue')" ]
        then
            # Now execute cgraph
            echo "+ cgraph will start now" 
            reads=""
            aligns=""
            set_LIBS
            for lib in ${LIBS[@]}
            do
                set_LIBSEQ ${lib}
                FASTQ_LIB=`basename ${LIBSEQ[1]}`
                LIB_NAME=${LIBSEQ[2]}
                issplint="${LIBSEQ[14]}"
                if [ "$issplint" == "0" ]
                then
                    echo "Skipping lib $LIB_NAME as it is not for spinting"
                    continue
                fi
                fpp="${LIBSEQ[13]}"
                if [ "$fpp" == "2" ]
                then
                    FASTQ_LIB="${FASTQ_LIB%,*}-merged.fastq"
                elif [ "$fpp" == "1" ]
                then
                    FASTQ_LIB="${FASTQ_LIB}-merged.fastq"
                fi
                [ -z "$reads" ] || reads="${reads},"
                reads="${reads}${FASTQ_LIB}"
                [ -z "$aligns" ] || aligns="${aligns},"
                aligns="${aligns}${LIB_NAME}-merAlignerOutput-cgraph"
     
            done
            CGRAPH=$(which cgraph || true)
            if [ ! -x "$CGRAPH" ]
            then
                echo "cgraph does not seem to be installed.  Was it built?"
                EXIT_VAL=1
                exit ${EXIT_VAL}
            fi
            uutigs=pruned_Bubbletigs_diplotigs-${KMER_LENGTH}
            meta=$(get_config is_metagenome)
            if [ "$meta" != "1" ]
            then
                uutigs=UUtigs_contigs-${KMER_LENGTH}
            fi
            CGRAPH_CMD="upcxx-run -shared-heap ${UPC_SHARED_HEAP_SIZE}M -n ${THREADS} -N ${NODES} ${CGRAPH} "\
"-c ${uutigs} -D merDepth_${uutigs}.txt -a ${aligns} -r ${reads} -o results "\
"-k ${SCAFF_MER_SIZE} -P -A $CACHED_IO_OPT -w 100"
            echo $CGRAPH_CMD
            ret=0
            $CGRAPH_CMD || ret=$?
            if [ $ret -ne 0 ]
            then
                echo -e "\n\n\033[1;31m>>>>> cgraph FAILED ($ret) <<<<<<\033[0;0m\n"
                EXIT_VAL=$reg
                exit $ret
            fi
        else
            [ "$USE_CGRAPH" != "1" ] || echo "+ cgraph continue condition was not detected in run.out, restarting main"
        fi
    fi

    # check results file and log for completion heuristics
    final=results/final_assembly.fa
    if [ "$USE_CGRAPH" == "1" ]
    then
        tail -10000 run.out | grep -v srun: | tail -5 | grep "# Finished cgraph " 2>&1 > completed
    else
        tail -10000 run.out | grep -v srun: | tail -5 | grep "# HipMer has completed successfully. Executed using version " > completed || true
    fi
    [ -s completed ] || rm completed

    if [ ${err} -ne 0 ] && [ -f ${final} ] && [ -f ${RUNDIR}/${final} ] && [ ${final} -nt .hipmer_command ] && [ -s completed ]
    then
        echo "WARNING command returned non-zero (${err}) but the final logs and assembly check out...  Assuming everything is okay"
        err=0
    fi
    if [ ${err} -eq 0 ] && ( [ ! -f ${final} ] && [ ! -f ${RUNDIR}/${final} ] && [ -s completed ] ) && [ "${AUTORESTART}" == "1" ]
    then
        echo "+ Hmm.  returned an exit of 0, but there is no final log entry or assembly at $(pwd)/${final} ... let us pretend that it failed and let AUTORESTART try again ..."
        err=1
    fi 
    [ ${err} -eq 0 ] || EXIT_VAL=${err}

    if [ "$err" -eq 0 ] && [ -s completed ]; then
        # double check: seems we could fail and return error code 0, especially with out of memory
        # we should see this string at the end of latest_run/run.out
        # for some reason, if this is a subcommand assigned to variable, the script just exits. So we
        # fork a command and wait for it to write to a file. Ugh.
        completed=$(cat completed)
        if [ "$completed" ]; then
            echo "+ Looks like a successful run"
            EXIT_VAL=0
            break
        fi
    fi
    # this is the return code when failing on the config - don't restart
    if [ "$err" -eq 100 ]; then
        echo "+ exit of 100 means the config was malformed.  Please fix the config!"
        set +x
        EXIT_VAL=1
        exit 1
    fi
    if [ "$AUTORESTART" == "0" ] && [ -z "$AUTORESTART_UFX" ]; then
        echo "+ AUTORESTART or AUTORESTART_UFX is disabled, quitting now"
        [ ${EXIT_VAL} -ne 0 ] || EXIT_VAL=1
        exit ${EXIT_VAL}
    fi
    old_stage=$stage
    # ensure run.out is up to date!
    sync run.out
    stage=$(grep "# Starting stage " run.out|grep -v grep |tail -n1|cut -d' ' -f4)
    if [ "$stage" == "$old_stage" ]; then
        echo -e "\n\n\033[1;31m>>>>> Cannot restart: stage $stage failed again <<<<<<\033[0;0m\n"
        set -x
        [ ${EXIT_VAL} -ne 0 ] || EXIT_VAL=1
        exit ${EXIT_VAL}
    fi

    if [ "${stage}" != "${stage#parCC}" ] && [ -f .manualOnoRestartCommand ]
    then
        echo
        echo "+ Attempting to restart parCC, but it looks like it failed at the oNo stage."
        echo "+ Attempting to restart just the oNo part: '$(cat .manualOnoRestartCommand)'"
        echo "+ running that command at $(date)"
        echo 
        set -x
        if source .manualOnoRestartCommand
        then
            set +x
            echo "+ Successful manual restart of ono commands for thread 0 at $(date)"
        else 
            set +x
            echo -e "\n\n\033[1;31m>>>>> manual oNo restart failed too <<<<<<\033[0;0m\n"
        fi
    fi

    echo -e "\n\n\033[1;31m>>>>> RESTARTING at stage $stage <<<<<<\033[0;0m\n"
    if [ "$stage" == "cgraph" ]; then
        RESTART_CMD=""
    else
        RESTART_CMD="$CMD -s ${stage}-end"
        if [ "$TEST_CHECKPOINTING" == "1" ]
        then
            echo  -e "\n\n\033[1;31m>>>>> purging per_rank files for test of checkpointing <<<<<<\033[0;0m\n"
            if [ "$CACHED_IO" == "1" ]
            then
                ls -lart /dev/shm/per_rank/00000000/00000000
                rm -rf /dev/shm/per_rank/00000000
            else
                ls -lart per_rank/00000000/00000000
                rm -rf per_rank/00000000
            fi
        fi
    fi

done


if [ "$EXIT_VAL" -eq 0 ]; then
    completed=$(cat completed)
    if [ "$completed" ]; then
        EXIT_VAL=0
        B2TUFX=$HIPMER_INSTALL/bin/b2tufx-$MAX_KMER_SIZE
        if [ -n "$ufx_md5sum" ] && [ -x "$B2TUFX" ] && [ -z "${STAGE_RUNDIR}" ]
        then
           echo "+ Generating a canonical UFX file for checksum"
           for ufx in intermediates/ALL_INPUTS.fofn-*.ufx.bin
           do
               if [ -f "$ufx" ]
               then
                  k1=${ufx##intermediates/ALL_INPUTS.fofn-}
                  k=${k1%.ufx.bin}
                  sorted=${ufx##intermediates/}.txt.full.sorted
                  ${MPIRUN} -n ${THREADS} ${B2TUFX} -k $k $ufx > $sorted.log 2>&1 \
                      && sort $ufx.txt.full > ${sorted}.tmp \
                      && mv ${sorted}.tmp ${sorted} &
                  export SAVE_INTERMEDIATES=1
               fi
           done 
        fi
        if [ -z "${SAVE_INTERMEDIATES}" ]
        then
           # use the save_intermediates parameter from the config file, if it exists
           SAVE_INTERMEDIATES=$(grep '^save_intermediates' ${CONFIG} | awk '{print $2}' || true)
        fi
        if [ -z "${SAVE_INTERMEDIATES}" ] || [ "${SAVE_INTERMEDIATES}" == "0" ]
        then
          echo "+ Removing intermediate files"
          rm -rf intermediates 
        fi
    else
        echo "+ Sorry it looks like this run failed to complete"
        EXIT_VAL=1
    fi
fi

microbenchmarkspid=
if [ "${MICROBENCHMARKS}" == "1" ]
then
  runbenchmark postrun-microbenchmarks &
  microbenchmarkspid=$!
  echo "+ microbenchmarkspid=$microbenchmarks"
fi

 
if [ -n "${STAGE_RUNDIR}" ]
then
    echo "+ Copying staged run to ${STAGE_RUNDIR}"
    # resize the job to one node, unless testing or something...
    if [ "${HIPMER_NO_RESIZE_FOR_STAGE}" != "1" ] && [ "$CORES_PER_NODE" != "$THREADS" ]
    then
      rm -f $TMPDIR/new_slurm_env.${SLURM_JOB_ID}*
      if ${HIPMER_INSTALL}/bin/check_hugepage_and_reduce_slurm_job.sh MIN_NODES=1 MAX_NODES=1
      then
        if [ -f $TMPDIR/new_slurm_env.${SLURM_JOB_ID} ]
        then
          echo "Shrinking job environment to one node for staging back data"
          set +e
          set -x
          source $TMPDIR/new_slurm_env.${SLURM_JOB_ID}
          set +=
          set -e
        fi
      fi 
    fi
    stageout_RUNDIR 
    cd $RUNDIR
fi

echo "+ waiting for backgrounded operations to complete $(date)"
if [ -n "$microbenchmarkspid" ]
then
   echo "+ waiting for microbenchmarks to complete $(date)"
   if ! wait $microbenchmarkspid
   then
       EXIT_VAL=1
       echo -e "\n\033[1;31m>>>>>  WARNING microbenchmarks failed $(date) <<<<<<\033[0;0m\\n"
   fi
fi

wait || EXIT_VAL=$?

trap "" 0

elapsed_mins=$((SECONDS/60))
elapsed_secs=$((SECONDS%60))
echo "+ " $(basename $0) "finished with exit code ${EXIT_VAL} at $(date) in ${elapsed_mins}m ${elapsed_secs}s"
echo "+ Final assembly can be found in $(pwd)/$final"
exit ${EXIT_VAL}
