MESSAGE("Building microbenchmarks ${CMAKE_BUILD_TYPE} UPC code using '${CMAKE_UPC_COMPILER} ${CMAKE_UPC_FLAGS}' to compile UPC code")

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
     set(CMAKE_UPC_FLAGS "${CMAKE_UPC_FLAGS} -g")
else()
     set(CMAKE_UPC_FLAGS "${CMAKE_UPC_FLAGS}")
endif()

# for c++ ufx reading code

find_package(ZLIB 1.2.3 REQUIRED)
find_package(RT QUIET)
# fix linking when objects are from multiple languages
set(CMAKE_C_IMPLICIT_LINK_LIBRARIES "")
set(CMAKE_C_IMPLICIT_LINK_DIRECTORIES "")
set(CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++")
set(CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "")

SET(CMAKE_EXE_LINKER_FLAGS)
SET_SOURCE_FILES_PROPERTIES(*.upc PROPERTIES LANGUAGE "UPC" OBJECT_DEPENDS "${CMAKE_SOURCE_DIR}/src/hipmer/common/upc_common.h" )

FOREACH(target benchmarkTrav benchmarkConstr benchmarkGet)
    ADD_EXECUTABLE(${target} ${target}.upc
        $<TARGET_OBJECTS:upc_common>
        $<TARGET_OBJECTS:COMMON>
        $<TARGET_OBJECTS:HIPMER_VERSION>
        $<TARGET_OBJECTS:OptList>
        $<TARGET_OBJECTS:Buffer>
        $<TARGET_OBJECTS:MemoryChk>
        $<TARGET_OBJECTS:HASH_FUNCS>
        $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
    )
    SET_TARGET_PROPERTIES(${target} PROPERTIES
                                    LINKER_LANGUAGE "UPC"
                                    )
    TARGET_LINK_LIBRARIES(${target} ${ZLIB_LIBRARIES} ${RT_LIBRARIES})
    INSTALL(TARGETS ${target} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

ENDFOREACH()

ADD_EXECUTABLE(monitorNetwork monitorNetwork.upc 
         $<TARGET_OBJECTS:upc_common>
         $<TARGET_OBJECTS:COMMON>
         $<TARGET_OBJECTS:HIPMER_VERSION>
         $<TARGET_OBJECTS:OptList>
         $<TARGET_OBJECTS:CPU_AFFINITY>
         $<TARGET_OBJECTS:Buffer>
         $<TARGET_OBJECTS:MemoryChk>
         $<TARGET_OBJECTS:HASH_FUNCS>
         $<${HIPMER_USE_REFERENCE_UPC_ATOMICS}:$<TARGET_OBJECTS:REF_UPC_ATOMIC>>
    )
SET_TARGET_PROPERTIES(monitorNetwork PROPERTIES
                                     LINKER_LANGUAGE "UPC"
                                     )
TARGET_LINK_LIBRARIES(monitorNetwork ${ZLIB_LIBRARIES} ${RT_LIBRARIES})
INSTALL(TARGETS monitorNetwork DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/ )

