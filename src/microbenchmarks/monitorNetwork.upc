#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#include <upc.h>
#include <upc_collective.h>

#include "optlist.h"

#include "defines.h"
#include "StaticVars.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "upc_output.h"
#include "common_base.h"
#include "common.h"
#include "hash_funcs.h"
#include "Buffer.h"
#include "timers.h"
#include "utils.h"

#include "../hipmer/main/cpu_affinity.h"

#define UNUSED 0
#define USED 1
#define NS 1000000000L

GlobalFinished finished = NULL;

long random_at_most(long max) {
   unsigned long num_bins = (unsigned long) max + 1, num_rand = (unsigned long) RAND_MAX + 1, bin_size = num_rand / num_bins, defect = num_rand % num_bins;
   
   long x;
   do {
      x = random();
   }
   while (num_rand - defect <= (unsigned long)x);
   
   x /= bin_size;
   if (x < 0 || x > max) DIE("What? random_at_most(%lld) returns %lld\n", (lld) max, (lld) x);
   assert(x <= max);
   assert(x >= 0);
   return x;
}

typedef shared[] char* shared_heap_ptr;

shared long _nBytesSmall;
shared long _nBytesLarge;
shared long _iters;
shared long _expansion;
shared long _sleepNS;

int main(int argc, char **argv) {

    int64_t iters, expansion, nBytesSmall, nBytesLarge, sleepNS;
    int alarmSec = 0;
    char terminateFile[MAX_FILE_PATH];
    double start, end;

    strcpy(terminateFile, ".monitorNetwork");
    OPEN_MY_LOG("MonitorNetwork");
    if (!MYTHREAD) {
        /* Use getopt() to read arguments */
        option_t *optList, *thisOpt;
        optList = NULL;
        optList = GetOptList(argc, argv, "i:s:l:T:S:A:");

        _iters = 10;
        _expansion = 10;
        _nBytesSmall = 64;
        _nBytesLarge = ONE_MB;
        _sleepNS = NS;
   
        while (optList != NULL) {
            thisOpt = optList;
            optList = optList->next;
            switch (thisOpt->option) {
                case 'i': _iters = atol(thisOpt->argument); break;
                case 'e': _expansion = atol(thisOpt->argument); break;
                case 's': _nBytesSmall = atol(thisOpt->argument); break;
                case 'l': _nBytesLarge = atol(thisOpt->argument); break;
                case 'T': strncpy(terminateFile, thisOpt->argument, MAX_FILE_PATH-1); break;
                case 'S': _sleepNS = atol(thisOpt->argument); break;
                case 'A': alarmSec = atoi(thisOpt->argument); break;
                default: DIE("Invalid option '%c'\n", thisOpt->option); break;
            }
        }
    }
    UPC_LOGGED_BARRIER;

    // shared options on all threads
    iters = broadcast_long(_iters, 0);
    expansion = broadcast_long(_expansion, 0);
    nBytesSmall = broadcast_long(_nBytesSmall, 0);
    nBytesLarge = broadcast_long(_nBytesLarge, 0);
    sleepNS = broadcast_long(_sleepNS, 0);
    struct timespec sleeper;
    sleeper.tv_sec = sleepNS / NS;
    sleeper.tv_nsec = sleepNS % NS;
    SLOG("Starting using iters=%lld, expansion=%lld, nBytesSmall=%lld, nBytesLarge=%lld, sleepNS=%lld, alarmSec=%d\n", (lld) _iters, (lld) _expansion, (lld) _nBytesSmall, (lld) _nBytesLarge, (lld) _sleepNS, alarmSec);

    assert(finished == NULL);
    finished = initGlobalFinished(1);
    if (MYTHREAD > 0)
        incrementGlobalFinished(finished);

    // make the terminate file
    if (!MYTHREAD) {
        if (alarmSec>0) {
            alarm(alarmSec+65);
        }
        FILE *terminate = fopen_chk(terminateFile, "w");
        fprintf(terminate, "%d", getpid());
        fclose_track(terminate);
        SLOG("Initialized.  To stop execute: rm %s\n", terminateFile);
    }
    double startTime = now();

    char networkSummaryFile[MAX_FILE_PATH];
    strcpy(networkSummaryFile,"networkSummaryFile.dat");
   
    char nodename[64];
    gethostname(nodename, 64);
    char affinity[512];
    get_cpu_affinity(affinity);
    SLOG("Running on %s with affinity %s\n", nodename, affinity);
    
    Buffer data = initBuffer(8192);
    if (!MYTHREAD) 
        printfBuffer(data, "Node\tTHREAD\tcs_latency\tcs_bw\tget_sm_latency\tget_sm_bw\tput_sm_latency\tput_sm_bw\tget_lg_latency\tget_lg_bw\tput_lg_latency\tput_lg_bw\tAffinity\n");

    // init shared buffers
    shared [1] int64_t *compareSwaps = NULL;
    UPC_ALL_ALLOC_CHK(compareSwaps, THREADS * expansion, sizeof(int64_t));

    shared [1] shared_heap_ptr *sharedHeaps = NULL;
    UPC_ALL_ALLOC_CHK(sharedHeaps, THREADS * expansion, sizeof(shared_heap_ptr));
    for (long i = 0; i < expansion; i++) {
        UPC_ALLOC_CHK(sharedHeaps[MYTHREAD + THREADS*i], nBytesLarge);
    }
    char *myBuf = calloc_chk(nBytesLarge, 1);

    UPC_LOGGED_BARRIER;
    long round = 0;
    double totalCSwapTime = 0.0, totalGetSmallTime = 0.0, totalGetLargeTime = 0.0, totalPutSmallTime = 0.0, totalPutLargeTime = 0.0;
    while(!isGlobalFinished(finished)) {

        nanosleep(&sleeper, NULL);
        round++;

        // reset my values
        for(long i = 0 ; i < expansion; i++) {
            assert(upc_threadof(&compareSwaps[MYTHREAD + THREADS*i]) == MYTHREAD);
            compareSwaps[MYTHREAD + i*THREADS] = UNUSED;
            assert(upc_threadof(sharedHeaps[MYTHREAD + THREADS*i]) == MYTHREAD);
            memset((char*) sharedHeaps[MYTHREAD + THREADS*i], (int) MYTHREAD, nBytesLarge);
        }
        memset(myBuf, MYTHREAD, nBytesLarge);

        UPC_LOGGED_BARRIER;

        // test cswap
        long got = 0;
        start = now();
        for (long i = 0; i < iters; i++) {
            long pos = random_at_most(THREADS * expansion - 1);
            int64_t oldVal;
            UPC_ATOMIC_CSWAP_I64(&oldVal, &compareSwaps[pos], UNUSED, USED);
            if (oldVal == UNUSED) got++;
        }
        end = now();
        double cswapTime = end - start;
        totalCSwapTime += cswapTime;
        double csL = cswapTime / iters * NS, csBW = sizeof(int64_t) * iters / cswapTime / ONE_MB;
        LOGF("cswap %0.6f s, %0.1f ns, %0.3f MB/s avg.  Got %lld, %0.3f %%\n", cswapTime, csL, csBW, (lld) got, 100.0 * got / iters);

        UPC_LOGGED_BARRIER;
        start = now();
        for (long i = 0; i < iters; i++) {
            long pos = random_at_most(THREADS * expansion - 1);
            upc_memget(myBuf, sharedHeaps[pos], nBytesSmall);
        }
        end = now();
        double getSmallTime = end - start;
        totalGetSmallTime += getSmallTime;
        double gsL = getSmallTime / iters * NS, gsBW = nBytesSmall * iters / getSmallTime / ONE_MB;
        LOGF("getSmall %lld: %0.6f s, %0.1f ns %0.3f MB/s avg\n", (lld) nBytesSmall, getSmallTime, gsL, gsBW);

        UPC_LOGGED_BARRIER;
        start = now();
        for (long i = 0; i < iters; i++) {
            long pos = random_at_most(THREADS * expansion - 1);
            upc_memget(myBuf, sharedHeaps[pos], nBytesLarge);
        }
        end = now();
        double getLargeTime = end - start;
        totalGetLargeTime += getLargeTime;
        double glL = getLargeTime / iters * NS, glBW = nBytesLarge * iters / getLargeTime / ONE_MB;
        LOGF("getLarge %lld: %0.6f s, %0.1f ns %0.3f MB/s avg\n", (lld) nBytesLarge, getLargeTime, glL, glBW);

        UPC_LOGGED_BARRIER;
        start = now();
        for (long i = 0; i < iters; i++) {
            long pos = random_at_most(THREADS * expansion - 1);
            upc_memput(sharedHeaps[pos], myBuf, nBytesSmall);
        }
        end = now();
        double putSmallTime = end - start;
        totalPutSmallTime += putSmallTime;
        double psL = putSmallTime / iters * NS, psBW = nBytesSmall * iters / putSmallTime / ONE_MB;
        LOGF("putSmall %lld: %0.6f s, %0.1f ns %0.3f MB/s avg\n", (lld) nBytesSmall, putSmallTime, psL, psBW);

        UPC_LOGGED_BARRIER;
        start = now();
        for (long i = 0; i < iters; i++) {
            long pos = random_at_most(THREADS * expansion - 1);
            upc_memput(sharedHeaps[pos], myBuf, nBytesLarge);
        }
        end = now();
        double putLargeTime = end - start;
        totalPutLargeTime += putLargeTime;
        double plL = putLargeTime / iters * NS, plBW = nBytesLarge * iters / putLargeTime / ONE_MB;
        LOGF("putLarge %lld: %0.6f s, %0.1f ns %0.3f MB/s avg\n", (lld) nBytesLarge, putLargeTime, plL, plBW);
            
        UPC_LOGGED_BARRIER;
        printfBuffer(data, "%s\t%d\t%0.1f\t%0.3f\t%0.1f\t%0.3f\t%0.1f\t%0.3f\t%0.1f\t%0.3f\t%0.1f\t%0.3f\t%s\n", nodename, MYTHREAD, csL, csBW, gsL, gsBW, psL, psBW, glL, glBW, plL, plBW, affinity);

        double elapsed = now() - startTime;
        SLOG("Completed round %lld %0.3f s elapsed. %0f s remaining\n", (lld) round, elapsed, alarmSec - elapsed);
        if (!MYTHREAD) {
            if ((alarmSec > 0 && elapsed >= alarmSec) || (get_file_size(terminateFile) == 0)) {
                incrementGlobalFinished(finished);
                SLOG("Detected terminate signal - %s is missing\n", terminateFile);
                unlink(terminateFile);
            }
        }
        UPC_LOGGED_BARRIER;
    }

    LOG_SET_FLUSH(1);
    UPC_LOGGED_BARRIER;
    freeGlobalFinished(&finished);

    double toNS = ((double) NS) / round * iters; 
    SLOG("Avg cswap: %0.1f ns, smallGet %0.1f ns, smallPut %0.1f ns, largeGet %0.1f ns, largePut %0.1f ns\n", totalCSwapTime * toNS, totalGetSmallTime * toNS, totalPutSmallTime * toNS, totalGetLargeTime * toNS, totalPutLargeTime * toNS); 
    double toMBs = ((double) round) * iters / 1000000;
    SLOG("Avg cswap: %0.3f MB/s, smallGet %0.3f MB/s, smallPut %0.3f MB/s, largeGet %0.3f MB/s, largePut %0.3f MB/s\n", sizeof(int64_t) * toMBs / totalCSwapTime, nBytesSmall * toMBs / totalGetSmallTime, nBytesSmall * toMBs / totalPutSmallTime, nBytesLarge * toMBs / totalGetLargeTime, nBytesLarge * toMBs / totalPutLargeTime); 

    allWriteFile(networkSummaryFile, "a", data, 0);
    freeBuffer(data);
    free_chk(myBuf);
    for (long i = 0; i < expansion; i++) {
        UPC_FREE_CHK(sharedHeaps[MYTHREAD + THREADS*i]);
    }
    UPC_ALL_FREE_CHK(sharedHeaps);
    UPC_ALL_FREE_CHK(compareSwaps);

    CLOSE_MY_LOG;
    return 0;
}

