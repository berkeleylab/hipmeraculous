#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include "optlist.h"

#include "defines.h"
#include "StaticVars.h"
#include "upc_compatibility.h"
#include "upc_common.h"
#include "upc_output.h"
#include "Buffer.h"

#define UNUSED 0
#define USED 1

long random_at_most(long max) {
   unsigned long num_bins = (unsigned long) max + 1, num_rand = (unsigned long) RAND_MAX + 1, bin_size = num_rand / num_bins, defect = num_rand % num_bins;
   
   long x;
   do {
      x = random();
   }
   while (num_rand - defect <= (unsigned long)x);
   
   return x/bin_size;
}

typedef shared[] char* shared_heap_ptr;
typedef shared[] UPC_ATOMIC_MIN_T* shared_flag_ptr;

shared shared_flag_ptr exchangeFlagsPtr[THREADS];
shared shared_heap_ptr exchange[THREADS];

int main(int argc, char **argv) {

   UPC_ATOMIC_MIN_T flag;
	int64_t nBytes, remoteThread, i, iters, poolSize, bound;
	int remoteLoc;
	int64_t fooLoc;
	UPC_TICK_T start, end;
	double memput_time;
	shared[] char *my_space, *remote_space;
   shared[] UPC_ATOMIC_MIN_T *my_flags, *remote_flag_space;
	char *my_private_buff;
   shared_heap_ptr *exchangeLocals;
   exchangeLocals = (shared_heap_ptr *) malloc(THREADS*sizeof(shared_heap_ptr));
   shared_flag_ptr *exchangeFlagsLocals;
   exchangeFlagsLocals = (shared_flag_ptr *) malloc(THREADS*sizeof(shared_flag_ptr));

   /* Use getopt() to read arguments */
   option_t *optList, *thisOpt;
   optList = NULL;
   optList = GetOptList(argc, argv, "b:i:p:");
   
   while (optList != NULL) {
      thisOpt = optList;
      optList = optList->next;
      switch (thisOpt->option) {
         case 'i':
            iters = atoi(thisOpt->argument);
            break;
         case 'b':
            bound = atoi(thisOpt->argument);
            break;
         case 'p':
            poolSize = atoi(thisOpt->argument);
            break;
         default:
            break;
      }
      free(thisOpt);
   }

   char latency_outputfile_name[255];
   char bandwidth_outputfile_name[255];
   sprintf(latency_outputfile_name,"latencyTrav_%d.dat",THREADS);
   sprintf(bandwidth_outputfile_name,"bandwidthTrav_%d.dat",THREADS);
   
   Buffer latency_buf = initBuffer(1024), bw_buf = initBuffer(1024);

	for (nBytes = 64; nBytes <= bound;) {
		/* START */
		upc_barrier;

		/* Allocate local shared space to benchmark upc_memputs */
      my_flags = (shared[] UPC_ATOMIC_MIN_T*) upc_alloc(poolSize * sizeof(UPC_ATOMIC_MIN_T));
		my_space = (shared[] char*) upc_alloc(nBytes * poolSize * sizeof(char));
		my_private_buff = (char*) malloc(nBytes * sizeof(char));
		exchange[MYTHREAD] = my_space;
      exchangeFlagsPtr[MYTHREAD] = my_flags;
		
      
      /* Some initialization */
      for (i=0; i<nBytes; i++) {
			my_private_buff[i] = MYTHREAD;
		}
      
      for (i=0; i<poolSize; i++) {
         my_flags[i] = UNUSED;
      }
      
      for (i=0; i<poolSize*nBytes; i++) {
         my_space[i] = MYTHREAD;
      }
		
		upc_barrier;
		upc_fence;
		upc_barrier;

		for(i=0; i<THREADS; i++) {
			exchangeLocals[i] = exchange[i];
         exchangeFlagsLocals[i] = exchangeFlagsPtr[i];
		}
	
		upc_barrier;
      
      /* Execute the actual microbenchmark */
      
		start = UPC_TICKS_NOW();

		for (i=0; i<iters; i++) {
			remoteThread = random_at_most(THREADS-1);
			remote_space = exchangeLocals[remoteThread];
         remote_flag_space = exchangeFlagsLocals[remoteThread];
			remoteLoc = random_at_most(poolSize-1);
      
         upc_memget((char*) my_private_buff, (shared[] char*) remote_space+(remoteLoc*nBytes), nBytes*sizeof(char));
         UPC_ATOMIC_CSWAP_MIN_T(&flag, (shared_flag_ptr) (remote_flag_space + remoteLoc), UNUSED, USED);
		}

		end = UPC_TICKS_NOW();
		memput_time = UPC_TICKS_TO_SECS(end - start);

		upc_barrier;

		if (MYTHREAD == 0) {
			printf("\nMessage size is %lld bytes\n", (lld) nBytes);
			printf("Time elapsed:\t\t %f seconds\n", memput_time);
			printf("Latency:\t\t %f usec\n", memput_time/iters * 1024.0 * 1024.0 );
			printf("Bandwidth:\t\t %f MB/s\n\n", iters*nBytes/(1024.0 * 1024.0 * memput_time));
		}
      printfBuffer(latency_buf,"%d\t%lld\t%f\n", MYTHREAD, (lld) nBytes, memput_time/iters * 1024.0 * 1024.0);
      printfBuffer(bw_buf,"%d\t%lld\t%f\n", MYTHREAD, (lld) nBytes, iters*nBytes/(1024.0 * 1024.0 * memput_time));

      upc_free(my_flags);
		upc_free(my_space);
      free(my_private_buff);


      if (nBytes < 2048 ) {
                 nBytes += 64;
        } else if (nBytes < 16384) {
                 nBytes += 2048;
        } else if (nBytes <= 32768 ) {
                 nBytes += 4096;
        }

	}

   allWriteFile(latency_outputfile_name, "w", latency_buf, 0);
   allWriteFile(bandwidth_outputfile_name, "w", bw_buf, 0);
   freeBuffer(latency_buf);
   freeBuffer(bw_buf);

   free(exchangeFlagsLocals);
   free(exchangeLocals);

	/* END */


	return 0;
}
