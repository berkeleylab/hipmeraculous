#ifndef __COMMON_H
#define __COMMON_H

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/stat.h>
#ifdef __APPLE__
  #include <sys/param.h>
  #include <sys/mount.h>
#else
  #include <sys/vfs.h>
#endif
#include <sys/types.h>
#include <dirent.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>
#include <stdarg.h>
#include <time.h>

typedef long long int lld;
typedef unsigned long long int llu;

#include "version.h"
#include "optlist.h"

#include "defines.h"
#include "colors.h"
#include "common_rankpath.h"

#ifndef DEFAULT_READ_LEN
#define DEFAULT_READ_LEN 1024
#endif


#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define GET_KMER_PACKED_LEN(k) ((k + 3) / 4)

/* set up LOG, SLOG, DBG, DBG2, DIE, SDIE, WARN, etc macros */

extern FILE *myLog; // This must be defined and initialized (NULL ok) by the primary executable
extern int _num_warnings;

/* verbosity levels: */
typedef enum _LOG_LEVELS {
                    LL_DIE = 0,  /*  FATAL/ERROR/DIE write to file and close file and print to stderr and flush and exit program */
                    LL_WARN, /*   WARN to file and print to stderr and flush */
                    LL_LOG,  /*   LOG/INFO to file and print to stderr */
                    LL_LOGF, /*   LOG/INFO just log to file */
                    LL_DBG,  /*   DEBUG1 to file only */
                    LL_DBG2, /*   DEBUG2 to file only */
                    LL_DBG3, /*   DEBUG3 to file only */
                    LL_MAX} _LOG_LEVELS_TYPE;
/*
   SLOG   only !MYTHREAD outputs
   SDIE   only !MYTHREAD outputs .. all terminate

   everything >= HIPMER_VERBOSITY gets either printed to stderr or writen to myLog or both
   everything <= 1 gets printed and flushed to stderr
*/

#ifdef HIPMER_VERBOSE
  #define HIPMER_VERBOSITY (HIPMER_VERBOSE + LL_WARN)
#else
  #ifndef DEBUG
    #define HIPMER_VERBOSITY LL_LOGF
  #else
    #define HIPMER_VERBOSITY LL_DBG
  #endif
#endif

//inline void setLogPrefix(char *_LOG_prefix, int size, _LOG_LEVELS_TYPE level, const char *fmt, const char *filename, int line) {
 void setLogPrefix(char *_LOG_prefix, int size, _LOG_LEVELS_TYPE level, const char *fmt, const char *filename, int line) {
   assert(size > 1);
   _LOG_prefix[0] = '\0';
   if (strchr(fmt, '\n')) {
     time_t _LOG_ltime; struct tm _LOG_result; char _LOG_stime[32];
     _LOG_ltime = time(NULL); localtime_r(&_LOG_ltime, &_LOG_result); asctime_r(&_LOG_result, _LOG_stime);
     _LOG_stime[strlen(_LOG_stime) - 1] = '\0';
     char _LOG_logLabel[32];
     strcpy(_LOG_logLabel, "[INFO]");
     if (level >= LL_DBG) { snprintf(_LOG_logLabel, 32, "%s[DBG%d]%s", KLBLUE, (int)(level-LL_DBG+1), KNORM); }
     if (level < LL_LOG)  { snprintf(_LOG_logLabel, 32, "%s[%s]%s", (level <= LL_DIE ? KLRED : KRED), (level <= LL_DIE ? "DIE" : "WARN"), KNORM); }
     snprintf(_LOG_prefix, size, "Thread %d, %s %s [%s:%d-%s]: ",  MYTHREAD, _LOG_logLabel, _LOG_stime, filename, line, HIPMER_VERSION);
    }
}

#define _LOG(level, fmt, ...) do { \
   if (HIPMER_VERBOSITY >= level) {                                        \
       char _LOG_prefix[192]; \
       setLogPrefix(_LOG_prefix, 192, level, fmt, __FILENAME__, __LINE__); \
       if (myLog != NULL) { \
          fprintf(myLog, "%s" fmt, _LOG_prefix, ##__VA_ARGS__); \
          if (level < LL_LOG || HIPMER_VERBOSITY >= LL_DBG) fflush(myLog); \
       } \
       if (level < LL_LOG) { \
          fprintf(stderr, "%s" fmt, _LOG_prefix, ##__VA_ARGS__); \
          fflush(stderr); \
       } else if (myLog == NULL || level == LL_LOG) { \
          fprintf(stdout, "%s" fmt, _LOG_prefix, ##__VA_ARGS__); \
          fflush(stdout); \
       } \
   } \
 } while(0)

#define LOG(fmt, ...)  _LOG(LL_LOG, fmt, ##__VA_ARGS__)
#define LOGF(fmt, ...) _LOG(LL_LOGF, fmt, ##__VA_ARGS__)
#define SLOG(fmt, ...) _LOG((MYTHREAD == 0 ? LL_LOG : LL_LOGF), fmt, ##__VA_ARGS__)

#define DBG(fmt, ...)  _LOG(LL_DBG, fmt, ##__VA_ARGS__)
#define DBG1(fmt, ...) _LOG(LL_DBG, fmt, ##__VA_ARGS__)
#define DBG2(fmt, ...) _LOG(LL_DBG2, fmt, ##__VA_ARGS__)
#define DBG3(fmt, ...) _LOG(LL_DBG3, fmt, ##__VA_ARGS__)

#define OPEN_MY_LOG(fmt, ...) \
    do { \
        char myLog_prefix[MAX_FILE_PATH], myLog_logfile_name[MAX_FILE_PATH];\
        snprintf(myLog_prefix, MAX_FILE_PATH, fmt, ##__VA_ARGS__); \
        snprintf(myLog_logfile_name, MAX_FILE_PATH, "%s-%d.log",  myLog_prefix, MYTHREAD); \
        char *rankpath = get_rank_path(myLog_logfile_name, MYTHREAD);\
        myLog = fopen(rankpath, "w");                                \
        if (!myLog) {\
            fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not open file '%s': %s\n" KNORM, \
                    MYTHREAD, __FILENAME__, __LINE__, HIPMER_VERSION, rankpath, strerror(errno));\
            EXIT_FUNC(1);\
        }                \
    } while (0)

#define CLOSE_MY_LOG  \
        do { \
            if (myLog != NULL) { \
                LOGF("Closing this file.\n"); \
                fclose_track(myLog); \
                myLog = NULL; \
            } \
            fflush(stderr); fflush(stdout); \
        } while (0)

#define DIE(fmt,...) \
    do { \
        _LOG(LL_DIE, fmt, ##__VA_ARGS__); \
        CLOSE_MY_LOG; \
        EXIT_FUNC(1); \
    } while (0)

#define SDIE(fmt,...) \
    do { \
        if (!MYTHREAD) \
            _LOG(LL_DIE, fmt, ##__VA_ARGS__); \
        CLOSE_MY_LOG; \
        EXIT_FUNC(1); \
    } while (0)

#define WARN(fmt,...) \
    do { \
        _num_warnings++;\
        _LOG(LL_WARN, fmt, ##__VA_ARGS__); \
    } while (0)

#define serial_printf(fmt, ...)                     \
    do {                                            \
        if (MYTHREAD == 0) {                        \
            fprintf(stdout, fmt, ##__VA_ARGS__);    \
            fflush(stdout);                         \
            if (myLog != NULL) LOGF(fmt, ##__VA_ARGS__);               \
        }                                           \
    } while (0)

#define CHECK_ERR(cmd)                                                  \
    do {                                                                \
        int err;                                                        \
        if ((err = cmd) != 0)                                           \
            DIE("Thread %d, [%s:%d-%s]: " #cmd " failed, error %s\n", MYTHREAD, __FILENAME__, __LINE__, HIPMER_VERSION, strerror(err)); \
    } while (0)


#define ASSERT_BOUNDS(x, bound) do { \
       assert(x >= 0); \
       assert(x < bound); \
   } while (0)

#ifdef DEBUG

#define CHECK_BOUNDS(x, bound) do {                                     \
      if (((x) >= (bound)) || (((lld) (x)) < 0)) {                               \
         lld x1 = x, bound1 = bound;                                    \
         DIE("index out of range for %s >= %s: %lld > %lld\n", #x, #bound, x1, bound1); \
      } \
   } while (0)

#define CHECK_UPPER_BOUND(x, bound) do {                                \
            if ((x) >= (bound)) {                                       \
                lld x1 = x, bound1 = bound;                             \
                DIE("index out of range for %s >= %s: %lld > %lld\n", #x, #bound, x1, bound1); \
            }                                                           \
        } while (0)
#else

#define CHECK_BOUNDS(x, bound)
#define CHECK_UPPER_BOUND(x, bound)

#endif

#define INT_CEIL(numerator, denominator) (((numerator) - 1) / (denominator) + 1);

#define sprintf_chk(s, max, fmt, ...) \
    do { \
        int linelen = snprintf(s, max, fmt, ##__VA_ARGS__); \
        if (linelen >= max) \
           WARN("Thread %d, buffer overflow printing '%s' at %s:%d-%s: (" fmt ")\n", MYTHREAD, fmt,  __FILENAME__, __LINE__, HIPMER_VERSION, ##__VA_ARGS__); \
    } while (0)

static off_t get_file_size(const char *fname)
{
    struct stat s;
    if (stat(fname, &s) != 0) {
        WARN("could not stat %s: %s\n", fname, strerror(errno));
        return 0;
    }
    return s.st_size;
}

#define malloc_chk(s) malloc_chk_at_line(s, __FILENAME__, __LINE__)
inline void *malloc_chk_at_line(size_t size, const char *which_file, int which_line)
{
	if (!size) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Request to malloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	void *x = malloc(size);
	if (!x) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not malloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	return x;
}

#define calloc_chk(n, s) calloc_chk_at_line(n, s, __FILENAME__, __LINE__)
inline void *calloc_chk_at_line(size_t nmemb, size_t size, const char *which_file, int which_line)
{
	if (!size) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Request to calloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	if (!nmemb) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Request to calloc %lu elements\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, nmemb);
		EXIT_FUNC(1);
	}
	void *x = calloc(nmemb, size);
	if (!x) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not calloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	return x;
}

#define realloc_chk(p, s) realloc_chk_at_line(p, s, __FILENAME__, __LINE__)
inline void *realloc_chk_at_line(void *ptr, size_t size, const char *which_file, int which_line)
{
	if (!size) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Request to realloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	void *x = realloc(ptr, size);
	if (!x) {
		fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not realloc %lu bytes\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, size);
		EXIT_FUNC(1);
	}
	return x;
}

 int fclose_track(FILE *f)
{
#ifdef TRACK_FILE_OPENS
    fflush(stderr);
    LOG("\n%p/%d CLOSE\n", f, MYTHREAD);
    fflush(stderr);
#endif
    int ret = fclose(f);
    if (ret != 0) WARN("Could not close file: %p!\n", f);
    return ret;
}

 FILE *fopen_track(const char *path, const char *mode)
{
    FILE *f = fopen(path, mode);
#ifdef TRACK_FILE_OPENS
    fflush(stderr);
    LOG("\n%p/%d OPEN %s\n", f, MYTHREAD, path);
    fflush(stderr);
#else
    // I have no idea why this should cause an issue, but it does
    LOGF("Opening %s: %s\n", path, f == NULL ? "FAILED!" : "success.");
#endif
    return f;
}

#define fopen_chk(p, m) fopen_chk_at_line(p, m, __FILENAME__, __LINE__)
inline FILE *fopen_chk_at_line(const char *path, const char *mode, const char *which_file, int which_line)
{
    FILE *f = fopen_track(path, mode);
    if (!f) {
        fprintf(stderr, KLRED "Thread %d, DIE [%s:%d-%s]: Could not open file '%s': %s\n" KNORM, 
                MYTHREAD, which_file, which_line, HIPMER_VERSION, path, strerror(errno));
        EXIT_FUNC(1);
    }
    return f;
}

static int purgeDir(const char *dir) {
    int count = 0;
    DIR *d = opendir(dir);
    // ignore permission denied errors - means directory owned by someone else
    if (!d && errno != 13) { 
        WARN("Could not opendir %s! %s\n", dir, strerror(errno)); 
        return count; 
    }
    //fprintf(stderr, "Opened dir %s\n", dir);
    struct dirent *result;
    int path_length;
    char path[MAX_FILE_PATH];
    struct stat s;
    while (( result = readdir(d)) != NULL ) {
        path_length = snprintf (path, MAX_FILE_PATH,
                                "%s/%s", dir, result->d_name);
        if (path_length >= MAX_FILE_PATH) {
            WARN("Path length has got too long.\n");
            continue;
        }
        stat(path, &s);
        if(S_ISDIR(s.st_mode)) {

            if (strcmp (result->d_name, "..") != 0 &&
                strcmp (result->d_name, ".") != 0) {

                /* Recursively call purgeDir  */
                count += purgeDir(path);
            }

        } else if (S_ISREG(s.st_mode) || S_ISLNK(s.st_mode)) {
#ifdef DEBUG
            fprintf(stderr, "Unlinking %s\n", path);
#endif
            count += unlink(path) == 0 ? 1 : 0;

        }
    }
    if (closedir(d) == 0) {
        if (strcmp(dir, "/dev/shm") == 0 || strcmp(dir, "/dev/shm/") == 0 || strcmp(dir, "/dev/shm/.") == 0) {
            //fprintf(stderr, "Finished purgeDir %s\n", dir);
        } else {
#ifdef DEBUG
            fprintf(stderr, "rmdir %s\n", dir);
#endif
            count += rmdir(dir) == 0 ? 1 : 0;
        }
    }
    return count;
}


static int check_seqs(const char *seq, char *label)
{
    int len = strlen(seq);
	for (int i = 0; i < len; i++) {
        char c = toupper(seq[i]);
		if (c != 'A' && c != 'C' && c != 'G' && c != 'T' && c != 'N') {
			DIE("Invalid base %c %d in sequence (len %d) %.80s\n%s\n", c, (int) c, len, seq, label);
        }
	}
    return len;
}

inline int check_seq_with_len(const char *seq, int len, char *label)
{
	for (int i = 0; i < len; i++) {
        char c = toupper(seq[i]);
		if (c != 'A' && c != 'C' && c != 'G' && c != 'T' && c != 'N') {
			DIE("Invalid base %c %d in sequence at pos %d (len %d) %.80s\n%s\n", c, c, i, len, seq, label);
        }
	}
    return len;
}

#define fopen_rank_path(p, m, r) fopen_rank_path_at_line(p, m, r, __FILENAME__, __LINE__)
static FILE * fopen_rank_path_at_line(char *buf, const char *mode, int rank, const char *which_file, int which_line) {
    char *rankpath = get_rank_path(buf, rank);
    FILE *f = fopen_chk_at_line(rankpath, mode, which_file, which_line);
    return f;
}

static char *createSHM(char *my_fname, int64_t fileSize) {
    // create shm file
    char *ret = NULL;
    int fd = shm_open(my_fname, O_CREAT|O_TRUNC|O_RDWR, S_IRUSR|S_IWUSR);
    if (fd != -1) {
      if (ftruncate(fd, fileSize) == -1) {
        WARN("Failed to truncate to %lld %s: %s\n", (lld) fileSize, my_fname, strerror(errno));
      } else if (fileSize > 0) {
#ifdef DEBUG
        fprintf(stderr, "Thread %d: Opened shm file %s for writing of size %lld\n", MYTHREAD, my_fname, (lld) fileSize);
#endif
        void *tmpaddr = mmap(NULL, fileSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
        if (tmpaddr == MAP_FAILED) {
            WARN("Could not mmap %s for writing: %s, fileSize %lld\n", my_fname, strerror(errno), (lld) fileSize);
        } else {
            ret = (char*) tmpaddr;
        }
      }
      close(fd);
    }
    return ret;
}


#define UPC_MEMGET_STR(dst, src, len) do {                              \
        upc_memget(dst, src, len);                                      \
        if (strlen(dst) != len - 1)                                     \
            DIE("Length mismatch when getting remote NULL terminated string: %lld != %lld\n", (lld) strlen(dst), (lld) len - 1); \
    } while (0)

#ifndef SEGMENT_LENGTH
#define SEGMENT_LENGTH 51
#endif

static size_t printFoldedSequence(FILE *out, const char *finalSequence, size_t cur_length) {
    /* Print contig in FASTA FORMAT */
    size_t total_written = 0, towrite;
    const char *seqF = (char*) finalSequence;
    char fastaSegment[SEGMENT_LENGTH];
    while ( total_written < cur_length ) {
        if (total_written + SEGMENT_LENGTH-1 < cur_length) {
            towrite = SEGMENT_LENGTH-1;
        } else {
            towrite = cur_length - total_written;
        }
        memcpy(fastaSegment, seqF+total_written, towrite * sizeof(char));
        fastaSegment[towrite] = '\0';
        fprintf(out, "%s\n", fastaSegment);
        total_written += towrite;
    }
    assert(total_written == cur_length);
    return total_written;
}

uint32_t MurmurHash3_x64_32(const void *key, int len);
uint64_t MurmurHash3_x64_64(const void *key, int len);
uint64_t murmur_hash2_64(const void * key, int len);

//#define hashkey(table_size, key, len) (MurmurHash3_x64_64(key, len) % (table_size))
//#define hashstr(table_size, key) (MurmurHash3_x64_64(key, strlen(key)) % (table_size))
#define hashkey(table_size, key, len) (murmur_hash2_64(key, len) % (table_size))
#define hashstr(table_size, key) (murmur_hash2_64(key, strlen(key)) % (table_size))

#ifndef ONE_KB
#define ONE_KB 1024L
#define ONE_MB 1048576L
#define ONE_GB 1073741824L
#endif

typedef struct {
    size_t vm_size;
    size_t vm_rss;
    size_t share;
} memory_usage_t;

#if defined(__APPLE__) && defined(__MACH__)

/*
// use mach_info
#include <mach/vm_statistics.h>
#include <mach/task.h>
#include <mach/task_info.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>
*/

static void get_mem_usage(memory_usage_t *mem) {
/*
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;

    if (KERN_SUCCESS != task_info(mach_task_self(),
                              TASK_BASIC_INFO, (task_info_t)&t_info, 
                              &t_info_count))
    {
        mem->vm_size = 0;
        mem->vm_rss = 0; 
    } else {
        // resident size is in t_info.resident_size;
        // virtual size is in t_info.virtual_size;
        mem->vm_size = t_info.virtual_size;
        mem->vm_rss *= t_info.resident_size;
    }
*/
    mem->vm_size = 0;
    mem->vm_rss = 0;
    mem->share = 0;

}
static int get_max_mem_usage_mb(void) {
    return 0;
}

static double get_free_mem_gb(void) {
    return 0.0;
}

#else

// use the /proc filesystem

static void get_mem_usage(memory_usage_t *mem)
{
    FILE *f = fopen_chk("/proc/self/statm", "r");
    if (fscanf(f, "%lu %lu %lu", &(mem->vm_size), &(mem->vm_rss), &(mem->share)) != 3) {
        WARN("could not read memory usage\n");
        memset(mem, 0, sizeof(memory_usage_t));
        return;
    }
    fclose_track(f);
    int ps = getpagesize();
    mem->vm_size *= ps;
    mem->vm_rss *= ps;
    mem->share *= ps;
}

static int get_max_mem_usage_mb(void)
{
    int usage;
    FILE *f = fopen_chk("/proc/self/status", "r");
    char buf[1000];
    char units[5];
    while (fgets(buf, 999, f)) {
        if (strncmp(buf, "VmHWM:", 6) == 0) {
            sscanf(buf + 6, "%d %s\n", &usage, units);
            break;
        }
    }
    if (strcmp(units, "kB") == 0)
        usage /= 1024;
    fclose_track(f);
    return usage;
}


static double get_free_mem_gb(void)
{
    char buf[1000];
    char units[5];
    long memfree = 0, buffers = 0, cached = 0;
    FILE *f = fopen_chk("/proc/meminfo", "r");
    while (fgets(buf, 999, f)) {
        if (strncmp(buf, "MemFree:", 8) == 0) {
            sscanf(buf + 8, "%ld %s\n", &memfree, units);
        } else if (strncmp(buf, "Buffers:", 8) == 0) {
            sscanf(buf + 8, "%ld %s\n", &buffers, units);
        } else if (strncmp(buf, "Cached:", 7) == 0) {
            sscanf(buf + 7, "%ld %s\n", &cached, units);
        }
    }
    fclose_track(f);
    memfree += buffers + cached;
    if (units[0] == 'k') 
        return (double)memfree / ONE_MB;
    else
        return memfree;
}

#endif

#ifdef SHOW_MEMORY
static void print_memory_used(void) 
{
    if (!MYTHREAD) {
        memory_usage_t mem;
        get_mem_usage(&mem);
        serial_printf("Memory used (GB): VmSize %.2f, VmRSS %.2f, share %.2f\n",
                      (double)mem.vm_size / ONE_GB, (double)mem.vm_rss / ONE_GB, 
                      (double)mem.share / ONE_GB);
    }
}
#else
#define print_memory_used()
#endif 

inline void print_args(option_t *optList, const char *stage)
{
    if (!MYTHREAD) {
        option_t *olist = optList;
        option_t *opt = NULL;
        //serial_printf(KLWHITE "STAGE %s ", stage);
        serial_printf("STAGE %s ", stage);
        while (olist) {
            opt = olist;
            olist = olist->next;
            serial_printf("-%c ", opt->option);
            if (opt->argument)
                serial_printf("%s ", opt->argument);
        }
        //serial_printf(KNORM "\n");
        serial_printf("\n");
    }
#ifdef _UPC_COMMON_H
    upc_barrier;
#endif
}


inline int get_shared_heap_mb(void) 
{
    char *shared_heap_size = getenv("UPC_SHARED_HEAP_SIZE");
    if (!shared_heap_size)
        return 0;
    int len = strlen(shared_heap_size);
    if (!len)
        return 0;
    int64_t shared_heap_bytes = atoi(shared_heap_size);
    if (shared_heap_size[len - 1] == 'G') 
        shared_heap_bytes *= ONE_GB;
    else if (shared_heap_size[len - 1] == 'M') 
        shared_heap_bytes *= ONE_MB;
    else if (shared_heap_size[len - 1] == 'K') 
        shared_heap_bytes *= ONE_KB;
    else 
        shared_heap_bytes *= ONE_MB;
    return shared_heap_bytes / ONE_MB;
}


#endif
