#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>

#include "../hipmer/common/upc_compatibility.h"
#include "upc_common.h"

#define UNUSED 0
#define USED 1

// Total size of pool, FOOTPRINT = BYTES * THREADS * POOL
#define FOOTPRINT 1073741824 

#if defined(GET) || defined(CONSTR) || defined(TRAV)
#define MAXBYTES 32768
#else
#define MAXBYTES 64
#endif


#define NITER 20000


long random_at_most(long max) {
  unsigned long num_bins = (unsigned long) max + 1, num_rand = (unsigned long) RAND_MAX + 1, bin_size = num_rand / num_bins, defect = num_rand % num_bins;
  long x;
  do {
    x = random();
  }
  while (num_rand - defect <= (unsigned long)x);
  return x/bin_size;
}

static shared double _all_double[THREADS];

double reduce_double(double myval, upc_op_t op) {
  shared double *per_thread_double;
  UPC_ALL_ALLOC_CHK(per_thread_double, THREADS, sizeof(double));
  per_thread_double[MYTHREAD] = myval;
  bupc_all_reduce_allD(_all_double, per_thread_double, op, THREADS, 1, NULL, UPC_IN_MYSYNC|UPC_OUT_MYSYNC);
  UPC_ALL_FREE_CHK(per_thread_double);
  return _all_double[MYTHREAD];
}

typedef shared[] char* shared_heap_ptr;
typedef shared[] UPC_ATOMIC_MIN_T* shared_flag_ptr;

#ifdef CONSTR
shared int64_t heapPtrs[THREADS];
#endif

shared shared_flag_ptr exchangeFlagsPtr[THREADS];
shared shared_heap_ptr exchange[THREADS];

FILE *myLog = NULL;
int _num_warnings = 0;

int main(int argc, char **argv) {
  UPC_ATOMIC_MIN_T flag;
  int64_t nBytes, remoteThread, i, iters, poolSize, bound;
  int remoteLoc;
  int64_t fooLoc;
  UPC_TICK_T start, end;
  double memput_time;
  shared[] char *my_space, *remote_space;
  shared[] UPC_ATOMIC_MIN_T *my_flags, *remote_flag_space;
  char *my_private_buff;
  shared_heap_ptr *exchangeLocals;
  exchangeLocals = (shared_heap_ptr *) malloc(THREADS*sizeof(shared_heap_ptr));
  shared_flag_ptr *exchangeFlagsLocals;
  exchangeFlagsLocals = (shared_flag_ptr *) malloc(THREADS*sizeof(shared_flag_ptr));

  srand(MYTHREAD);

  for (nBytes = 64; nBytes <= MAXBYTES;) {
    upc_barrier;

    size_t POOL = FOOTPRINT / (nBytes * THREADS);

    if (MYTHREAD==0) {
      printf("pool, bytes, net: %i %i %i\n", POOL, nBytes, nBytes * POOL * THREADS);
    }

    /* Allocate local shared space to benchmark upc_memputs */
    my_flags = (shared[] UPC_ATOMIC_MIN_T*) upc_alloc(POOL * sizeof(UPC_ATOMIC_MIN_T));
    my_space = (shared[] char*) upc_alloc(nBytes * POOL * sizeof(char));
    my_private_buff = (char*) malloc(nBytes * sizeof(char));
    exchange[MYTHREAD] = my_space;
    exchangeFlagsPtr[MYTHREAD] = my_flags;


#ifdef CONSTR
    heapPtrs[MYTHREAD] = 0;
#endif

	             
    /* Some initialization */
    for (i=0; i<nBytes; i++) my_private_buff[i] = MYTHREAD;
    for (i=0; i<POOL; i++)  my_flags[i] = UNUSED;     
    for (i=0; i<POOL*nBytes; i++) my_space[i] = MYTHREAD;   

    upc_barrier;
    upc_fence;
    upc_barrier;

    for(i=0; i<THREADS; i++) {
      exchangeLocals[i] = exchange[i];
      exchangeFlagsLocals[i] = exchangeFlagsPtr[i];
    }	
    upc_barrier;
    /* Execute the actual microbenchmark */          

    start = UPC_TICKS_NOW();

    for (i=0; i<NITER; i++) {
      remoteThread = random_at_most(THREADS-1);
      remote_space = exchangeLocals[remoteThread];
      remoteLoc = random_at_most(POOL-1);

#ifdef GET
      upc_memget((char*) my_private_buff, (shared[] char*) remote_space+(remoteLoc*nBytes), nBytes*sizeof(char));
#endif

#ifdef TRAV
      remote_flag_space = exchangeFlagsLocals[remoteThread];
      upc_memget((char*) my_private_buff, (shared[] char*) remote_space+(remoteLoc*nBytes), nBytes*sizeof(char));
      UPC_ATOMIC_CSWAP_MIN_T(&flag, (shared_flag_ptr) (remote_flag_space + remoteLoc), UNUSED, USED);
#endif

#ifdef CAS
      remote_flag_space = exchangeFlagsLocals[remoteThread];
      UPC_ATOMIC_CSWAP_MIN_T(&flag, (shared_flag_ptr) (remote_flag_space + remoteLoc), UNUSED, USED);
#endif

#ifdef READ
      remote_flag_space = exchangeFlagsLocals[remoteThread];
      flag = bupc_atomicI_read_strict((shared_flag_ptr) (remote_flag_space + remoteLoc));
#endif

#ifdef CONSTR
      UPC_ATOMIC_FADD_I64(&fooLoc, &heapPtrs[remoteThread], nBytes);
      upc_memput((shared[] char*) remote_space+(remoteLoc*nBytes), my_private_buff, nBytes*sizeof(char));
#endif

    }

    end = UPC_TICKS_NOW();
    memput_time = UPC_TICKS_TO_SECS(end - start);
    upc_barrier;

    double avgtime = reduce_double(memput_time, UPC_ADD) / (double) THREADS;
    double mintime = reduce_double(memput_time, UPC_MIN);
    double maxtime = reduce_double(memput_time, UPC_MAX);

    if (MYTHREAD == 0) {      
      printf("avg: %lld %f %f %f\n", (lld) nBytes, avgtime, avgtime/NITER * 1024.0 * 1024.0, NITER*nBytes/(1024.0 * 1024.0 * avgtime));
      printf("min: %lld %f %f %f\n", (lld) nBytes, mintime, mintime/NITER * 1024.0 * 1024.0, NITER*nBytes/(1024.0 * 1024.0 * mintime));
      printf("max: %lld %f %f %f\n", (lld) nBytes, maxtime, maxtime/NITER * 1024.0 * 1024.0, NITER*nBytes/(1024.0 * 1024.0 * maxtime));
    }

    upc_free(my_flags);
    upc_free(my_space);
    free(my_private_buff);

    if (nBytes < 256 ) {
      nBytes += 64;
    } else if (nBytes < 1024) {
      nBytes += 256;
    } else if (nBytes < 4096) {
      nBytes += 1024;
    } else if (nBytes <= 32768 ) {
       nBytes += 4096;
    }

  }


  free(exchangeFlagsLocals);
  free(exchangeLocals);

  /* END */


  return 0;
}
