#ifndef _UPC_COMMON_H
#define _UPC_COMMON_H

#include <assert.h>
#include <stdio.h>
#include <string.h>

#ifndef EXEC_FUNC
#include <upc.h>
#define EXIT_FUNC(x) do { upc_global_exit(x); } while(0)
#endif

#include "common.h"
#include "../hipmer/common/upc_compatibility.h"

typedef shared [] double* SharedDoublePtr;
typedef shared [] int64_t* SharedLongPtr;
typedef shared [] int8_t* SharedInt8Ptr;
typedef shared [] uint8_t* SharedUInt8Ptr;
typedef shared [] char* SharedCharPtr;
typedef shared [1] double* AllSharedDoublePtr;
typedef shared [1] int64_t* AllSharedLongPtr;
typedef shared [1] int8_t* AllSharedInt8Ptr;
typedef shared [1] uint8_t* AllSharedUInt8Ptr;
typedef shared [1] char* AllSharedCharPtr;
typedef shared void* SharedVoidPtr; // UPC forbids blocksize in void*

#ifndef PROTECT_UPC_ALLOCATIONS

#define UPC_ALL_ALLOC_CHK(ptr, nBlocks, nbytes) (ptr) = upc_all_alloc(nBlocks, nbytes)
#define UPC_ALL_FREE_CHK(ptr) upc_all_free(ptr)
#define UPC_ALLOC_CHK(ptr, nbytes) (ptr) = upc_alloc(nbytes)
#define UPC_FREE_CHK(ptr) upc_free(ptr)

#else

#ifndef UPC_BUFFERING_ALLOC
#ifdef DEBUG
#define UPC_ALL_BUFFERING_ALLOC 7
#define UPC_BUFFERING_ALLOC 7
#else
#define UPC_ALL_BUFFERING_ALLOC 0
#define UPC_BUFFERING_ALLOC 0
#endif
#endif

#define ZERO_INIT_ALLOC 0

#ifndef ZERO_INIT_ALLOC
  #ifdef DEBUG
    // highlight any bugs because allocated buffers are NOT allocated and initialized
    #define ZERO_INIT_ALLOC 0
  #else
    // in release build, always initialize buffers to 0, (should be very fast)
    #define ZERO_INIT_ALLOC 1
  #endif
#endif


#define GET_STRIDED_INT(value, ptr, offset, intbytes, blocksize) do { \
        value = 0; \
        for(int __i = 0; __i < intbytes; __i++) { \
            uint8_t *__tmpPtr = (uint8_t*) ((ptr) + __i*THREADS + (offset)); \
            value += ((uint64_t) __tmpPtr[0]) << (__i*8); \
            for(int __j = 1; __j < blocksize; __j++) { if (__tmpPtr[__j] != 0) WARN("Invalid zero byte padding in block of %llu at offset %lld, %d-%d for %s\n", (llu) blocksize, (lld) offset, __i, __j, #ptr); } \
        } \
    } while (0)

#define SET_STRIDED_INT(value, ptr, offset, intbytes, blocksize) do { \
        uint64_t __val = (value); \
        for(int __i = 0; __i < intbytes; __i++) { \
            uint8_t *__tmpPtr = (uint8_t*) ((ptr) + __i*THREADS + (offset)); \
            memset(__tmpPtr, 0, blocksize); \
            __tmpPtr[0] = (uint8_t) ((__val >> (__i*8)) & 0xff); \
        } \
        if (HIPMER_VERBOSITY > LL_DBG) { \
            GET_STRIDED_INT(__val, ptr, offset, intbytes, blocksize); \
            if (__val != (value)) WARN("get/set_strided_int did not agree %llu (%08llx) vs %llu (%08llx)\n", (llu) __val, (llu) __val, (llu) (value), (llu) (value)); \
        } \
    } while (0)

#define UPC_VALIDATE_ALL_PTR(ptr) do { \
      if (UPC_ALL_BUFFERING_ALLOC > 0) { \
          uint64_t __nblocks, __blockSize, __check1, __check2; \
          /* best guess at accurate values is middle of start buffer */ \
          GET_STRIDED_INT(__blockSize, ptr, MYTHREAD - THREADS*((UPC_ALL_BUFFERING_ALLOC/2+1) * 16 + 8), 8, 0); \
          GET_STRIDED_INT(__nblocks, ptr, MYTHREAD - THREADS*((UPC_ALL_BUFFERING_ALLOC/2+1) * 16), 8, __blockSize); \
          int64_t __startOffset = MYTHREAD - THREADS * UPC_ALL_BUFFERING_ALLOC * 16; \
          int64_t __blockModulo = __nblocks % THREADS; \
          int64_t __endOffset = __nblocks + (MYTHREAD < __blockModulo ? THREADS - __blockModulo + MYTHREAD : MYTHREAD - __blockModulo) + (UPC_ALL_BUFFERING_ALLOC-1)*THREADS * 16; \
          for (int __bufPos = 0; __bufPos < UPC_ALL_BUFFERING_ALLOC; __bufPos++) { \
               GET_STRIDED_INT(__check1, ptr, __startOffset, 8, __blockSize); \
               GET_STRIDED_INT(__check2, ptr, __startOffset + 8*THREADS, 8, __blockSize); \
               if (__check1 != __nblocks || __check2 != __blockSize) { WARN("Invalid allocation start buffer padding (%d) for %s at %llu (expect: %llu,%llu , start: %llu, %llu)\n", __bufPos, #ptr, (llu) upc_addrfield(ptr), (llu) __nblocks, (llu) __blockSize, (llu) __check1, (llu) __check2); } \
               GET_STRIDED_INT(__check1, ptr, __endOffset, 8, __blockSize); \
               GET_STRIDED_INT(__check2, ptr, __endOffset + 8*THREADS, 8, __blockSize); \
               if (__check1 != __nblocks || __check2 != __blockSize) { WARN("Invalid allocation end buffer padding (%d) for %s at %llu (expect: %llu,%llu , end: %llu, %llu)\n", __bufPos, #ptr, (llu) upc_addrfield(ptr), (llu) __nblocks, (llu) __blockSize, (llu) __check1, (llu) __check2); } \
               __startOffset += 16*THREADS; \
               __endOffset -= 16*THREADS; \
          } \
      } \
      assert(upc_threadof(ptr) == 0); \
    } while (0)

// upc_all_alloc should always be > 0!
#define UPC_ALL_ALLOC_CHK(ptr, nBlocks, nbytes) do {                    \
      if ((nbytes) <= 0 || (nBlocks) <= 0)                              \
         DIE("trying to allocate 0 or less bytes: %llu or blocks: %lld\n", (llu)(nbytes), (llu)(nBlocks)); \
      uint64_t __nblocks = nBlocks; \
      uint64_t __totalBlocks = (__nblocks) + (UPC_ALL_BUFFERING_ALLOC * 2 * THREADS * 16); \
      (ptr) = upc_all_alloc(__totalBlocks, (nbytes));                           \
      if (!(ptr))                                                       \
         DIE("upc_all_alloc(%llu, %llu) failed\n", (llu)(__nblocks), (llu)(nbytes)); \
      if (ZERO_INIT_ALLOC) { \
         for(int64_t __i = MYTHREAD; __i < __totalBlocks; __i+=THREADS) { \
             assert(upc_threadof( &(ptr)[__i] ) == MYTHREAD); \
             memset((char *) &(ptr)[__i], 0, (nbytes)); \
         } \
      } \
      if (UPC_ALL_BUFFERING_ALLOC > 0) { \
        assert(upc_threadof(ptr) == 0); \
        if (MYTHREAD == 0 || MYTHREAD == THREADS/2 || MYTHREAD == THREADS-1) DBG("(first-mid-last) upc_all_alloc adjusting buffer for %s at %llu with %llu blocks of %llu bytes %llu totalBlocks\n", #ptr, (llu) upc_addrfield(ptr), (llu) (__nblocks), (llu) (nbytes), (llu) (__totalBlocks)); \
        (ptr) = &((ptr)[UPC_ALL_BUFFERING_ALLOC * THREADS * 16]); /* move the pointer first */ \
        assert(upc_threadof(ptr) == 0); \
        int64_t __startOffset = MYTHREAD - THREADS * UPC_ALL_BUFFERING_ALLOC * 16; \
        int64_t __blockModulo = __nblocks % THREADS; \
        int64_t __endOffset = __nblocks + (MYTHREAD < __blockModulo ? THREADS - __blockModulo + MYTHREAD : MYTHREAD - __blockModulo) + (UPC_ALL_BUFFERING_ALLOC-1)*THREADS * 16; \
        uint64_t __check1, __check2; \
        for (int __bufPos = 0; __bufPos < UPC_ALL_BUFFERING_ALLOC; __bufPos++) { /* set buffer padding values to verify later and shift starting pointer */ \
            SET_STRIDED_INT((__nblocks), ptr, __startOffset, 8, nbytes); \
            SET_STRIDED_INT((nbytes), ptr, __startOffset + 8*THREADS, 8, nbytes); \
            SET_STRIDED_INT((__nblocks), ptr, __endOffset, 8, nbytes); \
            SET_STRIDED_INT((nbytes), ptr, __endOffset + 8*THREADS, 8, nbytes); \
            __startOffset += 16*THREADS; \
            __endOffset -= 16*THREADS; \
         } \
         /* UPC_VALIDATE_ALL_PTR(ptr); */  \
      } \
      assert(upc_threadof(ptr) == 0); \
   } while (0)


#define UPC_ALL_FREE_CHK(ptr) do { \
      if ((ptr) == NULL) { \
          WARN("Request to upc_all_free a null pointer! %s\n", #ptr); \
          break; \
      } \
      if (MYTHREAD == 0 || MYTHREAD == THREADS/2 || MYTHREAD == THREADS-1) LOGF("(first-mid-last) upc_all_free of %s at %llu\n", #ptr, (llu) upc_addrfield(ptr)); \
      if (UPC_ALL_BUFFERING_ALLOC > 0) { /* validate and move ptr back */ \
         UPC_VALIDATE_ALL_PTR(ptr); /* validate first */ \
         (ptr) = &((ptr)[- THREADS * UPC_ALL_BUFFERING_ALLOC * 16]); /* move the pointer last */ \
         assert(upc_threadof(ptr) == 0); \
         if (MYTHREAD == 0 || MYTHREAD == THREADS/2 || MYTHREAD == THREADS-1) DBG("(first-mid-last) upc_all_free re-adjusting buffer for %s at %llu\n", #ptr, (llu) upc_addrfield(ptr)); \
      } \
      upc_all_free(ptr); \
      (ptr) = NULL; \
   } while (0)


#define UPC_VALIDATE_PTR(ptr) do { \
      SharedLongPtr __start = (SharedLongPtr) (ptr), __end = NULL; \
      uint64_t __nbytes = 0; \
      if (UPC_BUFFERING_ALLOC > 0) { \
         __nbytes = *(__start - (UPC_BUFFERING_ALLOC/2 + 1)); \
         assert(__nbytes % 8 == 0); \
         __start = __start - UPC_BUFFERING_ALLOC; \
         __end = ((SharedLongPtr) ((SharedCharPtr)(ptr) + __nbytes)) + UPC_BUFFERING_ALLOC - 1; \
      } \
      for (int __bufPos = 0; __bufPos < UPC_BUFFERING_ALLOC; __bufPos++) { /* set buffer padding values to verify later and move pointer */ \
          if (*__start != __nbytes || *__end != __nbytes) { WARN("Invalid allocation buffer padding (%d) for %s at %llu (%llu) (expect: %llu, start: %llu, end: %llu)\n", __bufPos, #ptr, (llu) upc_addrfield(ptr), (llu) upc_phaseof(ptr), (llu) __nbytes, (llu) *__start, (llu) *__end); } \
          __start++; __end--;\
      } \
   } while (0)

// upc_alloc is okay to be 0 bytes, but log it
#define UPC_ALLOC_CHK(ptr, nBytes) do {                                 \
      uint64_t __nbytes = (nBytes); \
      if ((__nbytes) <= 0) {                                               \
         LOGF("trying to allocate 0 or less bytes for '%s': %llu\n", #ptr, (llu)(__nbytes)); \
         (ptr) = NULL; \
         break; \
      } \
      if (UPC_BUFFERING_ALLOC>0 && (__nbytes%8) != 0) __nbytes += 8-(__nbytes%8); \
      uint64_t __totalBytes = __nbytes + UPC_BUFFERING_ALLOC * 2 * 8; \
      (ptr) = upc_alloc(__totalBytes); \
      if (!(ptr))                                                       \
         DIE("upc_alloc(%llu) failed\n", (llu)(__nbytes));                  \
      if (ZERO_INIT_ALLOC) { \
         memset((char *) (ptr), 0, __totalBytes); \
      } \
      if (UPC_BUFFERING_ALLOC > 0) { /* move pointer and set buffer markers */ \
         DBG("upc_alloc adjusting %s at %llu (%llu) with %llu bytes\n", #ptr, (llu) upc_addrfield(ptr), (llu) upc_phaseof(ptr), (llu) (__nbytes)); \
         (ptr) = (SharedVoidPtr) (((SharedLongPtr) (ptr)) + UPC_BUFFERING_ALLOC); \
         int64_t __startOffset = - UPC_BUFFERING_ALLOC; \
         int64_t __endOffset = __nbytes/8 + UPC_BUFFERING_ALLOC - 1; \
         for (int __bufPos = 0; __bufPos < UPC_BUFFERING_ALLOC; __bufPos++) { /* set buffer padding values to verify later and move pointer */ \
             SharedLongPtr __start = (((SharedLongPtr) (ptr)) + __startOffset), __end = (((SharedLongPtr) (ptr)) + __endOffset); \
             *__start = __nbytes; \
             *__end = __nbytes; \
             __startOffset++; __endOffset--; \
         } \
         /* UPC_VALIDATE_PTR(ptr); */ \
      } \
   } while (0)

#define UPC_FREE_CHK(ptr) do { \
      if ((ptr) == NULL) { \
          LOGF("Request to upc_free a null pointer! %s\n", #ptr); \
          break; \
      } \
      if (UPC_BUFFERING_ALLOC > 0) { \
        UPC_VALIDATE_PTR(ptr); \
        SharedLongPtr __tmpPtr = ((SharedLongPtr) (ptr)) - UPC_BUFFERING_ALLOC; \
        (ptr) = (SharedVoidPtr) __tmpPtr; \
        DBG("upc_free adjusted %s at %llu (%llu)\n", #ptr, (llu) upc_addrfield(ptr), (llu) upc_phaseof(ptr)); \
      } \
      upc_free(ptr); \
      (ptr) = NULL; \
   } while (0)


#endif // PROTECT_UPC_ALLOC

// generic Heap structure for distributed memory applications

typedef struct _Heap {
    int64_t size;
    SharedCharPtr ptr;
    int64_t position, fencePosition;
} Heap;
typedef Heap *HeapPtr;
typedef shared Heap *SharedHeapPtr;

#if USE_NEW_HEAP

#define initHeap(heapPtr, bytes) do { \
      Heap heap; \
      SharedCharPtr tmp; \
      UPC_ALLOC_CHK(tmp, bytes); \
      heap.ptr = tmp; \
      heap.position = 0; \
      heap.fencePosition = 0; \
      heap.size = bytes; \
      *(heapPtr) = heap; \
      DBG("initHeap(%llu): %s at %llu\n", (llu) (bytes), #heapPtr, (llu) (upc_addrfield(tmp))); \
    } while (0)


#define freeHeap(heapPtr) do { \
      if ((heapPtr) != NULL) { \
          SharedCharPtr tmp = (heapPtr)->ptr; \
          DBG("freeHeap(%s at %llu) ptr=%llu\n", #heapPtr, (llu) upc_addrfield(heapPtr), (llu) upc_addrfield(tmp)); \
          Heap empty; empty.ptr = NULL; empty.size = 0; empty.position = 0; empty.fencePosition = 0; \
          *(heapPtr) = empty; \
          if (tmp != NULL) { \
              UPC_FREE_CHK(tmp); \
          } \
      } else { \
          WARN("Attempt to freeHeap of null ptr\n"); \
      } \
    } while (0)

// appends heap, potentially extending the size ***UNSAFE if multiple threads will access this data****
// returns a pointer to the newly copied data.
static SharedCharPtr _appendHeap(HeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line) {
  assert(heapPtr != NULL);
  assert(heapPtr->ptr != NULL);
  Heap oldHeap = *heapPtr;
  if (oldHeap.position + length >= oldHeap.size) {
      // Must reallocate heap
      int64_t newSize = (heapPtr->size + length) * 3 / 2 + 64;
      DBG("Reallocating heap from thread %llu (locally) from %llu to %llu bytes (%s:%d)\n", (llu) upc_threadof(oldHeap.ptr), (llu) oldHeap.size, (llu) newSize, whichFile, line);
      Heap newHeap;
      initHeap(&newHeap, newSize);
      assert(newHeap.size >= newSize);
      assert(newHeap.ptr != NULL);
      assert(upc_threadof(newHeap.ptr) == MYTHREAD);
      SharedCharPtr tmp1 = newHeap.ptr;
      char *dest = (char*) tmp1;
      SharedCharPtr src = oldHeap.ptr;
   
      if (upc_threadof(oldHeap.ptr) == MYTHREAD) {
         memcpy(dest, (const char *) src, oldHeap.position);
      } else {
         upc_memget(dest, src, oldHeap.position);
      }
      newHeap.position = oldHeap.position;
      assert(newHeap.position <= newHeap.size);
      UPC_FREE_CHK(heapPtr->ptr);
      *heapPtr = newHeap;
      upc_fence;
   }

   assert(heapPtr->ptr != NULL);
   assert(heapPtr->position + length <= heapPtr->size);
   SharedCharPtr result = heapPtr->ptr + heapPtr->position;
   upc_memput(result, data, length);
   heapPtr->position += length;
   return result;
}
#define appendHeap(heap, data, length) _appendHeap((HeapPtr) &(heap), (const char *) data, length, __FILE__, __LINE__) 

// Threadsafe version of _appendHeap
static SharedCharPtr _safeAppendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line) {
  assert(heapPtr != NULL);
  Heap localHeap, oldHeap;
  int64_t oldPos = -1, oldSize = -1;
  // if heapPtr is already past maximum, wait for another thread to fix
  while(1) {
    localHeap = *heapPtr;
    if (localHeap.position < localHeap.size) {
      // potentially okay. cached heap has some room
      if (oldPos == -1) {
          UPC_ATOMIC_FADD_I64(&oldPos, &(heapPtr->position), length);
      }
      UPC_ATOMIC_GET_I64(&oldSize, &(heapPtr->size));
      if (oldPos < oldSize) {
        // still okay oldPos is less than existing size
        if (oldPos < oldSize) {
          // ready to append
          if (localHeap.size != oldSize) {
            // need to refresh pointer
            localHeap = *heapPtr;
            assert(localHeap.size == oldSize);
          }           
          break;
        } // else wait for another thread to reallocate
      } // else wait for another thread to reallocate
    } // else wait for another thread to reallocate
    // try again - some other thread pushed size boundary and will reallocate
    UPC_POLL;
  }
  CHECK_BOUNDS(oldPos, localHeap.size);

  // no other thread has modified heapPtr
  assert(localHeap.size == heapPtr->size);
  assert(localHeap.ptr == heapPtr->ptr);

  // remember the old
  oldHeap = localHeap;

  int isNew = 0;
  int64_t newPos = oldPos + length;
  if (newPos >= localHeap.size) {
    isNew = 1;
    // this thread must re-allocate (since it pushed the size boundary)
    int64_t newSize = (localHeap.size + length) * 2 / 3 + 64;
    assert(newPos < newSize);
    Heap newHeap;
    initHeap(&newHeap, newSize);
    assert(upc_threadof(newHeap.ptr) == MYTHREAD);
    while (1) {
        int64_t oldFencePos;
        UPC_ATOMIC_CSWAP_I64(&oldFencePos, &(heapPtr->fencePosition), oldPos, newPos)) {
        if (oldPos == oldFencePos) break;
        // wait for all threads to finish upc_memput to the old heap!
        assert(oldFencePos <= oldPos);
        UPC_POLL;
    }
    assert(heapPtr->fencePosition == newPos);

    SharedCharPtr src = localHeap.ptr;
    SharedCharPtr dst = newHeap.ptr;
    assert(heapPtr->ptr == src);
    if (upc_threadof(src) == MYTHREAD) {
        memcpy((char*) dst, (char*) src, oldPos);
    } else {
        upc_memget((char*) dst, src, oldPos);
    }
    newHeap.position = newPos;
    newHeap.fencePosition = oldPos;
    assert(heapPtr->ptr == localHeap.ptr);
    assert(heapPtr->ptr == oldHeap.ptr);
    localHeap = newHeap;
  }
  SharedCharPtr newData = localHeap.ptr + oldPos;
  upc_memput(newData, data, length);
  UPC_FENCE;
  if (isNew) {
    *heapPtr = localHeap;
    UPC_FENCE;
    UPC_FREE_CHK(oldHeap.ptr);
  }
  UPC_ATOMIC_FADD_I64_RELAXED(NULL, &(heapPtr->fencePosition), length);
  return newData;
}
#define safeAppendSharedHeap(heap, data, length) _safeAppendSharedHeap((SharedHeapPtr) &(heap), (const char *) data, length, __FILE__, __LINE__)

// appends heap, potentially extending the size *** UNSAFE if multiple threads will access this data concurrently ***
// returns a pointer to the newly copied data.
static SharedCharPtr _appendSharedHeap(SharedHeapPtr heapPtr, const char *data, int64_t length, const char *whichFile, int line) {
   assert(heapPtr != NULL);
   Heap heap = *heapPtr;
   SharedCharPtr result = _appendHeap(&heap, data, length, whichFile, line);
   *heapPtr = heap;
   return result;
}
#define appendSharedHeap(heap, data, length) _appendSharedHeap((SharedHeapPtr) &(heap), (const char *) data, length, __FILE__, __LINE__) 

#endif // USE_NEW_HEAP

#endif // UPC_COMMON_H

