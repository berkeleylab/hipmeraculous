#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <upc.h>
#include <string.h>
#include <sys/types.h>
#include <upc_collective.h>

#include "../hipmer/common/upc_compatibility.h"
#include "upc_common.h"

long random_at_most(long max) {
  unsigned long num_bins = (unsigned long) max + 1, num_rand = (unsigned long) RAND_MAX + 1, bin_size = num_rand / num_bins, defect = num_rand % num_bins;
  long x;
  do {
    x = random();
  }
  while (num_rand - defect <= (unsigned long)x);
  return x/bin_size;
}

shared int64_t final_result;

FILE *myLog = NULL;
int _num_warnings = 0;

int main(int argc, char **argv) {
 
  shared int64_t *result_values = (shared int64_t*) upc_all_alloc(THREADS, sizeof(int64_t));
  shared int64_t *shared_values = (shared int64_t*) upc_all_alloc(THREADS, sizeof(int64_t));
   
  int64_t my_local_sum = 0;
  int64_t received_total_sum = 0;
  int64_t i;
  UPC_TICK_T start, end, end2, start2;
  double manual_reduction = 0.0;
  shared_values[MYTHREAD] = MYTHREAD;
  upc_barrier;
  
  // NAIVE 1
  start = UPC_TICKS_NOW();
  for (i=0; i<THREADS; i++) {
    my_local_sum += shared_values[i];
  }
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Naive all to all reduction took %f seconds\n", UPC_TICKS_TO_SECS(end - start));
  }
  upc_barrier;
  
  // NAIVE 2
  start = UPC_TICKS_NOW();
  my_local_sum = 0;
  if (MYTHREAD == 0) {
    for (i=0; i<THREADS; i++) {
      my_local_sum += shared_values[i];
    }
    final_result = my_local_sum;
  }
  end2 = UPC_TICKS_NOW();
  manual_reduction = UPC_TICKS_TO_SECS(end2 - start);
  upc_barrier;
  my_local_sum = final_result;
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Thread 0 calculated and all threads read shared result took %f seconds [REDUCTION TO SINGLE TOOK %f seconds]\n", UPC_TICKS_TO_SECS(end - start), manual_reduction);
  }

  upc_barrier;
  
  // NAIVE 3
  my_local_sum = 0;
  start = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    for (i=0; i<THREADS; i++) {
      my_local_sum += shared_values[i];
    }
    start2 = UPC_TICKS_NOW();
    for (i=0; i<THREADS; i++) {
      result_values[i] = my_local_sum;
    }
    end2 = UPC_TICKS_NOW();
  }
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Thread 0 calculated and wrote result to shared array in %f seconds [BROADCASTING FROM THREAD 0 took %f seconds]\n", UPC_TICKS_TO_SECS(end - start), UPC_TICKS_TO_SECS(end2 - start2));
  }
  upc_barrier;
  
  // Native UPC all to all reduction
  start = UPC_TICKS_NOW();
  bupc_all_reduce_allL(result_values, shared_values, UPC_ADD, THREADS, 1, NULL, UPC_IN_MYSYNC|UPC_OUT_MYSYNC);
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("NATIVE all to all reduction took %f seconds\n", UPC_TICKS_TO_SECS(end - start));
  }
  if (MYTHREAD == 0) {
    final_result = 0;
  }
  upc_barrier;
  
  // Code for HW atomic reduction
  start = UPC_TICKS_NOW();
  UPC_ATOMIC_FADD_I64_RELAXED(NULL, &final_result, shared_values[MYTHREAD]);
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Reduction with atomic FADD %f seconds\n", UPC_TICKS_TO_SECS(end - start));
  }
  
  // Code for  single destination reduction
  upc_barrier;
  start = UPC_TICKS_NOW();
  upc_all_reduceL(&final_result, shared_values, UPC_ADD, THREADS, 1, NULL, UPC_IN_MYSYNC|UPC_OUT_MYSYNC);
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Single destination collective reduction took %f seconds\n", UPC_TICKS_TO_SECS(end - start));
  }
  upc_barrier;
  
  // Code for broadcast
  start = UPC_TICKS_NOW();
  upc_all_broadcast(shared_values, &final_result, sizeof(long), UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
  upc_barrier;
  end = UPC_TICKS_NOW();
  if (MYTHREAD == 0) {
    printf("Single destination collective broadcast took %f seconds\n", UPC_TICKS_TO_SECS(end - start));
  }

  return 0;
}
