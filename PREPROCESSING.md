# *HipMer* -- Preparing your data

## Prerequisites

For each FASTQ file, you should know a few things about it:

1. How it is formatted (unpaired, paired interleaved or paired in two files)
    * If unpaired, use --single
    * If paired interleaved, use --interleaved
    * If paired in two files, use --paired
2. If paired, the approximate size and standard deviation between mates
    * If the insert size is overlapping this may be omitted and can generally be calculated at runtime
3. If paired, whether they are innie (FR) ----> <----  or outie (RF) <----   ---->

## Preparing the FASTQ

HipMer reads input fastq in parallel so, unfortunately, the fastq files must be *uncompressed*.  Additionally,
the filesystem where the input files must be visible to all the nodes that will be running HipMer, so be resident on a
properly configured parallel filesystem.  On LUSTRE, for example you should set the striping of the directory before copying
the files to the directory.  With LUSTRE, HipMer controls the striping properly for output files, but the inputs are assumed
to be ready to read from all the nodes at scale.

Additionally, we have found that for most assemblies a minimal amount of preprocessing produces a better assembly.  
It sometimes helps to remove adaptors and contaminates.  For this purpose a trim_fastq.sh script in the contrib directory can perform this basic
operation on typical Illumina datasets.

The following docker / shifter enabled command should work well to prepare the data from a compressed fastq.gz into a filtered dataset
of pairs and merged reads.

You should first enter and prepare your directory for the newly trimmed / merged files.  On Lustre, for example set the striping of OSTs wide.
Then you can execute one of these scripts and the new files will be placed in the current directory. We recommend just running the trim_fastq.sh script as
overlapping read mates are automatically detected and merged/corrected within the standard HipMer workflow.

```
contrib/trim_fastq.sh <reads.fastq>[.gz] [ read2.fastq[.gz] ]

This script will perform some basic trimming on the input fastq to remove
illumina sequencing artifacts and adaptors.

If using one of the merge scripts, and the data is paired, any overlapping reads will be merged into a single read
and two files will be output *-bbqc-merged.fq and *-bbqc-pairs.fq

Otherwise a single uncompressed file will be output to the current directory as : *-bbqc.fq

You may want to set TMPDIR to a place with enough disk to manipulate your FASTQ files
```


