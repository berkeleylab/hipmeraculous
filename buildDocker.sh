#/bin/bash
set -e
set -x

DOCKER_USER=${DOCKER_USER:=}
if [ "${DEBUG}" == "1" ]
then
  debug="-debug"
  DOCKER_USER=
else
  debug=
fi

docker build -f Docker/Dockerfile-openmpi -t openmpi . 
[ -z "${DOCKER_USER}" ] || ( docker tag openmpi $DOCKER_USER/openmpi:latest && docker push $DOCKER_USER/openmpi:latest  )

docker build -f Docker/Dockerfile-bupc${debug} -t bupc${debug} .
[ -z "${DOCKER_USER}" ] || ( docker tag bupc $DOCKER_USER/bupc:latest && docker push $DOCKER_USER/bupc:latest  )

docker build -f Docker/Dockerfile-hipmer${debug} -t hipmer${debug} .
#[ -z "${DOCKER_USER}" ] || ( docker tag hipmer $DOCKER_USER/hipmer:latest && docker push $DOCKER_USER/hipmer:latest )

