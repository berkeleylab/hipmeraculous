#!/bin/bash 

module rm PrgEnv-intel
module load PrgEnv-gnu
module load git
module load cmake
module swap gcc/7.1.0
module load bupc-narrow/2.28.0

# Fixme get an edison module that agrees on the gcc of bupc and upcxx
#module load upcxx
export HIPMER_NO_CGRAPH=1


module list

export HIPMER_ENV=edison-gnu
export HIPMER_BUILD_TYPE="Release"
export MPIRUN=$(which srun)
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)

export CORES_PER_NODE=${CORES_PER_NODE:=24}
export HYPERTHREADS=${HYPERTHREADS:=1}
export PHYS_MEM_MB=${PHYS_MEM_MB:=100000}
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=1500}

export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}
export HIPMER_NO_UNIT_TESTS=1
export USE_SBCAST=1
export HIPMER_NO_AVX512F=1
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export HIPMER_HMMER_CONFIGURE_OPTS=--host=x86_64
export MPICH_GNI_NDREG_ENTRIES=1024
export HIPMER_CRAY_MPI_HACK=1

# files to copy to HIPMER_INSTALL
export HIPMER_BIN_SCRIPTS="sbatch_edison.sh"
