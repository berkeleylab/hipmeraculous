#!/bin/bash
#SBATCH --partition=regular
#SBATCH --qos=premium
#SBATCH --nodes=240
#SBATCH --ntasks-per-node=24
#SBATCH --time=02:20:00
#SBATCH --job-name=HipMerWheat

export UFX_HLL=1
export USE_SHM=1
export CORES_PER_NODE=24
N=${N:=${SLURM_NTASKS}}

set -x
set -e

LOBLOLLY_DATA=/scratch1/scratchdirs/regan/PineLoblolly/
HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-edison}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/run_hipmer.sh ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  module list || /bin/true

  export THREADS=${THREADS:=${N}}
  export RUNDIR=${RUNDIR:=$SCRATCH/loblolly-${THREADS}-${SLURM_JOB_ID}-$(date '+%Y%m%d_%H%M%S')}
  echo "Preparing ${RUNDIR}"
  mkdir $RUNDIR
  # to improve IO performance on edison set stripe to max on scratch
  lfs setstripe -c -1 ${RUNDIR}
  echo "Setting stripe to maximum for $RUNDIR"

  mkdir $RUNDIR/assem_data
  for i in ${LOBLOLLY_DATA}/assem_data/* ; do ln -s $i $RUNDIR/assem_data ; done
  
  cp ${LOBLOLLY_DATA}/PineLoblolly-61.config $RUNDIR/loblolly.config
  
  if [ "$KMER_LEN" ]; then
      # if kmer len is set, modify the config file appropriately
      echo "Running kmer length $KMER_LEN"
      grep -v "mer_size" ${RUNDIR}/loblolly.config > ${RUNDIR}/tmp.config
      echo "mer_size $KMER_LEN" >> ${RUNDIR}/tmp.config
      mv ${RUNDIR}/tmp.config ${RUNDIR}/loblolly.config 
  fi

  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=1000}
  MPIRUN="srun -n" UPCRUN="upcrun -q -n" UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB} ${INST}/bin/run_hipmer.sh $INST ${RUNDIR}/loblolly.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi


