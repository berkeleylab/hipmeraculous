#!/bin/bash

set -e
myjobs=${USER}.$$.jobs
if .misc_deploy/build_envs.sh install .edison_deploy/env-debug.sh .edison_deploy/env.sh
then
  if /bin/true
  then
    inst=$SCRATCH/hipmer-install-edison-debug/bin
    AUTORESTART=0 CACHED_IO=0 sbatch --parsable --job-name="Hipmer-debug-validation" --nodes=2 --qos=debug --account=m2865 ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh validation-mg
    inst=$SCRATCH/hipmer-install-edison/bin
    AUTORESTART=0 CACHED_IO=0 sbatch --parsable --job-name="Hipmer-validation"      --nodes=2  --qos=debug  --account=m2865   ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh validation validation-mg_cgraph validation2D-diploid
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-ecoli"           --nodes=8  --qos=debug  --account=m2865   ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh ecoli
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-chr14-benchmark" --nodes=16 --qos=debug  --account=m2865   ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh chr14-benchmark
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-mg250"           --nodes=128 --qos=regular --account=m2865 ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh mg250-short
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --job-name="Hipmer-human-benchmark" --nodes=256 --qos=regular --account=m2865  ${inst}/sbatch_edison.sh ${inst}/test_hipmer.sh human-benchmark
  fi | tee ${myjobs}
else
  echo "FAILED TO BUILD!!" 1>&2
  exit 1
fi

echo $(cat ${myjobs})
echo less $(
for j in $(cat ${myjobs})
do
  echo slurm-${j}.out
done)
rm $myjobs
