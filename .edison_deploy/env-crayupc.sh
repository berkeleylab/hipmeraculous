#!/bin/bash -l

module rm PrgEnv-intel
module load git
module load PrgEnv-cray
module load darshan
module load cmake
module load perl
module load boost

module list

export HIPMER_ENV=edison-cray-upc
export MPIRUN=$(which srun)
export HIPMER_UPCC="$(which cc)"
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export UPCC=""
export UPCRUN=srun

export HIPMER_CXX11_FLAG="-h std=c++11"
export HIPMER_FULL_BUILD=0

#export HIPMER_BUILD_OPTS="-DCMAKE_UPC_COMPILER_INIT='$(which cc) -h upc'"

export CORES_PER_NODE=${CORES_PER_NODE:=24}
export HYPERTHREADS=${HYPERTHREADS:=1}
export PHYS_MEM_MB=${PHYS_MEM_MB:=100000}
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=1500}
export USE_SBCAST=1
export HIPMER_NO_AVX512F=1
export HIPMER_FULL_BUILD=0
export HIPMER_NO_UNIT_TESTS=1
export HIPMER_BIN_SCRIPTS="sbatch_edison.sh"
export HIPMER_CRAY_MPI_HACK=1
export HIPMER_NO_CGRAPH=1

export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
