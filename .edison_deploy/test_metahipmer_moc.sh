#!/bin/bash
#SBATCH --partition=debug
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=24
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer

export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
N=${N:=${SLURM_NTASKS}}


HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-edison}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/run_hipmer.sh ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  module list || /bin/true

  export THREADS=${THREADS:=${N}}
  export RUNDIR=${RUNDIR:=$SCRATCH/metaHipmer-${THREADS}-${SLURM_JOB_ID}-$(date '+%Y%m%d_%H%M%S')}
  echo "Preparing ${RUNDIR}"
  mkdir -p $RUNDIR
  # to improve IO performance on edison set stripe to max on scratch
  lfs setstripe -c -1 ${RUNDIR}
  echo "Setting stripe to maximum for $RUNDIR"

  HIPMER_METAG_DATA=${SCRATCH}/hipmer_metagenome_data
  for i in ${HIPMER_METAG_DATA}/* ; do ln -s $i $RUNDIR ; done
  export ILLUMINA_VERSION=18

  module load bupc-narrow
  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=1500}
  cp -p ${INST}//etc/meraculous/pipeline/meraculous-metagenome.config $RUNDIR

  MPIRUN="srun -n" UPCRUN="upcrun -q -n" UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB} ${INST}/bin/run_MetaHipmerSimple.sh $INST ${RUNDIR}/meraculous-metagenome.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi

