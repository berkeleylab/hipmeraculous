#!/bin/bash

BASE=${0%run_tests.sh}

set -e
./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=${BASE}env.sh install
./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=${BASE}env-debug.sh install

AUTORESTART=0 sbatch ${BASE}sbatch_lr3.sh $SCRATCH/hipmer-install-lawrencium-debug/bin/test_hipmer.sh validation
AUTORESTART=0 sbatch ${BASE}sbatch_lr3.sh ./bootstrap_hipmer_env.sh test_hipmer.sh validation
AUTORESTART=0 sbatch --ntasks 48 ${BASE}sbatch_lr3.sh ./bootstrap_hipmer_env.sh test_hipmer.sh ecoli
AUTORESTART=0 CACHED_IO=1 sbatch --ntasks $((16*24)) --qos=lr_normal ${BASE}sbatch_lr4.sh ./bootstrap_hipmer_env.sh test_hipmer.sh chr14-benchmark
AUTORESTART=0 CACHED_IO=1 sbatch --ntasks $((16*24)) --qos=lr_normal ${BASE}sbatch_lr4.sh ./bootstrap_hipmer_env.sh test_hipmer.sh mg250
