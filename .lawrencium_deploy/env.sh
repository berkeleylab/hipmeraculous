#!/bin/bash 

module purge
module load gcc/7.4.0
module load cmake
module load git
module load openmpi

export PATH=/global/home/users/rsegan/install-mpi/bin:$PATH

module list

export HIPMER_BUILD_TYPE="Release"
export HIPMER_ENV=lawrencium
export UPCXX_GASNET_CONDUIT=ibv

export NO_SLURM_BCAST=1
export GASNET_BACKTRACE=1
