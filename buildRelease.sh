#!/bin/bash
set -e
set -o pipefail
set -x

if [ -z "$HIPMER_VERSION" ]
then
  HIPMER_VERSION=$(./build/makeVersionFile/get_version.sh | sed 's/ .*//')
fi
MYCODE=$(pwd)

cd ${TMPDIR:=/tmp}
buildname=HipMer-${HIPMER_VERSION}
echo "Building ${buildname}"
rm -rf ${buildname}

git clone --recurse-submodules ${MYCODE} ${buildname}
rm -rf ${buildname}/.git
tar=${MYCODE}/opt/${buildname}.tar.gz
tar -czf ${tar} ${buildname}
set +x

rm -rf ${buildname}
echo "Release is in ${tar}"

