module rm PrgEnv-cray
module load PrgEnv-gnu
module load cmake

export HIPMER_ENV=swan-gnu

export PATH=$HOME/install/bin:$PATH

export MPIRUN=$(which aprun)
export CC="$(which cc) -fPIE"
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_NO_UNIT_TESTS=1
# TODO support full build... upc_canonical_assembly links poorly static libc.a & libpthread.d both define  " multiple definition of `__lll_unlock_wake_private'"
export HIPMER_FULL_BUILD=0


