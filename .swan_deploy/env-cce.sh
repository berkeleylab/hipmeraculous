where module load cmake
 
export HIPMER_ENV=cce
export PATH=$HOME/install/opt_cupc2c/bin:$PATH
export MPIRUN=$(which aprun)
export CC="$(which cc)"
export CXX="$(which CC)"
export MPICC=$(which cc)
export MPICXX="$(which CC)"
export HIPMER_UPCC="$(which cc)"
export UPCC="${HIPMER_UPCC}"
export HIPMER_CXX11_FLAG="-h std=c++11"
export HIPMER_FULL_BUILD=1
export HIPMER_NO_UNIT_TESTS=1

