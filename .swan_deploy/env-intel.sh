module rm PrgEnv-cray
module load PrgEnv-intel
module load cmake/3.0.1

export HIPMER_ENV=swan-intel

export PATH=$HOME/install/bin:$PATH

export MPIRUN=$(which aprun)
export CC="$(which cc)  -fPIE"
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
#export HIPMER_FULL_BUILD=1


