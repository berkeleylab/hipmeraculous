module rm PrgEnv-cray
module load PrgEnv-gnu
module load cmake/3.0.1

export HIPMER_ENV=swan-gnu-debug

export PATH=$HOME/install/bin:$PATH

export MPIRUN=$(which aprun)
export CC="$(which cc) -fPIE"
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE=Debug
# TODO support full build... upc_canonical_assembly links poorly static libc.a & libpthread.d both define  " multiple definition of `__lll_unlock_wake_private'"
#export HIPMER_FULL_BUILD=1


