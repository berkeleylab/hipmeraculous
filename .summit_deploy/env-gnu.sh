module use /gpfs/alpine/world-shared/csc296/summit/modulefiles

module load gcc/9.1.0
module load git/2.20.1
module load cmake/3.15.2
module load python

module load upcxx-bupc-narrow

# cmake  -DCMAKE_UPC_COMPILER=$(which upcc) -DCMAKE_C_COMPILER=$(which mpicc) -DCMAKE_CXX_COMPILER=$(which mpicxx)

# memory paramter for at-scale ~2500 knl nodes
export GASNET_COLL_SCRATCH_SIZE=4M
