# ChangeLog.md for MetaHipMer

This is the ChangeLog for MetaHipMer with official releases on [SourceForge](https://sourceforge.net/projects/hipmer/)

and git development on [bitbucket](https://bitbucket.org/berkeleylab/hipmeraculous)

### 1.2.2 : 2020-06-30
   * Greatly improved quality of the assembly as reflected in our publication in Scientific Reports 2020-07-01
   * Requires UPC and UPC++ version >= 2020.03 and C++17 compiler support
   * Improved scaffolding performance
   * Incorporated as a submodule the [upcxx_utils](https://bitbucket.org/berkeleylab/upcxx-utils/src/master/) library, replacing AggrStore and other utilities in the UPC++ modules
   * Tuned GASNET memory parameters to address Out of Memory errors when executing at scales above 1200 KNL nodes
   * Improved CMake build and compatibility with old and new versions, including support for UPC++ in CMake 
   * Added support for Theta at ALCF
   * Upgraded vectorization and compile options for better Power9 support and execution on Summit at OLCF
   * Fixed restart behavior to recover better after a failed run
   * Upgraded test infrastructure and automated tests
   * Fixed a bug in bubblefinder resulting in empty diplotigs
   * Various other bug fixes

### 1.2.1 : 2019-09-27
   * Supports 2019.09 version of UPC and UPC++ with fixes that avoid some Out of Memory issues
   * Improved the performance of kmer counting especially at scale by increasing message sizes and reducing message counts between nodes
   * Improved stability and warnings with various user input data, such as missing insert size estimation
   * Various other bug fixes

### 1.2.0 : 2019-08-23
   * Improvements to scaffolding algorithm for both quality and performance
   * Fixed bugs in restart and also with some automated tests
   * Incorporated new, faster UPC++ based kmer count module
   * Added support for paired read files with different naming structure
   * upgraded wrapper script to python3
   * Various other bug fixes

### 1.1 : 2019-06-06
   * Greatly improved alignment speeds especially on SMP and when k is smaller than used for contigs
   * Improved sensitivity to span size in ono
   * Improved efficiency in gap closing
   * Improved memory usage in ParCC
   * Improved scaffold reproducibility in ono
   * Implemented efficient parallel I/O especially with checkpoints
   * Improved support for long reads, including PacBio CCS
   * Various other bug fixes

### 1.0 : 2018-12-15
   * Stability and User Interface improvements for first official release
   * Many improvements to testing and validation and Continuous Integration
   * Improved building script and CMake support
   * Various other bug fixes

### 0.9.6.5.1 : 2018-09-14
   * Improved cross platform builds
   * Fixed atomic operations to build on all platforms
   * Fixed scaling of LocalizeReads module
   * Added HMM option to ParCC / Ono scaffolding module
   * Various other bug fixes

### 0.9.6 : 2017-09-29
   * Major refactor of memory allocation and squashing of leaks
   * Fixed several crashes
   * Various other bug fixes
   





