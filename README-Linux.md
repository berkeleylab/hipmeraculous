# Configuring your Ubuntu 16.04 or 18.04 to compile and build the UPC and UPC++ prerequisites

## Install required packages

(open your terminal)

```
sudo apt-get install build-essential cmake git 
```

---------

## Install UPC and UPC++

### Ubuntu 16.04  (not necessary in 18.04)

#### install a modern version of gcc

```
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install gcc-9 g++-9
```

#### Build and install Berkeley UPC, UPC++ and the Clang upc translator using gcc-9

This script calls contrib/install_upc.sh using gcc-9 installed above

```
.generic_deploy/build_latest_ubuntu_gcc-9.sh [install_path=$HOME/install-gcc-9]
```

### Ubuntu >=18.04

#### Build and install Berkeley UPC, UPC++ and the Clang upc translator using gcc-9

If you have a different networking layer like infiniband, replace smp with ivb...

```
contrib/install_upc.sh contrib/install_upc.sh smp posix [install_path=$HOME/install]
```

---------

## build and install Hipmer

Important!  The path to upcc, upcxx, etc installed above must be in your PATH environment variable!

```
ln -s .generic_deploy/env.sh hipmer_env.sh
./bootstrap_hipmer_env.sh install
```

Alternatively one can expertly customize a build with cmake options

```
mkdir build
cd build
cmake <cmake_options> .. && make -j16 && make install
```


