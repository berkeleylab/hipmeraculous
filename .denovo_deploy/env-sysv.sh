#!/bin/bash 


module purge
module load modules nsg slurm
module load PrgEnv-gnu/7.1
module load git
module load cmake
module load OFED/4.0-2.0.0.1-Mellanox
module load openmpi/4.0.0
module load gnuplot
module load zlib

export HIPMER_BUILD_OPTS="-DZLIB_ROOT=$ZLIB_DIR"

module use /usr/common/ftg/modulefiles

#module load upcxx/2018.9.6-snapshot-2018.12.2-gnu-7.1
#module load bupc/2018.12.2-snapshot-2018.12.2-gnu-7.1
module load upcxx-bupc/snapshot-2019-02-08-gex-2018.12.2
export UPCXX_GASNET_CONDUIT=ibv
export GASNET_IBV_SPAWNER=mpi

module list

export HIPMER_ENV=denovo-sysv
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=1}

# (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS=sbatch_denovo.sh


#export HIPMER_NO_UNIT_TESTS=1

