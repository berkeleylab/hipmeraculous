#!/bin/bash 


module purge
module load modules nsg slurm
module load PrgEnv-gnu
module load cmake
module rm OFED
module load openmpi
module load gnuplot
module load zlib
export HIPMER_BUILD_OPTS="-DZLIB_ROOT=$ZLIB_DIR"

module use ~regan/modulefiles-denovo
module load bupc/2.26.0-gnu-7.1-sysv
module load upcxx

module list

export HIPMER_ENV=denovo-sysv-tcp
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BIN_SCRIPTS=sbatch_denovo.sh

export HIPMER_NO_UNIT_TESTS=1

