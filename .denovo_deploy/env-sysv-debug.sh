#!/bin/bash 


module purge
module load modules nsg slurm
module load PrgEnv-gnu
module load git
module load cmake
module load OFED/4.0-2.0.0.1-Mellanox
module load openmpi/4.0.0
module load gnuplot
module load zlib
export HIPMER_BUILD_OPTS="-DZLIB_ROOT=$ZLIB_DIR"

module use ~regan/modulefiles-denovo
module load bupc/2.28.0-4.0.0-gnu-7.1-sysv-ibv
module load upcxx

module list

export HIPMER_ENV=denovo-sysv-debug
export HIPMER_BUILD_TYPE="Debug"
# (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS=sbatch_denovo.sh
export GASNET_BACKTRACE=1
export AUTORESTART=0

export HIPMER_NO_UNIT_TESTS=1

