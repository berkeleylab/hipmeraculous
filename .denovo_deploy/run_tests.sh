#!/bin/bash

export TMPDIR=/scratch/tmp

set -e

if ! .misc_deploy/build_envs.sh install .denovo_deploy/env-sysv.sh .denovo_deploy/env-sysv-debug.sh
then
  echo "Builds failed"
  exit 1
fi

inst=$SCRATCH/hipmer-install-denovo-sysv-debug/bin
log=denovo-tests.$(date '+%Y%m%d_%H%M%S').log
for r in run_hipmer_unified.sh 
do
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=0 sbatch -N 1 $inst/sbatch_denovo.sh $inst/test_hipmer.sh
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=0 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh validation-mg
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh validation2D-diploid
done 2>&1 | tee $log

inst=$SCRATCH/hipmer-install-denovo-sysv/bin
for r in run_hipmer_unified.sh
do
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh validation-mg
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh validation-mg_par_hmm
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 2 $inst/sbatch_denovo.sh $inst/test_hipmer.sh ecoli
  AUTORESTART=0 RUNSCRIPT=$r CACHED_IO=1 sbatch -N 8 $inst/sbatch_denovo.sh $inst/test_hipmer.sh chr14-benchmark
done 2>&1 | tee -a $log


