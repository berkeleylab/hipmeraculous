# Test harness configuration file

# ------------------------------------------------------------
# UPC Atomic Memory Operations (AMO) Library.
# ------------------------------------------------------------
BEGIN_DEFAULT_CONFIG
Files:          $TOP_SRCDIR$/upc-tests/spec1.3/$TESTNAME$.upc upc_atomic.upc
Flags:		-I. -D__UPC_ATOMIC__=1 -DBUPC_TEST_HARNESS
DynamicThreads: $DEFAULT$            # compile for dynamic threads
StaticThreads:  $DEFAULT$            # compile for static threads
CompileResult:  pass                 # expect compilation to succeed
TimeLimit:      $DEFAULT$            # run the test
PassExpr:       pass|SUCCESS         # pass expression
FailExpr:       (error|Error|ERROR): # fail expression
ExitCode:       0	             # passing exit code
BuildCmd:       upcc                 # use upcc to build code
AppArgs:                             # no args to run app
END_DEFAULT_CONFIG

TestName:	atomic_test_simple
Files:		atomic_test_simple.upc upc_atomic.upc

TestName:	atomic_test
Flags:		-lm
Flags:		driver_upcc && cc_xlc && nodebug ; -Wc,-qstrict=ieeefp # For IEEE infinities
Flags:		driver_ibm ; -qstrict=ieeefp # For IEEE infinities
TimeLimit:      $DEFAULT$ * ( 1 + $THREADS$ )
AppArgs:        8192
WarningFilter:  cc_cray; CC-7227 craycc: WARNING .*? # constant out of range (INFINITY)
WarningFilter:  cc_cray; CC-1227 craycc: WARNING .*? # constant expression overflows...
# Clang dislikes a code transformation the BUPC translator performs w/ -opt:
WarningFilter:  cc_clang; .*? warning: explicitly assigning a variable of type .*? to itself.*?
#KnownFailure:   run-crash; !trans_opt && cc_sun && nodebug ; Bug 3230 - Translator illegally reassociates signed integer arithmetic
#KnownFailure:   run-limit; cc_xlc && nodebug ; XLC -O3 permits non-IEEE handling of Infinities

TestName:	atomic_neg_arith
RunResult:      fail # want (FailExpr || !PassExpr || !ExitCods)
ProhibitFeature: nodebug # this test has undefined runtime semantics, so the ndebug result is meaningless

TestName:	atomic_neg_bitwise
RunResult:      fail # want (FailExpr || !PassExpr || !ExitCods)
# Bug 3755 - atomic_neg_bitwise exit hangs on Cygwin/clang/pshm leaves zombies
# So, prohibit running on that specific configuration
ProhibitFeature: (os_cygwin && cc_clang && pshm) || nodebug # this test has undefined runtime semantics, so the ndebug result is meaningless

TestName:	atomic_neg_pts
RunResult:      fail # want (FailExpr || !PassExpr || !ExitCods)
ProhibitFeature: nodebug # this test has undefined runtime semantics, so the ndebug result is meaningless

TestName:	atomic_r_access_pts

TestName:	atomic_r_access

TestName:	atomic_r_arith
#KnownFailure:  run-match ; (_threads > 3) ; Bug 3221 - failures at high thread counts (bad test)
#KnownFailure:   run-match ; (_threads > 1) && cc_xlc && nodebug ; Bug 3224 - failure on BG/Q xlc

TestName:	atomic_r_bitwise

TestName:	atomic_s_access_pts

TestName:	atomic_s_access

TestName:	atomic_s_arith
#KnownFailure:  run-match ; (_threads > 3) ; Bug 3221 - failures at high thread counts (bad test)
#KnownFailure:   run-match ; (_threads > 1) && cc_xlc && nodebug ; Bug 3224 - failure on BG/Q xlc

TestName:	atomic_s_bitwise

TestName:	atomic_err
TimeLimit: 	0 # don't bother running by default since any behavior is acceptable

TestName:	atomic_cswap_pts

TestName: atomicperf
Files:          $TOP_SRCDIR$/upc-tests/benchmarks/$TESTNAME$.upc upc_atomic.upc
SaveOutput: 	1
PassExpr: 	^done
TimeLimit:      4 * $DEFAULT$

