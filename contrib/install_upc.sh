#!/bin/bash

# set UPC versions
CUPCVER=9.0.1-2

# comment these out to use the non-hipmer specific versions of upcxx and Berkeley UPC
# for compatiblity of bupc and upc++, consult https://bitbucket.org/berkeleylab/upcxx/wiki/GASNet-EX%20Version%20Table

BUPCVER=${BUPCVER:=latest}
UPCXXVER=${UPCXXVER:=latest}

# The 2022-12 versions of bupc and upcxx
# BUPCVER=${BUPCVER:=2022.10.0}
# UPCXXVER=${UPCXXVER:=2022.9.0}

# The 2021-12-10 versions of bupc and upcxx
# BUPCVER=${BUPCVER:=2021.12.0}
# UPCXXVER=${UPCXXVER:=2021.9.0}
#
# NOTE for older systems with g++ < 6.4.0, you can use an older version of upc and upc++
# BUPCVER=2.28.0
# UPCXXVER=2018.9.0

getcores()
{
  if lscpu
  then
     :
  fi 2>/dev/null | awk '/^CPU\(s\):/ {print $2}'
  if sysctl -a
  then
     :
  fi 2>/dev/null | awk '/^machdep.cpu.core_count/ {print $2}'
}
BUILD_THREADS=${BUILD_THREADS:=$(getcores)}

conduit=$1
shift
if [ -n "$1" ]
then
  shm=$1
else
  shm=posix
fi

if [ -n "$2" ]
then
  installdir=${2}
else
  installdir=$HOME/install
fi

if [ -n "$3" ]
then
  builddir=${3}
else
  builddir=${TMPDIR:=/dev/shm}
fi
[ -d "$builddir" ] && [ -w "$builddir" ] || builddir=/tmp

if [ -n "$4" ]
then
  codedir=${4}
else
  codedir=$HOME
fi

CC=${CC:=$(which gcc)}
CXX=${CXX:=$(which g++)}

[ -d $builddir ] || builddir=/tmp

USAGE="$0 Conduit(smp|ibv|ofi|aries|mpi|udp) [ SHARED_MEMORY(posix|xpmem|sysv|file|none) [ INSTALL_DIR($installdir) [ BUILD_DIR($builddir) [ CODE_DIR(${codedir}) ] ] ] ]"

bupc_opts=${bupc_opts:=}

if [ -z "$conduit" ] 
then
  echo $USAGE
  echo "You must choose a default networking conduit: smp, ibv, ofi, aries, mpi or udp"
  echo
  exit 0
fi

oops()
{
  echo "uh oh, something bad happened!"
  exit 1
}

echo "Building bupc and upcxx, conduit=$conduit shared_memory=$shm installdir=$installdir builddir=$builddir codedir=$codedir"

trap oops 0

set -e
set -x


cd $codedir
builddir=$builddir/bupc-$USER-hipmer-builds
rm -rf ${builddir}
mkdir -p ${builddir}

# install clang-upc and clang-upc2c
if [ ! -x $installdir/bin/clang-upc2c ]
then
   echo "Building Clang UPC to C translator"
   cd $codedir
   CUPCTAR=clang-upc2c-${CUPCVER}.tar.gz
   CUPCURL=https://github.com/clangupc/upc2c/releases/download/clang-upc2c-${CUPCVER}/${CUPCTAR}
   [ -f $CUPCTAR ] || curl -LO $CUPCURL
   CUPCDIR=clang-upc2c-${CUPCVER}
   cd $builddir
   [ -d $CUPCDIR ] || tar -xzf $codedir/$CUPCTAR

   #CUPC2C_OPTS="-DCMAKE_INSTALL_PREFIX:PATH=$installdir -DLLVM_TARGETS_TO_BUILD:=host -DCMAKE_BUILD_TYPE:=Release -DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX}"
   CUPC2C_OPTS="-DCMAKE_INSTALL_PREFIX:PATH=$installdir -DLLVM_TARGETS_TO_BUILD:=host -DCMAKE_BUILD_TYPE:=Release"
   if [ "$(uname -s)" == "Darwin" ] && [ ! -d /usr/include ]
   then
     CUPC2C_OPTS="$CUPC2C_OPTS -DDEFAULT_SYSROOT=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk"
   fi

   rm -rf $builddir/build_clangupc
   mkdir -p $builddir/build_clangupc
   cd $builddir/build_clangupc
   # build clang with gcc if available
   if [ -n "$CUPC_CC" -a -n "$CUPC_CXX" ]
   then
     CC=$CUPC_CC CXX=$CUPC_CXX cmake $builddir/$CUPCDIR $CUPC2C_OPTS
   elif [ -z "$CC" -o -z "$CXX" ] && [ -x $(which gcc) ]
   then
     CC=gcc CXX=g++ cmake $builddir/$CUPCDIR $CUPC2C_OPTS
   else
     CC=$CC CXX=$CXX cmake $builddir/$CUPCDIR $CUPC2C_OPTS
   fi
   make -j 12 || make -j 1 VERBOSE=1
   make install
else
   echo "Clang UPC to C is already installed: $installdir/bin/clang-upc2c"
fi

if [ ! -x $installdir/bin/upcxx ]
then

  upcxx_opts=${upcxx_opts:=}
  echo "Building UPC++ -- upcxx_opts=${upcxx_opts}"
  # build upcxx
  cd $codedir

  UPCXXDIR=upcxx-${UPCXXVER}
  UPCXXTAR=${UPCXXDIR}.tar.gz
  UPCXXURLBASE=https://bitbucket.org/berkeleylab/upcxx/downloads
  if [ "$UPCXXVER" == "latest" ]
  then
    UPCXXURLBASE=https://upcxx.lbl.gov/third-party/hipmer
  fi


  rm -f $UPCXXTAR
  [ -f $UPCXXTAR ] || curl -LO $UPCXXURLBASE/${UPCXXTAR}
  cd $builddir
  [ -d ${UPCXXDIR} ] || tar -xvzf $codedir/${UPCXXTAR}
  [ -d ${UPCXXDIR} ] || mv upcxx-*/ ${UPCXXDIR}

  
  cd ${UPCXXDIR}
  if [ -x install ]
  then
    ./install $installdir
  else
    CC=${CC:=gcc} CXX=${CXX:=g++} ./configure --prefix=$installdir --with-default-network=$conduit ${upcxx_opts} ${with_mpicxx}
    make -j ${BUILD_THREADS} || make
    make install
  fi
  ${installdir}/bin/upcxx -version
else
  echo "upcxx is already installed $installdir/bin/upcxx"
fi

echo "Building Berkeley UPC"

# build berkeley upc

cd $codedir

build_debug=${build_debug:=1}
multiconf=
if [ "${build_debug}" == "0" ]
then
  multiconf="-dbg,-dbg_tv,-dbg_gupc,+opt_cupc2c"
else
  multiconf="+dbg_cupc2c,+opt_cupc2c"
fi


# install berkeley upc (with -cupc2c support)

cd $codedir

DIR=berkeley_upc-${BUPCVER}

TAR=$DIR.tar.gz
BUPCURLBASE=https://upc.lbl.gov/download/release
if [ "$BUPCVER" == "latest" ]
then
  BUPCURLBASE=https://upcxx.lbl.gov/third-party/hipmer
fi

rm -f $TAR
[ -f $TAR ] || curl -LO $BUPCURLBASE/$TAR

cd $builddir
[ -d $DIR ] || tar -xvzf $codedir/berkeley_upc-${BUPCVER}.tar.gz
[ -d $DIR ] || mv berkeley_upc-*/ ${DIR}
cd $DIR


edison_config="
 Configured with      | '--with-translator=/usr/common/ftg/upc/2.22.3/translato
                      | r/install/targ' '--enable-sptr-struct'
                      | '--disable-sptr-struct' '--with-sptr-packed-bits=10,18,
                      | 36' '--enable-cross-compile'
                      | '--host=x86_64-unknown-linux-gnu'
                      | '--build=x86_64-cnl-linux-gnu'
                      | '--target=x86_64-cnl-linux-gnu' '--program-prefix='
                      | '--disable-auto-conduit-detect' '--enable-mpi'
                      | '--enable-smp' '--disable-aligned-segments'
                      | '--enable-pshm' '--disable-pshm-posix'
                      | '--enable-pshm-xpmem' '--enable-throttle-poll'
                      | '--with-feature-list=os_cnl'
                      | '--enable-backtrace-execinfo' '--enable-aries'
                      | '--with-multiconf-file=multiconf_nersc.conf.in'
                      | '--with-multiconf=+opt,+dbg,+opt_trace,+opt_inst,
                      | +dbg_tv,+dbg_cupc2c,+opt_cupc2c,+opt_cupc2c_stats,
                      | +opt_cupc2c_trace,+opt_stats'
                      | '--prefix=/usr/common/ftg/upc/2.22.3/PrgEnv-intel-5.2.5
                      | 6-15.0.1.133-narrow/runtime/inst/opt'
                      | '--with-multiconf-magic=opt'
                      | 'build_alias=x86_64-cnl-linux-gnu'
                      | 'host_alias=x86_64-unknown-linux-gnu'
                      | 'target_alias=x86_64-cnl-linux-gnu' 'CC=cc -O0
                      | -wd10120'
----------------------+---------------------------------------------------------
 Configure features   | trans_bupc,pragma_upc_code,driver_upcc,runtime_upcr,
                      | gasnet,upc_collective,upc_io,upc_memcpy_async,
                      | upc_memcpy_vis,upc_ptradd,upc_thread_distance,upc_tick,
                      | upc_sem,upc_dump_shared,upc_trace_printf,
                      | upc_trace_mask,upc_local_to_shared,upc_all_free,pupc,
                      | upc_types,upc_castable,upc_nb,nodebug,notrace,nostats,
                      | nodebugmalloc,nogasp,nothrille,segment_fast,os_linux,
                      | cpu_x86_64,cpu_64,cc_intel,packedsptr,upc_io_64,os_cnl
"

rm -rf $builddir/build-bupc
mkdir -p $builddir/build-bupc
cd $builddir/build-bupc

SHM=
if [ "$shm" == "none" ]
then
  SHM="--disable-pshm --disable-aligned-segments"
elif [ "$shm" == "posix" ]
then
  SHM="--enable-pshm --enable-pshm-posix"
elif [ "$shm" == "sysv" ]
then
  SHM="--enable-pshm --disable-pshm-posix --enable-pshm-sysv"
elif [ "$shm" == "file" ]
then
  SHM="--enable-pshm --disable-pshm-posix --enable-pshm-file"
elif [ "$shm" == "xpmem" ]
then
  SHM="--enable-pshm --disable-pshm-posix --enable-pshm-xpmem"
fi

useconduit=""
if [ "$conduit" == 'ibv' ]
then
  useconduit="--enable-ibv"
elif [ "$conduit" == "ofi" ]
then
  useconduit="--enable-ofi"
elif [ "$conduit" == 'aries' ]
then
  useconduit="--enable-aries"
elif [ "$conduit" == 'mpi' ]
then
  useconduit="--enable-mpi"
elif [ "$conduit" == 'smp' ]
then
  useconduit="--enable-smp"
fi

if [ -n "${useconduit}" ]
then
  useconduit="--disable-auto-conduit-detect ${useconduit} "
fi

if [ "$conduit" == 'aries' ] ## Cray specific cross compile settings
then

   echo "Building for aries -- bupc_opts=$bupc_opts"
   # Whether the system has a working version of anonymous mmap
   CROSS_HAVE_MMAP='1' ; export CROSS_HAVE_MMAP

   # The system VM page size (ie mmap granularity, even if swapping is not supported)
   CROSS_PAGESIZE='4096' ; export CROSS_PAGESIZE

   # Does the system stack grow up?
   CROSS_STACK_GROWS_UP='0' ; export CROSS_STACK_GROWS_UP

   # Is char a signed type?
   CROSS_CHAR_IS_SIGNED='1' ; export CROSS_CHAR_IS_SIGNED

   # Basic primitive C type sizes (in bytes)
   CROSS_SIZEOF_CHAR='1' ; export CROSS_SIZEOF_CHAR
   CROSS_SIZEOF_SHORT='2' ; export CROSS_SIZEOF_SHORT
   CROSS_SIZEOF_INT='4' ; export CROSS_SIZEOF_INT
   CROSS_SIZEOF_LONG='8' ; export CROSS_SIZEOF_LONG
   CROSS_SIZEOF_LONG_LONG='8' ; export CROSS_SIZEOF_LONG_LONG
   CROSS_SIZEOF_VOID_P='8' ; export CROSS_SIZEOF_VOID_P
   CROSS_SIZEOF_SIZE_T='8' ; export CROSS_SIZEOF_SIZE_T
   CROSS_SIZEOF_PTRDIFF_T='8' ; export CROSS_SIZEOF_PTRDIFF_T

   # System signal values
   CROSS_SIGHUP='1' ; export CROSS_SIGHUP
   CROSS_SIGINT='2' ; export CROSS_SIGINT
   CROSS_SIGQUIT='3' ; export CROSS_SIGQUIT
   CROSS_SIGKILL='9' ; export CROSS_SIGKILL
   CROSS_SIGTERM='15' ; export CROSS_SIGTERM
   CROSS_SIGUSR1='10' ; export CROSS_SIGUSR1

   $builddir/$DIR/configure CC="${CC}" CXX="${CXX}" MPI_CC="${MPICC}" CUPC2C_TRANS=$installdir/bin/clang-upc2c \
       --enable-cross-compile --host=x86_64-unknown-linux-gnu --build=x86_64-cnl-linux-gnu --target=x86_64-cnl-linux-gnu \
       --program-prefix= --disable-auto-conduit-detect --enable-mpi --enable-smp --enable-aries --disable-aligned-segments \
       --enable-pshm --disable-pshm-posix --enable-pshm-xpmem --enable-throttle-poll --with-feature-list=os_cnl \
       --enable-backtrace-execinfo --prefix=$installdir --enable-aries --enable-mpi --enable-smp --enable-aries \
       --enable-pshm --disable-pshm-posix --enable-pshm-xpmem --enable-throttle-poll \
       --with-multiconf=$multiconf --with-sptr-packed-bits=10,18,36 \
    && (make -j ${BUILD_THREADS} || make) && make install 

else

  echo "Building for $useconduit -- bupc_opts=$bupc_opts"
  $builddir/$DIR/configure CC="${CC}" CXX="${CXX}" CUPC2C_TRANS=$installdir/bin/clang-upc2c --prefix=$installdir \
       --with-multiconf=$multiconf $useconduit --enable-pthreads --enable-udp --enable-smp --with-default-network=$conduit \
       $SHM --enable-sptr-struct --disable-sptr-struct --with-sptr-packed-bits=10,18,36 \
       ${bupc_opts} \
   && (make -j ${BUILD_THREADS} || make) && make install

fi

${installdir}/bin/upcc -version

set -x
trap "" 0

