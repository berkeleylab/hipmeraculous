#!/usr/bin/env perl

my $USAGE = "USAGE: $0 read1.fastq read2.fastq output.fastq\n\n";

if (@ARGV < 3) { die($USAGE); }

my $file = shift;
my $file2 = shift;
my $ofile = shift;

my $tmpfile = $ofile . ".tmpsquish";
my $in, $in2, $out;

open($in, "<", $file) || die("Could not open $file for reading! $!\n");
open($in2, "<", $file2) || die("Could not open $file2 for reading! $!\n");
open($out, ">", $tmpfile) || die("Could not open $tmpfile for writing! $!\n");

my $count = 0;
my $line1, $line2;
while ( $line1 = <$in> ) {
  if ($line1 !~ /^@/) { die("Invalid fastq format in $file1. expected ID line, got $_\n"); }
  $line2 = <$in2>;
  if ($line2 !~ /^@/) { die("Invalid fastq format in $file2. expected ID line, got $_\n"); }
  $count++;
  print $out "\@${count}/1\n";
  $line1 = <$in>; print $out $line1;
  $line1 = <$in>; print $out $line1;
  $line1 = <$in>; print $out $line1;
  print $out "\@${count}/2\n";
  $line2 = <$in2>; print $out $line2;
  $line2 = <$in2>; print $out $line2;
  $line2 = <$in2>; print $out $line2;
}
print "Processed $count reads per file\n";
rename($tmpfile, $ofile) || die ("Could not rename $tmpfile to $ofile! $!\n");
