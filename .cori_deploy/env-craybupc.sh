#!/bin/bash

module rm PrgEnv-intel
module rm PrgEnv-gnu
module rm PrgEnv-cray
module load PrgEnv-cray
module load git
module load darshan
module load cmake

# both of the two below are current workarounds for issues with Cori
#module swap intel/16.0.3.210
module load bupc-narrow/2.26.0

module list

export HIPMER_ENV=cori-craybupc
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BUILD_OPTS=""
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=2500}
export HIPMER_FULL_BUILD=0
export USE_SBCAST=1
export HIPMER_NO_AVX512F=1
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export MPICH_GNI_MALLOC_FALLBACK=enabled
export HUGETLB_MORECORE=no

# files to copy to HIPMER_INSTALL
export HIPMER_BIN_SCRIPTS="sbatch_cori.sh sbatch_cori-jgi.sh"
# copy to HIPMER_INSTALL and use as upcrun -conf=
#export HIPMER_UPCRUN_CONF=upcrun.conf
export MPICH_GNI_NDREG_ENTRIES=1024
