#!/bin/bash

module rm PrgEnv-intel
module rm PrgEnv-gnu
module rm PrgEnv-cray
module load PrgEnv-intel
module load git
module load darshan
module load cmake
module load python
module load bupc
module load upcxx

module list

export HIPMER_ENV=cori-wide
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BUILD_OPTS=""
export UPC_SHARED_HEAP_SIZE=${UPC_SHARED_HEAP_SIZE:=2500}
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}
export USE_SBCAST=1
export HIPMER_NO_AVX512F=1
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export MIN_NODES_FOR_HUGEPAGE_CHECK=${MIN_NODES_FOR_HUGEPAGE_CHECK:=2}
#export UPCRUN="upcrun -v"
export GASNET_BACKTRACE=${GASNET_BACKTRACE:=1}
export AUTORESTART=${AUTORESTART:=0}
export MPICH_GNI_NDREG_ENTRIES=1024
export MPICH_GNI_MALLOC_FALLBACK=enabled
export HUGETLB_MORECORE=no
export HIPMER_BUILD_TEST=1

# files to copy to HIPMER_INSTALL (located in .misc_deploy)
export HIPMER_BIN_SCRIPTS="sbatch_cori.sh sbatch_cori-jgi.sh"
# copy to HIPMER_INSTALL and use as upcrun -conf=
#export HIPMER_UPCRUN_CONF=upcrun.conf
