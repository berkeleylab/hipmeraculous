#!/bin/bash -l

module rm PrgEnv-intel
module rm PrgEnv-gnu
module rm PrgEnv-cray
module load PrgEnv-cray
module load git
module load darshan
module load cmake

export HIPMER_ENV=cori-cray-upc-debug
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
export HIPMER_BUILD_OPTS=""
export HIPMER_BUILD_TYPE="Debug"
#export HIPMER_BUILD_OPTS="-DCMAKE_UPC_COMPILER_INIT='$(which cc)\ -h\ upc' -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CXX"
export UPC_SHARED_HEAP_SIZE=2500
export USE_SBCAST=1
export HIPMER_NO_AVX512F=1
export CHECK_FREE_HUGEPAGES_MB=${CHECK_FREE_HUGEPAGES_MB:=0}
export HIPMER_NO_UNIT_TESTS=1
export HIPMER_FULL_BUILD=0
export HIPMER_BUILD_TEST=1
export UPCRUN=srun
export HIPMER_NO_CGRAPH=1
