#!/bin/bash

set -e

myjobs=/tmp/${USER}.$$
if .misc_deploy/build_envs.sh install .cori_deploy/env-debug.sh .cori_deploy/env.sh
then
  if /bin/true
  then
    inst=$SCRATCH/hipmer-install-cori-debug/bin
    GASNET_BACKTRACE=1 AUTORESTART=0 CACHED_IO=0 sbatch --parsable --qos=debug --account=m2865 --job-name="Hipmer-debug-validation"          --nodes=2 --time=15:00 ${inst}/sbatch_cori.sh ${inst}/test_hipmer.sh validation-par_hmm
    inst=$SCRATCH/hipmer-install-cori/bin
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --qos=debug --account=m2865 --job-name="Hipmer-ecoli"             --time=30:00 --nodes=16 ${inst}/sbatch_cori.sh ${inst}/test_hipmer.sh validation validation-mg_cgraph validation-mg validation2D-diploid ecoli chr14-benchmark
#    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --qos=regular --account=m2865 --job-name="Hipmer-chr14-benchmark"               --nodes=16 ${inst}/sbatch_cori.sh ${inst}/test_hipmer.sh chr14-benchmark
    AUTORESTART=0 CACHED_IO=1 sbatch --parsable --qos=regular --account=m2865 --job-name="Hipmer-mg250"                         --nodes=32 ${inst}/sbatch_cori.sh ${inst}/test_hipmer.sh mg250-short
    #AUTORESTART=0 CACHED_IO=1 sbatch --parsable --qos=regular --account=m2865 --job-name="Hipmer-human-benchmark" --nodes=256 ${inst}/sbatch_cori.sh ${inst}/test_hipmer.sh human-benchmark
  fi | tee ${myjobs}
else
  echo "FAILED TO BUILD!!" 1>&2
  exit 1
fi

echo $(cat ${myjobs})
echo less $(
for j in $(cat ${myjobs})
do
  echo slurm-${j}.out
done)
rm $myjobs
