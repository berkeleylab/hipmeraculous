#!/bin/bash

set -e

myjobs=/tmp/${USER}.$$

if ./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=.cori_deploy/env.sh install 
then
  if /bin/true
  then
    inst=$SCRATCH/hipmer-install-cori/bin
    MIN_NODES=8 AUTORESTART=0 CACHED_IO=1 sbatch --qos=genepool -A gtrnd --parsable --job-name="Hipmer-ecoli"             --time=15:00 --nodes=16   ${inst}/sbatch_cori-jgi.sh ${inst}/test_hipmer.sh ecoli
    MIN_NODES=16 AUTORESTART=0 CACHED_IO=1 sbatch --qos=genepool -A gtrnd --parsable --job-name="Hipmer-chr14-benchmark"                --nodes=24  ${inst}/sbatch_cori-jgi.sh ${inst}/test_hipmer.sh chr14-benchmark
    MIN_NODES=24 MAX_NODES=30 AUTORESTART=0 CACHED_IO=1 sbatch --qos=genepool -A gtrnd --parsable --job-name="Hipmer-mg250"                          --nodes=32 ${inst}/sbatch_cori-jgi.sh ${inst}/test_hipmer.sh mg250-short
    MIN_NODES=32 MAX_NODES=38 AUTORESTART=0 CACHED_IO=1 sbatch --qos=genepool -A gtrnd --parsable --job-name="Hipmer-human-benchmark"   --time=60:00 --nodes=40 ${inst}/sbatch_cori-jgi.sh ${inst}/test_hipmer.sh human-benchmark
  fi | tee -a ${myjobs}
else
  echo "FAILED to build Release!!" 1>&2
  exit 1
fi

echo $(cat ${myjobs})
echo less $(
for j in $(cat ${myjobs})
do
  echo slurm-${j}.out
done)
rm $myjobs
