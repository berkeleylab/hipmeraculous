#!/bin/bash 


export HIPMER_ENV=HipMer-MacOSX-debug
export HIPMER_BUILD_TYPE="Debug"
export HIPMER_BUILD_OPTS="-DCMAKE_C_COMPILER=$(which clang) -DCMAKE_CXX_COMPILER=$(which clang++)"

export HIPMER_NO_UNIT_TESTS=1
export HIPMER_NO_AIO=1
export UPCXX_GASNET_CONDUIT=smp



