# Testing with PyCTest

[PyCTest Documentation](https://pyctest.readthedocs.io)

## Installing PyCTest

- PyCTest is a package for executing testing and submitting to a CDash dashboard
- PyCTest is available from Anaconda (recommended) and PyPi

### Installing PyCTest with Anaconda

Installation from Anaconda is recommended because the Python binding to CMake/CTest are pre-built.

#### Install Miniconda

```console
# Linux
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh;
# MacOSX
wget https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -O miniconda.sh;

bash miniconda.sh -b -p ${HOME}/miniconda
export PATH="${HOME}/miniconda/bin:${PATH}"
```

#### Install PyCTest

```console
conda install -n pyctest -c conda-forge python=3.6 pyctest
```

#### Activate PyCTest

```console
# activate conda
source activate
# activate pyctest environment
conda activate pyctest
```

### Installing PyCTest with PyPi

Installation from PyPi requires building CMake and CTest from scratch and will consequently take a long time to build

```console
pip install -vvv pyctest
```

## PyCTest Example

```console
./pyctest-runner.py -n HipMer-macOS-Test -SF -- -VV -- -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON
```

- Use `./pyctest-runner.py` to view all available options
- The (required) `-n` option is a HipMer option to specify the build name for the dashboard
- The (optional) `-S` option above is a built-in pyctest option specifying to submit to a CDash dashboard
- The (optional) `-F` option above is a built-in pyctest option specifying to clean the previous build directory

The arguments after the first `--` are passed directly to CTest -- this can include regex statement for which tests to execute. Use `python -m pyctest.ctest` to view these options.

The arguments after the second `--` are passed directly to CMake -- this can provide direct CMake configuration when Python arguments
to enable configure options are not available. Use `python -m pyctest.cmake` to view the built-in CMake options.

