#include <map>

#include "src/hipmer/common/VectorMap.hpp"
#include "gtest/gtest.h"

#include "src/hipmer/common/hipmer_timemory.hpp"

namespace
{
// The fixture for testing class Foo.
class VectorMapTest : public ::testing::Test
{
protected:
    // You can remove any or all of the following functions if its body
    // is empty.

    VectorMapTest()
    {
        // You can do set-up work for each test here.
    }

    virtual ~VectorMapTest()
    {
        // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    virtual void SetUp()
    {
        // Code here will be called immediately after the constructor (right
        // before each test).
    }

    virtual void TearDown()
    {
        // Code here will be called immediately after each test (right
        // before the destructor).
    }

    // Objects declared here can be used by all tests in the test case for Foo.
};

typedef VectorMap<int64_t, int64_t> VM;

inline std::string
get_test_name()
{
    return ::testing::UnitTest::GetInstance()->current_test_info()->name();
}

TEST_F(VectorMapTest, VectorMapCanInsertFindEraseIterateAndClear)
{
    TIMEMORY_BLANK_AUTO_TIMER(get_test_name());
    VM  vm;
    int range = 10000;
    EXPECT_EQ(vm.empty(), true);
    EXPECT_EQ(vm.size(), 0);
    for(int i = 0; i < range; i++)
    {
        VM::value_type           v(i, i * (i - 1));
        pair<VM::iterator, bool> ret = vm.insert(v);
        EXPECT_EQ(ret.second, true);
        EXPECT_NE(ret.first, vm.end());
        EXPECT_EQ(ret.first->first, i);
        EXPECT_EQ(ret.first->second, i * (i - 1));
        pair<VM::iterator, bool> ret2 = vm.insert(v);
        EXPECT_EQ(ret2.second, false);
        EXPECT_NE(ret2.first, vm.end());
        EXPECT_EQ(ret2.first->first, i);
        EXPECT_EQ(ret2.first->second, i * (i - 1));
        ret2.first->second = i * i + 1;  // change the value through the iterator
        EXPECT_EQ(vm[i], i * i + 1);     // operator[] works
    }
    EXPECT_EQ(vm.size(), range);
    EXPECT_EQ(vm.find(-1), vm.end());
    EXPECT_EQ(vm.find(range + 1), vm.end());
    for(int i = 0; i < range; i++)
    {
        VM::const_iterator it = vm.find(i);
        EXPECT_NE(it, vm.end());
        EXPECT_EQ(it->first, i);
        EXPECT_EQ(it->second, i * i + 1);
        if(i % 10 == 0)
        {
            VM::iterator next = vm.erase(it);
            if(next != vm.end())
            {
                EXPECT_NE(i, next->first);
            }
        }
    }
    EXPECT_EQ(vm.size(), range - range / 10);
    int count = 0;
    for(VM::const_iterator it = vm.begin(); it != vm.end(); it++)
    {
        count++;
        EXPECT_EQ(it->first * it->first + 1, it->second);
    }
    EXPECT_EQ(count, vm.size());
    vm.clear();
    EXPECT_EQ(vm.empty(), true);
    EXPECT_EQ(vm.size(), 0);
}

TEST_F(VectorMapTest, newDestroyWorks)
{
    TIMEMORY_BLANK_AUTO_TIMER(get_test_name());
    int range = 10000;
    {
        VM vm;
        EXPECT_EQ(vm.empty(), true);
        EXPECT_EQ(vm.size(), 0);
        vm.reserve(range);
        for(int i = 0; i < range; i++)
        {
            VM::value_type           v(i, i * (i - 1));
            pair<VM::iterator, bool> ret = vm.insert(v);
        }
    }
    {
        VM* vm = new VM(range);
        EXPECT_EQ(vm->empty(), true);
        EXPECT_EQ(vm->size(), 0);
        delete vm;
        vm = new VM(range);
        vm->reserve(range / 10);
        EXPECT_EQ(vm->empty(), true);
        EXPECT_EQ(vm->size(), 0);
        for(int i = 0; i < range; i++)
        {
            VM::value_type           v(i, i * (i - 1));
            pair<VM::iterator, bool> ret = vm->insert(v);
            EXPECT_EQ(ret.first->first, i);
            EXPECT_EQ(ret.first->second, i * (i - 1));
        }
        EXPECT_EQ(vm->size(), range);
        vm->clear();
        EXPECT_EQ(vm->size(), 0);
        delete vm;
    }
}

TEST_F(VectorMapTest, withinSTL)
{
    TIMEMORY_BLANK_AUTO_TIMER(get_test_name());
    {
        vector<VM> vectorOfVM;
        vectorOfVM.resize(100);
        for(int i = 0; i < 100; i++)
        {
            VM& bin = vectorOfVM[i];
            for(int j = 0; j < 10000; j++)
            {
                VM::value_type v(i, i * (i - 1));
                bin.insert(v);
            }
        }
    }
}

TEST_F(VectorMapTest, eraseSome)
{
    TIMEMORY_BLANK_AUTO_TIMER(get_test_name());
    {
        VM vm;
        vm.reserve(100);
        int ntot = 100000;
        int nblk = 10000;
        for(int i = 0; i < ntot; i++)
        {
            TIMEMORY_BLANK_AUTO_TIMER(get_test_name(), "_block_", i / nblk, "_of_",
                                      ntot / nblk);
            VM::value_type v(i, i * (i - 1));
            vm.insert(v);
        }
        VM::const_iterator it = vm.begin();
        EXPECT_EQ(vm.size(), ntot);
        while(it != vm.end())
        {
            it = vm.erase(it);
        }
        EXPECT_EQ(vm.size(), 0);
    }
}

}  // namespace

int
main(int argc, char** argv)
{
#if defined(HIPMER_USE_TIMEMORY)
    // make sure the filtered test is in name
    std::string filter = "--gtest_filter=";
    std::string prefix = "";
    for(int i = 1; i < argc; ++i)
    {
        auto _arg = std::string(argv[i]);
        auto _idx = _arg.find(filter);
        if(_idx != std::string::npos)
            prefix = _arg.substr(_idx + filter.length()) + "-";
    }
    while(prefix.find('.') != std::string::npos)
        prefix.replace(prefix.find('.'), 1, "-");
    while(prefix.find('*') != std::string::npos)
        prefix.erase(prefix.find('*'), 1);
    while(prefix.find('-') == 0)
        prefix.erase(prefix.find('-'), 1);

    tim::settings::width()     = 16;
    tim::settings::precision() = 6;
    tim::timemory_init(argc, argv);
    tim::settings::output_prefix() = prefix;

    using namespace tim::component;
    using dart_t = tim::component_tuple<peak_rss, cpu_util>;
    using echo_t = tim::operation::echo_measurement<peak_rss>;
    dart_t m(argv[0]);
    m.start();
#endif
    ::testing::InitGoogleTest(&argc, argv);

    auto ret = RUN_ALL_TESTS();

#if defined(HIPMER_USE_TIMEMORY)
    m.stop();
    std::stringstream ss;
    auto&             rss    = m.get<peak_rss>();
    auto              peak_v = rss.get() * rss.get_unit() / (1.0 * tim::units::megabyte);
    auto              util_v = m.get<cpu_util>().get();
    echo_t::generate_measurement(ss, { { "name", "PEAK_RSS_MB" } }, peak_v);
    echo_t::generate_measurement(ss, { { "name", "CPU_UTIL" } }, util_v);
    std::cout << ss.str() << std::endl;
#endif

    return ret;
}
