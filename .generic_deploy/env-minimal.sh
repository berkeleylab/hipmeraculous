#!/bin/bash 

export HIPMER_SHARED_MEM_PCT=24
export HIPMER_ENV=$(uname -s)-minimal
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=0
export HIPMER_MAX_KMER_SIZE=32

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp
export HIPMER_ARCH=native
