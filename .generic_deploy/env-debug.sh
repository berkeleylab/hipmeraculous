#!/bin/bash 

export HIPMER_ENV=$(uname -s)-debug
export HIPMER_BUILD_TYPE="Debug"
export HIPMER_MAX_KMER_SIZE=128
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=1}
export AUTORESTART=${AUTORESTART:=0}

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp

export GASNET_BACKTRACE=1

export MPICXX=$(which mpicxx)
export CXX=$(which mpicxx)
export CC=$(which gcc)

