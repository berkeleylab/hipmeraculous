#!/bin/bash

set -e
set -x 

# assumes ubuntu 16.04 with gcc 8 from the test toolchain-r
# sudo add-apt-repository ppa:ubuntu-toolchain-r/test
# sudo apt-get update
# sudo apt-get install gcc-9 g++-9

inst=$1
[ -n "$inst" ] || inst=${HOME}/install-gcc-9

mkdir -p $inst

CC=gcc-9 CXX=g++-9 contrib/install_upc.sh smp posix $inst


