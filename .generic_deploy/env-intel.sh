#!/bin/bash 

export HIPMER_ENV=$(uname -s)-intel
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}
export HIPMER_NO_UNIT_TESTS=${HIPMER_NO_UNIT_TESTS:=0}

export CC=$(which icc)
export CXX=$(which icpc)

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp

#export HIPMER_BUILD_OPTS="-DCMAKE_UPC_CFLAGS=-Wc,-std=c99"
