#!/bin/bash

export HIPMER_ENV=$(uname -s)-udp
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

export HIPMER_UPC_FLAGS="-network=udp"
export UPCXX_GASNET_CONDUIT=udp
export HIPMER_ARCH=native


