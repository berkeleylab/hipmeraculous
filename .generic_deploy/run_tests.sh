#!/bin/bash

function oops()
{
  echo "uh oh, one of the tests failed!"
  exit 1
}


trap oops 0 1 2 3 15
set -e
set -x

export GASNET_BACKTRACE=1

HIPMER_ENV_SCRIPT=.generic_deploy/env-debug.sh ./bootstrap_hipmer_env.sh install

inst=$SCRATCH/hipmer-install-Linux-debug/bin
AUTORESTART=0 CACHED_IO=0 ${inst}/test_hipmer.sh validation
AUTORESTART=0 CACHED_IO=1 ${inst}/test_hipmer.sh validation-mg
AUTORESTART=0 CACHED_IO=1 ${inst}/test_hipmer.sh validation2D-diploid
[ "$HIPMER_NO_CGRAPH" == 1 ] || AUTORESTART=0 CACHED_IO=1 ${inst}/test_hipmer.sh validation-mg

HIPMER_ENV_SCRIPT=.generic_deploy/env-refatomic-debug.sh ./bootstrap_hipmer_env.sh install
inst=$SCRATCH/hipmer-install-Linux-refatomic-debug/bin
AUTORESTART=0 CACHED_IO=0 ${inst}/test_hipmer.sh validation-mg

HIPMER_ENV_SCRIPT=.generic_deploy/env.sh ./bootstrap_hipmer_env.sh install
inst=$SCRATCH/hipmer-install-Linux/bin

# test checkpointing
VERBOSE=0 TEST_CHECKPOINTING=1 AUTORESTART=1 USE_DEPRECATED=0 CACHED_IO=0 ${inst}/test_hipmer.sh validation validation-mg3
          TEST_CHECKPOINTING=1 AUTORESTART=1 USE_DEPRECATED=0 CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg3

# test workflow variations
rm -rf /dev/shm/per_thread/ 
for v in "-v" " " ; do
    for l in   "--localize-reads" " " ; do 
        for m in 0 1 2 ; do 
            for c in "--cached-io --cached-reads=false" "--cached-io=false" "--cached-io" ; do 
                echo $v $l $m $c;
                RUN_OPTS="$v --auto-restart=False $l --merge-reads=$m $c" ${inst}/test_hipmer.sh validation validation-mg
                RUN_OPTS="$v --auto-restart=False $l --merge-reads=$m $c" ${inst}/test_hipmer.sh validation-mg3
            done
        done
    done 
done

# test extended checkpointing
TEST_CHECKPOINTING_EXTENDED=1 CACHED_IO=1 RUN_OPTS="--localize-reads=True" ${inst}/test_hipmer.sh validation-mg
TEST_CHECKPOINTING_EXTENDED=1 CACHED_IO=1 RUN_OPTS="--localize-reads=False" ${inst}/test_hipmer.sh validation-mg

AUTORESTART=0 CACHED_IO=0 ${inst}/test_hipmer.sh ecoli ecoli
AUTORESTART=0 CACHED_IO=1 ${inst}/test_hipmer.sh chr14
AUTORESTART=0 CACHED_IO=1 UPC_SHARED_HEAP_SIZE=5000 ${inst}/test_hipmer.sh mg250

set +x
trap "" 0

