#!/bin/bash 

export HIPMER_ENV=$(uname -s)-gcc-8
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=1}

export PATH=~/install-gcc-8/bin:$PATH

export CC=gcc-8
export CXX=g++-8

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp
export HIPMER_ARCH=native
