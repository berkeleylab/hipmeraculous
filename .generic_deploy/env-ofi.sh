#!/bin/bash

export HIPMER_ENV=$(uname -s)-ofi
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

export HIPMER_UPC_FLAGS="-network=ofi"
export UPCXX_GASNET_CONDUIT=ofi
export HIPMER_ARCH=native
