#!/bin/bash

BASE=${0%run_tests*.sh}

function oops()
{
  echo "uh oh, one of the tests failed!"
  exit 1
}


trap oops 0 1 2 3 15
set -e
set -x

export DIST_CLEAN=1
export HIPMER_NO_GZIP=0

${BASE}../bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=${BASE}env-debug.sh install

AUTORESTART=0 CACHED_IO=1 $SCRATCH/hipmer-install-Linux-debug/bin/test_hipmer.sh validation-par_hmm
AUTORESTART=0 CACHED_IO=0 $SCRATCH/hipmer-install-Linux-debug/bin/test_hipmer.sh validation
AUTORESTART=0 CACHED_IO=1 $SCRATCH/hipmer-install-Linux-debug/bin/test_hipmer.sh validation-mg

${BASE}../bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=${BASE}env.sh install
AUTORESTART=0 CACHED_IO=0 $SCRATCH/hipmer-install-Linux/bin/test_hipmer.sh ecoli
AUTORESTART=0 CACHED_IO=1 $SCRATCH/hipmer-install-Linux/bin/test_hipmer.sh chr14
AUTORESTART=0 CACHED_IO=1 UPC_SHARED_HEAP_SIZE=5000 $SCRATCH/hipmer-install-Linux/bin/test_hipmer.sh mg250

set +x
trap "" 0

