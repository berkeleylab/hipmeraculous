#!/bin/bash

HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-generic}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
if [ -z "$CORES_PER_NODE" ]; then
	export CORES_PER_NODE=`grep processor /proc/cpuinfo|wc -l`
fi

RUNSCRIPT=${RUNSCRIPT:=run_hipmer_unified.sh}
CONFIG_FILE=${CONFIG_FILE:=meraculous-mg-small.config}

INST=${HIPMER_INSTALL:=$1}

if [ ! -f ${INST}/bin/${RUNSCRIPT} ]; then
  echo "Could not find HipMer runscript ${RUNSCRIPT} installed at ${INST}"
  exit 1
fi

src=$(echo ${INST}/env*.sh)
if [ -f ${src} ]; then
    . ${src}
else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
fi
  
#  export HIPMER_MAX_KMER_CACHE=4096
#  export HIPMER_SW_CACHE_MB=1024
export UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=1500}
export ILLUMINA_VERSION=18
export THREADS=${THREADS:=${CORES_PER_NODE}}
export RUNDIR=${RUNDIR:=$SCRATCH/mg-${THREADS}-$(date '+%Y%m%d_%H%M%S')-${RUNSCRIPT}}
mkdir $RUNDIR

HIPMER_METAG_DATA=${HIPMER_CHR14_DATA:=${SCRATCH}/hipmer_metagenome_data}
for i in ${HIPMER_METAG_DATA}/* ; do ln -s $i $RUNDIR ; done
cp -p ${INST}//etc/meraculous/pipeline/${CONFIG_FILE} $RUNDIR 

MPIRUN="mpirun -n" UPCRUN="upcrun -q -n" ${INST}/bin/${RUNSCRIPT} $INST ${RUNDIR}/${CONFIG_FILE}


