#!/bin/bash

function oops()
{
  echo "uh oh, one of the tests failed!"
  exit 1
}


trap oops 0 1 2 3 15
set -e
set -x


MAX_NODES=${MAX_NODES:=1}

for cio in 1 0
do
  for nodes in $(seq 1 ${MAX_NODES})
  do
    export CACHED_ID=$cio
    THREADS=$((CORES_PER_NODE*nodes)) test_hipmer.sh ecoli
    THREADS=$((CORES_PER_NODE*nodes)) test_hipmer.sh chr14
  done
done

set +x
trap "" 0

