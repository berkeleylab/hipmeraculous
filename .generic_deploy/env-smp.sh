#!/bin/bash 

export HIPMER_ENV=$(uname -s)-smp
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp
export HIPMER_ARCH=native

