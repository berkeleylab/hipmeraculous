#!/bin/bash

export HIPMER_ENV=$(uname -s)-mpi
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=0}

#export HIPMER_UPC_FLAGS="-network=mpi"
export UPCXX_GASNET_CONDUIT=mpi
export HIPMER_ARCH=native
