#!/bin/bash

set -e
set -x 

# assumes ubuntu 16.04 with gcc 8 from the test toolchain-r
# sudo add-apt-repository ppa:ubuntu-toolchain-r/test
# sudo apt-get update
# sudo apt-get install gcc-8 g++-8

inst=$1
[ -n "$inst" ] || inst=${HOME}/install-gcc-8

mkdir -p $inst

CC=gcc-8 CXX=g++-8 contrib/install_upc.sh smp posix $inst


