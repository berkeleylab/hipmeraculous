#!/bin/bash 

export HIPMER_ENV=$(uname -s)-gcc-9
export HIPMER_BUILD_TYPE="Release"
export HIPMER_FULL_BUILD=${HIPMER_FULL_BUILD:=1}

export PATH=~/install-gcc-9/bin:$PATH

export CC=gcc-9
export CXX=g++-9

export HIPMER_UPC_FLAGS="-network=smp"
export UPCXX_GASNET_CONDUIT=smp
export HIPMER_ARCH=native
