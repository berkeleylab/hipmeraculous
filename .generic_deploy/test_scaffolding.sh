#!/bin/bash

HIPMER_INSTALL=${HIPMER_INSTALL:=${SCRATCH}/install-generic}
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
if [ -z "$CORES_PER_NODE" ]; then
	export CORES_PER_NODE=`grep processor /proc/cpuinfo|wc -l`
fi

N=${CORES_PER_NODE}
export CAN_SPLIT_JOB=1

INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/run_hipmer.sh ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  export THREADS=${THREADS:=${N}}

  export RUNDIR=${RUNDIR:=$SCRATCH/metagenome-${THREADS}-$(date '+%Y%m%d_%H%M%S')}
  mkdir $RUNDIR

  HIPMER_METAG_DATA=${HIPMER_CHR14_DATA:=${SCRATCH}/hipmer_metagenome_data}
  for i in ${HIPMER_METAG_DATA}/* ; do ln -s $i $RUNDIR ; done
  export ILLUMINA_VERSION=18

  cp -p ${INST}//etc/meraculous/pipeline/meraculous-test.config $RUNDIR 

  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=1500}
  MPIRUN="mpirun -n" UPCRUN="upcrun -q -n" UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB} ${INST}/bin/run_Scaffolding.sh $INST ${RUNDIR}/meraculous-test.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi

