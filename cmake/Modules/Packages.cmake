#
#
#   3rd party packages
#
#

option(HIPMER_USE_TIMEMORY "Enable timemory" OFF)

if(HIPMER_USE_TIMEMORY)
    set(_COMPONENTS headers c cxx papi gotcha caliper cuda nvtx cupti cudart-device cudart gperftools-cpu)
    set(TIMEMORY_COMPONENTS "${_COMPONENTS}" CACHE STRING "timemory components")

    set(timemory_FIND_COMPONENTS_INTERFACE hipmer-timemory)
    find_package(timemory REQUIRED COMPONENTS ${TIMEMORY_COMPONENTS})
    include_directories(${timemory_INCLUDE_DIRS})
    target_compile_definitions(hipmer-timemory INTERFACE HIPMER_USE_TIMEMORY)
else()
    add_library(hipmer-timemory INTERFACE)
endif()
