#!/bin/bash 

[ -z "$SCRATCH" ] && echo "Define \$SCRATCH for the root of the build and install" && exit 1


export HIPMER_ENV=titan-debug
export CC=$(which cc)
export CXX=$(which CC)
export HIPMER_BUILD_OPTS=""
export HIPMER_BUILD_TYPE="Debug"
#export CORES_PER_NODE=${CORES_PER_NODE:=4}
#export HIPMER_POST_INSTALL="cp ${BASH_SOURCE[0]} ${PREFIX}"
export HYPERTHREADS=${HYPERTHREADS:=1}

