#!/bin/bash 
set -x

[ -n "${HIPMER_BUILD_ENV}" ] || . $(dirname $0)/env.sh

set -ex

[ -n "${BUILD}" ]
[ -n "${PREFIX}" ]

[ -z "${CLEAN}" ] || rm -fr ${PREFIX}
mkdir -p ${PREFIX}
if [ -x /usr/bin/lfs ]
then
  /usr/bin/lfs setstripe -c 1 ${PREFIX} || true
fi
cd ${BUILD}
TMPDIR=/tmp make -j 16 install | grep -v "jobserver unavailable"
cd -

if [ -z "${HIPMER_BUILD_ENV_SCRIPT}" ]
then
  unset HIPMER_BUILD_ENV
fi
${0%/*}/post-install.sh

#deploy=${BASH_SOURCE[0]}
#deploy=${deploy%/*}
#[ ! -d ${deploy}/module_dependencies ] || cp -p ${deploy}/module_dependencies $PREFIX/.deps
#
#if [ -n "${HIPMER_BUILD_ENV_SCRIPT}" ] && [ -f "${HIPMER_BUILD_ENV_SCRIPT}" ]
#then
#  cp -p ${HIPMER_BUILD_ENV_SCRIPT} ${PREFIX}
#fi
#
#if [ -n "${HIPMER_POST_INSTALL}" ]
#then
#  ${HIPMER_POST_INSTALL} || /bin/true
#fi
