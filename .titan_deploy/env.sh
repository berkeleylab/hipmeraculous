#!/bin/bash 

[ -z "$SCRATCH" ] && echo "Define \$SCRATCH for the root of the build and install dirs" && exit 1

module swap PrgEnv-pgi PrgEnv-intel/5.2.82
module use /ccs/proj/csc103/modulefiles
module load bupc-narrow/2.22.3-5.2.82-intel-15.0.2.164
module swap cray-mpich cray-mpich/7.4.0 # latest version of mpich2 (module name is a misnomer)
module load cray-shmem # for shared memory 
module load cray-hdf5-parallel/1.8.14 #this was in edison config
module load git #was also in edison config
module load cmake3/3.2.3 #cmake 3 behaves better than default cmake
module load dynamic-link # required for cmake tests
module list

export HIPMER_ENV=titan
export CC=cc
export CXX=CC
echo $CC
echo $CXX
export HIPMER_BUILD_OPTS=""
export HIPMER_BUILD_TYPE="Release"
export UPCC_FLAGS="$CRAY_XPMEM_POST_LINK_OPTS -lxpmem $CRAY_UGNI_POST_LINK_OPTS -lugni $CRAY_UDREG_POST_LINK_OPTS -ludreg $CRAY_PMI_POST_LINK_OPTS -lpmi"
export CORES_PER_NODE=${CORES_PER_NODE:=16}
export HYPERTHREADS=${HYPERTHREADS:=1}

