#!/bin/bash
# Set SGE options:
#$ -clear
#$ -cwd
#$ -j y
#$ -R y 
#$ -m as 
#$ -w e
#$ -l ram.c=7.5G,exclusive.c
#$ -pe pe_32 64
#$ -N HipMer

module load jgibio

$(which jgi_job_wrapper.sh) $@

