#!/bin/bash 

module purge

module load uge
module load PrgEnv-gnu/4.8
module load cmake
module rm OFED
module load openmpi
#module load OFED
#GP_INFINIBAND=0 IB_VER=tcp module load openmpi/2.1.1
module load perl
module load gnuplot
module load zlib

module use ~regan/modulefiles-genepool
module load bupc/regan-2.26.0-gnu48-sysv

module list

export HIPMER_ENV=genepool
export HIPMER_BUILD_TYPE="Release"

export HIPMER_NO_UNIT_TESTS=1
