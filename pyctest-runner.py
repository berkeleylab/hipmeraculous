#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PyCTest driver for HipMer
"""

import os
import sys
import shutil
import platform
import traceback
import warnings
import multiprocessing as mp
import pyctest.pyctest as pyct
import pyctest.pycmake as pycm
import pyctest.helpers as helpers

from test import dynamic as dynamic_tests

clobber_notes = True


#------------------------------------------------------------------------------#
#
def get_branch(wd=pyct.SOURCE_DIRECTORY):
    cmd = pyct.command(["git", "show", "-s", "--pretty=%d", "HEAD"])
    cmd.SetOutputStripTrailingWhitespace(True)
    cmd.SetWorkingDirectory(wd)
    cmd.Execute()
    branch = cmd.Output()
    branch = branch.split(" ")
    if branch:
        branch = branch[len(branch)-1]
        branch = branch.strip(")")
    if not branch:
        branch = pyct.GetGitBranch(wd)
    return branch


#------------------------------------------------------------------------------#
#
def configure():

    hipmer_binary_dir = os.path.join(
        os.getcwd(), "build-hipmeraculous", platform.system())
    hipmer_install_dir = None

    # Get pyctest argument parser that include PyCTest arguments
    parser = helpers.ArgumentParser(project_name="hipmeraculous",
                                    source_dir=os.getcwd(),
                                    binary_dir=hipmer_binary_dir,
                                    build_type="Release",
                                    vcs_type="git")

    parser.add_argument("-n", "--build-name",
                        help="Build label", type=str, required=True)
    parser.add_argument("--install", help="Install the project in addition to building",
                        default=hipmer_install_dir, type=str)
    parser.add_argument("-j", "--njobs", type=int, default=min([8, mp.cpu_count()]),
                        help="Parallel build jobs")
    parser.add_argument("-t", "--targets", type=str, default=["all"],
                        help="List of build targets", nargs='+')
    parser.add_argument("-c", "--categories", help="Test categories from test.dynamic",
                        default=["example"], type=str, nargs='+')

    # build configuration options
    parser.add_argument("--minimal-build", help="HIPMER_FULL_BUILD=OFF",
                        default=False, action='store_true')
    parser.add_argument("--khash", help="Use KHASH for kmer counting instead of STL or VectorMap for kmer counting",
                        default=False, action='store_true')
    parser.add_argument("--embed-perl", help="Embed perl",
                        default=False, action='store_true')
    parser.add_argument("--embed-hmmer", help="Embed a modified build of the hmmer library",
                        default=False, action='store_true')
    parser.add_argument("--hw-atomic", help="Enable hardware atomics",
                        default=False, action='store_true')
    parser.add_argument("--enable-avx512f", help="Enable AVX512F",
                        default=False, action='store_true')
    parser.add_argument("--read-buffer", help="Set the read buffer size",
                        default=1048576, type=int)
    parser.add_argument("--build-libs", help="Build shared and/or static libraries",
                        default=["static"], type=str, choices=["shared", "static"],
                        nargs="*")
    parser.add_argument("--use-timemory", help="Enable timemory",
                        default=False, action='store_true')

    args = parser.parse_args()

    if args.install is not None and not "install" in args.targets:
        args.targets += ["install"]

    if os.environ.get("CTEST_SITE") is not None:
        pyct.set("CTEST_SITE", "{}".format(os.environ.get("CTEST_SITE")))

    # disable banner
    os.environ["TIMEMORY_BANNER"] = "OFF"

    return args


#------------------------------------------------------------------------------#
#
def run_pyctest():

    #--------------------------------------------------------------------------#
    # run argparse, checkout source, copy over files
    #
    args = configure()

    #--------------------------------------------------------------------------#
    # Set the build name
    #
    pyct.BUILD_NAME = args.build_name

    #--------------------------------------------------------------------------#
    #   build specifications
    #
    install_dir = os.path.join(os.getcwd(), "install-hipmeraculous",
                               platform.system()) if args.install is None else args.install
    build_opts = {
        "CMAKE_BUILD_TYPE": "{}".format(pyct.BUILD_TYPE),
        "CMAKE_INSTALL_PREFIX": "{}".format(install_dir),
        "HIPMER_FULL_BUILD": "OFF" if args.minimal_build else "ON",
        "HIPMER_EMBED_HMMER": "ON" if args.embed_hmmer else "OFF",
        "HIPMER_READ_BUFFER": "{}".format(args.read_buffer),
        "HIPMER_EMBED_PERL": "ON" if args.embed_perl else "OFF",
        "HIPMER_KHASH": "ON" if args.khash else "OFF",
        "HIPMER_HWATOMIC": "ON" if args.hw_atomic else "OFF",
        "HIPMER_NO_AVX512F": "OFF" if args.enable_avx512f else "ON",
        "HIPMER_USE_TIMEMORY": "ON" if args.use_timemory else "OFF",
        "BUILD_SHARED_LIBS": "ON" if "shared" in args.build_libs else "OFF",
        "BUILD_STATIC_LIBS": "ON" if "static" in args.build_libs else "OFF",
    }

    # arguments provided via the command line
    cmake_args = " ".join(pycm.ARGUMENTS)

    # customized from args
    for key, val in build_opts.items():
        cmake_args = "{} -D{}={}".format(cmake_args, key, val)

    #--------------------------------------------------------------------------#
    # how to build the code
    #
    ctest_cmake_cmd = "${CTEST_CMAKE_COMMAND}"
    pyct.CONFIGURE_COMMAND = "{} {} {}".format(
        ctest_cmake_cmd, cmake_args, pyct.SOURCE_DIRECTORY)

    #--------------------------------------------------------------------------#
    # how to build the code
    #
    targets = ["--target {}".format(t) for t in args.targets]
    pyct.BUILD_COMMAND = "{} --build {} {}".format(
        ctest_cmake_cmd, pyct.BINARY_DIRECTORY, " ".join(targets))

    #--------------------------------------------------------------------------#
    # parallel build
    #
    if platform.system() != "Windows":
        pyct.BUILD_COMMAND = "{} -- -j{} VERBOSE=1".format(
            pyct.BUILD_COMMAND, args.njobs)
    else:
        pyct.BUILD_COMMAND = "{} -- /MP -A x64".format(pyct.BUILD_COMMAND)

    #--------------------------------------------------------------------------#
    # how to update the code
    #
    git_exe = helpers.FindExePath("git")
    pyct.UPDATE_COMMAND = "{}".format(git_exe)
    pyct.set("CTEST_UPDATE_TYPE", "git")
    pyct.set("CTEST_GIT_COMMAND", "{}".format(git_exe))

    #--------------------------------------------------------------------------#
    # find the CTEST_TOKEN_FILE
    #
    if args.pyctest_token_file is None and args.pyctest_token is None:
        home = helpers.GetHomePath()
        if home is not None:
            token_path = os.path.join(home, ".tokens", "nersc-cdash")
            if os.path.exists(token_path):
                pyct.set("CTEST_TOKEN_FILE", token_path)

    #--------------------------------------------------------------------------#
    # find a testing category
    #
    def get_category(names):
        category_method = None
        for n in names:
            try:
                category_method = getattr(dynamic_tests, "{}".format(n))
                if category_method is not None:
                    return category_method
            except:
                pass
        return None

    #--------------------------------------------------------------------------#
    # call all the testing category functions defined in tests.dynamic
    # which add tests
    #
    for category in args.categories:
        category_method = get_category([category + "_tests", category])
        if category_method is None:
            msg = "No category named: {0} or {0}_tests".format(category)
            raise RuntimeError(msg)
        category_method(**vars(args))

    # generate the base configuration
    pyct.generate_config(pyct.BINARY_DIRECTORY)
    # generate the dynamic tests
    pyct.generate_test_file(os.path.join(
        pyct.BINARY_DIRECTORY, "test", "dynamic"))
    pyct.run(pyct.ARGUMENTS, pyct.BINARY_DIRECTORY)


#------------------------------------------------------------------------------#
#
if __name__ == "__main__":

    try:
        run_pyctest()
    except Exception as e:
        print('Error running pyctest - {}'.format(e))
        exc_type, exc_value, exc_trback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_trback, limit=10)
        sys.exit(1)

    sys.exit(0)
