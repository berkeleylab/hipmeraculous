variables:
  GIT_STRATEGY: fetch
  REGISTRY: registry.gitlab.com
  APPLICATION: MetaHipMer
  TEST_IMAGE: $REGISTRY/$REGISTRY_USER/$APPLICATION:latest
  RELEASE_IMAGE: $REGISTRY/$REGISTRY_USER/$APPLICATION:$CI_BUILD_REF_NAME
  UPCXX_VER: 2022.9.0
  UPC_VER: 2022.10.0


stages:
  - build # all
  - validation # regan-sls, gpint
  - accuracy # regan-sls, edison
#  - cluster # edison, cori, cori-knl, denovo

#
# Bacteria
#

Bacteria:build:
  stage: build
  tags:
    - Bacteria
  script:
    - set -e
    - set -x
    - uname -a
    - pwd
    - date
    - export BASE=/scratch-bacteria/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - export GASNET_BACKTRACE=1
    - export INSTALL_PREFIX=${CI_SCRATCH}
    - export PATH=${CI_INSTALL}/bin:${PATH}
    - unset HIPMER_INSTALL
    - mkdir -p ${BASE}/scratch
    - rm -rf ${CI_SCRATCH}
    - mkdir -p ${CI_SCRATCH}/runs
    - exec >  >(tee -ia ${CI_SCRATCH}/build.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/build.err >&2)
    - date
    - echo "Purging any old tests"
    - ls -td ${BASE}/scratch/*-*-*-*-* | tail -n +15 | xargs  rm -rf '{}' ';' || /bin/true
    - df -h
    - echo "Checking for cmake, Berkeley UPC and UPC++"
    - which cmake && cmake --version
    - which upcxx && which upcc || BUPCVER=${UPC_VER}  UPCXXVER=${UPCXX_VER} ./contrib/install_upc.sh ibv posix $CI_INSTALL
    - upcxx --version
    - upcc --version
    - echo "Building all flavors"
    - mkdir -p ${CI_SCRATCH}/runs
    - export GASNET_BACKTRACE=1
    - export INSTALL_PREFIX=${CI_SCRATCH}
    - echo "Building debug"
    - TMPDIR=${CI_SCRATCH} DIST_CLEAN=1 .misc_deploy/build_envs.sh install .generic_deploy/env-debug.sh
    - inst=${CI_SCRATCH}/hipmer-install-Linux-debug/bin
    - echo "Testing debug build with cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-debug CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg3 validation2D-diploid
    - echo "Building other optmized flavor"
    - TMPDIR=${CI_SCRATCH} DIST_CLEAN=1 .misc_deploy/build_envs.sh install .generic_deploy/env.sh
    - inst=${CI_SCRATCH}/hipmer-install-Linux/bin
    - echo "Testing release build with cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg validation-mg3 validation2D-diploid
    - echo "Testing release build without cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-nocio CACHED_IO=0 ${inst}/test_hipmer.sh validation validation-mg validation-mg3 validation2D-diploid
    - echo "Testing checkpointing with cached io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio-checkpointing TEST_CHECKPOINTING=1 AUTORESTART=1 CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg3 validation2D-diploid
    - echo "Testing extended checkpointing - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio-checkpointing-ext TEST_CHECKPOINTING_EXTENDED=1 CACHED_IO=1 ${inst}/test_hipmer.sh validation
    - echo "Completed"


Bacteria:validate:
  stage: validation
  tags:
    - Bacteria
  script:
    - set -e
    - set -x
    - export GASNET_BACKTRACE=1
    - export BASE=/scratch-bacteria/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - export HIPMER_DATA=${BASE}/data
    - mkdir -p ${HIPMER_DATA}
    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
    - export INSTALL_PREFIX=${CI_SCRATCH}
    - export PATH=${CI_INSTALL}/bin:${PATH}
    - export CACHED_IO=0
    - export AUTO_RESTART=0
    - inst=${INSTALL_PREFIX}/hipmer-install-Linux/bin
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh
    - exec >  >(tee -ia ${CI_SCRATCH}/validation.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/validation.err >&2)
    - date
    - echo "Testing release build - $inst"
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh validation
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh validation-mg
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh validation
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh validation2D-diploid
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh validation-mg3
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh ecoli ecoli-single ecoli-mg ecoli-mg_single
    - echo "Testing checkpointing with cached io round 2 - $inst"
    - SCRATCH=${CI_SCRATCH}/runs TEST_CHECKPOINTING=1 AUTORESTART=1 CACHED_IO=1 ${inst}/test_hipmer.sh validation-mg validation validation-mg3
    - echo "test workflow variations"
    - rm -rf /dev/shm/per_rank/
    - echo "Running with options   --localize-reads --merge-reads=0 --cached-io --cached-reads=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=0 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=0 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=0 --cached-io=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=0 --cached-io"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=0 --cached-io" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=1 --cached-io --cached-reads=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=1 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=1 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=1 --cached-io=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=1 --cached-io"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=1 --cached-io" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=2 --cached-io --cached-reads=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=2 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=2 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=2 --cached-io=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads --merge-reads=2 --cached-io"
    - RUN_OPTS="  --auto-restart=False --localize-reads --merge-reads=2 --cached-io" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=0 --cached-io --cached-reads=false"
    # no cgraph when reads have not been localzed or merged as the readlength it too lang
    - RUN_OPTS="  --auto-restart=False --localize-reads=false  --merge-reads=0 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation
    - echo "Running with options   --localize-reads=false  --merge-reads=0 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=0 --cached-io=false" ${inst}/test_hipmer.sh validation
    - echo "Running with options   --localize-reads=false  --merge-reads=0 --cached-io"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=0 --cached-io" ${inst}/test_hipmer.sh validation
    - echo "Running with options   --localize-reads=false  --merge-reads=1 --cached-io --cached-reads=false"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=1 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=1 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=1 --cached-io=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=1 --cached-io"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=1 --cached-io" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=2 --cached-io --cached-reads=false"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=2 --cached-io --cached-reads=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=2 --cached-io=false"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=2 --cached-io=false" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Running with options   --localize-reads=false  --merge-reads=2 --cached-io"
    - RUN_OPTS="  --auto-restart=False  --localize-reads=false --merge-reads=2 --cached-io" ${inst}/test_hipmer.sh validation validation-mg3
    - echo "Completed"



Bacteria:accuracy:
  stage: accuracy
  tags:
    - Bacteria
  script:
    - set -e
    - set -x
    - export BASE=/scratch-bacteria/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - export HIPMER_DATA=${BASE}/data
    - export PATH=${CI_INSTALL}/bin:${PATH}
    - export INSTALL_PREFIX=${CI_SCRATCH}
    - mkdir -p ${HIPMER_DATA}
    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
    - export HIPMER_MG250_DATA=${HIPMER_DATA}/hipmer_metagenome-250_data
    - export HIPMER_MOCK150_DATA=${HIPMER_DATA}/hipmer_mock150_data
    - export HIPMER_CHR14_DATA=${HIPMER_DATA}/hipmer_chr14_data
    - export HIPMER_HUMAN_DATA=${HIPMER_DATA}/hipmer_human_data
    - export SCRATCH=${CI_SCRATCH}
    - export AUTO_RESTART=0
    - inst=${INSTALL_PREFIX}/hipmer-install-Linux/bin
    - export CACHED_IO=1
    - exec >  >(tee -ia ${CI_SCRATCH}/accuracy.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/accuracy.err >&2)
    - date
    - echo "Download data sets and install in ${SCRATCH}/runs, cache in ${HIPMER_DATA} between CI instances"
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh ecoli
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh mg250
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh chr14-benchmark
    - echo "Testing ecoli - $inst"
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh ecoli
    - echo "Testing mg250 - $inst"
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh mg250
    - echo "Testing chr14-benchmark - $inst"
    - SCRATCH=${CI_SCRATCH}/runs GASNET_BACKTRACE=1 ${inst}/test_hipmer.sh chr14-benchmark
    - echo "Completed"

#
## cori
##
#
#Cori:build:
#  stage: build
#  tags:
#    - Cori
#  script:
#    - set -e
#    - set -x
#    - git submodule init
#    - git submodule sync
#    - git submodule update
#    - uname -a
#    - pwd
#    - date
#    - unset HIPMER_INSTALL
#    - export HIPMER_DATA=${SCRATCH}
#    - export BASE=${SCRATCH}/tmp/gitlab-runner-scratch-Cori-${USER}
#    - export SCRATCH=${BASE}/inst-${CI_COMMIT_SHA}
#    - export INSTALL_PREFIX=${SCRATCH}
#    - mkdir -p ${BASE}
#    - find ${BASE} -maxdepth 1 '(' -name 'inst-*' -o -name '*hipmer-*' ')' -mtime +4 -type d -exec rm -rf '{}' ';' || /bin/true
#    - rm -rf ${SCRATCH}
#    - mkdir -p ${SCRATCH}/runs
#    - exec >  >(tee -ia ${SCRATCH}/build.log)
#    - exec 2> >(tee -ia ${SCRATCH}/build.err >&2)
#    - export INSTALL_PREFIX=${SCRATCH}
#    - export HIPMER_BUILD=/tmp/build-${CI_COMMIT_SHA}
#    - mkdir -p ${HIPMER_BUILD}
#    - lmstat -a -c $INTEL_LICENSE_FILE || /bin/true
#    - DIST_CLEAN=1 SERIAL_BUILD=1 BUILD_THREADS=12 .misc_deploy/build_envs.sh install .cori_deploy/env.sh .cori_deploy/env-debug.sh .cori_knl_deploy/env.sh
#    - rm -r ${HIPMER_BUILD}
#
#Cori:validation:
#  stage: validation
#  tags:
#    - Cori
#  script:
#    - set -e
#    - set -x
#    - uname -a
#    - pwd
#    - date
#    - export GASNET_BACKTRACE=1
#    - export HIPMER_DATA=${SCRATCH}
#    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
#    - export HIPMER_MG250_DATA=${HIPMER_DATA}/hipmer_metagenome-250_data
#    - export HIPMER_MOCK150_DATA=${HIPMER_DATA}/hipmer_mock150_data
#    - export HIPMER_CHR14_DATA=${HIPMER_DATA}/hipmer_chr14_data
#    - export HIPMER_HUMAN_DATA=${HIPMER_DATA}/hipmer_human_data
#    - export HIPMER_SYNTH64_DATA=${HIPMER_DATA}/hipmer_synth64_data
#    - export BASE=${SCRATCH}/tmp/gitlab-runner-scratch-Cori-${USER}
#    - export SCRATCH=${BASE}/inst-${CI_COMMIT_SHA}
#    - export INSTALL_PREFIX=${SCRATCH}
#    - export AUTO_RESTART=0
#    - exec >  >(tee -ia ${SCRATCH}/validation.log)
#    - exec 2> >(tee -ia ${SCRATCH}/validation.err >&2)
#    - cd $SCRATCH
#    - inst=$SCRATCH/hipmer-install-cori-debug/bin
#    - echo "Testing debug build on Haswell - $inst"
#    - MIN_NODES=4 MAX_NODES=4 salloc --job-name="CI-HipMer-${CI_COMMIT_SHA}" --account=m342 -C haswell --nodes=6-8  --qos=debug --time=30:00  --ntasks-per-node=32 $inst/sbatch_cori.sh $inst/test_hipmer.sh SCRATCH=${SCRATCH}/runs validation validation-mg3
#
#Cori:accuracy:
#  stage: accuracy
#  tags:
#    - Cori
#  script:
#    - set -e
#    - set -x
#    - uname -a
#    - pwd
#    - date
#    - export GASNET_BACKTRACE=1
#    - export HIPMER_DATA=${SCRATCH}
#    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
#    - export HIPMER_MG250_DATA=${HIPMER_DATA}/hipmer_metagenome-250_data
#    - export HIPMER_MOCK150_DATA=${HIPMER_DATA}/hipmer_mock150_data
#    - export HIPMER_CHR14_DATA=${HIPMER_DATA}/hipmer_chr14_data
#    - export HIPMER_HUMAN_DATA=${HIPMER_DATA}/hipmer_human_data
#    - export HIPMER_SYNTH64_DATA=${HIPMER_DATA}/hipmer_synth64_data
#    - export BASE=${SCRATCH}/tmp/gitlab-runner-scratch-Cori-${USER}
#    - export SCRATCH=${BASE}/inst-${CI_COMMIT_SHA}
#    - export INSTALL_PREFIX=${SCRATCH}
#    - export AUTO_RESTART=0
#    - exec >  >(tee -ia ${SCRATCH}/accuracy.log)
#    - exec 2> >(tee -ia ${SCRATCH}/accuracy.err >&2)
#    - cd $SCRATCH
#    - inst=$SCRATCH/hipmer-install-cori-knl/bin
#    - echo "Testing on KNL - $inst"
#    - knl_job=$(sbatch --parsable --job-name="CI-HipMer-${CI_COMMIT_SHA}" --account=m342 -C knl --nodes=12-16  --qos=debug --time=30:00 --wrap="$inst/test_hipmer.sh SCRATCH=${SCRATCH}/runs validation-mg3 ecoli validation-mg mg250-short")
#    - echo "Submitted knl job $knl_job"
#    - inst=$SCRATCH/hipmer-install-cori/bin
#    - echo "Testing release build on Haswell - $inst"
#    - MIN_NODES=4 MAX_NODES=4 salloc --job-name="CI-HipMer-${CI_COMMIT_SHA}" --account=m342 -C haswell --nodes=6-8  --qos=debug --time=30:00  --ntasks-per-node=32 $inst/sbatch_cori.sh $inst/test_hipmer.sh SCRATCH=${SCRATCH}/runs validation2D-diploid ecoli validation validation-mg validation2D-diploid
#    - echo "Waiting for knl job $knl_job"
#    - date
#    - while sacct -j $knl_job -o state -X -n | grep ING 2>/dev/null ; do sleep 120 ; date ; done
#    - sacct=$(sacct -j $knl_job)
#    - echo "$sacct"
#    - wasgood=$(sacct -j $knl_job | grep -v JobID | grep -v -- --- | grep -v '0:0' || true)
#    - if [ -z "$wasgood" ] ; then  true ; else  echo "knl job failed somehow"; false ; fi


#
# hulk
#

HULK:build:
  stage: build
  tags:
    - HULK
  script:
    - set -e
    - set -x
    - git submodule init
    - git submodule sync
    - git submodule update
    - export BASE=/work/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - mkdir -p ${BASE}
    - export PATH=$CI_INSTALL/bin:/bin:/usr/bin:/usr/local/bin
    - export HIPMER_DATA=${BASE}/scratch/
    - export SCRATCH=${CI_SCRATCH}
    - echo "Establishing all tests under BASE=$BASE and SCRATCH=$CI_SCRATCH"
    - rm -rf ${CI_SCRATCH}
    - mkdir -p ${CI_SCRATCH}
    - env
    - uname -a
    - pwd
    - exec >  >(tee -ia ${CI_SCRATCH}/build.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/build.err >&2)
    - find . -type d -ls
    - date
    - echo "Purging any old tests"
    - ls -td ${BASE}/scratch/inst-* | tail -n +8 | xargs  rm -rf '{}' ';' || /bin/true
    - df -h
    - echo "Checking for cmake, Berkeley UPC and UPC++"
    - which cmake && cmake --version
    - which upcxx && which upcc || BUPCVER=${UPC_VER}  UPCXXVER=${UPCXX_VER} ./contrib/install_upc.sh smp posix $CI_INSTALL
    - upcxx --version
    - upcc --version
    - echo "Building all flavors"
    - mkdir -p ${CI_SCRATCH}/runs
    - export GASNET_BACKTRACE=1
    - export INSTALL_PREFIX=${CI_SCRATCH}
    - echo "Building debug"
    - TMPDIR=${CI_SCRATCH} DIST_CLEAN=1 .misc_deploy/build_envs.sh install .generic_deploy/env-debug.sh
    - inst=${CI_SCRATCH}/hipmer-install-Linux-debug/bin
    - echo "Testing debug build with cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-debug CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg3 validation2D-diploid
    - echo "Building other optmized flavor"
    - TMPDIR=${CI_SCRATCH} DIST_CLEAN=1 .misc_deploy/build_envs.sh install .generic_deploy/env.sh
    - inst=${CI_SCRATCH}/hipmer-install-Linux/bin
    - echo "Testing release build with cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg validation-mg3 validation2D-diploid
    - echo "Testing release build without cached-io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-nocio CACHED_IO=0 ${inst}/test_hipmer.sh validation validation-mg validation-mg3 validation2D-diploid
    - echo "Testing checkpointing with cached io - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio-checkpointing TEST_CHECKPOINTING=1 AUTORESTART=1 CACHED_IO=1 ${inst}/test_hipmer.sh validation validation-mg3 validation2D-diploid
    - echo "Testing extended checkpointing - $inst"
    - SCRATCH=${CI_SCRATCH}/runs-cio-checkpointing-ext TEST_CHECKPOINTING_EXTENDED=1 CACHED_IO=1 ${inst}/test_hipmer.sh validation
    - echo "Completed"

    

HULK:validation:
  stage: validation
  tags:
    - HULK
  script:
    - set -e
    - set -x
    - export BASE=/work/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - export PATH=$CI_INSTALL/bin:/bin:/usr/bin:/usr/local/bin
    - export HIPMER_DATA=${BASE}/scratch/
    - export SCRATCH=$CI_SCRATCH
    - echo "Validating all tests under BASE=$BASE and SCRATCH=$CI_SCRATCH"
    - env
    - df -h
    - uname -a
    - pwd
    - exec >  >(tee -ia ${CI_SCRATCH}/validation.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/validation.err >&2)
    - date
    - upcxx --version
    - upcc --version
    - inst=${CI_SCRATCH}/hipmer-install-Linux/bin
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh
    - echo "Downloading ecoli data - $inst"
    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh ecoli
    - echo "Testing release build - $inst"
    - echo "Completed"

HULK:accuracy:
  stage: accuracy
  tags:
    - HULK
  script:
    - set -e
    - set -x
    - git submodule init
    - git submodule sync
    - git submodule update
    - export BASE=/work/gitlab-ci/${CI_PROJECT_NAME}
    - export CI_SCRATCH=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}
    - export CI_INSTALL=${BASE}/ci-install-${CI_PROJECT_NAME}-upc-${BUPC_VER}-upcxx-${UPCXX_VER}
    - export PATH=$CI_INSTALL/bin:/bin:/usr/bin:/usr/local/bin
    - export HIPMER_DATA=${BASE}/scratch/
    - export SCRATCH=$CI_SCRATCH
    - echo "Running accuracy tests under BASE=$BASE and SCRATCH=$CI_SCRATCH"
    - env
    - df -h
    - uname -a
    - pwd
    - exec >  >(tee -ia ${CI_SCRATCH}/accuracy.log)
    - exec 2> >(tee -ia ${CI_SCRATCH}/accuracy.err >&2)
    - date
    - upcxx --version
    - upcc --version
    - export HIPMER_ECOLI_DATA=${HIPMER_DATA}/hipmer_ecoli_data
    - export HIPMER_MG250_DATA=${HIPMER_DATA}/hipmer_metagenome-250_data
    - export HIPMER_MOCK150_DATA=${HIPMER_DATA}/hipmer_mock150_data
    - export HIPMER_CHR14_DATA=${HIPMER_DATA}/hipmer_chr14_data
    - export HIPMER_HUMAN_DATA=${HIPMER_DATA}/hipmer_human_data
    - echo "Download data sets and install in ${CI_SCRATCH}/runs, cache in ${HIPMER_DATA} between CI instances"
    - inst=${CI_SCRATCH}/hipmer-install-Linux/bin
    - SCRATCH=${CI_SCRATCH}/runs ${inst}/test_hipmer.sh ecoli
    - echo "Completed"

after_script:
  - echo "Done"

