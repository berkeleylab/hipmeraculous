Docker support is (very) experimental and will ONLY work on a single machine.

To build docker, from git:
git submodule init
git submodule update
docker build -t robegan21/openmpi -f Docker/Dockerfile-openmpi .
docker push robegan21/openmpi
docker build -t robegan21/bupc -f Docker/Dockerfile-bupc .
docker push robegan21/bupc
docker build -t robegan21/hipmer -f Docker/Dockerfile-hipmer .
docker push robegan21/hipmer

To run in docker, you must set the --memory and --shm-size parameters to your specific machine and mount volumes for your inputs and outputs

Example:

docker run --memory 50g --shm-size=32g --volume $datadir:/scratch robegan21/hipmer:latest test_hipmer.sh

Where $datadir is the directory on the host containing the input files and where the output will be written to.

