#!/bin/bash

#source /opt/Modules/default/init/bash

module purge
#module rm PrgEnv-gnu
#module rm PrgEnv-cray
#module rm gcc
module use /usr/common/software/carl_modulefiles
module load intel impi bupc cmake
module load openmpi
#module load vtune/2016.up3
module list

export HIPMER_ENV=carl
export CC=$(which icc)
export CXX=$(which icpc)
export MPICC=$(which mpicc)
export MPICXX=$(which mpic++)
export HIPMER_BUILD_TYPE="Release"
export CORES_PER_NODE=${CORES_PER_NODE:=64}
export HIPMER_BUILD_OPTS="-DCMAKE_UPC_FLAGS=-network=mpi"


export SCRATCH=$HOME/scratch
