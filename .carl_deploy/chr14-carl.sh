#!/bin/bash

rm -rf /dev/shm/*

source /opt/Modules/default/init/bash
source .carl_deploy/env.sh
unset HIPMER_INSTALL
export SCRATCH=$HOME/scratch

export N=${N:=64}

export I_MPI_PIN_DOMAIN=$N
export CORES_PER_NODE=$N

export HIPMER_INSTALL=${SCRATCH}/install-carl
USAGE="$0 hipmer-install-path
or set environment: HIPMER_INSTALL=${HIPMER_INSTALL}
"
INST=${HIPMER_INSTALL:=$1}
if [ -d ${INST}/bin ] && [ -x ${INST}/bin/run_hipmer.sh ]
then
  src=$(echo ${INST}/env*.sh)
  if [ -f ${src} ]
  then
    . ${src}
  else
    echo "Could not find an environment file to source in ${INST}!" 1>&2
  fi
  
  module list || /bin/true

  export THREADS=${THREADS:=${N}}
  export RUNDIR=${RUNDIR:=$SCRATCH/chr14-${THREADS}-${SLURM_JOB_ID}-$(date '+%Y%m%d_%H%M%S')}
  echo "Preparing ${RUNDIR}"
  mkdir -p $RUNDIR

  $INST/bin/hipmer_setup_chr14_data.sh ${INST} ${RUNDIR}

  UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB:=1000}
  MPIRUN="mpirun -np" UPCRUN="upcrun -n" UPC_SHARED_HEAP_MB=${UPC_SHARED_HEAP_MB} ${INST}/bin/run_hipmer.sh $INST ${RUNDIR}/meraculous-chr14.config

else
  echo "Could not find HipMer installed at ${INST}"
  exit 1
fi

