#!/bin/bash
#SBATCH --qos=debug
#SBATCH --nodes=2
#SBATCH -C knl,quad,cache
#SBATCH --ntasks-per-node=68
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer
#SBATCH --license=SCRATCH

set -e

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

