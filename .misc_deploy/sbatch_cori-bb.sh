#!/bin/bash
#SBATCH --qos=debug
#SBATCH --nodes=2
#SBATCH -C haswell
#SBATCH --ntasks-per-node=32
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer
#SBATCH --license=SCRATCH
#DW jobdw capacity=1000GB access_mode=striped type=scratch

set -e

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

