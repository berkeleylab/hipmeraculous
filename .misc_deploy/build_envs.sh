#!/bin/bash -l

set -e

if [ $# -lt 2 ]
then
  echo "USAGE: $0 target env1 [env2 ...]"
  exit 1
fi

pids=
build_envs_cleanup()
{
  echo "WARNING trapped a signal!!!"
  [ -z "$pids" ] || ( kill -2 $pids ; sleep 1; kill $pids )
  pids=
  exit
}
  
trap build_envs_cleanup 0 1 2 3 15
  
target=$1
shift
# get the cores on Linux or Mac
cores=$(lscpu -p 2>/dev/null | awk -F, '!/^#/ {print $2}' | sort | uniq | wc -l || sysctl -a | awk '/^machdep.cpu.core_count/ {print $2}')
builds=$#
if [ -n "${BUILD_THREADS}" ] 
then
  cores=${BUILD_THREADS}
fi
if [ "${SERIAL_BUILD}" == "1" ]
then
  export BUILD_THREADS=${cores}
else
  export BUILD_THREADS=$(((cores+builds-1)/builds+1))
fi

if [ -d .git ] && [ -f .gitmodules ]
then
  echo "Updating submodules"
  git submodule update --init --recursive || echo "WARNING: Could not update the submodules!! This must be an uncommited active developement!"
fi

buildprefix=${HIPMER_BUILD:=${TMPDIR:=/tmp}}
mkdir -p ${buildprefix}
logs=
pids=
for env in $*
do
    shortenv=${env/\//-}
    shortenv=${shortenv%.*}
    log=${TMPDIR:=/tmp}/build-$target-${shortenv}-$$
    logs="$logs $log"
    builddir=${buildprefix}/build-${shortenv}
    mkdir -p $builddir
    env > $log.log
                                     HIPMER_BUILD=$builddir ./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=$env $target >>$log.log 2>$log.err || \
      DIST_CLEAN= REBUILD=           HIPMER_BUILD=$builddir ./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=$env $target >>$log.log 2>$log.err || \
      DIST_CLEAN= REBUILD= VERBOSE=1 HIPMER_BUILD=$builddir ./bootstrap_hipmer_env.sh HIPMER_ENV_SCRIPT=$env $target >>$log.log 2>&1  &
    pid=$!
    echo "Building ($pid) $env => $log.log and .err"
    if [ "${SERIAL_BUILD}" == "1" ]
    then
        myret=0
        wait $pid || myret=$?
        if [ $myret -ne 0 ]
        then
            echo ; echo "ERROR $pid failed!" ; echo ; echo
            cat $log.log
            echo ; echo
            echo -e "\x1B[91m *** WARNING one or more builds failed! *** \x1B[0m"
            echo $log.log
            exit 1
        fi
    else
      pids="${pids} $pid"
    fi
done

ret=0
for pid in $pids
do
  myret=0
  wait $pid || myret=$?
  if [ $myret -ne 0 ]
  then
     echo "ERROR $pid failed!"
     ret=$myret
  else
     echo "$pid succeeded!"
  fi
done

for log in $logs
do

  echo
  echo "Log: $log.log"
  echo
  if [ $ret -eq 0 ]
  then
     tail $log.log
     echo; echo "Err: $log.err"; echo
     tail $log.err
  else
     echo ; echo "Possible errors: $log.log $log.err" ; echo
     grep -A10 -B10 -i 'error\|warn' $log.log $log.err
     echo; echo "Log: $log.log "; echo
     cat $log.log 
     echo; echo "Err: $log.err / $log.log"; echo
     cat $log.err
  fi

done

echo
if [ $ret -ne 0 ]
then
  echo -e "\x1B[91m *** WARNING one or more builds failed! *** \x1B[0m"
else

  echo -e "\x1B[92m All builds were successful! \x1B[0m"
fi
echo

trap "" 0 1 2 3 15

exit $ret


