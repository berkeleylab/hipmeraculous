#!/bin/bash
#PBS -q small
#PBS -l nodes=3:ppn=8
#PBS -l walltime=00:30:00
#PBS -j oe
#PBS -N hsw16
#PBS -S /bin/bash
 
. /opt/modules/default/init/bash
 
cd $PBS_O_WORKDIR
 
ulimit -s unlimited
# ulimit -n 4800
ulimit -a
 
lscpu

env

export CORES_PER_NODE=$PBS_NUM_PPN
export THREADS=$((CORES_PER_NODE * PBS_NUM_NODES))
UPCRUN="aprun -cc depth -N $CORES_PER_NODE " $SCRATCH/hipmer-install-cce/bin/test_hipmer.sh ${HIPMER_TEST}
 
