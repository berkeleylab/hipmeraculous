#!/bin/bash
#SBATCH --qos=debug
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer
#SBATCH --license=SCRATCH,scratch3,cscratch1

set -e

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

