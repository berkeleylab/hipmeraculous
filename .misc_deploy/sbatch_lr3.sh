#!/bin/bash 
#SBATCH --job-name=Hipmer
#SBATCH --partition=lr3
#SBATCH --qos=lr_debug
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=16
#SBATCH --time=0:30:0

set -e

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

