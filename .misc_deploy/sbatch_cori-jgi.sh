#!/bin/bash
#SBATCH --account=gtrnd
#SBATCH --qos=genepool
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=32
#SBATCH --exclusive
#SBATCH --cpus-per-task=2
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer
#SBATCH --license=SCRATCH

set -e

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

