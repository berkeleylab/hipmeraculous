#!/bin/bash
#SBATCH --nodes=2
#SBATCH --exclusive
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=2
#SBATCH --time=00:30:00
#SBATCH --job-name=HipMer

set -e

if [ -z "${TMPDIR}" ] || [ ! -w "${TMPDIR}" ] || [ ${TMPDIR} == "/tmp" ]
then
  echo "WARNING: TMPDIR is not properly defined yet (${TMPDIR})!"
  if [ -d /scratch/tmp ]
  then
    export TMPDIR=/scratch/tmp
  else
    export TMPDIR=/tmp
  fi
fi

env | grep '\(SLURM\|UPC\|HIPMER\|GASNET\)'

echo "Executing '$@' at $(date) on $(uname -n)"

$@

