#!/bin/bash -l
#
# hipmer_common_env.sh [ command and arguments to execute ]
#
# Please do not modify this script for a specific machine environment
#
# All the environmental variables and functions to build, install and execute hipmer
# certain environmental variables can and should be overridden in a machine-specific
# environment file specified during build and automatically installed thereafter
#
# one should only need HIPMER_INSTALL or HIPMER_ENV_SCRIPT defined to boot strap
#

# first parse any ENV=val command line arguments

job_env_args=()
for arg in "$@"
do
  if [ "${arg/=}" != "${arg}" ]
  then
    job_env_args+=("export ${arg};")
    setenvlog="$setenvlog
Setenv $arg"
    eval "export ${arg%=*}='${arg##*=}'"
    shift
  else
    break
  fi
done

get_job_id()
{
  id="${PBS_JOBID}${SLURM_JOBID}${LSB_JOBID}${JOB_ID}${LOAD_STEP_ID}"
  if [ -z "${id}" ]
  then
    id=$(uname -n).$$
  fi
  echo $id
}

# don't log for non-build, i.e. running test scripts
if [ -z "${SKIP_LOG}" ]
then 
    # automatically log
    HIPMER_BOOTSTRAP_FUNC=${HIPMER_BOOTSTRAP_FUNC:=bootstrap}
    if [ -n "$*" ] ; then HIPMER_BOOTSTRAP_FUNC=$(echo "$*" | tr ' ' '-') ; fi
    HIPMER_LOG=${HIPMER_LOG:=hipmer-${HIPMER_BOOTSTRAP_FUNC##*/}-$(date '+%Y%m%d_%H%M%S')-$(get_job_id).log}

    if [ -n "${HIPMER_AUTO_LOG}" ] || [ "${HIPMER_AUTO_LOG}" != "0" ]
    then
        echo "Logging everything to ${HIPMER_LOG}"
        exec 3>&1 1> >(tee -ia $HIPMER_LOG)
        exec 4>&2 2> >(tee -ia $HIPMER_LOG >&2)
        export HIPMER_AUTO_LOG=0
    fi
fi

echo "Set the following environment variables: ${setenvlog}"

#######################################################################################
#  compatibility functions                                                            #
#######################################################################################

OS=$(uname -s)
_readlink()
{
  local f=$1
  if [ "$OS" == "Darwin" ]
  then
    realpath $f
  else
    if [ -L $f ]
    then
      readlink $f
    else
      readlink -f $f
    fi
  fi
}

getsockets()
{
  if [ "$OS" == "Darwin" ]
  then
    echo 1
  elif [ -x $(which lscpu || true) ]
  then
     lscpu --parse | awk -F, '!/^#/ {print $3}' | sort | uniq | wc -l
  elif [ -f /proc/cpuinfo ]
  then
    grep '^physical id' /proc/cpuinfo | sort | uniq | wc -l
  else
    echo 1
  fi
}

# auto-detect the number of cores per node
getcores()
{
  if [ "$OS" == "Darwin" ]
  then
     sysctl -a 2>/dev/null | awk '/^machdep.cpu.core_count/ {print $2}'
  elif  [ -x $(which lscpu || true) ]
  then
     lscpu --parse | awk -F, '!/^#/ {print $2}' | sort | uniq | wc -l
  elif [ -f /proc/cpuinfo ]
  then
     echo $(( $(grep "^physical id" /proc/cpuinfo | sort | uniq | wc -l) * $(awk '/cpu cores/ {print $4; exit;}' /proc/cpuinfo) ))
  else
     echo 1
  fi
}

# auto-detect the number of cpus (cores * hyperthreads) per node
getthreads()
{
  if [ "$OS" == "Darwin" ]
  then
     sysctl -a 2>/dev/null | awk '/^machdep.cpu.thread_count/ {print $2}'
  elif [ -x $(which lscpu || true) ]
  then
     lscpu --parse | awk -F, '!/^#/ {print $1}' | sort | uniq | wc -l
  elif [ -f /proc/cpuinfo ]
  then
     cat /proc/cpuinfo | grep "^processor" | wc -l
  else
     echo 1
  fi
}

if [ -z "$SCRATCH" ]
then
  cd # home is the new scratch
  export SCRATCH=$(pwd -P)
  echo "Set exported environmental variable SCRATCH=${SCRATCH}"
  cd -
fi

echo "Currently in $(pwd)"

if [ -z "${HIPMER_ENV_SCRIPT}" ] || [ ! -f "${HIPMER_ENV_SCRIPT}" ]
then

  testenv=${0%${0##*/}}hipmer_env.sh
  echo "First checking for ${testenv}"
  if [ -f "${testenv}" ]
  then
    echo "Detected default HIPMER_ENV_SCRIPT at ${testenv}... Using this"
    export HIPMER_ENV_SCRIPT=${testenv}
  else
    # attempt to bootstrap if called via fully qualified path within a valid HIPMER_INSTALL/bin directory
    ABSOLUTE_PATH=$0
    if [ -n "${ABSOLUTE_PATH##/*}" ]
    then
       # attempt to get resolve relative path
       ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)/$(basename "${BASH_SOURCE[0]}")"
    fi

    testbin=${ABSOLUTE_PATH%/*}
    testprebin=${testbin%/bin}
    if [ -x "${testbin}/run_hipmer.sh" ] && [ "${testbin}" != "${testprebin}" ] && [ -d "${testprebin}" ]
    then
       test_HIPMER_INSTALL=${testprebin}
       echo "Next checking for ${test_HIPMER_INSTALL}/env.sh"
       if [ -f ${test_HIPMER_INSTALL}/env.sh ]
       then
         export HIPMER_ENV_SCRIPT=${test_HIPMER_INSTALL}/env.sh
       fi
    fi
  fi

fi

if [ -f "${HIPMER_ENV_SCRIPT}" ]
then
  echo "Sourcing ${HIPMER_ENV_SCRIPT}"
  source ${HIPMER_ENV_SCRIPT}
else
  echo "No HIPMER_ENV_SCRIPT was found"
fi

if [ -z "$SCRATCH" ] || ( [ ! -d "$SCRATCH" ] && ! mkdir -p $SCRATCH )
then
  echo "ERROR!!! You must define SCRATCH as a common dectory that all threads can access"
  exit 1
fi

HIPMER_ENV=${HIPMER_ENV:=$(uname -s)}
export HIPMER_INSTALL=${HIPMER_INSTALL:=${INSTALL_PREFIX:=$SCRATCH}/hipmer-install-${HIPMER_ENV}}
[ ! -d "${HIPMER_INSTALL}/bin" ] || export PATH=${HIPMER_INSTALL}/bin:$PATH

# TMPDIR is high speed and okay to be local
export TMPDIR=${TMPDIR:=/tmp}

# HIPMER_BUILD - only used for building the code
export HIPMER_BUILD=${HIPMER_BUILD:=${TMPDIR}/${USER}-hipmer-build-${HIPMER_ENV}}

export CC=${CC:=$(which cc || which icc || which gcc)}
export CXX=${CXX:=$(which CC || which c++ || which icpc || which g++)}
export UPCC=${UPCC:=${HIPMER_UPCC:=$(which upcc || echo $CC)}}
export UPCRUN=${UPCRUN:=$(which upcrun) -q}
export CMAKE=${CMAKE:=$(which cmake)}
export LFS=${LFS:=$(which lfs 2>/dev/null || true)}

MYENV="
  CC=${CC}			- the C compiler (potentially mpi-enabled)
  CXX=${CXX}			- the C++ compiler (potentially mpi-enabled)
  UPCC=${UPCC}			- the UPC compiler
  UPCRUN=${UPCRUN}		- the upcrun command
  CMAKE=${CMAKE}         	- the cmake command
  LFS=${LFS}			- the (optional) path to Luster File System control script
  HIPMER_UDP_NODES=${HIPMER_UDP_NODES} 
                                - if compiled with UDP conduit, set this to the list of nodes (once per node) to automatically generate the UPC_NODES and THREADS env var
"

MYDIRS="
  TMPDIR=${TMPDIR} - fast, local directory (i.e. /tmp or /dev/shm)
  SCRATCH=${SCRATCH} - global, shared, networked directory (${INSTALL_PREFIX}/hipmer-scratch)
  INSTALL_PREFIX=${INSTAL_PREFIX} ($HOME)
  HIPMER_INSTALL=${HIPMER_INSTALL} - global installation directory (${INSTALL_PREFIX}/hipmer-install)
"

test_building()
{
  foundall=1
  for test_exe in "$CC" "$CXX" "$UPCC" "${CMAKE}"
  do
    if [ ! -x "$(which ${test_exe})" ]
    then
      foundall=0
      echo "Failed to find ${test_exe}!" 1>&2
    fi
  done
  

  if [ ${foundall} -ne 1 ]
  then
    echo "You must have specified the following environmental variables:
${MYENV}

Perhaps you should set and/or configure a machine specific HIPMER_ENV_SCRIPT?
" 1>&2
    exit 1
  fi
}

for testdir in "${TMPDIR}" "${SCRATCH}" "${HIPMER_INSTALL%/*}"
do
  if [ ! -d "${testdir}" ]
  then
    echo "You must specify valid paths for the following variables:
${MYDIRS}

${testdir} could not be found
Perhaps you should set and/or configure a machine specific HIPMER_ENV_SCRIPT?
" 1>&2
    exit 1
  fi
done

for testdir in "${TMPDIR}" "${SCRATCH}"
do 
  if [ ! -w "${testdir}" ]
  then
     echo "You must be able to write to $testdir!" 1>&2
     exit 1
  fi
done

#######################################################################################
#  These variables should be overridden where necessary for each platform.            #
#  Ideally within the hipmer_env.sh script ($HIPMER_ENV_SCRIPT) referenced above.     #
#######################################################################################

# define the build type and options (Release, Debug, RelWithDebugFlags)
export HIPMER_BUILD_TYPE=${HIPMER_BUILD_TYPE:=Release}
export HIPMER_BUILD_OPTS="${HIPMER_BUILD_OPTS:=}"
export HIPMER_SRC=${HIPMER_SRC:=$(pwd)}
export HIPMER_POST_INSTALL=${HIPMER_POST_INSTALL:=}

#export PHYS_MEM_MB=${PHYS_MEM_MB:=$(awk '/MemTotal:/ { t=$2 ; print int(t / 1024)}' /proc/meminfo 2>/dev/null || sysctl -a | awk '/hw.memsize:/ {t=$2; print int(t/1024/1024)}' || echo 8000)}
#MIN_RESERVED_MEM=$((PHYS_MEM_MB/20)) # 5% reserved
#[ ${MIN_RESERVED_MEM} -gt 2000 ] || MIN_RESERVED_MEM=2000 # at least 2GB reserved
#PHYS_MEM_MB=$((PHYS_MEM_MB-MIN_RESERVED_MEM))
#if [ -z "${HIPMER_PROBE_MEM}" ]
#then
#  export GASNET_PHYSMEM_MAX=${GASNET_PHYSMEM_MAX:=${PHYS_MEM_MB}MB}
#  export GASNET_PHYSMEM_NOPROBE=${GASNET_PHYSMEM_NOPROBE:=1}
#fi

export BUILD_THREADS=${BUILD_THREADS:=$(($(getthreads)*3/2))}

export HIPMER_SHARED_MEM_PCT=${HIPMER_SHARED_MEM_PCT:=48}

UPC_PTHREADS=${UPC_PTHREADS:=}
UPC_PTHREADS_OPT=
if [ -n "$UPC_PTHREADS" ] && [ $UPC_PTHREADS -ne 0 ]
then
  UPC_PTHREADS_OPT="-pthreads=${UPC_PTHREADS}"
fi

##################################################################################
#          YOU SHOULD NEVER NEED TO MODIFY ANYTHING BELOW HERE                   #
##################################################################################

export DIST_CLEAN=${DIST_CLEAN:=}
export REBUILD=${REBUILD:-}
export CLEAN=${CLEAN:=}

set_dir_striping()
{
  local stripe=$1
  local dir=$2
  echo "set_dir_striping ${stripe} ${dir}"
  if [ -x "${LFS}" ]
  then
     lfsdf=$(${LFS} df ${dir})
     if [ -z "${lfsdf}" ]
     then
         echo "LUSTRE is not on ${dir}"
     else
       if [ "$stripe" == "-1" ]
       then
          stripe=$(( $(echo "${lfsdf}" | grep -c _UUID) * 8 / 10))
          echo "using stripe ${stripe} instead of -1"
       fi

       if [ ${stripe} -le 1 ]
       then
           stripe=1
       fi
       echo "Setting lustre stripe to ${stripe} on ${dir}"
       ${LFS} setstripe -c ${stripe} ${dir} || true
     fi
  else
     echo "LFS not detected, skipping stripe count $1 on $2"
  fi
}

build_hipmer_call_cmake()
{
  cd ${HIPMER_BUILD}
  cmakelog=${HIPMER_BUILD}/cmake.log
  if [ ! -f ${cmakelog} ]
  then
    # support other environmental variable based build options
    HIPMER_BUILD_OPTS2=
    [ -z "${HIPMER_FULL_BUILD}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_FULL_BUILD=${HIPMER_FULL_BUILD}"
    [ -z "${HIPMER_STATIC_BUILD}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_STATIC_BUILD=${HIPMER_STATIC_BUILD}"
    [ -z "${HIPMER_KHASH}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_KHASH=${HIPMER_KHASH}"
    [ -z "${HIPMER_USE_PTHREADS}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DCMAKE_UPC_USE_PTHREADS=${HIPMER_USE_PTHREADS}"
    [ -z "${HIPMER_READ_BUFFER}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_READ_BUFFER=${HIPMER_READ_BUFFER}"
    [ -z "${HIPMER_MAX_KMER_SIZE}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_MAX_KMER_SIZE=${HIPMER_MAX_KMER_SIZE}"
    [ -z "${HIPMER_KMER_LENGTHS}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_MAX_KMER_SIZE=${HIPMER_KMER_LENGTHS}"
    [ -z "${HIPMER_MAX_FILE_PATH}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_MAX_KMER_SIZE=${HIPMER_MAX_FILE_PATH}"
    [ -z "${HIPMER_MAX_READ_NAME}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_MAX_READ_NAME=${HIPMER_MAX_READ_NAME}"
    [ -z "${HIPMER_MAX_LIBRARIES}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_MAX_LIBRARIES=${HIPMER_MAX_LIBRARIES}"
    [ -z "${HIPMER_LIB_NAME_LEN}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_LIB_NAME_LEN=${HIPMER_LIB_NAME_LEN}"
    [ -z "${HIPMER_VERBOSE}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_VERBOSE=${HIPMER_VERBOSE}"
    [ -z "${HIPMER_BLOOM}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_BLOOM=${HIPMER_BLOOM}"
    [ -z "${HIPMER_DISCOVER_LIBC}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_DISCOVER_LIBC=${HIPMER_DISCOVER_LIBC}"
    [ -z "${HIPMER_SLACK}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_SLACK=${HIPMER_SLACK}"
    [ -z "${HIPMER_CHUNK_SIZE}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_CHUNK_SIZE=${HIPMER_CHUNK_SIZE}"
    [ -z "${HIPMER_NO_UPC_MEMORY_POOL}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DNO_UPC_MEMORY_POOL=1"
    [ -z "${HIPMER_BROKEN_ALLOCATOR_REBIND}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DBROKEN_ALLOCATOR_REBIND=1"
    [ -z "${HIPMER_ALTIVEC}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_ALTIVEC=${HIPMER_ALTIVEC}"
    [ -z "${HIPMER_NO_UNIT_TESTS}" ] ||  HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_NO_UNIT_TESTS=${HIPMER_NO_UNIT_TESTS}"
    [ -z "${HIPMER_TEST}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_ENABLE_TESTING=${HIPMER_TEST}"
    [ -z "${HIPMER_BUILD_TEST}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_ENABLE_TESTING=${HIPMER_BUILD_TEST}"
    [ -z "${HIPMER_ENABLE_TESTING}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_ENABLE_TESTING=${HIPMER_ENABLE_TESTING}"
    [ -z "${HIPMER_HWATOMIC}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_HWATOMIC=${HIPMER_HWATOMIC}"
    [ -z "${HIPMER_NO_AVX512F}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_NO_AVX512F=${HIPMER_NO_AVX512F}"
    [ -z "${HIPMER_NO_AIO}" ] ||  HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_NO_AIO=${HIPMER_NO_AIO}"
    [ -z "${HIPMER_EMBED_HMMER}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_EMBED_HMMER=${HIPMER_EMBED_HMMER}"
    [ -z "${HIPMER_HMMER_CONFIGURE_OPTS}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_HMMER_CONFIGURE_OPTS='${HIPMER_HMMER_CONFIGURE_OPTS}'"
    [ -z "${HIPMER_NO_HH}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_NO_HH=1"
    [ -z "${HIPMER_CRAY_MPI_HACK}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_CRAY_MPI_HACK=${HIPMER_CRAY_MPI_HACK}"
    [ -z "${HIPMER_NO_CGRAPH}" ] || HIPMER_BUILD_OPTS2="${HIPMER_BUILD_OPTS2} -DHIPMER_NO_CGRAPH=1"
    HIPMER_UPC_COMPILER=
    [ -z "${HIPMER_UPCC}" ] || HIPMER_UPC_COMPILER="-DCMAKE_UPC_COMPILER='${HIPMER_UPCC}'"
    HIPMER_UPC_FLAGS_INIT=
    [ -z "${HIPMER_UPC_FLAGS}" ] || HIPMER_UPC_FLAGS_INIT="-DCMAKE_UPC_FLAGS_INIT='${HIPMER_UPC_FLAGS}'"
    CMAKE_CXX11_FLAG=
    [ -z "${HIPMER_CXX11_FLAG}" ] || CMAKE_CXX11_FLAG="-DCXX11_FLAG='${HIPMER_CXX11_FLAG}'"
    set -e
    set -x
    eval ${CMAKE} -DCMAKE_INSTALL_PREFIX=${HIPMER_INSTALL} -DCMAKE_BUILD_TYPE=${HIPMER_BUILD_TYPE} \
        ${HIPMER_BUILD_OPTS} \
        ${HIPMER_BUILD_OPTS2} \
        ${HIPMER_UPC_COMPILER} ${HIPMER_UPC_FLAGS_INIT} ${CMAKE_CXX11_FLAG} \
        ${HIPMER_SRC} 2>&1 | tee -a ${cmakelog}.tmp && [ ${PIPESTATUS[0]} -eq 0 ] \
    && mv ${cmakelog}.tmp ${cmakelog} || ret=1
    set +x
    set +e
  fi
  cd -
  return $ret
}

clean_or_make_hipmer_build_dir()
{
  if [ -e "${HIPMER_BUILD}" ]
  then
    # check for same source directory
    cmakecache=${HIPMER_BUILD}/CMakeCache.txt
    if [ -f ${cmakecache} ]
    then
      testsrc=$(grep HipMer_SOURCE_DIR ${cmakecache} | sed 's/.*=//;')
      if [ "${testsrc}" != "${HIPMER_SRC}" ]
      then
        echo "Source dirs do not match ${testsrc} vs ${HIPMER_SRC}. performing a REBUILD=1 build to ${HIPMER_SRC}"
        export REBUILD=1
      fi
    else
      echo "Incompleted build dir, setting REBUILD=1"
      export REBUILD=1
    fi
  fi

  if ( [ "${REBUILD}" == "1" ] || [ "${DIST_CLEAN}" == "1" ] ) && [ -d "${HIPMER_BUILD}" ]
  then
    echo "Cleaning out old builds HipMer: ${HIPMER_BUILD}"
    chmod -R u+w ${HIPMER_BUILD}
    rm -r ${HIPMER_BUILD}
    if [ "${DIST_CLEAN}" == "1" ] && [ -d "${HIPMER_INSTALL}" ]
    then
      ( [ -w "${HIPMER_INSTALL}/." ] && echo "purging install ${HIPMER_INSTALL}" && rm -rf ${HIPMER_INSTALL} ) || echo "Could not DIST_CLEAN HIPMER_INSTALL: ${HIPMER_INSTALL}" 1>&2
    fi
    unset DIST_CLEAN
    unset REBUILD
  fi
  if [ ! -d ${HIPMER_BUILD} ]
  then
    echo "Creating build dir: ${HIPMER_BUILD}"
    mkdir ${HIPMER_BUILD}
    set_dir_striping 1 ${HIPMER_BUILD}
    if (cd ${HIPMER_BUILD} && build_hipmer_call_cmake)
    then
       echo "CMake environment is ready"
    else
       echo "Could not prepare the build environment."
       exit 1
    fi
  fi
  echo "Using build dir: ${HIPMER_BUILD}"
  if [ -n "${CLEAN}" ] && [ "${CLEAN}" == "1" ]
  then
    (cd ${HIPMER_BUILD} ; [ ! -d src/_project_libhmmer-stamp ] || [ -d ._project_libhmmer-stamp ] || mv src/_project_libhmmer-stamp ._project_libhmmer-stamp ; make clean ; [ -d src/_project_libhmmer-stamp ] || [ ! -d ._project_libhmmer-stamp ] || mv ._project_libhmmer-stamp src/_project_libhmmer-stamp )
  fi
  return 0
}

configure()
{
  test_building \
  && clean_or_make_hipmer_build_dir \
  || return $?
}

reconfigure()
{
  configure \
    && rm -f ${HIPMER_BUILD}/cmake.log \
    && build_hipmer_call_cmake || return $?
}
  
build()
{
  set -e
  ret=0
  configure \
    && build_hipmer_call_cmake || ret=$?
  if [ "${VERBOSE}" == "1" ]
  then
    export BUILD_THREADS=1
  fi
  if [ ${ret} -eq 0 ]
  then
    ( cd ${HIPMER_BUILD} && time nice make -j ${BUILD_THREADS} REPLACE_VERSION_H ) || ret=$?
  else
    if [ "${DIST_CLEAN}" != "1" ] && [ "${REBUILD}" != "1" ] && [ "${LAST_TRY}" != "1" ]
    then
      ret=0
      # try again
      export REBUILD=1
      export LAST_TRY=1
      build || ret=$?
      unset REBUILD
      return $ret
    else
      echo
      echo "Could not make REPLACE_VERSION_H... Please retry setting REBUILD=1 before contacting the developers"
      echo
      return 1
    fi
  fi
  [ $ret -eq 0 ] || return $ret
  ( cd ${HIPMER_BUILD} && time nice make -j ${BUILD_THREADS} 2>&1 | tee build.log | grep -v "jobserver unavailable" && [ ${PIPESTATUS[0]} -eq 0 ] ) || return 1
  echo "Building complete"
  set +e
  return $ret
}

install()
{
  set -e
  clean_or_make_hipmer_build_dir || return $?
  [ -d ${HIPMER_BUILD} ] && [ "${DIST_CLEAN}" != "1" ] && [ "${REBUILD}" != "1" ] || build
  if [ -z "${HIPMER_INSTALL}" ] ; then echo "Can not install -- empty HIPMER_INSTALL variable"; return 1 ; fi
  cd ${HIPMER_BUILD}

  mkdir -p ${HIPMER_INSTALL}
  set_dir_striping 1 ${HIPMER_INSTALL}
  if [ "${VERBOSE}" == "1" ]
  then
    export BUILD_THREADS=1
  fi

  ret=0
  time nice make -j ${BUILD_THREADS} install test 2>&1 | grep -v "jobserver unavailable" && [ ${PIPESTATUS[0]} -eq 0 ] || ret=$?
  
  cd -

  [ $ret -eq 0 ] || return $ret

  echo "HipMer installation complete"
  set +e
}

test()
{
  set -e
  clean_or_make_hipmer_build_dir
  [ -d ${HIPMER_BUILD} ] && [ "${DIST_CLEAN}" != "1" ] && [ "${REBUILD}" != "1" ] || build
  ( cd ${HIPMER_BUILD} && time nice make test )
}

setup_RUNDIR()
{
  myname=$1
  [ -n "${myname}" ] || myname=HipMer
  export RUNDIR=${RUNDIR:=$SCRATCH/${myname}-${THREADS}-$(date '+%Y%m%d_%H%M%S')-$(get_job_id)}
  oldumask=$(umask)
  umask 0000
  mkdir -p ${RUNDIR}/per_thread
  mkdir -p ${RUNDIR}/results ${RUNDIR}/intermediates
  umask $oldumask
  set_dir_striping -1 ${RUNDIR}/results
  set_dir_striping -1 ${RUNDIR}/intermediates
  set_dir_striping 1 ${RUNDIR}/per_thread
  set_dir_striping 1 ${RUNDIR}/.
  echo "created RUNDIR=${RUNDIR}"
}
stagein_RUNDIR()
{
  dest=$1
  if [ -n "$dest" ] && [ -d "$dest" ] && [ "$dest" != "${RUNDIR}" ]
  then
    echo "Staging RUNDIR from $RUNDIR to burst buffer datawarp $dest $(date)"
    df -h $dest
    setupstart=$SECONDS
    export STAGE_RUNDIR=$RUNDIR
    export RUNDIR=$dest/${RUNDIR##*/}
    oldumask=$(umask)
    umask 0000
    mkdir -p ${RUNDIR}
    mkdir -p ${RUNDIR}/per_thread ${RUNDIR}/results ${RUNDIR}/intermediates

    stagein_pids=
    for i in ${STAGE_RUNDIR}/*
    do
        # copy small files or symlink large ones
        tgt="${RUNDIR}/${i##*/}"
        if [ ! -d "$i" ] && [ ! -f "${tgt}" ] && [ ! -h "${tgt}" ]
        then
            if [ -f "$i" ] 
            then
                if [ -s "$i" ] && [ ! -h "$i" ] && [ $(stat -c '%s' $i) -lt 16777216 ] && [ "$i" != "${STAGE_RUNDIR}/run.out" ]
                then
                    cp -p $i ${RUNDIR}/
                else
                    ln -s $i ${RUNDIR}/
                fi  &
                stagein_pids="$stagein_pids $!"
            elif [ -L "$i" ]
            then
                ln -s $(readlink $i) ${RUNDIR}/${i##*/}
            fi
        fi
    done
    umask $oldumask
    [ -z "$stagein_pids" ] || wait $stagein_pids

    echo "New RUNDIR is $RUNDIR"
    echo "Staged-in $((SECONDS-setupstart)) seconds $(date)"
    ls -la ${RUNDIR}/
  fi
}
stageout_RUNDIR()
{
  if [ -n "${STAGE_RUNDIR}" ] && [ -d "${STAGE_RUNDIR}" ] && [ "$RUNDIR" != "$STAGE_RUNDIR" ]
  then
    echo "Copying Staged ${RUNDIR} back to ${STAGE_RUNDIR} at $(date)"
    starttime=$SECONDS
    df -h ${RUNDIR}
    ( cd ${RUNDIR} ; tar -cf ${STAGE_RUNDIR}/per_thread.tar.tmp per_thread/[^0]* per_thread/0*/*/HipMer*.log && mv ${STAGE_RUNDIR}/per_thread.tar.tmp ${STAGE_RUNDIR}/per_thread.tar ) &
    stageout_pids=$!
    dirs=$RUNDIR/results
    files=
    [ -n "${HIPMER_BB_PERSISTANT_NAME}" ] || [ ! -d ${RUNDIR}/intermediates/ ] || dirs="${dirs} ${RUNDIR}/intermediates"
    if ! [ ${RUNDIR}/run.out -ef ${STAGE_RUNDIR}/run.out ]
    then
        echo "How did run.out get out of sync?  It should be a symlink: $(ls -la ${RUNDIR}/run.out ${STAGE_RUNDIR}/run.out)"
        files="${files} ${RUNDIR}/run.out"
    fi
    rsync -a $(find ${RUNDIR} -maxdepth 1 -name '*.log' -o -name '*.txt' -o -name '*.sorted' -o -name 'completed') ${RUNDIR}/.hipmer_command ${files} ${dirs} ${STAGE_RUNDIR}/ &
    stageout_pids="${stageout_pids} $!"
    wait ${stageout_pids}
    echo "Finished Staged-out ${RUNDIR} back to ${STAGE_RUNDIR} at $(date) in $((SECONDS-starttime)) seconds"
    export RUNDIR=${STAGE_RUNDIR}
    unset STAGE_RUNDIR
  fi
}

echo "The following environment is now active:

$(env | grep '\(HIPMER\|GASNET\|UPC\)')

PATH=${PATH}
"

if [ -f "$HIPMER_INSTALL/HIPMER_VERSION" ]
then
  HIPMER_BOOTSTRAPPED=$(cat $HIPMER_INSTALL/HIPMER_VERSION)
  export HIPMER_VERSION=${HIPMER_BOOTSTRAPPED}
  echo "HIPMER_VERSION: ${HIPMER_VERSION}"
else
  HIPMER_BOOTSTRAPPED=1
fi

EXIT_VAL=0
endlog()
{
  if [ -n "${HIPMER_LOG}" ]
  then
    exec 1>&3 2>&4
    echo "$(date) - $SECONDS sec"
    built=$(grep -c Building ${HIPMER_LOG} || true)
    linked=$(grep -c Linking ${HIPMER_LOG} || true)
    installed=$(grep -c Installing ${HIPMER_LOG} || true)
    echo "Built $built, Linked $linked, Installed $installed"
    echo "You should be able to find a log of everything that just happened at ${HIPMER_LOG}"
  fi
  trap '' 0
  if [ -n "$@" ] || [ ${EXIT_VAL} -ne 0 ]
  then
    exit $@ ${EXIT_VAL}
  fi
}

if [ -n "$*" ] && [ "${0##*/}" == "bootstrap_hipmer_env.sh" ]
then
  echo "$0 (bootstrap) will now execute: $@"
  date
  $@ || EXIT_VAL=$?
  if [ $EXIT_VAL -ne 0 ]
  then
     echo "ERROR: '$@' returned $EXIT_VAL!" 1>&2
  fi
  echo "$0 (bootstrap) finished executing with exit val: ${EXIT_VAL}"
  trap "" 0
  endlog
  exit ${EXIT_VAL}
fi

